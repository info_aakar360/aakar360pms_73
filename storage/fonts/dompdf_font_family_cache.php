<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '/glyphicons_halflings_normal_9d43cd88056a1d18e9d6393ff18d0376',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/fontawesome_normal_898be7b8f0ca6c7340d534bc7af7553b',
  ),
  'themify' => array(
    'normal' => $fontDir . '/themify_normal_c88d8198192ead12bc8978d9f8099aaf',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '/simple_line_icons_normal_52ea6c6a673a8390a058aa70130b8954',
  ),
  'weathericons' => array(
    'normal' => $fontDir . '/weathericons_normal_b34e3731ccf83d2d289dd6d20f2b5ec3',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '/linea_arrows_10_normal_775bef747e55bf1174b3ccfb8f10233e',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '/linea_basic_10_normal_91135fa23b4bcd0facbb55e5e09c1ed3',
  ),
  'linea-basic-elaboration-10' => array(
    'normal' => $fontDir . '/linea_basic_elaboration_10_normal_da6b50a04b2a256af4eb7c5e9208c63c',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '/linea_ecommerce_10_normal_457e5a1c4e6e9751e53e509d2f61930e',
  ),
  'linea-music-10' => array(
    'normal' => $fontDir . '/linea_music_10_normal_8d705c2ab4bad30a751af97159657d3e',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '/linea_software_10_normal_0e79f57cecfc4b74d76380aaea0b24c5',
  ),
  'linea-weather-10' => array(
    'normal' => $fontDir . '/linea_weather_10_normal_c01a18e58c06e3cd4ad2c48ccef2d967',
  ),
) ?>