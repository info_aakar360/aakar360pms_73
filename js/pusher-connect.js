$.fn.noop = function () { return this; };
$(document).ready(function(){
    toastr.options = {
        "preventDuplicates": true,
        "preventOpenDuplicates": true,
        "positionClass": "toast-top-center"
    };
    let socket_id = $('input[name=socket_id]').val();
    let pusher = new Pusher('56d658761051d805686d', {
        //authEndpoint: base_url+'channels/authorize',
        cluster: 'ap2',
        /*auth: {
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        }*/
    });
    let conversation = pusher.subscribe('conversations');
    conversation.bind("conversation."+socket_id, (data) => {
            if(data.module=='chat'){
                let conversationid = data.moduleid;
                conversationchat(conversationid);
            }
            if(data.module=='projectpulse'){
                let conversationid = data.moduleid;
                getProjectLogData(conversationid);
            }
            if(data.module=='groupchat'){
                let conversationid = data.moduleid;
                getGroupChatData(conversationid);
            }
    });

    let channel = pusher.subscribe('notifications');
    channel.bind("new-notification."+socket_id, (data) => {
        alert(data.message);
        //updateNotsCount(data.count);
        /*$('input[name=nots_loaded]').val(0);
        if(nots_page == 1){
            location.reload();
        }*/
    });
    channel.bind("translation-refresh."+socket_id, (data) => {
        if($('.refresh-translation').length){
            $('.refresh-translation').trigger('click');
        }
    });
    channel.bind("read-notifications."+socket_id, (data) => {
        updateNotsCount(data.count);
        $('input[name=nots_loaded]').val(0);
    });
    function updateNotsCount(count){
        if(count){
            let html = '<span class="nots-count badge badge-danger">'+count+'</span>';
            if($('span.nots-count').length){
                $("span.nots-count").html(count);
            }else{
                $.each($(".notification-bell"), function(){
                    if($(this).hasClass("btn-icon-primary")){
                        $(this).append(html);
                    }else{
                        $(this).find("> a").append(html);
                    }
                });
            }
        }else{
            if($('span.nots-count').length){
                $("span.nots-count").remove();
            }
        }
    }
});

