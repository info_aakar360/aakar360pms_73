<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdInIncomeExpenseGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('income_expense_groups', function (Blueprint $table) {
            $table->integer('company_id')->default(0)->after('id');
            $table->string('income_type')->nullable()->after('company_id');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_expense_groups', function (Blueprint $table) {
            $table->dropColumn('company_id');
            $table->dropColumn('income_type');
        });
    }
}
