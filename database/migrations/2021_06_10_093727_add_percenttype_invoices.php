<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercenttypeInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('subproject_id')->nullable()->after('project_id');
            $table->string('percenttype')->nullable()->after('subproject_id');
            $table->string('percentvalue')->nullable()->after('percenttype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['subproject_id']);
            $table->dropColumn(['percenttype']);
            $table->dropColumn(['percentvalue']);
        });
    }
}
