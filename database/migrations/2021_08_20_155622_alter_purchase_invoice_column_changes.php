<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPurchaseInvoiceColumnChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice', function (Blueprint $table) {
            $table->integer('freight')->nullable()->change();
            $table->integer('gt')->nullable()->change();
            $table->integer('remark')->nullable()->change();
            $table->integer('payment_terms')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice', function (Blueprint $table) {
            $table->dropIfExists('freight');
            $table->dropIfExists('gt');
            $table->dropIfExists('remark');
            $table->dropIfExists('payment_terms');
        });
    }
}
