<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLatLongInAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances', function (Blueprint $table) {
            $table->string('clock_in_lat')->nullable();
            $table->string('clock_in_lang')->nullable();
            $table->string('clock_out_lat')->nullable();
            $table->string('clock_out_lang')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendances', function (Blueprint $table) {
            $table->dropIfExists('clock_in_lat');
            $table->dropIfExists('clock_in_lang');
            $table->dropIfExists('clock_out_lat');
            $table->dropIfExists('clock_out_lang');
        });
    }
}
