<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModuleidToConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conversation', function (Blueprint $table) {
            $table->string('moduleid')->nullable()->after('user_two');
            $table->string('module')->nullable()->after('moduleid');
            $table->text('users')->nullable()->after('module');
            $table->string('lastupdated')->nullable()->after('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conversation', function (Blueprint $table) {
            $table->dropColumn('moduleid');
            $table->dropColumn('module');
            $table->dropColumn('users');
            $table->dropColumn('lastupdated');
        });
    }
}
