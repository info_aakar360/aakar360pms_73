<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractInvoicesBoqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contract_invoices_boq', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('invoice_id')->nullable();
			$table->string('quantity')->nullable();
			$table->string('product_id')->nullable();
			$table->string('percent')->nullable();
			$table->string('amount')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contract_invoices_boq');
	}

}
