<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoqTemplatePositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boq_template_position', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default(0);
            $table->string('company_id')->default(0);
            $table->string('template_id')->default(0);
            $table->string('position')->nullable()->comment('row,col');
            $table->string('itemid')->default(0);
            $table->string('itemname')->nullable();
            $table->string('itemslug')->nullable();
            $table->tinyInteger('collock')->default(0);
            $table->tinyInteger('catlevel')->nullable();
            $table->integer('parent')->default(0);
            $table->integer('level')->default(0);
            $table->integer('inc')->default(0);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boq_template_position');

    }
}
