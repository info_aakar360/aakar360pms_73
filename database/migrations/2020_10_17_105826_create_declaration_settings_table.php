<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclarationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declaration_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('monthly_dec_from');
            $table->string('monthly_dec_to');
            $table->string('cutoff_dec_month');
            $table->string('cutoff_dec_month_date');
            $table->dateTime('yearly_dec_from');
            $table->dateTime('yearly_dec_to');
            $table->string('apply_to_emp_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declaration_settings');
    }
}
