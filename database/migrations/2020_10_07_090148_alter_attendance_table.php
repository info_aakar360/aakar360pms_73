<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances', function($table) {
            $table->text('clock_in_image')->default(NULL)->after('clock_out_time');
            $table->text('clock_out_image')->default(NULL)->after('clock_in_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendances', function($table) {
            $table->dropColumn('clock_in_image');
            $table->dropColumn('clock_out_image');
        });
    }
}
