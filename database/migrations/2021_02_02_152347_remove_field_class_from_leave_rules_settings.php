<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldClassFromLeaveRulesSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_rules_settings', function (Blueprint $table) {
            $table->longText('field_id_class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_rules_settings', function (Blueprint $table) {
            Schema::dropColumn('field_id');
            Schema::dropColumn('field_class');
            Schema::dropColumn('data-state');
            Schema::dropColumn('type');
        });
    }
}
