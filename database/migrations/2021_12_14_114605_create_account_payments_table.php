<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('account_payments', function (Blueprint $table) {
            $table->id();
            $table->string('company_id');
            $table->string('ledger_from')->default(0)->nullable();
            $table->string('ledger_to')->default(0)->nullable();
            $table->string('amount')->default(0)->nullable();
            $table->string('type')->default(0)->nullable();
            $table->string('prevbalance')->default(0)->nullable();
            $table->string('balance')->default(0)->nullable();
            $table->string('voucher_date')->nullable();
            $table->string('particulars')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_payments');
    }
}
