<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPunchItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('punch_item', function (Blueprint $table) {
            $table->string('type')->nullable()->default(null)->after('schimpact_days')->change();
            $table->string('distribution')->nullable()->default(null)->after('type')->change();
            $table->text('description')->nullable()->after('distribution')->change();
            $table->string('location')->nullable()->after('description')->change();
            $table->string('priority')->nullable()->after('location')->change();
            $table->string('reference')->nullable()->after('priority')->change();
            $table->text('attachments')->nullable()->after('reference')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('punch_item', function (Blueprint $table) {
          //
        });
    }
}
