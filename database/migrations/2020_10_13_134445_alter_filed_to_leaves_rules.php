<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFiledToLeavesRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaves_rules', function (Blueprint $table) {
            $table->string('leave_rules_descriptionn');
            $table->enum('weekend_bet_leave',['0','1']);
            $table->enum('hollyday_bet_leaves',['0','1']);
            $table->string('allow_probation');
            $table->enum('leave_carrey_forward',['0','1']);
            $table->enum('leave_backdated_allow',['0','1']);
            $table->string('backdated_allow_upto');
            $table->string('backdated_allow_till');
            $table->string('accural');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaves_rules', function (Blueprint $table) {
            //
        });
    }
}
