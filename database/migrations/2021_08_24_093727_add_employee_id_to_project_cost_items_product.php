<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeIdToProjectCostItemsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->string('employee_id')->default(0)->after('assign_to');
            $table->string('contractor_employee_id')->default(0)->after('contractor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->dropColumn(['employee_id']);
            $table->dropColumn(['contractor_employee_id']);
        });
    }
}
