<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('added_by');
            $table->integer('company_id');
            $table->integer('project_id')->default(0);
            $table->integer('emp_id')->default(0);
            $table->integer('contractor')->default(0);
            $table->integer('category')->default(0);
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->string('salary')->nullable();
            $table->string('salarytype')->nullable();
            $table->string('workinghours')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
