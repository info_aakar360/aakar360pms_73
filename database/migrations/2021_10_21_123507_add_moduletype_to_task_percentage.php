<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModuletypeToTaskPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_percentage', function (Blueprint $table) {
            $table->string('moduletype')->nullable()->after('comment');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_percentage', function (Blueprint $table) {
            $table->dropIfExists('moduletype');
        });
    }
}
