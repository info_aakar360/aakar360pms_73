<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIdInManpowerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manpower_logs', function (Blueprint $table) {
            $table->text('unique_id')->nullable()->after('id');
            $table->integer('inc')->default(0)->after('description');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manpower_logs', function (Blueprint $table) {
            $table->dropColumn('unique_id');
        });
    }
}
