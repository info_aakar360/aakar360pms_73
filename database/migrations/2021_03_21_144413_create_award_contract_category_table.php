<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardContractCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awarded_contract_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('awarded_contract_id')->unsigned();
            $table->text('category')->nullable();
            $table->text('level')->nullable();
            $table->text('parent')->nullable();
            $table->text('inc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awarded_contract_category');
    }
}
