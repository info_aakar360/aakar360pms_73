<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberToTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenders', function (Blueprint $table) {
            $table->string('title_id')->nullable();
            $table->string('number')->nullable();
            $table->string('status')->nullable();
            $table->string('deadline')->nullable();
            $table->string('distribution')->nullable();
            $table->string('accept_submition')->nullable();
            $table->string('enable_blind')->nullable();
            $table->string('include_bid_doc')->nullable();
            $table->string('send_count_emails')->nullable();
            $table->string('count_email')->nullable();
            $table->string('anticipated_date')->nullable();
            $table->longText('bidding_information')->nullable();
            $table->longText('project_information')->nullable();
            $table->string('enable_prebid_rfi')->nullable();
            $table->string('prebid_rfi_date')->nullable();
            $table->string('enable_prebid_walkthrough')->nullable();
            $table->string('prebid_walkthrough_date')->nullable();
            $table->string('walkthough_information')->nullable();
            $table->string('added_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenders', function (Blueprint $table) {
            $table->dropIfExists('title_id');
            $table->dropIfExists('number');
            $table->dropIfExists('status');
            $table->dropIfExists('accept_submition');
            $table->dropIfExists('deadline');
            $table->dropIfExists('distribution');
            $table->dropIfExists('enable_blind');
            $table->dropIfExists('include_bid_doc');
            $table->dropIfExists('send_count_emails');
            $table->dropIfExists('count_email');
            $table->dropIfExists('anticipated_date');
            $table->dropIfExists('bidding_information');
            $table->dropIfExists('project_information');
            $table->dropIfExists('enable_prebid_rfi');
            $table->dropIfExists('prebid_rfi_date');
            $table->dropIfExists('enable_prebid_walkthrough');
            $table->dropIfExists('prebid_walkthrough_date');
            $table->dropIfExists('walkthough_information');
            $table->dropIfExists('added_by');
        });
    }
}
