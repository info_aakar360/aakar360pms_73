<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_issue', function (Blueprint $table) {
            $table->integer('sub_project_id')->nullable();
            $table->integer('segment_id')->nullable();
            $table->integer('task_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_issue', function (Blueprint $table) {
            $table->dropIfExists('sub_project_id');
            $table->dropIfExists('segment_id');
            $table->dropIfExists('task_id');
        });
    }
}
