<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSegmentsPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_segments_position', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_id')->default(0);
            $table->string('title')->default(0);
            $table->string('position')->default('')->comment('row,col');
            $table->string('itemid')->default(0);
            $table->string('itemname')->default('');
            $table->string('itemslug')->default('');
            $table->tinyInteger('collock')->default(0);
            $table->integer('level')->default(0);
            $table->integer('parent')->default(0);
            $table->integer('inc')->default(0);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_segments_position');

    }
}
