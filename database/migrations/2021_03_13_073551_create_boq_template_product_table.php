<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoqTemplateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boq_template_product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default(0);
            $table->string('company_id')->default(0);
            $table->string('template_id')->default(0);
            $table->string('cost_items_id')->default(0);
            $table->string('category')->default(0);
            $table->string('description')->nullable();
            $table->string('unit')->nullable();
            $table->string('qty')->nullable();
            $table->string('rate')->nullable();
            $table->string('finalrate')->nullable();
            $table->string('finalamount')->nullable();
            $table->string('worktype')->nullable();
            $table->string('marktype')->nullable();
            $table->string('markvalue')->nullable();
            $table->string('collock')->default(0);
            $table->string('inc')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boq_template_product');

    }
}
