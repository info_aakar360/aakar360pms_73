<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductIssueReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_issue_return', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('added_by');
            $table->string('unique_id')->nullable();
            $table->integer('issue_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->integer('unit_id')->nullable();
            $table->string('qty')->default(0);
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_issue_return');
    }
}
