<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnWorkRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_request', function (Blueprint $table) {
            $table->integer('asset_id')->nullable()->after('status_id');
            $table->integer('subasset_id')->nullable()->after('asset_id');
            $table->dateTime('start_time')->nullable()->after('subasset_id');
            $table->dateTime('end_time')->nullable()->after('start_time');
            $table->string('recurring_time')->nullable()->after('end_time');
            $table->integer('recurring_status')->nullable()->after('recurring_time');
            $table->integer('recurring_mode')->nullable()->after('recurring_status');
            $table->integer('location_id')->nullable()->after('recurring_mode');
            $table->integer('sub_location_id')->nullable()->after('location_id');

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_request', function (Blueprint $table) {
            $table->dropColumn(['asset_id', 'subasset_id', 'start_time', 'end_time', 'recurring_time', 'recurring_status', 'recurring_mode', 'location_id', 'sub_location_id']);
        });
    }
}
