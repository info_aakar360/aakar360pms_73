<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManpowerAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manpower_attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('added_by')->unsigned();
            $table->string('manpower_id');
            $table->text('manpower_category')->nullable();
            $table->text('manpower')->nullable();
            $table->text('salaryperday')->nullable();
            $table->text('hourspershift')->nullable();
            $table->text('workinghours')->nullable();
            $table->text('totalprice')->nullable();
            $table->text('clockin')->nullable();
            $table->text('clockout')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manpower_attendance');
    }
}
