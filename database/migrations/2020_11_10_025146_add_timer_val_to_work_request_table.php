<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimerValToWorkRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_request', function (Blueprint $table) {
            $table->string('timer_val')->nullable()->after('project_id');
            $table->string('timer_status')->nullable()->after('timer_val');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_request', function (Blueprint $table) {
            //
        });
    }
}
