<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_sheet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('resource_id');
            $table->bigInteger('combo_resource_id');
            $table->decimal('rate', 10, 2);
            $table->string('unit')->nullable();
            $table->text('formula')->nullable();
            $table->text('remark')->nullable();
            $table->decimal('final_rate', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo_sheet');
    }
}
