<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_id');
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->date('start_date')->nullable();
            $table->date('due_date')->nullable();
            $table->integer('assign_to')->nullable();
             $table->integer('project_id')->nullable()->default(0);
            $table->integer('subproject_id')->nullable()->default(0);
             $table->integer('segment_id')->nullable()->default(0);
             $table->integer('task_category_id')->unsigned()->default(0);
            $table->enum('priority', ['low', 'medium', 'high'])->default('medium');
            $table->integer('private')->unsigned()->default(0);
            $table->string('status')->default('notstarted')->comment('notstarted, inprogress, delayed, inproblem, completed');
            $table->integer('recurring_todo_id')->nullable();
            $table->integer('dependent_todo_id')->nullable();
            $table->integer('added_by')->default(0);
            $table->date('completed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo');
    }
}
