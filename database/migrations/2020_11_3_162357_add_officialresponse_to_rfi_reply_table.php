<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfficialresponseToRfiReplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfi_reply', function (Blueprint $table) {
            $table->string('officialresponse')->nullable()->after('rfi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfi_reply', function (Blueprint $table) {
            $table->dropIfExists('officialresponse');
        });
    }
}
