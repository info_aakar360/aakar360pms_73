<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountPaymentsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_payments_files', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('added_by');
            $table->integer('payment_id')->default(0);
            $table->string('hashname')->nullable();
            $table->string('filename')->nullable();
            $table->text('description')->nullable();
            $table->string('google_url')->nullable();
            $table->string('size')->nullable();
            $table->string('dropbox_link')->nullable();
            $table->string('external_link')->nullable();
            $table->string('external_link_name')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_payments_files');
    }
}
