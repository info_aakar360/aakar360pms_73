<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionIdInProjectSegmentsPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_segments_position', function (Blueprint $table) {
            $table->string('productposition_id')->nullable()->after('segment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_segments_position', function (Blueprint $table) {
            $table->dropColumn('productposition_id');
        });
    }
}
