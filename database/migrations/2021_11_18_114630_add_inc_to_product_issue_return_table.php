<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIncToProductIssueReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_issue_return', function (Blueprint $table) {
            $table->integer('inc')->default(0)->after('remark');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_issue_return', function (Blueprint $table) {
            $table->dropColumn('inc');
        });
    }
}
