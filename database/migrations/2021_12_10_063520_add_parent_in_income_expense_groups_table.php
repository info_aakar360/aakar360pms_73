<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentInIncomeExpenseGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('income_expense_groups', function (Blueprint $table) {
            $table->integer('parent')->default(0)->after('name');
            $table->integer('default')->default(0)->after('parent');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_expense_groups', function (Blueprint $table) {
            $table->dropColumn('parent');
            $table->dropColumn('default');
        });
    }
}
