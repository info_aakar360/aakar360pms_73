<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeStartdateTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function(Blueprint $table){
            $table->string('start_date')->nullable()->change();
            $table->string('due_date')->nullable()->change();
        });
        Schema::table('project_cost_items_product', function(Blueprint $table){
            $table->string('start_date')->nullable()->change();
            $table->string('deadline')->nullable()->change();
        });
        Schema::table('project_segments_product', function(Blueprint $table){
            $table->string('start_date')->nullable()->change();
            $table->string('deadline')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
