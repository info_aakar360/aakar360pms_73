<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SlterStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->string('website')->nullable()->default(null)->change();
            $table->string('contact_person')->nullable()->default(null)->change();
            $table->string('email')->nullable()->default(null)->change();
            $table->string('phone')->nullable()->default(null)->change();
            $table->string('gst_no')->nullable()->default(null)->change();
            $table->text('note')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
