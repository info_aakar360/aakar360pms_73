<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('meeting_id')->unsigned();
            $table->string('title');
            $table->text('meeting_overview')->nullable();
            $table->string('meeting_date');
            $table->string('start_time');
            $table->string('finish_time');
            $table->string('meeting_location')->nullable();
            $table->integer('is_draft')->default(0)->nullable();
            $table->integer('is_private')->default(0)->nullable();
            $table->string('assigned_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_details');
    }
}
