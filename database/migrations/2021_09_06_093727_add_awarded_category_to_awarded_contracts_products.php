<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAwardedCategoryToAwardedContractsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('awarded_contracts_products', function (Blueprint $table) {
            $table->string('awarded_category')->default(0)->after('category');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('awarded_contracts_products', function (Blueprint $table) {
            $table->dropColumn(['awarded_category']);
        });
    }
}
