<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTenderProductToTenderBiddingProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tender_bidding_product', function (Blueprint $table) {
            $table->string('tender_product')->default(0)->after('bidding_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenders_product', function (Blueprint $table) {
            $table->dropColumn(['tender_product']);
        });
    }
}
