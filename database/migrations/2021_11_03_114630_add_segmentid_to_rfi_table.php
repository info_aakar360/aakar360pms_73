<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSegmentidToRfiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->integer('segmentid')->default(0)->after('titleid');
            $table->integer('activityid')->default(0)->after('segmentid');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->dropColumn('segmentid');
            $table->dropColumn('activityid');
        });
    }
}
