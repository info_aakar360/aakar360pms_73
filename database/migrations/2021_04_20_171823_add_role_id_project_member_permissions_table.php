<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleIdProjectMemberPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_member_permissions', function (Blueprint $table) {
            $table->integer('role_id')->after('user_id')->default('0');
            $table->integer('company_id')->after('id')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_member_permissions', function (Blueprint $table) {
            $table->dropColumn('role_id');
            $table->dropColumn('company_id');
        });
    }
}
