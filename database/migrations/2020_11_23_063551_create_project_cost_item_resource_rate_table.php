<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCostItemResourceRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_cost_item_resource_rate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_cost_item')->default(0);
            $table->string('resource_id')->default(0);
            $table->string('name')->nullable();
            $table->string('unit')->nullable();
            $table->string('rate')->nullable();
            $table->string('formula')->nullable();
            $table->string('final_rate')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_cost_item_resource_rate');

    }
}
