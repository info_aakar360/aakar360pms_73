<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManpowerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manpower_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('title_id')->nullable();
            $table->integer('costitem_id')->nullable();
            $table->integer('task_id')->nullable();
            $table->integer('added_by')->nullable();
            $table->string('contractor')->nullable();
            $table->string('manpower')->nullable();
            $table->string('workinghours')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manpower_logs');
    }
}
