<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWorkerIdToManpowerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manpower_logs', function (Blueprint $table) {
            $table->integer('emp_id')->default(0)->nullable()->after('contractor');
            $table->integer('worker_id')->default(0)->nullable()->after('emp_id');
            $table->string('salarytype')->nullable()->after('salaryperday');
            $table->string('overtimehours')->nullable()->after('workingminutes');
            $table->string('overtimeamount')->nullable()->after('overtimehours');
            $table->string('overtimetotal')->nullable()->after('overtimeamount');
            $table->string('latefineamount')->nullable()->after('overtimetotal');
            $table->string('latefinehours')->nullable()->after('latefineamount');
            $table->string('latefinetotal')->nullable()->after('latefinehours');
            $table->text('allowances')->nullable()->after('latefinetotal');
            $table->text('deductions')->nullable()->after('allowances');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manpower_logs', function (Blueprint $table) {

        });
    }
}
