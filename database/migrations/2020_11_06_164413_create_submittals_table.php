<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmittalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submittals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_id')->nullable();
            $table->string('added_by')->nullable();
            $table->string('title')->nullable();
            $table->string('number')->nullable();
            $table->string('revision')->nullable();
            $table->string('submittal_manager')->nullable();
            $table->string('ballincourt')->nullable();
            $table->string('draft')->nullable();
            $table->string('status')->nullable();
            $table->string('rec_contractor')->nullable();
            $table->string('received_from')->nullable();
            $table->string('distribution')->nullable();
            $table->string('location')->nullable();
            $table->longText('description')->nullable();
            $table->string('private')->nullable();
            $table->string('projectid')->nullable();
            $table->string('titleid')->nullable();
            $table->string('costitemid')->nullable();
            $table->string('type')->nullable();
            $table->string('leadtime')->nullable();
            $table->string('designreviewtime')->nullable();
            $table->string('internalreviewtime')->nullable();
            $table->string('scheduletask')->nullable();
            $table->string('submitdate')->nullable();
            $table->string('issuedate')->nullable();
            $table->string('receiveddate')->nullable();
            $table->string('finalduedate')->nullable();
            $table->string('onsitedate')->nullable();
            $table->string('planreturndate')->nullable();
            $table->string('planinternaldate')->nullable();
            $table->string('plansubmitdate')->nullable();
            $table->string('anticipateddeliverydate')->nullable();
            $table->string('confirmdeliverydate')->nullable();
            $table->string('actualdeliverydate')->nullable();
            $table->string('distributed_to')->nullable();
            $table->string('distributed_on')->nullable();
            $table->string('revisesubmittals_id')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submittals');
    }
}
