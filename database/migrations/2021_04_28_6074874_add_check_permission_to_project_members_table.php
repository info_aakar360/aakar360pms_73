<?php

use App\ClientDetails;
use App\CreditNotes;
use App\EmployeeDetails;
use App\Estimate;
use App\Invoice;
use App\Lead;
use App\Notice;
use App\Project;
use App\Proposal;
use App\Company;
use App\Task;
use App\Ticket;
use App\UniversalSearch;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckPermissionToProjectMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_members', function (Blueprint $table) {
            $table->smallInteger('check_permission')->default(0)->after('share_project');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_members', function (Blueprint $table) {
            $table->dropColumn(['check_permission']);
        });
    }
}
