<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('voucher_no');
            $table->bigInteger('branch_id')->nullable();
            $table->string('cheque_number')->nullable();
            $table->bigInteger('income_expense_head_id')->nullable();
            $table->bigInteger('bank_cash_id')->nullable();
            $table->string('voucher_type');
            $table->date('date');
            $table->bigInteger('dr')->nullable();
            $table->bigInteger('cr')->nullable();
            $table->string('particulars')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_transactions');
    }
}
