<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpIdInIncomeExpenseHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('income_expense_heads', function (Blueprint $table) {
            $table->integer('default')->default(0)->after('name');
            $table->integer('emp_id')->default(0)->after('default');
            $table->string('amount')->nullable()->default('0')->after('emp_id');
            $table->string('voucherdate')->nullable()->after('amount');
            $table->string('participation')->nullable()->after('voucherdate');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_expense_heads', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
