<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTodayqtyToTaskPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_percentage', function (Blueprint $table) {
            $table->float('todayqty')->nullable()->after('status');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_percentage', function (Blueprint $table) {
            $table->dropIfExists('todayqty');
        });
    }
}
