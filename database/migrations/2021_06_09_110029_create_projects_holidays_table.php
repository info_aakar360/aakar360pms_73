<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsHolidaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects_holidays', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('company_id')->nullable();
			$table->string('project_id')->nullable();
			$table->date('date')->unique();
			$table->string('occassion',100)->nullable()->default(null);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects_holidays');
	}

}
