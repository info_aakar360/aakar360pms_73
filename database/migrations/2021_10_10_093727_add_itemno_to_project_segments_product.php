<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemnoToProjectSegmentsProduct  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_segments_product', function (Blueprint $table) {
            $table->string('itemno')->nullable()->after('category');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_segments_product', function (Blueprint $table) {
            $table->dropColumn(['itemno']);
        });
    }
}
