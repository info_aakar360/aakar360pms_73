<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledsToPfandEsiSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pfand_esi_settings', function (Blueprint $table) {
            $table->string('pf_applicable')->nullable();
            $table->string('pf_number')->nullable();
            $table->dateTime('pf_joining_date')->nullable();
            $table->string('uan')->nullable();
            $table->string('eps_number')->nullable();
            $table->string('vfo')->nullable();
            $table->string('esi_applicable')->nullable();
            $table->string('esi_number')->nullable();
            $table->string('esi_employee')->nullable();
            $table->string('esi_employer')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pfand_esi_settings', function (Blueprint $table) {
            //
        });
    }
}
