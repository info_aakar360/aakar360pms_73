<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservationsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('observation_id');
            $table->string('question_id');
            $table->string('filename');
            $table->text('description')->nullable();
            $table->text('google_url')->nullable();
            $table->text('hashname')->nullable();
            $table->text('size')->nullable();
            $table->text('dropbox_link')->nullable();
            $table->text('external_link')->nullable();
            $table->text('external_link_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations_files');
    }
}
