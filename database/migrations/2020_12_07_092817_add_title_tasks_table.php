<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->integer('title')->unsigned()->nullable()->after('project_id');
            $table->integer('boqinclude')->after('cost_item_id')->nullable();
             $table->integer('private')->after('column_priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn(['title']);
            $table->dropColumn(['boqinclude']);
            $table->dropColumn(['private']);
        });
    }
}
