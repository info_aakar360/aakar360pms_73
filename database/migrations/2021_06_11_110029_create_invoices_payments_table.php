<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices_payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('company_id')->nullable();
			$table->string('added_by')->nullable();
			$table->string('invoice_id')->nullable();
			$table->string('payment_date')->nullable();
			$table->string('note')->nullable();
			$table->string('totalamount')->nullable();
			$table->string('percenttype')->nullable();
			$table->string('percentvalue')->nullable();
			$table->string('status')->default('unpaid')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices_payments');
	}

}
