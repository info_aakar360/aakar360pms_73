<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_id')->nullable();
            $table->string('added_by')->nullable();
            $table->string('project_id')->nullable();
            $table->string('subproject_id')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('contractor_id')->nullable();
            $table->string('awarded_contract_id')->nullable();
            $table->string('issue_date')->nullable();
            $table->string('due_date')->nullable();
            $table->string('status')->nullable();
            $table->text('note')->nullable();
            $table->float('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_invoices');
    }
}
