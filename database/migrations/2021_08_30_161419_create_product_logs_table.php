<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('store_id');
            $table->integer('project_id');
            $table->integer('module_id');
            $table->string('module_name');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('balance_quantity');
            $table->string('transaction_type');
            $table->integer('created_by');
            $table->text('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_logs');
    }
}
