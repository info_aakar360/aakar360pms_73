<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdduniqueUserIdToLeaveAssignedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_assigneds', function (Blueprint $table) {
            DB::statement('ALTER TABLE `leave_assigneds` MODIFY `user_id` int(11) UNIQUE NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_assigneds', function (Blueprint $table) {
            //
        });
    }
}
