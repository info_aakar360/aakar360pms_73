<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalamountToTenderBiddingProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tender_bidding_product', function (Blueprint $table) {
            $table->string('bidding_id')->nullable()->after('tender_id');
            $table->string('category')->nullable()->after('bidding_id');
            $table->string('finalprice')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tender_bidding_product', function (Blueprint $table) {
            $table->dropIfExists('bidding_id');
            $table->dropIfExists('category');
            $table->dropIfExists('finalprice');
        });
    }
}
