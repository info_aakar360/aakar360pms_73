<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWrFormDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wr_form_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('wr_id');
            $table->text('field_type')->nullable();
            $table->text('field_title')->nullable();
            $table->text('multiple')->nullable();
            $table->text('response')->nullable();
            $table->text('parent_id')->nullable();
            $table->text('child_id')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wr_form_details');
    }
}
