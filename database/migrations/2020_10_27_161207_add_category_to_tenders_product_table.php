<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToTendersProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenders_product', function (Blueprint $table) {
            $table->string('category')->nullable()->after('tender_id');
            $table->integer('cost_items')->nullable()->after('category');
            $table->string('unit')->nullable()->after('qty');
            $table->integer('inc')->nullable()->after('unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenders_product', function (Blueprint $table) {
            $table->dropIfExists('category');
            $table->dropIfExists('cost_items');
            $table->dropIfExists('unit');
            $table->dropIfExists('inc');
        });
    }
}
