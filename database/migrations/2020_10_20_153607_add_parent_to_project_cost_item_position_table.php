<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentToProjectCostItemPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_item_position', function (Blueprint $table) {
            $table->bigInteger('parent')->nullable()->after('level');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_item_position', function (Blueprint $table) {
            $table->dropIfExists('parent');
        });
    }
}
