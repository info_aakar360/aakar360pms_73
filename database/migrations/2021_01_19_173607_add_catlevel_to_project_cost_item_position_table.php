<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatlevelToProjectCostItemPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_item_position', function (Blueprint $table) {
            $table->string('catlevel')->nullable()->after('level');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_item_position', function (Blueprint $table) {
            $table->dropIfExists('catlevel');
        });
    }
}
