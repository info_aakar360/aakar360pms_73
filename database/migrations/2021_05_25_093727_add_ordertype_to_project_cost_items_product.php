<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdertypeToProjectCostItemsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->string('ordertype')->nullable()->after('qty');
            $table->string('status')->nullable()->after('finalamount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->dropColumn(['ordertype']);
            $table->dropColumn(['status']);
        });
    }
}
