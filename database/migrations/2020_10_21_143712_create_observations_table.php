<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('inspection_id');
            $table->bigInteger('question_id');
            $table->string('title')->nullable();
            $table->string('assign_to')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->string('distribution')->nullable();
            $table->text('description')->nullable();
            $table->string('location')->nullable();
            $table->string('start_date')->nullable();
            $table->string('due_date')->nullable();
            $table->string('private')->nullable();
            $table->string('priority')->nullable();
            $table->string('reference')->nullable();
            $table->text('attachments')->nullable();
            $table->string('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}
