<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesPaymentsBoqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices_payments_boq', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('invoice_id')->nullable();
			$table->string('invoice_payments_id')->nullable();
			$table->string('invoice_boq_id')->nullable();
			$table->string('percent')->nullable();
			$table->string('amount')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices_payments_boq');
	}

}
