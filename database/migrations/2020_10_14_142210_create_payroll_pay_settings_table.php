<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollPaySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_pay_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('effective_date');
            $table->string('pay_frequncy');
            $table->string('pay_structure');
            $table->string('pay_cycle');
            $table->string('input_cycle');
            $table->string('payout_date');
            $table->string('doj_date');
            $table->string('include_weekly_leave');
            $table->string('include_hollyday_leave');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_pay_settings');
    }
}
