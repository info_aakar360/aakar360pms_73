<?php

use Illuminate\Database\Seeder;

class PredefindData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyarray = \App\Company::orderBy('id','asc')->get();
        foreach ($companyarray as $company){
            $projectcategoryarray = array('House','Building','Infra','Commercial Complex','Shop','Real Estate');
            foreach ($projectcategoryarray as $projectcategory){
                $prevcat = \App\ProjectCategory::where('company_id',$company->id)->where('category_name',$projectcategory)->first();
                if(empty($prevcat)){
                    $category = new \App\ProjectCategory();
                    $category->category_name = $projectcategory;
                    $category->company_id = $company->id;
                    $category->save();
                }
            }

        $unitsarray = array('bags'=>'Bags','nos'=>'Numbers','box'=>'Box','pack'=>'Pack','dozn'=>'Dozen','m'=>'Meter',
            'rm'=>'Running meter','mm'=>'Millimeter','cm'=>'Centimeter','ft'=>'Feet','rft'=>'Running Feet','in'=>'Inch',
            'sqm'=>'Square meter','cum'=>'Cubic meter','cft'=>'Cubic feet','mt'=>'Metric Ton','kg'=>'Kilogram','gal'=>'Gallon',
            'lt'=>'Liter','qt'=>'Quintal','each'=>'Each','pcs'=>'Pieces','trp'=>'Trip'
        );
        foreach ($unitsarray as $symbol => $units){
            $prevunit = \App\Units::where('company_id',$company->id)->where('name',$units)->first();
            if(empty($prevunit)){
                $category = new \App\Units();
                $category->symbol = $symbol;
                $category->name = $units;
                $category->company_id = $company->id;
                $category->save();
            }
        }

        $productcatarray = array('Electrical','Civil','Plumbing','Mechanical','Machinery','Painting','False ceiling','HVAC',
            'Steel','Wood work','Formwork & scaffolding','Doors & windows','Safety equipment','Vehicle','Oil','Hardware',
            'Sanitary','Gardening','Stone & tiles');
        foreach ($productcatarray as  $productcat){
            $prevunit = \App\ProductCategory::where('company_id',$company->id)->where('name',$productcat)->first();
            if(empty($prevunit)){
                $category = new \App\ProductCategory();
                $category->name = $productcat;
                $category->company_id = $company->id;
                $category->save();
            }
        }
        $boqarray = array('Earthwork','Backfill','TMT Steel Work','Brick work','Centering & shuttering','Doors & Windows Frame',
            'Concrete work','Plumbing','Electrical work','Plastering','Painting','Tile & Flooring','HVAC','Sanitary fittings',
            'Ceiling','Carpentry Work','General','Finishing');
        foreach ($boqarray as  $boqa){
            $prevunit = \App\BoqCategory::where('company_id',$company->id)->where('title',$boqa)->first();
            if(empty($prevunit)){
                $category = new \App\BoqCategory();
                $category->title = $boqa;
                $category->company_id = $company->id;
                $category->save();
            }
        }
        $manpowercatarray = array('Head mason','Mason','Labour','Helper','Cooli','Reja',
            'Fitter','Bar bender','Carpenter','Plumber','Electrician','Waterman');
        foreach ($manpowercatarray as  $manpowercat){
            $prevunit = \App\ManpowerCategory::where('company_id',$company->id)->where('title',$manpowercat)->first();
            if(empty($prevunit)){
                $category = new \App\ManpowerCategory();
                $category->title = $manpowercat;
                $category->company_id = $company->id;
                $category->save();
            }
        }



        }

    }
}
