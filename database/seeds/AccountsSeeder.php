<?php

use Illuminate\Database\Seeder;

class AccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allcompaniesarray = \App\Company::orderBy('id','asc')->get();
        foreach ($allcompaniesarray as $company){

            $dataarray = array('Asset'=>'dr','Liability'=>'cr','Expense'=>'dr','Income'=>'cr');
            foreach ($dataarray as $name => $type){
                $incomearray = \App\IncomeExpenseType::where('name',$name)->where('company_id',$company->id)->first();
                if(empty($incomearray)){
                    $incomearray = new \App\IncomeExpenseType();
                    $incomearray->company_id = $company->id;
                    $incomearray->name = $name;
                    $incomearray->type = $type;
                    $incomearray->default = '1';
                    $incomearray->save();
                }
            }

            $dataarray = array('Asset'=>array('Client'),'Liability'=>array('Employee','Contractor','Supplier'),'Expense'=>array(),'Income'=>array());
            foreach ($dataarray as $parentname => $subgrouparray){
                if($parentname=='Income'){
                    $groupname =  'Other';
                }else if($parentname=='Expense'){
                    $groupname =  'Expense';
                }else{
                    $groupname = 'Current '.$parentname;
                }
                $incomegrouparray = \App\IncomeExpenseGroup::where('name',$groupname)->where('company_id',$company->id)->first();
                if(empty($incomegrouparray)){
                    $incometype = \App\IncomeExpenseType::where('name',$parentname)->where('company_id',$company->id)->first();
                    $incomeparentarray = new \App\IncomeExpenseGroup();
                    $incomeparentarray->company_id = $company->id;
                    $incomeparentarray->name = $groupname;
                    $incomeparentarray->income_type = $incometype->id;
                    $incomeparentarray->parent = '0';
                    $incomeparentarray->default = '1';
                    $incomeparentarray->save();
                    if(count($subgrouparray)>0){
                        foreach ($subgrouparray as $subgroup){
                            $incomearray = new \App\IncomeExpenseGroup();
                            $incomearray->company_id = $company->id;
                            $incomearray->name = $subgroup;
                            $incomearray->income_type = $incometype->id;
                            $incomearray->parent = $incomeparentarray->id;
                            $incomearray->default = '1';
                            $incomearray->save();
                        }
                    }
                }
            }

            $dataarray = array('Cash','Bank');
            foreach ($dataarray as $name){
                $incomearray = \App\IncomeExpenseHead::where('name',$name)->where('company_id',$company->id)->first();
                if(empty($incomearray)){
                    $incometype = \App\IncomeExpenseGroup::where('name','Current Asset')->where('company_id',$company->id)->first();
                    $incomearray = new \App\IncomeExpenseHead();
                    $incomearray->company_id = $company->id;
                    $incomearray->name = $name;
                    $incomearray->income_expense_group_id = $incometype->id;
                    $incomearray->income_expense_type_id = $incometype->income_type;
                    $incomearray->default = '1';
                    $incomearray->save();
                }
            }
            $dataarray = array('Food','Machinery','Travel','Transporting charge','Labour payment','Material','Service charge','Salary','Other');
            foreach ($dataarray as $name){
                $incomearray = \App\IncomeExpenseHead::where('name',$name)->where('company_id',$company->id)->first();
                if(empty($incomearray)){
                    $incometype = \App\IncomeExpenseGroup::where('name','Expense')->where('company_id',$company->id)->first();
                    $incomearray = new \App\IncomeExpenseHead();
                    $incomearray->company_id = $company->id;
                    $incomearray->name = $name;
                    $incomearray->income_expense_group_id = $incometype->id;
                    $incomearray->income_expense_type_id = $incometype->income_type;
                    $incomearray->default = '0';
                    $incomearray->save();
                }
            }

        }
    }
}
