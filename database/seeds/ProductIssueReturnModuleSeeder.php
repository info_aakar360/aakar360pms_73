<?php

use Illuminate\Database\Seeder;

class ProductIssueReturnModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $modulename = 'product_issue_return';
       $moduledetails = \App\Module::where('module_name',$modulename)->first();
       if(empty($moduledetails)){
           $moduledetails = new \App\Module();
           $moduledetails->module_name = $modulename;
           $moduledetails->display_name = 'Product Issue Return';
           $moduledetails->description = 'Product Issue Return';
           $moduledetails->save();

           $permissionsarray = array('add_product_issue_return'=>'Add Product Issue Return','edit_product_issue_return'=>'Edit Product Issue Return','view_product_issue_return'=>'View Product Issue Return','delete_product_issue_return'=>'Delete Product Issue Return');
           foreach ($permissionsarray as $permissions => $displayname){
               $permis = \App\Permission::where('name',$permissions)->where('module_id',$moduledetails->id)->first();
               if(empty($permis)){
                   $permis = new \App\Permission();
                   $permis->module_id = $moduledetails->id;
                   $permis->name = $permissions;
                   $permis->display_name = $displayname;
                   $permis->description = '';
                   $permis->save();
               }
           }
       }
    }
}
