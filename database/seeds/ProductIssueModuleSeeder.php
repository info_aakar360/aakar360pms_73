<?php

use Illuminate\Database\Seeder;

class ProductIssueModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $modulename = 'product_issue';
       $moduledetails = \App\Module::where('module_name',$modulename)->first();
       if(empty($moduledetails)){
           $moduledetails = new \App\Module();
           $moduledetails->module_name = $modulename;
           $moduledetails->display_name = 'Product Issue';
           $moduledetails->description = 'Product Issue';
           $moduledetails->save();

           $permissionsarray = array('add_product_issue'=>'Add Product Issue','edit_product_issue'=>'Edit Product Issue','view_product_issue'=>'View Product Issue','delete_product_issue'=>'Delete Product Issue');
           foreach ($permissionsarray as $permissions => $displayname){
               $permis = \App\Permission::where('name',$permissions)->where('module_id',$moduledetails->id)->first();
               if(empty($permis)){
                   $permis = new \App\Permission();
                   $permis->module_id = $moduledetails->id;
                   $permis->name = $permissions;
                   $permis->display_name = $displayname;
                   $permis->description = '';
                   $permis->save();
               }
           }
       }
    }
}
