<?php

use Illuminate\Database\Seeder;
use App\ModuleSetting;

class ModuleSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

       /* $ledgermodule = \App\Module::where('module_name','ledger')->first();
        if(empty($ledgermodule)){
                $ledgermodule = new \App\Module();
            $ledgermodule->module_name = 'ledger';
        }
        $ledgermodule->display_name = "Ledger";
        $ledgermodule->description = "Accounts Ledger";
        $ledgermodule->save();

        $ledgerpermissions= array('add_ledger'=>'Add Ledger','edit_ledger'=>'Edit Ledger','view_ledger'=>'View Ledger','delete_ledger'=>'Delete Ledger',);
        foreach ($ledgerpermissions as $key=>$name) {
              $permission = \App\Permission::where('module_id',$ledgermodule->id)->where('name',$key)->first();
                if(empty($permission)){
                    $permission = new \App\Permission();
                    $permission->module_id = $ledgermodule->id;
                }
                $permission->name = $key;
                $permission->display_name = $name;
                $permission->description = '';
                $permission->save();
        }*/
        $ledgermodule = \App\Module::where('module_name','material_in_out')->first();
        if(empty($ledgermodule)){
            $ledgermodule = new \App\Module();
            $ledgermodule->module_name = 'material_in_out';
        }
        $ledgermodule->display_name = "Material In & Out";
        $ledgermodule->description = "Material In & Out";
        $ledgermodule->save();

        $ledgerpermissions= array('add_mat_inout'=>'Add Material In & Out','edit_mat_inout'=>'Edit Material In & Out','view_mat_inout'=>'View Material In & Out','delete_mat_inout'=>'Delete Material In & Out');
        foreach ($ledgerpermissions as $key=>$name) {
            $permission = \App\Permission::where('module_id',$ledgermodule->id)->where('name',$key)->first();
            if(empty($permission)){
                $permission = new \App\Permission();
                $permission->module_id = $ledgermodule->id;
            }
            $permission->name = $key;
            $permission->display_name = $name;
            $permission->description = '';
            $permission->save();
        }

    }

}
