<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class Quotes extends Model
{
    protected $table = 'quotes';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
        $company = company();
        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('quotes.company_id', '=', $company->id);
            }
        });
    }
}
