<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PiProducts extends Model
{
    protected $table = 'pi_products';

    protected static function boot()
    {
        parent::boot();
    }
}
