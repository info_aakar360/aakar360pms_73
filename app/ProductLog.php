<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;

class ProductLog extends Model
{
    protected $table = 'product_logs';
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('product_logs.company_id', '=', $company->id);
            }
        });
    }
}
