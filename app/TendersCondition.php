<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TendersCondition extends Model
{
    protected $table = 'tenders_conditions';

    protected static function boot()
    {
        parent::boot();
    }
}
