<?php

namespace App;

use App\Observers\ManpowerLogObserver;
use App\Observers\ProjectTimeLogObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ManpowerLogReply extends Model
{
    protected $table = 'manpower_reply';

    protected $appends = ['alignment','userdetails','images','datetime'];
    protected static function boot()
    {
        parent::boot();

    }
    //define accessor
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d-M-Y, h:i A');
    }
    //define accessor
    public function getUserdetailsAttribute()
    {
        $allign = array();
        if(!empty($this->added_by)){
            $userid = $this->added_by;
            $allign['name'] = get_user_name($userid);
            $allign['image'] = get_users_image_link($userid);
        }
        return $allign;
    }
    //define accessor
    public function getAlignmentAttribute()
    {
        $allign = 'right';
        if(!empty($this->added_by)){
            $userid = $this->added_by;
            $allign = 'left';
        }
        return $allign;
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $punchitemid = $this->manpower_id;
        $companyid = $this->company_id;
        $punchitemarray = ManpowerLogFiles::where('manpower_id',$punchitemid)->where('reply_id',$id)->get();
        $filename = '';
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'manpower-log-files/'.$punchitemid.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/manpower-log-files/'.$punchitemid.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
