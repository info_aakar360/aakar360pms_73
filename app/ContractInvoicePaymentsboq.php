<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractInvoicePaymentsboq extends Model
{
    protected $guarded = ['id'];
    protected $table = 'contract_invoices_payments_boq';

}
