<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Submittals extends Model
{
    protected $table = 'submittals';

    protected static function boot()
    {
        parent::boot();
    }
}
