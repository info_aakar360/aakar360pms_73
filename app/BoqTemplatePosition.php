<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BoqTemplatePosition extends Model
{
    protected $table = 'boq_template_position';

    protected static function boot()
    {
        parent::boot();

    }
}
