<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
class IncomeExpenseHeadFiles extends Model
{
    protected $table = 'income_expense_heads_files';

}
