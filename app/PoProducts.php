<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Stripe\Invoice as StripeInvoice;
use Laravel\Cashier\Invoice;

class PoProducts extends Model
{
    protected $table = 'po_products';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
    }
}
