<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InspectionQuestion extends Model
{
    protected $table = 'inspection_question';

    protected static function boot()
    {
        parent::boot();
    }
}
