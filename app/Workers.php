<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Workers extends Model
{

    protected $table = 'workers';
    protected $appends = ['category_name','project_name','datetime'];
   /* protected static function boot()
    {
        parent::boot();

        static::observe(ManpowerLogObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('manpower_logs.company_id', '=', $company->id);
            }
        });
    }*/
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y');
    }
    //define accessor
    public function getProjectNameAttribute()
    {
        return get_project_name($this->project_id);
    }
    //define accessor
    public function getCategoryNameAttribute()
    {
        return get_manpower_category($this->category);
    }
}
