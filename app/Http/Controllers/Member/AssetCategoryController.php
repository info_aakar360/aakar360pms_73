<?php

namespace App\Http\Controllers\Member;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\SubAsset;


use App\Location;
use Illuminate\Http\Request;

class AssetCategoryController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Asset Category';
        $this->pageIcon = 'icon-user';
    }


    public function index()
    {
        $this->categories = AssetCategory::all();
        return view('member.asset-category.index', $this->data);
    }


    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('member.boq-category.create', $this->data);
    }


    public function createCat()
    {
        $this->categories = BoqCategory::all();
        return view('member.boq-category.index', $this->data);
    }


    public function createassetCat(Request $request)
    {
        $category = new AssetCategory();
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }



    public function createAsset(Request $request)
    {
        $category = new Asset();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->area = $request->area;
        $category->category = $request->category;
        $category->save();

        return Reply::success(__('messages.AssetAdded'));
    }



    public function createSubAsset(Request $request)
    {
        $category = new SubAsset();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->asset_id = $request->asset_id;

        $category->save();

        return Reply::success(__('messages.SubAssetAdded'));
    }

    public function storeCat(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }


    public function assets()
    {
        $this->pageTitle = 'Assets';
        $this->assets = Asset::all();
        $this->assetCategory=AssetCategory::all();
        $this->location=Location::all();
        return view('member.asset-category.asset', $this->data);
    }

    public function subAsset()
    {$this->pageTitle = 'Sub Assets';
        $this->subasset= SubAsset::all();
        $this->asset=Asset::all();

        return view('member.asset-category.sub-asset', $this->data);
    }

    public function editassetCat($id)
    {
        $this->categories = AssetCategory::all();

        $this->category = AssetCategory::where('id',$id)->first();
        $this->status='success';
        return view('admin.asset-category.edit', $this->data);
    }

    public function editAsset($id)
    {
        $this->categories = Asset::all();
        $this->assetcategories = AssetCategory::all();
        $this->categoryss = Asset::where('id',$id)->first();
        $this->selected_cat = AssetCategory::where('id', $this->categoryss->category)->first();
        $this->status='success';
        return view('member.asset-category.asset-edit', $this->data);
    }
    public function editSubAsset($id)
    {
        $this->allsubAsset = SubAsset::all();
        $this->allAsset = Asset::all();
        $this->selected_subAsset = SubAsset::where('id',$id)->first();
        $this->selected_asset = Asset::where('id', $this->selected_subAsset->asset_id)->first();
        $this->status='success';
        return view('member.asset-category.sub-asset-edit', $this->data);
    }

    public function updateAsset(Request $request,$id)
    {
        $category = Asset::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->area = $request->area;
        $category->category = $request->category;
        $category->status == 'success';

        $category->save();

        return Reply::success(__('messages.assetUpdate'));
    }
    public function updateSubAsset(Request $request,$id)
    {
        $category = SubAsset::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->asset_id = $request->asset_id;
        $category->status == 'success';

        $category->save();

        return Reply::success(__('messages.subAssetUpdate'));
    }


    public function updateassetCat(Request $request, $id)
    {
        $category = AssetCategory::find($id);
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('messages.categoryUpdated'));
    }



    public function deleteAsset($id)
    {
        Asset::destroy($id);
        $categoryData = Asset::all();

        return Reply::successWithData(__('messages.assetDeleted'),['data' => $categoryData]);
    }
    public function deleteSubAsset($id)
    {
        SubAsset::destroy($id);
        $categoryData = SubAsset::all();

        return Reply::successWithData(__('messages.subAssetDeleted'),['data' => $categoryData]);
    }
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function show($id)
    {

    }
}
