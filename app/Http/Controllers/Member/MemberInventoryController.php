<?php

namespace App\Http\Controllers\Member;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Rfq\StoreRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Indent;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PoProducts;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\ProductReturns;
use App\ProductTrade;
use App\Project;
use App\PurchaseInvoice;
use App\PurchaseOrder;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\QuoteProducts;
use App\Quotes;
use App\Role;
use App\Rfq;
use App\RfqProducts;
use App\Stock;
use App\Store;
use App\Supplier;
use App\TmpInvoice;
use App\TmpRfq;
use App\Transactions;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberInventoryController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.inventory';
        $this->pageIcon = 'icon-people';

        $this->middleware(function ($request, $next) {
            if (!in_array('inventory', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pos = PurchaseOrder::all();
        $this->stores = Store::all();
        $this->pos = count($this->pos);
        return view('member.inventory.index', $this->data);
    }

    public function stock()
    {
        $this->pageTitle = 'app.menu.stock';
        $this->stores = Store::all();
        $this->categories = ProductCategory::all();
        $this->lproducts = Product::all();
        return view('member.inventory.stock', $this->data);
    }

    public function purchaseHistory($bid, $store = 0)
    {
        //$company_id = Auth::user()->company_id;
        $this->pageTitle = 'app.menu.purchaseHistory';
        $this->bid = $bid;
        $this->store = $store;
        return view('member.inventory.purchase-history', $this->data);
    }

    public function purchaseInvoices(){
        $this->pageTitle = 'app.menu.pinvoices';
        $this->stores = Store::all();
        $this->suppliers = Supplier::all();
        return view('member.inventory.purchase-invoices', $this->data);
    }

    public function pReturns(){
        $this->pageTitle = 'app.menu.return';
        $this->stores = Store::all();
        $this->suppliers = Supplier::all();
        return view('member.inventory.returns', $this->data);
    }

    public function pReturn($invId){
        $this->pageTitle = 'app.menu.return';
        $this->tmpData = Stock::where('inv_id', $invId)->get();
        $this->inv = PurchaseInvoice::find($invId);
        return view('member.inventory.pro-return', $this->data);
    }

    public function makePayment($invId){
        $this->pageTitle = 'app.menu.makePayment';
        $this->inv = PurchaseInvoice::find($invId);
        $this->tmpData = Transactions::where('invoice_id', $invId)->get();
        return view('member.inventory.payment', $this->data);
    }

    public function submitPayment($invId, Request $request){
        $this->pageTitle = 'app.menu.makePayment';
        $inv = PurchaseInvoice::find($invId);
        $transact = new Transactions();
        $transact->invoice_id = $inv->id;
        $transact->amount = $request->amount;
        $transact->dated = $request->dated;
        $transact->status = $request->status;
        $transact->save();
        $inv->status = $request->status;
        $inv->save();
        return Reply::redirect(route('member.inventory.invoices'));
    }

    public function postReturn($invId, Request $request){
        $inv = PurchaseInvoice::find($invId);
        foreach ($request->retQty as $key=>$qty){
            $st = Stock::find($key);
            if($st !== null){
                $ret = new ProductReturns();
                $ret->invoice_id = $inv->id;
                $ret->cid = $st->cid;
                $ret->bid = $st->bid;
                $ret->quantity = $qty;
                $ret->unit = $st->unit;
                $ret->price = $st->price;
                $ret->store_id = $inv->store_id;
                $ret->supplier_id = $inv->supplier_id;
                $ret->save();
                $st->stock = $st->stock - $qty;
                $st->save();
            }
        }
        return Reply::redirect(route('member.inventory.returns'));
    }

    public function showAll($id)
    {
        $supIds = Quotes::where('rfq_id', $id)->pluck('supplier_id');
        $this->suppliers = Supplier::whereIn('id', $supIds)->get();
        $this->products = RfqProducts::where('rfq_id', $id)->get();
        $this->rfq = Rfq::find($id);
        return view('member.quotes.show-all', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->projects = Project::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        return view('member.rfq.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $bids = ProductCategory::where('id', $pid)->first()->brands;
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = '<option value="">Select Brand</option>';
        foreach($brands as $brand){
            $html .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        }
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRfqRequest $request)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq = new Rfq();
        $rfq->rfq_no = $rfq_no;
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('member.rfq.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('rfqTmpDataSession', $sid);
        }
        $all_data = RfqProducts::where('rfq_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpRfq();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpRfq::where('session_id', $sid)->get();
        $this->rfq = Rfq::where('id', $id)->first();
        return view('member.rfq.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRfqRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $rfq = Rfq::find($id);
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        RfqProducts::where('rfq_id', $id)->delete();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('member.rfq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        Rfq::destroy($id);
        RfqProducts::where('rfq_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.rfqDeleted'));
    }

    public function stockData (Request $request)
    {
        $pos = null;
        $posx = null;
        $brands = array();
        if($request->category_id != 0){
            $bs = ProductCategory::where('id', $request->category_id)->first();
            if($bs !== null){
                $brands = explode(',', $bs->brands);
            }

            $pos = collect(ProductBrand::whereIn('id', $brands)->select(['id as cid', 'id as bid', 'id as stock', 'table_name'])->get());
            $posx = collect(Product::where('id', $request->category_id)->select(['id as cid', 'id as bid', 'id as stock', 'table_name', 'trade_id'])->get());
        }else{
            $pos = collect(ProductBrand::select(['id as cid', 'id as bid', 'id as stock', 'table_name'])->get());
            $posx = collect(Product::select(['id as cid', 'id as bid', 'id as stock', 'table_name', 'trade_id'])->get());
        }

        $store_id = $request->store_id;
        $user = Auth::user();
        $final = $pos->merge($posx);

        $project_id = 0;

        $proid = Store::where('id',$store_id)->select('project_id')->first();
        if($proid !== null){
            $project_id = $proid;
        }

        return DataTables::of($final)
            ->addColumn('action', function ($row) use ($user, $store_id) {
                $ret = '';
                if($store_id == 0) {
                    $ret = '<a href="'.route('member.inventory.purchase-history', [$row->bid]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }else{
                    $ret = '<a href="'.route('member.inventory.purchase-history', [$row->bid, $store_id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'cid',
                function ($row) {
                    if($row->table_name == 'brand') {
                        $cat = ProductCategory::whereRaw('FIND_IN_SET(' . $row->cid . ', brands)')->first();
                        if ($cat !== null) {
                            return $cat->name;
                        }
                        return 'NA';
                    }elseif ($row->table_name == 'products'){
                        $cat = Product::where('id',$row->cid)->first();
                        if ($cat !== null) {
                            return $cat->name;
                        }
                        return 'NA';
                    }
                }
            )
            ->editColumn(
                'unit',
                function ($row) { return get_unit_name($row->unit); }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    if($row->table_name == 'brand') {
                        return get_pbrand_name($row->bid);
                    }elseif ($row->table_name == 'products'){
                        return get_trade_name($row->trade_id);
                    }
                }
            )
            ->editColumn(
                'est_qty',
                function ($row) use ($project_id, $store_id) {
                    return get_est_qty($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'est_rate',
                function ($row) use ($project_id, $store_id) {
                    return get_est_rate($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'indented_qty',
                function ($row) use ($project_id, $store_id) {
                    return get_indented_qty($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'issued_project',
                function ($row) use ($project_id) {
                    return get_project_name($project_id);
                }
            )
            ->editColumn(
                'stock',
                function ($row) use ($store_id) { return getStock($store_id, $row->bid); }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function purchaseData (Request $request)
    {
        $pos = Stock::select(['sku','cid', 'bid', 'quantity', 'unit', 'price', 'tax', 'stock', 'supplier_id', 'store_id'])->where('bid', $request->bid)->orderBy('id', 'DESC');
        if($request->store_id != 0){
            $pos->where('store_id', $request->store_id);
        }
        $user = Auth::user();
        return DataTables::of($pos)
            /*->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="' . route('member.inventory.purchase-history', [$row->bid]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';

                return $ret;
            })*/
            ->editColumn(
                'cid',
                function ($row) {
                    return get_pcat_name($row->cid);
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function invoiceData(Request $request)
    {
        $pos = PurchaseInvoice::query();
        if($request->store_id != 0){
            $pos = PurchaseInvoice::where('store_id', $request->store_id);
        }
        if($request->supplier_id != 0){
            $pos = PurchaseInvoice::where('supplier_id', $request->supplier_id);
        }
        $pos->orderBy('id', 'DESC');
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="' . route('member.inventory.return', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Return Product"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('member.purchase.payment', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="Make Payment"><i class="fa fa-inr" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending Payment';
                    }elseif($row->status == 1){
                        return 'Partially Paid';
                    }else{
                        return 'Fully Paid';
                    }
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function returnData(Request $request)
    {
        $pos = ProductReturns::query();
        if($request->store_id != 0){
            $pos = ProductReturns::where('store_id', $request->store_id);
        }
        if($request->supplier_id != 0){
            $pos = ProductReturns::where('supplier_id', $request->supplier_id);
        }
        $pos->orderBy('id', 'DESC');
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="' . route('member.inventory.return', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Return Product"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'invoice_id',
                function ($row) {
                    $inv = PurchaseInvoice::find($row->invoice_id);
                    return $inv->invoice_no;
                }
            )
            ->editColumn(
                'cid',
                function ($row) {
                    return get_pcat_name($row->cid);
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function export($status, $rfq)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'rfq')
            ->where('roles.company_id', company()->id)
            ->leftJoin('rfq_details', 'users.id', '=', 'rfq_details.user_id')
            ->select(
                'users.id',
                'rfq_details.name',
                'rfq_details.email',
                'rfq_details.mobile',
                'rfq_details.company_name',
                'rfq_details.address',
                'rfq_details.website',
                'rfq_details.created_at'
            )
            ->where('rfq_details.company_id', company()->id);
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }
        if ($rfq != 'all' && $rfq != '') {
            $rows = $rows->where('users.id', $rfq);
        }
        $rows = $rows->get()->makeHidden(['image']);
        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];
        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }
        // Generate and return the spreadsheet
        Excel::create('rfqs', function ($excel) use ($exportArray) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('rfqs file');
            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function rfqLink($id){
        $this->rfq = Rfq::find($id);
        return view('member.rfq.links', $this->data);
    }

    public function linkData(Request $request, $rfqId)
    {
        $suppliers = Supplier::all();
        $user = Auth::user();
        return DataTables::of($suppliers)
            ->addColumn('action', function ($row) use ($user, $rfqId){
                $ret = '<a href="javascript:;" onClick="copyLink(\'id'.$row->id.'\')" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>&nbsp;';

                return $ret;
            })
            ->addColumn('link', function($row) use ($rfqId){
                return '<span id="id'.$row->id.'">'.route('front.submitQuotation', [$rfqId, $row->id]).'</span>';
            })
            ->addColumn('status', function($row) use ($rfqId){
                $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                if($quotes === null){
                    return 'Not Submitted';
                }
                return 'Submitted';
            })
            ->editColumn(
                'updated_at',
                function ($row) use ($rfqId){
                    $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                    if($quotes === null){
                        return 'Never';
                    }
                    return Carbon::parse($quotes->updated_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['company_name', 'action', 'link'])
            ->make(true);
    }

    public function postPO(Request $request){
        $sr = 1;
        $pox = PurchaseOrder::orderBy('id', 'DESC')->first();
        if($pox !== null){
            $in = explode('/', $pox->po_number);
            $sr = $in[2];
            $sr++;
        }
        $po_no = 'PO/'.date("Y").'/'.$sr;

        $po = new PurchaseOrder();
        $po->po_number = $po_no;
        $po->quote_id = $request->quote;
        $po->rfq_id = $request->rfq;
        $po->supplier_id = $request->supplier;
        $po->remark = $request->remark;
        $po->payment_terms = $request->payment_terms;
        $po->dated = $request->dated;
        $po->save();

        foreach($request->cid as $key=>$cid){
            $popro = new PoProducts();
            $popro->po_id = $po->id;
            $popro->cid = $cid;
            $popro->bid = $request->bid[$key];
            $popro->quantity = $request->quantity[$key];
            $popro->unit = $request->unit[$key];
            $popro->price = $request->price[$key];
            $popro->amount = $request->amount[$key];
            $popro->save();
        }
        $rfq = Rfq::find($request->rfq);
        if($rfq !== null) {
            $rfq->status =  1;
            $rfq->save();
        }
        $ind = Indent::where('indent_no', $rfq->indent_no)->first();
        if($ind !== null){
            $ind->status = 2;
            $ind->save();
        }
        $quote = Quotes::find($request->quote);
        if($quote !== null){
            $quote->status = 1;
            $quote->save();
        }
        return Reply::redirect(route('member.rfq.index'));
    }

    public function add($poId){
        $this->pageTitle = 'Add Inventory';
        $this->pageIcon = 'icon-people';
        $this->po = PurchaseOrder::find($poId);
        $this->tmpData = PoProducts::where('po_id', $poId )->get();
        $quote = Quotes::find($this->po->quote_id);
        $this->supplier = Supplier::find($this->po->supplier_id);
        $this->rfq = Rfq::find($quote->rfq_id);
        $this->quote = $quote;
        $this->projects = Project::all();
        return view('member.inventory.add-po', $this->data);
    }

    public function postAdd($poId, Request $request){
        if($request->invoice_no == '' || $request->inv_dated == '' || $request->inv_remark == ''){
            return Reply::error('Mandatory field(s) cannot be left blank.');
        }
        $pi = new PurchaseInvoice();
        $pi->invoice_no = $request->invoice_no;
        $pi->po_id = $request->po;
        $pi->supplier_id = $request->supplier;
        $pi->store_id = $request->store_id;
        $pi->dated = $request->inv_dated;
        $pi->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $pi->remark = $request->inv_remark;
        $pi->payment_terms = $request->payment_terms;
        $gt = 0;
        foreach($request->cid as $key=>$cid){
            $price = $request->price[$key] + ($request->price[$key]*$request->tax[$key]/100);
            $t = (is_numeric($price) && is_numeric($request->rec_qty[$key])) ? $price*$request->rec_qty[$key] : 0;
            $gt += $t;
        }
        $pi->gt = $gt;
        $pi->save();

        $sku = time();
        foreach($request->cid as $key=>$cid) {
            $stock = new Stock();
            $stock->sku = $sku;
            $stock->po = $request->po;
            $stock->invoice_no = $request->invoice_no;
            $stock->inv_id = $pi->id;
            $stock->cid = $cid;
            $stock->bid = $request->bid[$key];
            $stock->quantity = $request->rec_qty[$key];
            $stock->stock = $request->rec_qty[$key];
            $stock->unit = $request->unit[$key];
            $stock->price = $request->price[$key];
            $stock->tax = $request->tax[$key];
            $stock->store_id = $request->store_id;
            $stock->supplier_id = $request->supplier;
            $stock->save();
        }
        Quotes::where('id', $request->quote)->update(['status'=>2]);
        Rfq::where('id', $request->rfq)->update(['status'=>2]);
        $rfq = Rfq::find($request->rfq);
        if($rfq !== null) {
            Indent::where('indent_no', $rfq->indent_no)->update(['status' => 3]);
        }

        return Reply::redirect(route('member.purchase-order.index'));

    }
    public function addNew(Request $request){
        $this->pageTitle = 'Add Purchase Invoice';
        $this->pageIcon = 'icon-people';
        $this->products = ProductCategory::all();
        $this->lproducts = Product::get();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->stores = Store::all();
        $this->suppliers = Supplier::all();
        $this->trades = ProductTrade::all();
        $this->projects = Project::all();
        if($request->session()->has('piTmpDataSession')) {
            $sid = $request->session()->get('piTmpDataSession');
            TmpInvoice::where('session_id', $sid)->delete();
            $request->session()->forget('piTmpDataSession');
        }
        return view('member.inventory.add-new', $this->data);
    }

    public function storeTmp(Request $request){
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('piTmpDataSession', $sid);
        }
        $store_id = $request->store_id;
        $tmpdata = new TmpInvoice();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->trade_id = $request->trade_id;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->price = floatval($request->price);
        $tmpdata->tax = floatval($request->tax);
        $total = floatval($request->price)+(floatval($request->price)*floatval($request->tax)/100);
        $tmpdata->amount = floatval($request->qty)*$total;
        $tmpdata->save();
        $allData = TmpInvoice::where('session_id', $sid)->get();
        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Trade</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'</td><td>'.get_trade_name($data->trade_id).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->price.'</td><td>'.$data->tax.'</td><td>'.$data->amount.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="9">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function deletePayment($id){
        DB::beginTransaction();
        $tran = Transactions::find($id);
        if($tran !== null){
            $inv = PurchaseInvoice::find($tran->invoice_id);
            $status = 0;
            $tran->delete();
            $trans = Transactions::where('invoice_id', $inv->id)->orderBy('id', 'DESC')->first();
            if($trans !== null){
                $status = $trans->status;
            }
            $inv->status = $status;
            $inv->save();
        }
        DB::commit();
        return Reply::success(__('messages.paymentDeleted'));
    }

    public function deleteTmp(Request $request){
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }
        TmpInvoice::where('id', $request->did)->delete();
        $allData = TmpInvoice::where('session_id', $sid)->get();
        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->price.'</td><td>'.$data->tax.'</td><td>'.$data->amount.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="9">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function postAddNew(Request $request){
        //dd($request);
        if($request->store_id == '' || $request->supplier_id == '' || $request->invoice_no == '' || $request->inv_dated == ''){
            return Reply::error('Mandatory field(s) cannot be left blank.');
        }
        $pi = new PurchaseInvoice();
        $pi->invoice_no = $request->invoice_no;
        $pi->po_id = $request->po_number;
        $pi->supplier_id = $request->supplier_id;
        $pi->store_id = $request->store_id;
        $pi->dated = $request->inv_dated;
        $pi->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $pi->remark = $request->inv_remark;
        $pi->payment_terms = $request->payment_terms;
        $gt = 0;
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }
        $tmpData = TmpInvoice::where('session_id', $sid)->get();
        foreach($tmpData as $tmp){
            $price = $tmp->price + ($tmp->price*$tmp->tax/100);
            $t = (is_numeric($price) && is_numeric($tmp->qty)) ? $price*$tmp->qty : 0;
            $gt += $t;
        }
        $pi->gt = $gt;
        $pi->save();
        $sku = time();
        foreach($tmpData as $temp) {
            $stock = new Stock();
            $stock->sku = $sku;
            $stock->po = $request->po_number;
            $stock->invoice_no = $request->invoice_no;
            $stock->inv_id = $pi->id;
            $stock->cid = $temp->cid;
            $stock->trade_id = $temp->trade_id;
            $stock->bid = $temp->bid;
            $stock->quantity = $temp->qty;
            $stock->stock = $temp->qty;
            $stock->unit = $temp->unit;
            $stock->price = $temp->price;
            $stock->tax = $temp->tax;
            $stock->store_id = $request->store_id;
            $stock->supplier_id = $request->supplier_id;
            $stock->save();
        }
        TmpInvoice::where('session_id', $sid)->delete();
        $request->session()->forget('piTmpDataSession');
        return Reply::redirect(route('member.inventory.invoices'));
    }
}
