<?php

namespace App\Http\Controllers\Member;

use App\Bom;
use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\Project;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\Store;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberStoresController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.stores';
        $this->pageIcon = 'icon-people';

        $this->middleware(function ($request, $next) {
            if (!in_array('stores', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$company_id = Auth::user()->company_id;
        $this->stores = Store::all();


        $this->totalStores = count($this->stores);

        return view('member.stores.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        $this->projects = Project::where('company_id', Auth::user()->company_id)->get();
        $this->sp = 0;
        if($leadID){
            $this->sp = $leadID;
        }
        return view('member.stores.create', $this->data);
    }

    public function createNew($leadID = null)
    {
        $this->projects = Project::where('company_id', Auth::user()->company_id)->get();
        $this->sp = 0;
        if($leadID){
            $this->sp = $leadID;
        }
        return view('member.stores.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStoreRequest $request)
    {
        //dd($request);
        $supplier = new Store();
        $supplier->company_name = $request->company_name;
        $supplier->project_id = $request->project_id;
        $supplier->website = $request->website;
        $supplier->contact_person = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->mobile;
        $supplier->address = $request->address;
        $supplier->gst_no = $request->gst_number;
        $supplier->note = $request->note;
        $company_id = Auth::user()->company_id;
        $supplier->added_by = $company_id;
        $supplier->save();

        return Reply::redirect(route('member.stores.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->supplier = Store::find($id);
        $this->projects = Project::where('company_id', Auth::user()->company_id)->get();
        return view('member.stores.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStoreRequest $request, $id)
    {
        $supplier = Store::find($id);
        $supplier->company_name = $request->company_name;
        $supplier->project_id = $request->project_id;
        $supplier->website = $request->website;
        $supplier->contact_person = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->mobile;
        $supplier->address = $request->address;
        $supplier->gst_no = $request->gst_number;
        $supplier->note = $request->note;
        $supplier->save();

        return Reply::redirect(route('member.stores.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkdata = Bom::where('store_id',$id)->first();
        if(!empty($checkdata)){
            return Reply::success(__('Please Delete Store Data First!'));
        }else{
            DB::beginTransaction();
            Store::destroy($id);
            DB::commit();
            return Reply::success(__('messages.storeDeleted'));
        }

    }

    public function data(Request $request, $id = 0)
    {

        $users = Store::where('added_by', Auth::user()->company_id);
        if($id){
            $users = $users->where('project_id', $id);
        }

        if ($request->supplier != 'all' && $request->supplier != '') {
            $users = $users->where('users.id', $request->supplier);
        }

        $users = $users->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('member.stores.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="' . route('member.stores.edit', $row->id) . '">' . ucfirst($row->contact_person) . '</a>';
                }
            )
            ->editColumn(
                'company_name',
                function ($row) {
                    return '<a href="' . route('member.projects.showStoresProducts', [$row->project_id, $row->id]) . '">' . ucfirst($row->company_name) . '</a>';
                }
            )
            ->editColumn(
                'project_id',
                function ($row) {
                    $p_name = 'NA';
                    $project = Project::find($row->project_id);
                    if($project !== null){
                        $p_name = $project->project_name;
                    }
                    return '<a target="_blank" href="' . route('member.projects.show', [$row->project_id]) . '">' . ucfirst($p_name) . '</a>';
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'project_id', 'action', 'status', 'company_name'])
            ->make(true);
    }



    public function export($status, $store)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'store')
            ->where('roles.company_id', company()->id)
            ->leftJoin('store_details', 'users.id', '=', 'store_details.user_id')
            ->select(
                'users.id',
                'store_details.name',
                'store_details.email',
                'store_details.mobile',
                'store_details.company_name',
                'store_details.address',
                'store_details.website',
                'store_details.created_at'
            )
            ->where('store_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($store != 'all' && $store != '') {
            $rows = $rows->where('users.id', $store);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('stores', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('stores file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
