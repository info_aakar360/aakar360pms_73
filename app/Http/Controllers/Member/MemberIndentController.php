<?php

namespace App\Http\Controllers\Member;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Indent\StoreIndentRequest;
use App\Http\Requests\Admin\Indent\UpdateIndentRequest;
use App\Http\Requests\Admin\Rfq\ConvertRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\Project;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Rfq;
use App\RfqProducts;
use App\Role;
use App\Indent;
use App\IndentProducts;
use App\Store;
use App\TmpIndent;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberIndentController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.indent';
        $this->pageIcon = 'icon-people';

        $this->middleware(function ($request, $next) {
            if (!in_array('indent', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$company_id = Auth::user()->company_id;
        $this->indents = Indent::all();
        $this->stores = Store::all();

        $this->totalIndents = count($this->indents);

        return view('member.indent.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->lproducts = Product::all();
        $this->projects = Project::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('member.indent.create', $this->data);
    }

    public function createNew($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        if($leadID){
            $this->stores = Store::where('project_id', $leadID)->get();
        }
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('member.indent.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $bids = ProductCategory::where('id', $pid)->first()->brands;
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = '<option value="">Select Brand</option>';
        foreach($brands as $brand){
            $html .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        }
        return $html;
    }
    public function storeTmp(Request $request){
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $tmpdata = new TmpIndent();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->dated = $request->dated;
        $tmpdata->remark = $request->remark;
        $tmpdata->save();

        $allData = TmpIndent::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function deleteTmp(Request $request){
        $sid = null;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        TmpIndent::where('id', $request->did)->delete();
        $allData = TmpIndent::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIndentRequest $request)
    {
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $sr = 1;
        $ind = Indent::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->indent_no);
            $sr = $in[2];
            $sr++;
        }
        $indent_no = 'IND/'.date("Y").'/'.$sr;
        $indent = new Indent();
        $indent->indent_no = $indent_no;
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remark;
        $indent->save();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('member.indent.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->projects = Project::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('member.indent.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIndentRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remark;
        $indent->save();
        IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('member.indent.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        Indent::destroy($id);
        IndentProducts::where('indent_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.indentDeleted'));
    }
    public function approve($id, $val)
    {
        DB::beginTransaction();
        $ind = Indent::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('Indent updated successfully');
    }

    public function data(Request $request, $id = 0)
    {
        $indents = Indent::join('stores', 'stores.id', '=', 'indents.store_id');
        if($id){
            $indents = $indents->where('stores.project_id', $id);
        }
        if ($request->store != 'all' && $request->store != '') {
            $indents = $indents->where('store_id', $request->store);
        }
        if ($request->status != 'all' && $request->status != '') {
            $indents = $indents->where('status', $request->status);
        }
        $indents = $indents->select(['indents.*'])->get();
        $user = Auth::user();
        return DataTables::of($indents)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '';
                if($user->can('edit_indent') && $row->status == 0){
                    $ret .= '<a href="' . route('member.indent.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('delete_indent') && $row->status == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('approve_indent') && $row->status == 0 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('approve_indent') && $row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }

                if($user->can('add_rfq') && $row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="' . route('member.indent.gconvert', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To RFQ"><i class="fa fa-send" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending RFQ';
                    }else if($row->status == 1){
                        return 'Pending PO';
                    }else if($row->status == 2){
                        return 'Pending Purchase';
                    }else{
                        return 'Purchase Done';
                    }
                }
            )
            ->editColumn(
                'approve',
                function ($row) {
                    if($row->approve == 0){
                        return 'Pending Approval';
                    }else if($row->approve == 1){
                        return 'Approved';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function convert($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('member.indent.convert', $this->data);
    }
    public function postConvert($id, ConvertRfqRequest $request){
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $rfq = new Rfq();
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq->rfq_no = $rfq_no;
        $rfq->indent_no = $indent->indent_no;
        $rfq->store_id = $indent->store_id;
        $rfq->remark = $indent->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        //IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        $indent->status = 1;
        $indent->save();
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('member.rfq.index'));
    }

    public function export($status, $indent)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'indent')
            ->where('roles.company_id', company()->id)
            ->leftJoin('indent_details', 'users.id', '=', 'indent_details.user_id')
            ->select(
                'users.id',
                'indent_details.name',
                'indent_details.email',
                'indent_details.mobile',
                'indent_details.company_name',
                'indent_details.address',
                'indent_details.website',
                'indent_details.created_at'
            )
            ->where('indent_details.company_id', company()->id);
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }
        if ($indent != 'all' && $indent != '') {
            $rows = $rows->where('users.id', $indent);
        }
        $rows = $rows->get()->makeHidden(['image']);
        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];
        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }
        // Generate and return the spreadsheet
        Excel::create('indents', function ($excel) use ($exportArray) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('indents file');
            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
