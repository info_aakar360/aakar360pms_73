<?php

namespace App\Http\Controllers\Member;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\BoqCategory;
use App\ProductCategory;
use App\ProductType;
use Illuminate\Http\Request;

class ManageProductTypeController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Type';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function workforceIndex()
    {
        $this->categories = ProductType::where('type','=','Workforce')->get();
        return view('member.product-type.workforce.index', $this->data);
    }

    public function equipmentIndex()
    {
        $this->categories = ProductType::where('type','=','Equipment')->get();
        return view('member.product-type.equipment.index', $this->data);
    }

    public function materialIndex()
    {
        $this->categories = ProductType::where('type','=','Material')->get();
        return view('member.product-type.material.index', $this->data);
    }

    public function commitmentIndex()
    {
        $this->categories = ProductType::where('type','=','Commitment')->get();
        return view('member.product-type.commitment.index', $this->data);
    }

    public function ownerCostIndex()
    {
        $this->categories = ProductType::where('type','=','Owner Cost')->get();
        return view('member.product-type.owner-cost.index', $this->data);
    }

    public function professionalServicesIndex()
    {
        $this->categories = ProductType::where('type','=','Professional Services')->get();
        return view('member.product-type.professional-services.index', $this->data);
    }

    public function otherIndex()
    {
        $this->categories = ProductType::where('type','=','Other')->get();
        return view('member.product-type.other.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('member.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.workforce.create', $this->data);
    }

    public function createEquipmentType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.equipment.create', $this->data);
    }

    public function createMaterialType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.material.create', $this->data);
    }

    public function createCommitmentType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.commitment.create', $this->data);
    }

    public function createOwnerCostType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.owner-cost.create', $this->data);
    }

    public function createProfessionalServicesType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.professional-services.create', $this->data);
    }

    public function createOtherType()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.other.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    public function addProduct()
    {
        $this->products = ProductCategory::all();
        return view('member.product-type.add-product', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeProduct(Request $request)
    {
        $category = new ProductCategory();
        $category->name = $request->name;
        $category->added_by = $this->user->company_id;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    public function editProduct($id)
    {
        $this->product = ProductCategory::findOrFail($id);
        return view('member.product-type.edit-product', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProduct(Request $request, $id)
    {
        $category = ProductCategory::find($id);
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeType(Request $request)
    {

        foreach ($request->product_id as $cat){
            $category = new ProductType();
            $category->company_id = $this->user->company_id;
            $category->product_id = $cat;
            $category->type = $request->type;
            $category->save();
        }

        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editType($id)
    {
        $this->products = ProductCategory::all();
        $this->types = ProductType::where('type','=','Workforce')->get();
        return view('member.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $id)
    {
        $category = BoqCategory::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductType::destroy($id);
        $categoryData = ProductType::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }
}
