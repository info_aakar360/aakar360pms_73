<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\Member\Location\SubLocationRequest;
use App\Http\Requests\Request;
use App\Helper\Reply;
use App\Http\Requests\Member\Location\StoreLocationRequest;

use App\Location;
use App\SubLocation;

use App\Cities;
use App\State;
class ManageLocationController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Location';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->location= Location::all();

        $this->state = State::where('country_id','100')->get();
        $this->sublocation = SubLocation::all();

        return view('member.location.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->Location = Location::all();
        return view('member.location.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->Location = Location::all();
        return view('member.location.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Location $request)
    {
        $category = new Location();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.locationAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreLocationRequest $request)
    {
        $location = new Location();
       // $location->name = $request->name;
        $location->location_name = $request->buildingName;
        $location->description = $request->description;
        $location->local_address = $request->localAddress;
       // $gps=array("let:Null","long:Null");
      //  $location->gps_location = $gps;
        $location->state_id = $request->state_id;
        $location->city_id = $request->city_id;
        $location->country_id = "100";
        $location->save();
       $this->Location = Location::all();
      //  return Reply::successWithData(__('messages.locationAdded'),['data' => $locationData]);
       // return view('admin.location.index', $this->data);
        return Reply::success(__('messages.LocationAdded'));
    }

    public function storesubCat(SubLocationRequest $request)
    {
        $location = new SubLocation();
        $location->name = $request->subName;
        $location->location_id = $request->locationId;
        $location->description = $request->description;
        $location->save();
        $this->Location = Location::all();
        return Reply::success(__('messages.LocationSubAdded'));
    }

    public function editsubCat($id)
    {

        $this->location = Location::all();

        $this->sublocation_selected = SubLocation::where('id',$id)->first();
        return view('member.location.sub-edit', $this->data);

    }

    public function deletesubCat($id)
    {
        SubLocation::destroy($id);
        $categoryData = SubLocation::all();
        return Reply::successWithData(__('messages.SublocDeleted'),['data' => $categoryData]);
    }

    public function editLocation(StoreLocationRequest $request, $id)
    {
        $this->location = Location::all();
        $this->state = State::where('country_id','100')->get();
       // $this->statess = State::where('id',$state_id)->get();




        $this->location_selected = Location::where('id',$id)->first();
        $this->city = Cities::where('state_id',$this->location_selected->state_id)->get();
        $this->status='success';
        //dd($this->city);
        return view('member.location.location-edit', $this->data);



    }

    public function deleteLocation($id)
    {
        Location::destroy($id);
        $categoryData = Location::all();
        return Reply::successWithData(__('messages.locationDeleted'),['data' => $categoryData]);
    }

    public function updateLocation(StoreLocationRequest $request, $id){


        $category = Location::find($id);
        $category->location_name = $request->buildingName;
        $category->description = $request->description;
        $category->local_address = $request->localAddress;
        $category->state_id = $request->state_id;   $category->description = $request->description;
        $category->city_id = $request->city_id;

        $category->save();

        return Reply::success(__('messages.Locationupdate'));
    }

    public function getCity($state_id)
    {



         $data = array();
       // $category->name = $state_id->name;
        $data['state'] = array_pluck(State::where('country_id','100')->get(), 'name', 'id');
        $data['city']= array_pluck(Cities::where('state_id',$state_id)->get(), 'name', 'id');
        $data['status']='success';


      //  return view('admin.location.index', $this->data);
        return json_encode($data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


public function updatesubCat(SubLocationRequest $request, $id){


    $category = SubLocation::find($id);
    $category->name = $request->name;
    $category->description = $request->description;
    $category->location_id = $request->location_id;
    $category->save();

    return Reply::success(__('messages.subLocupdate'));
}


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Location::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.Locationupdate'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);
        $locationData = Location::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $$locationData]);
    }
}
