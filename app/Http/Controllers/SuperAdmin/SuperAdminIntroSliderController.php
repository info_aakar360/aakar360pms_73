<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\SubAsset;


use App\Location;
use App\Type;
use App\IntroSliders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class SuperAdminIntroSliderController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Intro Slider';
        $this->pageIcon = 'icon-user';
    }

    public function index()
    {
        $user = $this->user;
        $this->sliderarray = IntroSliders::get();
        return view('super-admin.intro-slider.index', $this->data);
    }


    public function store(Request $request)
    {
        $type = new IntroSliders();
        $type->title = $request->title;
        $type->status = 1;
        $type->save();
        return Reply::dataOnly(['slideid' => $type->id]);
    }

    public function edit($id)
    {
        $this->slide = IntroSliders::find($id);
        $this->status = 'success';
        return view('super-admin.intro-slider.edit', $this->data);
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $storage = storage();
            $punchitem = IntroSliders::find($request->slideid);
            $fileData = $request->file;
                switch ($storage) {
                    case 'local':
                        $destinationPath = 'uploads/introslides/';
                        if (!file_exists('' . $destinationPath)) {
                            mkdir('' . $destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('/introslides/', $fileData, $fileData->hashName(), 'public');
                        break;
                }
                $punchitem->image = $fileData->hashName();
            $punchitem->save();
        }
        return Reply::success("File updated successfully");
    }

    public function update(Request $request, $id)
    {
        $type = IntroSliders::find($id);
        $type->title = $request->title;
        $type->save();
        return Reply::dataOnly(['slideid' => $type->id]);
    }
    public function destroy($id)
    {
        IntroSliders::destroy($id);
        return Reply::success('Slide Deleted');
    }
}
