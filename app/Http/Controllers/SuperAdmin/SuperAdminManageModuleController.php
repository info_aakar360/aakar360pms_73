<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\Module;
use App\Permission;
use App\PermissionRole;
use App\ProductCategory;
use App\Role;
use Dompdf\Exception;
use Illuminate\Http\Request;

class SuperAdminManageModuleController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Manage Modules';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->modules = Module::all();
        return view('super-admin.manage-modules.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('super-admin.manage-modules.create', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $module = new Module();
        $module->module_name = strtolower(filter_string($request->module_name));
        $module->display_name = strtolower(filter_string($request->displayname));
        $module->description = $request->description;
        $module->save();

        if($module){
            foreach ($request->name as $key=>$name) {
                if($name !== '') {
                    $permission = new Permission();
                    $permission->name = $name;
                    $permission->display_name = $request->display_name[$key];
                    $permission->description = $request->permission_description[$key];
                    $permission->module_id = $module->id;
                    $permission->save();
                }
            }
        }

        return Reply::success(__('New Module Added Successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->module = Module::find($id);
        $this->permissions = Permission::where('module_id',$id)->get();
        return view('super-admin.manage-modules.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $module = Module::find($id);
        $module->module_name = strtolower(filter_string($request->module_name));
         $module->display_name = strtolower(filter_string($request->displayname));
        $module->description = $request->description;
        $module->save();

        if($module){
            Permission::where('module_id',$id)->delete();
            foreach ($request->name as $key=>$name) {
                if($name !== '') {
                    $permission = new Permission();
                    $permission->name = $name;
                    $permission->display_name = $request->display_name[$key];
                    $permission->description = $request->permission_description[$key];
                    $permission->module_id = $id;
                    $permission->save();
                }
            }
        }
        return Reply::success(__('Module Updated Successfully'));

        }catch (Exception $exception){

            return Reply::error($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        $categoryData = Module::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function assignPermissions(){
        $this->roles = Role::where('company_id','1')->get();
        $this->modules = Module::get();
        return view('super-admin.manage-modules.assign-permission', $this->data);
    }

    public function getPermissions(Request $request){
        $id = $request->id;
        $html = '';
        $permissions = Permission::where('module_id',$id)->get();
        $html = '<table class="table ">
                <tbody>
                    <tr>';
        foreach ($permissions as $per){
            $html .= '<td>
                <div class="switchery-demo">
                    <input type="checkbox" name="permissions[]" class="js-switch assign-role-permission permission_'.$per->id.'" value="'.$per->id.'" checked data-size="small" data-color="#00c292" data-permission-id="'.$per->id.'" data-role-id="'.$id.'" id="'.$id.'" />
                    <label for="'.$id.'">'.$per->name.'</label>
                </div>
            </td>';
        }
        $html .= '</tr>
                </tbody>
            </table>';
        return $html;
    }

    public function storeAssignPermissions(Request $request){
         $role = $request->role_id;
         $permissionsarray = $request->permissions;
         if(!empty($permissionsarray)){
             foreach ($permissionsarray as $permissions){
                 $permissionrole = PermissionRole::where('permission_id',$permissions)->where('role_id',$role)->first();
                 if(empty($permissionrole->id)){
                     $permissionrole = new PermissionRole();
                     $permissionrole->permission_id = $permissions;
                     $permissionrole->role_id = $role;
                     $permissionrole->save();
                 }
             }
         }
        return 'success';
    }
}
