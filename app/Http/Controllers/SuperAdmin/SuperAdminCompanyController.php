<?php

namespace App\Http\Controllers\SuperAdmin;

use App\AccountPayments;
use App\AppProductIssue;
use App\AppProject;
use App\Attendance;
use App\AttendanceRules;
use App\AttendanceSetting;
use App\AwardContractCategory;
use App\AwardContractFiles;
use App\AwardContractProducts;
use App\AwardedContracts;
use App\Bom;
use App\BoqCategory;
use App\Boqtemplate;
use App\Company;
use App\CostItems;
use App\Currency;
use App\Employee;
use App\FileManager;
use App\GlobalCurrency;
use App\Helper\Reply;

use App\Http\Requests\SuperAdmin\Companies\DeleteRequest;
use App\Http\Requests\SuperAdmin\Companies\PackageUpdateRequest;
use App\Http\Requests\SuperAdmin\Companies\StoreRequest;
use App\Http\Requests\SuperAdmin\Companies\UpdateRequest;

use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\IncomeExpenseType;
use App\Indent;
use App\InspectionName;
use App\Leave;
use App\ManpowerLog;
use App\OfflineInvoice;
use App\OfflinePaymentMethod;
use App\Package;
use App\PayrollRegister;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\ProductIssue;
use App\ProductIssueReturn;
use App\ProductLog;
use App\ProductTrade;
use App\Project;
use App\ProjectCostItemsBaseline;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\ProjectsHoliday;
use App\ProjectsLogs;
use App\PunchItem;
use App\PurchaseInvoice;
use App\Quotes;
use App\Rfi;
use App\RfiFile;
use App\Rfq;
use App\Role;
use App\Segment;
use App\Store;
use App\StripeInvoice;
use App\Task;
use App\TenderAssign;
use App\TenderBidding;
use App\Tenders;
use App\TendersCategory;
use App\TendersContracts;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\Trade;
use App\Traits\CurrencyExchange;
use App\Units;
use App\User;
use App\WeatherLog;
use Carbon\Carbon;
use Google\Service\YouTube\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class SuperAdminCompanyController extends SuperAdminBaseController
{
    use CurrencyExchange;

    /**
     * AdminProductController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Companies';
        $this->pageIcon = 'icon-layers';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     /*   $companyusers = User::select('company_id')->where('company_id','<>','0')->groupBy('company_id')->get();
         foreach ($companyusers as $company){

            $dataarray = array('Asset'=>'dr','Liability'=>'cr','Income'=>'cr','Expense'=>'dr');
            foreach ($dataarray as $name => $type){
                $incomearray = IncomeExpenseType::where('name',$name)->where('company_id',$company->company_id)->first();
                if(empty($incomearray)){
                    $incomearray = new IncomeExpenseType();
                    $incomearray->company_id = $company->company_id;
                    $incomearray->name = $name;
                    $incomearray->type = $type;
                    $incomearray->default = '1';
                    $incomearray->save();
                }
            }

            $dataarray = array('Asset'=>array('Client'),'Liability'=>array('Employee','Supplier','Contractor'),'Expense'=>array(),'Income'=>array());
        foreach ($dataarray as $parentname => $subgrouparray){
            if($parentname=='Income'){
                $groupname =  'Other';
            }else if($parentname=='Expense'){
                $groupname =  'Expense';
            }else{
                $groupname = 'Current '.$parentname;
            }
                $incomegroup = IncomeExpenseGroup::where('name',$groupname)->where('company_id',$company->company_id)->first();
                if(empty($incomegroup)){
                    $incometype = IncomeExpenseType::where('name',$parentname)->where('company_id',$company->company_id)->first();
                    $incomegroup = new IncomeExpenseGroup();
                    $incomegroup->company_id = $company->company_id;
                    $incomegroup->name = $groupname;
                    $incomegroup->income_type = $incometype->id;
                    $incomegroup->parent = '0';
                    $incomegroup->default = '1';
                    $incomegroup->save();
                }
                if(count($subgrouparray)>0){
                    foreach ($subgrouparray as $subgroup){
                        $incomearray = IncomeExpenseGroup::where('company_id',$company->company_id)->where('name',$subgroup)->where('parent',$incomegroup->id)->where('income_type',$incomegroup->income_type)->first();
                           if(empty($incomearray)){
                               $incomearray = new IncomeExpenseGroup();
                               $incomearray->company_id = $company->company_id;
                               $incomearray->name = $subgroup;
                               $incomearray->income_type = $incomegroup->income_type;
                               $incomearray->parent = $incomegroup->id;
                               $incomearray->default = '1';
                               $incomearray->save();
                           }
                    }
                }
            }

            $dataarray = array('Cash','Bank');
            foreach ($dataarray as $name){
                $incomearray = IncomeExpenseHead::where('name',$name)->where('company_id',$company->company_id)->first();
                if(empty($incomearray)){
                    $incometype = IncomeExpenseGroup::where('name','Current Asset')->where('company_id',$company->company_id)->first();
                    $incomearray = new IncomeExpenseHead();
                    $incomearray->company_id = $company->company_id;
                    $incomearray->name = $name;
                    $incomearray->income_expense_group_id = $incometype->id;
                    $incomearray->income_expense_type_id = $incometype->income_type;
                    $incomearray->default = '1';
                    $incomearray->save();
                }
            }

        }
        }  */

        $this->totalCompanies = Company::count();
        $this->packages = Package::all();
        return view('super-admin.companies.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        $this->currencies = GlobalCurrency::all();
        return view('super-admin.companies.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        DB::beginTransaction();

        $company = new Company();

        $companyDetail = $this->storeAndUpdate($company, $request);

        $globalCurrency = GlobalCurrency::findOrFail($request->currency_id);
        $currency = Currency::where('currency_code', $globalCurrency->currency_code)
            ->where('company_id', $companyDetail->id)->first();

        if(is_null($currency)) {
            $currency = new Currency();
            $currency->currency_name = $globalCurrency->currency_name;
            $currency->currency_symbol = $globalCurrency->currency_symbol;
            $currency->currency_code = $globalCurrency->currency_code;
            $currency->is_cryptocurrency = $globalCurrency->is_cryptocurrency;
            $currency->usd_price = $globalCurrency->usd_price;
            $currency->company_id = $companyDetail->id;
            $currency->save();
        }

        $company->currency_id = $currency->id;
        $company->save();

        $adminRole = Role::where('name', 'admin')->where('company_id', $companyDetail->id)->withoutGlobalScope('active')->first();

        $user = new User();
        $user->company_id = $companyDetail->id;
        $user->name = 'Admin';
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->roles()->attach($adminRole->id);

        $employeeRole = Role::where('name', 'employee')->where('company_id', $user->company_id)->first();
        $user->roles()->attach($employeeRole->id);
        $contractorRole = Role::where('name', 'contractor')->where('company_id', $user->company_id)->first();
        $user->roles()->attach($contractorRole->id);

        DB::commit();
        return Reply::redirect(route('super-admin.companies.index'), 'Company added successfully.');
    }
//ALTER TABLE pms.companies DROP FOREIGN KEY companies_currency_id_foreign;`
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        //
    }


    /**
     * @param $companyId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Throwable
     */
    public function editPackage($companyId) {
        $packages = Package::all();
        $global = $this->global;
        $company = Company::find($companyId);
        $currentPackage = Package::find($company->package_id);
        $lastInvoice = StripeInvoice::where('company_id', $companyId)->orderBy('created_at', 'desc')->first();
        $packageInfo = [];
        foreach($packages as $package) {
            $packageInfo[$package->id] = [
                'monthly' => $package->monthly_price,
                'annual' => $package->annual_price
            ];
        }

        $offlinePaymentMethod = OfflinePaymentMethod::whereNull('company_id')->get();
        $modal = view('super-admin.companies.editPackage', compact('packages', 'company', 'currentPackage', 'lastInvoice', 'packageInfo', 'global', 'offlinePaymentMethod'))->render();

        return response(['status' => 'success', 'data' => $modal], 200);
}

    public function updatePackage(PackageUpdateRequest $request, $companyId)
    {
        $company = Company::find($companyId);

        try {
            $package = Package::find($request->package);
            $company->package_id = $package->id;
            $company->package_type = $request->packageType;
            $company->status = 'active';

            $payDate = $request->pay_date ? Carbon::parse($request->pay_date): Carbon::now();

            $company->licence_expire_on = ($company->package_type == 'monthly') ?
                $payDate->copy()->addMonth()->format('Y-m-d') :
                $payDate->copy()->addYear()->format('Y-m-d') ;

            $nextPayDate = $request->next_pay_date ? Carbon::parse($request->next_pay_date) : $company->licence_expire_on;

            if($company->isDirty('package_id') || $company->isDirty('package_type')) {
                $offlineInvoice = new OfflineInvoice();

            } else {
                $offlineInvoice = OfflineInvoice::where('company_id', $companyId)->orderBy('created_at', 'desc')->first();
                if($offlineInvoice){
                    $offlineInvoice = new OfflineInvoice();
                }
            }
            $offlineInvoice->company_id = $company->id;
            $offlineInvoice->package_id = $company->package_id;
            $offlineInvoice->package_type = $request->packageType;
            $offlineInvoice->amount = $request->amount ?: $package->{$request->packageType.'_price'};
            $offlineInvoice->pay_date = $payDate;
            $offlineInvoice->next_pay_date = $nextPayDate;
            $offlineInvoice->status = 'paid';

            $offlineInvoice->save();
            $company->save();

            return response(['status' => 'success', 'message' => 'Package Updated Successfully.'], 200);
        } catch (\Exception $e) {
            return response(['status' => 'fail', 'message' => 'Some unknown error occur. Please try again.'], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->company = Company::find($id);

        $this->timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        $this->currencies = Currency::where('company_id', $id)->get();
        $this->packages = Package::all();

        return view('super-admin.companies.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return array
     */
    public function update(UpdateRequest $request, $id)
    {
        $company = Company::find($id);
        $this->storeAndUpdate($company, $request);

        $company->currency_id = $request->currency_id;
        $company->save();

        return Reply::redirect(route('super-admin.companies.index'), 'Company updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteRequest $request
     * @param  int $id
     * @return array
     */
    public function destroy(DeleteRequest $request, $id)
    {
        $company = Company::findOrFail($id);
        if(!empty($company)){
            $project = AppProject::where('company_id',$company->id)->pluck('id')->toArray();

            Title::whereIn('project_id', $project)->delete();
            Segment::whereIn('projectid', $project)->delete();
            ProjectsHoliday::whereIn('project_id', $project)->delete();
            FileManager::whereIn('project_id',$project)->delete();
            ProjectsLogs::whereIn('module_id',$project)->where('module_id','projects')->delete();
            ProjectCostItemsBaseline::whereIn('project_id', $project)->delete();
            ProjectSegmentsPosition::whereIn('project_id', $project)->delete();
            ProjectCostItemsPosition::whereIn('project_id', $project)->delete();
            ProjectCostItemsProduct::whereIn('project_id', $project)->delete();
            ProjectSegmentsProduct::whereIn('project_id', $project)->delete();
            Store::whereIn('project_id',$project)->delete();
            Task::whereIn('project_id',$project)->delete();
            ManpowerLog::whereIn('project_id',$project)->delete();
            Indent::whereIn('project_id',$project)->delete();
            ProductIssueReturn::whereIn('project_id',$project)->delete();
            ProductIssue::whereIn('project_id',$project)->delete();
            ProductLog::whereIn('project_id',$project)->delete();
            WeatherLog::whereIn('project_id',$project)->delete();
            Quotes::whereIn('project_id',$project)->delete();
            TenderBidding::whereIn('project_id',$project)->delete();
            InspectionName::where('company_id',$company->id)->delete();
            PurchaseInvoice::where('company_id',$company->id)->delete();
            Rfq::where('company_id',$company->id)->delete();
            Employee::where('company_id',$company->id)->delete();
            Leave::where('company_id',$company->id)->delete();
            Attendance::where('company_id',$company->id)->delete();
            AttendanceSetting::where('company_id',$company->id)->delete();
            AttendanceRules::where('company_id',$company->id)->delete();
            ProjectMember::where('company_id', $company->id)->delete();
            PayrollRegister::where('company_id', $company->id)->delete();
            ProductBrand::where('company_id', $company->id)->delete();
            ProductCategory::where('company_id', $company->id)->delete();
            Product::where('company_id', $company->id)->delete();
            IncomeExpenseHead::where('company_id', $company->id)->delete();
            IncomeExpenseGroup::where('company_id', $company->id)->delete();
            IncomeExpenseType::where('company_id', $company->id)->delete();
            ProductTrade::where('company_id', $company->id)->delete();
            Trade::where('company_id', $company->id)->delete();
            PunchItem::where('company_id', $company->id)->delete();
            BoqCategory::where('company_id', $company->id)->delete();
            Task::where('company_id', $company->id)->delete();
            Boqtemplate::where('company_id', $company->id)->delete();
            Rfi::where('company_id', $company->id)->delete();
            RfiFile::where('company_id', $company->id)->delete();
            Tenders::where('company_id', $company->id)->delete();
            TendersContracts::where('company_id', $company->id)->delete();
            TendersFiles::where('company_id', $company->id)->delete();
            TenderAssign::where('company_id', $company->id)->delete();
            AwardContractProducts::where('company_id', $company->id)->delete();
            AwardedContracts::where('company_id', $company->id)->delete();
            AwardContractFiles::where('company_id', $company->id)->delete();
            Bom::where('company_id', $company->id)->delete();
            Units::where('company_id', $company->id)->delete();
            CostItems::where('company_id', $company->id)->delete();

            AppProject::where('company_id',$company->id)->delete();
        }
        return Reply::success('Company deleted successfully.');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function data(Request $request)
    {
        $packages = Company::query();

     /*   if($request->package != 'all' && $request->package != ''){
            $packages = $packages->where('package_id', $request->package);
        }

        if($request->type != 'all' && $request->type != ''){
            $packages = $packages->where('package_type', $request->type);
        }*/
        $packages = $packages->get();
        return Datatables::of($packages)
            ->addIndexColumn()
            ->addColumn('action', function($row){

                return '<a href="'.route('super-admin.companies.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                       <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('company_name', function ($row) {
                    return ucfirst($row->company_name);
            })
            ->editColumn('status', function ($row) {
                $class = ($row->status == 'active') ? 'label-custom' : 'label-danger';
                    return '<span class="label label-rouded '.$class.'">'.ucfirst($row->status).'</span>';
            })
            ->editColumn('company_email', function ($row) {
                    return '<a href="mailto:'.$row->company_email.'" target="_blank">'.$row->company_email.'</a>';
            })
            ->editColumn('company_phone', function ($row) {
                    return '<a href="tel:'.$row->company_phone.'" target="_blank">'.$row->company_phone.'</a>';
            })
            ->editColumn('logo', function ($row) {
                    return '<img src="'.$row->logo_url().'" class="img-responsive"/>';
            })
            ->editColumn('currency', function ($row) {
                    return !empty($row->currency) ? $row->currency->currency_name. ' ('.$row->currency->currency_symbol.')' : '';
            })
            ->editColumn('last_login', function ($row) {
                if($row->last_login != null) {
                    return $row->last_login->diffForHumans();
                }
                    return '-';
            })
            ->editColumn('web_login', function ($row) {
                $users = User::where('company_id', $row->id)->first();
                $checked = $webloginhtml = '';
                if (!empty($users)) {
                    if ($users->web_login == 'enable') {
                        $checked = 'checked';
                    }
                    $webloginhtml = '<label class="switch">
                                        <input type="checkbox" name="weblogin"  value="' . $users->id . '" class="weblogin" ' . $checked . ' ><div class="slider round weblogincheckbox"><span class="on"></span><span class="off"></span></div></label>';
                 }
                return $webloginhtml;
            })
            ->editColumn('package', function ($row) {
                $package = '<div class="w-100 text-center">';
                $package .= '<div class="m-b-5">' . ucwords($row->package->name). ' ('.ucfirst($row->package_type).')' . '</div>';

                $package .= '<a href="javascript:;" class="label label-rouded label-custom package-update-button"
                      data-toggle="tooltip" data-company-id="'.$row->id.'" data-original-title="Change"><i class="fa fa-edit" aria-hidden="true"></i> Change </a>';
                $package .= '</div>';
                return $package;
            })
            ->addColumn('details', function($row) {
                $companyUser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('company_id', $row->id)->first();

                if($companyUser && $companyUser->email_verification_code == null)
                {
                    $verified = '<i class="fa fa-check-circle" style="color: green;"></i>';
                } else if($companyUser && $companyUser->email_verification_code != null)
                {
                    $verified = '<i class="fa fa-times" style="color: red;"></i>';
                } else {
                    $verified = '-';
                }

                $registerDate = $row->created_at->format('d-m-Y');
                $totalUsers = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('company_id', $row->id)->count();

                $string = "<ul>";
                $string .= "<li>".__('modules.superadmin.verified').": ".$verified ."</li>";
                $string .= "<li>".__('modules.superadmin.registerDate').": ".$registerDate ."</li>";
                $string .= "<li>".__('modules.superadmin.totalUsers').": ".$totalUsers ."</li>";
                $string .= "</ul>";

                return $string;
            })
            ->rawColumns(['action', 'details', 'company_email',  'company_phone',  'web_login', 'logo', 'status', 'package'])
            ->make(true);
    }

    public function storeAndUpdate($company, $request) {
        $company->company_name = $request->input('company_name');
        $company->company_email = $request->input('company_email');
        $company->company_phone = $request->input('company_phone');
        $company->website = $request->input('website');
        $company->address = $request->input('address');
        // $company->currency_id = $request->input('currency_id');
        $company->timezone = $request->input('timezone');
        $company->locale = $request->input('locale');
        $company->status = $request->status;

        if ($request->hasFile('logo')) {
            $company->logo = $request->logo->hashName();
            $request->logo->store('uploads/app-logo');
        }
        
        $company->last_updated_by = $this->user->id;

        $company->save();


        //$this->updateExchangeRatesCompanyWise($company);

        return $company;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function usage()
    {
        $this->totalCompanies = Company::count();
        $this->packages = Package::all();
        return view('super-admin.companies.usage', $this->data);
    }
    public function usagedata(Request $request)
    {
        $companyid = $request->companyid;
        $packages = Company::query();
        $startdate = !empty($request->startdate) ? date('Y-m-d',strtotime($request->startdate)) : '';
        $endate = !empty($request->endate) ? date('Y-m-d',strtotime($request->endate)) : '';
        if(!empty($companyid)){
            $explodecom = explode(',',$companyid);
            $packages = $packages->whereIn('id',$explodecom);
        }
        $packages = $packages->get();
        return Datatables::of($packages)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'.route('super-admin.companies.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->editColumn('company_name', function ($row) {
                return ucfirst($row->company_name);
            })
            ->editColumn('company_email', function ($row) {
                return ucfirst($row->company_email);
            })
            ->editColumn('company_phone', function ($row) {
                return ucfirst($row->company_phone);
            })
            ->editColumn('projects',function ($row)use($startdate,$endate) {
                 $projectscount = Project::where('company_id',$row->id);
                 if(!empty($startdate)){
                     $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                 }
                 if(!empty($endate)){
                     $projectscount = $projectscount->where('created_at','<=',$startdate.' 23:59:59');
                 }
                $projectscount = $projectscount->count();
                 return $projectscount;
            })
            ->editColumn('employee', function ($row)use($startdate,$endate) {
                 $projectscount = Employee::where('company_id',$row->id)->where('user_type','employee');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }   if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$startdate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                 return $projectscount;
            })
            ->editColumn('client',function ($row)use($startdate,$endate) {
                 $projectscount = Employee::where('company_id',$row->id)->where('user_type','client');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$startdate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                 return $projectscount;
            })
            ->editColumn('contractor',function ($row)use($startdate,$endate) {
                 $projectscount = Employee::where('company_id',$row->id)->where('user_type','contractor');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                 return $projectscount;
            })
            ->editColumn('drawings',function ($row)use($startdate,$endate) {
                $projectscount = FileManager::where('company_id',$row->id)->where('pathtype','outline')->where('hashname','<>','');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('photos',function ($row)use($startdate,$endate) {
                $projectscount = FileManager::where('company_id',$row->id)->where('pathtype','photos')->where('hashname','<>','');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('documents',function ($row)use($startdate,$endate) {
                $projectscount = FileManager::where('company_id',$row->id)->where('pathtype','documents')->where('hashname','<>','');
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('labourattendance',function ($row)use($startdate,$endate) {
                $projectscount = ManpowerLog::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('issues',function ($row)use($startdate,$endate) {
                $projectscount = PunchItem::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('activity',function ($row)use($startdate,$endate) {
                $projectscount = Project::where('company_id',$row->id)->pluck('id')->toArray();
                $activitycount = ProjectCostItemsPosition::whereIn('project_id',$projectscount)->where('position','row');
                if(!empty($startdate)){
                    $activitycount = $activitycount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $activitycount = $activitycount->where('created_at','<=',$endate.' 23:59:59');
                }
                $activitycount = $activitycount->count();
                return $activitycount;
            })
            ->editColumn('task',function ($row)use($startdate,$endate) {
                $projectscount = Project::where('company_id',$row->id)->pluck('id')->toArray();
                $activitycount = ProjectCostItemsProduct::whereIn('project_id',$projectscount);
                if(!empty($startdate)){
                    $activitycount = $activitycount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $activitycount = $activitycount->where('created_at','<=',$endate.' 23:59:59');
                }
                $activitycount = $activitycount->count();
                return $activitycount;
            })
            ->editColumn('products',function ($row)use($startdate,$endate) {
                $projectscount = Product::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('indents',function ($row)use($startdate,$endate) {
                $projectscount = Indent::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('materialreceive',function ($row)use($startdate,$endate) {
                $projectscount = PurchaseInvoice::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('materialissue',function ($row)use($startdate,$endate) {
                $projectscount = ProductIssue::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('materialissuereturn',function ($row)use($startdate,$endate) {
                $projectscount = ProductIssueReturn::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('attendance',function ($row)use($startdate,$endate) {
                $projectscount = ProductIssueReturn::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->editColumn('payments',function ($row)use($startdate,$endate) {
                $projectscount = AccountPayments::where('company_id',$row->id);
                if(!empty($startdate)){
                    $projectscount = $projectscount->where('created_at','>=',$startdate.' 23:59:59');
                }
                if(!empty($endate)){
                    $projectscount = $projectscount->where('created_at','<=',$endate.' 23:59:59');
                }
                $projectscount = $projectscount->count();
                return $projectscount;
            })
            ->make(true);
    }
    public function weblogin(Request $request){
        $checked = $request->checked;
        $user = User::find($checked);
        if($user->web_login=='enable'){
            $user->web_login = 'disable';
        }else{
            $user->web_login= 'enable';
        }
        $user->save();
    }
    public function unRegUsers(){

        return view('super-admin.companies.un-reg-users', $this->data);
    }
    public function unRegUsersData(Request $request)
    {

        $startdate = !empty($request->startdate) ? date('Y-m-d',strtotime($request->startdate)) : '';
         $packages = User::withoutGlobalScope('active')->where('company_id','0');
        if(!empty($startdate)){
            $packages = $packages->where('created_at','>=',$startdate.' 00:00:01');
            $packages = $packages->where('created_at','<=',$startdate.' 23:59:59');
        }
        $packages = $packages->get();

        return Datatables::of($packages)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '';
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            ->editColumn('mobile', function ($row) {
                return '<a href="tel:'.$row->mobile.'" target="_blank">'.$row->mobile.'</a>';
            })
            ->editColumn('created_at', function ($row) {
                return date('d M Y',strtotime($row->created_at));
            })
            ->rawColumns(['action', 'mobile', 'created_at'])
            ->make(true);
    }
}
