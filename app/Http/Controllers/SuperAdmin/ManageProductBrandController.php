<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\ProductBrand;
use App\ProductCategory;
use Illuminate\Http\Request;

class ManageProductBrandController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Brands';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->categories = ProductBrand::all();
        return view('super-admin.product-brand.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('super-admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->categories = BoqCategory::all();
        return view('super-admin.boq-category.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::all();
        $this->category = BoqCategory::where('id',$id)->first();
        return view('super-admin.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = BoqCategory::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function data()
    {
        $products = ProductBrand::select('id', 'name')->get();

        return DataTables::of($products)
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            
            ->make(true);
    }
}
