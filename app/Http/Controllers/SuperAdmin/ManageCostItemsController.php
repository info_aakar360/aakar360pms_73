<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\Http\Requests\CostItems\StoreCostItems;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\ProductBrand;
use App\Units;
use App\ProductCategory;
use Illuminate\Http\Request;

class ManageCostItemsController extends SuperAdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Task';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->cost_items = CostItems::all();
        $this->categories = BoqCategory::all();
        return view('super-admin.cost-items.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        $this->cats = BoqCategory::all();
        return view('super-admin.cost-items.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->categories = CostItems::all();
        return view('super-admin.cost-items', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCostItems $request)
    {
        $category = new CostItems();
        $category->cost_item_name = $request->cost_item_name;
        $category->cost_item_description = $request->cost_item_description;
        $category->save();
        $lastid = $category->id;
        //dd();
        $cost_item_lavel = $request->cost_item_lavel;
        for($i = 0; $i < count($cost_item_lavel); $i++ ){
            $costlavel = new CostItemsLavel();
            $costlavel->cost_items_id = $lastid;
            $costlavel->boq_category_id = $cost_item_lavel[$i];
            $costlavel->save();
        }

        return Reply::success(__('Task Added Successfully'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreCostItems $request)
    {
        $category = new CostItems();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = CostItems::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::get();
        $this->cats = BoqCategory::all();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        return view('super-admin.cost-items.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CostItems::find($id);
        $category->cost_item_name = $request->cost_item_name;
        $category->cost_item_description = $request->cost_item_description;
        $category->save();
        //dd();
        $cost_item_lavel = $request->cost_item_lavel;
        $costlavel = CostItemsLavel::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsLavel::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('Task Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CostItems::destroy($id);
        $categoryData = CostItems::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function data()
    {
        $products = CostItems::select('id', 'cost_item_name', 'cost_item_description')
            ->get();

        dd($products);
        return DataTables::of($products)
            ->addColumn('action', function($row){
                return '<a href="'.route('super-admin.task.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('cost_item_name', function ($row) {
                return ucfirst($row->cost_item_name);
            })
            ->editColumn('cost_item_description', function ($row) {
                return ucfirst($row->cost_item_description);
            })

            ->rawColumns(['action'])
            ->make(true);
    }

    public function productAdd($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproducts = CostItemsProduct::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('super-admin.cost-items.product-add', $this->data);
    }

    public function productEdit($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproduct = CostItemsProduct::where('id', $id)->first();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('super-admin.cost-items.product-edit', $this->data);
    }

    public function getCost(Request $request){
        $qty = $request->qty;
        $rate = $request->rate;

        $final = $qty*$rate;

        return $final;
    }

    public function storeProduct(Request $request, $id)
    {
        $cost_item_lavel = $request->product_category_id;
        $costlavel = CostItemsProduct::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsProduct::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('Task Updated Successfully'));
    }

    public function productUpdate(Request $request, $id)
    {

        $costlavel = CostItemsProduct::find($id);
        $costlavel->product_category_id = $request->product_category_id;
        $costlavel->product_brand_id = $request->product_brand_id;
        $costlavel->unit = $request->unit;
        $costlavel->qty = $request->qty;
        $costlavel->wastage = $request->wastage;
        $costlavel->rate = $request->rate;
        $costlavel->cost = $request->cost;
        $costlavel->save();

        return Reply::success(__('Task Updated Successfully'));
    }

    public function destroyProduct($id)
    {
        CostItemsProduct::destroy($id);
        $categoryData = CostItemsProduct::all();
        return Reply::successWithData(__('Deleted Successfully'),['data' => $categoryData]);
    }
}
