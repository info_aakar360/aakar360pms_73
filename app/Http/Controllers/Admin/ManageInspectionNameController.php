<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Inspection\StoreInspectionName;
use App\InputFields;
use App\InspectionAnswer;
use App\InspectionAssignedUser;
use App\InspectionAssingnForm;
use App\InspectionFile;
use App\InspectionName;
use App\InspectionQuestion;
use App\InspectionType;
use App\ObservationReply;
use App\ObservationFiles;
use App\Observations;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\FileManager;
use Illuminate\Support\Facades\File;
use phpseclib\Crypt\RC4;
use Yajra\DataTables\DataTables;

class ManageInspectionNameController extends AdminBaseController
{
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',

        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Inspection';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $this->names = InspectionName::where('company_id',$user->company_id)->get();
        $this->inspectiontypes = InspectionType::where('company_id',$user->company_id)->get();
        return view('admin.inspection-name.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        return view('admin.inspection-name.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createName()
    {
        $user = $this->user;
        $this->types = InspectionType::where('company_id',$user->company_id)->get();
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        return view('admin.inspection-name.create', $this->data);
    }

    public function formdata(Request $request)
    {
        $user = $this->user;
        $inspectiontype = $request->inspectiontype;
        $inspectionarray = InspectionName::where('company_id',$user->company_id);
        if(!empty($inspectiontype)){
            $inspectionarray = InspectionName::where('type',$inspectiontype);
        }
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a href="'.route('admin.inspectionName.inspectionAssignForm',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Assign Employees"><i class="fa fa-user-plus" aria-hidden="true"></i></a>
                                  <a href="'.route('admin.inspectionName.editName',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                if($row->added_by==$user->id){
                    $actionlink .='<a href="javascript:;" data-cat-id="'.$row->id.'" class="btn btn-sm btn-danger btn-circle delete-category" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
              return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'description',
                function ($row) {
                    return substr(strip_tags($row->description),0,50);
                }
            )
            ->editColumn(
                'type',
                function ($row) {
                    return get_ins_type_name($row->type);
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_user_name($row->added_by)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'action','type', 'description', 'added_by'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeName(StoreInspectionName $request)
    {
        $user = $this->user;
        $category = new InspectionName();
        $category->company_id = $user->company_id;
        $category->name = $request->title;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->added_by = $user->id;
        $category->save();
        if(!empty($category->id)&&!empty($request->categoryid)){
            for($x=0;$x<=count($request->categoryid);$x++){
                if(!empty($request->categoryid[$x])){
                    $newcat = $request->categoryid[$x];
                    $questionlist = !empty($request->question[$newcat]) ? $request->question[$newcat] : array();
                    $responselist = !empty($request->response[$newcat]) ? $request->response[$newcat] : array();
                    if(!empty($questionlist)&&!empty($responselist)){
                        for($q=0;$q<=count($questionlist);$q++){
                            if(!empty($questionlist[$q])){
                                $question = new InspectionQuestion();
                            $question->inspection_name_id = $category->id;
                            if($request->category[$x]) {
                                $question->category = $request->category[$x];
                            }
                            $question->input_field_id = !empty($responselist[$q]) ? $responselist[$q] : '';
                            $question->question = !empty($questionlist[$q]) ? $questionlist[$q] : '';
                            $question->save();
                          }
                        }
                    }
                }
            }
        }

        $categoryData = InspectionName::all();
        return Reply::dataOnly(['inspectionID' => $category->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editName($id)
    {
        $user = $this->user;
        $this->names = InspectionName::where('company_id',$user->company_id)->get();
        $this->name = InspectionName::where('id',$id)->first();
        $this->types = InspectionType::where('company_id',$user->company_id)->get();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        $this->quescount = InspectionQuestion::where('inspection_name_id',$id)->count();
        $this->catcount =  \App\InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->count();
        return view('admin.inspection-name.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateName(StoreInspectionName $request, $id)
    {

        $user = $this->user;
        $category = InspectionName::find($id);
        $category->company_id = $user->company_id;
        $category->name = $request->title;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->added_by = $this->user->id;
        $category->save();
        if(!empty($category->id)&&!empty($request->categoryid)){
            InspectionQuestion::where('inspection_name_id',$category->id)->delete();
            for($x=0;$x<count($request->categoryid);$x++){
                $newcat = $request->categoryid[$x];
                $questionlist = !empty($request->question[$newcat]) ? $request->question[$newcat] : array();
                $responselist = !empty($request->response[$newcat]) ? $request->response[$newcat] : array();
                if(!empty($questionlist)&&!empty($responselist)){
                    for($q=0;$q<count($questionlist);$q++){
                        if(!empty($questionlist[$q])){
                            $question = new InspectionQuestion();
                            $question->inspection_name_id = $category->id;
                            if($request->category[$x]) {
                                $question->category = $request->category[$x];
                            }
                            $question->input_field_id = !empty($responselist[$q]) ? $responselist[$q] : '';
                            $question->question = !empty($questionlist[$q]) ? $questionlist[$q] : '';
                            $question->save();
                        }
                    }
                }
            }
        }
        return Reply::dataOnly(['inspectionID' => $category->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyName($id)
    {
        $user = $this->user;
        InspectionName::destroy($id);
        $categoryData = InspectionName::where('company_id',$user->company_id)->get();
        return Reply::successWithData(__('Inspection deleted successfully'),['data' => $categoryData]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyQuestion(Request $request)
    {
        $id = $request->id;
        if(empty($request->id)){
            $insid = $request->insid;
            $category = $request->category;
            if($category){
                InspectionQuestion::where('category',$category)->where('inspection_name_id',$insid)->delete();
            }else{
                InspectionQuestion::where('inspection_name_id',$insid)->where('id',$id)->delete();
            }
        }
        return Reply::success(__('Question deleted successfully'));
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $companyid = $this->user->company_id;
                $file = new InspectionFile();
                $file->user_id = $this->user->id;
                $file->company_id = $companyid;
                $file->inspection_id = $request->inspection_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/inspection-files/'.$request->inspection_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid.'/inspection-files/'.$request->inspection_id, $fileData, $fileData->hashname(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'inspection-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('inspection-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->inspection_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->inspection_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->inspection_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('inspection-files/'.$request->inspection_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/inspection-files/'.$request->inspection_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }

        }
        return Reply::redirect(route('admin.inspection-name.index'), __('modules.projects.projectUpdated'));
    }

    public function inspectionAnswer()
    {
        $user = $this->user;
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        $this->names = InspectionName::where('company_id',$user->company_id)->get();
        return view('admin.inspection-name.answer', $this->data);
    }

    public function inspectionAnswerForm($id)
    {
        $user = $this->user;
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->where('company_id',$user->company_id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->where('company_id',$user->company_id)->get();
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        $this->names = InspectionName::where('company_id',$user->company_id)->get();
//        $this->names = InspectionName::whereIn('assign_to',$this->user->id, $this->insp->assign_to)->get();
        return view('admin.inspection-name.answer-form', $this->data);
    }

    public function inspectionQuestion($id)
    {
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->where('company_id',$user->company_id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->where('company_id',$user->company_id)->get();
        $this->fields = InputFields::get();
        return view('admin.inspection-name.question', $this->data);
    }

    public function getData(Request $request){
        $id = $request->inputfeild;
        $row = $request->row_id;
        $html = get_input_field($id,$row);
        return Reply::dataOnly([$html]);
    }

    public function storeQuestion(Request $request){
        //dd($request->question);

        foreach($request->question as $key=>$value ){
            $question = new InspectionQuestion();
            $question->inspection_name_id = $request->inspection_id;
            if($request->category) {
                $question->category = $request->category;
            }
            $question->input_field_id = $request->input_field_id[$key];
            $question->question = $request->question[$key];
            $question->save();
        }

        return Reply::success(__('Question updated successfully'));
    }

    public function storeAnswer(Request $request){
        $questionid=$request->questionid;
        $replytype=$request->replytype;
        $formid=$request->formid;
        $color=$request->color;
        $inspectionassign = InspectionAssingnForm::where('id',$formid)->first();
        $insepctionid=$inspectionassign->inspection_id;
        $question = new InspectionAnswer();
        $question->inspection_id = $insepctionid;
        $question->inspection_question_id = $questionid;
        $question->replytype = $request->replytype ?: 'comment';
        $question->answer = $request->answer ?: 'N/A';
        $question->color = $color ?: 'blue';
        $question->answered_by = $this->user->id;
        $question->save();
        if ($request->hasFile('attachments')) {
            foreach ($request->attachments as $fileData){
                $storage = storage();
                $companyid = $this->user->company_id;
                $file = new InspectionFile();
                $file->user_id = $this->user->id;
                $file->company_id = $companyid;
                $file->inspection_id = $insepctionid;
                $file->question_id = $questionid ?: 0;
                $file->answer_id = $question->id ?: 0;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/inspection-files/'.$insepctionid, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid.'/inspection-files/'.$insepctionid, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'inspection-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('inspection-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $insepctionid)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$insepctionid);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $insepctionid)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('inspection-files/'.$insepctionid.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/inspection-files/'.$request->inspection_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->mimetype = $fileData->getClientMimeType();
                if($file->mimetype=='image/jpeg'||$file->mimetype=='image/jpg'||$file->mimetype=='image/png'){
                    $file->filetype = 'photo';
                }else{
                    $file->filetype = 'attachment';
                }
                $file->save();
            }
        }
        if($replytype=='response'){
           return Reply::success(__('Answer updated successfully'));
        }else{
            return redirect(url('admin/inspection-name/inspection-replies-view/'.$formid.'/'.$this->user->id));
        }
    }

    public function assignUser(){
        $this->employees = User::allEmployees();
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        $this->names = InspectionName::get();
        return view('admin.inspection-name.assign-user', $this->data);
    }

    public function assignUserStore(Request $request){
//        $x = explode(',', $request->assign_to);
        foreach($request->assign_to as $a) {
            $category = new InspectionAssignedUser();
            $category->inspection_id = $request->id;
            $category->user_id = $a;
            $category->save();
        }

        return Reply::success(__('User Assigned successfully'));
    }

    public function removeFile($id){
        $inspectionFiles = InspectionFile::findOrFail($id);
        $storage = storage();
        $companyid = $inspectionFiles->company_id;
        switch ($storage) {
            case 'local':
                File::delete($inspectionFiles->hashname, 'inspection-files/' . $inspectionFiles->inspection_id.'/'.$inspectionFiles->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete($companyid.'/inspection-files/' . $inspectionFiles->inspection_id.'/'.$inspectionFiles->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('inspection-files/' . $inspectionFiles->inspection_id.'/'.$inspectionFiles->hashname);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('inspection-files/' . $inspectionFiles->inspection_id.'/'.$inspectionFiles->hashname);
                break;
        }
        $inspectionFiles->delete();
        return Reply::success(__('image deleted successfully'));
    }

    public function inspectionReplies(){
        $this->employees = User::allEmployees();
        $this->fields = InputFields::where('company_id',$user->company_id)->get();
        $this->namess = InspectionName::where('company_id',$user->company_id)->get();
        $this->answers = InspectionAssignedUser::where('company_id',$user->company_id)->get();
        return view('admin.inspection-name.inspection-replies', $this->data);
    }

    public function inspectionRepliesView($id,$userid){
        $user = $this->user;
        $assigned = InspectionAssingnForm::where('id',$id)->first();
        $this->assigned = $assigned;
        $this->inspection = InspectionName::where('id',$assigned->inspection_id)->first();
        $this->files = InspectionFile::where('inspection_id',$assigned->inspection_id)->whereNull('question_id')->where('company_id',$user->company_id)->get();
        $this->userid = $userid;
        return view('admin.inspection-name.inspection-replies-view', $this->data);
    }
    public function inspectionAssigned(){
        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        $this->inspectiontypes = InspectionType::where('company_id',$user->company_id)->get();
        $this->userid = $user->id;
        return view('admin.inspection-name.inspection-assign', $this->data);
    }
    public function inspectionAssignedData(Request $request){
        $user = $this->user;
        $inspectiontype = $request->inspectiontype;
        $userid = $request->user;
        $startdate = $request->startdate;
        $enddate = $request->enddate;
        $assignstatus = $request->assignstatus;
        $this->employees = Employee::getAllEmployees($user);
        $this->inspectiontypes = InspectionType::where('company_id',$user->company_id)->get();
        $publicinspection =$assignedinspection =$distributioninspection = array();
        $publicinspection = InspectionAssingnForm::whereNull('private')->Orwhere('private','0')->pluck('inspection_id')->toArray();
        $assignedinspection = InspectionAssingnForm::whereRaw('FIND_IN_SET("'.$user->id.'",assign_to)')->where('private','1')->pluck('inspection_id')->toArray();
        $distributioninspection = InspectionAssingnForm::whereRaw('FIND_IN_SET("'.$user->id.'",distribution)')->where('private','1')->pluck('inspection_id')->toArray();
        $inspectionlist = array_unique(array_filter(array_merge($publicinspection,$assignedinspection,$distributioninspection)));
        $inspectionarray = InspectionName::join('inspection_assign_form','inspection_assign_form.inspection_id','=','inspection_name.id')
            ->select('inspection_name.*','inspection_assign_form.id as insid','inspection_assign_form.status as assignstatus');

          /*  ->whereRaw('FIND_IN_SET("'.$user->id.'",inspection_assign_form.assign_to)')
            ->OrwhereRaw('FIND_IN_SET("'.$user->id.'",inspection_assign_form.distribution)')*/
        $inspectionarray = $inspectionarray->whereIn('inspection_name.id',$inspectionlist);
          if(!empty($userid)){
              $inspectionarray = $inspectionarray->whereRaw('FIND_IN_SET("'.$userid.'",inspection_assign_form.assign_to)');
              $inspectionarray = $inspectionarray->where('inspection_assign_form.private','1');
          }
          if(!empty($inspectiontype)){
              $inspectionarray = $inspectionarray->where('inspection_name.type',$inspectiontype);
          }
          if(!empty($startdate)){
              $startdate = Carbon::parse($startdate)->format('d-m-Y');
              $inspectionarray = $inspectionarray->where('inspection_assign_form.due_date','>=',$startdate);
          }
          if(!empty($enddate)){
              $enddate = Carbon::parse($enddate)->format('d-m-Y');
              $inspectionarray = $inspectionarray->where('inspection_assign_form.due_date','<=',$enddate);
          }
          if(!empty($assignstatus)){
              $inspectionarray = $inspectionarray->where('inspection_assign_form.status','=',$assignstatus);
          }
        $inspectionarray = $inspectionarray->groupby('inspection_assign_form.inspection_id')->where('company_id',$user->company_id)->get();

        $roles = Role::where('name', '<>', 'client')->whereOr('name', '<>', 'contractor')->where('company_id',$user->company_id)->get();

        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {

                return '<a href="'.route('admin.inspectionName.inspectionRepliesView',[$row->insid, $user->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                           data-toggle="tooltip" data-original-title="View Reply"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'description',
                function ($row) {
                    return substr(strip_tags($row->description),0,50);
                }
            )
            ->editColumn(
                'type',
                function ($row) {
                    return get_ins_type_name($row->type);
                }
            )
            ->editColumn(
                'assignstatus',
                function ($row) {
                    return ucwords($row->assignstatus);
                }
            )
            ->rawColumns([ 'action','type', 'description', 'assignstatus'])
            ->make(true);
    }
    public function AssignForm($id){
        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->inspectionform = InspectionAssingnForm::where('inspection_id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->get();
        return view('admin.inspection-name.inspection-assign-employees', $this->data);
    }
    public function SubmitAssignForm(Request $request){
        $user = $this->user;
        $assignto =   $distribution = '';
        $assignto = implode(',',array_unique(array_unique($request->assign_to)));
        $distribution = implode(',',array_unique(array_unique($request->distribution)));
        $assignsubmit = new InspectionAssingnForm();
        $assignsubmit->user_id = $user->id;
        $assignsubmit->inspection_id = $request->inspectionid;
        $assignsubmit->assign_to = $assignto;
        $assignsubmit->distribution = $distribution;
        $assignsubmit->due_date = $request->due_date;
        $assignsubmit->location = $request->location;
        $assignsubmit->private = $request->private;
        $assignsubmit->status = $request->status;
        $assignsubmit->save();
        return Reply::success(__('Answer updated successfully'));
    }
  /*  public function inspectionRepliesSubmit(Request $request){
        $user = Auth::user();
        $questionid=$request->questionid;
        $formid=$request->formid;
        $comment = $request->comment;
        $inspectionanswer = new InspectionAnswer();
        $inspectionanswer->inspection_question_id = $questionid;
        $inspectionanswer->inspection_id = $formid;
        $inspectionanswer->answer = $comment;
        $inspectionanswer->answered_by = $user->id;
        $inspectionanswer->save();
        return redirect(url('admin/inspection-name/inspection-replies-view/'.$formid.'/'.$user->id));
    }*/

    public function observationIndex(){
        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        $this->inspectiontypes = InspectionType::get();
        $this->inspectionnames = InspectionName::get();
        $userid = $user->id;
        $this->items = Observations::whereRaw('FIND_IN_SET(?,assign_to)', [$userid])->OrwhereRaw('FIND_IN_SET(?,distribution)', [$userid])->get();
        return view('admin.observations.index', $this->data);
    }
     public function GetObservation(Request  $request,$inspection,$questionid){
        $user = $this->user;
         $inspectionname = InspectionName::find($inspection);
         if(empty($inspectionname)){
            return redirect(route('admin.inspection-name.index'));
         }
         $this->employees = Employee::getAllEmployees($user);
         $this->inspectiontypes = InspectionType::get();
         $this->inspectionassign = InspectionAssingnForm::where('inspection_id',$inspection)->first();
         $this->inspection = $inspectionname;
         $this->inspectionquestion = InspectionQuestion::where('inspection_name_id',$inspection)->whereid($questionid)->first();
         $this->page = $request->page;
         return view('admin.observations.create', $this->data);
    }
     public function observationEdit($obid){
         $user = $this->user;
        $observation = Observations::find($obid);
         $this->observation = $observation;
         $this->files = ObservationFiles::where('observation_id',$observation->id)->get();
         $this->employees = Employee::getAllEmployees($user);
         $this->inspection = InspectionName::find($observation->inspection_id);
         $this->inspectionquestion = InspectionQuestion::where('inspection_name_id',$observation->inspection_id)->whereid($observation->question_id)->first();
         return view('admin.observations.edit', $this->data);
    }
     public function observationReply($obid){
        $user = Auth::user();
        $observation = Observations::find($obid);
         $this->observation = $observation;
         $this->files = ObservationFiles::where('observation_id',$observation->id)->get();
         $this->employees = User::get();
         $this->inspection = InspectionName::find($observation->inspection_id);
         $this->inspectionquestion = InspectionQuestion::where('inspection_name_id',$observation->inspection_id)->whereid($observation->question_id)->first();
         $this->replies = ObservationReply::where('observation_id',$observation->id)->get();
         return view('admin.observations.reply', $this->data);
    }
     public function observationStore(Request $request){
         $user = $this->user;
         $this->employees = Employee::getAllEmployees($user);
         $inspectionid = $request->inspection;
         $questionid = $request->question;
         $observationid = $request->observationid;
         if(!empty($observationid)){
             $inspection = Observations::find($observationid);
         }else{
             $inspection = new Observations();
         }
         $inspection->company_id = $user->company_id;
         $inspection->title = $request->title;
         if ($request->description != '') {
             $inspection->description = $request->description;
         }
         $inspection->start_date = !empty($request->start_date) ? Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d') : '';
         $inspection->due_date = !empty($request->due_date) ?  Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d') : '';
         if($request->user_id != '') {
             $inspection->assign_to = implode(',', $request->user_id);
         }
         $inspection->inspection_id = $inspectionid;
         $inspection->question_id = $questionid;
         $inspection->priority = $request->priority;
         $inspection->status = $request->status;
         $inspection->type = $request->type;
         if($request->distribution != '') {
             $inspection->distribution = implode(',', $request->distribution);
         }
         $inspection->location = $request->location;
         if ($request->private != '') {
             $inspection->private = $request->private;
         }
         if ($request->reference != '') {
             $inspection->reference = $request->reference;
         }
         $inspection->added_by = $this->user->id;
         $inspection->save();

         return Reply::dataOnly(['observationID' => $inspection->id]);
         //        return Reply::redirect(route('admin.all-inspections.index'), __('messages.inspectionCreatedSuccessfully'));
    }
    public function assignStatusUpdate(Request $request)
    {

        $id = $request->assignid;
        $inspection = InspectionAssingnForm::findOrFail($id);
        $inspection->status = $request->status;
        $inspection->save();
        return Reply::success(__('Status updated successfully'));
    }
    public function observationreplyPost(Request $request, $id)
    {
        $inspection = Observations::findOrFail($id);
        $inspection->status = $request->status;
        $inspection->save();
        $pi = new ObservationReply();
        $pi->status = $request->status;
        $pi->comment = $request->comment;
        $pi->observation_id = $id;
        $pi->added_by = $this->user->id;
        $pi->save();
        return Reply::success(__('Observations added successfully'));
    }
    public function observationstoreImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $companyid = $this->user->company_id;
                $file = new ObservationFiles();
                $file->user_id = $this->user->id;
                $file->company_id = $companyid;
                $file->observation_id = $request->observation_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/observation-files/'.$request->observation_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid.'/observation-files/'.$request->observation_id, $fileData, $fileData->hashname(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'observation-files')
                            ->first();
                        if(!$dir) {
                            Storage::cloud()->makeDirectory('observation-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->observation_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->observation_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->observation_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('observation-files/'.$request->observation_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/observation-files/'.$request->observation_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->observation_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('admin.punch-items.index'), __('modules.projects.projectUpdated'));
    }
    public function observationremoveImage($id){
        $inspectionFiles = ObservationFiles::findOrFail($id);
        Files::deleteFile($inspectionFiles->hashname, 'observation-files/' . $inspectionFiles->observation_id);
        $inspectionFiles->delete();
        return Reply::success(__('image deleted successfully'));
    }
    public function destroy(Request $request, $id)
    {
        $inspection = Observations::findOrFail($id);

        // If it is recurring and allowed by user to delete all its recurring inspections
//        if ($request->has('recurring') && $request->recurring == 'yes') {
//            Task::where('recurring_inspection_id', $id)->delete();
//        }

        $inspectionFiles = ObservationFiles::where('observation_id', $id)->get();

        foreach ($inspectionFiles as $file) {
            Files::deleteFile($file->hashname, 'observation-files/' . $file->inspection_id);
            $file->delete();
        }

        Observations::destroy($id);
        //calculate project progress if enabled

        return Reply::success(__('Observations deleted successfully'));
    }
    public function observationData(Request $request)
    {
        $user = $this->user;

        $inspectionname = $request->inspectionname;
        $assignid = $request->user;
        $selectdate = $request->selectdate;
        $status = $request->status;
        $inspectionarray = Observations::where('company_id',$user->company_id);

        if(!empty($inspectionname)){
            $inspectionarray = $inspectionarray->where('inspection_id',$inspectionname);
        }
        if(!empty($selectdate)){
            $inspectionarray = $inspectionarray->where('due_date','<=',$selectdate);
        }
        if(!empty($status)){
            $inspectionarray = $inspectionarray->where('status',$status);
        }
        if(!empty($assignid)){
            $inspectionarray = $inspectionarray->whereRaw('FIND_IN_SET(?,assign_to)', [$assignid]);
        }
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                    $as = explode(',',$row->assign_to);
                    if (in_array($user->id, $as)){
                        $actionlink .= '<a href="'.route('admin.observations.reply', $row->id).'" class="btn btn-info btn-circle"
                           data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-reply" aria-hidden="true"></i></a>';
                        }
                        if($row->added_by==$user->id){
                    $actionlink .= '<a href="'.route('admin.observations.edit',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                      $actionlink .='<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                           data-toggle="tooltip" data-observation-id="'.$row->id.'" data-original-title="Delete">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>';
                         }
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'start_date',
                function ($row) {
                    return Carbon::parse($row->start_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'due_date',
                function ($row) {
                    return Carbon::parse($row->due_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'private',
                function ($row) {
                    return !empty($row->private) ? 'Private' : 'Public';
                }
            )
            ->editColumn(
                'inspectionname',
                function ($row) {
                    $inspectionname = \App\InspectionName::where('id',$row->inspection_id)->first();
                    return $inspectionname->name;
                }
            )
            ->editColumn(
                'assign_to',
                function ($row) {
                    $dis = '';
                    $distributionarra = explode(',',$row->assign_to);
                    foreach ($distributionarra as $distribut){
                        $dis .= '<div class="row"><div class="col-sm-3 col-xs-4">'.get_users_images($distribut).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $distribut) . '">'.ucwords(get_user_name($distribut)).'</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'distribution',
                function ($row) {
                    $dis = '';
                    $distributionarra = explode(',',$row->distribution);
                    foreach ($distributionarra as $distribut){
                        $dis .= '<div class="row"><div class="col-sm-3 col-xs-4">'.get_users_images($distribut).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $distribut) . '">'.ucwords(get_user_name($distribut)).'</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_user_name($row->added_by)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'action','inspectionname', 'start_date', 'due_date', 'added_by', 'assign_to', 'distribution'])
            ->make(true);
    }
}
