<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\Http\Requests\ProductCategory\StoreProCategoryRequest;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class ProductCategoryController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Category';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'store';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->categories = ProductCategory::all();
        return view('admin.product-category.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::all();
        $this->category = BoqCategory::where('id',$id)->first();
        return view('admin.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBoqCategory $request, $id)
    {
        $category = BoqCategory::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function addProduct(Request $request){
        return view('admin.product-category.create', $this->data);
    }
    public function postProduct(StoreProCategoryRequest $request){
        $category = new ProductCategory();
        $category->name = $request->name;
        $category->company_id = $this->companyid;
        $category->save();
        return Reply::success(__('Product Category Saved Successfully!!'));
    }
    public function data(){
        $proData = ProductCategory::where('company_id',$this->companyid)->get();
        return DataTables::of($proData)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return '<a onclick="showEditProduct('.$row->id.')" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('name', function ($row) {
                return $row->name;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function editProduct($id)
    {
        $this->pcategory = ProductCategory::where('id',$id)->first();
        return view('admin.product-category.edit', $this->data);
    }

    public function updateCategory(StoreProCategoryRequest $request){

        $category  = ProductCategory::find($request->rowid);
        $category->name = $request->name;
        $category->company_id = $this->companyid;
        $category->save();
        return Reply::success(__('Category Updated Successfully!!'));
    }

    public function destroyCategory($id){
        ProductCategory::destroy($id);
        return Reply::success(__('Category Deleted Successfully!!'));
    }
}
