<?php

namespace App\Http\Controllers\Admin;

use App\Bom;
use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Indent\StoreIndentRequest;
use App\Http\Requests\Admin\Indent\UpdateIndentRequest;
use App\Http\Requests\Admin\Rfq\ConvertRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Http\Requests\Grn\StoreGrnRequest;
use App\IndentsFiles;
use App\IndentsReply;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PiProducts;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\ProductLog;
use App\ProductReturns;
use App\Project;
use App\ProjectMember;
use App\ProjectsLogs;
use App\PurchaseInvoice;
use App\PurchaseOrder;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Quotes;
use App\Rfq;
use App\RfqProducts;
use App\Role;
use App\Indent;
use App\IndentProducts;
use App\Stock;
use App\Store;
use App\TmpIndent;
use App\Units;
use App\UniversalSearch;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Stripe\Quote;
use Yajra\DataTables\Facades\DataTables;

class MemberIndentController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.indent';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';
        $this->middleware(function ($request, $next) {
            if (!in_array('indent', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->indents = Indent::where('company_id',$this->companyid)->get();
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->categories = ProductCategory::where('company_id',$this->companyid)->get();
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        $this->totalIndents = count($this->indents);
        return view('admin.indent.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this-> projects = Project::whereIn('id', $projecta)->get();
        $this->products = Product::where('company_id',$this->companyid)->get();
        $this->lproducts = Bom::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('admin.indent.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $bids = ProductCategory::where('id', $pid)->select('brands')->first();
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html['estQty'] = get_est_qty($project_id, $store_id, $pid);
        $html['reqQty'] = get_est_qty($project_id, $store_id, $pid);
        $html['unit'] = get_unit($project_id, $store_id, $pid);
        $html['unitData'] = get_unit_data($project_id, $store_id, $pid);
        if(count($brands)){
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
            foreach($brands as $brand){
                $html['brands'] .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
            }
        }else{
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
        }
        return $html;
    }
    public function getStores(Request $request){
        $pid = $request->pid;
        $stores = Store::where('project_id', $pid)->select('id','company_name')->get();
        if(count($stores) > 0){
            $html['stores'] = '<option value="">Select Store</option>';
            foreach($stores as $store){
                $html['stores'] .= '<option value="'.$store->id.'">'.$store->company_name.'</option>';
            }
        }else{
            $html['stores'] = '<option value="">Select Store</option>';
        }
        return $html;
    }
    public function getBomIndent(Request $request){
        $pid = $request->pid;
        $sid = $request->sid;
        $products = Bom::where('project_id', $pid)->where('store_id', $sid)->get();

        if(count($products) > 0){
            $html['products'] = '<option value="">Select Products</option>';
            foreach($products as $product){
                $html['products'] .= '<option value="'.$product->product_id.'">'.get_local_product_name($product->product_id).'</option>';
            }
        }else{
            $html['products'] = '<option value="">Select Products</option>';
        }
        return $html;
    }
    public function getTempdata(Request $request){
        $sid = null;
        $project_id = '1';
        $store_id = '1';
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $allData = TmpIndent::where('session_id', $sid)->get();
        return DataTables::of($allData)
            ->addColumn('action', function ($row)  {
                $ret = '';
                $completegrn = getCompleteGrn($row->id,$row->project_id,$row->store_id,$row->company_id);
                $ret .= '<a href="' . route('admin.indent.viewIndent', [$row->id]) . '" class="btn btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="View Indent"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'category',
                function ($row) {
                    return get_local_product_name($row->cid);
                }
            )
            ->editColumn(
                'brand',
                function ($row) {
                        return get_pbrand_name($row->bid);
                }
            ) ->editColumn(
                'estquantity',
                function ($row) use($project_id,$store_id){
                        return get_est_qty($project_id, $store_id, $row->cid);
                }
            ) ->editColumn(
                'resquantity',
                function ($row) use($project_id,$store_id){
                    return get_est_qty($project_id, $store_id, $row->cid);
                }
            )->editColumn(
                'reqquantity',
                function ($row) {
                        return $row->qty;
                }
            )->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'duedate',
                function ($row) {
                    return $row->dated;
                }
            ) ->editColumn(
                'remark',
                function ($row) {
                    return  $row->remark;
                }
            )

            ->addIndexColumn()
            ->rawColumns(['category','brand','estquantity','resquantity','duedate','action'])
            ->make(true);

    }
    public function storeTmp(Request $request){

        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }

        $project_id = $request->project_id;
        $store_id = $request->store_id;
        $tmpdata = new TmpIndent();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->dated = $request->dated;
        $tmpdata->remark = $request->remark;
        $tmpdata->save();
       $allData = TmpIndent::where('session_id', $sid)->get();
        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';

        return $html;
    }

    public function deleteTmp(Request $request){
        $project_id = $request->project_id;
        $store_id = $request->store_id;
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }

        TmpIndent::where('id',$request->did)->delete();
        $allData = TmpIndent::where('session_id', $sid)->get();
        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIndentRequest $request)
    {
       // dd($request->all());
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $sr = 1;
        $ind = Indent::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->indent_no);
            $sr = $in[2];
            $sr++;
        }
        $indent_no = 'IND/'.date("Y").'/'.$sr;
        $indent = new Indent();
        $indent->indent_no = $indent_no;
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remarkx;
        if($request->work_order_id==null){
            $indent->work_ord_id = $request->work_order_id;

        }
        $indent->company_id = $this->user->company_id;
        $indent->save();
        $all_data = TmpIndent::where('session_id',$sid)->get();

        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.indent.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this-> projects = Project::whereIn('id', $projecta)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->lproducts =  Bom::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->indent_id = $id;
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        $this->stores = Store::where('company_id',$this->companyid)->get();
        return view('admin.indent.edit', $this->data);
    }

    public function viewIndent($id, Request $request)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->stores = Store::all();
        $this->projects = Project::whereIn('id',$projectlist)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->lproducts = Product::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        $this->replies = IndentsReply::where('indents_id', $id)->orderBy('id','desc')->get();
        return view('admin.indent.view-indent', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIndentRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remark;
        $indent->save();
        IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.indent.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indent = Indent::find($id);
        if(!empty($indent)){
            $projectde  = Rfq::where('indent_id',$id)->where('store_id',$indent)->where('project_id',$indent->project_id)->first();
            if (!empty($projectde->id)) {
                $errormsg = 'Please Remove RFQ First.';
            }
        }
        if (!empty($projectde->id)) {
            $projectde  = Quotes::where('rfq_id',$projectde->id)->first();
            if (!empty($projectde->id)) {
                $errormsg = 'Please Remove Quotation First.';
            }
        }
        $projectde  = PurchaseOrder::where('indent_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Order First.';
        }
        $projectde  = PurchaseInvoice::where('indent_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove GRN First.';
        }

        if (!empty($projectde->id)) {
            $projectde  = ProductReturns::where('invoice_id', $projectde->id)->first();
            if (!empty($projectde->id)) {
                $errormsg = 'Please Remove Product Return First.';
            }
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            Indent::destroy($id);
            IndentProducts::where('indent_id', $id)->delete();
            ProjectsLogs::where('module_id',$id)->where('module','indents')->delete();
            return Reply::success(__('messages.indentDeleted'));
        }
    }
    public function approve($id, $val)
    {
        DB::beginTransaction();
        $ind = Indent::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('Indent updated successfully');
    }

    public function data(Request $request)
    {
        $done = '';
        $indents = Indent::join('stores', 'stores.id', '=', 'indents.store_id');
        if ($request->store != 'all' && $request->store != '') {
            $indents = $indents->where('indents.store_id', $request->store);
        }
        if ($request->status != 'all' && $request->status != '') {
            $indents = $indents->where('indents.status', $request->status);
        }
        if ($request->project_id != 'all' && $request->project_id != '') {
            $indents = $indents->where('indents.project_id', $request->project_id);
        }
        $indents = $indents->select(['indents.*'])->orderBy('indents.id','DESC')->groupBy('indents.id')->get();
         $user = Auth::user();
        return DataTables::of($indents)
            ->addColumn('action', function ($row) use ($user,$request) {
                $ret = '';
                $completegrn = getCompleteGrn($row->id,$row->project_id,$row->store_id,$row->company_id);
                if(count(array_unique($completegrn)) === 1 && end($completegrn) === 'done'){
                    $done = 'done';
                }else {
                    $done = 'pending';
                }
                $pretrun = \App\ProductLog::where('project_id',$row->project_id)->where('store_id',$row->store_id)->where('company_id',$row->company_id)->where('indent_id',$row->id)->where('module_name','=','product_return')->sum('quantity');
                if($done === 'pending' && !empty($pretrun)){
                    $indent = Indent::find($row->id);
                    $indent->status = 2;
                    $indent->save();
                }
                if($row->status == 0 || $row->status == 4){
                    $ret .= '<a href="' . route('admin.indent.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 || $row->status == 4 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 || $row->status == 4 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }

                if($row->status == 0 || $row->status == 4 && $row->approve == 1){
                    $ret .= '<a href="' . route('admin.indent.gconvert', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To RFQ"><i class="fa fa-send" aria-hidden="true"></i></a>&nbsp;';

                }
                if($done !== 'done' && $row->status !== 3 && $row->approve == 1) {
                    $ret .= '<a href="' . route('admin.indent.convertGrn', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To GRN"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->approve == 1){
                    $ret .= '<a href="' . route('admin.stores.projects.convertProductIssue', [$row->project_id, $row->store_id, $row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Issue Product"><i class="fa fa-plus" aria-hidden="true"></i></a>&nbsp;';
                }
                $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';

                $ret .= '<a href="'.route('admin.indent.viewIndent', [$row->id]) .'" class="btn btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="View Indent"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';

                $ret .= '<a href="'.route('admin.indent.exportPDF', [$row->id]) .'" data-toggle="tooltip" data-original-title="Export to PDF"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px;color:red"></i></a>&nbsp;';

                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending RFQ';
                    }else if($row->status == 1){
                        return 'Pending PO';
                    }else if($row->status == 2){
                        return 'Pending Purchase';
                    }else if($row->status == 4){
                        return 'Pending ';
                    }else{
                        return 'Purchase Done';
                    }
                }
            )
            ->editColumn(
                'approve',
                function ($row) {
                    if($row->approve == 0){
                        return 'Pending Approval';
                    }else if($row->approve == 1){
                        return 'Approved';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function exportPDF($id){
        $this->indent = Indent::find($id);
        $this->all_data = IndentProducts::where('indent_id', $id)->get();
        $pdf = PDF::loadView('admin.indent.indentPDF',$this->data);
        $pdf->setPaper("A4", "portrait");
        return $pdf->download('indent.pdf');
    }
    public function convert($id,Request $request)
    {
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();

        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->unit = $data->unit;
            $tmp->qty = $data->quantity;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        $this->indent_id = $id;
        return view('admin.indent.convert', $this->data);
    }
    public function postConvert($id, ConvertRfqRequest $request){
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $rfq = new Rfq();

        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq->rfq_no = $rfq_no;
        $rfq->indent_no = $indent->indent_no;
        $rfq->store_id = $indent->store_id;
        $rfq->project_id = $indent->project_id;
        $rfq->remark = $indent->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->save();
        //IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        $indent->status = 1;
        $indent->save();
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    public function export($status, $indent)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'indent')
            ->where('roles.company_id', company()->id)
            ->leftJoin('indent_details', 'users.id', '=', 'indent_details.user_id')
            ->select(
                'users.id',
                'indent_details.name',
                'indent_details.email',
                'indent_details.mobile',
                'indent_details.company_name',
                'indent_details.address',
                'indent_details.website',
                'indent_details.created_at'
            )
            ->where('indent_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($indent != 'all' && $indent != '') {
            $rows = $rows->where('users.id', $indent);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('indents', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('indents file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $company = $this->user->company_id;
                $file = new IndentsFiles();
                $file->company_id = $company;
                $file->added_by = $this->user->id;
                $file->indents_id = $request->indent_id;
                $file->reply_id = $request->reply_id ?: 0;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/indents-files/'.$request->indent_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($company.'/indents-files/'.$request->indent_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'indents-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('indents-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->indent_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->indent_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->indent_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('indents-files/'.$request->indent_id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/indents-files/'.$request->indent_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->indent_id, __('messages.newFileUploadedToTheProject'));
            }
        }
    }

    public function destroyImage(Request $request, $id)
    {
        $file = IndentsFiles::findOrFail($id);
        $storage = storage();
        $company = company()->id;
        switch($storage) {
            case 'local':
                File::delete('user-uploads/indents-files/'.$id.'/'.$file->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete($company.'/indents-files/'.$id.'/'.$file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('indents-files/'.$id.'/'.$file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('indents-files/'.$id.'/'.$file->filename);
                break;
        }
        $file->delete();
        return Reply::successWithData(__('messages.fileDeleted'));
    }

    public function removeFile($id){

        $file = IndentsFiles::findOrFail($id);
        $storage = storage();
        $company = company()->id;
        switch($storage) {
            case 'local':
                File::delete('user-uploads/indents-files/'.$id.'/'.$file->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete($company.'/indents-files/'.$id.'/'.$file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('indents-files/'.$id.'/'.$file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('indents-files/'.$id.'/'.$file->filename);
                break;
        }
        $file->delete();
        return Reply::success(__('image deleted successfully'));
    }

    public function reply($id)
    {
        $this->indent = Indent::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = IndentsFiles::where('indents_id',$id)->where('reply_id','0')->get();
        $this->replies = IndentsReply::where('indents_id',$id)->get();
        return view('admin.indent.reply', $this->data);
    }
    public function replyPost(Request $request, $id)
    {
        $user = $this->user;

        $mentionusers = !empty($request->mentionusers) ? array_unique(array_filter(explode(',',$request->mentionusers))) : null;
        $punchitem = Indent::find($id);

        $pi = new IndentsReply();
        $pi->company_id = $user->company_id;
        $pi->comment = $request->comment;
        $pi->indents_id = $punchitem->id;
        $pi->added_by = $this->user->id;
        $pi->mentionusers = !empty($mentionusers) ? implode(',',$mentionusers) : null;
        $pi->save();

        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $pi->id;
        $createlog->module = 'indents_reply';
        $createlog->modulename = 'indents_comment';
        $createlog->project_id = $punchitem->project_id ?: '';
        $createlog->subproject_id = $punchitem->title ?: '';
        $createlog->segment_id = $punchitem->segment ?: '';
        $createlog->heading =  $pi->comment;
        $createlog->description = 'Indent Commented by '.$user->name.' with no '.$punchitem->indent_no.' for the project '.get_project_name($punchitem->project_id);
        $createlog->medium = 'web';
        $createlog->mentionusers =  !empty($mentionusers) ? implode(',',$mentionusers) : null;
        $createlog->save();

        return Reply::dataOnly(['indentID' => $punchitem->id,'replyID' => $pi->id]);
    }
    public function projectMembers(Request $request,$projectid){
        $user = $this->user;
        if($projectid){
            $pm = ProjectMember::where('project_id',$projectid)->get()->pluck('user_id')->toArray();
            if($pm){
                $usersarray = array();
                $userslist = User::whereIn('id',$pm)->get();
                foreach ($userslist as $users){
                    $usersdata = array();
                    $usersdata['id'] = $users->id;
                    $usersdata['name'] = $users->name;
                    $usersdata['avatar'] = get_users_image_link($users->id);
                    $usersdata['type'] = 'contact';
                    $usersarray[] = $usersdata;
                }
                return response()->json($usersarray);
            }
        }
    }

    public function convertGrn(Request $request, $id)
    {
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
           $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
       }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id',$id)->first();
        $this->id = $id;
        return view('admin.indent.convert-grn', $this->data);
    }

    public function postConvertGrn(StoreGrnRequest $request,$id){
        $sid = null;
        $puro_id = '';
        $sup_id = '0';
        $status = '0';
        $price = $request->price;
        $tax = $request->tax;
        $qauantity = $request->qty;
       // dd(array_unique($qauantity));
        if (count(array_unique($qauantity)) === 1 && end($qauantity) === "0") {
           return Reply::error('Please Enter Valid Quantity');
        }
        $indent_qty = $request->indent_qty;
        $bid = $request->bid;
        $unit = $request->unit;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $ind = PurchaseInvoice::orderBy('id', 'DESC')->first();
        if($ind == null){
            $grnNo = 'GRN-'.date("d-m-Y").'-1';
        }else {
            $grnNo = 'GRN-' . date("d-m-Y") . '-' . $ind->id;
        }
        if(!empty($indent) && $indent->status == 2){
            $purchase_invoice = PurchaseInvoice::where('indent_id',$id)->first();
            if(!empty($purchase_invoice)){
                $puro_id     =  $purchase_invoice->po_id;
                $sup_id =  $purchase_invoice->supplier_id;
            }
        }

        $rfq = new PurchaseInvoice();
        $rfq_no = $grnNo;
        $rfq->invoice_no = $rfq_no;
        $rfq->po_id = $puro_id;
        $rfq->supplier_id = $sup_id;
        $rfq->store_id = $indent->store_id;
        $rfq->company_id = $this->user->company_id;
        $rfq->project_id = $indent->project_id;
        $rfq->dated = $request->inv_dated;
        $rfq->indent_id = !empty($request->indent_id)?$request->indent_id:'';
        $rfq->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $rfq->remark = $indent->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->gt = $request->grandTotal;
        $rfq->save();
        $all_data = $request->cid;
        foreach ($all_data as $key=>$data){
            if($request->qty[$key] !== '') {
                $indPro = new PiProducts();
                $indPro->pi_id = $rfq->id;
                $indPro->product_id = $data;
                $indPro->indent_id = $id;
                $indPro->bid = $bid[$key];
                $indPro->quantity = $qauantity[$key];
                $indPro->unit_id = $unit[$key];
                $indPro->price = $price[$key];
                $indPro->date = $request->inv_dated;
                $indPro->tax = $tax[$key];
                $indPro->remark = $request->remark[$key];
                $indPro->save();
                $sku = time();
                if($indPro){
                    $stockCheck = Stock::where('cid',$data)->where('store_id',$indent->store_id)->where('project_id',$indent->project_id)->where('indent_id',$id)->first();
                    $getStockQuantity = Stock::where('cid',$data)->where('store_id',$indent->store_id)->where('project_id',$indent->project_id)->where('indent_id',$id)->sum('quantity');
                    if($indent_qty[$key] >= $qauantity[$key]){
                        $stock = new Stock();
                        $stock->sku = $sku;
                        $stock->po = $request->po;
                        $stock->indent_id = $id;
                        $rfq->invoice_no = $rfq_no;
                        $stock->inv_id = $rfq->id;
                        $stock->cid = (!empty($data)) ? $data : '';
                        $stock->bid = (!!empty($request->bid[$key])) ? $request->bid[$key] : '';
                        $stock->quantity = (!empty($request->qty[$key])) ? $request->qty[$key] : '';
                        $stock->stock = (!empty($request->qty[$key])) ? $request->qty[$key] : '';
                        $stock->unit = (!empty($request->unit[$key])) ? $request->unit[$key] : '';
                        $stock->store_id = $indent->store_id;
                        $stock->project_id = $indent->project_id;
                        $stock->supplier_id = (!empty($request->supplier)) ? $request->supplier : '';
                        $stock->company_id = $this->user->company_id;
                        $stock->save();
                        if($stock){
                            $pl = new ProductLog();
                            $pl->store_id = $indent->store_id;
                            $pl->indent_id = $id;
                            $pl->project_id = $indent->project_id;
                            $pl->company_id = $this->user->company_id;
                            $pl->created_by = $this->user->id;
                            $pl->module_id = $rfq->id;
                            $pl->module_name = 'grn';
                            $pl->product_id = (!empty($data)) ? $data : '';
                            $pl->quantity = (!empty($request->qty[$key])) ? $request->qty[$key] : '';
                            $pl->balance_quantity = (!empty($request->qty[$key])) ? $request->qty[$key] : '';
                            $pl->transaction_type = 'plus';
                            $pl->remark = (!empty($request->remark[$key])) ? $request->remark[$key] : '';
                            $pl->save();
                        }
                    }else{
                        if(!empty($stockCheck)){
                            $stockCheck->stock = (int)$stockCheck->stock + (int)$request->qty[$key];
                            $stockCheck->save();
                            if(!empty($stockCheck)){
                                $pl = new ProductLog();
                                $pl->store_id = $indent->store_id;
                                $pl->project_id = $indent->project_id;
                                $pl->indent_id = $id;
                                $pl->company_id = $this->user->company_id;
                                $pl->created_by = $this->user->id;
                                $pl->module_id = $rfq->id;
                                $pl->module_name = 'grn';
                                $pl->product_id = (!empty($data)) ? $data : '';
                                $pl->quantity = (!empty($request->qty[$key])) ? $request->qty[$key] : '';
                                $pl->balance_quantity = (int)$stockCheck->stock+(int)$request->qty[$key];
                                $pl->transaction_type = 'plus';
                                $pl->remark = (!empty($request->remark[$key])) ? $request->remark[$key] : '';
                                $pl->save();
                            }
                        }
                    }
                }
            }
        }
        $completegrn = getCompleteGrn($id,$indent->project_id,$indent->store_id,$this->user->company_id);
        if (count(array_unique($completegrn)) === 1 && end($completegrn) === 'done') {
            $status = 3;
        }else {
            $status = 2;
        }
        $indent->status = $status;
        $indent->save();
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.inventory.invoices'));
    }
}
