<?php

namespace App\Http\Controllers\Admin;

use App\AdhocDeduction;
use App\AdhocEarning;
use App\Allowance;
use App\AsignStructure;
use App\ContractSalaryStructure;
use App\Employee;
use App\EmployeeDetails;
use App\PfandEsiSettings;
use App\RecurringComponent;
use App\SalaryBaseComponent;
use App\SalaryComponentVariable;
use App\SalaryStructureSettings;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helper\Reply;
use Illuminate\Support\Arr;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class ManageSalaryStructureController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.salarycomponent';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('payroll',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function recurring(){
        return view('admin.salary-structure.recurring.index',$this->data);
    }

    public function data(){
        // Getting View data
        $recurringdata = RecurringComponent::where('company_id',$this->user->company_id)->get();
        return DataTables::of($recurringdata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->component_name;
            })
            ->editColumn('type', function ($row)  {

                return $row->component_type;
            })
            ->editColumn('taxable', function ($row)  {

                return $row->taxable;
            })->editColumn('anuallimit', function ($row)  {
                return $row->anual_amount;
            })->editColumn('individualOverride', function ($row)  {
                return $row->individual_override;
            })->editColumn('proofRequired', function ($row)  {
                return $row->proof_required;
            })->editColumn('includedinCTC', function ($row)  {
                return $row->include_in_ctc;
            })->editColumn('appliedTo', function ($row)  {
                return $row->esi_applicable;
            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="assignRules('.$row->id.')"
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'taxable','anuallimit','individualOverride','includedinCTC','appliedTo','action'])
            ->make(true);
    }
    public function variableData(){
        // Getting View data
        $vriabledata = SalaryComponentVariable::where('company_id',$this->user->company_id)->get();
        return DataTables::of($vriabledata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->component_name;
            })
            ->editColumn('type', function ($row)  {
                return $row->Payout_frequency;
            })
            ->editColumn('taxable', function ($row)  {
                return $row->taxable;
            })->editColumn('anuallimit', function ($row)  {
                return $row->anual_amount;
            })->editColumn('individualOverride', function ($row)  {
                return $row->individual_override;
            })->editColumn('includedinCTC', function ($row)  {
                return $row->include_in_ctc;
            })->editColumn('appliedTo', function ($row)  {
                return $row->esi_applicable;
            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="assignRules('.$row->id.')"
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name','type','taxable','esiapplicable','action'])
            ->make(true);
    }

    public function addComponent(){
        return view('admin.salary-structure.recurring.add_component',$this->data);
    }

    public function storeRecurringcomponent(Request $request){
        $storeRecuring = new RecurringComponent();
        $storeRecuring->company_id = $this->user->company_id;
        $storeRecuring->component_name = $request->component_name;
        $storeRecuring->component_type = $request->component_type;
        $storeRecuring->lop_dependent = $request->lop_dependent;
        $storeRecuring->include_in_ctc = $request->include_in_ctc;
        $storeRecuring->individual_override = $request->individual_override;
        $storeRecuring->proof_required = $request->proof_required;
        $storeRecuring->taxable = $request->taxable;
        $storeRecuring->calc_type = $request->calc_type;
        $storeRecuring->anual_amount = $request->anual_amount;
        $storeRecuring->baded_on = $request->baded_on;
        $storeRecuring->from = $request->from;
        $storeRecuring->to = $request->to;
        $storeRecuring->equal = $request->equal;
        $storeRecuring->save();
        return Reply::success(__('messages.recuringComponentSaveSuccess'));
        abort(403);
    }
    public function storeBaseComponent(Request $request){
        $storeBaseRecuring = new SalaryBaseComponent();
        $storeBaseRecuring->company_id = $this->user->company_id;
        $storeBaseRecuring->basic = $request->component_basic;
        $storeBaseRecuring->hra = $request->component_hra;
        $storeBaseRecuring->pf_employer = 'yes';
        $storeBaseRecuring->esi_employer = 'yes';
        $storeBaseRecuring->special_allowance_name = $request->specialallowane;
        $storeBaseRecuring->save();
        return Reply::success(__('Base Component Saved Successfully!!'));
        abort(403);
    }
    public function storeVariablegComponent(Request $request){
        $storeVariable = new SalaryComponentVariable();
        $storeVariable->company_id = $this->user->company_id;
        $storeVariable->component_name = $request->component_name;
        $storeVariable->Payout_frequency = $request->component_type;
        $storeVariable->lop_dependent = $request->lop_dependent;
        $storeVariable->include_in_ctc = $request->include_in_ctc;
        $storeVariable->individual_override = $request->individual_override;
        $storeVariable->taxable = $request->taxable;
        $storeVariable->calc_type = $request->calc_type;
        $storeVariable->anual_amount = $request->anual_amount;
        $storeVariable->baded_on = $request->baded_on;
        $storeVariable->from = $request->from;
        $storeVariable->to = $request->to;
        $storeVariable->equal = $request->equal;
        $storeVariable->save();
        return Reply::success(__('messages.variableComponentSaveSuccess'));
        abort(403);
    }


    public function variable(){
        return view('admin.salary-structure.variable.index',$this->data);
    }

    public  function baseComponent(){
        return view('admin.salary-structure.base.index',$this->data);
    }

    public  function addVariable(){
        return view('admin.salary-structure.variable.add_variable',$this->data);
    }

    public function addhocEarning(){
        return view('admin.salary-structure.adhoc.create_adhoc_earning',$this->data);
    }

    public function addhocDiduction(){
        return view('admin.salary-structure.adhoc.create_adhoc_deduction',$this->data);
    }

    public function adhocEarningData(){
        // Getting View data
        $adhocearningdata = AdhocEarning::where('company_id',$this->user->company_id)->get();
        return DataTables::of($adhocearningdata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->component_name;
            })
            ->editColumn('taxable', function ($row)  {
                return $row->taxable;
            })
            ->editColumn('esiapplicable', function ($row)  {
                return $row->esi_applicable;
            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="assignRules('.$row->id.')"
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'taxable','esiapplicable','action'])
            ->make(true);
    }

    public function deleteAdhocEarning($id){
        $addhocearing = AdhocEarning::find($id);
        $addhocearing->delete();
    }

    public function adhocDiductionData(){
        // Getting View data
        $adhocdiductiondata = AdhocDeduction::where('company_id',$this->user->company_id)->get();
        return DataTables::of($adhocdiductiondata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->component_name;
            })
            ->editColumn('taxable', function ($row)  {
                return $row->remove_taxable;
            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="assignRules('.$row->id.')"
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'taxable','esiapplicable','action'])
            ->make(true);
    }

    public function deleteAdhocDeduction($id){
        $adhocdeduction = AdhocDeduction::find($id);
        $adhocdeduction->delete();
    }

    public function addNewAdhocEarning(){
        return view('admin.salary-structure.adhoc.create_new_adhoc_earning', $this->data)->render();
    }
    public function addNewAdhocDeduction(){
        return view('admin.salary-structure.adhoc.create_new_adhoc_deduction', $this->data)->render();
    }
    public function postAdhocEarning(Request $request){
        $store_AdhocEarning = new AdhocEarning();
        $store_AdhocEarning->company_id = $this->user->company_id;
        $store_AdhocEarning->component_name = $request->name;
        $store_AdhocEarning->taxable = $request->taxable;
        $store_AdhocEarning->esi_applicable = $request->esiapplicable;
        $store_AdhocEarning->save();
        return Reply::success(__('messages.adhocearningSaveSuccess'));
    }
    public function postAdhocDiduction(Request $request){
        $store_AdhocDiduction = new AdhocDeduction();
        $store_AdhocDiduction->company_id = $this->user->company_id;
        $store_AdhocDiduction->component_name = $request->name;
        $store_AdhocDiduction->remove_taxable = $request->taxable;
        $store_AdhocDiduction->save();
        return Reply::success(__('messages.adhocearningSaveSuccess'));
    }

    public function salaryStructure(){
        $single = array();
        $this->pfsettings = PfandEsiSettings::all();
        $this->allowances = RecurringComponent::all();
        $structures = SalaryStructureSettings::all();
        foreach ($structures as $key=>$structure){
            $single[] = explode(',',$structure->allowance);
        }
        $this->allallowances = array_unique(array_flatten($single));
        $this->salaryStructure = SalaryStructureSettings::where(['company_id'=>$this->user->company_id])->select('status')->first();
        return view('admin.salary-structure.salary_structure', $this->data);
    }

    public function addSalaryStructure(){
        $this->componets = RecurringComponent::where('company_id',$this->user->company_id)->select('id','component_name')->get();
        $this->variables = SalaryComponentVariable::where('company_id',$this->user->company_id)->select('id','component_name')->get();
        return view('admin.salary-structure.create_new_salary_structure',$this->data);
    }

    public function getStructureAmount(Request $request){
        $data = array();
        if(str_starts_with($request->id,'var')){
            $id = ltrim($request->id,'var');
            $data['amount']= Arr::pluck(SalaryComponentVariable::where('id',$id)->where('company_id',$this->user->company_id)->get(), 'anual_amount');
        }else{
           $data['amount'] = Arr::pluck(RecurringComponent::where('id',$request->id)->where('company_id',$this->user->company_id)->get(), 'anual_amount');
        }
        return json_encode($data);
    }

    public function storeStructure(Request $request){
        //$component = array_combine($request->allowance , $request->amount);
        $store_Structure = new SalaryStructureSettings();
        $store_Structure->company_id = $this->user->company_id;
        $store_Structure->structure_name = $request->name;
        $store_Structure->structure_desc = $request->description;
        $store_Structure->basic = $request->component_basic;
        $store_Structure->hra = $request->component_hra;
        $store_Structure->allowance = implode(',',$request->allowance);
        $store_Structure->amount = implode(',',$request->amount);
        $store_Structure->save();
        return Reply::success(__('messages.structureSaveSuccess'));
    }
    public function updateStructure(Request $request){

        //$component = array_combine($request->allowance , $request->amount);
        $store_Structure = SalaryStructureSettings::find($request->id);
        $store_Structure->company_id = $this->user->company_id;
        $store_Structure->structure_name = $request->name;
        $store_Structure->structure_desc = $request->description;
        $store_Structure->basic = $request->component_basic;
        $store_Structure->hra = $request->component_hra;
        $store_Structure->allowance = implode(',',$request->allowance);
        $store_Structure->amount = implode(',',$request->amount);
        $store_Structure->save();
        return Reply::success(__('messages.structureSaveSuccess'));
    }

    public function editStructure($id){
        $this->structure  = SalaryStructureSettings::find($id);
        $this->componets = RecurringComponent::where('company_id',$this->user->company_id)->select('id','component_name')->get();
        $this->variables = SalaryComponentVariable::where('company_id',$this->user->company_id)->select('id','component_name')->get();
        return view('admin.salary-structure.edit_salary_structure',$this->data);
    }

    public function structureData(){
        // Getting View data
        $adhocearningdata = SalaryStructureSettings::where('company_id',$this->user->company_id)->get();
        return DataTables::of($adhocearningdata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->structure_name;
            })
            ->editColumn('description', function ($row)  {

                return $row->structure_desc;
            })
            ->editColumn('basic', function ($row)  {

                return $row->basic;
            }) ->editColumn('hra', function ($row)  {

                return $row->hra;
            })
            ->addColumn('action', function ($row) {
                return '<a   href="' . route('admin.structure.editStructure', $row->id) . '" class="btn btn-info btn-circle"  
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'taxable','esiapplicable','action'])

            ->make(true);
    }

    public function structureRulesData(){

        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->join('teams','teams.id','=','employee_details.department_id')->select('employee.id','employee.name','teams.team_name','employee_details.address')->groupBy('employee.id')->get();

        $wordata = Employee::join('asign_structures','asign_structures.user_id','=','employee.id')
            ->join('salary_structure_settings','salary_structure_settings.id','=','asign_structures.rules_id')->select('asign_structures.user_id','salary_structure_settings.structure_name','asign_structures.effective_date')->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<input type="checkbox" class="sub_chk" data-id="'.$row->id.'" />';
            })->rawColumns(['checkbox'])
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })->editColumn('department', function ($row)  {

                return $row->team_name;
            })->editColumn('location', function ($row)  {

                return $row->address;

            })->editColumn('effective_date', function ($row) use($wordata)  {
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return Carbon::parse($work->effective_date)->format('Y-m-d');
                    }

                }

            })

            ->addColumn('rulesapplied', function ($row) use($wordata){
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->structure_name;
                    }

                }

            })
            ->rawColumns(['action','name','department','location','rulesapplied'])

            ->make(true);
    }

    public function assignStructure(Request $request){
        $this->ids = $request->ids;
        $this->rules = SalaryStructureSettings::where('company_id',$this->user->company_id)->get();
        return view('admin.salary-structure.assign_rules', $this->data)->render();
    }

    public function postAssignStructure(Request $request){
        $date = Carbon::parse($request->effective_date)->format('Y-m-d');
        $user_ids = explode(',', $request->ids);
        foreach ($user_ids as $usr_id){
            AsignStructure::updateOrCreate(
                ['user_id' => $usr_id],
                ['user_id' => $usr_id,'company_id' => $this->user->company_id,'rules_id' => $request->rules_id,'effective_date'=>$date]
            );
        }
        return Reply::success(__('Assigned Successfully!!'));
        abort(403);
    }
    public function contratorSalaryStructure(){
        return view('admin.salary-structure.contractor_salary_structure', $this->data);
    }

    public function addContractorSalaryStructure(){
        return view('admin.salary-structure.create_new_contractor_salary_structure',$this->data);
    }

    public function storeContractorStructure(Request $request){
        $store_Structure = new ContractSalaryStructure();
        $store_Structure->company_id = $this->user->company_id;
        $store_Structure->name = $request->name;
        $store_Structure->description = $request->description;
        $store_Structure->tds = $request->tds;
        $store_Structure->profesional_fee = $request->pf_name;
        $store_Structure->save();
        return Reply::success(__('messages.structureSaveSuccess'));
    }

    public function contractorStructureData(){
        // Getting View data
        $adhocearningdata = SalaryStructureSettings::where('company_id',$this->user->company_id)->get();
        return DataTables::of($adhocearningdata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->structure_name;
            })
            ->editColumn('description', function ($row)  {

                return $row->structure_desc;
            })
            ->editColumn('basic', function ($row)  {

                return $row->basic;
            }) ->editColumn('hra', function ($row)  {

                return $row->hra;
            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="assignRules('.$row->id.')"
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'taxable','esiapplicable','action'])

            ->make(true);
    }

    public function deleteContactSalary($id){
     
        $contact_salary = SalaryStructureSettings::find($id);
        $contact_salary->delete();
    }

    public function assignContractorStructure($id){
        $this->id = $id;
        $this->employees = User::all();
        return view('admin.salary-structure.assign_rules', $this->data)->render();
    }

    /*public function  postAssignRules(Request $request){

        $users_rules  = new RulesUser();
        $users_rules->user_id = $request->input('employee');
        $users_rules->rules_id = $request->input('id');
        $users_rules->save();
        return Reply::success(__('messages.attendanceSaveSuccess'));
        abort(403);
    }*/

    public function salaryStructureOverview(){

        return view('admin.salary-structure.overview', $this->data);
    }

    function array_flatten($array) {
        if (!is_array($array)) {
            return false;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result = array_merge($result, array($key => $value));
            }
        }
        return $result;
    }

    public function salaryStructureOverviewData(Request $request){
        $single = [];
        $allowances = RecurringComponent::all();
        $structures = SalaryStructureSettings::all();
        foreach ($structures as $key=>$structure){
            $single[] = explode(',',$structure->allowance);
        }
        $allallowances = array_unique(array_flatten($single));

        $pfesi = PfandEsiSettings::all();
        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
                    ->join('designations','designations.id','=','employee_details.designation_id')
                    ->select('employee.id','employee.name','employee_details.address','employee_details.monthly_rate','designations.name as dname')->get();

        $wordata = Employee::join('asign_structures','asign_structures.user_id','=','employee.id')
            ->join('salary_structure_settings','salary_structure_settings.id','=','asign_structures.rules_id')->select('asign_structures.user_id','salary_structure_settings.structure_name','salary_structure_settings.basic','salary_structure_settings.hra','asign_structures.effective_date','salary_structure_settings.allowance','salary_structure_settings.amount')->get();

        $array = [];
        $datatable = Datatables::of($users);
        $datatable->addIndexColumn();
        $datatable->editColumn('name', function ($row)  {
            return $row->name;
        });
        $datatable->editColumn('location', function ($row)  {
            return $row->address;

        });
        $datatable->editColumn('designation', function ($row)  {
            return $row->dname;
        });
        $datatable->editColumn('pan', function ($row)  {
                return 'NA';
            });
        $datatable->editColumn('pfnumber', function ($row)  {
                return 'NA';
            });

        $datatable->editColumn('salstructure', function ($row)use($wordata)  {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    return $work->structure_name;
                }

            }
            });
        $datatable->editColumn('saleffectivedate', function ($row) use($wordata) {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    return Carbon::parse($work->effective_date)->format('Y-m-d');
                }

            }
        });
        $datatable->editColumn('salarymodificationdate', function ($row)use($wordata)  {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    return Carbon::parse($work->effective_date)->format('Y-m-d');
                }

            }
            });
        $datatable->editColumn('ctc', function ($row) use($request) {
                if($request->periodtype == 'yearly'){
                    return $row->monthly_rate*12;
                }else{
                    return $row->monthly_rate;
                }

            });
        $datatable->editColumn('basic', function ($row) use($wordata,$request) {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    if($request->periodtype == 'yearly'){
                        return ((int)$row->monthly_rate*12)*(int)$work->basic/100;
                    }else{
                        return (int)$row->monthly_rate*(int)$work->basic/100;
                    }
                }
            }
            });
        $datatable->editColumn('hra', function ($row)use($wordata,$request)  {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    if($request->periodtype == 'yearly'){
                        return (((int)$row->monthly_rate*12)*(int)$work->basic/100*(int)$work->hra)/100;
                    }else{
                        return ((int)$row->monthly_rate*(int)$work->basic/100*(int)$work->hra)/100;
                    }
                }
            }
            });
        foreach($allowances as $allowance) {
            if (in_array($allowance->id,$allallowances)) {
                $datatable->addColumn($allowance->component_name, function ($row) use ($allowance, $array, $request, $allallowances) {
                    $array[] = (int)$allowance->anual_amount / 12;
                    if ($request->periodtype == 'yearly') {
                        return ceil((int)$allowance->anual_amount);
                    } else {
                        return ceil((int)$allowance->anual_amount / 12);
                    }
                    return 'NA';
                });
            }
        }
        $datatable->editColumn('pfemployer', function ($row) use($wordata,$pfesi,$request) {
            foreach ($wordata as $work) {
                if ($pfesi[0]->pf_applicable == 'on' && $row->monthly_rate >= $pfesi[0]->min_pf_calc_amount) {
                    if($request->periodtype == 'yearly'){
                        return ceil(((int)$row->monthly_rate*12) *(int)$work->basic/100 *(int)$pfesi[0]->custum_formula_percent) / 100;
                    }else{
                        return ceil((int)$row->monthly_rate *(int)$work->basic/100 *(int)$pfesi[0]->custum_formula_percent) / 100;
                    }
                }
            }
            return 'NA';
        });
        $datatable->editColumn('specialallow', function ($row) use($wordata,$pfesi,$array,$request) {
            foreach ($wordata as $work){
                if($row->id == $work->user_id){
                    if($request->periodtype == 'yearly'){
                        return  ((int)$row->monthly_rate*12) - (((int)$row->monthly_rate*(int)$work->basic/100 + ((int)$row->monthly_rate*(int)$work->basic/100*(int)$work->hra)/100 + ((int)$row->monthly_rate *(int)$work->basic/100 *(int)$pfesi[0]->custum_formula_percent) / 100) + array_sum($array));
                    }else{
                        return (int)$row->monthly_rate - (((int)$row->monthly_rate*(int)$work->basic/100 + ((int)$row->monthly_rate*(int)$work->basic/100*(int)$work->hra)/100 + ((int)$row->monthly_rate *(int)$work->basic/100 *(int)$pfesi[0]->custum_formula_percent) / 100) + array_sum($array));
                    }

                }
            }
                return 'NA';
            });

        $datatable->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="EditStructureOverview('.$row->id.')"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['action']);

        return $datatable->make(true);
    }

    public function editStructureOverview($id){
        $this->id = $id;
        $this->allowances = RecurringComponent::all();
        $this->employee = User::join('employee_details','employee_details.user_id','=','users.id')
            ->select('users.id','users.name','employee_details.monthly_rate')->where('users.id',$id)->first();
        $this->wordata = User::join('asign_structures','asign_structures.user_id','=','users.id')
            ->join('salary_structure_settings','salary_structure_settings.id','=','asign_structures.rules_id')->select('asign_structures.user_id','salary_structure_settings.structure_name','salary_structure_settings.basic','salary_structure_settings.hra','asign_structures.effective_date','salary_structure_settings.allowance','salary_structure_settings.amount')->where('users.id',$id)->first();
        return view('admin.salary-structure.editStructureOverview', $this->data)->render();
    }

    public function postStructureOverview(Request $request){
        $employeeDetail = EmployeeDetails::find('user_id',$request->id);
        $employeeDetail->monthly_rate = $request->ctc;
        $employeeDetail->save();
    }

    public  function updateStatus(Request $request){
        SalaryStructureSettings::where(['company_id'=>$this->user->company_id])->update(['status'=>$request->status]);
    }
}
