<?php

namespace App\Http\Controllers\Admin;

use App\AwardContractProducts;
use App\AwardedContracts;
use App\BoqCategory;
use App\ClientPayment;
use App\Contract;
use App\ContractInvoice;
use App\ContractInvoiceboq;
use App\ContractInvoicePayments;
use App\ContractInvoicePaymentsboq;
use App\ContractMeasurementSheet;
use App\CreditNotes;
use App\Currency;
use App\Employee;
use App\Estimate;
use App\Helper\Reply;
use App\Http\Requests\InvoiceFileStore;
use App\Http\Requests\Invoices\StoreInvoice;
use App\Invoice;
use App\Invoiceboq;
use App\InvoiceItems;
use App\InvoicePayments;
use App\InvoicePaymentsboq;
use App\InvoiceSetting;
use App\MeasurementSheet;
use App\MeasurementType;
use App\Notifications\NewInvoice;
use App\Notifications\OfflineInvoicePaymentAccept;
use App\Notifications\OfflineInvoicePaymentReject;
use App\Notifications\PaymentReminder;
use App\OfflineInvoicePayment;
use App\OfflinePaymentMethod;
use App\Product;
use App\Project;
use App\ProjectCostItemsProduct;
use App\Proposal;
use App\Tax;
use App\Title;
use App\Type;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Invoices\UpdateInvoice;
use App\ProjectMilestone;

class ManageContractInvoicesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.contractinvoices';
        $this->pageIcon = 'ti-receipt';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('invoices', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id',$projectlist)->get();
        $this->contractors = Employee::getAllContractors($user);
        return view('admin.contract-invoices.index', $this->data);
    }
    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function data(Request $request)
    {
        $user = $this->user;
        $firstInvoice = ContractInvoice::orderBy('id', 'desc')->first();
        $invoices = ContractInvoice::select('id', 'project_id', 'contractor_id',  'invoice_number',   'total', 'status', 'issue_date');


        if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
            $invoices = $invoices->where(DB::raw('DATE(contract_invoices.`issue_date`)'), '>=', $request->startDate);
        }

        if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
            $invoices = $invoices->where(DB::raw('DATE(contract_invoices.`issue_date`)'), '<=', $request->endDate);
        }

        if ($request->status != 'all' && !is_null($request->status)) {
            $invoices = $invoices->where('contract_invoices.status', '=', $request->status);
        }

        if ($request->projectID != 'all' && !is_null($request->projectID)) {
            $invoices = $invoices->where('contract_invoices.project_id', '=', $request->projectID);
        }

        if ($request->contractorID != 'all' && !is_null($request->contractorID)) {
            $invoices = $invoices->where('contract_invoices.contractor_id', '=', $request->contractorID);
        }

        $invoices = $invoices->whereHas('project', function($q) {
            $q->whereNull('deleted_at');
        }, '>=', 0)->orderBy('contract_invoices.id', 'desc')->get();

        return DataTables::of($invoices)
            ->addIndexColumn()
            ->addColumn('action', function ($row)  use ($user) {
                $action = '<a href="' . route("admin.contract-invoices.record-payments", $row->id) . '" data-toggle="tooltip" title="Record Payment" class="btn btn-warning"><i class="fa fa-cogs"></i></a>';
                $action .= '&nbsp;';
                $action .= '<a href="' . route("admin.contract-invoices.edit", $row->id) . '" data-toggle="tooltip" title="Edit Invoice" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                $action .= '&nbsp;';
                $action .= '<a href="javascript:;" data-toggle="tooltip" title="Delete Invoice"  data-invoice-id="' . $row->id . '" class="btn btn-danger sa-params"><i class="fa fa-trash"></i></a>';
                $actionold = '<div class="btn-group m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info btn-outline  dropdown-toggle waves-effect waves-light" type="button">'.__('app.action').' <span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu">';
                if ($row->status == 'paid') {
                  /*  $action .= '<li><a href="' . route("admin.contract-invoices.download", $row->id) . '"><i class="fa fa-download"></i> '.__('app.download').'</a></li>';
                     $action .= ' <li><a href="javascript:" data-invoice-id="' . $row->id . '" class="invoice-upload" data-toggle="modal" data-target="#invoiceUploadModal"><i class="fa fa-upload"></i> '.__('app.upload').' </a></li>';*/
                }

                if ($row->status != 'paid') {
                    $actionold .= '<li><a href="' . route("admin.contract-invoices.edit", $row->id) . '"><i class="fa fa-pencil"></i> '.__('app.edit').'</a></li>';

                    /*if (in_array('payments', $this->user->modules) && $row->credit_note == 0 && $row->total > 0) {
                        $action .= '<li><a href="' . route("admin.payments.payInvoice", [$row->id]) . '" data-toggle="tooltip" ><i class="fa fa-plus"></i> ' . __('modules.payments.addPayment') . '</a></li>';
                        $action .= '<li><a href="' . route("front.invoice", [md5($row->id)]) . '" target="_blank" data-toggle="tooltip" ><i class="fa fa-link"></i> ' . __('modules.payments.paymentLink') . '</a></li>';

                    }*/
                }
                $actionold .= '<li><a href="javascript:;" data-toggle="tooltip"  data-invoice-id="' . $row->id . '" class="sa-params"><i class="fa fa-times"></i> '.__('app.delete').'</a></li>';


                $actionold .= '</ul>
              </div>';

                return $action;
            })
            ->editColumn('contractor', function ($row) {
                if (!is_null($row->contractor_id)) {
                    return  ucfirst(get_user_name($row->contractor_id));
                }
                else {
                    return "--";
                }
            })
            ->editColumn('project_name', function ($row) {
                if (!is_null($row->project_id)) {
                    return  ucfirst(get_project_name($row->project_id));
                }
                else {
                    return "--";
                }
            })
            ->addColumn('unique_id', function ($row) {
                return '<a href="' . route('admin.contract-invoices.show', $row->id) . '">' . ucfirst($row->unique_id) . '</a>';
            })
            ->editColumn('status', function ($row) {

                if ($row->status == 'unpaid') {
                    return '<label class="label label-danger">' . strtoupper($row->status) . '</label>';
                } elseif ($row->status == 'paid') {
                    return '<label class="label label-success">' . strtoupper($row->status) . '</label>';
                } elseif($row->status == 'review') {
                    return '<label class="label label-warning">' . strtoupper($row->status) . '</label>';
                } elseif($row->status == 'canceled') {
                    return '<label class="label label-danger">' . strtoupper($row->status) . '</label>';
                }else {
                    return '<label class="label label-info">' . strtoupper(__('modules.invoices.partial')) . '</label>';
                }
            })
            ->editColumn('total', function ($row) {
                $currencySymbol = $row->currency_symbol;
                return '<div class="text-right">Total: '.$currencySymbol.$row->total.'<br>Paid: '.$currencySymbol.$row->amountPaid().'<br>Due: '.$currencySymbol.$row->amountDue().'</div>';
            })
            ->editColumn(
                'issue_date',
                function ($row) {
                    return $row->issue_date->timezone($this->global->timezone)->format($this->global->date_format);
                }
            )
            ->rawColumns(['project_name',  'contractor', 'action', 'status', 'unique_id', 'total'])
            ->removeColumn('currency_symbol')
            ->removeColumn('currency_code')
            ->removeColumn('project_id')
            ->make(true);
    }
    public function amtCalculation(Request $request){
        $qty = $request->qty;
        $invoicetoqut = $request->invoicetoqut;
        $rateval = $request->rateval;
        $totalqty = $request->qtyval;
        $totalamount = 0;
        $calarray = array();
        $calqty = $qty+$invoicetoqut;
        if($calqty>$totalqty){
            $qty = $totalqty-$invoicetoqut;
            $qtytotal = $qty;
        }else{
            $qtytotal = $qty+$invoicetoqut;
        }
        $totalamount = $rateval*$totalqty;
        $pramount = $qty*$rateval;
        $percentage = (($totalamount-$pramount)/$totalamount)*100;
        $calarray['qty'] = $qty;
        $calarray['amount'] = numberformat($pramount);
        $calarray['totalamount'] = numberformat($totalamount);
        $calarray['qtytotal'] = $qtytotal;
        $calarray['amttotal'] = $qtytotal*$rateval;
        $calarray['percentage'] = 100-$percentage;
        return $calarray;
    }
    public function domPdfObjectForDownload($id)
    {
        $this->invoice = ContractInvoice::findOrFail($id);
        $this->paidAmount = $this->invoice->getPaidAmount();
        $this->creditNote = 0;
        if ($this->invoice->credit_note) {
            $this->creditNote = CreditNotes::where('invoice_id', $id)
                ->select('cn_number')
                ->first();
        }

        if ($this->invoice->discount > 0) {
            if ($this->invoice->discount_type == 'percent') {
                $this->discount = (($this->invoice->discount / 100) * $this->invoice->sub_total);
            } else {
                $this->discount = $this->invoice->discount;
            }
        } else {
            $this->discount = 0;
        }

        $taxList = array();

        $items = InvoiceItems::whereNotNull('taxes')
            ->where('invoice_id', $this->invoice->id)
            ->get();

        foreach ($items as $item) {
            foreach (json_decode($item->taxes) as $tax){
                $this->tax = InvoiceItems::taxbyid($tax)->first();
                if ($this->tax){
                    if (!isset($taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'])) {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = ($this->tax->rate_percent / 100) * $item->amount;
                    } else {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] + (($this->tax->rate_percent / 100) * $item->amount);
                    }
                }
            }
        }

        $this->taxes = $taxList;

        $this->settings = $this->global;

        $this->invoiceSetting = InvoiceSetting::first();
        //        return view('invoices.'.$this->invoiceSetting->template, $this->data);

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('invoices.' . $this->invoiceSetting->template, $this->data);
        $filename = $this->invoice->invoice_number;

        return [
            'pdf' => $pdf,
            'fileName' => $filename
        ];
    }

    public function download($id)
    {

        $this->invoice = ContractInvoice::findOrFail($id);

        // Download file uploaded
        if ($this->invoice->file != null) {
            return response()->download(storage_path('app/invoice-files') . '/' . $this->invoice->file);
        }

        $pdfOption = $this->domPdfObjectForDownload($id);
        $pdf = $pdfOption['pdf'];
        $filename = $pdfOption['fileName'];

        return $pdf->download($filename . '.pdf');
    }

    public function destroy($id)
    {
            ContractInvoice::destroy($id);
            ContractInvoicePayments::where('invoice_id',$id)->delete();
            ContractInvoicePaymentsboq::where('invoice_id',$id)->delete();
            ContractInvoiceboq::where('invoice_id',$id)->delete();
            return Reply::success(__('messages.invoiceDeleted'));

    }
    public function PaymentDestory($id)
    {

        $invoicepayment = ContractInvoicePayments::where('id',$id)->first();

        ContractInvoicePayments::where('id',$id)->delete();
        ContractInvoicePaymentsboq::where('invoice_payments_id',$id)->delete();

        $invoice = ContractInvoice::where('id',$invoicepayment->invoice_id)->first();
        $invoiceboqtotalamt = ContractInvoicePaymentsboq::where('invoice_id',$invoice->id)->sum('amount');
        if($invoice->total!=$invoiceboqtotalamt){
            $invoice->status = 'unpaid';
            $invoice->save();
            InvoicePayments::where('invoice_id',$invoice->id)->update(['status'=>'unpaid']);
        }
        return Reply::redirect(route('admin.contract-invoices.show',$invoice->id), __('messages.invoiceCreated'));
    }
    public function create()
    {
        $user = $this->user;
        $countinvoice = ContractInvoice::where('company_id',$user->company_id)->orderBy('maxinc','desc')->first();

        $newcountinvoice = !empty($countinvoice) ? $countinvoice->maxinc+1 : 1;
        $prlist = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id',$prlist)->get();
        $this->currencies = Currency::all();
        $this->lastInvoice = $newcountinvoice;
        $this->invoiceSetting = InvoiceSetting::first();
        $this->zero = '';
        if (strlen($this->lastInvoice) < $this->invoiceSetting->invoice_digit) {
            for ($i = 0; $i < $this->invoiceSetting->invoice_digit - strlen($this->lastInvoice); $i++) {
                $this->zero = '0' . $this->zero;
            }
        }
        $this->taxes = Tax::all();
        $this->products = Product::all();
        $this->contractors = Employee::getAllContractors($user);
        $invoicesessid = \request()->session()->get('contractinvoicesessionid');
        if(empty($invoicesessid)){
            $sess = uniqid();
            $invoicesessid =  \request()->session()->put('contractinvoicesessionid',$sess);
        }
        return view('admin.contract-invoices.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = $this->user;
        $project_id = $request->project_id;
        if(!empty($project_id)){
            $projectdetails = Project::find($project_id);
            $companyid = $projectdetails->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $tasksarray = $request->taskid;
        $quantityarray = $request->quantity;
        $percentagearray = $request->percentage;
        $invoicesessid = \request()->session()->get('contractinvoicesessionid');
        if(!empty($tasksarray)){

        $invoice = new ContractInvoice();
            $invoice->company_id = $companyid;
        $invoice->added_by = $user->id;
        $invoice->project_id = $request->project_id ?: '';
        $invoice->subproject_id = $request->subproject_id ?: '0';
        $invoice->contractor_id = $request->contractor ?: '0';
        $invoice->awarded_contract_id = $request->awarded_contract ?: '0';

            $countinvoice = ContractInvoice::where('company_id',$user->company_id)->orderBy('maxinc','desc')->pluck('maxinc')->toArray();
            $countinvoice = count($countinvoice);
            $newcountinvoice = !empty($countinvoice) ? $countinvoice+1 : 1;
        $invoice->invoice_number = $newcountinvoice;
        $invoice->issue_date = !empty($request->issue_date) ? date('Y-m-d',strtotime($request->issue_date)) : '';
        $invoice->due_date = !empty($request->due_date) ? date('Y-m-d',strtotime($request->due_date)) : '';
        $invoice->total =  numberformat($request->total);
        $invoice->note = $request->note ?: '';
        $invoice->status =   'unpaid';
        $invoice->save();

            foreach ($tasksarray as $tasks) {
                $percent = 100;
                if (!empty($quantityarray[$tasks])) {
                    $quantity = $quantityarray[$tasks];
                    $percent = !empty($percentagearray[$tasks]) ? str_replace('%','',$percentagearray[$tasks]) : 0;
                    $taskproduct = AwardContractProducts::find($tasks);
                    $totalamt = $taskproduct->rate * $taskproduct->qty;
                    $qtyamount = $taskproduct->rate * $quantity;
                        /*$previnvoice = \App\Invoiceboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
                            \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"), \Illuminate\Support\Facades\DB::raw("SUM(quantity) as invquantity"),
                        ])->where('product_id', $tasks);
                        $previnvoice = $previnvoice->first();*/
                    if (!empty($percent) && !empty($qtyamount)) {
                        $invoiceboq = new ContractInvoiceboq();
                        $invoiceboq->invoice_id = $invoice->id;
                        $invoiceboq->product_id = $tasks;
                        $invoiceboq->quantity = $quantity;
                        $invoiceboq->percent = numberformat($percent);
                        $invoiceboq->amount = numberformat($qtyamount);
                        $invoiceboq->save();

                        ContractMeasurementSheet::where('product_id',$tasks)->where('session_id',$invoicesessid)->update(['invoice_id'=>$invoice->id]);
                    }
                }
            }
            return Reply::redirect(route('admin.contract-invoices.index'), __('messages.invoiceCreated'));
        }
        return Reply::error( __('messages.selectTasks'));
    }

    public function remindForPayment($taskID)
    {
        $invoice = ContractInvoice::with(['project','project.client'])->findOrFail($taskID);
        // Send  reminder notification to user

        $userClient = User::findOrFail($invoice->project ? $invoice->project->client->user_id : $invoice->client_id);
        $notifyUser = $userClient;
        $notifyUser->notify(new PaymentReminder($invoice));

        return Reply::success('messages.reminderMailSuccess');
    }

    public function edit($id)
    {
        $invoice = ContractInvoice::findOrFail($id);
        $this->invoice = $invoice;
        $user = $this->user;
        $prlist = explode(',',$user->projectlist);
        $this->projectsarray = Project::whereIn('id',$prlist)->get();
        $this->subprojectsarray = Title::where('project_id',$invoice->project_id)->get();
        $this->invoiceSetting = InvoiceSetting::first();
        $this->awardedcontractsarray = AwardedContracts::where('contractor_id',$invoice->contractor_id)->where('project_id',$invoice->project_id)->where('subproject_id',$invoice->subproject_id)->get();
       /* $this->zero = '';
        if (strlen($this->lastInvoice) < $this->invoiceSetting->invoice_digit) {
            for ($i = 0; $i < $this->invoiceSetting->invoice_digit - strlen($this->lastInvoice); $i++) {
                $this->zero = '0' . $this->zero;
            }
        }*/
        $this->taxes = Tax::all();
        $this->products = Product::all();
        $this->contractorsarray = Employee::getAllContractors($user);
        return view('admin.contract-invoices.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = $this->user;
        $tasksarray = $request->taskid;
        $quantityarray = $request->quantity;
        $percentagearray = $request->percentage;
        if(!empty($tasksarray)){

            $invoice =  ContractInvoice::find($id);
            $invoice->project_id = $request->project_id ?: '';
            $invoice->subproject_id = $request->subproject_id ?: '0';
            $invoice->contractor_id = $request->contractor ?: '0';
            $invoice->awarded_contract_id = $request->awarded_contract ?: '0';
            $invoice->issue_date = !empty($request->issue_date) ? date('Y-m-d',strtotime($request->issue_date)) : '';
            $invoice->due_date = !empty($request->due_date) ? date('Y-m-d',strtotime($request->due_date)) : '';
            $invoice->total = round($request->total, 2) ?: '';
            $invoice->note = $request->note ?: '';
            $invoice->save();
            ContractInvoiceboq::where('invoice_id',$id)->delete();
            foreach ($tasksarray as $tasks) {
                $percent = 100;
                if (!empty($quantityarray[$tasks])) {
                    $quantity = $quantityarray[$tasks];
                    $percent = !empty($percentagearray[$tasks]) ? str_replace('%','',$percentagearray[$tasks]) : 0;
                    $taskproduct = AwardContractProducts::find($tasks);
                    $totalamt = $taskproduct->rate * $taskproduct->qty;
                    $qtyamount = $taskproduct->rate * $quantity;
                    /*$previnvoice = \App\Invoiceboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
                        \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"), \Illuminate\Support\Facades\DB::raw("SUM(quantity) as invquantity"),
                    ])->where('product_id', $tasks);
                    $previnvoice = $previnvoice->first();*/
                    if (!empty($percent) && !empty($qtyamount)) {
                        $invoiceboq = new ContractInvoiceboq();
                        $invoiceboq->invoice_id = $invoice->id;
                        $invoiceboq->product_id = $tasks;
                        $invoiceboq->quantity = $quantity;
                        $invoiceboq->percent = numberformat($percent);
                        $invoiceboq->amount = numberformat($qtyamount);
                        $invoiceboq->save();

                        ContractMeasurementSheet::where('product_id',$tasks)->update(['invoice_id'=>$invoice->id]);
                    }
                }
            }

            return Reply::redirect(route('admin.contract-invoices.index'), __('messages.invoiceUpdated'));
        }
        return Reply::redirect(route('admin.contract-invoices.edit',$id), __('messages.selectTasks'));
    }

    public function show($id)
    {
        $invoice = ContractInvoice::findOrFail($id);
        $this->invoice = $invoice;
        $this->settings = $this->company;
        $this->invoiceSetting = InvoiceSetting::first();
        $this->invoiceboqarray = ContractInvoiceboq::join('awarded_contracts_products','awarded_contracts_products.id','=','contract_invoices_boq.product_id')
                            ->join('cost_items','cost_items.id','=','awarded_contracts_products.cost_items')
                           ->select('contract_invoices_boq.*','awarded_contracts_products.qty','cost_items.cost_item_name','awarded_contracts_products.rate','awarded_contracts_products.finalrate')
                            ->where('contract_invoices_boq.invoice_id',$invoice->id)->get();
        $this->invoicepaymentsarray = ContractInvoicePayments::where('invoice_id',$invoice->id)->get();
        return view('admin.contract-invoices.show', $this->data);
    }
    public function invoicePaymentLoop(Request $request){
        $percenttype = $request->percenttype ?: '';
        $percentvalue = $request->percentvalue ?: 0;
        $invoiceid = $request->invoiceid ?: '';
        $user = $this->user;
        $this->percenttype =  $percenttype;
        $this->percentvalue = (int)$percentvalue;
        $this->invoiceid = (int)$invoiceid;
        $messageview = View::make('admin.contract-invoices.invoice-payment-loop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function recordPayments($id){
        $invoice = ContractInvoice::findOrFail($id);
        $this->invoice = $invoice;
        $this->settings = $this->company;
        $this->invoiceSetting = InvoiceSetting::first();
        $this->invoiceboqarray = ContractInvoiceboq::join('awarded_contracts_products','awarded_contracts_products.id','=','contract_invoices_boq.product_id')
            ->join('cost_items','cost_items.id','=','awarded_contracts_products.cost_items')
            ->select('contract_invoices_boq.*','awarded_contracts_products.cost_items','cost_items.cost_item_name')
            ->where('contract_invoices_boq.invoice_id',$invoice->id)->get();
        $previnvoice = \App\ContractInvoicePaymentsboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
            \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"),
        ])->where('invoice_id',$id)->first();
        $paidpercent = 0;
        if(!empty($previnvoice->invpercent)&&!empty($previnvoice->invamount)){
            $paidamount = $previnvoice->invamount;
            $paidpercent = $previnvoice->invpercent;
        }
        $paidpercent = 100-$paidpercent;
        $this->paidpercent = $paidpercent;
        return view('admin.contract-invoices.record-payments', $this->data);
    }
    public function recordPaymentsEdit($id){
        $invoicepayment = ContractInvoicePayments::find($id);
        $invoice = ContractInvoice::findOrFail($invoicepayment->invoice_id);
        $this->invoicepayment = $invoicepayment;
        $this->invoice = $invoice;
        $this->settings = $this->company;
        $this->invoiceSetting = InvoiceSetting::first();
        $this->invoiceboqarray = ContractInvoiceboq::join('awarded_contracts_products','awarded_contracts_products.id','=','contract_invoices_boq.product_id')
            ->join('cost_items','cost_items.id','=','awarded_contracts_products.cost_items')
            ->select('contract_invoices_boq.*','awarded_contracts_products.cost_items','cost_items.cost_item_name')
            ->where('contract_invoices_boq.invoice_id',$invoice->id)->get();

        $previnvoice = \App\ContractInvoicePaymentsboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
            \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"),
        ])->where('invoice_id',$id)->first();
        $paidpercent = 0;
        if(!empty($previnvoice->invpercent)&&!empty($previnvoice->invamount)){
            $paidamount = $previnvoice->invamount;
            $paidpercent = $previnvoice->invpercent;
        }
        $paidpercent = 100-$paidpercent;
        $this->paidpercent = $paidpercent;

        return view('admin.contract-invoices.record-payments-edit', $this->data);
    }
    public function invoiceTaskLoop(Request $request){
        $percenttype = $request->percenttype ?: '';
        $percentvalue = $request->percentvalue ?: 0;
        $invoiceid = $request->invoiceid ?: '';
        $invoicepaymentid = $request->invoicepaymentid ?: '';
        $user = $this->user;
        $this->percenttype =  $percenttype;
        $this->percentvalue = (int)$percentvalue;
        $this->invoiceid = (int)$invoiceid;
        $this->invoicepaymentid = (int)$invoicepaymentid;
        $messageview = View::make('admin.contract-invoices.invoice-task-loop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function invoiceRecordPaymentPost(Request $request){
             $user = $this->user;
         $invoiceid = $request->invoiceid;
         $invoicepaymentid = $request->invoicepaymentid;
        $percentvalue = $request->percentvalue;
        if($percentvalue>100){
            $percentvalue = 100;
        }
         if(!empty($invoicepaymentid)){
             $invoicepayments =ContractInvoicePayments::find($invoicepaymentid);
         }else{
             $invoicepayments = new ContractInvoicePayments();
         }

        $invoicepayments->company_id = $user->company_id;
        $invoicepayments->added_by = $user->id;
        $invoicepayments->invoice_id = $invoiceid;
        $invoicepayments->payment_mode = $request->paymentmode;
        $invoicepayments->payment_date = $request->payment_date;
        $invoicepayments->note = $request->note;
        $invoicepayments->percenttype = $request->percenttype;
        $invoicepayments->percentvalue = $percentvalue;
        $invoicepayments->status = 'unpaid';
        $invoicepayments->save();
        $totalamount = 0;
        $tasksarray = $request->taskid;
        if(!empty($tasksarray)){
            if(!empty($invoicepaymentid)){
              ContractInvoicePaymentsboq::where('invoice_id',$invoiceid)->where('invoice_payments_id',$invoicepaymentid)->delete();
            }
            foreach ($tasksarray as $tasks){
                $taskproduct = ContractInvoiceboq::find($tasks);
                $percent = 100;
                $amt = $taskproduct->amount;
                if($request->percenttype=='percent'){
                    $percent = $invoicepayments->percentvalue;
                    $amt = ($percent/100)*$amt;
                }elseif($request->percenttype=='custom'){
                    $percent = !empty($request->invoiceboqpercent[$tasks]) ? $request->invoiceboqpercent[$tasks] : '';
                    $amt = !empty($request->invoiceboqamt[$tasks]) ? $request->invoiceboqamt[$tasks] : '';
                }
                if(!empty($percent)&&!empty($amt)){
                    $invoiceboq = new ContractInvoicePaymentsboq();
                    $invoiceboq->invoice_id = $invoiceid;
                    $invoiceboq->invoice_payments_id = $invoicepayments->id;
                    $invoiceboq->invoice_boq_id = $tasks;
                    $invoiceboq->percent = $percent;
                    $invoiceboq->amount = $amt;
                    $invoiceboq->save();
                    $totalamount += $amt;
                }
            }
        }

        $invoicepayments->totalamount = $totalamount;
        $invoicepayments->save();

        $invoice = ContractInvoice::where('id',$invoiceid)->first();
        $invoiceboqtotalamt = ContractInvoicePaymentsboq::where('invoice_id',$invoiceid)->sum('amount');
        if($invoice->total==$invoiceboqtotalamt){
            $invoice->status = 'paid';
            $invoice->save();
            ContractInvoicePayments::where('invoice_id',$invoiceid)->update(['status'=>'paid']);
        }
        return Reply::redirect(route('admin.contract-invoices.show',$invoiceid), __('messages.invoiceUpdated'));
    }
    public function appliedCredits(Request $request, $id)
    {
        $this->invoice = ContractInvoice::findOrFail($id);

        $this->creditNotes = $this->invoice->credit_notes()->orderBy('date', 'DESC')->get();

        return view('admin.contract-invoices.applied_credits', $this->data);
    }

    public function deleteAppliedCredit(Request $request, $id)
    {
        $this->invoice = ContractInvoice::findOrFail($request->invoice_id);

        // delete from credit_notes_invoice_table
        $invoiceCreditNote = $this->invoice->credit_notes()->wherePivot('id', $id);
        $creditNote = $invoiceCreditNote->first();
        $invoiceCreditNote->detach();

        // change invoice status
        $this->invoice->status = 'partial';
        if ($this->invoice->amountPaid() == $this->invoice->total) {
            $this->invoice->status = 'paid';
        }
        if ($this->invoice->amountPaid() == 0) {
            $this->invoice->status = 'unpaid';
        }
        $this->invoice->save();

        // change credit note status
        if ($creditNote->status == 'closed') {
            $creditNote->status = 'open';
            $creditNote->save();
        }

        $this->creditNotes = $this->invoice->credit_notes()->orderBy('date', 'DESC')->get();
        if ($this->creditNotes->count() > 0) {
            $view = view('admin.contract-invoices.applied_credits', $this->data)->render();

            return Reply::successWithData(__('messages.creditedInvoiceDeletedSuccessfully'), ['view' => $view]);
        }
        return Reply::redirect(route('admin.contract-invoices.show', [$this->invoice->id]), __('messages.creditedInvoiceDeletedSuccessfully'));
    }

    public function convertEstimate($id)
    {
        $user = $this->user;
        $this->estimateId = $id;
        $this->invoice = Estimate::with('items')->findOrFail($id);

        $countinvoice = ContractInvoice::where('company_id',$user->company_id)->orderBy('maxinc','desc')->pluck('maxinc');
        $newcountinvoice = $countinvoice+1;
        $this->lastInvoice = $countinvoice;
        $this->invoiceSetting = InvoiceSetting::first();
        $this->projects = Project::all();
        $this->currencies = Currency::all();
        $this->taxes = Tax::all();
        $this->products = Product::all();
        $this->zero = '';
        if (strlen($this->lastInvoice) < $this->invoiceSetting->invoice_digit) {
            for ($i = 0; $i < $this->invoiceSetting->invoice_digit - strlen($this->lastInvoice); $i++) {
                $this->zero = '0' . $this->zero;
            }
        }
        //        foreach ($this->invoice->items as $items)

        $discount = $this->invoice->items->filter(function ($value, $key) {
            return $value->type == 'discount';
        });

        $tax = $this->invoice->items->filter(function ($value, $key) {
            return $value->type == 'tax';
        });

        $this->totalTax = $tax->sum('amount');
        $this->totalDiscount = $discount->sum('amount');

        return view('admin.contract-invoices.convert_estimate', $this->data);
    }

    public function convertProposal($id)
    {
        $this->invoice = Proposal::findOrFail($id);
        $this->lastInvoice = ContractInvoice::withoutGlobalScope('company')->orderBy('id', 'desc')->first();
        $this->invoiceSetting = InvoiceSetting::first();
        $this->projects = Project::all();
        $this->currencies = Currency::all();
        return view('admin.contract-invoices.convert_estimate', $this->data);
    }

    public function addItems(Request $request)
    {
        $this->items = Product::with('tax')->find($request->id);
        $this->taxes = Tax::all();
        $view = view('admin.contract-invoices.add-item', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }


    public function paymentDetail($invoiceID)
    {
        $this->invoice = ContractInvoice::with('payment', 'currency')->findOrFail($invoiceID);

        return View::make('admin.contract-invoices.payment-detail', $this->data);
    }

    /**
     * @param InvoiceFileStore $request
     * @return array
     */
    public function storeFile(InvoiceFileStore $request)
    {
        $invoiceId = $request->invoice_id;
        $file = $request->file('file');

        $newName = $file->hashName(); // setting hashName name
        // Getting invoice data
        $invoice = ContractInvoice::find($invoiceId);

        if ($invoice != null) {

            if ($invoice->file != null) {
                unlink(storage_path('app/invoice-files') . '/' . $invoice->file);
            }

            $file->move(storage_path('app/invoice-files'), $newName);

            $invoice->file = $newName;
            $invoice->file_original_name = $file->getClientOriginalName(); // Getting uploading file name;

            $invoice->save();

            return Reply::success(__('messages.fileUploadedSuccessfully'));
        }

        return Reply::error(__('messages.fileUploadIssue'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function destroyFile(Request $request)
    {
        $invoiceId = $request->invoice_id;

        $invoice = ContractInvoice::find($invoiceId);

        if ($invoice != null) {

            if ($invoice->file != null) {
                unlink(storage_path('app/invoice-files') . '/' . $invoice->file);
            }

            $invoice->file = null;
            $invoice->file_original_name = null;

            $invoice->save();
        }

        return Reply::success(__('messages.fileDeleted'));
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $status
     * @param $projectID
     */
    public function export($startDate, $endDate, $status, $projectID)
    {

        $invoices = ContractInvoice::with(['project:id,project_name', 'currency:id,currency_symbol']);

        if ($startDate !== null && $startDate != 'null' && $startDate != '') {
            $invoices = $invoices->where(DB::raw('DATE(contract_invoices.`issue_date`)'), '>=', $startDate);
        }

        if ($endDate !== null && $endDate != 'null' && $endDate != '') {
            $invoices = $invoices->where(DB::raw('DATE(contract_invoices.`issue_date`)'), '<=', $endDate);
        }

        if ($status != 'all' && !is_null($status)) {
            $invoices = $invoices->where('contract_invoices.status', '=', $status);
        }

        if ($projectID != 'all' && !is_null($projectID)) {
            $invoices = $invoices->where('contract_invoices.project_id', '=', $projectID);
        }

        $invoices = $invoices->orderBy('id', 'desc')
        ->get()
        ->map(function($invoice) {
            return [
                'id' => $invoice->id,
                'invoice_number' => $invoice->invoice_number,
                'project_name' => $invoice->project->project_name,
                'status' => $invoice->status,
                'total' => $invoice->currency->currency_symbol.$invoice->total,
                'amount_used' => $invoice->currency->currency_symbol.$invoice->amountPaid(),
                'amount_remaining' => $invoice->currency->currency_symbol.$invoice->amountDue(),
                'issue_date' => $invoice->issue_date ? $invoice->issue_date->format($this->global->date_format) : ''
            ];
        })->toArray();

        // Define the Excel spreadsheet headers
        $headerRow = ['ID', 'Invoice #', 'Project Name', 'Status', 'Total Amount', 'Amount Paid', 'Amount Due', 'Invoice Date'];

        array_unshift($invoices, $headerRow);

        // Generate and return the spreadsheet
        Excel::create('invoice', function ($excel) use ($invoices) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Invoice');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('invoice file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($invoices) {
                $sheet->fromArray($invoices, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function getClient($projectID)
    {
        $companyName = Project::with('client')->find($projectID);
        return $companyName->client->company_name;
    }

    public function getClientOrCompanyName($projectID = '')
    {
        $this->projectID = $projectID;

        if($projectID == '')
        {
            $this->clients = User::allClients();
        } else {
            $companyName = Project::where('id', $projectID)->with('clientdetails')->first();
            $this->companyName = $companyName->clientdetails ? $companyName->clientdetails->company_name : '';
        }

        $list = view('admin.contract-invoices.client_or_company_name', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }


    public function convertMilestone($id)
    {
        $this->invoice = ProjectMilestone::findOrFail($id);
        $this->lastInvoice = ContractInvoice::orderBy('id', 'desc')->first();
        $this->invoiceSetting = InvoiceSetting::first();
        $this->lastInvoice = ContractInvoice::count()+1;
        $this->projects = Project::all();
        $this->currencies = Currency::all();
        $this->taxes = Tax::all();
        $this->products = Product::all();
        $this->zero = '';
        if (strlen($this->lastInvoice) < $this->invoiceSetting->invoice_digit) {
            for ($i = 0; $i < $this->invoiceSetting->invoice_digit - strlen($this->lastInvoice); $i++) {
                $this->zero = '0' . $this->zero;
            }
        }
        return view('admin.contract-invoices.convert_milestone', $this->data);
    }

    public function verifyOfflinePayment($id)
    {
        $this->invoice = ContractInvoice::with('offline_invoice_payment','offline_invoice_payment.payment_method')->findOrFail($id);
        return view('admin.contract-invoices.verify-payment-detail', $this->data);
    }

    public function verifyPayment(Request $request, $id)
    {
        $offlineRequest = OfflineInvoicePayment::findOrFail($id);
        $invoice = ContractInvoice::findOrFail($offlineRequest->invoice_id);

        // Change the status of payment request
        $offlineRequest->status = 'approve';
        $offlineRequest->save();

        // change the status of payment to paid
        $payment = ClientPayment::where('invoice_id', $invoice->id)->where('status', 'pending')->first();
        $payment->status = 'complete';
        $payment->save();

        //Change the status of invoice
        $invoice->status = 'paid';
        $invoice->save();


        $client = User::withoutGlobalScope('company')->find($offlineRequest->client_id);
        $client->notify(new OfflineInvoicePaymentAccept($offlineRequest));

        return Reply::success('Successfully verified');
    }

    public function rejectPayment(Request $request, $id)
    {
        $offlineRequest = OfflineInvoicePayment::findOrFail($id);
        $invoice = ContractInvoice::findOrFail($offlineRequest->invoice_id);

        $offlineRequest->status = 'reject';
        $offlineRequest->save();

        //Change the status of invoice
        $invoice->status = 'unpaid';
        $invoice->save();

        $client = User::withoutGlobalScope('company')->find($offlineRequest->client_id);
        $client->notify(new OfflineInvoicePaymentReject($offlineRequest));

        return Reply::success('Successfully rejected');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function cancelStatus(Request $request)
    {
        $invoice = ContractInvoice::find($request->invoiceID);
        $invoice->status = 'canceled'; // update status as canceled
        $invoice->save();

        return Reply::success(__('messages.invoiceUpdated'));
    }
    public function getAwardedContracts(Request $request){
        $contractorid = $request->contractorid ?: 0;
        $subprojectid = $request->subprojectid ?: 0;
        $projectid = $request->projectid ?: 0;
        $contractsarray = AwardedContracts::where('contractor_id',$contractorid)->where('project_id',$projectid)->where('subproject_id',$subprojectid)->get();
        $htmlcontent = '<option value="">Awarded Contract</option>';
        foreach ($contractsarray as $contract){
            $htmlcontent .= '<option value="'.$contract->id.'">'.$contract->title.' - '.$contract->number.'</option>';
        }
        return $htmlcontent;
    }
    public function boqInfo(Request $request){

        $awardedcontract = $request->awardedcontract ?: 0;
        $invoiceid = $request->invoiceid ?: '';
        $user = $this->user;
        $this->user = $user;
        $this->unitsarray = Units::get();
        $this->invoiceid = (int)$invoiceid;
        $this->awardedcontract = (int)$awardedcontract;
        $messageview = View::make('admin.contract-invoices.boqtitleloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function measurementSheet($id,$invoiceid=false){
        $user = $this->user;
        $this->pageTitle = 'app.menu.measurementsheet';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->units = Units::where('company_id',$user->company_id)->get();
        $projectcostitem = AwardContractProducts::find($id);
        $this->product = $projectcostitem;
        $invoicesessid = \request()->session()->get('contractinvoicesessionid');
        $this->invoicesessionid = $invoicesessid;
        $this->units = Units::where('id',$projectcostitem->unit)->first();
        $this->invoiceid =  !empty($invoiceid) ? $invoiceid : '';
        $measurementsheets = ContractMeasurementSheet::where('product_id',$projectcostitem->id);
        if(!empty($invoiceid)){
            $measurementsheets = $measurementsheets->where('invoice_id',$invoiceid);
        }else{
            $measurementsheets = $measurementsheets->where('session_id',$invoicesessid);
        }
        $this->measurementsheets = $measurementsheets->get();
        $ret = view('admin.contract-invoices.boqmeasurementsheet', $this->data)->renderSections()['content'];
        return $ret;
    }
    public function addQtytoMeasuresheet(Request $request){

        $ratesheetid = $request->sheetid;
        $productid = $request->productid;
        $invoiceid = $request->invoiceid ?: 0;
        $invoicesessid = \request()->session()->get('contractinvoicesessionid');
        if(!empty($ratesheetid)){
            $measuresheet = ContractMeasurementSheet::find($ratesheetid);
        }else{
            $measuresheet = new ContractMeasurementSheet();
            $measuresheet->session_id = $invoicesessid;
            $measuresheet->product_id = $productid;
            $measuresheet->invoice_id = $invoiceid;
        }
        if(!empty($request->name)){
            $name = $request->name;
            $measuresheet->name = $name;
        }
        if(!empty($request->nos)){
            $nos = $request->nos;
            $measuresheet->nos = $nos;
        }
        $qty = 0;
        if(!empty($request->types)){
            $types = array_values(array_filter($request->types));
            $measuresheet->types = implode(',',$types);
        }else{
            $types = explode(',',$measuresheet->types);
        }
        $qty = array_product($types);
        $nos =  $measuresheet->nos;
        $totqt = array_product(array($qty,$nos));
        $measuresheet->qty = round($totqt, 3);
        $measuresheet->save();
        $ret['id'] = $measuresheet->id;
        $ret['qty'] = $measuresheet->qty;
        if(!empty($invoiceid)){
            $totalqty = ContractMeasurementSheet::where('product_id',$productid)->where('invoice_id',$invoiceid)->sum('qty');
        }else{
            $totalqty = ContractMeasurementSheet::where('product_id',$productid)->where('session_id',$invoicesessid)->sum('qty');
        }
        $ret['totalqty'] = $totalqty;
        return $ret;
    }
    public function removeQtytoMeasuresheet(Request $request){

        $ratesheetid = $request->sheetid;
        $measuresheet = ContractMeasurementSheet::find($ratesheetid);
        $measuresheet->delete();

    }
}
