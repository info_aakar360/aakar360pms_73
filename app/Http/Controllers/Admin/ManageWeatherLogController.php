<?php

namespace App\Http\Controllers\Admin;

use App\Contractors;
use App\DailyLogFiles;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\TimeLogs\StoreTimeLog;
use App\LogTimeFor;
use App\Segment;
use App\WeatherLog;
use App\WeatherLogFiles;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\Task;
use App\Title;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageWeatherLogController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Weather Logs';
        $this->pageIcon = 'icon-clock';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('tasks', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });

    }

    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',', $user->projectlist);
        $this->projectlist = Project::whereIn('id', $projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        return view('admin.weather-logs.index', $this->data);
    }

    public function create()
    {
        $user = $this->user;
        $projectlist = explode(',', $user->projectlist);
        $this->projectlist = Project::whereIn('id', $projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        return view('admin.weather-logs.create', $this->data);
    }

    public function edit($id)
    {
        $user = $this->user;
        $projectlist = !empty($user->projectlist) ? explode(',', $user->projectlist) : array();
        $weather = WeatherLog::findOrFail($id);
        $this->weather = $weather;
        $this->employees = Employee::getAllEmployees($user);
        $this->projectarray = Project::whereIn('id', $projectlist)->get();
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->titlesarray = !empty($weather->project_id) ? Title::where('project_id', $weather->project_id)->get() : array();
        $this->segmentsarray = !empty($weather->project_id) ? Segment::where('projectid', $weather->project_id)->get() : array();
        $this->files = WeatherLogFiles::where('weather_id',$weather->id)->get();
        return view('admin.weather-logs.edit', $this->data);
    }


    public function data(Request $request)
    {

        $projectId = $request->projectId;
        $projectName = 'projects.project_name';
        $timeLogs = WeatherLog::join('users', 'users.id', '=', 'weather_logs.added_by');
        $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'weather_logs.project_id');
        $timeLogs = $timeLogs->leftjoin('segment', 'segment.id', '=', 'weather_logs.segment_id');
        $timeLogs = $timeLogs->leftjoin('title', 'title.id', '=', 'weather_logs.title_id');
        $timeLogs = $timeLogs->select('weather_logs.id', 'users.name', 'weather_logs.project_id', $projectName, 'title.title', 'segment.name', 'weather_logs.mintemp', 'weather_logs.maxtemp', 'weather_logs.sitecondition', 'weather_logs.weathercondition', 'weather_logs.raintimings');
        /*  if(!is_null($employee) && $employee !== 'all'){
              $timeLogs->where('weather_logs.added_by', $employee);
          }*/
        if (!is_null($projectId) && $projectId !== 'all') {
            $timeLogs->where('weather_logs.project_id', '=', $projectId);
        }

        $timeLogs = $timeLogs->get();

        return DataTables::of($timeLogs)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.weather-logs.edit', $row->id) . '" class="btn btn-info btn-circle edit-time-log"
                      data-toggle="tooltip" data-time-id="' . $row->id . '"  data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                        <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                        data-toggle="tooltip" data-time-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('project_name', function ($row) {
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->editColumn('segment_name', function ($row) {
                return ucfirst($row->segment_name);
            })
            ->editColumn('files', function ($row) {
                $files = WeatherLogFiles::where('weather_id', $row->id)->get();
                foreach ($files as $file) {
                    if ($file->external_link != '') {
                        return '<a target="_blank" href="' . $file->external_link . '" >' . ucfirst($file->filename) . '</a ><br>';
                    } elseif (config('filesystems.default') == 'local') {
                        return '<a target="_blank" href="' . uploads_url().'/weather-log-files/' . $row->id . '/' . $file->hashname . '">' . ucfirst($file->filename) . '</a ><br>';
                    } elseif (config('filesystems.default') == 's3') {
                        return '<a target="_blank" href="' . $this->url . $this->company->id . '/weather-log-files/' . $row->id . '/' . $file->hashname . '">' . ucfirst($file->filename) . '</a ><br>';
                    } elseif (config('filesystems.default') == 'google') {
                        return '<a target="_blank" href="' . $file->google_url . '">' . ucfirst($file->filename) . '</a><br>';
                    } elseif (config('filesystems.default') == 'dropbox') {
                        return '<a target="_blank" href="' . $file->dropbox_link . '">' . ucfirst($file->filename) . '</a><br>';
                    }
                }
//                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';

            })
            ->rawColumns(['action', 'project_name', 'files'])
            ->removeColumn('project_id')
            ->removeColumn('task_id')
            ->make(true);
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'project_id' => 'required',
                'tempmin' => 'required',
                'tempmax' => 'required',
                'site_condition' => 'required',
                'weather_condition' => 'required'],

            ['project_id.required' => 'Project field is required',
                'tempmin.required' => 'Min Temp field is required',
                'tempmax.required' => 'Max Temp field is required',
                'site_condition.required' => 'Working hours field is required',
                'weather_condition.required' => 'Work date is required',
            ]
        );

        $timeLog = new WeatherLog();
        $timeLog->mintemp = $request->tempmin;
        $timeLog->maxtemp = $request->tempmax;
        $timeLog->description = $request->description;
        $timeLog->project_id = $request->project_id ?: 0;
        $timeLog->title_id = $request->title_id ?: 0;
        $timeLog->segment_id = $request->segment_id ?: 0;
        $timeLog->task_id = $request->task_id ?: '';
        $timeLog->sitecondition = $request->site_condition ?: '';
        $timeLog->weathercondition = $request->weather_condition ?: '';
        $timeLog->raintimings = !empty($request->timings) ? json_encode($request->timings) : '';
        $timeLog->added_by = $this->user->id;
        $timeLog->save();
        return Reply::dataOnly(['weatherID' => $timeLog->id]);
    }

    public function update(Request $request, $id)
    {
        $timeLog = WeatherLog::findorfail($id);
        $timeLog->mintemp = $request->tempmin;
        $timeLog->maxtemp = $request->tempmax;
        $timeLog->description = $request->description;
        $timeLog->project_id = $request->project_id ?: 0;
        $timeLog->title_id = $request->title_id ?: 0;
        $timeLog->segment_id = $request->segment_id ?: 0;
        $timeLog->task_id = $request->task_id ?: '';
        $timeLog->sitecondition = $request->site_condition ?: '';
        $timeLog->weathercondition = $request->weather_condition ?: '';
        $timeLog->raintimings = !empty($request->timings['fromtime'][0]) ? json_encode($request->timings) : '';
        $timeLog->added_by = $this->user->id;
        $timeLog->save();
        return Reply::dataOnly(['weatherID' => $timeLog->id]);
    }

    public function destroy($id)
    {
            WeatherLog::destroy($id);
            WeatherLogFiles::where('weather_id', $id)->delete();
        return Reply::success(__('messages.weatherLogDeleted'));
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData) {
                $storage = storage();
                $company = $this->company->id;
                $file = new WeatherLogFiles();
                $file->added_by = $this->user->id;
                $file->company_id = $this->user->company_id;
                $file->weather_id = $request->weather_id;
                switch ($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/weather-log-files/' . $request->weather_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($company . '/weather-log-files/' . $request->weather_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'task-files')
                            ->first();

                        if (!$dir) {
                            Storage::cloud()->makeDirectory('task-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->weather_id)
                            ->first();

                        if (!$directory) {
                            Storage::cloud()->makeDirectory($dir['path'] . '/' . $request->weather_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->weather_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('weather-log-files/' . $request->weather_id . '/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/' . $request->weather_id . '/' . $fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->weather_id, __('messages.newFileUploadedToTheProject'));
            }

        }
//        return Reply::redirect(route('admin.man-power-logs.index'), __('modules.projects.projectUpdated'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function stopTimer(Request $request)
    {
        $timeId = $request->timeId;
        $timeLog = WeatherLog::findOrFail($timeId);
        $timeLog->end_time = Carbon::now();
        $timeLog->edited_by_user = $this->user->id;
        $timeLog->save();

        $timeLog->total_hours = ($timeLog->end_time->diff($timeLog->start_time)->format('%d') * 24) + ($timeLog->end_time->diff($timeLog->start_time)->format('%H'));

        if ($timeLog->total_hours == 0) {
            $timeLog->total_hours = round(($timeLog->end_time->diff($timeLog->start_time)->format('%i') / 60), 2);
        }
        $timeLog->total_minutes = ($timeLog->total_hours * 60) + ($timeLog->end_time->diff($timeLog->start_time)->format('%i'));

        $timeLog->save();

        $this->activeTimers = WeatherLog::whereNull('end_time')
            ->get();
        $view = view('admin.projects.man-power-logs.active-timers', $this->data)->render();
        return Reply::successWithData(__('messages.timerStoppedSuccessfully'), ['html' => $view, 'activeTimers' => count($this->activeTimers)]);
    }

    /**
     * @param $projectId
     * @return mixed
     * @throws \Throwable
     */
    public function membersList($projectId)
    {

        $this->members = ProjectMember::byProject($projectId);

        $list = view('admin.tasks.members-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $id
     */
    public function export($startDate, $endDate, $id, $employee = null)
    {

        $projectName = 'projects.project_name'; // Set default name for select in mysql
        $timeLogs = WeatherLog::join('users', 'users.id', '=', 'weather_logs.added_by');

        $this->logTimeFor = LogTimeFor::first();

        // Check for apply join Task Or Project
        if ($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task') {
            $timeLogs = $timeLogs->join('tasks', 'tasks.id', '=', 'weather_logs.weather_id');
            $projectName = 'tasks.heading as project_name';
        } else {
            $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'weather_logs.project_id');
        }

        // Fields selecting  For excel
        $timeLogs = $timeLogs->select('weather_logs.id', 'users.name', $projectName, 'weather_logs.start_time', 'weather_logs.end_time', 'weather_logs.memo', 'weather_logs.total_minutes');

        // Condition according start_date
        if (!is_null($startDate)) {
            $timeLogs->where(DB::raw('DATE(weather_logs.`start_time`)'), '>=', "$startDate");
        }

        // Condition according start_date
        if (!is_null($endDate)) {
            $timeLogs->where(DB::raw('DATE(weather_logs.`end_time`)'), '<=', "$endDate");
        }

        // Condition according employee
        if (!is_null($employee) && $employee !== 'all') {
            $timeLogs->where('weather_logs.added_by', $employee);
        }

        // Condition according select id
        if (!is_null($id) && $id !== 'all') {
            if ($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task') {
                $timeLogs->where('weather_logs.task_id', '=', $id);
            } else {
                $timeLogs->where('weather_logs.project_id', '=', $id);
            }
        }
        $attributes = ['total_minutes', 'duration', 'timer'];
        $timeLogs = $timeLogs->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'User', 'Log For', 'Start Time', 'End Time', 'Memo', 'Total Hours'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($timeLogs as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('timelog', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Time Log');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('time log file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });

        })->download('xlsx');
    }


}
