<?php

namespace App\Http\Controllers\Admin;

use App\AwardContractCategory;
use App\AwardContractProducts;
use App\AwardedContracts;
use App\BoqCategory;
use App\Contractors;
use App\CostItems;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Tax\StoreTax;
use App\InspectionFile;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectSegmentsProduct;
use App\Segment;
use App\Tax;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersCategory;
use App\TendersContracts;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\FileManager;
use Illuminate\Support\Facades\File;
use  View;
use Yajra\DataTables\Facades\DataTables;

class TenderController extends AdminBaseController
{
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    /**
     * ManageProjectFilesController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.tenders';
        $this->activeMenu = 'pms';

    }

    public function index()
    {
        $user = $this->user;
        $this->tendersarray = Tenders::where('company_id',$user->company_id)->orderBy('startdate','asc')->get();
        return view('admin.tenders.index', $this->data);
    }

    public function create()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        return view('admin.tenders.create', $this->data);
    }

    public function store(Request $request)
    {
        $user = $this->user;
        $memberExistsInTemplate = false;
        $project = new Tenders();
        $project->company_id = $user->company_id;
        $project->name = $request->title;
        $project->number = $request->number;
        $project->distribution = !empty($request->distribution) ? implode(',',$request->distribution) : '';
        $project->accept_submition = !empty($request->accept_submition) ? '1' : '0';
        $project->enable_blind = !empty($request->enable_blind) ? '1' : '0';
        $project->include_bid_doc = !empty($request->include_bid_doc) ? '1' : '0';
        $project->send_count_emails = !empty($request->send_count_emails) ? '1' : '0';
        $project->enable_prebid_rfi = !empty($request->enable_prebid_rfi) ? '1' : '0';
        $project->enable_prebid_walkthrough = !empty($request->enable_prebid_walkthrough) ? '1' : '0';
        $project->count_email = !empty($request->count_email) ? $request->count_email : '0';
        if ($request->prebid_rfi_date != '') {
            $project->prebid_rfi_date = date('Y-m-d', strtotime($request->prebid_rfi_date));
        }
        if ($request->start_date != '') {
            $project->startdate = date('Y-m-d', strtotime($request->start_date));
        }
        if ($request->due_date != '') {
            $project->deadline = date('Y-m-d', strtotime($request->due_date));
        }
        if ($request->prebid_walkthrough_date != '') {
            $project->prebid_walkthrough_date = date('Y-m-d', strtotime($request->prebid_walkthrough_date));
        }
        if ($request->anticipated_date != '') {
            $project->anticipated_date = date('Y-m-d', strtotime($request->anticipated_date));
        }
        if ($request->bidding_information != '') {
            $project->bidding_information = $request->bidding_information;
        }
        if ($request->project_information != '') {
            $project->project_information = $request->project_information;
        }
        if ($request->walkthough_information != '') {
            $project->walkthough_information = $request->walkthough_information;
        }
        $project->status = $request->status;
        $project->added_by =  $this->user->id;
        $project->save();
        return Reply::dataOnly(['tenderID' => $project->id]);
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $file = new TendersFiles();
                $file->company_id = $this->user->company_id;
                $file->user_id = $this->user->id;
                $file->tender_id = $request->tender_id;
                switch($storage) {
                    case 'local':
                        $destinationPath = 'uploads/tender-files/'.$file->tender_id;
                        if (!file_exists(''.$destinationPath)) {
                            mkdir(''.$destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('tender-files/'.$request->tender_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'tender-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('tender-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->tender_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->tender_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->tender_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('tender-files/'.$request->tender_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/tender-files/'.$request->tender_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->tender_id, __('messages.newFileUploadedToTheProject'));
            }

        }
     /*   return Reply::redirect(route('admin.tenders.index'), __('modules.projects.projectUpdated'));*/
    }

    public function projectTitles(Request $request){
        $tender = $request->tender;
        $projectid = $request->projectid;
        Tenders::where('id',$tender)->update(['project_id'=>$projectid]);
        $titleoption = '<option value="">Select Sub project</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
        }
        return $titleoption;
    }
    public function segmentList(Request $request){
        $tender = $request->tender;
        $projectid = $request->projectid;
        $titleid = $request->titleid;
        $titleoption = '';
        Tenders::where('id',$tender)->update(['project_id'=>$projectid]);
        if($projectid&&$titleid){
            $titlearray = Segment::where('projectid',$projectid)->where('titleid',$titleid)->get();
            if(count($titlearray)>0){
                $titleoption = '<option value="">Select Segment</option>';
                foreach ($titlearray as $item) {
                    $titleoption .= '<option value="'.$item->id.'">'.$item->name.'</option>';
                }
            }
        }
        return $titleoption;
    }
    public function boqCostItems(Request $request){
        $projectid = $request->projectid;
        $titleid = $request->titleid ?: 0;
        $segmentid = $request->segmentid ?: 0;
        $tender = $request->tender;
        Tenders::where('id',$tender)->update(['title_id'=>$titleid,'segment_id'=>$segmentid]);
        $projectdetails = Project::find($projectid);
        $segmentoption = '';
        $titleoption = '<option value="">Select Task</option>';
        $catoption = '<option value="">Select Activity</option>';
        if($projectid){
            $segmentsarray = Segment::where('projectid',$projectid)->where('titleid',$titleid)->get();
            if($projectdetails->segment=='enable'){
                $segmentoption = '<option value="">Select Segment</option>';
                if(count($segmentsarray)>0){
                    foreach ($segmentsarray as $item) {
                        $segmentoption .= '<option value="'.$item->id.'">'.$item->name.'</option>';
                    }
                }
            }
            if(!empty($segmentid)){
                $titlearray = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->pluck('cost_items_id','id');
                foreach ($titlearray as $item =>$value) {
                    $costitemname = get_cost_name($value);
                    $titleoption .= '<option value="'.$item.'" data-name="'.$costitemname.'" >'.$costitemname.'</option>';
                }
                $categoryarray = DB::table('project_segments_product')
                    ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
                    ->where('project_id',$projectid)
                    ->where('title',$titleid)
                    ->where('segment',$segmentid)
                    ->first();
                $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
                $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
                foreach ($boqlevel1categories as $colums) {
                    $catoption .= '<option value="'.$colums->id.'" data-name="'.$colums->title.'" >'.$colums->title.'</option>';
                }

            }else{

                $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->pluck('cost_items_id','id');
                foreach ($titlearray as $item =>$value) {
                    $costitemname = get_cost_name($value);
                    $titleoption .= '<option value="'.$item.'" data-name="'.$costitemname.'" >'.$costitemname.'</option>';
                }
            $categoryarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
                ->where('project_id',$projectid)
                ->where('title',$titleid)
                ->first();
            $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
                $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
                foreach ($boqlevel1categories as $colums) {
                    $catoption .= '<option value="'.$colums->id.'" data-name="'.$colums->title.'" >'.$colums->title.'</option>';
                }

            }

        }
        return array('segments'=>$segmentoption,'costitem'=>$titleoption,'costcat'=>$catoption);
    }

    public function boqItemCatRow(Request $request){
        $projectid = $request->projectid;
        $titleid = $request->titleid;
        $parent = $request->parent;
        $catid = $request->catid;
        $itemid = $request->category;
        $tender = $request->tender;
        $level = $request->level;
        $maxinc = TendersCategory::where('tender_id',$tender)->where('level',$level)->max('inc');
        $newid = (int)$maxinc+1;
        $tendercategory = new TendersCategory();
        $tendercategory->tender_id = $tender;
        $tendercategory->category = $itemid;
        $tendercategory->product_category = $catid;
        $tendercategory->level = $level;
        $tendercategory->parent = $parent;
        $tendercategory->inc = $newid;
        $tendercategory->save();
    }
    public function boqItemRow(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $titleid = $request->titleid ?: 0;
        $segmentid = $request->segmentid;
        $showrate = $request->showrate;
        $rowid = $request->rowid;
        $costitemname = $request->costitemname;
        $catid = $request->catid;
        $tender_category = $request->tendercat;
        $tender = $request->tender;
        $unitsarray = Units::where('company_id',$user->company_id)->get();
        if(!is_numeric($rowid)){
            $cost_item = CostItems::where('company_id',$user->company_id)->where('cost_item_name',$costitemname)->first();
            if(empty($cost_item)){
                $cost_item = new CostItems();
                $cost_item->cost_item_name = $costitemname;
                $cost_item->company_id = $user->company_id;
                $cost_item->save();
            }
            $rowid = $cost_item->id;
        }
        if(!empty($segmentid)){
            $costitemrow = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->where('id',$rowid)->first();
            $titlearray = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->pluck('cost_items_id','id');
        }else{
        $costitemrow = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->where('id',$rowid)->first();
        $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->pluck('cost_items_id','id');
        }
        $maxinc = TendersProduct::where('tender_id',$tender)->max('inc');
        $newid = (int)$maxinc+1;
        $tendercategory = new TendersProduct();
        $tendercategory->tender_id = $tender;
        $tendercategory->tenders_category = $tender_category;
        $tendercategory->category = $catid;
        $tendercategory->products = $costitemrow->id;
        $tendercategory->cost_items = $costitemrow->cost_items_id;
        $tendercategory->qty = $costitemrow->qty ?: 0;
        $tendercategory->unit = $costitemrow->unit ?: 0;
        $tendercategory->inc = $newid;
        $tendercategory->save();
    }
    public function edit($id)
    {
        $user = $this->user;
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $this->tenders = Tenders::find($id);
        $this->files = TendersFiles::where('tender_id',$id)->get();
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        return view('admin.tenders.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $user = $this->user;
        $project = Tenders::find($id);
        $project->name = $request->title;
        $project->number = $request->number;
        $project->distribution = !empty($request->distribution) ? implode(',',$request->distribution) : '';
        $project->accept_submition = !empty($request->accept_submition) ? '1' : '0';
        $project->enable_blind = !empty($request->enable_blind) ? '1' : '0';
        $project->include_bid_doc = !empty($request->include_bid_doc) ? '1' : '0';
        $project->send_count_emails = !empty($request->send_count_emails) ? '1' : '0';
        $project->enable_prebid_rfi = !empty($request->enable_prebid_rfi) ? '1' : '0';
        $project->enable_prebid_walkthrough = !empty($request->enable_prebid_walkthrough) ? '1' : '0';
        $project->count_email = !empty($request->count_email) ? $request->count_email : '0';
        if ($request->prebid_rfi_date != '') {
            $project->prebid_rfi_date = date('Y-m-d', strtotime($request->prebid_rfi_date));
        }
        if ($request->start_date != '') {
            $project->startdate = date('Y-m-d', strtotime($request->start_date));
        }
        if ($request->due_date != '') {
            $project->deadline = date('Y-m-d', strtotime($request->due_date));
        }
        if ($request->prebid_walkthrough_date != '') {
            $project->prebid_walkthrough_date = date('Y-m-d', strtotime($request->prebid_walkthrough_date));
        }
        if ($request->anticipated_date != '') {
            $project->anticipated_date = date('Y-m-d', strtotime($request->anticipated_date));
        }
        if ($request->bidding_information != '') {
            $project->bidding_information = $request->bidding_information;
        }
        if ($request->project_information != '') {
            $project->project_information = $request->project_information;
        }
        if ($request->walkthough_information != '') {
            $project->walkthough_information = $request->walkthough_information;
        }
        $project->status = $request->status;
        $project->added_by =  $this->user->id;
        $project->save();

        return Reply::dataOnly(['tenderID' => $project->id]);
    }
    public function editCostItem($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $tenders = Tenders::find($id);
        $subprojectid = $tenders->title_id ?: 0;
        $this->user = $user;
        $this->tenders = $tenders;
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        return view('admin.tenders.editcostitem', $this->data);
    }
    public function destroy($id)
    {
        $projectde = Tenders::where('id',$id)->first();
        if(!empty($projectde->id)){
            TendersFiles::where('tender_id',$projectde->id)->delete();
            TendersCategory::where('tender_id',$projectde->id)->delete();
            TendersProduct::where('tender_id',$projectde->id)->delete();
            TenderBidding::where('tender_id',$projectde->id)->delete();
            TenderBiddingProduct::where('tender_id',$projectde->id)->delete();
            $projectde->delete();
            return Reply::success(__('messages.tenderDeleted'));
        }
    }
    public function boqItemRemoveRow($id)
    {
        TendersProduct::where('id',$id)->delete();
        return Reply::success(__('messages.taskDeletedSuccessfully'));
    }
    public function removeFile($id){
        $inspectionFiles = TendersFiles::findOrFail($id);
        $inspectionFiles->delete();
        return Reply::success(__('messages.imageDeletedSuccessfully'));
    }

    public function tenderBidding($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);

        $tenderscontract = TendersContracts::find($id);
        $tenders = Tenders::find($tenderscontract->tender_id);
        $subprojectid = $tenders->title_id ?: 0;
        $this->tendercontract = $tenderscontract;
        $this->tenders = $tenders;
        $this->contractor = User::where('id',$tenderscontract->user_id)->first();
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        $this->files = TendersFiles::where('tender_id',$id)->get();
        return view('admin.tenders.bidding', $this->data);
    }
    public function tenderBiddingLoop(Request $request)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);

        $tenderscontract = TendersContracts::find($request->tendercontract);
        $tenders = Tenders::find($tenderscontract->tender_id);
        $subprojectid = $tenders->title_id ?: 0;
        $this->tendercontract = $tenderscontract;
        $this->tenders = $tenders;
        $this->contractor = $tenderscontract->user_id;
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        $this->files = TendersFiles::where('tender_id',$tenders->id)->get();
        $messageview = View::make('admin.tenders.biddingloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function contractorBiddingPrice(Request $request)
    {
        $tenderproductid = $request->tenderproduct;
        $tenderproduct = TendersContracts::find($tenderproductid);
        $price = $request->price;
        $userid = $tenderproduct->user_id;
        $companyid = $tenderproduct->company_id ?: 1;
        if(!empty($tenderproductid)&&!empty($price)){
            $tenderproduct = \App\TendersProduct::where('id',$tenderproductid)->first();
            if(!empty($tenderproduct->id)){
                    $tender = Tenders::find($tenderproduct->tender_id);
                    if(!empty($tender->id)){
                        $tenderbidding = TenderBidding::where('user_id',$userid)->where("tender_id",$tender->id)->where("project_id",$tender->project_id)->first();
                        if(empty($tenderbidding->id)){
                            $tenderbidding = new TenderBidding();
                            $tenderbidding->project_id = $tender->project_id;
                            $tenderbidding->tender_id = $tender->id;
                            $tenderbidding->user_id = $userid;
                            $tenderbidding->company_id = $companyid;
                            $tenderbidding->sourcing_id  = '' ;
                            $tenderbidding->save();
                        }
                        $tenderbiddingproduct = TenderBiddingProduct::where('tender_id',$tender->id)->where('bidding_id',$tenderbidding->id)->where('products',$tenderproduct->cost_items)->where('category',$tenderproduct->category)->first();
                        if(empty($tenderbiddingproduct->id)){
                            $tenderbiddingproduct = new TenderBiddingProduct();
                            $tenderbiddingproduct->tender_id = $tender->id;
                            $tenderbiddingproduct->bidding_id = $tenderbidding->id;
                            $tenderbiddingproduct->tender_product = $tenderbidding->id;
                            $tenderbiddingproduct->products = $tenderproduct->cost_items;
                            $tenderbiddingproduct->category = $tenderproduct->category;
                        }
                        $qty = $tenderproduct->qty ?: 0;
                        $finalamount = $price*$qty;
                        $tenderbiddingproduct->qty = $qty;
                        $tenderbiddingproduct->price = $price;
                        $tenderbiddingproduct->finalprice = $finalamount;
                        $tenderbiddingproduct->save();
                        return Reply::dataOnly(['amount' => $finalamount]);
                    }
            }
        }
    }
    public function tenderBiddingList($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $tenders = Tenders::find($id);
        $subprojectid = $tenders->title_id ?: 0;
        $this->tenders = $tenders;
        $employeesarray = Employee::where('company_id',$user->company_id)->where('user_type','contractor')->pluck('id')->toArray();
        $this->tendercontractsarray = TendersContracts::whereIn('employee_id',$employeesarray)->where('tender_id',$tenders->id)->get();
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        $this->files = TendersFiles::where('tender_id',$id)->get();
        return view('admin.tenders.bidding-list', $this->data);
    }
    public function tenderBiddingsheet($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $tenders = Tenders::find($id);
        $supliers =  TenderBidding::where('tender_id',$tenders->id)->where('project_id',$tenders->project_id)->pluck('user_id')->toArray();
        $employeesarray = Employee::where('company_id',$user->company_id)->where('user_type','contractor')->whereIn('user_id',$supliers)->get();
         $subprojectid = $tenders->title_id ?: 0;
        $this->tenders = $tenders;
        $this->supplierlist = $employeesarray;
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        $this->files = TendersFiles::where('tender_id',$id)->get();
        return view('admin.tenders.bidding-sheet', $this->data);
    }
    public function tenderSellerSubmit(Request $request){
        $user = $this->user;
        if(!empty($request->tender_id)){
            $tenderdetails = Tenders::find($request->tender_id);
            if(!empty($tenderdetails->id)){
                if(!empty($request->supplier)){

                    $tenderbidding = \App\TenderBidding::where('tender_id', $tenderdetails->id)->where('user_id', $request->supplier)->first();
                    if(!empty($tenderbidding)){

                        $awaredcontracts = AwardedContracts::where('contractor_id',$request->supplier)->where('tender_id',$tenderdetails->id)->first();
                    if(empty($awaredcontracts->id)){
                        $awaredcontracts =  new AwardedContracts();
                        $awaredcontracts->user_id = $user->id;
                        $awaredcontracts->company_id = $user->company_id;
                        $awaredcontracts->contractor_id = $request->supplier;
                        $awaredcontracts->tender_id = $request->tender_id;
                        $awaredcontracts->tendertype = 'allot';
                        $awaredcontracts->title = $tenderdetails->name;
                        $awaredcontracts->number = $tenderdetails->number;
                        $awaredcontracts->distribution = $tenderdetails->distribution;
                        $awaredcontracts->status = $tenderdetails->status;
                        $awaredcontracts->project_id = $tenderdetails->project_id;
                        $awaredcontracts->subproject_id = $tenderdetails->title_id;
                        $awaredcontracts->segment_id = $tenderdetails->segment_id;
                        $awaredcontracts->save();

                        $tendercategoryarrays = TendersCategory::where('tender_id',$tenderdetails->id)->where('level',0)->where('parent',0)->get();
                        $looparray = array();
                        $looparray['user'] = $user;
                        $looparray['request'] = $request;
                        $looparray['categoryarray'] = $tendercategoryarrays;
                        $looparray['contractor'] = $request->supplier;
                        $looparray['tenderdetails'] = $tenderdetails;
                        $looparray['awaredcontracts'] = $awaredcontracts;
                        $looparray['tenderbidding'] = $tenderbidding;
                        $looparray['level'] = 0;
                        $looparray['parent'] = 0;
                       $this->awardedproducts($looparray);


                        return Reply::redirect(route('admin.awardedcontracts.awardedContracts'),'Contract request submitted');
                   }else{
                        return Reply::redirectWithError(route('admin.tenders.bidding-sheet',[$request->tender_id]),'Contract request already submitted');
                    }
                    }else{
                        return Reply::redirectWithError(route('admin.tenders.bidding-sheet',[$request->tender_id]),'Bidding information not found');
                    }
                }else{
                    return Reply::redirectWithError(route('admin.tenders.bidding-sheet',[$request->tender_id]),'Please select contractor');
                }
            }else{
                return Reply::redirectWithError(route('admin.tenders.index'),'Tender not found');
            }
        }else{
            return Reply::redirectWithError(route('admin.tenders.index'),'Tender not found');
        }
    }
    protected function awardedproducts($looparray){
        $request = $looparray['request'];
        $user = $looparray['user'];
        $tendercategoryarrays = $looparray['categoryarray'];
        $contractor = $looparray['contractor'];
        $awaredcontracts = $looparray['awaredcontracts'];
        $tenderdetails = $looparray['tenderdetails'];
        $tenderbidding = $looparray['tenderbidding'];
        $level = $looparray['level'];
        $parent = $looparray['parent'];
        foreach ($tendercategoryarrays as $tendercat){
            $prevawardcontract = AwardContractCategory::where('awarded_contract_id',$awaredcontracts->id)->where('product_category',$tendercat->product_category)->first();
            if(empty($prevawardcontract->id)){
                $tendercategory = new AwardContractCategory();
                $tendercategory->awarded_contract_id = $awaredcontracts->id;
                $tendercategory->product_category = $tendercat->product_category;
                $tendercategory->category = $tendercat->category;
                $tendercategory->level = $level;
                $tendercategory->parent = $parent;
                $tendercategory->inc = $tendercat->inc;
                $tendercategory->save();
            }

            $tenderproductsarray = TendersProduct::where('tender_id',$tenderdetails->id)->where('tenders_category',$tendercat->id)->get();
            foreach ($tenderproductsarray as $tenderproducts){
                $tenderbiddingpro =  TenderBiddingProduct::where('tender_product',$tenderproducts->id)->where('tender_id', $tenderdetails->id)->where('bidding_id', $tenderbidding->id)->first();
                if(!empty($tenderbiddingpro->id)){
                    $rate = $tenderbiddingpro->price ?: 0;
                    $qty = $tenderbiddingpro->qty ?: 0;
                    $finalrate = $rate*$qty;
                    $tenderwarproducts = new AwardContractProducts();
                    $tenderwarproducts->company_id = $user->company_id;
                    $tenderwarproducts->added_by = $user->id;
                    $tenderwarproducts->awarded_contract_id = $awaredcontracts->id;
                    $tenderwarproducts->awarded_category = $tendercategory->id;
                    $tenderwarproducts->category = $tenderproducts->category;
                    $tenderwarproducts->products = $tenderproducts->products;
                    $tenderwarproducts->cost_items = $tenderproducts->cost_items;
                    $tenderwarproducts->unit = $tenderproducts->unit ?: 0;
                    $tenderwarproducts->qty = $qty ?: 0;
                    $tenderwarproducts->rate = $rate ?: 0;
                    $tenderwarproducts->finalrate = $finalrate ?: 0;
                    $tenderwarproducts->inc = $tenderproducts->inc;
                    $tenderwarproducts->save();
                }
            }
            $newlevel = $level+1;
            $newparent = $tendercat->id;
            $tendercategoryarrays = TendersCategory::where('tender_id',$tenderdetails->id)->where('level',$newlevel)->where('parent',$newparent)->get();
            $looparray = array();
            $looparray['user'] = $user;
            $looparray['request'] = $request;
            $looparray['categoryarray'] = $tendercategoryarrays;
            $looparray['contractor'] = $contractor;
            $looparray['tenderdetails'] = $tenderdetails;
            $looparray['awaredcontracts'] = $awaredcontracts;
            $looparray['tenderbidding'] = $tenderbidding;
            $looparray['level'] = $newlevel;
            $looparray['parent'] = $newparent;
            $this->awardedproducts($looparray);
        }
    }
    public function contractorsList(Request $request,$id){
        $user = $this->user;
        $tenderde = Tenders::find($id);
        $tendercostitem =  TendersProduct::where('tender_id',$id)->first();
        if(!empty($tendercostitem->id)){
            $users = Employee::getAllContractors($user,$tenderde->project_id);
            return DataTables::of($users)
                ->addColumn('checkbox', function ($row) {
                    return '<input type="checkbox" class="contractors" name="contractors[]" value="'.$row->id.'" />';
                 })
                ->addColumn('link', function($row) use ($user, $id){
                    $tendercontact = TendersContracts::where('company_id',$row->company_id)->where('user_id',$row->user_id)->where('employee_id',$row->id)->where('tender_id',$id)->first();
                    if(!empty($tendercontact)) {
                        return '<a href="javascript:void(0);" onClick="copyLink(\'id' . $tendercontact->id . '\')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>
                                &nbsp;<a href="https://api.whatsapp.com/send?text=' . route('front.tenderContract', [$tendercontact->id, $tendercontact->unique_id]) . '" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                &nbsp;<a href="javascript:;" class="btn btn-primary btn-circle sendEmail" data-id="'.$tendercontact->id.'" data-tendercontract-id="'.$tendercontact->id.'" data-toggle="tooltip" data-original-title="Send Email"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                <br><span id="id' . $tendercontact->id . '">' . route('front.tenderContract', [$tendercontact->id, $tendercontact->unique_id]) . '</span>';
                    }
                    return '';
                })
                ->addIndexColumn()
                ->rawColumns(['link','checkbox'])
                ->make(true);
        }else{
            return Reply::error('Tender product not available');
        }
    }
    public function sendContractorsMail(Request $request){
        $user = $this->user;
        $tender = $request->tender;
        $tenderdetails = Tenders::find($tender);
        $contractors = $request->contractors;
        if($tender&&$contractors){
            $contractors = explode(',',$contractors);
            $contractorsarray = Employee::whereIn('id',$contractors)->where('user_type','contractor')->get();
            $sendmail['success'] = true;
            foreach ($contractorsarray as $contractor){
                $tenderscontracts = TendersContracts::where('company_id',$user->company_id)->where('user_id',$contractor->user_id)->where('employee_id',$contractor->id)->where('tender_id',$tender)->first();
                if(empty($tenderscontracts)){
                    $tenderscontracts = new TendersContracts();
                    $tenderscontracts->added_by = $user->id;
                    $tenderscontracts->company_id = $user->company_id;
                    $tenderscontracts->user_id = $contractor->user_id;
                    $tenderscontracts->employee_id = $contractor->id;
                    $tenderscontracts->tender_id = $tender;
                    $tenderscontracts->unique_id = generate_string(10);
                    $tenderscontracts->save();
                }
                if(!empty($contractor->email)){
                $mailarray = array();
                $mailarray['email'] = $contractor->email;
                $mailarray['subject'] = 'Aakar360 Tender Bidding Email';
                $htmlcontent = '';
                $htmlcontent .= '<p>Hi <strong>'.$contractor->name.'</strong></p>';
                $htmlcontent .= '<p>A new tender as been released</p>';
                $htmlcontent .= '<p>Tender: <strong>'.$tenderdetails->name.'</strong></p>';
                $htmlcontent .= '<p>Please update your rate sheet here in link</p>';
                $htmlcontent .= route('front.tenderContract',[$tenderscontracts->id,$tenderscontracts->unique_id]);
                $mailarray['message'] = $htmlcontent;
                $sendmail = $user->sendEmail($mailarray);
                }
            }
              return $sendmail;
        }else{
            return Reply::error("Tender Information not found");
        }
    }

    public  function boqChangeColPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                TendersProduct::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public function costitemCatRemove($id){

        $position = TendersCategory::where('id',$id)->first();
        if(!empty($position->id)){
            $category = $position->category;
            TendersProduct::where('category',$category)->where('tender_id',$position->tender_id)->delete();
            $position->delete();
            return Reply::success(__('messages.activityDeleted'));
        }
    }
    public function costitemLoop(Request $request){
        $user = $this->user;
        $id = $request->tender;
        $projectlist = explode(',',$user->projectlist);
        $tenders = Tenders::find($id);
        $tenders->project_id = $request->projectid ?: 0;
        $tenders->title_id = $request->titleid ?: 0;
        $tenders->save();
        $subprojectid = $tenders->title_id ?: 0;
        $this->tenders = $tenders;
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$subprojectid)->get();
        $messageview = View::make('admin.tenders.costitemloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }

}
