<?php

namespace App\Http\Controllers\Admin;

use App\Boqtemplate;
use App\BoqTemplatePosition;
use App\BoqTemplateProduct;
use App\Employee;
use App\GlobalSetting;
use App\ManpowerLog;
use App\Module;
use App\Package;
use App\Permission;
use App\PermissionRole;
use App\ProjectPermission;
use App\ProjectsLogs;
use App\PunchItem;
use App\Role;
use App\TaskFile;
use App\TaskPercentage;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Log;
use View;
use App\Bom;
use App\Company;
use App\Condition;
use App\Contractors;
use App\CostItemDescription;
use App\Currency;
use App\Expense;
use App\Helper\Reply;
use App\Http\Requests\Admin\Indent\StoreIndentRequest;
use App\Http\Requests\Admin\Indent\UpdateIndentRequest;
use App\Http\Requests\Admin\Rfq\ConvertRfqRequest;
use App\Http\Requests\Project\StoreProject;
use App\Indent;
use App\IndentProducts;
use App\Payment;
use App\Product;
use App\ProductIssue;
use App\ProjectActivity;
use App\ProjectAttachmentFiles;
use App\ProjectAttachmentDesigns;
use App\ProjectCategory;
use App\ProjectCostItemsBaseline;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsResourceRate;
use App\ProjectFile;
use App\ProjectMember;
use App\ProjectSegmentsPosition;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\Resource;
use App\Rfq;
use App\RfqProducts;
use App\SchedulingDue;
use App\Segment;
use App\SourcingPackage;
use App\SourcingPackageProduct;
use App\Stock;
use App\Store;
use App\Task;
use App\TaskboardColumn;
use App\TenderAssign;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersCondition;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\TmpIndent;
use App\Type;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectMilestone;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use  Excel;
use Session;
use function PHPSTORM_META\type;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\ProjectProgress;
use App\TaskCategory;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\ProjectCostItemsProduct;
use App\ProductCategory;
use App\ProductBrand;
use App\Units;
use App\FileManager;
use App\Helper\Files;
use Illuminate\Support\Facades\Storage;

class ManageChangeOrdersController extends AdminBaseController
{
    use ProjectProgress;

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.changeorder';
        $this->pageIcon = 'icon-layers';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules) && !in_array('stores', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Boq(Request $request,$id){
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->contractorlist = Employee::getAllContractors($user);
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);

            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.changeorders.boq', $this->data);
    }
    public function BoqTitle(Request $request, $id,$title=false){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $title = $title ?: 0;
        $user = $this->user;
        $projectdetails = Project::find($id);
        $this->projectdetails = $projectdetails;
            $columsarray = array(
                'costitem'=> 'Task',
                'description'=>'Description',
                'assign_to'=>'Assign to',
                'ordertype'=>'Order type',
                'contractor' =>'Contractor',
                'startdate'=>'Start date',
                'enddate'=>'End date',
                'unit'=>'Unit',
                'qty'=>'Qty',
                'rate'=>'Rate',
                'amount'=>'Base amount',
                'changeorderrate'=>'CO Rate',
                'changeorderamount'=>'CO Amount',
                'finalrate'=>'Contract rate',
                'totalamount'=>'Contract amount'
            );
            $col =1; foreach($columsarray as $colkey => $colums){
            $postitionarray = ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('itemslug',$colkey)->where('position','col')->first();
            if(empty($postitionarray->id)){
                $columsarray = new ProjectCostItemsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->position = 'col';
                $columsarray->itemid = 0;
                $columsarray->itemname = $colums;
                $columsarray->itemslug = $colkey;
                $columsarray->collock = 0;
                $columsarray->level = 0;
                $columsarray->catlevel = '';
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
               }
             }
        $categoryarray = DB::table('project_cost_items_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
            ->where('project_id',$id)
            ->where('title',$title)
            ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
        $col =1; foreach($boqlevel1categories as  $colums){
            $prevcol = ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('itemid',$colums->id)->first();
            if(empty($prevcol->id)){
                $columsarray = new ProjectCostItemsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->position = 'row';
                $columsarray->itemid = $colums->id;
                $columsarray->itemname = $colums->title;
                $columsarray->itemslug = '';
                $columsarray->collock = 0;
                $columsarray->parent = 0;
                $columsarray->catlevel = '';
                if($colums->parent==0){
                    $columsarray->level = 0;
                    $columsarray->parent = 0;
                }else{
                    $columsarray->level = 1;
                    $columsarray->parent = $colums->parent;
                }
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $this->categories = BoqCategory::where('company_id',$projectdetails->company_id)->get();
        $this->unitsarray = Units::where('company_id',$projectdetails->company_id)->get();
        $this->typesarray = Type::where('company_id',$projectdetails->company_id)->get();
        $this->boqtemplatearray = Boqtemplate::where('company_id',$projectdetails->company_id)->get();
        $this->contractorarray = Employee::getAllContractors($user);
        $this->userarray = User::all();
        $this->title = $title;
        $this->id = (int)$id;
        $itemnoslug = ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('itemslug','itemno')->where('position','col')->first();
        $this->itemnoslug = $itemnoslug;
        $colsarray = array('costitem','description','assign_to','ordertype','contractor','startdate','enddate','rate',
            'changeorderrate','changeorderamount','unit','qty','amount','finalrate','totalamount');
        $this->columsarray = $colsarray;
        return view('admin.changeorders.boqtitlenew', $this->data);
    }
    public function BoqTitleloop(Request $request){

        $subprojectid = $request->subprojectid ?: 0;
        $projectid = $request->projectid ?: 0;
        $contractorid = $request->contractorid ?: 0;
        $projectdetails = Project::find($projectid);
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$projectdetails->company_id)->get();
        $this->unitsarray = Units::where('company_id',$projectdetails->company_id)->get();
        $this->typesarray = Type::where('company_id',$projectdetails->company_id)->get();
        $this->boqtemplatearray = Boqtemplate::where('company_id',$user->company_id)->get();
        $this->contractorarray = Employee::getAllContractors($user);
        $this->employeesarray = Employee::getAllEmployees($user);
        $this->title = $subprojectid;
        $this->id = (int)$projectid;
        $this->contractorid = (int)$contractorid;
        $this->projectdetails = $projectdetails;
        $itemnoslug = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('itemslug','itemno')->where('position','col')->first();
        $this->itemnoslug = $itemnoslug;
        $colsarray = array('costitem','description','assign_to','ordertype','contractor','startdate','enddate','rate',
            'changeorderrate','changeorderamount','unit','qty','amount','finalrate','totalamount');
        $this->columsarray = $colsarray;
        $messageview = View::make('admin.changeorders.boqtitleloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function addBoqCostItemCategory(Request $request){

        if(empty($request->category)){
            return 'Category is empty';
        }
        $user = $this->user;
        $catid = $request->catid;
        $catname = $request->category;
        $parent = $request->parent ?: 0;
        if(empty($request->catid)){
            $boqcategory = BoqCategory::where('title',$catname)->first();
            if(empty($boqcategory)){
                $boqcategory = new BoqCategory();
                $boqcategory->title = $catname;
                $boqcategory->company_id = $user->company_id;
                $boqcategory->parent = $parent ?: 0;
                $boqcategory->save();
            }
            $catid =  $boqcategory->id;
            $catname =  $boqcategory->title;
        }
        $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$request->projectid)->where('title',$request->titleid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new ProjectCostItemsPosition();
        $columsarray->project_id = $request->projectid;
        $columsarray->title = $request->titleid;
        $columsarray->position = 'row';
        $columsarray->itemid = $catid;
        $columsarray->itemname = $catname;
        $columsarray->catlevel = $request->catlevel;
        $columsarray->level = $request->level;
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();
        return 'success';
    }

    public function costitemDestroy($id)
    {
        ProjectCostItemsProduct::where('id',$id)->delete();

        $taskid = Task::where('cost_item_id',$id)->first();
        if(!empty($taskid)){
            TaskPercentage::where('task_id',$taskid->id)->delete();
            TaskFile::where('task_id',$taskid->id)->delete();
            ManpowerLog::where('task_id',$taskid->id)->first();
            PunchItem::where('task_id',$taskid->id)->first();
            ProjectsLogs::where('module_id',$taskid->id)->where('module','tasks')->delete();
            $taskid->delete();
        }
        return Reply::success(__('messages.taskDeletedSuccessfully'));

    }
    public function costitemCatDestroy($id)
    {$x = 1;
        while($x == 1) {
            $position = ProjectCostItemsPosition::where('id',$id)->first();
            if(!empty($position->id)){
                $costproductsarray =  ProjectCostItemsProduct::where('position_id',$position->id)->get();
                if(count($costproductsarray)>0){
                    foreach($costproductsarray as $costproduct){
                        $taskid = Task::where('cost_item_id',$costproduct->id)->first();
                        if(!empty($taskid)){
                            TaskPercentage::where('task_id',$taskid->id)->delete();
                            TaskFile::where('task_id',$taskid->id)->delete();
                            ManpowerLog::where('task_id',$taskid->id)->first();
                            PunchItem::where('task_id',$taskid->id)->first();
                            ProjectsLogs::where('module_id',$taskid->id)->where('module','tasks')->delete();
                            $taskid->delete();
                        }
                        $costproduct->delete();
                    }
                }
                $position->delete();
                $prevposition = ProjectCostItemsPosition::where('parent',$id)->first();
                if(empty($prevposition)){
                    $x = 0;
                }else{
                    $id = $prevposition->id;
                }
            }
        }
        return Reply::success(__('messages.activityDeleted'));

    }
    public function titleDestroy($id)
    {
        $titledata = Title::where('id',$id)->first();
        if(!empty($titledata->id)){
            ProjectCostItemsPosition::where('project_id',$titledata->project_id)->where('title',$titledata->id)->delete();
            ProjectCostItemsProduct::where('project_id',$titledata->project_id)->where('title',$titledata->id)->delete();
            $titledata->delete();
            return Reply::success(__('messages.titleDelete'));
        }

    }
    public function addBoqCostItemRow(Request $request){


        $projectdetails = Project::find($request->projectid);
        $user = $this->user;
        $maxcostitemid = ProjectCostItemsProduct::where('title',$request->titleid)->where('project_id',$request->projectid)->max('inc');
        $newid = $maxcostitemid+1;
        $costitem = $request->itemid;
        $positionid = $request->positionid;
        if(empty($request->itemid)){
            $costvalue = $request->value;
            $costitemdetails = CostItems::where('company_id',$projectdetails->company_id)->where('cost_item_name',$costvalue)->first();
            if(empty($costitemdetails->id)){
                $costitemdetails = new CostItems();
                $costitemdetails->company_id = $projectdetails->company_id;
                $costitemdetails->cost_item_name = $costvalue;
                $costitemdetails->save();
            }
        }else{
            $costitemdetails = CostItems::where('id',$costitem)->first();
        }
        $proprogetdat = new ProjectCostItemsProduct();
        $proprogetdat->title = $request->titleid;
        $proprogetdat->project_id = $request->projectid;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_id = $costitemdetails->id;
        $proprogetdat->position_id = $positionid;
        $proprogetdat->unit = $costitemdetails->unit;
        $proprogetdat->start_date = date('Y-m-d');
        $proprogetdat->deadline = date('Y-m-d');
        $proprogetdat->ordertype = $request->ordertype ?: 'changeorder';
        $proprogetdat->status = 'open';
        $proprogetdat->inc = $newid;
        $proprogetdat->save();

        $newtask = new Task();
        $newtask->heading = get_cost_name($proprogetdat->cost_items_id);
        $newtask->task_category_id = $proprogetdat->category;
        $newtask->project_id = $proprogetdat->project_id;
        $newtask->title = $proprogetdat->title;
        $newtask->cost_item_id = $proprogetdat->id;
        $newtask->boqinclude = 1;
        $newtask->start_date = $proprogetdat->start_date ?: '';
        $newtask->due_date = $proprogetdat->deadline ?: '';
        $newtask->user_id = $proprogetdat->assign_to ?: 0;
        $newtask->description = $proprogetdat->description;
        $newtask->created_by = $user->id;
        $newtask->status = 'not started';
        $newtask->save();

        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $newtask->id;
        $createlog->module = 'tasks';
        $createlog->medium = 'web';
        $createlog->project_id = $newtask->project_id;
        $createlog->subproject_id = $newtask->title ?: 0;
        $createlog->description = $newtask->heading.' task created by '.$user->name.' for the project '.get_project_name($newtask->project_id);
        $createlog->save();

        return 'success';
    }
    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('admin.changeorders.project-boq-create', $this->data);
    }
    public  function boqChangeColPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
         if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function boqChangeCostitemPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function projectBoqLock(Request $request){
        $positionarray = $request->lockcolms;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)&&count($positionarray)>0){
            ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->update(['collock'=>0]);
        }
        return redirect(url('admin/projects/boq/'.$projectid.'/'.$title));
    }
    public  function updateCostItem(Request $request){
        $user = $this->user;
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $projectid = $request->projectid;
        $title = $request->title;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'start_date':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'deadline':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'worktype':
                $worktype = Type::where('title',$itemvalue)->first();
                if(!empty($worktype->id)){
                    $dataarray[$item] = $worktype->id;
                }
                break;
        }
        ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->where('id',$itemid)->update($dataarray);
        $projectcostitem = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->where('id',$itemid)->first();
        $marktype = $projectcostitem->markuptype;
        $markupvalue = $projectcostitem->markupvalue;
        $rate = $projectcostitem->rate;
        $qty = $projectcostitem->qty;
        $adjustment = $projectcostitem->adjustment ?: 0;
        $finalrate = $rate;
        $totalamount = 0;
        if(!empty($qty)&&!empty($rate)){
            $totalamount = $qty*$rate;
            $projectcostitem->finalrate = $rate;
            $projectcostitem->finalamount = $totalamount;
        }
        if(!empty($marktype)&&!empty($markupvalue)){
            if($marktype=='percent'){
                $percent = ($markupvalue/100)*$rate;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            if($marktype=='amount'){
                $percent =$markupvalue;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            $totalamount = $finalrate-$adjustment;
            $projectcostitem->finalrate = $finalrate;
            $projectcostitem->finalamount = $totalamount;
        }
        $projectcostitem->save();

            $prevtask = Task::where('project_id',$projectcostitem->project_id)->where('title',$projectcostitem->title)->where('cost_item_id',$projectcostitem->id)->first();
            if(empty($prevtask->id)){
                $prevtask = new Task();
            }
        $prevtask->heading = get_cost_name($projectcostitem->cost_items_id);
        $prevtask->task_category_id = $projectcostitem->category;
        $prevtask->project_id = $projectcostitem->project_id;
        $prevtask->title = $projectcostitem->title;
        $prevtask->cost_item_id = $projectcostitem->id;
        $prevtask->boqinclude = 1;
        $prevtask->start_date = $projectcostitem->start_date ?: '';
        $prevtask->due_date = $projectcostitem->deadline ?: '';
        $prevtask->user_id = $projectcostitem->assign_to;
        $prevtask->description = $projectcostitem->description;
        $prevtask->save();
    }
    public function boqRateSheet(Request $request,$id){

        $this->pageTitle = 'app.menu.ratesheet';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->categories = array(
            0 => 'Simple',
            1 => 'Complex',
        );
        $this->types = Type::all();
        $this->resources = Resource::all();
        $this->complexresources = Resource::where('category','1')->get();
        $this->product_cost_item = $id;
        $this->cost_item_resources = ProjectCostItemsResourceRate::where('project_cost_item',$id)->get();
        $ret = view('admin.changeorders.boqratesheet', $this->data)->renderSections()['content'];
        return $ret;
    }
    public function addRateToProductCost(Request $request){

        $ratesheetid = $request->ratesheetid;
        $costitem = $request->costitem;
        $resource = $request->resource;
        $resourcedata = Resource::find($resource);
        $resourcerate = ProjectCostItemsResourceRate::where('id',$ratesheetid)->first();
        if(empty($resourcerate->id)){
            $resourcerate = new ProjectCostItemsResourceRate();
        }
        $resourcerate->project_cost_item = $costitem;
        $resourcerate->resource_id = $resourcedata->id;
        $resourcerate->name = $resourcedata->description;
        $resourcerate->unit = $resourcedata->unit ?: '';
        $resourcerate->rate = $resourcedata->rate ?: 0;
        $resourcerate->final_rate = $resourcedata->rate ?: 0;
        $resourcerate->save();
        $ret = array();
        $ret['id'] = $resourcerate->id;
        $ret['category'] = $resourcedata->category;
        $ret['name'] = $resourcerate->name;
        $ret['unit'] = $resourcerate->unit;
        $ret['rate'] = $resourcerate->rate;
        $finalrate =  $resourcerate->final_rate;
        $ret['final_rate'] = $finalrate;
        $result = ProjectCostItemsResourceRate::where('project_cost_item',$costitem)->sum('final_rate');
        $result = number_format($result,'2','.',',');
        ProjectCostItemsProduct::where('id',$costitem)->update(['rate'=>$result]);
        $ret['result'] = $result;
        return json_encode($ret);
    }
    public function addRateSheetStore(Request $request){

        $ratesheetidarray = $request->ratesheetid;
        $rate = $request->rate;
        $unit = $request->unit;
        $formula = $request->formula;
        $result = $request->result;
        $resource = $request->resource;
        $resourcedata = Resource::find($resource);
        if(!empty($ratesheetidarray)){
            foreach ($ratesheetidarray as $key => $ratesheetid){
                $resourcerate = ProjectCostItemsResourceRate::where('id',$ratesheetid)->first();
                if(!empty($resourcerate->id)&&!empty($resource[$key])) {
                    $resourcerate->name = $resource[$key];
                    $resourcerate->unit = !empty($unit[$key]) ? $unit[$key] : '';
                    $resourcerate->rate = !empty($rate[$key]) ? $rate[$key] : 0;
                    $resourcerate->formula = !empty($formula[$key]) ? $formula[$key] : '';
                    $resourcerate->final_rate =  !empty($result[$key]) ? $result[$key] : 0;
                    $resourcerate->save();
                }
            }
        }
        $costitem = $request->product_cost_item;
        $finalrate = ProjectCostItemsResourceRate::where('project_cost_item',$costitem)->sum('final_rate');
        $finalrate = number_format($finalrate,'2','.',',');
        ProjectCostItemsProduct::where('id',$costitem)->update(['rate'=>$finalrate]);
        $ret = array();
        $ret['status'] = 'success';
        $ret['costitem'] = $costitem;
        $ret['result'] = $finalrate;
        return json_encode($ret);
    }

    public function getItemLavel(Request $request){
        $this->categories = BoqCategory::get();
        $costitemlavel = CostItemsLavel::where('boq_category_id', $request->category_id)->groupby('cost_items_id')->get();
        $html = '<option value="">Please select Task</option>';
        foreach ($costitemlavel as $cil){
            $ciid = $cil->cost_items_id;
            $costitem = CostItems::where('id',$ciid)->first();
            $html .= '<option value="'.$costitem->id.'">'.$costitem->cost_item_name.'</option>';
        }
        return $html;
    }

    public function costData(Request $request){
        $id = $request->id;
        if(strpos($id, 'p') !== false){
            $id = ltrim($id, 'p');
             $projectcostitem = ProjectCostItemsProduct::find($id);
            $projectcostitem->start_date = date('d-m-Y',strtotime($projectcostitem->start_date));
            $projectcostitem->deadline = date('d-m-Y',strtotime($projectcostitem->deadline));
            return $projectcostitem;
        }
    }
    public function costDataUpdate(Request $request){
        $assignto = '';
        if(!empty($request->assign_to)){
            $assignto = array_unique(array_filter($request->assign_to));
            $assignto = implode(',',$assignto);
        }
        $id = $request->project_cost_id;
        $linktype = $request->linktype;
        $target = $request->target;
        $milestone = $request->milestone;
        $startdate =  Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        $deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');
        $formprocess = true;
        $message = '';
        $projectcost = ProjectCostItemsProduct::find($id);
        if(!empty($target)){
            $targetcostitem = ProjectCostItemsProduct::find($target);
            switch ($linktype){
                case '0':
                    $targetdeadline = strtotime($targetcostitem->deadline);
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($startdate)));
                    if(strtotime($startdate)>=$targetdeadline){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Start date  should be more than '.$targetcostitem->deadline;
                    }
                    break;
                case '1':
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($startdate)));
                    $targetstartdate = strtotime($targetcostitem->start_date);
                    if(strtotime($startdate)>=$targetstartdate){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Start date should be  more than '.date('Y-m-d',strtotime('-1 days',strtotime($targetcostitem->start_date)));
                    }
                    break;
                case '2':
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('-'.$targetdatediff.' days',strtotime($deadline)));
                    $targetdeadline = strtotime($targetcostitem->deadline);
                    if(strtotime($deadline)>=$targetdeadline){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Due date should be more than '.date('Y-m-d',strtotime('-1 days',strtotime($targetcostitem->deadline)));
                    }
                    break;
                case '3':
                    $targetdatediff =  Carbon::parse($targetcostitem->deadline)->diffInDays(Carbon::parse($targetcostitem->start_date));
                    $targetstartdate = date('Y-m-d',strtotime('-'.$targetdatediff.' days',strtotime($startdate)));
                    $formprocess = true;
                    $targetcostitem->start_date =  date('Y-m-d',strtotime($targetstartdate));
                    $targetcostitem->deadline =  date('Y-m-d',strtotime($startdate));
                    $targetcostitem->save();
                    break;
            }
        }else{
            $targetcost = ProjectCostItemsProduct::where('target',$projectcost->id)->first();
            if(!empty($targetcost->id)){
            $targetlinktype = $targetcost->type;
            switch ($targetlinktype){
                case '0':
                $targetdatediff =  Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                $targetstartdate = date('Y-m-d',strtotime('+1 days',strtotime($deadline)));
                $targetdeaddate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($targetstartdate)));
                $targetcost->start_date =  $targetstartdate;
                $targetcost->deadline =  $targetdeaddate;
                $targetcost->save();
                break;
                case '1':
                    if(strtotime($startdate)>strtotime($targetcost->start_date)){
                        $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                        $targetstartdate = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
                        $targetdeaddate = date('Y-m-d', strtotime('+' . $targetdatediff . ' days', strtotime($targetstartdate)));
                        $targetcost->start_date =  $targetstartdate;
                        $targetcost->deadline = $targetdeaddate;
                        $targetcost->save();
                    }
                break;
                case '2':
                    if(strtotime($deadline)>strtotime($targetcost->deadline)) {
                        $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                        $targetstartdate = date('Y-m-d', strtotime('-' . $targetdatediff . ' days', strtotime($deadline)));
                        $targetcost->start_date = $targetstartdate;
                        $targetcost->deadline = $deadline;
                        $targetcost->save();
                    }
                break;
                case '3':
                    if(strtotime($startdate)>strtotime($targetcost->startdate)) {
                        $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                        $targetdeaddate = date('Y-m-d', strtotime('+' . $targetdatediff . ' days', strtotime($deadline)));
                        $targetcost->start_date = $deadline;
                        $targetcost->deadline = $targetdeaddate;
                        $targetcost->save();
                    }
                break;
                 }
            }
        }
        if($formprocess){
        $projectcost->assign_to = $assignto;
        if(!empty($projectcost->baselinedate)){
            $projectcost->planned_start =  $projectcost->start_date;
            $projectcost->planned_end = $projectcost->deadline;
            $projectcost->start_date = date('Y-m-d',strtotime($startdate));
            $projectcost->deadline = date('Y-m-d',strtotime($deadline));
        }else{
            $projectcost->planned_start =  '';
            $projectcost->planned_end =  '';
            $projectcost->start_date = $startdate;
            $projectcost->deadline =  $deadline;
        }
        if($linktype>=0){
            $projectcost->type = $linktype ?: 0;
        }else{
            $projectcost->type = '';
        }
        $projectcost->target = $target ?: 0;
        $projectcost->linked = !empty($target) ? '1' : '0';
        $projectcost->milestone = !empty($milestone) ? '1' : '0';
        if($projectcost->milestone>0){
            $projectcost->deadline =  $startdate;
        }
        $projectcost->save();
        ProjectCostItemsProduct::where('id',$request->target)->update([
            'linked' => 1
        ]);
        $data['status'] = 'success';
        }else{
            $data['status'] = 'fail';
            $data['message'] = $message;
        }
        return response()->json($data);
    }
    public function baselineUpdate(Request $request){

        $data[] = array();
        $id = $request->projectid;
        $titleid = $request->titleid;
        $baselinedate = !empty($request->baselinedate) ? date('Y-m-d',strtotime($request->baselinedate)) : '';
        $baselinelabel = !empty($request->baselinelabel) ? $request->baselinelabel : '';
        $projectcost = ProjectCostItemsProduct::where('project_id',$id)->where('title',$titleid)->update([
            'baselinedate' => $baselinedate,
            'baselinelabel' => $baselinelabel,
            'planned_start' => '',
            'planned_end' => '',
        ]);
        if(empty($request->baselinedate)){
            $projectcost = ProjectCostItemsProduct::where('project_id',$id)->where('title',$titleid)->update([
                'planned_start' => '',
                'planned_end' => '',
            ]);
        }
        $data['status'] = 'success';
        return response()->json($data);
    }
    public function createBaseline(Request $request){

        $data[] = array();
        $id = $request->projectid;
        $titleid = $request->titleid;
        $baselinelabel = !empty($request->baselinelabel) ? $request->baselinelabel : '';
        $baselinecolor = !empty($request->baselinecolor) ? $request->baselinecolor : '';
        $projectcostarray = ProjectCostItemsProduct::select('id','start_date','deadline')->where('project_id',$id)->where('title',$titleid)->get();
        $projectcostids = array();
        if(count($projectcostarray)>0){
            foreach($projectcostarray as $projectcost){
                $projectcostids[] = $projectcost->id;
                $projectcostdates[$projectcost->id] = array('start_date'=>$projectcost->start_date,'deadline'=>$projectcost->deadline);
            }
        }
        $projectcostjson =
        $projectcostjson = json_encode($projectcostdates);
        $projectcostbaseline = new ProjectCostItemsBaseline();
        $projectcostbaseline->project_id = $id;
        $projectcostbaseline->title = $titleid;
        $projectcostbaseline->label = $baselinelabel;
        $projectcostbaseline->color = $baselinecolor;
        $projectcostbaseline->cost_item_product = implode(',',$projectcostids);
        $projectcostbaseline->dates = $projectcostjson;
        $projectcostbaseline->status = '1';
        $projectcostbaseline->save();
        $data['status'] = 'success';
        return response()->json($data);
    }
    public function removeBaseline(Request $request){

        $id = $request->baselineid;
        $projectcostbaseline = ProjectCostItemsBaseline::find($id);
        $projectcostbaseline->delete();
        $data['status'] = 'success';
        return response()->json($data);
    }
    public function activeBaseline(Request $request){
        $id = $request->baselineid;
        $projectcostbaseline = ProjectCostItemsBaseline::find($id);
        if($projectcostbaseline->status=='1'){
            $projectcostbaseline->status = '0';
        }else{
            $projectcostbaseline->status = '1';
        }
        $projectcostbaseline->save();
        $data['status'] = 'success';
        return response()->json($data);
    }
    public function resourseData(Request $request){
        $data = array();
        $links = array();
        $projectId = $request->projectId;
        $title = $request->title;
        if($projectId != '')
        {
            $assignarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(assign_to SEPARATOR ',')) as `assign_to`"))
                ->where('project_id',$projectId)
                ->where('title',$title)
                ->where('assign_to', '<>', '')
                ->first();
            $assignarray = array_unique(array_filter(explode(',',$assignarray->assign_to)));
            foreach ($assignarray as $assign){
                $resourcework =  ProjectCostItemsProduct::selectRaw("count(*) as count,min(start_date) as start_date,min(deadline) as deadline")->where('project_id',$projectId)
                    ->where('assign_to', '<>', '')
                    ->whereRaw('FIND_IN_SET(?,assign_to)', [$assign])->first();
                $users = User::where('id',$assign)->first();
                $username = !empty($users->name) ? $users->name : '';
                $data[] = [
                    'id' => $assign,
                    'text' => $username,
                    'start_date' => $resourcework->start_date,
                    'work' => $resourcework->count,
                    'parent' => null,
                ];
            }
        }
        return response()->json($data);
    }
    public function schedulingDue(Request $request, $projectId)
    {
        $due = new SchedulingDue();
        $due->title = $request->title;
        $due->project_id = $request->project_id;
        $due->cost_item_id = $request->cost_item_id;
        $due->dependent_id = $request->dependent_id;
        $due->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');;
        $due->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');;
        $due->assign_to = implode(',',$request->assign_to);
        $due->save();
        return back();
    }
    public function boqExcellImport(Request $request){
        $message = '';
        $projectid = (int)$request->projectid;
        $subprojectid = (int)$request->title;
        $user = $this->user;
        if($request->hasFile('excellsheet')){
            Excel::load($request->file('excellsheet')->getRealPath(), function ($reader) use($projectid,$subprojectid,$user) {
                $renderarray = array_filter($reader->toArray());
                if(!empty($renderarray[1])){
                    unset($renderarray[1]);
                    foreach ($renderarray as $key => $row) {
                        $positioncat = $actaarray = array();
                        $arraycount = count($row);
                        $activitycount = $arraycount-6;
                        for($a=1;$a<$activitycount;$a++){
                            $actaarray[] = trim($row['activity_'.$a]);
                        }
                        $parent =   0;
                        $actaarray = array_values(array_filter($actaarray));
                        foreach($actaarray as $key => $activty){
                            $boqcategory = BoqCategory::where('title',$activty)->first();
                            if(empty($boqcategory)){
                                $boqcategory = new BoqCategory();
                                $boqcategory->title = $activty;
                                $boqcategory->company_id = $user->company_id;
                                $boqcategory->parent = $parent ?: 0;
                                $boqcategory->save();
                            }
                            $procostposition = ProjectCostItemsPosition::where("project_id",$projectid)->where("title",$subprojectid)->where('position','row')->where('itemid',$boqcategory->id)->first();
                            if(empty($procostposition)){
                                    $level =   $key+1;
                                    $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('title',$subprojectid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                                    $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
                                    $procostposition = new ProjectCostItemsPosition();
                                    $procostposition->project_id = $projectid;
                                    $procostposition->title = $subprojectid;
                                    $procostposition->position = 'row';
                                    $procostposition->itemid = $boqcategory->id;
                                    $procostposition->itemname = $boqcategory->title;
                                    $procostposition->collock = '0';
                                    $procostposition->catlevel = !empty($positioncat) ? implode(',',$positioncat) : '';
                                    $procostposition->level = $key;
                                    $procostposition->parent = $parent ?: 0;
                                    $procostposition->inc = $newid;
                                    $procostposition->save();
                            }
                            $parent =   $procostposition->itemid;
                            $positioncat[] = $procostposition->itemid;
                        }
                        $unitname = $row['unit'];
                        $units = Units::where('name',$unitname)->first();
                        if(empty($units)){
                            $units = new Units();
                            $units->name = $unitname;
                            $units->company_id = $user->company_id;
                            $units->symbol = '';
                            $units->save();
                        }
                        $taskname = $row['task'];
                        $cost_item = CostItems::where('cost_item_name',$taskname)->first();
                        if(empty($cost_item)){
                            $cost_item = new CostItems();
                            $cost_item->cost_item_name = $taskname;
                            $cost_item->company_id = $user->company_id;
                            $cost_item->unit = $units->id;
                            $cost_item->save();
                        }
                        $projecttask = new ProjectCostItemsProduct();
                        $projecttask->project_id = $projectid;
                        $projecttask->title = $subprojectid;
                        $projecttask->category = !empty($positioncat) ? implode(',',$positioncat) : 0;
                        $projecttask->cost_items_id = $cost_item->id;
                        $projecttask->position_id = $procostposition->id;
                        $projecttask->description = $row['description'];
                        $projecttask->unit = $units->id;
                        $projecttask->qty = $row['quantity'];
                        $projecttask->rate = $row['rate'];
                        $projecttask->start_date  = date('Y-m-d');
                        $projecttask->deadline   = date('Y-m-d');
                        $projecttask->save();

                        $taskcreate = new Task();
                        $taskcreate->project_id = $projectid;
                        $taskcreate->title = $subprojectid;
                        $taskcreate->company_id = $user->company_id;
                        $taskcreate->heading = $row['task'];
                        $taskcreate->description = $row['description'];
                        $taskcreate->user_id = $user->id;
                        $taskcreate->cost_item_id = $projecttask->id;
                        $taskcreate->boqinclude = 1;
                        $taskcreate->task_category_id =$procostposition->itemid;
                        $taskcreate->save();
                    }
                }else{
                    $message = 'Excel sheet is empty.';
                }
            });
        }
            return redirect(route('admin.changeorders.boqtitle',[$projectid,$subprojectid]));
    }
    public function templateSubmit(Request $request){
        $message = '';
        $templateid = (int)$request->templateid;
        $projectid = (int)$request->projectid;
        $subprojectid = (int)$request->title;
        $user = $this->user;
        if(!empty($templateid)){
            $templatepositionarray = BoqTemplatePosition::where('template_id',$templateid)->where('position','row')->get();

            foreach($templatepositionarray as  $templateposition){
                $procostposition = ProjectCostItemsPosition::where("project_id",$projectid)->where("title",$subprojectid)->where('itemid',$templateposition->itemid)->where('position','row')->where('parent',$templateposition->parent)->where('level',$templateposition->level)->first();
                if(empty($procostposition)){
                    $procostposition = new ProjectCostItemsPosition();
                    $procostposition->project_id = $projectid;
                    $procostposition->title = $subprojectid;
                    $procostposition->position = $templateposition->position;
                    $procostposition->itemid = $templateposition->itemid;
                    $procostposition->itemname = $templateposition->itemname;
                    $procostposition->collock = $templateposition->collock;
                    $procostposition->catlevel = $templateposition->catlevel;
                    $procostposition->level = $templateposition->level;
                    $procostposition->parent = $templateposition->parent;
                    $procostposition->inc = $templateposition->inc;
                    $procostposition->save();
                }
                if(!empty($procostposition->catlevel)){
                    $categoryid = $procostposition->catlevel.','.$procostposition->itemid;
                }else{
                    $categoryid = $procostposition->itemid;
                }
                $boqtemplateproductsarray = BoqTemplateProduct::where("template_id",$templateid)->where("position_id",$templateposition->id)->get();
               foreach ($boqtemplateproductsarray as $boqtemplateproduct){
                    $procostproduct = ProjectCostItemsProduct::where("project_id",$projectid)->where("title",$subprojectid)->where('position_id',$procostposition->id)->where('category',$categoryid)->first();
                   if(empty($procostproduct->id)) {
                        $projecttask = new ProjectCostItemsProduct();
                       $projecttask->project_id = $projectid;
                        $projecttask->title = $subprojectid;
                        $projecttask->category = $categoryid;
                        $projecttask->cost_items_id = $boqtemplateproduct->cost_items_id;
                        $projecttask->position_id = $procostposition->id;
                        $projecttask->description = $boqtemplateproduct->description;
                        $projecttask->unit = $boqtemplateproduct->unit;
                        $projecttask->qty = $boqtemplateproduct->qty;
                        $projecttask->rate = $boqtemplateproduct->rate;
                        $projecttask->start_date = date('Y-m-d');
                        $projecttask->deadline = date('Y-m-d');
                        $projecttask->finalamount = $boqtemplateproduct->finalamount;
                        $projecttask->finalrate = $boqtemplateproduct->finalrate;
                        $projecttask->worktype = $boqtemplateproduct->worktype;
                        $projecttask->markuptype = $boqtemplateproduct->marktype;
                        $projecttask->markupvalue = $boqtemplateproduct->markvalue;
                        $projecttask->markupvalue = $boqtemplateproduct->markvalue;
                        $projecttask->inc = $boqtemplateproduct->inc;
                        $projecttask->save();
                        $taskcreate = new Task();
                        $taskcreate->project_id = $projectid;
                        $taskcreate->title = $subprojectid;
                        $taskcreate->company_id = $user->company_id;
                        $taskcreate->heading = get_cost_name($boqtemplateproduct->cost_items_id);
                        $taskcreate->description = $boqtemplateproduct->description;
                        $taskcreate->user_id = $user->id;
                        $taskcreate->cost_item_id = $projecttask->id;
                        $taskcreate->boqinclude = 1;
                        $taskcreate->task_category_id = $procostposition->itemid;
                        $taskcreate->save();
                    }
                }
            }
        }else{
            $message = 'Select a template.';
        }
        return redirect(route('admin.changeorders.boqtitle',[$projectid,$subprojectid]));
    }

    public function ProjectFinalQty(Request $request, $id){
        $title = $request->title;
        $category = $request->category;
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $qty = $request->total_qty;
        $total_amount = $request->total_amount;
        $description = $request->description;
        $coid = $request->coid;
        foreach ($qty as $key=>$value) {
            ProjectCostItemsFinalQty::where('title', $title)->where('project_id', $id)->where('cost_item_id', $cost_item_id[$key])->delete();
        }
        foreach ($qty as $key=>$value) {
            if ($value != '' && $value != "0") {
                $pcip = new ProjectCostItemsFinalQty();
                $pcip->title = $title;
                $pcip->category = $category[$key];
                $pcip->project_id = $project_id;
                $pcip->cost_item_id = $cost_item_id[$key];
                $pcip->qty = $value;
                $pcip->total_amount = $total_amount[$key];
                $pcip->save();
            }
        }
        foreach ($description as $key=>$des){
            $cid = new CostItemDescription();
            $cid->boq_id = $id;
            $cid->cost_item_id = $key;
            $cid->description = $des;
            $cid->save();
        }
        return redirect()->route('admin.changeorders.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function titleCreate(Request $request, $id){
        $title = $request->title;
        $t = new Title();
        $t->title = $title;
        $t->project_id = $id;
        $t->save();
        return redirect()->route('admin.changeorders.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function getFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                        foreach($this->files as $file){
                            $path = $file->id;
                            $html .= '<tr>
                                <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                            $html .='</tr>';
                        }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                        foreach($this->files as $file){
                            $path = $file->id;
                            $html .= '<tr>
                                <td>';
                                if($file->type == 'file') {
                                    $html .= '<i class="ti-file"></i> ' . $file->filename;
                                }else {
                                    $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                        <i class="ti-folder"></i> ' . $file->filename . '
                                    </a>';
                                }
                                $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
                                if($file->type == 'file') {
                                    $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                }
                                if($file->type == 'folder') {
                                    $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                }
                                $html .='</td>
                            </tr>';
                        }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function showFiles(Request $request, $id){

        $this->pageTitle = __('app.menu.drawings');

        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->get()->pluck('project_id');
        $this->projectlist = Project::where('company_id', $user->company_id)->orWhereIn('id', $pids)->get();
        if(is_numeric($id)){

        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $this->files = FileManager::where('parent',$parent)->where('project_id',$id)->where('pathtype','outline')->where('revisionfileid','0')->get();
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->id = $id;
        }else{

            $parent = 0;
            $filemanager = array();
            if(isset($_GET['path'])){
                $parent = $_GET['path'];
                $filemanager = \App\FileManager::where('id',$_GET['path'])->first();
            }
            $this->id = 'all';
            $this->files = FileManager::where('parent',$parent)->where('revisionfileid','0')->get();
            $this->foldername = FileManager::where('id',$parent)->first();
            $this->filemanager = $filemanager;
        }
        return view('admin.changeorders.project-files.show', $this->data);
    }
    public function revisionFiles(Request $request, $id,$fileid){
        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->get()->pluck('project_id');
        $this->projectlist = Project::where('company_id', $user->company_id)->orWhereIn('id', $pids)->get();
            if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);
            $parent = 0;
            if(isset($_GET['path'])){
                $parent = $_GET['path'];
            }
            $b = FileManager::where('id',$fileid)->orderby('id','desc')->get();
            $a = FileManager::where('parent',$parent)->where('project_id',$id)->where('pathtype','outline')->where('revisionfileid',$fileid)->orderby('id','desc')->get();
            $this->files = $a->merge($b);
            $this->foldername = FileManager::where('id',$parent)->first();
            $this->id = $id;
        }else{
            $this->id = 'all';
        }
        return view('admin.changeorders.project-files.revisionfiles', $this->data);
    }

    public function selectFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentFiles();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteFile($id)
    {
        $project = ProjectAttachmentFiles::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function getDesignFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                            foreach($this->files as $file){
                                $path = $file->id;
                                $html .= '<tr>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                    $html .='</td>
                                    <td>'.$file->type.'</td>
                                    <td>'.$file->size .'Bytes</td>
                                    <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                                    $html .='</td>
                                </tr>';
                            }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openDesignFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                            foreach($this->files as $file){
                                $path = $file->id;
                                $html .= '<tr>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                    $html .='</td>
                                    <td>'.$file->type.'</td>
                                    <td>'.$file->size .'Bytes</td>
                                    <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                                    $html .='</td>
                                </tr>';
                            }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function showDesignFiles(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->files = ProjectAttachmentDesigns::where('project_id',$id)->get();
        $this->id = $id;
        return view('admin.changeorders.project-files.show-design', $this->data);
    }

    public function selectDesignFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentDesigns();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteDesignFile($id)
    {
        $project = ProjectAttachmentDesigns::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function projectEditFile(Request $request, $projectId, $fileId){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($projectId);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $file = FileManager::findOrFail($fileId);
        $parent = $file->parent;
        $filePath = $file->hashname;
        $mime = explode('.', $filePath);
        while ($parent){
            $filep = \App\FileManager::findOrFail($parent);
            $filePath = $filep->filename.'/'.$filePath;
            $parent = $filep->parent;
        }
        /*$this->urlx = $this->project->project_name.'/'.$filePath;*/
        $this->urlx =  $filePath;
        $this->queryUrl = '&user='.Auth::user()->id.'&project='.$projectId.'&file_id='.$fileId.'&username='.Auth::user()->name;
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->file = $file;
        $this->id = $projectId;
        return view('admin.changeorders.project-files.edit-file', $this->data);
    }

    public function sourcingPackages(Request $request, $id){
        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->get()->pluck('project_id');
        $this->projectlist = Project::where('company_id', $user->company_id)->orWhereIn('id', $pids)->get();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        if(is_numeric($id)){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->id = $id;
        }else{
            $this->id = $id;
        }
        return view('admin.changeorders.sourcing-package', $this->data);
    }

    public function addSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$id)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->id = $id;
        return view('admin.changeorders.sourcing-package-create', $this->data);
    }

    public function storeSourcingPackages(Request $request, $id){
        $source = new SourcingPackage();
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            $pro = $request->products;
            for($j = 0; $j < count($pro); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $pro[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('admin.changeorders.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function editSourcingPackages(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->id = $pid;
        return view('admin.changeorders.sourcing-package-edit', $this->data);
    }

    public function updateSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $source = SourcingPackage::findOrFail($id);
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            SourcingPackageProduct::where('sourcing_package_id',$source->id)->delete();
            $cfp = $request->products;
            for($j = 0; $j < count($cfp); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $cfp[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('admin.changeorders.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function getProducts(Request $request){
        $html = '';
        $cfk = $request->create_from;
        for($k = 0; $k < count($cfk); $k++ ) {
            $cats = ProjectCostItemsProduct::where('title', $cfk[$k])->where('project_id', $request->project_id)->groupBy('title')->get();
            foreach ($cats as $cat) {
                $xcat = explode(',', $cat->category);

                for ($i = 0; $i < count($xcat); $i++) {
                    $name = BoqCategory::where('id', $xcat[$i])->first();
                    if($name->id !== null) {
                        $html .= '<option value="' . $name->id . '">' . $name->title . '</option>';
                    }
                }
            }
        }
        return $html;
    }

    public function filterData(Request $request){
        $html = '';
        $cf = $request->create_from;
        $html .= '
                <input type="hidden" name="project_id" value="'.$request->project_id.'"/>
                <input type="hidden" name="title" value="'.$request->title.'"/>';
        foreach($request->create_from as $caf) {
            $html .= '<input type="hidden" name="create_from[]" value="' . $caf . '"/>';
        }
        foreach($request->category as $c) {
            $html .= '<input type="hidden" name="category[]" value="' . $c . '"/>';
        }
        foreach($request->type as $t) {
            $html .= '<input type="hidden" name="type[]" value="' . $t . '"/>';
        }
        $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Product</b></div>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Quantity</b></div>
                </div>';
        for($j = 0; $j < count($cf); $j++ ) {
            $products = ProjectCostItemsProduct::where('title',$cf[$j])->where('project_id', $request->project_id)
                ->join('category', 'category.id', '=', 'project_cost_items_product.product_category_id')
                ->join('product_type', 'product_type.product_id', '=', 'project_cost_items_product.product_category_id')
                ->where('product_type.type',$request->type[$j])
                ->select('category.name as name', 'category.id as id')
                ->get();
            foreach ($products as $product) {
                $fqty = DB::table('project_cost_item_final_qty')
                    ->where('title',$cf[$j])
                    ->where('project_id', $request->project_id)
                    ->sum('qty');
                $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;"> 
                        <input type="checkbox" name="products[]" value="' . $product->id . '" class="form-check-input"> 
                        ' . $product->name . '
                    </label>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;">';
                        if($fqty) $html .= $fqty;
                    $html .= '</label>
                </div>';
            }
        }
        return $html;
    }

    public function deleteSourcingPackages($id)
    {
        $project = SourcingPackage::findOrFail($id);
        $project->forceDelete();
        $sp = SourcingPackageProduct::where('sourcing_package_id',$id)->delete();
        return Reply::success(__('Package deleted successfully'));
    }

    public function createTender(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_packages_product.sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->pid = $pid;
        $this->id = $id;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        return view('admin.changeorders.create-tender', $this->data);
    }

    public function getBrandsTag(Request $request){
        $q = $_GET['query'];
        $query = ProductBrand::where('name','LIKE','%'.$q.'%')->select('id', 'name')->get();
        $return = array();
        if($query){
            foreach($query as $q) {
                $return[] = $q;
            }
        }
        $json = json_encode($return);
        return $json;
    }
    public function saveTenders(Request $request){
        $tender = new Tenders();
        $tender->company_id = $this->user->company_id;
        $tender->project_id = $request->project_id;
        $tender->sourcing_package_id = $request->sourcing_package_id;
        $tender->name = $request->tender_name;
        $tender->save();
        if($tender->id){
            $cfp = $request->conditions;
            for($j = 1; $j < count($cfp); $j++ ) {
                $sourcep = new TendersCondition();
                $sourcep->tender_id = $tender->id;
                $sourcep->condition_id = $cfp[$j];
                $sourcep->terms = $request->terms[$j];
                $sourcep->save();
            }
            foreach($request->products as $key=>$pi ) {
                $tp = new TendersProduct();
                $tp->tender_id = $tender->id;
                $tp->products = $pi;
                $tp->qty = $request->qty[$key];
                $tp->brands = $request->brand_ids[$key];
                $tp->save();
            }
        }
        return Reply::dataOnly(['taskID' => $tender->id]);
    }

    public function storeTenderFiles(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.cloud');
                $file = new TendersFiles();
                $file->user_id = $this->user->id;
                $file->tender_id = $request->task_id;
                $file->company_id = $this->user->company_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/tender-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('tender-files/'.$request->task_id, $fileData, $fileData->hashName(), 'public');
                    break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'tender-files')
                            ->first();
                        if(!$dir) {
                            Storage::cloud()->makeDirectory('tender-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();
                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                    break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('tender-files/'.$request->tender_id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/tender-files/'.$request->tender_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                    break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::redirect(route('admin.changeorders.create-tender'), __('modules.changeorders.projectUpdated'));
    }

    public function tendersList(Request $request, $pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$id)->first();
        $this->tenders = Tenders::where('company_id',$this->user->company_id)
            ->where('project_id',$pid)->where('sourcing_package_id',$id)->get();
        $this->id = $id;
        $this->pid = $pid;
        return view('admin.changeorders.tenders-list', $this->data);
    }

    public function editTender(Request $request, $pid, $id, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$sid)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_packages_product.sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        return view('admin.changeorders.edit-tender', $this->data);
    }

    public function assignTender(Request $request, $pid, $sid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$id)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->sid = $sid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$sid)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_packages_product.sourcing_package_id',$sid)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        $this->employees = User::allEmployees();
        return view('admin.changeorders.assign-tender', $this->data);
    }

    public function storeAssignTender(Request $request){
        foreach($request->user_id as $user) {
            $tenderAssign = new TenderAssign();
            $tenderAssign->company_id = $this->user->company_id;
            $tenderAssign->project_id = $request->project_id;
            $tenderAssign->tender_id = $request->tender_id;
            $tenderAssign->sourcing_id = $request->sourcing_id;
            $tenderAssign->user_id = $user;
            $tenderAssign->save();
        }
        return back();
    }

    public function bidding(Request $request, $company_id, $project_id, $tender_id, $sourcing_id, $user_id){
        $this->assign_tender = TendersCondition::where('company_id', $company_id)
            ->where('project_id', $project_id)
            ->where('tender_id', $tender_id)
            ->where('sourcing_id', $sourcing_id)
            ->where('user_id', $user_id)
            ->first();
        if($this->assign_tender){
            $this->tender = Tenders::where('company_id',$company_id)
                ->where('id',$tender_id)->first();
            $this->company_id = $company_id;
            $this->project_id = $project_id;
            $this->tender_id = $tender_id;
            $this->sourcing_id = $sourcing_id;
            $this->user_id = $user_id;
            $this->conditions = Condition::all();
            $this->brands = ProductBrand::all();
            $this->packages = SourcingPackage::where('company_id',$company_id)->get();
            $this->costitems = ProjectCostItemsProduct::where('project_id',$project_id)
                ->join('title','title.id','=','project_cost_items_product.title')
                ->select('title.id','title.title')
                ->groupBy('project_cost_items_product.title')
                ->get();
            $this->package = SourcingPackage::where('id',$sourcing_id)->first();
            $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$sourcing_id)
                ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
                ->select('category.name as name', 'category.id as id')
                ->get();
            $this->cons = TendersCondition::where('tender_id', $tender_id)->get();
            $this->pros = TendersProduct::where('tender_id', $tender_id)->get();
            $this->files = TendersFiles::where('tender_id', $tender_id)->get();
        }
        return view('admin.changeorders.bidding', $this->data);
    }

    public function tenderBiddings(Request $request){
        $this->tenders = Tenders::where('company_id',$this->user->company_id)->get();
        return view('admin.changeorders.tenders-biddings', $this->data);
    }

    public function tenderBiddingDetails(Request $request, $id){
        $supIds = TenderBidding::where('company_id',$this->user->company_id)->where('tender_id', $id)->pluck('user_id');
        $this->users = User::whereIn('id', $supIds)->get();
        $this->tenders = TenderBidding::where('company_id',$this->user->company_id)->where('tender_id',$id)->get();
        $this->tender = Tenders::where('company_id',$this->user->company_id)->where('id',$id)->first();
        $this->products = TenderBiddingProduct::where('tender_id', $id)->groupBy('products')->get();
        return view('admin.changeorders.tender-bidding-detail', $this->data);
    }

    public function productIssue(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->products = ProductCategory::get();
        $this->issuedProducts = ProductIssue::where('project_id',$id)->groupBy('unique_id')->get();
        $this->projectId = $id;
        return view('admin.changeorders.product-issue', $this->data);
    }

    public function createIssuedProduct(Request $request, $id){
        $this->products = ProductCategory::get();
        $this->units = Units::get();
        $this->projectId = $id;
        return view('admin.changeorders.create-product-issue', $this->data);
    }

    public function storeIssuedProduct(Request $request){
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $unique_id = 'PI-'.date("d-m-Y").'-'.count($product_id);
        $unit_id = $request->unit_id;
        $quantity = $request->quantity;
        $issued_by = $this->user->id;

        foreach($product_id as $key=>$value ){
            $pro = new ProductIssue();
            $pro->project_id = $project_id;
            $pro->product_id = $value;
            $pro->unique_id = $unique_id;
            $pro->unit_id = $unit_id[$key];
            $pro->quantity = $quantity[$key];
            $pro->issued_by = $issued_by;
            $pro->save();
        }

        return Reply::success(__('Product issued successfully'));
    }

    public function issuedProductDetail(Request $request,$pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        return view('admin.changeorders.issued-product-detail', $this->data);
    }

    public function returnIssuedProduct(Request $request,$pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        return view('admin.changeorders.return-issued-product', $this->data);
    }

    public function storeReturnedProduct(Request $request){
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $unique_id = 'PI-'.date("d-m-Y").'-'.count($product_id);
        $unit_id = $request->unit_id;
        $quantity = $request->quantity;
        $issued_by = $this->user->id;

        foreach($product_id as $key=>$value ){
            $pro = new ProductIssue();
            $pro->project_id = $project_id;
            $pro->product_id = $value;
            $pro->unique_id = $unique_id;
            $pro->unit_id = $unit_id[$key];
            $pro->quantity = $quantity[$key];
            $pro->issued_by = $issued_by;
            $pro->save();
        }

        return Reply::success(__('Product issued successfully'));
    }

    public function showStoresProducts(Request $request, $id, $sid)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->products = Bom::where('store_id',$sid)->where('project_id',$id)->get();
        $this->store = Store::where('id',$sid)->first();
        $this->units = Units::get();
        return view('admin.changeorders.show-store-products', $this->data);
    }

    public function bom($pid,$sid){
        $this->products = Product::where('company_id',$this->user->company_id)->get();
        $this->pid = $pid;
        $this->sid = $sid;
        return view('admin.changeorders.bom', $this->data);
    }

    public  function storeBom(Request $request){
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $trade_id = $request->trade_id;
        foreach ($product_id as $key=>$value) {
            $check = Bom::where('project_id', $project_id)->where('store_id', $store_id)->where('product_id', $value)->where('trade_id', $trade_id[$key])->first();
            if($check === null){
                $bom = new Bom();
                $bom->project_id = $project_id;
                $bom->store_id = $store_id;
                $bom->product_id = $value;
                $bom->trade_id = $trade_id[$key];
                $bom->save();
            }
        }
        return Reply::success(__('Material added successfully'));
    }

    public  function updateBom(Request $request){
        $id = $request->id;
        $unit_id = $request->unit_id;
        $est_qty = $request->est_qty;
        $est_rate = $request->est_rate;
        $opening_stock = $request->opening_stock;
        foreach ($id as $key=>$value){
            $bom = Bom::findOrFail($value);
            $bom->unit_id = $unit_id[$key];
            $bom->est_qty = $est_qty[$key];
            $bom->est_rate = $est_rate[$key];
            $bom->opening_stock = $opening_stock[$key];
            $bom->save();
        }
        return Reply::success(__('Material updated successfully'));
    }

    public function showStoresStock(Request $request,$id, $sid)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);

        $this->products = Bom::where('store_id',$sid)->where('project_id',$id)->get();
        $this->store = Store::where('id',$sid)->first();
        $this->units = Units::get();
        return view('admin.changeorders.show-store-stock', $this->data);
    }

    public function stockProjectData (Request $request, $id, $sid)
    {
        $pos = Stock::where('store_id', $sid)->where('project_id', $id)->groupBy('cid')->get();
        $store_id = $sid;
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user, $store_id) {
                $ret = '';
                if($store_id == 0) {
                    $ret = '<a href="' . route('member.inventory.purchase-history', [$row->bid]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }else{
                    $ret = '<a href="' . route('member.inventory.purchase-history', [$row->bid, $store_id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'cid',
                function ($row) {
                    return get_procat_name($row->cid);
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'est_qty',
                function ($row) use ($id, $sid) {
                    return get_est_qty($id, $sid, $row->cid);
                }
            )
            ->editColumn(
                'est_rate',
                function ($row) use ($id, $sid) {
                    return get_est_rate($id, $sid, $row->cid);
                }
            )
            ->editColumn(
                'indented_qty',
                function ($row) use ($id, $sid) {
                    return get_indented_qty($id, $sid, $row->cid);
                }
            )
            ->editColumn(
                'issued_project',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'stock',
                function ($row) use ($id, $sid) {
                    return getProjectStoreStock($id, $sid, $row->cid);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function indentCreate($leadID = null, Request $request, $pid, $sid)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->lproducts = Product::all();
        $this->projects = Project::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->pid = $pid;
        $this->sid = $sid;
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('admin.changeorders.create-indent', $this->data);
    }

    public function indentCreateNew(Request $request, $pid, $sid)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);

        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->lproducts = Product::all();
        $this->pid = $pid;
        $this->sid = $sid;
        $this->store = Store::where('id',$sid)->first();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('admin.changeorders.create-indent', $this->data);
    }

    public function indentGetBrands(Request $request){
        $pid = $request->pid;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $bids = ProductCategory::where('id', $pid)->select('brands')->first();
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = array();
        $html['estQty'] = get_est_qty($project_id, $store_id, $pid);
        $html['reqQty'] = get_est_qty($project_id, $store_id, $pid);

        if(count($brands)){
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
            foreach($brands as $brand){
                $html['brands'] .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
            }
        }else{
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
        }

        return $html;
    }
    public function indentBrand(Request $request)
    {
        $category = new ProductBrand();
        $category->name = $request->brand;
        $category->save();

        $bid = $category->id;

        $brand = ProductBrand::where('id', $bid)->first();
        $html = '<option value="'.$brand->id.'" selected>'.$brand->name.'</option>';

        return $html;
    }
    public function indentStoreTmp(Request $request){
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $tmpdata = new TmpIndent();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->dated = $request->dated;
        $tmpdata->remark = $request->remark;
        $tmpdata->save();

        $allData = TmpIndent::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function indentDeleteTmp(Request $request){
        $sid = null;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        TmpIndent::where('id', $request->did)->delete();
        $allData = TmpIndent::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function indentStore(StoreIndentRequest $request)
    {
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $sr = 1;
        $ind = Indent::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->indent_no);
            $sr = $in[2];
            $sr++;
        }
        $indent_no = 'IND/'.date("Y").'/'.$sr;
        $indent = new Indent();
        $indent->indent_no = $indent_no;
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remark;
        $indent->save();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.changeorders.indents'));
    }

    public function indentEdit(Request $request, $pid, $sid, $id)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);

        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->lproducts = Product::all();
        $this->projects = Project::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->pid = $pid;
        $this->sid = $sid;
        $this->store = Store::where('id',$sid)->first();
        if($request->session()->has('indentTmpDataSession')) {
            $ssid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $ssid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $ssid = null;
        if($request->session()->has('indentTmpDataSession')){
            $ssid = $request->session()->get('indentTmpDataSession');
        }else{
            $ssid = uniqid();
            $request->session()->put('indentTmpDataSession', $ssid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $ssid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $ssid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('admin.changeorders.edit-indent', $this->data);
    }

    public function indentUpdate(UpdateIndentRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remark;
        $indent->save();
        IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.changeorders.indents'));
    }

    public function indentDestroy($id)
    {
        DB::beginTransaction();
        Indent::destroy($id);
        IndentProducts::where('indent_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.indentDeleted'));
    }
    public function indentApprove($id, $val)
    {
        DB::beginTransaction();
        $ind = Indent::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('Indent updated successfully');
    }

    public function indentData(Request $request, $pid, $sid)
    {
        $indents = Indent::join('stores', 'stores.id', '=', 'indents.store_id');
        if($pid){
            $indents = $indents->where('stores.project_id', $pid);
        }
        if ($sid != 'all' && $sid != '') {
            $indents = $indents->where('store_id', $sid);
        }
        if ($request->status != 'all' && $request->status != '') {
            $indents = $indents->where('status', $request->status);
        }
        $indents = $indents->select(['indents.*'])->get();
        $user = Auth::user();
        return DataTables::of($indents)
            ->addColumn('action', function ($row) use ($user, $pid, $sid) {
                $ret = '';
                if($user->can('edit_indent') && $row->status == 0){
                    $ret .= '<a href="' . route('member.changeorders.indent.edit', [$pid, $sid, $row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('delete_indent') && $row->status == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('approve_indent') && $row->status == 0 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                if($user->can('approve_indent') && $row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }

                if($user->can('add_rfq') && $row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="' . route('member.changeorders.indent.gconvert', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To RFQ"><i class="fa fa-send" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending RFQ';
                    }else if($row->status == 1){
                        return 'Pending PO';
                    }else if($row->status == 2){
                        return 'Pending Purchase';
                    }else{
                        return 'Purchase Done';
                    }
                }
            )
            ->editColumn(
                'approve',
                function ($row) {
                    if($row->approve == 0){
                        return 'Pending Approval';
                    }else if($row->approve == 1){
                        return 'Approved';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function indentConvert($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('admin.indent.convert', $this->data);
    }
    public function indentPostConvert($id, ConvertRfqRequest $request){
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $rfq = new Rfq();
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq->rfq_no = $rfq_no;
        $rfq->indent_no = $indent->indent_no;
        $rfq->store_id = $indent->store_id;
        $rfq->remark = $indent->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        //IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        $indent->status = 1;
        $indent->save();
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    public function indentExport($status, $indent)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'indent')
            ->where('roles.company_id', company()->id)
            ->leftJoin('indent_details', 'users.id', '=', 'indent_details.user_id')
            ->select(
                'users.id',
                'indent_details.name',
                'indent_details.email',
                'indent_details.mobile',
                'indent_details.company_name',
                'indent_details.address',
                'indent_details.website',
                'indent_details.created_at'
            )
            ->where('indent_details.company_id', company()->id);
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }
        if ($indent != 'all' && $indent != '') {
            $rows = $rows->where('users.id', $indent);
        }
        $rows = $rows->get()->makeHidden(['image']);
        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];
        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }
        // Generate and return the spreadsheet
        Excel::create('indents', function ($excel) use ($exportArray) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('indents file');
            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function stores($id)
    {
        $this->userDetail = auth()->user();

        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        $isMember = ProjectMember::checkIsMember($id, $this->user->id);

        // Check authorised user

            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if ($this->project->deadline) {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
            $minute = 0;
            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }

            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);

            return view('admin.changeorders.show-stores', $this->data);
    }
    public function indents($id, $sid)
    {
        $this->indents = Indent::where('project_id', $id)->where('id',$sid)->get();
        $this->stores = Store::where('project_id', $id)->get();
        $this->userDetail = auth()->user();
        $this->store = Store::where('id',$sid)->first();

        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        $isMember = ProjectMember::checkIsMember($id, $this->user->id);

        // Check authorised user

            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if ($this->project->deadline) {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
            $minute = 0;
            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }

            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);

            return view('admin.changeorders.show-indents', $this->data);
    }

    public function rfqs(Request $request, $pid, $sid)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $pid)->get();
        $this->userDetail = auth()->user();
        $this->store = Store::where('id',$sid)->first();

        $this->project = Project::findOrFail($pid)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        $isMember = ProjectMember::checkIsMember($pid, $this->user->id);

        // Check authorised user

            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if ($this->project->deadline) {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
            $minute = 0;
            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }

            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($pid, 10, $this->userDetail->id);

            return view('admin.changeorders.show-rfqs', $this->data);
    }

    public function quotations(Request $request, $pid, $sid)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $pid)->get();
        $this->store = Store::where('id', $sid)->first();
        $this->userDetail = auth()->user();

        $this->project = Project::findOrFail($pid)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        $isMember = ProjectMember::checkIsMember($pid, $this->user->id);

        // Check authorised user

            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if ($this->project->deadline) {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
            $minute = 0;
            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }

            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($pid, 10, $this->userDetail->id);

            return view('admin.changeorders.show-quotes', $this->data);
    }

    public function purchaseOrders($id)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $id)->get();
        $this->userDetail = auth()->user();

        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        $isMember = ProjectMember::checkIsMember($id, $this->user->id);

        // Check authorised user

            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if ($this->project->deadline) {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
            $minute = 0;
            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }

            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);

            return view('admin.changeorders.show-po', $this->data);
    }
    public function getTitlesByProjects(Request $request){
        $projectid = $request->projectid;
        $projectsarray = array();
        if($projectid){
            $projectsarray['titles'] = Title::where('project_id',$projectid)->pluck('title','id');
            $projectsarray['segments'] = Segment::where('projectid',$projectid)->pluck('name','id');
            $projectsarray['cositems'] = ProjectCostItemsProduct::join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')->where('project_cost_items_product.project_id',$projectid)->pluck('cost_items.cost_item_name','project_cost_items_product.id');
         }
        return  json_encode($projectsarray);
    }
    public function getSegmentsByProjects(Request $request){
        $projectid = $request->projectid;
        $titleid = $request->subprojectid;
        $projectsarray = array();
        if($projectid){
            $segments = Segment::where('projectid',$projectid);
            if($titleid){
                $segments = $segments->where('titleid',$titleid);
            }
            $projectsarray['segments'] = $segments->pluck('name','id');
        }
        return  json_encode($projectsarray);
    }
    public function getCostitemByTitle(Request $request){
        $projectid = $request->projectid;
        $title = $request->title;
        $projectsarray = array();
        if($projectid){
            $projectsarray['cositems'] = ProjectCostItemsProduct::join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')->where('project_cost_items_product.project_id',$projectid)->pluck('cost_items.cost_item_name','project_cost_items_product.id');
        }
        return  json_encode($projectsarray);
    }
    public function getProgressReport(){
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectarray = Project::whereIn('id',$projectlist)->get();
        $this->contractorsarray = Contractors::where('company_id',$user->company_id)->pluck('name','user_id')->toArray();
         $dataarray = array();
        $dataarray['projectid'] = '';
        $dataarray['subprojectid'] = '';
        $dataarray['segmentid'] = '';
        $dataarray['startdate'] =   \Carbon\Carbon::today()->subDays(7)->format('Y-m-d');
        $dataarray['enddate'] = \Carbon\Carbon::today()->format('Y-m-d');
        $dataarray['dateformat'] = $this->global->date_format;
        $dataarray['exportype'] = '';
        $dataarray['tasksimagesarray'] = array();
        $dataarray['contractorarray'] = array();
        $dataarray['issueslist'] = array();
        $dataarray['indentslist'] = array();
        $dataarray['notstarted'] = '';
        $dataarray['inprogress'] = '';
        $dataarray['delayed'] =  '';
        $dataarray['completed'] = '';
        $dataarray['inproblem'] = '';
        $messageview = View::make('admin.changeorders.reports.progress-reporthtml',$dataarray);
        $mailcontent = $messageview->render();
        $this->reporthtml = $mailcontent;
        return view('admin.changeorders.reports.progress-report', $this->data);
    }
    public function getProgressReportHtml(Request $request){
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectarray = Project::whereIn('id',$projectlist)->get();
        $tasksarray = Task::where('project_id',$request->project_id)->pluck('id')->toArray();
        $tasksimagesarray = TaskFile::whereIn('task_id',$tasksarray)->get();
        $dataarray = array();
        $projectid= $request->project_id;
        $dataarray['user'] = $user;
        $dataarray['projectid'] = $projectid;
        $dataarray['subprojectid'] = $request->subproject_id;
        $dataarray['segmentid'] = $request->segment_id;
        $dataarray['startdate'] = \Carbon\Carbon::parse($request->startdate)->format('Y-m-d');
        $dataarray['enddate'] = \Carbon\Carbon::parse($request->enddate)->format('Y-m-d');
        $dataarray['dateformat'] = $this->global->date_format;
        $dataarray['tasksimagesarray'] = $tasksimagesarray;
        $dataarray['manpowercategoryarray'] = \App\ManpowerCategory::all();;
        $dataarray['contractorarray'] = Employee::getAllContractors($user)->pluck('name','user_id')->toArray();
        $dataarray['curmonth'] = date('m');
        $dataarray['curyear'] = date('Y');
        $dataarray['notstarted'] = Task::where('project_id',$projectid)->where('status', 'notstarted')->count();
        $dataarray['inprogress'] = Task::where('project_id',$projectid)->where('status', 'inprogress')->count();
        $dataarray['delayed'] = Task::where('project_id',$projectid)->where('status', 'delayed')->count();
        $dataarray['completed'] = Task::where('project_id',$projectid)->where('status', 'completed')->count();
        $dataarray['inproblem'] = Task::where('project_id',$projectid)->where('status', 'inproblem')->count();
        $dataarray['issueslist'] = PunchItem::where('projectid',$projectid)->get();
        $dataarray['indentslist'] = IndentProducts::join('indents','indents.id','=','indent_products.indent_id')
                                    ->select('indent_products.*','indents.indent_no')->where('indents.project_id',$projectid)->get();
        $dataarray['exportype'] = '';
        $messageview = View::make('admin.changeorders.reports.progress-reporthtml',$dataarray);
        return $messageview->render();
    }
    public function progressReportPdf(Request $request){
        $user = $this->user;
        $this->projectarray = Project::all();
        $this->contractorarray = Employee::getAllContractors($user);
        $exporttype = $request->exporttype;
        $this->user = $user;
        $this->projectid = $request->project_id;
        $this->subprojectid = $request->subproject_id;
        $this->segmentid = $request->segment_id;
        $this->startdate = \Carbon\Carbon::parse($request->startdate)->format('Y-m-d');
        $this->enddate = \Carbon\Carbon::parse($request->enddate)->format('Y-m-d');
        $this->dateformat = $this->global->date_format;
        $this->exportype = '';
        if($exporttype=='pdf'){
            $pdf = PDF::loadView('admin.changeorders.reports.progress-reporthtml',$this->data)->setPaper('a4', 'landscape');
            return $pdf->download('progress-report.pdf');
        }
        if($exporttype=='excell'){
            $report = View::make('admin.changeorders.reports.progress-reportexcell',$this->data)->render();
            $exportArray =  json_decode($report);
            Excel::create('Progress-report', function ($excel) use ($exportArray) {
                $excel->setTitle('Progress report');
                $excel->setCreator('Worksuite')->setCompany($this->companyName);
                $excel->setDescription('Progress report');
                $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                    $sheet->fromArray($exportArray, null, 'A1', false, false);
                    $sheet->row(1, function ($row) {
                        $row->setFont(array(
                            'bold'       =>  true
                        ));
                    });
                });
            })->download('xlsx');
        }

    }
    public function viewTask(Request $request){

        $user = $this->user;
        $id = $request->id;
        $id = ltrim($id, 'p');
        $task = Task::where('cost_item_id',$id)->first();
        $this->user  = $user;
        if(!empty($task->id)){
            $this->task  = $task;
            $this->tasklabourlog = \App\ManpowerLog::where('task_id',$task->id)->count();
            $this->taskissues = \App\PunchItem::where('task_id',$task->id)->count();
            $alltimeline = collect();
            $taskpercentagearray = TaskPercentage::select('*','id as percentid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
            foreach ($taskpercentagearray as $taskpercentage){
                $alltimeline =  $alltimeline->push($taskpercentage);
            }
            $manpowerlogsarray = ManpowerLog::select('*','id as manpowerid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
            foreach ($manpowerlogsarray as $manpowerlogs){
                $alltimeline =  $alltimeline->push($manpowerlogs);
            }
            $punchitemlogsarray = PunchItem::select('*','id as punchitemid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
            foreach ($punchitemlogsarray as $punchitemlogs){
                $alltimeline =  $alltimeline->push($punchitemlogs);
            }
            $this->timelinearray =  $alltimeline->sortBy('created_at');
        }
        $view = view('admin.changeorders.view-task', $this->data)->render();
        return $view;
    }
    public function replyComment(Request $request){
        $user = $this->user;
        $id = $request->taskid;
        $tasks = Task::find($id);
        if(!empty($tasks->id)){
            $taskspercent = new TaskPercentage();
            $taskspercent->task_id = $tasks->id;
            $taskspercent->added_by = $user->id;
            $taskspercent->percentage = '0';
            $taskspercent->taskqty =   0;
            $taskspercent->comment = $request->comment ?: '';
            $taskspercent->status = '';
            $taskspercent->save();
            $taskhtml = '';
            $taskhtml .= '<div class="col-md-11 timelineblock float-right">
                 <div class="col-md-2">
                     <img class="img-circle w-100" src="'.get_users_image_link($user->id).'">
                 </div>
                 <div class="col-md-10">
                     <div class="taskusername">'.ucwords($user->name).'</div>
                     <div class="tasksdate">'.Carbon::parse($taskspercent->created_at)->format('d/m/Y').'</div>
                     <div class="commenttext">
                            <div><strong>Comment: </strong><br>
                            '.$taskspercent->comment.'</div>
                                <ul class="list-group" id="files-list">
                                
                                </ul> 
                         </div>
                     </div>
                  </div>';
            return Reply::dataOnly(['taskID' => $id,'percentageID' => $taskspercent->id,'taskhtml'=>$taskhtml]);
        }
    }
    public function projectMembers(Request $request,$projectid){
         $user = $this->user;
         if($projectid){
            $pm = ProjectMember::where('project_id',$projectid)->get()->pluck('user_id')->toArray();
            if($pm){
                $usersarray = array();
                $userslist = User::whereIn('id',$pm)->get();
                foreach ($userslist as $users){
                    $usersdata = array();
                    $usersdata['id'] = $users->id;
                    $usersdata['name'] = $users->name;
                    $usersdata['avatar'] = get_users_image_link($users->id);
                    $usersdata['type'] = 'contact';
                    $usersarray[] = $usersdata;
                }
                return response()->json($usersarray);
            }
         }
    }
    public function costDataCreate(Request $request){
        $user = $this->user;
        $assignto = '';
        if(!empty($request->assign_to)){
            $assignto = array_unique(array_filter($request->assign_to));
            $assignto = implode(',',$assignto);
        }
        $id = substr($request->prevtaskid,1);

        $linktype = $request->linktype;
        $project_id = $request->project_id;
        $subproject_id = $request->subproject_id;
        $target = $request->target;
        $taskname = $request->taskname;
        $milestone = $request->milestone;
        $startdate =  Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        $deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');
        $formprocess = true;
        $message = '';
        if(!empty($id)){
            $prevprojectcost = ProjectCostItemsProduct::find($id);
            $maxcostitemid = $prevprojectcost->inc;
            $newid = $maxcostitemid+1;
        }else{
            $prevprojectcost = ProjectCostItemsProduct::where('project_id',$project_id)->where('title',$subproject_id)->orderBy('id','desc')->first();
             $maxcostitemid = $prevprojectcost->inc;
            $newid = $maxcostitemid+1;
        }
        $projectcost = new ProjectCostItemsProduct();
        $cost_item = CostItems::where('cost_item_name',$taskname)->first();
        if(empty($cost_item)){
            $cost_item = new CostItems();
            $cost_item->cost_item_name = $taskname;
            $cost_item->company_id = $user->company_id;
            $cost_item->unit = '';
            $cost_item->save();
        }
        $projectcost->cost_items_id = $cost_item->id;
        $projectcost->project_id = $prevprojectcost->project_id;
        $projectcost->title = $prevprojectcost->title;
        $projectcost->category = $prevprojectcost->category;
        $projectcost->deadline = $deadline;
        $projectcost->start_date = $startdate;
        if($linktype>=0){
            $projectcost->type = $linktype ?: 0;
        }else{
            $projectcost->type = '';
        }
        $projectcost->inc = $newid;
        $projectcost->save();
        if(!empty($target)){
            $targetcostitem = ProjectCostItemsProduct::find($target);
            switch ($linktype){
                case '0':
                    $targetdeadline = strtotime($targetcostitem->deadline);
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($startdate)));
                    if(strtotime($startdate)>=$targetdeadline){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Start date  should be more than '.$targetcostitem->deadline;
                    }
                    break;
                case '1':
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($startdate)));
                    $targetstartdate = strtotime($targetcostitem->start_date);
                    if(strtotime($startdate)>=$targetstartdate){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Start date should be  more than '.date('Y-m-d',strtotime('-1 days',strtotime($targetcostitem->start_date)));
                    }
                    break;
                case '2':
                    $targetdatediff =  Carbon::parse($projectcost->deadline)->diffInDays(Carbon::parse($projectcost->start_date));
                    $targetdeadlinedate = date('Y-m-d',strtotime('-'.$targetdatediff.' days',strtotime($deadline)));
                    $targetdeadline = strtotime($targetcostitem->deadline);
                    if(strtotime($deadline)>=$targetdeadline){
                        $formprocess = true;
                    }else{
                        $formprocess = false;
                        $message = 'Due date should be more than '.date('Y-m-d',strtotime('-1 days',strtotime($targetcostitem->deadline)));
                    }
                    break;
                case '3':
                    $targetdatediff =  Carbon::parse($targetcostitem->deadline)->diffInDays(Carbon::parse($targetcostitem->start_date));
                    $targetstartdate = date('Y-m-d',strtotime('-'.$targetdatediff.' days',strtotime($startdate)));
                    $formprocess = true;
                    $targetcostitem->start_date =  date('Y-m-d',strtotime($targetstartdate));
                    $targetcostitem->deadline =  date('Y-m-d',strtotime($startdate));
                    $targetcostitem->save();
                    break;
            }
        }else{
            $targetcost = ProjectCostItemsProduct::where('target',$projectcost->id)->first();
            if(!empty($targetcost->id)){
                $targetlinktype = $targetcost->type;
                switch ($targetlinktype){
                    case '0':
                        $targetdatediff =  Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                        $targetstartdate = date('Y-m-d',strtotime('+1 days',strtotime($deadline)));
                        $targetdeaddate = date('Y-m-d',strtotime('+'.$targetdatediff.' days',strtotime($targetstartdate)));
                        $targetcost->start_date =  $targetstartdate;
                        $targetcost->deadline =  $targetdeaddate;
                        $targetcost->save();
                        break;
                    case '1':
                        if(strtotime($startdate)>strtotime($targetcost->start_date)){
                            $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                            $targetstartdate = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
                            $targetdeaddate = date('Y-m-d', strtotime('+' . $targetdatediff . ' days', strtotime($targetstartdate)));
                            $targetcost->start_date =  $targetstartdate;
                            $targetcost->deadline = $targetdeaddate;
                            $targetcost->save();
                        }
                        break;
                    case '2':
                        if(strtotime($deadline)>strtotime($targetcost->deadline)) {
                            $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                            $targetstartdate = date('Y-m-d', strtotime('-' . $targetdatediff . ' days', strtotime($deadline)));
                            $targetcost->start_date = $targetstartdate;
                            $targetcost->deadline = $deadline;
                            $targetcost->save();
                        }
                        break;
                    case '3':
                        if(strtotime($startdate)>strtotime($targetcost->startdate)) {
                            $targetdatediff = Carbon::parse($targetcost->deadline)->diffInDays(Carbon::parse($targetcost->start_date));
                            $targetdeaddate = date('Y-m-d', strtotime('+' . $targetdatediff . ' days', strtotime($deadline)));
                            $targetcost->start_date = $deadline;
                            $targetcost->deadline = $targetdeaddate;
                            $targetcost->save();
                        }
                        break;
                }
            }
        }
        if($formprocess){
            $projectcost->assign_to = $assignto;
            if(!empty($projectcost->baselinedate)){
                $projectcost->planned_start =  $projectcost->start_date;
                $projectcost->planned_end = $projectcost->deadline;
                $projectcost->start_date = date('Y-m-d',strtotime($startdate));
                $projectcost->deadline = date('Y-m-d',strtotime($deadline));
            }else{
                $projectcost->planned_start =  '';
                $projectcost->planned_end =  '';
                $projectcost->start_date = $startdate;
                $projectcost->deadline =  $deadline;
            }
            if($linktype>=0){
                $projectcost->type = $linktype ?: 0;
            }else{
                $projectcost->type = '';
            }
            $projectcost->target = $target ?: 0;
            $projectcost->linked = !empty($target) ? '1' : '0';
            $projectcost->milestone = !empty($milestone) ? '1' : '0';
            if($projectcost->milestone>0){
                $projectcost->deadline =  $startdate;
            }
            $projectcost->save();
            ProjectCostItemsProduct::where('id',$request->target)->update([
                'linked' => 1
            ]);
            $data['status'] = 'success';
        }else{
            $data['status'] = 'fail';
            $data['message'] = $message;
        }
        return response()->json($data);
    }
    public function activityCreate(Request $request){
        if(empty($request->activity)){
            return 'Activity is empty';
        }
        $user = $this->user;
        $prevtaskid = $request->prevtaskid;
        if(!empty($prevtaskid)){
            $prevpostion = ProjectCostItemsPosition::where('id',$prevtaskid)->first();
            $parent =   $prevpostion->itemid;
            $level =   $prevpostion->level+1;
            if(!empty($prevpostion->catlevel)) {
                $catlevel = $prevpostion->catlevel . ',' . $prevpostion->itemid;
            }else{
                $catlevel =  $prevpostion->itemid;
            }
        }else{
            $parent =   0;
            $level =   0;
            $catlevel = '';
        }
        $catid = $request->catid;
        $catname = $request->activity;
        $boqcategory = BoqCategory::where('title',$catname)->first();
        if(empty($boqcategory->id)){
            $boqcategory = new BoqCategory();
            $boqcategory->title = $catname;
            $boqcategory->company_id = $user->company_id;
            $boqcategory->parent =  $parent;
            $boqcategory->save();
        }
        $catid =  $boqcategory->id;
        $catname =  $boqcategory->title;
        $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$request->project_id)->where('title',$request->subproject_id)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new ProjectCostItemsPosition();
        $columsarray->project_id = $request->project_id;
        $columsarray->title = $request->subproject_id;
        $columsarray->position = 'row';
        $columsarray->itemid = $catid;
        $columsarray->itemname = $catname;
        $columsarray->catlevel = $catlevel;
        $columsarray->level = $level;
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();
        $data = array();
        $data['status'] = 'success';
        return $data;
    }
}
