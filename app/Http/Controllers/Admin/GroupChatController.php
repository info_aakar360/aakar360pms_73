<?php

namespace App\Http\Controllers\Admin;

use App\GroupChat;
use App\GdprSetting;
use App\GlobalSetting;
use App\Project;
use App\ProjectMember;
use App\ProjectsLogs;
use App\User;
use Illuminate\Http\Request;
use View;
use App\Setting;
use Carbon\Carbon;
use Froiden\Envato\Traits\AppBoot;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;

class GroupChatController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.groupchat';
        $this->pageIcon = 'icon-calender';
        $this->activeMenu = 'pms';
    }
    public function index(){
        $user = $this->user;
        $this->userList = User::where('company_id',$user->company_id)->get();
        $this->groupchatsList = GroupChat::where('company_id',$user->company_id)->get();
        $this->projectsarray = Project::where('company_id',$user->company_id)->get();
        return view('pulse.groupchats', $this->data);
    }
    public function store(Request $request)
    {
        $user = $this->user;
        $response = array();
        $projectid = $request->project_id;
        $name = $request->name;
        $employee = new GroupChat();
        $employee->company_id = $user->company_id;
        $employee->added_by = $user->id;
        $employee->project_id = $projectid;
        $employee->name = $name;
        if ($request->hasFile('image')) {
            $storage = storage();
            $image = $request->image->hashName();
            switch($storage) {
                case 'local':
                    $request->image->storeAs('user-uploads/groupchats', $image);
                    break;
                case 's3':
                    Storage::disk('s3')->putFileAs('groupchats/', $request->image, $request->image->hashName(), 'public');
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'avatar')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('avatar');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->image)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->image)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('groupchats/'.'/', $request->image, $request->image->hashName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    break;
            }

            $employee->image= $request->image->hashName();
        }
        $employee->save();
        $response['status'] = 'success';
        $response['message'] = 'Group Created Successfully';
        return $response;
    }
}
