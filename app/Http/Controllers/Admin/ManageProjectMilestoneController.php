<?php

namespace App\Http\Controllers\Admin;

use App\CostItems;
use App\Project;
use App\Http\Requests\Milestone\StoreMilestone;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\Helper\Reply;
use App\Segment;
use App\Task;
use App\Title;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Currency;

class ManageProjectMilestoneController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.projects-milestone');
        $this->pageIcon = 'icon-layers';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id){
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);

            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.milestones.index', $this->data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function milestoneslist(Request $request, $projectid,$subprojectid){
        $user = $this->user;
        $project = Project::find($projectid);
        if(empty($project->id)){
            return redirect(route('admin.milestones.show','all'));
        }
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        $this->project = $project;
        $this->subproject = Title::find($subprojectid);
        return view('admin.projects.milestones.milestoneslist', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        return view('admin.projects.milestones.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMilestone $request)
    {
        $user = $this->user;
       $title = $request->title;
       $tasks = $request->tasks;
       $project_id = $request->project_id ?: 0;
       $sub_project = $request->sub_project ?: 0;
       if(!empty($tasks)){
           $maxcostitemid = ProjectCostItemsProduct::where('id',$tasks)->where('title',$sub_project)->where('project_id',$project_id)->pluck('inc');
           $maxcostitemid = !empty($maxcostitemid[0]) ? $maxcostitemid[0]: 0;
       }else{
           $maxcostitemid = ProjectCostItemsProduct::where('title',$sub_project)->where('project_id',$project_id)->max('inc');

       }
       $newid = $maxcostitemid+1;
        $costitemdetails = CostItems::where('company_id',$user->company_id)->where('cost_item_name',$title)->first();
        if(empty($costitemdetails->id)){
            $costitemdetails = new CostItems();
            $costitemdetails->company_id = $user->company_id;
            $costitemdetails->cost_item_name = $title;
            $costitemdetails->save();
        }
        $positionid = $request->activity;
        $procostprosiion = ProjectCostItemsPosition::where('id',$positionid)->first();
        $proprogetdat = new ProjectCostItemsProduct();
        $proprogetdat->title = $sub_project;
        $proprogetdat->project_id = $project_id;
        $proprogetdat->category = $procostprosiion->itemid;
        if($procostprosiion->parent){
            $proprogetdat->category = $procostprosiion->parent.','.$procostprosiion->itemid;
        }
        $proprogetdat->cost_items_id = $costitemdetails->id;
        $proprogetdat->position_id = $procostprosiion->id;
        $proprogetdat->unit = $costitemdetails->unit;
        $proprogetdat->start_date = !empty($request->start_date) ? date('Y-m-d',strtotime($request->start_date)) : date('Y-m-d');
        $proprogetdat->deadline = !empty($request->due_date) ? date('Y-m-d',strtotime($request->due_date)) : date('Y-m-d');
        $proprogetdat->inc = $newid;
        $proprogetdat->milestone = '1';
        $proprogetdat->ordertype = 'estimate';
        $proprogetdat->status = $request->status;
        $proprogetdat->description = $request->description;
        $proprogetdat->save();
        return Reply::success(__('messages.milestoneSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::find($id);
            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.milestones.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $milestone = ProjectCostItemsProduct::where('id',$id)->where('milestone','1')->first();
        if(empty($milestone->id)){
            return redirect(route('admin.milestone.show','all'));
        }
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        $this->subprojectlist = !empty($milestone->title) ? Title::where('project_id',$milestone->project_id)->get() : array();
        $this->activitylist = !empty($milestone->project_id) ? ProjectCostItemsPosition::where('project_id',$milestone->project_id)->where('title',$milestone->title)->where('position','row')->get() : array();
        $this->costitemlist = ProjectCostItemsProduct::join('cost_items', 'cost_items.id', '=', 'project_cost_items_product.cost_items_id')
            ->where('project_cost_items_product.project_id', $milestone->project_id)
            ->where('project_cost_items_product.title', $milestone->title)
            ->where('project_cost_items_product.position_id', $milestone->position_id)
            ->orderBy('project_cost_items_product.inc','asc')->pluck('cost_items.cost_item_name', 'project_cost_items_product.id');
          $this->milestone = $milestone;
        $inc = !empty($milestone->inc) ? $milestone->inc-1 : 1;
        $inc = !empty($inc) ? $inc : '1';
        $prevcostitem = ProjectCostItemsProduct::where('position_id', $milestone->position_id)->where('inc',$inc)->first();
        $this->costitemid = !empty($prevcostitem->id) ? $prevcostitem->id : '';
        return view('admin.projects.milestones.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $milestone = ProjectCostItemsProduct::where('id',$id)->where('milestone','1')->first();
        if(empty($milestone->id)){
            return Reply::error("Milestone not found");
        }
        $tasks = $request->tasks;
        $project_id = $request->project_id ?: 0;
        $sub_project = $request->sub_project ?: 0;
        $newid = $milestone->inc ?: 1;
        if(!empty($tasks)){
            $maxcostitemid = ProjectCostItemsProduct::where('id',$tasks)->where('title',$sub_project)->where('project_id',$project_id)->pluck('inc');
            $maxcostitemid = !empty($maxcostitemid[0]) ? $maxcostitemid[0]: 0;
            $newid = $maxcostitemid+1;
        }
        $costvalue = $request->title;
        $costitemdetails = CostItems::where('company_id',$user->company_id)->where('cost_item_name',$costvalue)->first();
        if(empty($costitemdetails->id)){
            $costitemdetails = new CostItems();
            $costitemdetails->company_id = $user->company_id;
            $costitemdetails->cost_item_name = $costvalue;
            $costitemdetails->save();
        }
        $positionid = $request->activity;
        $procostprosiion = ProjectCostItemsPosition::where('id',$positionid)->first();
        $milestone->project_id = $project_id ?: 0;
        $milestone->title = $sub_project ?: 0;
        if($procostprosiion->parent){
            $milestone->category = $procostprosiion->parent.','.$procostprosiion->itemid;
        }
        $milestone->cost_items_id = $costitemdetails->id;
        $milestone->position_id = $procostprosiion->id;
        $milestone->description = $request->description;
        $milestone->status = $request->status;
        $milestone->inc = $newid;
        $milestone->start_date = date('Y-m-d',strtotime($request->start_date));
        $milestone->deadline = date('Y-m-d',strtotime($request->due_date));
        $milestone->save();

        return Reply::success(__('messages.milestoneSuccess')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProjectCostItemsProduct::where('id',$id)->delete();
        Task::where('cost_item_id',$id)->delete();

        return Reply::success(__('messages.deleteSuccess'));
    }

    public function data()
    {

        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);

        $milestones = ProjectCostItemsProduct::whereIn('project_id',$projectlist)->where('milestone', 1)->get();

        return DataTables::of($milestones)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'.route('admin.milestone.edit',[$row->id]).'" class="btn btn-info btn-circle"
                        data-toggle="tooltip" data-milestone-id="'.$row->id.'"  data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                        <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                        data-toggle="tooltip" data-milestone-id="'.$row->id.'" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('status', function($row){
                if($row->status == 'complete'){
                    return '<label class="label label-success">'.$row->status.'</label>';
                }
                else{
                    return '<label class="label label-danger">'.$row->status.'</label>';
                }
            })
            ->editColumn('task', function($row) {
                return get_cost_name($row->cost_items_id);
            })
            ->editColumn('project_id', function($row) {
                return get_project_name($row->project_id);
            })
            ->editColumn('subproject_id', function($row) {
                return get_subproject($row->title);
            })
            ->rawColumns(['status', 'action', 'project_id', 'subproject_id', 'task'])
            ->make(true);
    }

    public function detail($id)
    {
        $this->milestone = ProjectCostItemsProduct::findOrFail($id);
        return view('admin.projects.milestones.detail', $this->data);
    }

    public function getProjectsInfo(Request $request){

        $projectid = $request->projectid;
        $subprojectid = $request->subprojectid ?: 0;
        $projectsarray = array();
        if($projectid){
            $projectsarray['titles'] = Title::where('project_id',$projectid)->pluck('title','id')->toArray();
            $projectsarray['segments'] = Segment::where('projectid',$projectid)->pluck('name','id')->toArray();
            $projectsarray['activitylist'] = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->orderBy('inc')->pluck('itemname','id')->toArray();
        }
        return  json_encode($projectsarray);
    }
/////
}
