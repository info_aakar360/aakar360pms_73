<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\IncomeExpenseGroup;
use App\IncomeExpenseType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


class IncomeExpenseGroupController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Income Expense Group';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = IncomeExpenseGroup::class;
    public $parentRoute = 'income_expense_group';
    public $parentView = "admin.income-expense-group";


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;

        $this->itemsarray = IncomeExpenseGroup::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $this->groupsarray = IncomeExpenseGroup::where('company_id',$user->company_id)->get();
        $this->itemsarray =  IncomeExpenseType::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user;
        $request->validate([
            'name' => 'required|string',
            'type' => 'required|string',
        ]);
        $incomegroup = IncomeExpenseGroup::where('name',$request->name)->where('company_id',$user->company_id)->first();
        if(!empty($incomegroup)){
            return Reply::error(__('Group already Exists'));
        }
        $items = new IncomeExpenseGroup();
        $items->company_id = $user->company_id;
        $items->name = $request->name;
        $items->income_type = $request->type;
        $items->parent = $request->parent;
        $items->created_by = $user->id;
        $items->save();
        return Reply::redirect(route('admin.income_expense_group'),__('Group Created Successfully'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->item = IncomeExpenseGroup::find($request->id);
        if (empty($this->item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $this->groupsarray = IncomeExpenseGroup::where('company_id',$user->company_id)->get();
        $this->itemsarray =  IncomeExpenseType::where('company_id',$user->company_id)->get();
        $this->group = IncomeExpenseGroup::find($id);
        if (empty($this->group)) {
            return redirect()->back()->with('error','Item not found');
        }
        if ($this->group->default=='1') {
            return redirect()->back()->with('error','Default Group cannot be edited');
        }
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $request->validate([
            'name' => 'required|string',
            'type' => 'required|string',
        ]);
        $items = IncomeExpenseGroup::find($id);
        $items->company_id = $user->company_id;
        $items->name = $request->name;
        $items->income_type = $request->type;
        $items->parent = $request->parent;
        $items->updated_by = $user->id;
        $items->save();
        return Reply::redirect(route('admin.income_expense_group'),__('Group Updated Successfully'));
    }

    public function pdf(Request $request)
    {
        $item = IncomeExpenseGroup::find($request->id);
        if (empty($item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format').' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Ledger Group'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
        return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = IncomeExpenseGroup::find($id);
        if (empty($items)) {
            return Reply::error(__('Item not found'));
        }
        if ($items->default=='1') {
            return Reply::error(__('Default group cannot be deleted.'));
        }
        if (count(IncomeExpenseGroup::find($id)->IncomeExpenseHeads) > 0) {  // Child has or not
            return Reply::error(__('You can not delete it.Because it has ledger items'));
        }
        $items->delete_by = \Auth::user()->email;
        $items->delete();
        return Reply::success(__('Successfully Trashed'));
    }

    public function trashed()
    {
        $this->items = IncomeExpenseGroup::onlyTrashed()->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

    public function restore($id)
    {
        $items = IncomeExpenseGroup::onlyTrashed()->where('id', $id)->first();
        $items->restore();
        return Reply::success(__('Successfully Restore'));
    }

    public function kill($id)
    {
        $items = IncomeExpenseGroup::withTrashed()->where('id', $id)->first();
        if (count(IncomeExpenseGroup::withTrashed()->find($id)->IncomeExpenseHeads) > 0) {  // Child has or not
            return Reply::error(__('You can not delete it.Because it has ledger items'));
        }
        $items->forceDelete();
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = IncomeExpenseGroup::where('name', 'like', '%' . $search . '%')
            ->orWhere('code', 'like', '%' . $search . '%')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = IncomeExpenseGroup::where('name', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('code', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
}
