<?php

namespace App\Http\Controllers\Admin;

use App\AppProject;
use App\Employee;
use App\Helper\Reply;
use App\ManpowerCategory;
use App\Project;
use App\Workers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageWorkersController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Workers';
        $this->pageIcon = 'icon-clock';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('manpowerlog', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = AppProject::whereIn('id',$projectlist)->get(); 
        return view('admin.workers.index', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->manpowercategoryarray = ManpowerCategory::where('company_id',$user->company_id)->get();
        return view('admin.workers.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user;
        $projectid = $request->project_id;
        $name = $request->name;
        $contractor = explode('-',$request->contractor);
        $category = $request->category;
        $workinghours = $request->workinghours;
        $salary = $request->salary;
        $salarytype = $request->salarytype;

        $projectdetails = AppProject::where('id',$projectid)->first();
        $usercompany = $projectdetails->company_id;

        $employee = new Workers();
        $employee->company_id = $usercompany;
        $employee->added_by = $user->id;
        $employee->project_id = $projectdetails->id;
        $employee->name = $name;
        if(!empty($name)){
            $employee->name = $name;
            $employee->type = 'worker';
        }else{
            $employee->type = 'category';
        }
        $employee->emp_id = trim($contractor[0]);
        $employee->contractor = trim($contractor[1]);
        $employee->category = $category;
        $employee->workinghours = $workinghours;
        $employee->salary = $salary;
        $employee->salarytype = $salarytype;
        $employee->save();

        return Reply::redirect(route('admin.workers.index'),'Workers created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->client = Employee::findOrFail($id);
        return view('admin.workers.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $this->worker = Workers::find($id);
        $projectlist = explode(',',$user->projectlist);
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->contractorsarray = Employee::where('company_id',$this->worker->company_id)->get();
        $this->manpowercategoryarray = ManpowerCategory::where('company_id',$user->company_id)->get();
        return view('admin.workers.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $projectid = $request->project_id;
        $name = $request->name;
        $contractor = explode('-',$request->contractor);
        $category = $request->category;
        $workinghours = $request->workinghours;
        $salary = $request->salary;
        $salarytype = $request->salarytype;

        $projectdetails = AppProject::where('id',$projectid)->first();
        $usercompany = $projectdetails->company_id;

        $employee = Workers::find($id);
        $employee->company_id = $usercompany;
        $employee->added_by = $user->id;
        $employee->project_id = $projectdetails->id;
        $employee->name = $name;
        $employee->emp_id = trim($contractor[0]);
        $employee->contractor = trim($contractor[1]);
        $employee->category = $category;
        $employee->workinghours = $workinghours;
        $employee->salary = $salary;
        $employee->salarytype = $salarytype;
        $employee->save();

        return Reply::redirect(route('admin.workers.index'),'Workers updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Workers::where('id',$id)->delete();
        return Reply::success('Workers deleted successfully.');
    }
    public function workerdData(Request $request)
    {
        try{
        $user = $this->user;
        if(!empty($request->projects)){
            $projectlist = explode(',',$request->projects);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $workers = Workers::whereIn('company_id',$companylist)->get();
        return DataTables::of($workers)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.workers.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
 
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="#">' . ucfirst($row->name) . '</a>';
                }
            )->editColumn(
                'contractor_name',
                function ($row) {
                    return get_employee_name($row->emp_id);
                }
            )->editColumn(
                'project_name',
                function ($row) {
                    return get_project_name($row->project_id);
                }
            )->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'contractor_name', 'project_name'])
            ->make(true);

        }catch (\Exception $e){

        }
    }

    public function getContractors(Request $request)
    {
        $projectid = $request->projectid;
        $project = Project::where('id',$projectid)->first();
        $titleoption = '';
            $titlearray = Employee::where('company_id',$project->company_id)->where('user_type','contractor')->get();
            if(count($titlearray)>0){
                $titleoption = '<option value="">Select Contractor</option>';
                foreach ($titlearray as $item) {
                    $titleoption .= '<option value="'.$item->id.'-'.$item->user_id.'">'.$item->name.'</option>';
                }
            }

        return $titleoption;
    }
    public function workerList(Request $request){
        $projectid = $request->projectid;
        $contractor = $request->contractor;
        $data = array();
        $projectdetails = Project::find($projectid);
        $data['contractor'] = Employee::find($contractor);
        $data['mancategoryarray'] = ManpowerCategory::where('company_id',$projectdetails->company_id)->get();
        $data['workerslist'] = Workers::where('project_id',$projectid)->where('emp_id',$contractor)->get();
        return view('admin.workers.workers_list', $data)->render();
    }
}
