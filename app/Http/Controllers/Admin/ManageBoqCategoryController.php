<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\BoqCategory;
use Illuminate\Http\Request;

class ManageBoqCategoryController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Activity';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        return view('admin.boq-category.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $user = $this->user;
        $category = new BoqCategory();
        $category->company_id = $user->company_id;
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.activityAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreBoqCategory $request)
    {
        $user = $this->user;
        $category = new BoqCategory();
        $category->company_id = $user->company_id;
        $category->title = $request->category_name;
        $category->parent = $request->parent_id;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.activityAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        $this->activity = BoqCategory::where('id',$id)->first();
        return view('admin.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBoqCategory $request, $id)
    {
        $user = $this->user;
        $category = BoqCategory::find($id);
        $category->company_id = $user->company_id;
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.activityUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $bo=  BoqCategory::where('id',$id)->delete();
        return Reply::success(__('messages.activityDeleted'));
    }
}
