<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\SubAsset;


use App\Location;
use App\MeasurementType;
use App\Units;
use Illuminate\Http\Request;

class ManageMeasurementTypesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Measurement Type';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }


    public function index()
    {
        $this->types = MeasurementType::all();
        $this->unitsarray = Units::all();
        return view('admin.measurementtypes.index', $this->data);
    }


    public function store(Request $request)
    {
        $type = new MeasurementType();
        $type->unit = $request->unit;
        $type->title = $request->title;
        $type->symbol = $request->symbol;
        $type->save();
        return Reply::success(__('messages.typeAdded'));
    }

    public function edit($id)
    {
        $this->type = MeasurementType::find($id);
        $this->unitsarray = Units::all();
        $this->status='success';
        return view('admin.measurementtypes.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $type = MeasurementType::find($id);
        $type->unit = $request->unit;
        $type->title = $request->title;
        $type->symbol = $request->symbol;
        $type->save();

        return Reply::success(__('messages.typeUpdate'));
    }
    public function destroy($id)
    {
        MeasurementType::destroy($id);
        return Reply::success(__('messages.categoryDeleted'));
    }
}
