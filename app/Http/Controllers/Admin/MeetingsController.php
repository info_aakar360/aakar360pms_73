<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\CostItems\StoreCostItems;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\Http\Requests\Meetings\StoreMeetings;
use App\MeetingAssignedUser;
use App\MeetingBusinessItem;
use App\MeetingBusinessItemFile;
use App\MeetingCategory;
use App\MeetingDetail;
use App\MeetingDetailFiles;
use App\MeetingMinutes;
use App\Meetings;
use App\ProductBrand;
use App\Units;
use App\ProductCategory;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class MeetingsController extends AdminBaseController
{

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Meetings';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->meetings = Meetings::where('company_id',$this->user->company_id)->where('parent_id','0')->orderBy('id','DESC')->get();
        return view('admin.meetings.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMeeting()
    {
        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        return view('admin.meetings.create', $this->data);
    }

    public function storeMeeting(StoreMeetings $request)
    {
        $meeting = new Meetings();
        $meeting->company_id = $this->user->company_id;
        $meeting->title = $request->title;
        $meeting->created_by = $this->user->id;
        $meeting->save();

        if($meeting){
            $md = new MeetingDetail();
            $md->meeting_id = $meeting->id;
            $md->title = $request->title;
            $md->meeting_overview = $request->meeting_overview;
            $md->meeting_date = $request->meeting_date;
            $md->start_time = $request->start_time;
            $md->finish_time = $request->finish_time ?: '';
            $md->meeting_location = $request->meeting_location;
            if(isset($request->is_draft)){
                $md->is_draft = $request->is_draft;
            }else{
                $md->is_draft = '0';
            }
            if(isset($request->is_private)){
                $md->is_private = $request->is_private;
            }else{
                $md->is_private = '0';
            }
            $md->save();

            $assigned_user = $request->assigned_user;

            if(!empty($request->assigned_user)){
                foreach ($assigned_user as $au){
                    $mau = new MeetingAssignedUser();
                    $mau->meeting_id = $meeting->id;
                    $mau->user = $au;
                    $mau->status = '0';
                    $mau->save();
                }
            }

            return Reply::dataOnly(['meetingID' => $md->id]);
        }
    }

    public function editMeeting($id)
    {

        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        $meeting = MeetingDetail::where('id',$id)->first();
        $this->meeting = $meeting;
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        $this->assignedusers = MeetingAssignedUser::where('meeting_id',$meeting->id)->pluck('user')->toArray();
        return view('admin.meetings.edit', $this->data);
    }

    public function updateMeeting(Request $request)
    {
        $user = $this->user;
        $md = MeetingDetail::where('id',$request->meeting_id)->first();

        $meeting = Meetings::find($md->meeting_id);
        $meeting->company_id = $user->company_id;
        $meeting->title = $request->title;
        $meeting->created_by = $user->id;
        $meeting->save();

        if($meeting){
            $md->meeting_id = $meeting->id;
            $md->title = $request->title;
            $md->meeting_overview = $request->meeting_overview;
            $md->meeting_date = $request->meeting_date;
            $md->start_time = $request->start_time;
            $md->finish_time = !empty($request->finish_time) ? $request->finish_time : '';
            $md->meeting_location = $request->meeting_location;
           /* if(isset($request->is_draft)){
                $md->is_draft = $request->is_draft;
            }else{
                $md->is_draft = '0';
            }*/

            if(isset($request->is_private)){
                $md->is_private = $request->is_private;
            }else{
                $md->is_private = '0';
            }
            $md->save();
            if(!empty($request->assigned_user)){
                $assigned_user = $request->assigned_user;

                foreach ($assigned_user as $au){

                    $maudel = MeetingAssignedUser::where('meeting_id',$meeting->id)->where('user',$au)->first();
                    if(empty($maudel)){
                        $mau = new MeetingAssignedUser();
                        $mau->meeting_id = $meeting->id;
                        $mau->user = $au;
                        $mau->status = '0';
                        $mau->save();
                    }
                }

            }
            return Reply::dataOnly(['meetingID' => $md->id]);
        }
    }

    public function viewMeeting($id)
    {
        $user = $this->user;
        $this->employees = Employee::getAllEmployees($user);
        $meetingDetail = MeetingDetail::where('id',$id)->first();
        $this->meetingDetail = $meetingDetail;
        $this->meeting = Meetings::where('id',$meetingDetail->meeting_id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        $this->assignedUser = MeetingAssignedUser::where('meeting_id',$id)->get();
        return view('admin.meetings.view-meetings', $this->data);
    }

    public function meetingConvertToMinute($id)
    {
        $this->employees = User::allEmployees();
        $this->meeting = Meetings::where('id',$id)->first();
        $this->meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $this->assignedUser = MeetingAssignedUser::where('meeting_id',$id)->get();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        return view('admin.meetings.meeting-convert-to-minute', $this->data);
    }

    public function followUpMeeting($id)
    {
        $this->employees = User::allEmployees();
        $this->meeting = Meetings::where('id',$id)->first();
        $this->meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $this->assignedUser = MeetingAssignedUser::where('meeting_id',$id)->select('user')->get()->pluck('user')->toArray();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        return view('admin.meetings.follow-up-meeting', $this->data);
    }

    public function editBusinessItem($id)
    {
        $this->employees = User::allEmployees();
        $this->businessItem = MeetingBusinessItem::where('id',$id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$this->businessItem->meeting_detail_id)->get();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        return view('admin.meetings.business-item-edit', $this->data);
    }

    public function deleteCat($id){
        DB::beginTransaction();
        MeetingCategory::destroy($id);
        MeetingBusinessItem::where('category_id', $id)->delete();
        DB::commit();
        return Reply::success(__('category deleted'));
    }

    public function deleteBusinessItem($id){
        DB::beginTransaction();
        MeetingBusinessItem::where('id', $id)->delete();
        DB::commit();
        return Reply::success(__('Business Item Deleted'));
    }

    public function editCategory($id)
    {
        $this->employees = User::allEmployees();
        $this->category = MeetingDetail::where('id',$id)->first();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        return view('admin.meetings.category-edit', $this->data);
    }

    public function editMinutes($id)
    {
        $this->category = MeetingMinutes::where('id',$id)->first();
        return view('admin.meetings.minutes-edit', $this->data);
    }

    public function updateMinutes(Request $request, $id){
        $mcat = MeetingMinutes::find($id);
        $mcat->minutes = $request->description;
        $mcat->save();

        return Reply::success(__('Minutes Edited Successfully'));
    }

    public function updateCategory(Request $request, $id){
        $mcat = MeetingCategory::find($id);
        $mcat->title = $request->title;
        $mcat->save();

        return Reply::success(__('Category Edited Successfully'));
    }

    public function updateStatus(Request $request){
        $id = $request->id;
        $mcat = MeetingBusinessItem::find($id);
        $mcat->status = $request->status;
        $mcat->save();
        return redirect()->back()->with('success', 'Status Updated Successfully');
//        return Reply::success(__('Status updated successfully'));
    }

    public function updateUserStatus(Request $request, $id, $status){
        $mcat = MeetingAssignedUser::find($id);
        $mcat->status = $status;
        $mcat->save();

        return redirect()->back()->with('success', 'Status Updated Successfully');
        //return Reply::success(__('Status Updated Successfully'));
    }

    public function storeMeetingCategory(Request $request){
        $mcat = new MeetingCategory();
        $mcat->meeting_id = $request->meeting_id;
        $mcat->title = $request->title;
        $mcat->save();
        return Reply::success(__('Category Added Successfully'));
    }

    public function meetingMinutes(Request $request){
        $mcat = new MeetingMinutes();
        $mcat->meeting_id = $request->meeting_id;
        $mcat->business_item_id = $request->business_item_id;
        $mcat->category_id = $request->category_id;
        $mcat->minutes = $request->description;
        $mcat->save();

        return Reply::success(__('Minutes Added Successfully'));
    }

    public function storeBusinessItems(Request $request)
    {
        $meeting = new MeetingBusinessItem();
        $meeting->meeting_detail_id = $request->meeting_id;
        $meeting->title = $request->title;
        $meeting->description = $request->description;
        $meeting->due_date = $request->due_date;
        $meeting->category_id = $request->category_id;
        $meeting->status = $request->status;
        $meeting->priority = $request->priority;
        $meeting->assigned_user = implode(',',$request->assigned_user);
        $meeting->save();

        return Reply::dataOnly(['meetingID' => $meeting->id]);
    }

    public function updateBusinessItem(Request $request)
    {
        $meeting = MeetingBusinessItem::find($request->id);
        $meeting->meeting_detail_id = $request->meeting_id;
        $meeting->title = $request->title;
        $meeting->description = $request->description;
        $meeting->due_date = $request->due_date;
        $meeting->category_id = $request->category_id;
        $meeting->status = $request->status;
        $meeting->priority = $request->priority;
        $meeting->assigned_user = implode(',',$request->assigned_user);
        $meeting->save();

        return Reply::dataOnly(['meetingID' => $meeting->id]);
    }

    public function storeBusinessImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $meeting = MeetingBusinessItem::where('id',$request->meeting_id)->first();
                $file = new MeetingBusinessItemFile();
                $file->company_id = $this->user->company_id;
                $file->user_id = $this->user->id;
                $file->meeting_detail_id = $request->meeting_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/meeting-business-items-files/'.$meeting->id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('meeting-business-items-files/'.$meeting->id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'meeting-business-items-files')
                            ->first();
                        if(!$dir) {
                            Storage::cloud()->makeDirectory('meeting-business-items-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->meeting_id)
                            ->first();
                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$meeting->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->meeting_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('meeting-business-items-files/'.$meeting->id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/meeting-business-items-files/'.$request->meeting_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::success(__('Business Item Added Successfully'));
    }
    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $meeting = MeetingDetail::where('id',$request->meeting_id)->first();
                $file = new MeetingDetailFiles();
                $file->company_id = $this->user->company_id;
                $file->user_id = $this->user->id;
                $file->meeting_detail_id = $request->meeting_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/meeting-detail-files/'.$meeting->id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('meeting-detail-files/'.$meeting->id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'meeting-detail-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('meeting-detail-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->meeting_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$meeting->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->meeting_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('meeting-detail-files/'.$meeting->id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/meeting-detail-files/'.$request->meeting_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::redirect(route('admin.meetings.index'), __('Meeting Added Successfully'));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        if(!empty($meetingDetail)){
            $mbi = MeetingBusinessItem::where('meeting_detail_id',$meetingDetail->id)->first();

            MeetingCategory::where('meeting_id',$id)->delete();

            if($meetingDetail !== null) {
                MeetingBusinessItem::where('meeting_detail_id', $meetingDetail->id)->delete();
                MeetingDetailFiles::where('meeting_detail_id',$id)->delete();
            }
            MeetingAssignedUser::where('meeting_id',$id)->delete();
            if($mbi !== null) {
                MeetingBusinessItemFile::where('meeting_business_item_id', $mbi->id)->delete();
            }
            $meetingDetail->delete();
        }
        Meetings::destroy($id);

        return Reply::success(__('messages.meetingDeleted'));
    }
    public function detailsDestroy($id)
    {
        $meetingDetail = MeetingDetail::where('id',$id)->first();
        if(!empty($meetingDetail)){
            $mbi = MeetingBusinessItem::where('meeting_detail_id',$meetingDetail->id)->first();

            MeetingCategory::where('meeting_id',$meetingDetail->meeting_id)->delete();

            if($meetingDetail !== null) {
                MeetingBusinessItem::where('meeting_detail_id', $meetingDetail->id)->delete();
                MeetingDetailFiles::where('meeting_detail_id',$meetingDetail->id)->delete();
            }
            MeetingAssignedUser::where('meeting_id',$meetingDetail->meeting_id)->delete();
            if($mbi !== null) {
                MeetingBusinessItemFile::where('meeting_business_item_id', $mbi->id)->delete();
            }
            Meetings::where('id',$meetingDetail->meeting_id)->delete();
            $meetingDetail->delete();
        }

        return Reply::success(__('messages.meetingDeleted'));
    }

    public function destroyProduct($id)
    {
        CostItemsProduct::destroy($id);
        $categoryData = CostItemsProduct::all();
        return Reply::successWithData(__('Deleted Successfully'),['data' => $categoryData]);
    }

    public function distributeAgenda(Request $request, $id){
        $this->employees = User::allEmployees();
        $this->meeting = Meetings::where('id',$id)->first();
        $this->meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $this->assignedUser = MeetingAssignedUser::where('meeting_id',$id)->get();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();

        $employees = User::allEmployees();
        $meeting = Meetings::where('id',$id)->first();
        $meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $assignedUser = MeetingAssignedUser::where('meeting_id',$id)->get();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();

        $data = '
<div class="row"><img src="'.$this->global->logo().'" class="admin-logo"  alt="home" style="width: 150px;" /></div>
<div class="row">
        <div class="col-md-12">
            <div class="panel-heading"><h3> '.$meeting->title.'</h3></div>
            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                <tr>
                    <td>Meeting Date :</td>
                    <td>Start Time :</td>
                </tr>
                <tr>
                    <td><b>'.\Carbon\Carbon::parse($meetingDetail->meeting_date)->format('d M, Y').'</b></td>
                    <td><b>'.$meetingDetail->start_time.'</b></td>
                </tr>
                <tr>
                    <td>Finish Time :</td>
                    <td>Meeting Location :</td>
                </tr>
                <tr>
                    <td><b>'.$meetingDetail->finish_time.'</b></td>
                    <td><b>'.$meetingDetail->meeting_location.'</b></td>
                </tr>
                <tr>';
        if($meetingDetail->is_draft == '1'){
            $data .= '<td>Draft Meeting</td>';
        }
        if($meetingDetail->is_private == '1'){
            $data .='<td>Private Meeting</td>';
        }
        $data .='</tr>
            </table>

            <div class="card mb-10">
                <div class="card-header" style="background: #efefef; padding: 10px;">
                    <div class="col-md-12">
                        <p class="rowcat">Scheduled Attendees</p>
                    </div>
                </div>
                <div class="card-body" style="padding: 10px;">
                    <div class="table-responsive"><br><br><br><br>
                        <table class="table" id="example"  style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Person</th>
                                </tr>
                            </thead>
                            <tbody>';
                            foreach ($assignedUser as $key=>$u) {
                                $data .= '<tr>
                                    <td>'; $data .= $key + 1; $data .= '</td>
                                    <td>'.get_user_name($u->user).'</td>';
                                $data .= '</tr>';
                            }
                            $data .='</tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">';
                        foreach($meetingCat as $mcat) {
                            $data .= '<div class="row" id="cat-' . $mcat->id . '">
                                    <div class="col-md-12">
                                        <div class="card mb-10">
                                            <div class="card-header">
                                                <div class="col-md-12">
                                                    <p class="rowcat">' . $mcat->title . '
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="card-body">';
                                            $bitems = \App\MeetingBusinessItem::where('category_id', $mcat->id)->get();
                                            if (count($bitems)) {
                                                $data .= '<div class="table-responsive"><br><br><br><br>
                                        <table class="table" id="example" style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%">#</th>
                                                    <th>Title</th>
                                                    <th>Due Date</th>
                                                    <th>Priority</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
                foreach ($bitems as $key => $meetingD) {
                    $data .= '<tr>
                                                    <td>';
                    $data .= $key + 1;
                    $data .= '</td>
                                                    <td>' . $meetingD->title . '</td>
                                                    <td>' . $meetingD->due_date . '</td>
                                                    <td>' . get_priority($meetingD->priority) . '</td>
                                                    <td>';
                    if ($meetingD->status == '0') {
                        $data .= 'Open';
                    }
                    if ($meetingD->status == '1') {
                        $data .= 'Closed';
                    }
                    if ($meetingD->status == '2') {
                        $data .= 'Pending';
                    }
                    if ($meetingD->status == '3') {
                        $data .= 'Completed';
                    }
                    if ($meetingD->status == '4') {
                        $data .= 'Hold';
                    }
                    $data .= '</td>
                                                </tr>';
                    $catMinutes = \App\MeetingMinutes::where('meeting_id', $meeting->id)->where('category_id', $meetingD->category_id)->first();
                    if ($catMinutes !== null) {
                        $data .= '<tr>
                                                        <td colspan="5">' . $catMinutes->minutes . '</td>
                                                    </tr>';
                    } else {
                        $data .= '<tr>
                                                        <td colspan = "5" > Minutes have not been entered </td >
                                                    </tr >';
                    }
                }
                $data .= '</tbody>
                                            </table>
                                        </div>';
            }
            $data .= '</div>
                                    </div>
                                </div>
                            </div>';
        }
        $data .='</div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
        foreach ($assignedUser as $au) {
            $qty = \App\User::where('id', $au->user)->first();
            if(isset($qty->id)){
                $mailarray = array();
                $mailarray['email'] = $qty->email;
                $mailarray['subject'] = 'Meeting Agenda';
                $mailarray['message'] = $data;
                $qty->sendEmail($mailarray);
            }
        }

        $mcat = Meetings::find($meeting->id);
        $mcat->distributed = '1';
        $mcat->save();

        view()->share('meeting',$this->data);
        if($request->has('download')){
            $pdf = PDF::loadView('admin.meetings.pdf_view');
            return $pdf->download('admin.meetings.pdf_view');
        }
        $pdf = PDF::loadView('admin.meetings.pdf_view',$this->data);
        return $pdf->download($this->meeting->title.'.pdf');
    }

    public function addFollowupMeeting(Request $request){
        $meeting_category = $request->meeting_category;
        $mcatId = $request->mcatId;
        if(empty($request->mcatId)){
            return Reply::error('Category is empty.');
        }
        $meeting = new Meetings();
        $meeting->company_id = $this->user->company_id;
        $meeting->title = $request->meeting_title;
        $meeting->parent_id = $request->parent_id;
        $meeting->created_by = $this->user->id;
        $meeting->save();
        if($meeting){
            $md = new MeetingDetail();
            $md->meeting_id = $meeting->id;
            $md->title = $request->meeting_title;
            $md->meeting_overview = $request->meeting_overview;
            $md->meeting_date = $request->meeting_date;
            $md->start_time = $request->meeting_start_time;
            $md->finish_time = $request->meeting_finish_time;
            $md->meeting_location = $request->meeting_location;
            if(isset($request->is_draft)){
                $md->is_draft = $request->is_draft;
            }else{
                $md->is_draft = '0';
            }
            if(isset($request->is_private)){
                $md->is_private = $request->is_private;
            }else{
                $md->is_private = '0';
            }
            $md->save();

            $assigned_user = $request->assigned_user;
            foreach ($assigned_user as $au){
                $mau = new MeetingAssignedUser();
                $mau->meeting_id = $meeting->id;
                $mau->user = $au;
                $mau->status = '0';
                $mau->save();
            }

            foreach ($mcatId as $key=>$mc) {
                $mcat = new MeetingCategory();
                $mcat->meeting_id = $meeting->id;
                $mcat->title = $meeting_category[$key];
                $mcat->save();

                $business_item_id = $request->business_item_id[$mc];
                foreach ($business_item_id as $bii) {
                    $mbi = new MeetingBusinessItem();
                    $mbi->meeting_detail_id = $meeting->id;
                    $mbi->title = $request->business_item_title[$mc][$bii];
                    $mbi->description = $request->description[$mc][$bii];
                    $mbi->assigned_user = $request->business_assigned_user[$mc][$bii];
                    $mbi->due_date = $request->business_item_due_date[$mc][$bii];
                    $mbi->category_id = $mcat->id;
                    $mbi->status = $request->business_item_status[$mc][$bii];
                    $mbi->priority = $request->business_item_priority[$mc][$bii];
                    $mbi->save();
                }
            }
            return Reply::dataOnly(['meetingID' => $meeting->id]);
        }
    }

    public function createBusinessItem($id, $mid){
        $this->employees = User::allEmployees();
        $this->meeting = $mid;
        $this->cat = $id;
        $this->meetingCat = MeetingCategory::where('meeting_id',$mid)->get();
        return view('admin.meetings.add-business-item', $this->data);
    }

    public function convertMeetingUpdate(Request $request){
        $id = $request->id;
        $mcat = Meetings::find($id);
        $mcat->converted = $request->converted;
        $mcat->save();
        return Reply::success('Converted Successfully');
    }

    public function distributeMinute($id)
    {
        $this->employees = User::allEmployees();
        $this->meeting = Meetings::where('id',$id)->first();
        $this->meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $this->meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $this->assignedUser = MeetingAssignedUser::where('meeting_id',$id)->select('user')->get()->pluck('user')->toArray();
        $this->meetingFiles = MeetingDetailFiles::where('meeting_detail_id',$id)->get();
        return view('admin.meetings.distribute-minute', $this->data);
    }

    public function distributeMinutesUpdate($id){
        $employees = User::allEmployees();
        $meeting = Meetings::where('id',$id)->first();
        $meetingDetail = MeetingDetail::where('meeting_id',$id)->first();
        $meetingCat = MeetingCategory::where('meeting_id',$id)->get();
        $assignedUser = MeetingAssignedUser::where('meeting_id',$id)->get();

        $data = '
<div class="row"><img src="'.$this->global->logo().'" class="admin-logo"  alt="home" style="width: 150px;" /></div>
<div class="row">
        <div class="col-md-12">
            <div class="panel-heading"><h3> '.$meeting->title.'</h3></div>
            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                <tr>
                    <td>Meeting Date :</td>
                    <td>Start Time :</td>
                </tr>
                <tr>
                    <td><b>'.\Carbon\Carbon::parse($meetingDetail->meeting_date)->format('d M, Y').'</b></td>
                    <td><b>'.$meetingDetail->start_time.'</b></td>
                </tr>
                <tr>
                    <td>Finish Time :</td>
                    <td>Meeting Location :</td>
                </tr>
                <tr>
                    <td><b>'.$meetingDetail->finish_time.'</b></td>
                    <td><b>'.$meetingDetail->meeting_location.'</b></td>
                </tr>
                <tr>';
        if($meetingDetail->is_draft == '1'){
            $data .= '<td>Draft Meeting</td>';
        }
        if($meetingDetail->is_private == '1'){
            $data .='<td>Private Meeting</td>';
        }
        $data .='</tr>
            </table>

            <div class="card mb-10">
                <div class="card-header" style="background: #efefef; padding: 10px;">
                    <div class="col-md-12">
                        <p class="rowcat">Scheduled Attendees</p>
                    </div>
                </div>
                <div class="card-body" style="padding: 10px;">
                    <div class="table-responsive"><br><br><br><br>
                        <table class="table" id="example"  style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Person</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($assignedUser as $key=>$u) {
            $data .= '<tr>
                                    <td>'; $data .= $key + 1; $data .= '</td>
                                    <td>'.get_user_name($u->user).'</td>';
            $data .= '</tr>';
        }
        $data .='</tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">';
        foreach($meetingCat as $mcat) {
            $data .= '<div class="row" id="cat-' . $mcat->id . '">
                                    <div class="col-md-12">
                                        <div class="card mb-10">
                                            <div class="card-header">
                                                <div class="col-md-12">
                                                    <p class="rowcat">' . $mcat->title . '
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="card-body">';
            $bitems = \App\MeetingBusinessItem::where('category_id', $mcat->id)->where('status','<>','0')->get();
            if (count($bitems)) {
                $data .= '<div class="table-responsive"><br><br><br><br>
                                        <table class="table" id="example" style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%">#</th>
                                                    <th>Title</th>
                                                    <th>Due Date</th>
                                                    <th>Priority</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
                foreach ($bitems as $key => $meetingD) {
                    $data .= '<tr>
                                                    <td>';
                    $data .= $key + 1;
                    $data .= '</td>
                                                    <td>' . $meetingD->title . '</td>
                                                    <td>' . $meetingD->due_date . '</td>
                                                    <td>' . get_priority($meetingD->priority) . '</td>
                                                    <td>';
                    if ($meetingD->status == '0') {
                        $data .= 'Open';
                    }
                    if ($meetingD->status == '1') {
                        $data .= 'Closed';
                    }
                    if ($meetingD->status == '2') {
                        $data .= 'Pending';
                    }
                    if ($meetingD->status == '3') {
                        $data .= 'Completed';
                    }
                    if ($meetingD->status == '4') {
                        $data .= 'Hold';
                    }
                    $data .= '</td>
                                                </tr>';
                    $catMinutes = \App\MeetingMinutes::where('meeting_id', $meeting->id)->where('category_id', $meetingD->category_id)->first();
                    if ($catMinutes !== null) {
                        $data .= '<tr>
                                                        <td colspan="5">' . $catMinutes->minutes . '</td>
                                                    </tr>';
                    } else {
                        $data .= '<tr>
                                                        <td colspan = "5" > Minutes have not been entered </td >
                                                    </tr >';
                    }
                }
                $data .= '</tbody>
                                            </table>
                                        </div>';
            }
            $data .= '</div>
                                    </div>
                                </div>
                            </div>';
        }
        $data .='</div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
        foreach ($assignedUser as $au) {
            $qty = \App\User::where('id', $au->user)->first();
            if(isset($qty->id)){
                $mailarray = array();
                $mailarray['email'] = $qty->email;
                $mailarray['subject'] = 'Meeting Agenda';
                $mailarray['message'] = $data;
                $qty->sendEmail($mailarray);
            }
        }

        $mcat = Meetings::find($id);
        $mcat->converted = '2';
        $mcat->save();

        return Reply::success('Distributed Successfully');
    }
}
