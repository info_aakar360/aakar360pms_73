<?php

namespace App\Http\Controllers\Admin;

use App\Branch;
use App\Helper\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


class BranchController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Branches';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = Branch::class;
    public $parentRoute = 'branch';
    public $parentView = "admin.branch";



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->items = Branch::orderBy('created_at', 'desc')->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:branches',
        ]);
        Branch::create([
            'name' => $request->name,
            'location' => $request->location,
            'description' => $request->description,
            'created_by' => Auth::user()->id,
        ]);
        return Reply::success(__('Created Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->item = Branch::find($request->id);
        if (empty($item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        return view($this->parentView . '.show', $this->data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->item = Branch::find($id);
        if (empty($this->item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }

        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'sometimes|string|unique:branches,name,' . $id,
        ]);
        $items = Branch::find($id);
        $items->name = $request->name;
        $items->location = $request->location;
        $items->description = $request->description;
        $items->updated_by = Auth::user()->id;
        $items->save();
        return Reply::success(__('Updated Successfully'));
    }

    public function pdf(Request $request)
    {
        $item = Branch::find($request->id);
        if (empty($item)) {
            return Reply::error(__('Item not found'));
        }
        $now = new \DateTime();
        $date = $now->format('Y-m-d h:i:s');

        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Branch Manage'
        );

        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item,  'extra' => $extra])->setPaper('a4', 'landscape');
        //return $pdf->stream('invoice.pdf');
        return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = Branch::find($id);
        if (count( Branch::find($id)->Transaction ) > 0)
        {
            return Reply::success(__('You can not delete it.Because it has Some Transaction'));
        }

        $items->delete_by = \Auth::user()->id;

        $items->delete();
//        Session::flash('success', "Successfully Trashed");
        return Reply::success(__('Deleted Successfully'));
    }


    public function trashed()
    {
        $this->items = Branch::onlyTrashed()->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }


    public function restore($id)
    {
        $items = Branch::onlyTrashed()->where('id', $id)->first();

        $items->restore();
//        Session::flash('success', 'Successfully Restore');
        return Reply::success(__('Restored Successfully'));
    }

    public function kill($id)
    {
        $items = Branch::withTrashed()->where('id', $id)->first();

        if (count( Branch::withTrashed()->find($id)->Transaction ) > 0)
        {
            return Reply::error(__('You can not delete it.Because it has Some Transaction'));
        }
        $items->forceDelete();
//        Session::flash('success', 'Permanently Delete');
        return Reply::success(__('Deleted Successfully'));
    }

    public function activeSearch(Request $request)
    {

        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = Branch::where('name', 'like', '%' . $search . '%')
            ->orWhere('location', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->paginate(60);

        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);

        $search = $request["search"];

        $this->items = Branch::where('name', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('location', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('description', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->paginate(60);

        return view($this->parentView . '.trashed', $this->data);
    }


//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);

        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }

            return redirect()->back();

        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {

            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();

        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }

    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);

        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {

            foreach ($request->items["id"] as $id) {

                $this->kill($id);
            }
            return redirect()->back();

        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }


}
