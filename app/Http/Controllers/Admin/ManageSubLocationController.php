<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Request;
use App\Helper\Reply;
use App\Http\Requests\Admin\Location\StoreLocationRequest;

use App\Location;

use App\Cities;
use App\State;
class ManageSubLocationController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Location';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'maintenance';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->location= Location::all();

        $this->state = State::where('country_id','100')->get();

        return view('admin.location.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->Location = Location::all();
        return view('admin.location.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->Location = Location::all();
        return view('admin.location.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Location $request)
    {
        $category = new Location();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.locationAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreLocationRequest $request)
    {
        $location = new Location();
       // $location->name = $request->name;
        $location->location_name = $request->buildingName;
        $location->description = $request->description;
        $location->local_address = $request->localAddress;
       // $gps=array("let:Null","long:Null");
      //  $location->gps_location = $gps;
        $location->state_id = $request->state_id;
        $location->city_id = $request->city_id;
        $location->country_id = "100";
        $location->save();
       $this->Location = Location::all();
      //  return Reply::successWithData(__('messages.locationAdded'),['data' => $locationData]);
       // return view('admin.location.index', $this->data);
        return Reply::success(__('messages.LocationAdded'));
    }

    public function getCity($state_id)
    {



         $data = array();
       // $category->name = $state_id->name;
        $data['state'] = array_pluck(State::where('country_id','100')->get(), 'name', 'id');
        $data['city']= array_pluck(Cities::where('state_id',$state_id)->get(), 'name', 'id');
        $data['status']='success';


      //  return view('admin.location.index', $this->data);
        return json_encode($data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->locations = Location::all();
        $this->location = Location::where('id',$id)->first();
        return view('admin.location.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Location::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.locationAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);
        $locationData = Location::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $$locationData]);
    }
}
