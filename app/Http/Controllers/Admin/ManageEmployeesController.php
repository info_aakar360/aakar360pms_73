<?php

namespace App\Http\Controllers\Admin;

use App\Allowance;
use App\Attendance;
use App\AttendanceSetting;
use App\Company;
use App\Currency;
use App\Designation;
use App\DirectReportTeam;
use App\DocumentType;
use App\Employee;
use App\EmployeeDetails;
use App\EmployeeDocs;
use App\EmployeeEducationDetails;
use App\EmployeeEmergencyContactDetail;
use App\EmployeeSkill;
use App\EmployeeTeam;
use App\Helper\Reply;
use App\Http\Requests\Admin\Employee\StoreRequest;
use App\Http\Requests\Admin\Employee\UpdateRequest;
use App\Http\Requests\Admin\User\StoreUser;
use App\Http\Requests\Admin\User\UpdateEmployee;
use App\Leave;
use App\LeaveType;
use App\Module;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\Overtime;
use App\Package;
use App\PaypalInvoice;
use App\PayrollRegister;
use App\PayrollVerify;
use App\Permission;
use App\PermissionRole;
use App\PfandEsiSettings;
use App\Project;
use App\ProjectMember;
use App\ProjectPermission;
use App\ProjectTimeLog;
use App\PtSettings;
use App\RazorpayInvoice;
use App\Role;
use App\RoleUser;
use App\Skill;
use App\StripeInvoice;
use App\Task;
use App\Team;
use App\UniversalSearch;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\TaskboardColumn;

class ManageEmployeesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.employees';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('employees', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->get()->pluck('project_id');
        $this->projectlist = Project::where('company_id', $user->company_id)->orWhereIn('id', $pids)->get();
        $this->employees = User::allEmployees();
        $this->skills = Skill::all();
        $this->departments = Team::all();
        $this->designations = Designation::all();
        $this->totalEmployees = count($this->employees);
        $this->roles = Role::where('roles.name', '<>', 'client')->get();
        $whoseProjectCompleted = ProjectMember::join('projects', 'projects.id', '=', 'project_members.project_id')
            ->join('users', 'users.id', '=', 'project_members.user_id')
            ->select('users.*')
            ->groupBy('project_members.user_id')
            ->havingRaw("min(projects.completion_percent) = 100 and max(projects.completion_percent) = 100")
            ->orderBy('users.id')
            ->get();

        $notAssignedProject = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name')->whereNotIn('users.id', function ($query) {
                $query->select('user_id as id')->from('project_members');
            })
            ->where('roles.name', '<>', 'client')
            ->get();
        $this->freeEmployees = $whoseProjectCompleted->merge($notAssignedProject)->count();
        return view('admin.employees.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = new EmployeeDetails();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        return view('admin.employees.create', $this->data);
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {

        $company = company();

        if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
            return Reply::error(__('messages.upgradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }

        if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
            return Reply::error(__('messages.downGradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->gender = $request->gender;
        $user->password = Hash::make($request->password);
        if ($request->hasFile('image')) {
                $storage = config('filesystems.default');
                $image = $request->image->hashName();
                switch($storage) {
                    case 'local':
                        $request->image->storeAs('user-uploads/avatar', $image);
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('avatar/', $request->image, $request->image->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('image', '=', 'avatar')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('avatar');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->image)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/');
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());

                        $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $user->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $user->image= $request->image->getClientOriginalName();
        }
        $user->save();
        $role = Role::where('name', 'employee')->where('company_id',Auth::user()->company_id)->first();

        $user->attachRole($role->id);
        $employeedetail = new EmployeeDetails();
        $employeedetail->user_id = $user->id;

        $employeedetail->address = $request->address;
        $employeedetail->save();
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->employee = User::with(['employeeDetail', 'employeeDetail.designation', 'employeeDetail.department'])->withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->employee->id)->first();
        $this->employeeDocs = EmployeeDocs::where('user_id', '=', $this->employee->id)->get();

        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }

        $completedTaskColumn = TaskboardColumn::where('slug', 'completed')->first();
        $this->taskCompleted = Task::where('user_id', $id)
            ->where('board_column_id', $completedTaskColumn->id)
            ->count();

        $hoursLogged = ProjectTimeLog::where('user_id', $id)->sum('total_minutes');
        $timeLog = intdiv($hoursLogged, 60) . ' hrs ';

        if (($hoursLogged % 60) > 0) {
            $timeLog .= ($hoursLogged % 60) . ' mins';
        }

        $this->hoursLogged = $timeLog;

        $this->activities = UserActivity::where('user_id', $id)->orderBy('id', 'desc')->get();
        $this->projects = Project::select('projects.id', 'projects.project_name', 'projects.deadline', 'projects.completion_percent')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $id)
            ->get();
        $this->leaves = Leave::byUser($id);

        $this->leaveTypes = LeaveType::byUser($id);
        $this->allowedLeaves = LeaveType::sum('no_of_leaves');
        $this->roles = Role::where('company_id', Auth::user()->company_id)->where('name', '<>', 'admin')->get();
        $this->projects = Project::where('company_id', Auth::user()->company_id)->get();
        $this->user_roles = RoleUser::where('user_id', $this->employee->id)->pluck('role_id')->toArray();
        $this->user_rolesx = Role::whereIn('id', $this->user_roles)->pluck('name')->toArray();
        $this->totalPermissions = Permission::count();
        $user = Auth::user();
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        //dd();
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->firstAdmin = User::firstAdmin();
        return view('admin.employees.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }
        return view('admin.employees.edit', $this->data);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return array
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->password != '') {
            $user->password = Hash::make($request->input('password'));
        }
        $user->mobile = $request->input('mobile');
        $user->gender = $request->input('gender');
        $user->status = $request->input('status');
        $user->login = $request->login;

        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/' . $user->image);

            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');
            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/' . $user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $user->save();

        $tags = json_decode($request->tags);
        if (!empty($tags)) {
            EmployeeSkill::where('user_id', $user->id)->delete();
            foreach ($tags as $tag) {
                // check or store skills
                $skillData = Skill::firstOrCreate(['name' => strtolower($tag->value)]);
                // Store user skills
                $skill = new EmployeeSkill();
                $skill->user_id = $user->id;
                $skill->skill_id = $skillData->id;
                $skill->save();
            }
        }

        $employee = EmployeeDetails::where('user_id', '=', $user->id)->first();
        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $user->id;
        }
        $employee->employee_id = $request->employee_id;
        $employee->address = $request->address;
        $employee->hourly_rate = $request->hourly_rate;
        $employee->slack_username = $request->slack_username;
        $employee->joining_date = Carbon::createFromFormat($this->global->date_format, $request->joining_date)->format('Y-m-d');
        $employee->last_date = null;

        if ($request->last_date != '') {
            $employee->last_date = Carbon::createFromFormat($this->global->date_format, $request->last_date)->format('Y-m-d');
        }

        $employee->department_id = $request->department;
        $employee->designation_id = $request->designation;
        $employee->save();

        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        return Reply::redirect(route('admin.employees.index'), __('messages.employeeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);

        if ($user->id == 1) {
            return Reply::error(__('messages.adminCannotDelete'));
        }

        $universalSearches = UniversalSearch::where('searchable_id', $id)->where('module_type', 'employee')->get();
        if ($universalSearches){
            foreach ($universalSearches as $universalSearch){
                UniversalSearch::destroy($universalSearch->id);
            }
        }
        User::destroy($id);
        return Reply::success(__('messages.employeeDeleted'));
    }

    public function data(Request $request)
    {

        $projectid = $request->projects;
        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->groupBy('project_id')->get()->pluck('project_id')->toArray();
        if ($request->role != 'all' && $request->role != '') {
            $userRoles = Role::findOrFail($request->role);
        }
        $users = User::leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->leftJoin('project_members', 'project_members.user_id', '=', 'users.id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'roles.name as roleName', 'roles.id as roleId', 'users.image', 'users.status');

        if(!empty($projectid)&&$projectid!='all'){
            $users = $users->where('project_members.project_id', $projectid);
        }
        if ($request->status != 'all' && $request->status != '') {
            $users = $users->where('users.status', $request->status);
        }if ($request->employee != 'all' && $request->employee != '') {
            $users = $users->where('users.id', $request->employee);
        }if ($request->role != 'all' && $request->role != '' && $userRoles) {
            if ($userRoles->name == 'admin') {
                $users = $users->where('roles.id', $request->role);
            } elseif ($userRoles->name == 'employee') {
                $users =  $users->where(\DB::raw("(select user_roles.role_id from role_user as user_roles where user_roles.user_id = users.id ORDER BY user_roles.role_id DESC limit 1)"), $request->role)
                    ->having('roleName', '<>', 'admin');
            } else {
                $users = $users->where(\DB::raw("(select user_roles.role_id from role_user as user_roles where user_roles.user_id = users.id ORDER BY user_roles.role_id DESC limit 1)"), $request->role);
            }
        }

        $users = $users->groupBy('users.id')->get();
        $roles = Role::where('name', '<>', 'client')->whereOr('name', '<>', 'contractor')->get();

        $firstAdmin = User::firstAdmin();
        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('role', function ($row) use ($roles, $firstAdmin) {
                $roleRow = '';
                if($row->roleId == 0){
                    return 'Custom Role';
                }
                elseif ($row->id != $firstAdmin->id) {
                    $roleArray = array_pluck($row->role, 'role_id');
                    $flag = 0;

                    foreach ($roles as $role) {

                        $roleRow .= '<div class="checkbox checkbox-info">
                              <input type="checkbox" name="role_' . $row->id . '" class="assign_role" data-user-id="' . $row->id . '"';
                        $custom = '';
                        foreach ($row->role as $urole) {

                            if ($role->id == $urole->role_id && $flag == 0) {
                                $roleRow .= ' checked ';
                                if(in_array(0, $roleArray)){
                                    $custom = '(Custom)';
                                }
                                if ($role->name == 'admin') {
                                    $flag = 1; //do not check any other role for user if is admin
                                }
                            }
                        }

                        if ($role->id <= 3) {
                            $roleRow .= 'id="none_role_' . $row->id . $role->id . '" data-role-id="' . $role->id . '" value="' . $role->id . '"> <label for="none_role_' . $row->id . $role->id . '" data-role-id="' . $role->id . '" data-user-id="' . $row->id . '">' . __('app.' . $role->name) . ' '. $custom. '</label></div>';
                        } else {
                            $roleRow .= 'id="none_role_' . $row->id . $role->id . '" data-role-id="' . $role->id . '" value="' . $role->id . '"> <label for="none_role_' . $row->id . $role->id . '" data-role-id="' . $role->id . '" data-user-id="' . $row->id . '">' . ucwords($role->name) . ' '. $custom. '</label></div>';
                        }

                        $roleRow .= '<br>';
                    }
                    return $roleRow;
                } else {
                    return __('messages.roleCannotChange');
                }
            })
            ->addColumn('action', function ($row) {

                return '<a href="' . route('admin.employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit & Add Details"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if ($row->status == 'active') {
                        return '<label class="label label-success">' . __('app.active') . '</label>';
                    } else {
                        return '<label class="label label-danger">' . __('app.inactive') . '</label>';
                    }
                }
            )
            ->editColumn('name', function ($row) use ($roles) {

                $designation = ($row->designation_name) ? ucwords($row->designation_name) : ' ';

              return  '<div class="row"><div class="col-sm-3 col-xs-4">'.get_users_images($row->id).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->id) . '">'.ucwords($row->name).'</a><br><span class="text-muted font-12">'.$designation.'</span></div></div>';

            })
            ->rawColumns(['name', 'action', 'role', 'status'])
            ->removeColumn('roleId')
            ->removeColumn('roleName')
            ->removeColumn('current_role')
            ->make(true);
    }

    public function salarydata(Request $request)
    {
        $users = User::all();

        $startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $endDate = Carbon::now()->timezone($this->global->timezone);

        foreach ($users as $user){
            $totalpresent = Attendance::countDaysPresentByUser($startDate,$endDate,$user->id);
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                $image = ($row->image) ? '<img src="' . asset('user-uploads/avatar/' . $row->image) . '"
                                                                alt="user" class="img-circle" width="30"> ' : '<img src="' . asset('default-profile-2.png') . '"
                                                                alt="user" class="img-circle" width="30"> ';
                $designation = ($row->designation_name) ? ucwords($row->designation_name) : ' ';
                return  '<div class="row"><div class="col-sm-3 col-xs-4">'.$image.'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->id) . '">'.ucwords($row->name).'</a><br><span class="text-muted font-12">'.$designation.'</span></div></div>';

            })
            ->addColumn('present', function ($row) use ($totalpresent) {
                return  $totalpresent;
            })
            ->addColumn('absent', function ($row)  {
                return  0;
            })
            ->addColumn('holidays', function ($row) {
                return  0;
            })->addColumn('actualdays', function ($row) {
                return  0;
            })->addColumn('actualhour', function ($row) {
                return  0;
            })
            ->addColumn('allowance', function ($row)  {
                return  '<div class="row"><div class="col-sm-12 col-xs-4"><button class="btn btn-info">Add Allowance</button></div></div>';
            })
            ->addColumn('overtime', function ($row) {
                return  0;
            })
            ->addColumn('diduction', function ($row) {
                return  0;
            })
            ->addColumn('totalamount', function ($row) {
                return  0;
            })
            ->addColumn('action', function ($row){
                return '
                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Salary Slip"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'present', 'absent', 'holidays','allowance','overtime','diduction','totalamount','action'])
            ->make(true);
    }

    public function tasks($userId, $hideCompleted)
    {
        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->join('taskboard_columns', 'taskboard_columns.id', '=', 'tasks.board_column_id')
            ->select('tasks.id', 'projects.project_name', 'tasks.heading', 'tasks.due_date', 'tasks.status', 'tasks.project_id', 'taskboard_columns.column_name', 'taskboard_columns.label_color')
            ->where('tasks.user_id', $userId);
        if ($hideCompleted == '1') {
            $tasks->where('tasks.board_column_id', $taskBoardColumn->id);
        }
        $tasks->get();

        return DataTables::of($tasks)
            ->editColumn('due_date', function ($row) {
                if ($row->due_date->isPast()) {
                    return '<span class="text-danger">' . $row->due_date->format($this->global->date_format) . '</span>';
                }
                return '<span class="text-success">' . $row->due_date->format($this->global->date_format) . '</span>';
            })
            ->editColumn('heading', function ($row) {
                return ucfirst($row->heading);
            })
            ->editColumn('column_name', function($row){
                return '<label class="label" style="background-color: '.$row->label_color.'">'.$row->column_name.'</label>';
             })
            ->editColumn('project_name', function ($row) {
                if (!is_null($row->project_name)) {
                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
                }
            })
            ->rawColumns(['column_name', 'project_name', 'due_date'])
            ->removeColumn('project_id')
            ->make(true);
    }

    public function timeLogs($userId)
    {
        $timeLogs = ProjectTimeLog::join('projects', 'projects.id', '=', 'project_time_logs.project_id')
            ->select('project_time_logs.id', 'projects.project_name', 'project_time_logs.start_time', 'project_time_logs.end_time', 'project_time_logs.total_hours', 'project_time_logs.memo', 'project_time_logs.project_id', 'project_time_logs.total_minutes')
            ->where('project_time_logs.user_id', $userId);
        $timeLogs->get();

        return DataTables::of($timeLogs)
            ->editColumn('start_time', function ($row) {
                return $row->start_time->timezone($this->global->timezone)->format($this->global->date_format . ' ' . $this->global->time_format);
            })
            ->editColumn('end_time', function ($row) {
                if (!is_null($row->end_time)) {
                    return $row->end_time->timezone($this->global->timezone)->format($this->global->date_format . ' ' . $this->global->time_format);
                } else {
                    return "<label class='label label-success'>Active</label>";
                }
            })
            ->editColumn('project_name', function ($row) {
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->editColumn('total_hours', function ($row) {
                $timeLog = intdiv($row->total_minutes, 60) . ' hrs ';
                if (($row->total_minutes % 60) > 0) {
                    $timeLog .= ($row->total_minutes % 60) . ' mins';
                }
                return $timeLog;
            })
            ->rawColumns(['end_time', 'project_name'])
            ->removeColumn('project_id')
            ->make(true);
    }

    public function export($status, $employee, $role)
    {
        if ($role != 'all' && $role != '') {
            $userRoles = Role::findOrFail($role);
        }
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', '<>', 'client')
            ->leftJoin('employee_details', 'users.id', '=', 'employee_details.user_id')
            ->leftJoin('designations', 'designations.id', '=', 'employee_details.designation_id')

            ->select(
                'users.id',
                'users.name',
                'users.email',
                'users.mobile',
                'designations.name as designation_name',
                'employee_details.address',
                'employee_details.hourly_rate',
                'users.created_at',
                'roles.name as roleName'
            );
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($employee != 'all' && $employee != '') {
            $rows = $rows->where('users.id', $employee);
        }

        if ($role != 'all' && $role != '' && $userRoles) {
            if ($userRoles->name == 'admin') {
                $rows = $rows->where('roles.id', $role);
            } elseif ($userRoles->name == 'employee') {
                $rows =  $rows->where(\DB::raw("(select user_roles.role_id from role_user as user_roles where user_roles.user_id = users.id ORDER BY user_roles.role_id DESC limit 1)"), $role)
                    ->having('roleName', '<>', 'admin');
            } else {
                $rows = $rows->where(\DB::raw("(select user_roles.role_id from role_user as user_roles where user_roles.user_id = users.id ORDER BY user_roles.role_id DESC limit 1)"), $role);
            }
        }
        $attributes =  ['roleName'];
        $rows = $rows->groupBy('users.id')->get()->makeHidden($attributes);

        $exportArray = [];

        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Designation', 'Address', 'Hourly Rate', 'Created at', 'Role'];

        foreach ($rows as $row) {
            $exportArray[] = [
                "id" => $row->id,
                "name" => $row->name,
                "email" => $row->email,
                "mobile" => $row->mobile,
                "Designation" => $row->designation_name,
                "address" => $row->address,
                "hourly_rate" => $row->hourly_rate,
                "created_at" => $row->created_at->format('Y-m-d h:i:s a'),
                "roleName" => $row->roleName
            ];
        }

        Excel::create('Employees', function ($excel) use ($exportArray) {
            $excel->setTitle('Employees');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('Employees file');

            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function assignRole(Request $request)
    {
        $userId = $request->userId;
        $roleId = $request->role;
        $employeeRole = Role::where('name', 'employee')->first();
        $user = User::findOrFail($userId);

        //RoleUser::where('user_id', $user->id)->delete();
        $user->roles()->attach($employeeRole->id);
        if ($employeeRole->id != $roleId) {
            if($request->assign) {
                $user->roles()->attach($roleId);
            }else{
                RoleUser::where('role_id', $roleId)->where('user_id', $userId)->delete();
            }
        }

        return Reply::success(__('messages.roleAssigned'));
    }

    public function assignProjectAdmin(Request $request)
    {
        $userId = $request->userId;
        $projectId = $request->projectId;
        $project = Project::findOrFail($projectId);
        $project->project_admin = $userId;
        $project->save();

        return Reply::success(__('messages.roleAssigned'));
    }

    public function docsCreate(Request $request, $id)
    {
        $this->employeeID = $id;
        return view('admin.employees.docs-create', $this->data);
    }

    public function freeEmployees()
    {
        if (\request()->ajax()) {
            $whoseProjectCompleted = ProjectMember::join('projects', 'projects.id', '=', 'project_members.project_id')
                ->join('users', 'users.id', '=', 'project_members.user_id')
                ->select('users.*')
                ->groupBy('project_members.user_id')
                ->havingRaw("min(projects.completion_percent) = 100 and max(projects.completion_percent) = 100")
                ->orderBy('users.id')
                ->get();

            $notAssignedProject = User::join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.*')
                ->whereNotIn('users.id', function ($query) {
                    $query->select('user_id as id')->from('project_members');
                })
                ->where('roles.name', '<>', 'client')
                ->get();

            $freeEmployees = $whoseProjectCompleted->merge($notAssignedProject);

            return DataTables::of($freeEmployees)
                ->addColumn('action', function ($row) {
                    return '<a href="' . route('admin.employees.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                })
                ->editColumn(
                    'created_at',
                    function ($row) {
                        return Carbon::parse($row->created_at)->format($this->global->date_format);
                    }
                )
                ->editColumn(
                    'status',
                    function ($row) {
                        if ($row->status == 'active') {
                            return '<label class="label label-success">' . __('app.active') . '</label>';
                        } else {
                            return '<label class="label label-danger">' . __('app.inactive') . '</label>';
                        }
                    }
                )
                ->editColumn('name', function ($row) {
                    $image = ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'"
                    alt="user" class="img-circle" width="30">' : '<img src="'.asset('default-profile-2.png').'"
                    alt="user" class="img-circle" width="30"> ';
                    return '<a href="'.route('admin.employees.show', $row->id).'">'.$image.' '.ucwords($row->name).'</a>';
                })
                ->rawColumns(['name', 'action', 'role', 'status'])
                ->removeColumn('roleId')
                ->removeColumn('roleName')
                ->removeColumn('current_role')
                ->make(true);
        }
        return view('admin.employees.free_employees', $this->data);
    }

    public function addJobProfile($id){
        $this->user = User::find(Auth::user()->id);
        $employee = new EmployeeDetails();
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        $this->employeeSkill = EmployeeSkill::where('user_id', '=', $this->userDetail->id)->get();
        $this->id = $id;
        return view('admin.employees.create_job_profile',$this->data);
    }

    public function storeJobProfile(Request $request){
        $tags = json_decode($request->tags);
        if (!empty($tags)) {
            EmployeeSkill::where('user_id', $request->id)->delete();
            foreach ($tags as $tag) {
                $skillData = Skill::firstOrCreate(['name' => strtolower($tag->value)]);
                $skill = new EmployeeSkill();
                $skill->user_id = $request->id;
                $skill->skill_id = $skillData->id;
                $skill->save();
            }
        }
        $employee = EmployeeDetails::where('user_id', '=', $request->id)->first();

        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $request->id;
            $employee->employee_id = $request->employee_id;
        }else{
            $employee->employee_id = $request->employee_id;
        }
        if($request->workingrate == "hourly"){
            $employee->hourly_rate = $request->hourly_rate?$request->hourly_rate:'';
        }elseif ($request->workingrate == "monthly"){
            $employee->monthly_rate = $request->hourly_rate?$request->hourly_rate:'';
        }else{
            $employee->yearly_rate = $request->hourly_rate?$request->hourly_rate:'';
        }
        $employee->joining_date = Carbon::parse($request->startDate)->format('Y-m-d');
        $employee->last_date = null;
        if ($request->last_date != '') {
            $employee->last_date = Carbon::parse($request->startDate)->format('Y-m-d');
        }
        $employee->department_id = $request->department;
        $employee->designation_id = $request->designation;
        $employee->save();
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        return Reply::success(__('Job Profile Added Successfully'));
    }

    public function  addProfileDocuments ($id){
        $employee = new EmployeeDetails();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }
        $this->docs = EmployeeDocs::where('user_id',$id)->get();
        return view('admin.employees.create_document_profile',$this->data);
    }

    public function salaryslipDetail(Request $request){
        $this->employees = User::allEmployees();
        $this->pfesidata = PfandEsiSettings::all();
        $this->ptsettings = PtSettings::all();
        $now = Carbon::now();
        $this->year = $now->format('Y');
        $this->month = $now->format('m');
        $this->verified = PayrollVerify::where('verifion_month',$this->month)->pluck('verified_values')->toArray();
        $this->totalCompanies = Company::count();
        $this->totalPackages = Package::where('default', '!=', 'trial')->count();
        $this->activeCompanies = Company::where('status', '=', 'active')->count();
        $this->inactiveCompanies = Company::where('status', '=', 'inactive')->count();
        $expiredCompanies =  Company::with('package')->where('status', 'license_expired')->get();
        $this->expiredCompanies = $expiredCompanies->count();;
        $this->recentExpired = $expiredCompanies->sortBy('updated_at')->take(5);
        $months = [
            '1' => 'jan',
            '2' => 'Feb',
            '3' => 'Mar',
            '4' => 'Apr',
            '5' => 'May',
            '6' => 'Jun',
            '7' => 'Jul',
            '8' => 'Aug',
            '9' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec',
        ];
        $invoices = StripeInvoice::selectRaw('SUM(amount) as amount,YEAR(pay_date) as year, MONTH(pay_date) as month')->whereNotNull('stripe_invoices.pay_date')->havingRaw('year = ?', [Carbon::now()->year])->groupBy('month')->get()->groupBy('month')->toArray();
        $paypalInvoices = PaypalInvoice::selectRaw('SUM(total) as total,YEAR(paid_on) as year, MONTH(paid_on) as month')->where('paypal_invoices.status', 'paid')->havingRaw('year = ?', [Carbon::now()->year])->groupBy('month')->get()->groupBy('month')->toArray();
        $razorpayInvoice = RazorpayInvoice::selectRaw('SUM(amount) as amount,YEAR(pay_date) as year, MONTH(pay_date) as month')->whereNotNull('razorpay_invoices.pay_date')->havingRaw('year = ?', [Carbon::now()->year])->groupBy('month')->get()->groupBy('month')->toArray();
        $chartData = [];
        foreach($months as $key => $month) {
            if(key_exists($key, $invoices)) {
                foreach($invoices[$key] as $amount) {
                    $chartData[] = ['month' => $month, 'amount' => $amount['amount']];
                }
            }
            else{
                $chartData[] = ['month' => $month, 'amount' => 0];
            }
            if(key_exists($key, $razorpayInvoice)) {
                foreach($razorpayInvoice[$key] as $amount) {
                    $chartData[] = ['month' => $month, 'amount' => $amount['amount']];
                }
            }
            else{
                $chartData[] = ['month' => $month, 'amount' => 0];
            }
            if(key_exists($key, $paypalInvoices)) {
                foreach($paypalInvoices[$key] as $amount) {
                    $chartData[] = ['month' => $month, 'amount' => $amount['total']];
                }
            }
            else{
                $chartData[] = ['month' => $month, 'amount' => 0];
            }
        }
        $this->chartData = json_encode($chartData);
        // Collect data of recent registered 5 companies
        $this->recentRegisteredCompanies = Company::with('package')->take(5)->latest()->get();
        $stripe = DB::table("stripe_invoices")
            ->join('packages', 'packages.id', 'stripe_invoices.package_id')
            ->join('companies', 'companies.id', 'stripe_invoices.company_id')
            ->selectRaw('stripe_invoices.id ,companies.company_name, packages.name, companies.package_type,"Stripe" as method, stripe_invoices.pay_date as paid_on, "" as end_on ,stripe_invoices.next_pay_date, stripe_invoices.created_at')
            ->whereNotNull('stripe_invoices.pay_date');

        $razorpay = DB::table("razorpay_invoices")
            ->join('packages', 'packages.id', 'razorpay_invoices.package_id')
            ->join('companies', 'companies.id', 'razorpay_invoices.company_id')
            ->selectRaw('razorpay_invoices.id ,companies.company_name , packages.name as name, companies.package_type, "Razorpay" as method, razorpay_invoices.pay_date as paid_on , "" as end_on,razorpay_invoices.next_pay_date,razorpay_invoices.created_at')
            ->whereNotNull('razorpay_invoices.pay_date');

        $allInvoices = DB::table("paypal_invoices")
            ->join('packages', 'packages.id', 'paypal_invoices.package_id')
            ->join('companies', 'companies.id', 'paypal_invoices.company_id')
            ->selectRaw('paypal_invoices.id,companies.company_name, packages.name, companies.package_type, "Paypal" as method, paypal_invoices.paid_on, paypal_invoices.end_on,paypal_invoices.next_pay_date,paypal_invoices.created_at')
            ->where('paypal_invoices.status', 'paid')
            ->union($stripe)
            ->union($razorpay)
            ->get();

        $this->recentSubscriptions = $allInvoices->sortByDesc(function ($temp, $key) {
            return Carbon::parse($temp->created_at)->getTimestamp();
        })->take(5);

        try {
            $client = new Client();
            $res = $client->request('GET', config('froiden_envato.updater_file_path'), ['verify' => false]);
            $lastVersion = $res->getBody();
            $lastVersion = json_decode($lastVersion, true);

            if ($lastVersion['version'] > File::get(public_path().'version.txt')) {
                $this->lastVersion = $lastVersion['version'];
            }
        } catch (\Throwable $th) {
            //throw $th;
        }

        return view('admin.employees.salary_slip',$this->data);
    }

    public function showSalarySlip(){
        return view('admin.employees.show_salary_slip',$this->data);
    }

    public function addAllowace(){
        $this->currencies = Currency::all();
        $this->employees = User::allEmployees();

        $employees = $this->employees->toArray();
        foreach ($employees as $key => $employee) {
            $user = User::select('id', 'name')->where('id', $employee['id'])->first();
            $user_arr = [
                'id' => $user->id,
                'name' => $user->name
            ];
            $employee = array_add($employee, 'user', $user_arr);
            $employees[$key] = $employee;
        }
        foreach ($this->employees as $employee) {
            $filtered_array = array_filter($employees, function ($item) use ($employee) {
                return $item['user']['id'] == $employee->id;
            });
            $projects = [];
            foreach ($employee->member as $member) {
                if (!is_null($member->project)) {
                    array_push($projects, $member->project()->select('id', 'project_name')->first()->toArray());
                }
            }
            $employees[key($filtered_array)]['user'] = array_add(reset($filtered_array)['user'], 'projects', $projects);
        }
        $this->employees = $employees;
        return view('admin.employees.add_allowace',$this->data);
    }

    public function postAllowance(Request $request){
        $expense = new Allowance();
        $expense->item_name = $request->item_name;
        $expense->purchase_date = Carbon::createFromFormat($this->global->date_format, $request->purchase_date)->format('Y-m-d');
        $expense->price = round($request->price, 2);
        if(!empty($expense->currency_id)){
            $expense->currency_id = $request->currency_id;
        }
        $expense->expence_type = $request->expence_type;
        $expense->user_id = $request->user_id;

        if ($request->project_id > 0) {
            $expense->project_id = $request->project_id;
        }

        if ($request->hasFile('bill')) {
            $expense->bill = $request->bill->hashName();
            $request->bill->store('user-uploads/expense-invoice');
            //            dd($expense->bill);
            // $img = Image::make('user-uploads/expense-invoice/' . $expense->bill);
            // $img->resize(500, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
        }

        $expense->status = 'approved';
        $expense->save();


        return Reply::redirect(route('admin.employees.allowance'), __('messages.expenseSuccess'));
    }

    public function allowance(){
        $this->employees = User::allEmployees();
        return view('admin.employees.allowance',$this->data);
    }

    public function allowaceData(){
        $allowancedata = Allowance::all();
        return DataTables::of($allowancedata)
        ->addIndexColumn()
        ->editColumn('employee',function ($row) {
                $user = User::find($row->user_id);
                return $user->name;
        })
        ->editColumn('project',function ($row){
            $project = Project::find($row->project_id);
            return $project->project_name;
        })
        ->editColumn('price',function ($row){
            return $row->price;
        })
        ->editColumn('type',function ($row){
            return $row->expence_type;
        })
        ->editColumn('remark',function ($row){
            return $row->item_name;
        })->editColumn('status',function ($row){
            return $row->status;
        })
        ->addColumn('action', function ($row) {
            return '<a href="' . route('admin.employees.edit_allowance', [$row->id]) . '" class="btn btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                  <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                  data-toggle="tooltip" data-allowance-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
        })

        ->rawColumns(['action','status'])
        ->make(true);
    }


    public function editAllowance($id){
        $this->employees = User::allEmployees();

        $employees = $this->employees->toArray();
        foreach ($employees as $key => $employee) {
            $user = User::select('id', 'name')->where('id', $employee['id'])->first();
            $user_arr = [
                'id' => $user->id,
                'name' => $user->name
            ];
            $employee = array_add($employee, 'user', $user_arr);
            $employees[$key] = $employee;
        }
        foreach ($this->employees as $employee) {
            $filtered_array = array_filter($employees, function ($item) use ($employee) {
                return $item['user']['id'] == $employee->id;
            });
            $projects = [];

            foreach ($employee->member as $member) {
                if (!is_null($member->project)) {
                    array_push($projects, $member->project()->select('id', 'project_name')->first()->toArray());
                }
            }
            $employees[key($filtered_array)]['user'] = array_add(reset($filtered_array)['user'], 'projects', $projects);
        }
        $this->employees = $employees;
        $allowance = Allowance::find($id);
        $this->exproject = Project::find($allowance->project_id);
        $this->exemployee = User::find($id);
        $this->allowance = $allowance;
        return view('admin.employees.edit_allowance',$this->data);
    }

    public function updateAllowance(Request $request){
        $expense = Allowance::find($request->id);
        $expense->item_name = $request->item_name;
        $expense->purchase_date = Carbon::createFromFormat($this->global->date_format, $request->purchase_date)->format('Y-m-d');
        $expense->price = round($request->price, 2);
        if(!empty($expense->currency_id)){
            $expense->currency_id = $request->currency_id;
        }
        $expense->expence_type = $request->expence_type;
        $expense->user_id = $request->user_id;

        if ($request->project_id > 0) {
            $expense->project_id = $request->project_id;
        }

        if ($request->hasFile('bill')) {
            $expense->bill = $request->bill->hashName();
            $request->bill->store('user-uploads/expense-invoice');

        }

        $expense->status = 'approved';
        $expense->save();


        return Reply::redirect(route('admin.employees.allowance'), __('messages.expenseSuccess'));
    }

    public function destroyAllowance($id){
        $allowance = Allowance::find($id);
        $allowance->delete();
    }
    public function  addOvertime(){
        $this->currencies = Currency::all();
        $this->employees = User::allEmployees();

        $employees = $this->employees->toArray();
        foreach ($employees as $key => $employee) {
            $user = User::select('id', 'name')->where('id', $employee['id'])->first();
            $user_arr = [
                'id' => $user->id,
                'name' => $user->name
            ];
            $employee = array_add($employee, 'user', $user_arr);
            $employees[$key] = $employee;
        }
        foreach ($this->employees as $employee) {
            $filtered_array = array_filter($employees, function ($item) use ($employee) {
                return $item['user']['id'] == $employee->id;
            });
            $projects = [];

            foreach ($employee->member as $member) {
                if (!is_null($member->project)) {
                    array_push($projects, $member->project()->select('id', 'project_name')->first()->toArray());
                }
            }
            $employees[key($filtered_array)]['user'] = array_add(reset($filtered_array)['user'], 'projects', $projects);
        }
        $this->employees = $employees;
        return view('admin.employees.add_overtime',$this->data);
    }

    public function overtime(){
        $this->employees = User::allEmployees();
        return view('admin.employees.overtime',$this->data);
    }

    public function overtimeData(){
        $overtimedata = Overtime::all();
        return DataTables::of($overtimedata)
        ->addIndexColumn()
        ->editColumn('employee',function ($row) {
                $user = User::find($row->user_id);
                return $user->name;
        })
        ->editColumn('clock_in',function ($row){
            return $row->in_time;
        })
        ->editColumn('clock_out',function ($row){
            return $row->out_time;
        })
        ->editColumn('salarytype',function ($row){
            return $row->salary_type;
        })
        ->editColumn('amount',function ($row){
            return $row->amount;
        })
        ->addColumn('action', function ($row) {
            return '<a href="' . route('admin.employees.edit_overtime', [$row->id]) . '" class="btn btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                  <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                  data-toggle="tooltip" data-overtime-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
        })

        ->rawColumns(['action','status'])
        ->make(true);
    }

    public function deleteOvertime($id){
        $overtime = Overtime::find($id);
        $overtime->delete();
    }

    public function overtimeEdit($id){
        $this->employees = User::allEmployees();
        $this->overtime = Overtime::findOrFail($id);
        return view('admin.employees.edit_overtime',$this->data);
    }

    public function postOvertime(Request $request){
        $company = company();

        if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
            return Reply::error(__('messages.upgradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }

        if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
            return Reply::error(__('messages.downGradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if($request->clock_out_time != ''){
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_in_out, $this->global->timezone);
            $clockOut->setTimezone('UTC');
            $clockOut = $date.' '.$clockOut->format('H:i:s');

        }
        else{
            $clockOut = null;
        }
        $user = new Overtime();
        $user->user_id = $request->input('user_id');
        $user->in_time = $date.' '.$clockIn;
        $user->out_time = $date.' '.$clockOut;
        $user->salary_type = $request->input('sallary_id');
        $user->amount = $request->input('amount');
        $user->save();

        return Reply::redirect(route('admin.employees.overtime'), __('messages.employeeAdded'));
    }

    public function updateOvertime(Request $request){
        $company = company();

        if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
            return Reply::error(__('messages.upgradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }

        if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
            return Reply::error(__('messages.downGradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if($request->clock_out_time != ''){
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_in_out, $this->global->timezone);
            $clockOut->setTimezone('UTC');
            $clockOut = $date.' '.$clockOut->format('H:i:s');
        }
        else{
            $clockOut = null;
        }

        $user = Overtime::find($request->id);
        $user->user_id = $request->input('user_id');
        $user->in_time = $date.' '.$clockIn;
        $user->out_time = $date.' '.$clockOut;
        $user->salary_type = $request->input('sallary_id');
        $user->amount = $request->input('amount');
        $user->save();
        return Reply::redirect(route('admin.employees.overtime'), __('messages.employeeAdded'));
    }

    public function reportTeam($id){
        $employees = User::all();
        $this->employeelists =  view('admin.employees.employeelist',['employees'=>$employees])->render();
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $this->employeeTeam = EmployeeTeam::where('user_id',$this->userDetail->id)->get();
        $this->employeeDirect = DirectReportTeam::where('user_id',$this->userDetail->id)->get();
        return view('admin.employees.report_team',$this->data);
    }
    public function educationDetail($id){
        $employees = User::all();
        $this->employeelists =  view('admin.employees.employeelist',['employees'=>$employees])->render();
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        return view('admin.employees.educationdetail',$this->data);
    }
    public function familyDetail($id){
        $employees = User::all();
        $this->employeelists =  view('admin.employees.employeelist',['employees'=>$employees])->render();
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('user_id', '=', $this->userDetail->id)->first();
        $this->familyDetail = EmployeeEmergencyContactDetail::where('user_id',$id)->get();
        return view('admin.employees.familydetail',$this->data);
    }

    public function storeReportManager(Request $request){
        $reportManager = array_combine($request->date, $request->occasion);
        foreach ($reportManager as $index => $value) {
            if ($index){
                $add = Holiday::firstOrCreate([
                    'user_id' => $value->user_id,
                    'type' => $value->type,
                ]);
            }
        }
        return Reply::redirect(route('member.employees.team'), __('messages.managerReportAddedSuccess'));
    }
    public function storeFamilyDetail(Request $request){
       if($request->emg_name > 0){
        foreach ($request->emg_name as $index => $value) {

                $add = EmployeeEmergencyContactDetail::firstOrCreate([
                    'user_id' => $request->user_id,
                    'family_memeber_name' => $request->emg_name[$index],
                    'family_memeber_relation' => $request->emg_relation[$index],
                    'family_memeber_number' => $request->number[$index],
                ]);
            }
        }
        return Reply::redirect(route('admin.employees.family',[$request->user_id]), __('messages.managerReportAddedSuccess'));
    }
    public function storeEducationDetail(Request $request){

        $date =  Carbon::today()->timezone($this->global->timezone);

       if($request->qualification_type > 0){
        foreach ($request->qualification_type as $index => $value) {

                $add = EmployeeEducationDetails::firstOrCreate([
                    'user_id' => $request->user_id,
                    'qualification_type' => $request->qualification_type[$index],
                    'course_name' => $request->course_name[$index],
                    'course_type' => $request->course_type[$index],
                    'stream' => $request->stream[$index],
                    'course_start_date' => $request->startDate[$index],
                    'course_end_date' => $request->endDate[$index],
                    'college_name' => $request->college_name[$index],
                    'univercity_name' => $request->univercity_name[$index],
                ]);
            }
        }
        return Reply::redirect(route('admin.employees.family',[$request->user_id]), __('messages.managerReportAddedSuccess'));
    }

    public function getSalaryOvervies(Request $request){
        $this->totalEmployee  = Employee::getAllEmployees($this->user)->count();
        $this->totalpayroll  = PayrollRegister::where('month',$request->month)->where('year',$request->year)->count();
        $this->totalgrosspay  = PayrollRegister::where('month',$request->month)->where('year',$request->year)->sum('gross');
        $this->totalnetpay  = PayrollRegister::where('month',$request->month)->where('year',$request->year)->sum('net_pay');
        $this->tax =  User::join('employee_tax_details','employee_tax_details.user_id','=','users.id')->where('users.id',$this->user->id)->select('incometax')->first();
        return  view('admin.employees.salary_slip_overview',$this->data)->render();

    }

    public function saveRolePermissions(Request $request, $id){
        $user = User::find($id);
        if($user === null){
            return Reply::error('User not Found');
        }
        $role = isset($request->custom) ? $request->role_type : $request->roles;
        $check = ProjectMember::where('company_id', $user->company_id)->where('user_id', $id)->first();
        if($check !== null){
            $uppm = ProjectMember::find($check->id);
            $uppm->user_type = $role;
            $uppm->save();
        }else{
            $pm = new ProjectMember();
            $pm->company_id = $user->company_id;
            $pm->user_id = $id;
            $pm->project_id = $request->project_id;
            $pm->user_type = $role;
            $pm->save();
        }
        if(!isset($request->custom)){
            $rolex = Role::where('name', $request->role_type)->where('company_id', $user->company_id)->first();

            $permissions = PermissionRole::where('role_id', $rolex->id)->get()->pluck('permission_id');
            ProjectPermission::where('user_id', $user->id)->where('project_id', $request->project_id)->delete();
            foreach ($permissions as $permission){
                $pprm = new ProjectPermission();
                $pprm->project_id = $request->project_id;
                $pprm->user_id = $user->id;
                $pprm->permission_id = $permission;
                $pprm->save();
            }
        }else{
            ProjectPermission::where('user_id', $user->id)->where('project_id', $request->project_id)->delete();
            $permissions = $request->rolesx;
            foreach ($permissions as $key=>$value) {
                $pprm = new ProjectPermission();
                $pprm->project_id = $request->project_id;
                $pprm->user_id = $user->id;
                $pprm->permission_id = $key;
                $pprm->save();
            }
        }
        return Reply::redirect(route('admin.employees.index'), __('messages.roleAssigned'));
    }

    public function addId($id){
        $this->types = DocumentType::all();
        $this->id = $id;
        return  view('admin.employees.add_id', $this->data);
    }

    public function uploadId(Request $request, $id){
        $user = new EmployeeDocs();
        if ($request->hasFile('file')) {
            $storage = config('filesystems.default');
            $image = $request->file->hashName();
            switch($storage) {
                case 'local':
                    $request->file->storeAs('user-uploads/documents', $image);
                    break;
                case 's3':
                    $st = Storage::disk('s3')->putFileAs('documents/', $request->file, $request->file->getClientOriginalName(), 'public');
                    $user->filename = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/".str_replace('//', '/',$st);
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'documents')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('documents');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->file)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->file)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                    $user->filename = $user->google_url;
                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/documents/'.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    $user->filename = $user->dropbox_link;
                    break;
            }
            $user->company_id = $this->user->company_id;
            $user->user_id = $id;
            $user->doc_type = $request->document_type;
            $user->filename = $request->file->getClientOriginalName();
            $user->save();
        }
        return Reply::redirect(route('admin.employees.employee_profile_documents', [$id]), __('Document Uploaded Successfully'));
    }
    public function deleteId($id)
    {
        EmployeeDocs::destroy($id);
        return Reply::success(__('Document Deleted'));
    }
    public function editId($id){
        $this->types = DocumentType::all();
        $this->doc = EmployeeDocs::where('id',$id)->first();
        $this->id = $id;
        return  view('admin.employees.edit_id', $this->data);
    }

    public function updateId(Request $request, $id){
        $user = EmployeeDocs::find($id);
        if ($request->hasFile('file')) {
            $storage = config('filesystems.default');
            $image = $request->file->hashName();
            switch($storage) {
                case 'local':
                    $request->file->storeAs('user-uploads/documents', $image);
                    break;
                case 's3':
                    $st = Storage::disk('s3')->putFileAs('documents/', $request->file, $request->file->getClientOriginalName(), 'public');
                    $user->filename = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/".str_replace('//', '/',$st);
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'documents')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('documents');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->file)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->file)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                    $user->filename = $user->google_url;
                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/documents/'.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    $user->filename = $user->dropbox_link;
                    break;
            }
            $user->doc_type = $request->document_type;
            $user->filename = $request->file->getClientOriginalName();
            $user->save();
        }
        return Reply::redirect(route('admin.employees.employee_profile_documents', [$id]), __('Document Updated Successfully'));
    }

    public function storeReportTeam(Request $request){
        $team_id = $request->employee;
        foreach ($team_id as $key=>$ti) {
            $et = new EmployeeTeam();
            $et->team_id = $ti;
            $et->user_id = $request->user_id;
            $et->type = $request->type[$key];
            $et->save();
        }
        return Reply::success(__('Added Successfully'));
    }

    public function storeDirectTeam(Request $request){
        $team_id = $request->employee;
        foreach ($team_id as $key=>$ti) {
            $et = new DirectReportTeam();
            $et->team_id = $ti;
            $et->user_id = $request->user_id;
            $et->save();
        }
        return Reply::success(__('Added Successfully'));
    }
}
