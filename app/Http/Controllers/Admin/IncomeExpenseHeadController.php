<?php

namespace App\Http\Controllers\Admin;


use App\AccountPayments;
use App\Helper\Reply;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\IncomeExpenseHeadFiles;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\DataTables;


class IncomeExpenseHeadController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Income Expense Head';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = IncomeExpenseHead::class;
    public $parentRoute = 'admin.income_expense_head';
    public $parentView = "admin.income-expense-head";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $this->itemsarray = IncomeExpenseHead::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $this->groupsarray = IncomeExpenseGroup::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user;
        $request->validate([
            'name' => 'required|string',
            'income_expense_group_id' => 'required|numeric|min:1',
        ]);
        $prevhead = IncomeExpenseHead::where('company_id',$user->company_id)->where('name',$request->name)->first();
        if(!empty($prevhead)){
            return Reply::error(__('Ledger name already exists'));
        }
        $incomegroup = IncomeExpenseGroup::where('id',$request->income_expense_group_id)->first();
        $items = new IncomeExpenseHead();
        $items->company_id = $user->company_id;
        $items->name = $request->name;
        $items->amount = $request->amount;
        $items->voucherdate = $request->voucher_date;
        $items->particulars = $request->particulars;
        $items->income_expense_type_id = $incomegroup->income_type;
        $items->income_expense_group_id = $incomegroup->id;
        $items->created_by = $user->id;
        $items->save();
        return Reply::dataOnly(['ledgerID' => $items->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->item = IncomeExpenseHead::find($request->id);
        if (empty($this->item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        return view($this->parentView . '.show')->with('items', $this->item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $this->groupsarray = IncomeExpenseGroup::where('company_id',$user->company_id)->get();
        $this->item = IncomeExpenseHead::find($id);
        if (empty($this->item)) {
            Session::flash('error', "Name not found");
            return redirect()->back();
        }
        $this->files = IncomeExpenseHeadFiles::where('ledger_id',$this->item->id)->get();
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $request->validate([
            'name' => 'sometimes|string',
            'income_expense_group_id' => 'required|numeric|min:1',
        ]);
        $prevhead = IncomeExpenseHead::where('company_id',$user->company_id)->where('name',$request->name)->where('id','<>',$id)->first();
        if(!empty($prevhead)){
            return Reply::error(__('Ledger name already exists'));
        }
        $incomegroup = IncomeExpenseGroup::where('id',$request->income_expense_group_id)->first();
        $items = IncomeExpenseHead::find($id);
        $items->company_id = $user->company_id;
        $items->name = $request->name;
        $items->amount = $request->amount;
        $items->voucherdate = $request->voucher_date;
        $items->particulars = $request->particulars;
        $items->income_expense_type_id = $incomegroup->income_type;
        $items->income_expense_group_id = $incomegroup->id;
        $items->created_by = $user->id;
        $items->save();

        return Reply::dataOnly(['ledgerID' => $items->id]);
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $storage = storage();
            $company = $this->user->company_id;
            $punchitem = IncomeExpenseHead::find($request->ledger_id);
            foreach ($request->file as $fileData) {
                $file = new IncomeExpenseHeadFiles();
                $file->company_id = $punchitem->company_id;
                $file->added_by = $this->user->id;
                $file->ledger_id = $punchitem->id; 
                switch ($storage) {
                    case 'local':
                        $destinationPath = 'uploads/ledger-files/' . $file->ledger_id;
                        if (!file_exists('' . $destinationPath)) {
                            mkdir('' . $destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('/ledger-files/' . $file->ledger_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'ledger-files')
                            ->first();

                        if (!$dir) {
                            Storage::cloud()->makeDirectory('ledger-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->ledger_id)
                            ->first();

                        if (!$directory) {
                            Storage::cloud()->makeDirectory($dir['path'] . '/' . $request->ledger_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->ledger_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('ledger-files/' . $request->ledger_id . '/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/ledger-files/' . $request->ledger_id . '/' . $fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->ledger_id, __('messages.newFileUploadedToTheProject'));
            }
        }
        return Reply::success("Ledger updated successfully");
    }

    public function pdf(Request $request)
    {
        $item = IncomeExpenseHead::find($request->id);
        if (empty($item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format').' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Ledger Name'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
        //return $pdf->stream('invoice.pdf');
        return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = IncomeExpenseHead::find($id);
        if (empty($items)) {
            return Reply::error(__('Item not found'));
        }
        if (count(IncomeExpenseHead::find($id)->Transaction) > 0) {
            return Reply::error(__('You can not delete it.Because it has Some Transaction'));
        }
        $items->deleted_by = \Auth::user()->id;
        $items->save();
        $items->delete();
        return Reply::success(__('Successfully Trashed'));
    }

    public function trashed()
    {
        $this->items = IncomeExpenseHead::onlyTrashed()->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

    public function restore($id)
    {
        $items = IncomeExpenseHead::onlyTrashed()->where('id', $id)->first();
        $items->restore();
        $items->updated_by = \Auth::user()->email;
        $items->save();
        return Reply::success(__('Successfully Restore'));
    }

    public function kill($id)
    {
        $items = IncomeExpenseHead::withTrashed()->where('id', $id)->first();
        if (count(IncomeExpenseHead::withTrashed()->find($id)->Transaction) > 0) {
            return Reply::error(__('You can not delete it.Because it has Some Transaction'));
        }
        $items->forceDelete();
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = IncomeExpenseHead::where('name', 'like', '%' . $search . '%')
            ->orWhereHas('IncomeExpenseType', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->orWhereHas('IncomeExpenseGroup', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->orWhere('unit', 'like', '%' . $search . '%')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = IncomeExpenseHead::where('name', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhereHas('IncomeExpenseType', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->onlyTrashed()
            ->orWhereHas('IncomeExpenseGroup', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            })
            ->onlyTrashed()
            ->orWhere('unit', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }

}
