<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\Http\Requests\ProductBrand\StoreProBrandRequest;
use App\ProductBrand;
use App\ProductCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class ProductBrandController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Brands';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'store';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->categories = ProductBrand::all();
        return view('admin.product-brand.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::all();
        $this->category = BoqCategory::where('id',$id)->first();
        return view('admin.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = BoqCategory::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function data()
    {
        $products = ProductBrand::select('id', 'name')->get();

        return DataTables::of($products)
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            
            ->make(true);
    }

    public function addBrand(){
        return view('admin.product-brand.create',$this->data);
    }
    public function postBrand(StoreProBrandRequest $request){
        $brand  = new ProductBrand();
        $brand->name = $request->name;
        $brand->company_id = $this->companyid;
        $brand->save();
        return Reply::success(__('Brand Added Successfully!!'));
    }
    public function brandData(){
        $brandData = ProductBrand::where('company_id',$this->companyid)->get();
        return DataTables::of($brandData)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                return '<a onclick="showEditBrand('.$row->id.')" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('name', function ($row) {
                return $row->name;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function editBrand($id){
        $this->brands = ProductBrand::find($id);
        return view('admin.product-brand.edit',$this->data);
    }
    public function updateBrand(StoreProBrandRequest $request){

        $brand  = ProductBrand::find($request->rowid);
        $brand->name = $request->name;
        $brand->company_id = $this->companyid;
        $brand->save();
        return Reply::success(__('Brand Updated Successfully!!'));
    }
    public function destroyBrand($id){
        ProductBrand::destroy($id);
        return Reply::success(__('Brand Deleted Successfully!!'));
    }
}
