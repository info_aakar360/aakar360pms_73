<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\IncomeExpenseType;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;

class IncomeExpenseTypeController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Income Expense Type';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }


//    Important properties
    public $parentModel = IncomeExpenseType::class;
    public $parentRoute = 'income_expense_type';
    public $parentView = "admin.income-expense-type";


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $this->itemsarray =  IncomeExpenseType::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user;
        $request->validate([
            'name' => 'required|string|unique:income_expense_types',
        ]);

        IncomeExpenseType::create([
            'name' => $request->name,
            'code' => $request->code,
            'created_by' => $user->id,
        ]);
        return Reply::success(__('Created Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->item = IncomeExpenseType::find($request->id);
        if (empty($this->item)) {
            return redirect()->back()->with('error','Item not found');
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->item = IncomeExpenseType::find($id);
        if (empty($this->item)) {
            return redirect()->back()->with('error','Item not found');
        }
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'sometimes|string|unique:income_expense_types,name,' . $id,
            'code' => 'sometimes|string|unique:income_expense_types,code,' . $id,
        ]);

        $items = IncomeExpenseType::find($id);
        $items->name = $request->name;
        $items->code = $request->code;
        $items->updated_by = \Auth::user()->id;
        $items->save();
        return Reply::success(__('Updated Successfully'));
    }

    public function pdf(Request $request)
    {
        $item = IncomeExpenseType::find($request->id);
        if (empty($item)) {
            return Reply::error(__('Item not found'));
        }

        $now = new \DateTime();
        $date = $now->format('Y-m-d h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Ledger Type'
        );

        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
        return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = IncomeExpenseType::find($id);
        if (empty($items)) {
            return Reply::error(__('Item not found'));
        }

        if (count(IncomeExpenseType::find($id)->IncomeExpenseHeads) > 0) {  // Child has or not
            return Reply::error(__('You can not delete it.Because it has ledger items'));
        }
        $items->delete_by = \Auth::user()->id;
        $items->delete();
        return Reply::success(__('Successfully Trashed'));
    }


    public function trashed()
    {
        $this->items = IncomeExpenseType::onlyTrashed()->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }


    public function restore($id)
    {
        $items = IncomeExpenseType::onlyTrashed()->where('id', $id)->first();
        $items->restore();
        return Reply::success(__('Restored Successfully'));
    }

    public function kill($id)
    {
        $items = IncomeExpenseType::withTrashed()->where('id', $id)->first();
        if (count(IncomeExpenseType::withTrashed()->find($id)->IncomeExpenseHeads) > 0) {  // Child has or not
            return Reply::error(__('You can not delete it.Because it has ledger items'));
        }
        $items->forceDelete();
        return Reply::success(__('Deleted Successfully'));
    }

    public function activeSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);

        $search = $request["search"];
        $this->items = IncomeExpenseType::where('name', 'like', '%' . $search . '%')
            ->orWhere('code', 'like', '%' . $search . '%')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = IncomeExpenseType::where('name', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('code', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }


//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
}
