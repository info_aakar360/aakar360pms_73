<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Helper\Reply;
use App\Http\Requests\Project\StoreProjectCategory;
use Illuminate\Http\Request;

class ManageTaskCategoryController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        $this->parentcategories = BoqCategory::where('company_id',$user->company_id)->where('parent','0')->get();
        return view('admin.tasks.create-category', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectCategory $request)
    {
        $user = $this->user;
        $category = new BoqCategory();
        $category->company_id = $user->company_id;
        $category->title = $request->category_name;
        $category->parent = $request->parent_id;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreProjectCategory $request)
    {
        $user = $this->user;
        $category = new BoqCategory();
        $category->company_id = $user->company_id;
        $category->title = $request->category_name;
        $category->parent = $request->parent_id;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        return Reply::success(__('messages.categoryDeleted'));
    }
}
