<?php

namespace App\Http\Controllers\Admin;

use App\AdvanceRules;
use App\Attendance;
use App\AttendanceRules;
use App\AttendanceSetting;
use App\Designation;
use App\Employee;
use App\EmployeeDetails;
use App\Helper\Reply;
use App\Holiday;
use App\Http\Requests\Attendance\StoreAttendance;
use App\Http\Requests\Attendance\GeneralRulesRequest;
use App\Leave;
use App\Role;
use App\RoleUser;
use App\RulesUser;
use App\Skill;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class ManageAttendanceController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.attendance';
        $this->pageIcon = 'icon-clock';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('attendance',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });


        // Getting Attendance setting data
        $this->attendanceSettings = AttendanceSetting::first();

        //Getting Maximum Check-ins in a day

        if(!empty($this->attendanceSettings)){
            $this->maxAttandenceInDay = $this->attendanceSettings->clockin_in_day;
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $openDays = json_decode($this->attendanceSettings->office_open_days);
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::now()->timezone($this->global->timezone);
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->companId = User::first()->id;

        $this->totalWorkingDays = $this->startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $this->endDate);
        $this->daysPresent = Attendance::countDaysPresentByUser($this->startDate, $this->endDate, $this->companId);
        $this->daysLate = Attendance::countDaysLateByUser($this->startDate, $this->endDate, $this->companId);
        $this->halfDays = Attendance::countHalfDaysByUser($this->startDate, $this->endDate, $this->companId);
        $this->holidays = Count(Holiday::getHolidayByDates($this->startDate->format('Y-m-d'), $this->endDate->format('Y-m-d')));

        return view('admin.attendance.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::now()->timezone($this->global->timezone);
        /*$this->employees = User::allEmployees();*/
        $this->empdata = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.attendance.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAttendance $request)
    {

        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn = $clockIn->format('H:i:s');
        if($request->clock_out_time != ''){
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);

            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date.' '.$clockOut;
        }
        else{
            $clockOut = null;
        }

        if(Str::startsWith($request->user_id,'emp-')) {
            $emp_id = ltrim($request->user_id,'emp-');
            $attendance = Attendance::where('emp_id',$emp_id)
                ->where(DB::raw('DATE(`clock_in_time`)'), $date)
                ->whereNull('clock_out_time')
                ->first();
        }else{
            $attendance = Attendance::where('user_id', $request->user_id)
                ->where(DB::raw('DATE(`clock_in_time`)'), $date)
                ->whereNull('clock_out_time')
                ->first();
        }
       /* if(Str::startsWith($request->user_id,'emp-')) {
            $emp_id = ltrim($request->user_id, 'emp-');
            $clockInCount = Attendance::getTotalUserClockIn($date,$emp_id);
        }else{
            $clockInCount = Attendance::getTotalUserClockIn($date, $request->user_id);
        }*/

        if(Str::startsWith($request->user_id,'emp-')){
            $emp_id = ltrim($request->user_id,'emp-');

        }else{
            $user_id = $request->user_id;
        }
        if(!is_null($attendance)){
            $attendance->update([
                'user_id' => $user_id,
                'emp_id' => $emp_id,
                'clock_in_time' => $date.' '.$clockIn,
                'clock_in_ip' => $request->clock_in_ip,
                'clock_out_time' => $clockOut,
                'clock_out_ip' => $request->clock_out_ip,
                'working_from' => $request->working_from,
                'late' => $request->late,
                'half_day' => $request->half_day

            ]);
        }else{

             $attendance = new Attendance();
                if(Str::startsWith($request->user_id,'emp-')){
                    $emp_id = ltrim($request->user_id,'emp-');
                    $attendance->emp_id = $emp_id;
                }else{
                    $attendance->user_id = $request->user_id;
                }
                $attendance->clock_in_time = $date.' '.$clockIn;
                $attendance->clock_out_time = $clockOut;
                $attendance->working_from = $request->working_from;

                if ($request->file) {
                    if (!file_exists('user-uploads/attendance-image')) {
                        mkdir('user-uploads/attendance-image', 0777, true);
                    }
                    define('UPLOAD_DIR', 'user-uploads/attendance-image/');
                    $img = $request->file;
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $dataimage = base64_decode($img);
                    $file = UPLOAD_DIR . uniqid() . '.png';
                    $success = file_put_contents($file, $dataimage);
                    if($request->btnpressed == 'clockin'){
                        $attendance->clock_in_image = $file;
                    }
                    if($request->btnpressed == 'clockout'){
                        $attendance->clock_out_image = $file;
                    }
                }
                $attendance->save();

        }

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attendance = Attendance::find($id);

        $this->date = $attendance->clock_in_time->format('Y-m-d');
        $this->row =  $attendance;
        $this->clock_in = 1;
        $this->userid = $attendance->user_id;
        $this->total_clock_in  = Attendance::where('user_id', $attendance->user_id)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $this->date)
            ->whereNull('attendances.clock_out_time')->count();
        $this->type = 'edit';
        return view('admin.attendance.attendance_mark', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attendance = Attendance::findOrFail($request->id);
        $date = Carbon::createFromFormat($this->global->date_format, $request->attendance_date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            $clockOut->setTimezone('UTC');
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance->user_id = $request->user_id;
        $attendance->company_id = $this->user->company_id;
        $attendance->clock_in_time = $date . ' ' . $clockIn;
        $attendance->clock_in_ip = $request->clock_in_ip;
        $attendance->clock_out_time = $clockOut;
        $attendance->clock_out_ip = $request->clock_out_ip;
        $attendance->working_from = $request->working_from;
        $attendance->late = ($request->has('late'))? 'yes' : 'no';
        $attendance->half_day = ($request->has('half_day'))? 'yes' : 'no';
        if ($request->file) {
            if (!file_exists('user-uploads/attendance-image')) {
                mkdir('user-uploads/attendance-image', 0777, true);
            }
            define('UPLOAD_DIR', 'user-uploads/attendance-image/');
            $img = $request->file;
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $dataimage = base64_decode($img);
            $file = UPLOAD_DIR . uniqid() . '.png';
            $success = file_put_contents($file, $dataimage);
            if($request->btnpressed == 'clockin'){
                $attendance->clock_in_image = $file;
            }
            if($request->btnpressed == 'clockout'){
                $attendance->clock_out_image = $file;
            }
        }
        $attendance->save();

        return Reply::success(__('messages.attendanceSaveSuccess'));

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Attendance::destroy($id);
        return Reply::success(__('messages.attendanceDelete'));
    }

    public function data(Request $request){
        //dd($request->all());
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceByDate($date);
        if (!empty($request->user_id)){
            $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);
        }
        return DataTables::of($attendances)
            ->editColumn('id', function ($row) use($request){
                return view('admin.attendance.attendance_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();
    }

    public function singleData(Request $request){

        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        if(!empty($request)){}
        $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);
        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();
    }


    public function refreshCount($startDate = null, $endDate = null, $userId = null){

        $openDays = json_decode($this->attendanceSettings->office_open_days);
        $startDate = Carbon::createFromFormat('!Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('!Y-m-d', $endDate)->addDay(1); //addDay(1) is hack to include end date

        $totalWorkingDays = $startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $endDate);
        $daysPresent = Attendance::countDaysPresentByUser($startDate, $endDate, $userId);
        $daysLate = Attendance::countDaysLateByUser($startDate, $endDate, $userId);
        $halfDays = Attendance::countHalfDaysByUser($startDate, $endDate, $userId);
        $daysAbsent = (($totalWorkingDays - $daysPresent) < 0) ? '0' : ($totalWorkingDays - $daysPresent);
        $holidays = Count(Holiday::getHolidayByDates($startDate->format('Y-m-d'), $endDate->format('Y-m-d')));

        return Reply::dataOnly(['daysPresent' => $daysPresent, 'daysLate' => $daysLate, 'halfDays' => $halfDays, 'totalWorkingDays' => $totalWorkingDays, 'absentDays' => $daysAbsent, 'holidays' => $holidays]);

    }

    public function employeeData($startDate = null, $endDate = null, $userId = null){
        $ant = []; // Array For attendance Data indexed by similar date
        $dateWiseData = []; // Array For Combine Data

        $attendances = Attendance::userAttendanceByDate($startDate, $endDate, $userId);// Getting Attendance Data
        $holidays = Holiday::getHolidayByDates($startDate, $endDate); // Getting Holiday Data

        // Getting Leaves Data
        $leavesDates = Leave::where('user_id', $userId)
            ->where('leave_date', '>=', $startDate)
            ->where('leave_date', '<=', $endDate)
            ->where('status', 'approved')
            ->select('leave_date', 'reason')
            ->get()->keyBy('date')->toArray();

        $holidayData = $holidays->keyBy('holiday_date');
        $holidayArray = $holidayData->toArray();

        // Set Date as index for same date clock-ins
        foreach($attendances as $attand){
            $ant[$attand->clock_in_date][] = $attand; // Set attendance Data indexed by similar date
        }

        $endDate = Carbon::parse($endDate)->timezone($this->global->timezone);
        $startDate = Carbon::parse($startDate)->timezone($this->global->timezone)->subDay();

        // Set All Data in a single Array
        for($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay()){

            // Set default array for record
            $dateWiseData[$date->toDateString()] = [
                'holiday' =>false,
                'attendance' =>false,
                'leave' =>false
            ];

            // Set Holiday Data
            if(array_key_exists($date->toDateString(), $holidayArray)){
                $dateWiseData[$date->toDateString()]['holiday'] = $holidayData[$date->toDateString()];
            }

            // Set Attendance Data
            if(array_key_exists($date->toDateString(), $ant)){
                $dateWiseData[$date->toDateString()]['attendance'] = $ant[$date->toDateString()];
            }

            // Set Leave Data
            if(array_key_exists($date->toDateString(), $leavesDates)){
                $dateWiseData[$date->toDateString()]['leave'] = $leavesDates[$date->toDateString()];
            }
        }

        // Getting View data
        $view = view('admin.attendance.user_attendance',['dateWiseData' => $dateWiseData,'global' => $this->global])->render();

        return Reply::dataOnly(['status' => 'success', 'data' => $view]);
    }

    public function employeeDataByRange(Request $request){
        $user = $this->user;
        $userId = $request->user_id;
        $this->table = '';
        $date = Carbon::createFromFormat($this->global->date_format, $request->startDate)->format('Y-m-d');
        $startDate= Carbon::parse($request->startDate);
        $this->startDate = Carbon::parse($request->startDate);
        $this->endDate = Carbon::parse($request->endDate);
        $endDate = Carbon::parse($request->endDate);
        $this->different_day = $this->startDate->diffInDays($this->endDate);
        $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);// Getting Attendance Data

        if(!empty($request->user_id) && str_starts_with($request->user_id,'emp-')){
            $this->table = 'employee';
        }
        //dd($attendances);
        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_by_date_range_list', ['row' => $row,'startDate'=>$this->startDate,'endDate'=>$this->endDate,'different_day'=>$this->different_day, 'global' => $this->global, 'maxAttandenceInDay' => $this->maxAttandenceInDay,'table'=>$this->table])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();

    }

    public function attendanceByDate(){
        return view('admin.attendance.by_date', $this->data);
    }


    public function byDateData(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceDate($date)->get();

        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_date_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->make();
    }

    public function dateAttendanceCount(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $checkHoliday = Holiday::checkHolidayByDate($date);
        $totalPresent = 0;
        $totalAbsent  = 0;
        $holiday  = 0;
        $holidayReason  = '';
        $totalEmployees = count(User::allEmployees());

        if(!$checkHoliday){
            $totalPresent = Attendance::where(DB::raw('DATE(`clock_in_time`)'), '=', $date)->count();
            $totalAbsent = ($totalEmployees-$totalPresent);
        }
        else{
            $holiday = 1;
            $holidayReason = $checkHoliday->occassion;
        }

        return Reply::dataOnly(['status' => 'success', 'totalEmployees' => $totalEmployees, 'totalPresent' => $totalPresent, 'totalAbsent' => $totalAbsent, 'holiday' => $holiday, 'holidayReason' => $holidayReason]);
    }

    public function checkHoliday(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $checkHoliday = Holiday::checkHolidayByDate($date);
        return Reply::dataOnly(['status' => 'success', 'holiday' => $checkHoliday]);
    }

    // Attendance Detail Show
    public function attendanceDetail(Request $request){

        // Getting Attendance Data By User And Date
        $this->attendances =  Attendance::attedanceByUserAndDate($request->date, $request->userID);
        return view('admin.attendance.attendance-detail', $this->data)->render();
    }

    public function salarydata(Request $request)
    {
        // Getting Attendance setting data
        $users = User::all();

        $startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $endDate = Carbon::now()->timezone($this->global->timezone);

        foreach ($users as $user){
            $totalpresent = Attendance::countDaysPresentByUser($startDate,$endDate,$user->id);
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                $image = ($row->image) ? '<img src="' . asset('user-uploads/avatar/' . $row->image) . '"
                                                                alt="user" class="img-circle" width="30"> ' : '<img src="' . asset('default-profile-2.png') . '"
                                                                alt="user" class="img-circle" width="30"> ';

                $designation = ($row->designation_name) ? ucwords($row->designation_name) : ' ';

                return  '<div class="row"><div class="col-sm-3 col-xs-4">'.$image.'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->id) . '">'.ucwords($row->name).'</a><br><span class="text-muted font-12">'.$designation.'</span></div></div>';

            })
            ->addColumn('present', function ($row) use ($totalpresent) {

                return  $totalpresent;

            })
            ->addColumn('absent', function ($row)  {

                return  0;
            })
            ->addColumn('holidays', function ($row) {

                return  0;

            })->addColumn('actualdays', function ($row) {

                return  0;

            })->addColumn('actualhour', function ($row) {

                return  0;

            })
            ->addColumn('allowance', function ($row)  {

                return  '<div class="row"><div class="col-sm-12 col-xs-4"><button class="btn btn-info">Add Allowance</button></div></div>';

            })
            ->addColumn('overtime', function ($row) {

                return  0;

            })
            ->addColumn('diduction', function ($row) {

                return  0;

            })
            ->addColumn('totalamount', function ($row) {

                return  0;

            })
            ->addColumn('action', function ($row){
                return '
                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Salary Slip"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            })


            ->rawColumns(['name', 'present', 'absent', 'holidays','allowance','overtime','diduction','totalamount','action'])

            ->make(true);
    }

    public function export($startDate = null, $endDate = null, $employee = null) {
        //
    }

    public function summary()
    {
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $now = Carbon::now();
        $this->year = $now->format('Y');
        $this->month = $now->format('m');
        return view('admin.attendance.summary',$this->data);
    }

    public function summaryData(Request $request)
    {
        // dd($request->all());
        /*new employee part*/
        $companyid = $this->user->company_id;
        $newtableemployees = Employee::with(
            ['attendance' => function ($query) use ($request) {
                $query->whereRaw('MONTH(attendances.clock_in_time) = ?', [$request->month])
                    ->whereRaw('YEAR(attendances.clock_in_time) = ?', [$request->year]);
            }]
        )->select('employee.id', 'employee.name', 'employee.email', 'employee.created_at')
            ->where('company_id',$companyid)
            ->where('user_type','employee');

        if ($request->userId == '0') {
            $newtableemployees = $newtableemployees->get();
        } else {
            $newtableemployees = $newtableemployees->where('employee.id', $request->userId)->get();
        }
        // dd($newtableemployees);
        $this->holidays = Holiday::whereRaw('MONTH(holidays.date) = ?', [$request->month])->whereRaw('YEAR(holidays.date) = ?', [$request->year])->get();
        $newfinal = [];
        $this->daysInMonth = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        $now = Carbon::now()->timezone($this->global->timezone);
        $requestedDate = Carbon::parse(Carbon::parse('01-'.$request->month.'-'.$request->year))->endOfMonth();
        foreach ($newtableemployees as $employee) {
            $dataTillToday = array_fill(1, $now->copy()->format('d'), 'Absent');
            $dataFromTomorrow = [];
            if (($now->copy()->addDay()->format('d') != $this->daysInMonth) && !$requestedDate->isPast()) {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), '-');
            } else {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), 'Absent');
            }
            $newfinal[$employee->id . '#' . $employee->name] = array_replace($dataTillToday, $dataFromTomorrow);
            foreach ($employee->attendance as $attendance) {
                $newfinal[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = '<a href="javascript:;" class="view-attendance" data-attendance-id="'.$attendance->id.'"><i class="fa fa-check text-success"></i></a>';
            }
            $image = ($employee->image) ? '<img src="' . asset('user-uploads/avatar/' . $employee->image) . '"
                                                            alt="user" class="img-circle" width="30"> ' : '<img src="' .asset_url('user.png'). '"
                                                            alt="user" class="img-circle" width="30"> ';
            $newfinal[$employee->id . '#' . $employee->name][] = '<a class="userData" id="userID'.$employee->id.'" data-employee-id="'.$employee->id.'"  href="' . route('admin.employees.show', $employee->id) . '">' . $image . ' ' . ucwords($employee->name) . '</a>';
            foreach($this->holidays as $holiday) {
                $newfinal[$employee->id . '#' . $employee->name][$holiday->date->day] = 'Holiday';
            }
        }
        //dd($newfinal);
        $this->employeeAttendence = $newfinal;
        $view = view('admin.attendance.summary_data', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'data' => $view]);
    }

    public function detail($id)
    {
        $role = Role::where('name','=','admin')->where('company_id',$this->user->company_id)->first();
        $this->roleuser = RoleUser::where('role_id',$role->id)->where('user_id',$this->user->id)->first();
        $attendance = Attendance::find($id);
        $this->attendanceActivity = Attendance::userAttendanceByDate($attendance->clock_in_time->format('Y-m-d'), $attendance->clock_in_time->format('Y-m-d'), $attendance->user_id);
        $this->firstClockIn = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), $attendance->clock_in_time->format('Y-m-d'))
            ->where('emp_id', $attendance->emp_id)->orderBy('id', 'asc')->first();
        $this->lastClockOut = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), $attendance->clock_in_time->format('Y-m-d'))
            ->where('emp_id', $attendance->emp_id)->orderBy('id', 'desc')->first();

        $this->startTime = Carbon::parse($this->firstClockIn->clock_in_time)->timezone($this->global->timezone);

        if (!is_null($this->lastClockOut->clock_out_time)) {
            $this->endTime = Carbon::parse($this->lastClockOut->clock_out_time)->timezone($this->global->timezone);
        } elseif (($this->lastClockOut->clock_in_time->timezone($this->global->timezone)->format('Y-m-d') != Carbon::now()->timezone($this->global->timezone)->format('Y-m-d')) && is_null($this->lastClockOut->clock_out_time)) {
            $this->endTime = Carbon::parse($this->startTime->format('Y-m-d') . ' ' . $this->attendanceSettings->office_end_time, $this->global->timezone);
            $this->notClockedOut = true;
        } else {
            $this->notClockedOut = true;
            $this->endTime = Carbon::now()->timezone($this->global->timezone);
        }

        $this->totalTime = $this->endTime->diff($this->startTime, true)->format('%h.%i');
        $this->attendanceData = $attendance;

        return view('admin.attendance.attendance_info', $this->data);
    }

    public function mark(Request $request, $userid,$day,$month,$year)
    {
        $this->date = Carbon::createFromFormat('d-m-Y', $day.'-'.$month.'-'.$year)->format('Y-m-d');
        $this->row = Attendance::attendanceByUserDate($userid, $this->date);
        $this->clock_in = 0;
        $this->total_clock_in = Attendance::where('user_id', $userid)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $this->date)
            ->whereNull('attendances.clock_out_time')->count();

        $this->userid = $userid;
        $this->type = 'add';
        return view('admin.attendance.attendance_mark', $this->data);
    }

    public function storeMark(StoreAttendance $request)
    {

        // $date = Carbon::createFromFormat($this->global->date_format,$request->attendance_date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        //$clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            //$clockOut->setTimezone('UTC');
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $request->attendance_date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance = Attendance::where('emp_id', $request->user_id)
            ->where(DB::raw('DATE(`clock_in_time`)'), "$request->attendance_date")
            ->whereNull('clock_out_time')
            ->first();

        //$clockInCount = Attendance::getTotalUserClockIn($request->attendance_date, $request->user_id);

        if (!is_null($attendance)) {
            $attendance->update([
                'emp_id' => $request->user_id,
                'clock_in_time' => $request->attendance_date . ' ' . $clockIn,
                'clock_in_ip' => '',
                'clock_out_time' => $clockOut,
                'clock_out_ip' => '',
                'working_from' => 'Office',

            ]);
        } else {
            // Check maximum attendance in a day
           /* if ($clockInCount < $this->attendanceSettings->clockin_in_day) {*/
                Attendance::create([
                    'emp_id' => $request->user_id,
                    'clock_in_time' => $request->attendance_date . ' ' . $clockIn,
                    'clock_in_ip' => '',
                    'clock_out_time' => $clockOut,
                    'clock_out_ip' =>   '',
                    'working_from' => 'Office',

                ]);
           /* } else {
                return Reply::error(__('messages.maxColckIn'));
            }*/
        }

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    public function byCamera()
    {
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.attendance.by_punching_machine', $this->data);
    }

    public function singleAttendance(Request $request){
        $time=strtotime('Y-m-d',$request->date);
        $month=date("m",$time);
        $year=date("Y",$time);
        $attendance =  Employee::with(
            ['attendance' => function ($query) use ($month,$year) {
                $query->whereRaw('MONTH(attendances.clock_in_time) = ?', [$month])
                    ->whereRaw('YEAR(attendances.clock_in_time) = ?', [$year]);
            }]
        )->where('company_id',$this->companyid)
            ->where('user_type','employee')
            ->where('user_id',$this->user->id)->first();
        return view('admin.attendance.single_attendance', $this->data);
    }

    public function singleAttendanceDateRange(){
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::now()->timezone($this->global->timezone);
        /*$this->employees = User::allEmployees();*/
        $this->empdata = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.attendance.create_attandancee_by_date_range', $this->data);
    }

    public function rules(){

        $this->employees = User::all();
        $this->empdata = Employee::where('company_id',$this->companyid)->get();
        $this->skills = Skill::all();
        $this->departments = Team::all();
        $this->designations = Designation::all();
        $this->rules = AttendanceRules::where('company_id',$this->companyid)->where('attendance_rules.name', '<>', 'client')->get();
        return view('admin.attendance.rules',$this->data);
    }
    public function storeGeneralRules(GeneralRulesRequest $request){
        $user = $this->user;
        if(!empty($request->shifttime)){
            $date =  Carbon::today()->timezone($this->global->timezone)->format('Y-m-d');
            $shift_in_time = Carbon::createFromFormat($this->global->time_format, $request->shift_in_time, $this->global->timezone);
            $shift_in_time = $shift_in_time->format('H:i:s');
            if($request->shift_out_time != ''){
                $shift_out_time = Carbon::createFromFormat($this->global->time_format, $request->shift_out_time, $this->global->timezone);
                $shift_out_time = $shift_out_time->format('H:i:s');
                $shift_out_time = $date.' '.$shift_out_time;
            }
            else{
                $shift_out_time = null;
            }
        }
        $attendancerules = new AttendanceRules();
        $attendancerules->company_id  = $user->company_id;
        $attendancerules->name  = $request->rulename;
        $attendancerules->description = $request->description;
        $attendancerules->shift_in_time = '0';
        $attendancerules->shift_out_time = '0';
        if(!empty($request->shifttime)){
            $attendancerules->shift_in_time = $date.' '.$shift_in_time;
            $attendancerules->shift_out_time = $shift_out_time;
        }
        $attendancerules->anomaly_grace_in = $request->anomaly_in_time ?: '';
        $attendancerules->anomaly_grace_out = $request->anomaly_out_time ?: '';
        $attendancerules->work_full_time = $request->full_day;
        $attendancerules->work_half_time = $request->half_day;
        $attendancerules->company_id = $this->companyid;
        $attendancerules->auto_clock_out = $request->auto_clock_out?$request->auto_clock_out:'';
        $attendancerules->save();
        session(['rules_id' => $attendancerules->id]);
        return Reply::success(__('messages.attendanceSaveSuccess'));
    }
    public function updateGeneralRules(GeneralRulesRequest $request,$id){
        $user = $this->user;
        if(!empty($request->shifttime)){
            $date =  Carbon::today()->timezone($this->global->timezone)->format('Y-m-d');
            $shift_in_time = Carbon::createFromFormat($this->global->time_format, $request->shift_in_time, $this->global->timezone);
            $shift_in_time = $shift_in_time->format('H:i:s');
            if($request->shift_out_time != ''){
                $shift_out_time = Carbon::createFromFormat($this->global->time_format, $request->shift_out_time, $this->global->timezone);
                $shift_out_time = $shift_out_time->format('H:i:s');
                $shift_out_time = $date.' '.$shift_out_time;
            }
            else{
                $shift_out_time = null;
            }
        }
        $attendancerules = AttendanceRules::find($id);
        $attendancerules->company_id  = $user->company_id;
        $attendancerules->name  = $request->rulename;
        $attendancerules->description = $request->description;
        $attendancerules->shift_in_time = '0';
        $attendancerules->shift_out_time = '0';
        if(!empty($request->shifttime)){
            $attendancerules->shift_in_time = $date.' '.$shift_in_time;
            $attendancerules->shift_out_time = $shift_out_time;
        }
        $attendancerules->anomaly_grace_in = $request->anomaly_in_time ?: '';
        $attendancerules->anomaly_grace_out = $request->anomaly_out_time ?: '';
        $attendancerules->work_full_time = $request->full_day;
        $attendancerules->work_half_time = $request->half_day;
        $attendancerules->company_id = $this->companyid;
        $attendancerules->auto_clock_out = $request->auto_clock_out?$request->auto_clock_out:'';
        $attendancerules->save();
        session(['rules_id' => $attendancerules->id]);
        return Reply::success(__('messages.attendanceRuleUpdate'));
    }

    public function storeAdvanceRules(Request $request){
        $user = $this->user;
        $advancerules = new AdvanceRules();
        $advancerules->company_id  = $user->company_id;
        $advancerules->enable_overtime  = $request->overtime?$request->overtime:false;
        $advancerules->rules_id  = session()->get('rules_id');
        $advancerules->in_time_leave_allow  = $request->yearlyleave;
        $advancerules->in_time_penalty = $request->penaly;
        $advancerules->in_time_deduction = $request->leavededuction;
        $advancerules->out_time_leave_allow = $request->outyearlyleave;
        $advancerules->out_time_penalty = $request->outpenaly;
        $advancerules->out_time_deduction = $request->outleavededuction;
        $advancerules->work_time_leave_allow = $request->workyearlyleave;
        $advancerules->work_time_penalty = $request->workpenalty;
        $advancerules->work_time_deduction = $request->workleavededuction;

        $advancerules->save();
        return Reply::success(__('messages.attendanceSaveSuccess'));

    }
    public function updateAdvanceRules(Request $request,$id){
        $user = $this->user;
        $advancerules = AdvanceRules::find($id);
        if(!empty($advancerules)){
            $advancerules->company_id  = $user->company_id;
            $advancerules->enable_overtime  = !empty($request->overtime)? $request->overtime : '';
            $advancerules->rules_id  = $id;
            $advancerules->in_time_leave_allow  = $request->yearlyleave;
            $advancerules->in_time_penalty = $request->penaly;
            $advancerules->in_time_deduction = $request->leavededuction;
            $advancerules->out_time_leave_allow = $request->outyearlyleave;
            $advancerules->out_time_penalty = $request->outpenaly;
            $advancerules->out_time_deduction = $request->outleavededuction;
            $advancerules->work_time_leave_allow = $request->workyearlyleave;
            $advancerules->work_time_penalty = $request->workpenalty;
            $advancerules->work_time_deduction = $request->workleavededuction;
            $advancerules->save();
            return Reply::success(__('messages.attendanceAdRuleUpdate'));
        }else{
            $adrules = new AdvanceRules();
            $adrules->company_id  = $user->company_id;
            $adrules->enable_overtime  = !empty($request->overtime)? $request->overtime : '';
            $adrules->rules_id  = $id;
            $adrules->in_time_leave_allow  = $request->yearlyleave;
            $adrules->in_time_penalty = $request->penaly;
            $adrules->in_time_deduction = $request->leavededuction;
            $adrules->out_time_leave_allow = $request->outyearlyleave;
            $adrules->out_time_penalty = $request->outpenaly;
            $adrules->out_time_deduction = $request->outleavededuction;
            $adrules->work_time_leave_allow = $request->workyearlyleave;
            $adrules->work_time_penalty = $request->workpenalty;
            $adrules->work_time_deduction = $request->workleavededuction;
            $adrules->save();
            return Reply::success(__('messages.attendanceAdRuleSave'));
        }

    }

    public function  rulesData(Request $request){
        $rules = AttendanceRules::where('company_id',$this->companyid)->get();
        return DataTables::of($rules)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->name;
            })
            ->editColumn('description', function ($row)  {
                return $row->description;
            })
            ->editColumn('shift_in_time', function ($row)  {

                return $row->shift_in_time;
            }) ->editColumn('shift_out_time', function ($row)  {

                return $row->shift_out_time;
            }) ->editColumn('anomaly_grace_in_time', function ($row)  {
                return $row->anomaly_grace_in;

            }) ->editColumn('anomaly_grace_out_time', function ($row)  {
                return $row->anomaly_grace_out;

            }) ->editColumn('work_full_time', function ($row)  {
                return $row->work_full_time;

            })
            ->editColumn('work_half_time', function ($row)  {

                return $row->work_half_time;
            })
            ->addColumn('action', function ($row) {
                return '<a   href="' . route('admin.attendances.editrules', [$row->id]) . '" class="btn btn-info btn-circle" 
                      data-toggle="tooltip" data-original-title="Edit Rules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'description','shift_in_time','shift_out_time','anomaly_grace_in_time','anomaly_grace_out_time', 'work_full_time', 'work_half_time','action'])

            ->make(true);
    }

    public function attendaceRulesData(){

        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->select('employee.id','employee.name','employee_details.address','employee_details.department_id as deptid')
            ->where('employee.company_id',$this->companyid)->where('user_type','employee')
            ->groupBy('employee.id')->get();
        $wordata = Employee::join('rules_users','rules_users.user_id','=','employee.id')
            ->join('attendance_rules','attendance_rules.id','=','rules_users.rules_id')->select('rules_users.user_id','attendance_rules.name','rules_users.effective_date')->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {

                return '<input type="checkbox" class="sub_chk" data-id="'.$row->id.'" />';
            })->rawColumns(['checkbox'])
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })->editColumn('department', function ($row)  {
                $team = Team::where('id',$row->deptid)->first();
                if(!empty($team)){
                    return $team->team_name;
                }
                return 'NA';
            })->editColumn('location', function ($row)  {

                return $row->address;

            })->editColumn('effective_date', function ($row) use($wordata)  {
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->effective_date;
                    }

                }

            })

            ->addColumn('rulesapplied', function ($row) use($wordata){
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->name;
                    }

                }

            })
            ->rawColumns(['action','name','department','location','rulesapplied'])

            ->make(true);

    }
    public function createRules(){
        return view('admin.attendance.create_new_rules',$this->data);
    }

    public function editRules($id){
        $this->rules  =  AttendanceRules::find($id);
        $this->adrules  =  AdvanceRules::where('rules_id',$id)->first();
        return view('admin.attendance.edit_rules',$this->data);
    }
    public function deleteRules($id){
        $rules = AttendanceRules::find($id);
        $rules->delete();

    }
    // Attendance Detail Show
    public function assignRules(Request $request){
        $this->ids = $request->ids;
        $this->rules = AttendanceRules::where('company_id',$this->companyid)->get();
        return view('admin.attendance.assign_rules', $this->data)->render();
    }

    public function postAssignRules(Request $request){
        $date = Carbon::parse($request->effective_date)->format('Y-m-d');
        $emp_ids = explode(',', $request->ids);
        foreach ($emp_ids as $key=>$emp_id){
            $ruleuser = RulesUser::where('user_id',$emp_id)->first();
            if(!empty($ruleuser)){
                $ruleuser->user_id = $emp_id;
                $ruleuser->rules_id = $request->rules_id;
                $ruleuser->effective_date = !empty($date)?$date:'';
                $ruleuser->save();
            }else{
                $newruleuser = new RulesUser();
                $newruleuser->user_id = $emp_id;
                $newruleuser->rules_id = $request->rules_id;
                $newruleuser->effective_date = !empty($date)?$date:'';
                $newruleuser->save();
            }
        }
        return Reply::success(__('messages.attendanceSaveSuccess'));
        abort(403);
    }


    public function getData(Request $request){
        $value= array();
        $id = $request->id;
        $date = $request->date;
        $stadate  = Carbon::parse($date)->format('Y-m-d');
        $startdate = $stadate.' 00:00:01';
        $enddate = $stadate.' 23:59:59';
        if(str_starts_with($id,'emp-')){
            $empid  = ltrim($id,'emp-');
            $att = Attendance::where('created_at','>=',$startdate)->where('created_at','<=',$enddate)->where('emp_id',$empid)->first();
        }else{
            $att = Attendance::where('created_at','>=',$startdate)->where('created_at','<=',$enddate)->where('user_id',$id)->first();
        }
        if($att !== null){
            $value['id'] = $att->id;
            $value['clock_in'] = Carbon::parse($att->clock_in_time)->format('h:i A');
            return $value;
        }else {
            return '';
        }
    }

    public function updateAttendance(Request $request,$id){

        $attendance = Attendance::findOrFail($id);
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');

        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance->user_id = $request->user_id;
        $attendance->clock_in_time = $date . ' ' . $clockIn;
        $attendance->clock_in_ip = '::1';
        $attendance->clock_out_time = $clockOut;
        $attendance->clock_out_ip = '::1';
        $attendance->working_from = 'Office';
        /*$attendance->late = ($request->has('late'))? 'yes' : 'no';
        $attendance->half_day = ($request->has('half_day'))? 'yes' : 'no';*/
        if ($request->file) {
            if (!file_exists('user-uploads/attendance-image')) {
                mkdir('user-uploads/attendance-image', 0777, true);
            }
            define('UPLOAD_DIR', 'user-uploads/attendance-image/');
            $img = $request->file;
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $dataimage = base64_decode($img);
            $file = UPLOAD_DIR . uniqid() . '.png';
            $success = file_put_contents($file, $dataimage);
            if($request->btnpressed == 'clockin'){
                $attendance->clock_in_image = $file;
            }
            if($request->btnpressed == 'clockout'){
                $attendance->clock_out_image = $file;
            }
        }
        $attendance->save();

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

}
