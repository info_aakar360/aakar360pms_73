<?php

namespace App\Http\Controllers\Admin;


use App\Helper\Reply;
use App\IncomeExpenseHead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class JournalVoucherController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Journal Voucher';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = Transaction::class;
    public $parentRoute = 'admin.jnl_voucher';
    public $parentView = "admin.jnl-voucher";
    public $voucher_type = "JV";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->items = Transaction::where('voucher_type', $this->voucher_type)
            ->orderBy('voucher_no', 'desc')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $ledgerdetails = IncomeExpenseHead::where('company_id',$user->company_id)->get();
        $this->ledgerdetails = $ledgerdetails;
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'branch_id' => 'required',
            'branch_id_cr' => 'required',
            'voucher_date' => 'required',
        ]);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $voucher_info = Transaction::where('voucher_type', $this->voucher_type)
            ->withTrashed()
            ->orderBy('voucher_no', 'desc')
            ->get()
            ->first();
        if (!empty($voucher_info)) {
            $voucher_no = $voucher_info->voucher_no + 1;
        } else {
            $voucher_no = 1;
        }
        foreach ($request->income_expense_head_id as $key => $id) {
            if ($id == 0) {
                continue;
            }
            $ts = Transaction::create([
                'voucher_no' => $voucher_no,
                'branch_id' => $request->branch_id,
                'voucher_type' => $this->voucher_type,
                'date' => $voucher_date,
                'particulars' => $request->particulars,
                'income_expense_head_id' => $id,
                'dr' => $request->amount[$key],
                'created_by' => \Auth::user()->id,
            ]);
        }
        foreach ($request->income_expense_head_id_cr as $key => $id) {
            if ($id == 0) {
                continue;
            }
            Transaction::create([
                'voucher_no' => $voucher_no,
                'branch_id' => $request->branch_id_cr,
                'voucher_type' => $this->voucher_type,
                'date' => $voucher_date,
                'particulars' => $request->particulars,
                'income_expense_head_id' => $id,
                'cr' => $request->amount_cr[$key],
                'created_by' => \Auth::user()->id,
            ]);
        }
        return Reply::success(__('Successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        $item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($item)==0) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if ( count($this->items) <= 0) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        $DrItems = array();
        $CrItems = array();
        foreach ($this->items as $item) {
            if ($item->dr > 0) {
                $DrItems[] = $item;
            } else {
                $CrItems[] = $item;
            }
        }
        $this->items->dr = $DrItems;
        $this->items->cr = $CrItems;
//        $this->DrCrItems = array('dr' => $DrItems, 'cr' => $CrItems);
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'branch_id' => 'required',
            'branch_id_cr' => 'required',
            'voucher_date' => 'required',
        ]);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'

        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        $created_by=$items[0]->created_by;
        $created_at=$items[0]->created_at;
        $this->kill($id); /// Old Item kill then new items created
        foreach ($request->income_expense_head_id as $key => $income_expense_head_id) {
            if ($income_expense_head_id == 0) {
                continue;
            }
            $d_model=Transaction::create([
                'voucher_no' => $id,
                'branch_id' => $request->branch_id,
                'voucher_type' => $this->voucher_type,
                'date' => $voucher_date,
                'particulars' => $request->particulars,
                'income_expense_head_id' => $income_expense_head_id,
                'dr' => $request->amount[$key],
                'created_by' => $created_by,
                'updated_by' => \Auth::user()->id,
            ]);
            $d_model->created_at=$created_at;
            $d_model->save();
        }
        foreach ($request->income_expense_head_id_cr as $key => $income_expense_head_id_cr) {
            if ($income_expense_head_id_cr == 0) {
                continue;
            }
            $c_model=Transaction::create([
                'voucher_no' => $id,
                'branch_id' => $request->branch_id_cr,
                'voucher_type' => $this->voucher_type,
                'date' => $voucher_date,
                'particulars' => $request->particulars,
                'income_expense_head_id' => $income_expense_head_id_cr,
                'cr' => $request->amount_cr[$key],
                'created_by' => $created_by,
                'updated_by' => \Auth::user()->id,
            ]);
            $c_model->created_at=$created_at;
            $c_model->save();
        }
        return Reply::success(__('Successfully Updated'));
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        $item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($item) == 0) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format') . ' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Journal Voucher Report',
            'voucher_type' => 'JOURNAL VOUCHER'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
         return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($items) < 1) {
            return Reply::error(__('Item Not Found'));
        }
        foreach ($items as $item) {
            $item->deleted_by = \Auth::user()->id;
            $item->save();
        }
        foreach ($items as $item) {
            $item->delete();
        }
        return Reply::success(__('Successfully Trashed'));
    }

    public function trashed()
    {
        $this->items = Transaction::where('voucher_type', $this->voucher_type)
            ->onlyTrashed()
            ->orderBy('deleted_at', 'desc')
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

    public function restore($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->onlyTrashed()
            ->get();
        foreach ($items as $item) {
            $item->restore();
            $item->updated_by = \Auth::user()->id;
            $item->save();
        }
        return Reply::success(__('Successfully Restored'));
    }

    public function kill($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->withTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->withTrashed()
            ->get();
        foreach ($items as $item) {
            $item->forceDelete();
        }
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('voucher_date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->onlyTrashed()
            ->orderBy('created_at', 'desc')
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->withTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->withTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
}
