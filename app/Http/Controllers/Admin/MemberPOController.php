<?php

namespace App\Http\Controllers\Admin;

use App\ClientDetails;
use App\Company;
use App\Helper\Reply;
use App\Http\Requests\Admin\Rfq\StoreRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Indent;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PoProducts;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\ProductReturns;
use App\Project;
use App\PurchaseInvoice;
use App\PurchaseOrder;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\QuoteProducts;
use App\Quotes;
use App\Role;
use App\Rfq;
use App\RfqProducts;
use App\Store;
use App\Supplier;
use App\TmpRfq;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberPOController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.po';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';

        $this->middleware(function ($request, $next) {
            if (!in_array('po', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$company_id = Auth::user()->company_id;
        $this->pos = PurchaseOrder::all();
        $this->stores = Store::all();
        $this->categories = ProductCategory::all();
        $this->projects = Project::all();

        $this->pos = count($this->pos);

        return view('admin.po.index', $this->data);
    }

    public function showAll($id)
    {
        //$company_id = Auth::user()->company_id;
        $supIds = Quotes::where('rfq_id', $id)->pluck('supplier_id');
        $this->suppliers = Supplier::whereIn('id', $supIds)->get();
        $this->products = RfqProducts::where('rfq_id', $id)->get();
        $this->rfq = Rfq::find($id);
        return view('admin.quotes.show-all', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        return view('admin.rfq.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $bids = ProductCategory::where('id', $pid)->first()->brands;
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = '<option value="">Select Brand</option>';
        foreach($brands as $brand){
            $html .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        }
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRfqRequest $request)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq = new Rfq();
        $rfq->rfq_no = $rfq_no;
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->save();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('rfqTmpDataSession', $sid);
        }
        $all_data = RfqProducts::where('rfq_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpRfq();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpRfq::where('session_id', $sid)->get();
        $this->rfq = Rfq::where('id', $id)->first();
        return view('admin.rfq.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRfqRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $rfq = Rfq::find($id);
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        RfqProducts::where('rfq_id', $id)->delete();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        Rfq::destroy($id);
        RfqProducts::where('rfq_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.rfqDeleted'));
    }
    public function delete($id)
    {

        $projectde  = PurchaseInvoice::where('po_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove GRN first.';
        }
        $projectde  = ProductReturns::where('invoice_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Product Return first.';
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            PurchaseOrder::where('id', $id)->delete();
            return Reply::success(__('messages.rfqDeleted'));
        }
    }

    public function approve($id, $val)
    {
        DB::beginTransaction();
        $ind = PurchaseOrder::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('PO updated successfully');
    }

    public function data(Request $request)
    {

        $pos = PurchaseOrder::join('suppliers', 'suppliers.id', '=', 'purchase_order.supplier_id');
        if ($request->store != 'all' && $request->store != '') {
            $pos = $pos->where('purchase_order.store_id', $request->store);
        }
        if ($request->status != 'all' && $request->status != '') {
            $pos = $pos->where('purchase_order.status', $request->status);
        }
        if ($request->project_id != 'all' && $request->project_id != '') {
            $pos = $pos->where('purchase_order.project_id', $request->project_id);
        }
        $rfqs = $pos->select(['purchase_order.*', 'suppliers.company_name', 'suppliers.id as link'])->get();

        $user = Auth::user();
        return DataTables::of($rfqs)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '';
                $getstore = Rfq::where('id',$row->rfq_id)->first();
                $completegrn = getCompleteGrn($row->indent_id,$row->project_id,$getstore->store_id,$row->company_id);
                if(count(array_unique($completegrn)) === 1 && end($completegrn) === 'done'){
                    $done = 'done';
                }else {
                    $done = 'pending';
                }
                $pretrun = \App\ProductLog::where('project_id',$row->project_id)->where('store_id',$getstore->store_id)->where('company_id',$row->company_id)->where('indent_id',$row->indent_id)->where('module_name','=','product_return')->sum('quantity');
                if($done === 'pending' && !empty($pretrun)){
                    $indent = Indent::find($row->id);
                    $indent->status = 2;
                    $indent->save();
                }
                $ret .= '<a href="'.route('admin.purchase-order.viewPO', [$row->id]).'" class="btn btn-primary btn-circle" data-toggle="tooltip" data-original-title="View Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                if($done !== 'done' && $row->status !== 3 && $row->approve == 1){
                    $ret .= '<a href="' . route('admin.inventory.add', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Create GRN"><i class="fa fa-plus" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }
                $ret .= '<a  class="btn btn-danger btn-circle sa-params" data-user-id="'.$row->id.'"
                      data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'rfq_id',
                function ($row) {
                    return get_rfq_name($row->rfq_id);
                }
            )
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name_by_rfq($row->rfq_id);
                }
            )
            ->addColumn('link', function($row){
                if($row->approve) {
                    return '<a href="javascript:void(0);" onClick="copyLink(\'id' . $row->id . '\')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>&nbsp;<a href="https://api.whatsapp.com/send?text='.urlencode(route('front.viewPO', [$row->id, $row->supplier_id])).'" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>&nbsp;<a href="javascript:;" class="btn btn-primary btn-circle sendEmail" data-id="'.$row->id.'" data-supplier-id="'.$row->supplier_id.'" data-toggle="tooltip" data-original-title="Send Email"><i class="fa fa-envelope" aria-hidden="true"></i></a><br><span id="id' . $row->id . '">' . route('front.viewPO', [$row->id, $row->supplier_id]) . '</span>';
                }
                return 'Link Not Generated';
            })
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending Purchase';
                    }else if($row->status == 1){
                        return 'Products Received';
                    }
                }
            )
            ->editColumn(
                'approve',
                function ($row) {
                    if($row->approve == 0){
                        return 'Pending Approval';
                    }else if($row->approve == 1){
                        return 'Approved';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['link','action', 'status'])
            ->make(true);
    }



    public function export($status, $rfq)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'rfq')
            ->where('roles.company_id', company()->id)
            ->leftJoin('rfq_details', 'users.id', '=', 'rfq_details.user_id')
            ->select(
                'users.id',
                'rfq_details.name',
                'rfq_details.email',
                'rfq_details.mobile',
                'rfq_details.company_name',
                'rfq_details.address',
                'rfq_details.website',
                'rfq_details.created_at'
            )
            ->where('rfq_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($rfq != 'all' && $rfq != '') {
            $rows = $rows->where('users.id', $rfq);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('rfqs', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('rfqs file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function linkData(Request $request, $poId)
    {
        $suppliers = Supplier::all();
        $user = Auth::user();
        return DataTables::of($suppliers)
            ->addColumn('action', function ($row) use ($user, $poId){
                $ret = '<a href="javascript:;" onClick="copyLink(\'id'.$row->id.'\')" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>&nbsp;';

                return $ret;
            })
            ->addColumn('link', function($row) use ($poId){
                return '<span id="id'.$row->id.'">'.route('front.viewPO', [$poId, $row->id]).'</span>';
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'link'])
            ->make(true);
    }

    public function postPO(Request $request){
        $sr = 1;
        $pox = PurchaseOrder::orderBy('id', 'DESC')->first();
        if($pox !== null){
            $in = explode('/', $pox->po_number);
            $sr = $in[2];
            $sr++;
        }
        $po_no = 'PO/'.date("Y").'/'.$sr;

        $po = new PurchaseOrder();
        $po->po_number = $po_no;
        $po->quote_id = $request->quote;
        $po->rfq_id = $request->rfq;
        $po->supplier_id = $request->supplier;
        $po->remark = $request->remark;
        $po->payment_terms = $request->payment_terms;
        $po->dated = $request->dated;
        $po->company_id = $this->user->company_id;
        $po->save();

        foreach($request->cid as $key=>$cid){
            $popro = new PoProducts();
            $popro->po_id = $po->id;
            $popro->cid = $cid;
            $popro->bid = $request->bid[$key];
            $popro->quantity = $request->quantity[$key];
            $popro->unit = $request->unit[$key];
            $popro->price = $request->price[$key];
            $popro->amount = $request->amount[$key];
            $popro->save();
        }

        $rfq = Rfq::find($request->rfq);
        if($rfq !== null) {
            $rfq->status =  1;
            $rfq->save();
        }

        $ind = Indent::where('indent_no', $rfq->indent_no)->first();
        if($ind !== null){
            $ind->status = 2;
            $ind->save();
        }

        $quote = Quotes::find($request->quote);
        if($quote !== null){
            $quote->status = 1;
            $quote->save();
        }
        return Reply::redirect(route('admin.rfq.index'));
    }

    public function generatePO(Request $request){
        $this->pageTitle = 'Submit Quotation';
        $this->pageIcon = 'icon-people';
        $this->supplier = Supplier::find($request->supplier_id);
        $this->tmpData = QuoteProducts::whereIn('id', explode(',', $request->qpid))->get();
        $quote_id = QuoteProducts::whereIn('id', explode(',', $request->qpid))->first();
        $quote = Quotes::find($quote_id->quote_id);
        $this->rfq = Rfq::find($quote->rfq_id);
        $this->quote = $quote;
        return view('admin.quotes.generate-po', $this->data);
    }
    public function poLinks($id){
        $this->po = PurchaseOrder::find($id);
        return view('admin.po.links', $this->data);
    }

    public function viewPO($poId, $supplierId = 0){
        $this->pageTitle = 'View Purchase Order';
        $this->pageIcon = 'icon-people';
        $all_data = PoProducts::where('po_id',$poId)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseOrder::where('id', $poId)->first();
        $this->supplier = false;
        if(!empty($supplierId)) {
            $this->supplier = Supplier::find($supplierId);
        }
        $rfq = Rfq::find($this->po->rfq_id);
        $this->store = Store::find($rfq->store_id);
        $this->submitted = false;
        return view('admin.po.view-po', $this->data);
    }

    public function printPO($poId, $supplierId = 0){
        $this->pageTitle = 'View Purchase Order';
        $this->pageIcon = 'icon-people';
        $all_data = PoProducts::where('po_id', $poId)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseOrder::where('id', $poId)->first();
        $this->supplier = false;
        if($supplierId) {
            $this->supplier = Supplier::find($supplierId);
        }
        $rfq = Rfq::find($this->po->rfq_id);
        $this->store = Store::find($rfq->store_id);
        $this->company = Company::find($this->user->company_id);
        $this->submitted = false;
        return view('admin.po.print-po', $this->data);
    }

    public function sendEmail(Request $request, $id, $poid){
        $user = \App\User::where('id', Auth::user()->id)->first();
        $po = PurchaseOrder::where('id',$id)->first();
        $qty = Supplier::where('id', $po->supplier_id)->first();
        if(isset($qty->id)){
            $mailarray = array();
            $mailarray['email'] = $qty->email;
            $mailarray['subject'] = 'Link for submit quotation';
            $mailarray['message'] = route('front.viewPO', [$id, $poid]);
            $user->sendEmail($mailarray);
        }
        return Reply::success('Email sent successfully');
    }

    public function sendRFQEmail(Request $request, $id, $poid, $rfqid){
        $user = \App\User::where('id', Auth::user()->id)->first();
        $po = Rfq::where('id',$id)->first();
        $qty = Supplier::where('id', $po->supplier_id)->first();
        if(isset($qty->id)){
            $mailarray = array();
            $mailarray['email'] = $qty->email;
            $mailarray['subject'] = 'Link for submit quotation';
            $mailarray['message'] = route('front.submitQuotation', [$rfqid, $id]);
            $user->sendEmail($mailarray);
        }
        return Reply::success('Email sent successfully');
    }
}
