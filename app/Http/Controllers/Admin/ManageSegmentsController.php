<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Contractors;
use App\CostItems;
use App\Employee;
use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\SubAsset;
use App\Segment;
use App\Task;
use App\Title;
use App\Type;
use App\Units;
use App\User;
use View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManageSegmentsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Segments';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    } 
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->user = $user;
        $this->projectarray = Project::whereIn('id',$projectlist)->get();
        $this->segments = Segment::whereIn('projectid',$projectlist)->get();
        return view('admin.segments.index', $this->data);
    }


    public function store(Request $request)
    {
        $user = $this->user;
        $segment = new Segment();
        $segment->company_id = $user->company_id;
        $segment->name = $request->name;
        $segment->projectid = $request->projectid;
        $segment->titleid = $request->subprojectid ?: 0;
        $segment->save();
        return Reply::success(__('messages.segmentAdded'));
    }

    public function edit($id)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $segment =  Segment::find($id);
        $this->segment = $segment;
        $this->projectarray = Project::whereIn('id',$projectlist)->get();
        $this->titlesarray = !empty($segment->projectid) ? Title::where('project_id',$segment->projectid)->get() : array();
        return view('admin.segments.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $segment = Segment::find($id);
        $segment->name = $request->name;
        $segment->projectid = $request->projectid;
        $segment->titleid = $request->subprojectid ?: 0;
        $segment->save();

        return Reply::success(__('messages.segmentUpdate'));
    }
    public function destroy($id)
    {
        Segment::destroy($id);
        $categoryData = Segment::all();
        return Reply::successWithData(__('messages.segmentDelete'),['data' => $categoryData]);
    }
    public function Segments(Request $request, $id){

        $user = $this->user;
        $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
        $pm = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
        $prarray = array_filter(array_merge($project,$pm));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);
            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.segments', $this->data);
    }
    public function segmentsTitle(Request $request, $id,$title=false,$segment=false){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $title = $title ?: 0;
        $user = $this->user;
        $segmentsarray = Segment::where('projectid',$id)->where('titleid',$title)->get();
        $this->segmentsarray = $segmentsarray;
        $this->userarray =  Employee::getAllEmployees($user);
        $this->title = (int)$title;
        if(empty($segment)){
            $segment = !empty($segmentsarray[0]) ?  $segmentsarray[0]->id : '';
        }
        $this->segment = (int)$segment;
        $this->id = (int)$id;
        $columsarray = array(
            'task'=>'Task',
            'description'=>'Description',
            'assign_to'=>'Assign to',
            'contractor'=>'Contractor',
            'startdate'=>'Start date',
            'enddate'=>'End date',
            'totalqty'=>'BOQ Qty',
            'qty'=>'Segment Qty',
            'rate'=>'Rate',
            'unit'=>'Unit',
            'finalrate'=>'Final rate',
            'totalamount'=>'Total amount'
        );
        $col =1; foreach($columsarray as $colkey => $colums){
            $postitionarray = ProjectSegmentsPosition::where('project_id',$id)->where('title',$title)->where('segment',$segment)->where('itemslug',$colkey)->where('position','col')->first();
            if(empty($postitionarray->id)){
                $columsarray = new ProjectSegmentsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->segment = $segment;
                $columsarray->position = 'col';
                $columsarray->itemid = 0;
                $columsarray->itemname = $colums;
                $columsarray->itemslug = $colkey;
                $columsarray->collock = 0;
                $columsarray->level = 0;
                $columsarray->catlevel = '';
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->typesarray = Type::where('company_id',$user->company_id)->get();
        $this->contractorarray =  Employee::getAllContractors($user);
        return view('admin.projects.segmentstitlenew', $this->data);
    }
    public function addBoqCostItemRow(Request $request){

        if(empty($request->segmentid)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $user = $this->user;
        $subprojectid = $request->subprojectid ?: 0;
        $projectid = $request->projectid ?: 0;
        $segmentid = $request->segmentid ?: 0;
        $positionid = $request->positionid ?: 0;
        $maxcostitemid = ProjectSegmentsProduct::where('title',$subprojectid)->where('project_id',$projectid)->where('segment',$segmentid)->max('inc');
        $newid = $maxcostitemid+1;
        $costitemproduct = $request->itemid;
        $costitem = ProjectCostItemsProduct::find($costitemproduct);
        $proprogetdat = new ProjectSegmentsProduct();
        $proprogetdat->title = $subprojectid;
        $proprogetdat->project_id = $projectid;
        $proprogetdat->segment = $segmentid;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_product_id = $costitem->id;
        $proprogetdat->cost_items_id = $costitem->cost_items_id;
        $proprogetdat->unit = $costitem->unit;
        $proprogetdat->description = $costitem->description;
        $proprogetdat->assign_to = $costitem->assign_to;
        $proprogetdat->position_id = $positionid;
        $proprogetdat->rate = $costitem->rate;
        $proprogetdat->start_date = date('Y-m-d');
        $proprogetdat->deadline = date('Y-m-d');
        $proprogetdat->totalqty = $costitem->qty;
        $proprogetdat->inc = $newid;
        $proprogetdat->save();
        return array('status'=>true);
    }
    public function costitemDestroy($id)
    {
        ProjectSegmentsProduct::where('id',$id)->delete();
        return Reply::success(__('messages.taskDelete'));

    }
    public function costitemCatDestroy($id)
    {
        $position = ProjectSegmentsPosition::where('id',$id)->first();
        if(!empty($position->id)){
            $category = '';
            if(!empty($position->parent)){
                $category .= $position->parent;
                $category .= ',';
            }
            $category .= $position->itemid;
            ProjectSegmentsProduct::where('category',$category)->where('project_id',$position->project_id)->where('title',$position->title)->where('segment',$position->segment)->delete();
            $position->delete();
            return Reply::success(__('messages.taskDelete'));
        }
    }
    public function addBoqCostItemCategory(Request $request){

        if(empty($request->segmentid)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $parent = $request->parent ?: 0;
        $projectid = $request->projectid ?: 0;
        $subprojectid = $request->subprojectid ?: 0;
        $segmentid = $request->segmentid ?: 0;
        $maxpositionarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new ProjectSegmentsPosition();
        $columsarray->project_id = $projectid;
        $columsarray->title = $subprojectid;
        $columsarray->segment = $segmentid;
        $columsarray->position = 'row';
        $columsarray->productposition_id = $request->positionid;
        $columsarray->itemid = $request->itemid;
        $columsarray->itemname = $request->itemname;
        $columsarray->level = $request->level;
        $columsarray->catlevel = $request->catlevel ?: '';
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();
    }
    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('admin.projects.project-boq-create', $this->data);
    }
    public  function boqChangeColPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCostitemPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function projectBoqLock(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->lockcolms;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)&&count($positionarray)>0){
            ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->update(['collock'=>0]);
        }
        return redirect(url('admin/projects/segments/'.$projectid.'/'.$title.'/'.$segment));
    }
    public  function updateCostItem(Request $request){
        if(empty($request->segmentid)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $user = $this->user;
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $projectid = $request->projectid;
        $title = $request->subprojectid ?: 0;
        $segment = $request->segmentid ?: 0;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'start_date':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'deadline':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'worktype':
                $worktype = Type::where('title',$itemvalue)->first();
                if(!empty($worktype->id)){
                    $dataarray[$item] = $worktype->id;
                }
                break;
        }
        ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$itemid)->update($dataarray);
        $projectcostitem = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$itemid)->first();
        $marktype = $projectcostitem->markuptype;
        $markupvalue = $projectcostitem->markupvalue;
        $rate = $projectcostitem->rate;
        $qty = $projectcostitem->qty;
        $adjustment = $projectcostitem->adjustment ?: 0;
        $finalrate = $rate;
        $totalamount = 0;
        if(!empty($qty)&&!empty($rate)){
            $totalamount = $qty*$rate;
            $projectcostitem->finalrate = $rate;
            $projectcostitem->finalamount = $totalamount;
        }
        if(!empty($marktype)&&!empty($markupvalue)){
            if($marktype=='percent'){
                $percent = ($markupvalue/100)*$rate;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            if($marktype=='amount'){
                $percent =$markupvalue;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            $totalamount = $finalrate-$adjustment;
            $projectcostitem->finalrate = $finalrate;
            $projectcostitem->finalamount = $totalamount;
        }
        $projectcostitem->save();
    }
    public function boqtitleLoop(Request $request){
        $segmentid = $request->segmentid ?: 0;
        $subprojectid = $request->subprojectid ?: 0;
        $projectid = $request->projectid ?: 0;
        $user = $this->user;
        $this->user = $user;
        $this->categories = BoqCategory::get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->typesarray = Type::where('company_id',$user->company_id)->get();
        $this->contractorarray = Employee::getAllContractors($user);
        $this->userarray = Employee::getAllEmployees($user);
        $this->subprojectid = $subprojectid ?: 0;
        $this->projectid = (int)$projectid;
        $this->segmentid = (int)$segmentid ?: 0;

        $columsarray = array(
            'task'=>'Task',
            'description'=>'Description',
            'assign_to'=>'Assign to',
            'contractor'=>'Contractor',
            'startdate'=>'Start date',
            'enddate'=>'End date',
            'totalqty'=>'BOQ Qty',
            'qty'=>'Segment Qty',
            'rate'=>'Rate',
            'unit'=>'Unit',
            'finalrate'=>'Final rate',
            'totalamount'=>'Total amount'
        );

        $this->columsarray =  $columsarray;
        $messageview = View::make('admin.projects.segmentboqtitleloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
}
