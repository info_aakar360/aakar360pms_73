<?php

namespace App\Http\Controllers\Admin;

use App\Conversation;
use App\Employee;
use App\GroupChat;
use App\GroupChatMessage;
use App\GroupChatUsers;
use App\Helper\Reply;
use App\Http\Requests\ChatStoreRequest;
use App\MessageSetting;
use App\Notifications\NewChat;
use App\Project;
use App\ProjectMember;
use App\ProjectsLogFiles;
use App\ProjectsLogs;
use View;
use App\Team;
use App\User;
use App\UserChat;
use App\Traits\Notifications;
use Illuminate\Http\Request;

/**
 * Class MemberChatController
 * @package App\Http\Controllers\Member
 */
class AdminChatController extends AdminBaseController
{
    use Notifications;
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.messages';
        $this->pageIcon = 'icon-envelope';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('messages',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }
    public function index()
    {
        $user = $this->user;

        $id   = !empty($this->userList[0]) ? $this->userList[0]->id : '';
        $name   = !empty($this->userList[0]) ? $this->userList[0]->name : '';

        $this->dpData = $id;
        $this->dpName = $name;
        $this->menulist = $this->menulist();

       /* $prarray = array_filter(explode(',',$user->projectlist));
        $projectsarray = Project::whereIn('id', $prarray)->get();
        foreach ($projectsarray as $groupchat){
            $projectmembers = ProjectMember::where('project_id',$groupchat->id)->pluck('user_id')->toArray();
            $conversation = new Conversation();
            $conversation->users = !empty($projectmembers) ? implode(',',$projectmembers) : '';
            $conversation->moduleid = $groupchat->id;
            $conversation->module = 'project';
            $conversation->lastupdated = date('Y-m-d H:i:s');
            $conversation->save();
        }*/


        return view('admin.user-chat.index', $this->data);
    }
    public function memberListModal(){
        $user = $this->user;
        $employeearray = Employee::where('company_id',$user->company_id)->get();

    }
    public function chatBox(Request $request){
        $user = $this->user;
        $page = $request->page;
        $count = pagecount();
        $skip = 0;
        if($page){
            $skip = $page*$count;
        }
        $userid = !empty($request->userID) ? $request->userID : '';
        $employee = Employee::where('company_id',$user->company_id)->where('user_id',$userid)->first();
        $dataarray['user'] = $user;
        if(!empty($employee)){
            $dataarray['employee'] = $employee;
            $conversionDetail = UserChat::conversionDetail($userid, $user->id);
            $chatDetails = UserChat::chatDetail($userid, $user->id,$skip);
            $dataarray['chatdata'] = $this->userChatData($chatDetails,$conversionDetail);
            $messageview = View::make('admin.user-chat.employee-chat-box',$dataarray);
        }else{
            $users = User::withoutGlobalScope('active')->where('id',$userid)->first();
            $dataarray['inviteuser'] = $users;
            $conversionDetail = UserChat::conversionDetail($userid, $user->id);
            $chatDetails = UserChat::chatDetail($userid, $user->id,$skip);
            $dataarray['chatdata'] = $this->userChatData($chatDetails,$conversionDetail);
            $messageview = View::make('admin.user-chat.chat-box',$dataarray);
        }
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function chatMessages(Request $request)
    {
        $user = $this->user;
        $userID = $request->userID;
        $id     = $userID;
        $page = $request->page;
        $count = pagecount();
        $skip = 0;
        if($page){
            $skip = $page*$count;
        }
        $updateData = ['message_seen' => 'yes'];
        UserChat::messageSeenUpdate($user->id, $id, $updateData);
        $this->conversionDetail = UserChat::conversionDetail($id, $this->user->id);
        $this->chatDetails = UserChat::chatDetail($id, $this->user->id,$skip);
        if (request()->ajax()) {
            return $this->userChatData($this->chatDetails,$this->conversionDetail);
        }
    }

    public function conversationChatMessages(Request $request)
    {
        $user = $this->user;
        $conversationid = $request->conversationid;
        $converstaion = Conversation::find($conversationid);
        $userID = $request->conversationid;
        $oneid     = $converstaion->user_one;
        $twoid     = $converstaion->user_two;

        $this->conversionDetail = UserChat::conversionDetail($oneid, $twoid);
        $this->chatDetails = UserChat::chatDetail($oneid, $twoid);
        if (request()->ajax()) {
            return $this->userChatData($this->chatDetails,$this->conversionDetail);
        }
    }

    /**
     * @param $menulist
     * @return array
     */
    public function menulist()
    {
        $user = $this->user;
        $conversationchat = array();
        $conversationarray = Conversation::whereRaw('FIND_IN_SET(?,users)', [$user->id])->orderBy('lastupdated','desc')->get();
        foreach ($conversationarray as $conversation){
            $conarray = array();
            $conarray['id'] = $conversation->id;
            $conarray['userid'] = $user->id;
            $conarray['moduleid'] = $conversation->moduleid;
            $conarray['module'] = $conversation->module;
            switch ($conversation->module){
                case 'chat':
                    $usersarray = explode(',',$conversation->users);
                    if (($key = array_search($user->id, $usersarray)) !== false) {
                        unset($usersarray[$key]);
                    }
                    $chatusers = Employee::where('company_id',$user->company_id)->whereIn('user_id',$usersarray)->first();
                    if(!empty($chatusers)){
                        $conarray['chatid'] = $chatusers->user_id;
                        $conarray['name'] = $chatusers->name;
                        $conarray['image'] = get_users_image_link($chatusers->id);
                    }else{
                        $users = User::withoutGlobalScope('active')->whereIn('id',$usersarray)->first();
                        $conarray['chatid'] = $users->id;
                        $conarray['name'] = $users->name;
                        $conarray['image'] = get_users_image_link($users->id);
                    }
                    break;
                case 'project':
                    $projectinfo =  Project::find($conversation->moduleid);
                    $conarray['projectid'] = $projectinfo->id;
                    $conarray['name'] = $projectinfo->project_name;
                    $conarray['image'] = $projectinfo->imageurl;
                    break;
                case 'group':
                    $projectinfo =  GroupChat::find($conversation->moduleid);
                    $conarray['groupid'] = $projectinfo->id;
                    $conarray['name'] = $projectinfo->name;
                    $conarray['image'] = uploads_url().'user.png';
                    break;
            }
            $conversationchat[] = $conarray;
        }
        $dataarray['user'] = $user;
        $dataarray['conversationarray'] = $conversationchat;
        $messageview = View::make('layouts.pulsemenu',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;

    }
    /**
     * @param $chatDetails
     * @param $type
     * @return string
     */
    public function userChatData($chatDetails,$conversionDetail)
    {
        $chatMessage = '';
        $this->chatDetails = $chatDetails;
        $this->conversion = $conversionDetail;
        $chatMessage .= view('admin.user-chat.ajax-chat-list', $this->data)->render();
        $chatMessage .= '<li id="scrollHere"></li>';

        return Reply::dataOnly(['menulist' => $this->menulist(),'conversionid' => $conversionDetail,'chatData' => $chatMessage]);

    }

    /**
     * @return mixed
     */
    public function postChatMessage(ChatStoreRequest $request)
    {
       $user = $this->user;

        $conversationid = $request->conversationid;
        $message = $request->get('message');
        $userID = $request->get('user_id');

        if(!empty($message)){
            $allocatedModel = new UserChat();
            $allocatedModel->message         = $message;
            $allocatedModel->user_one        = $this->user->id;
            $allocatedModel->user_id         = $userID;
            $allocatedModel->from            = $this->user->id;
            $allocatedModel->to              = $userID;
            $allocatedModel->type = 'text';
            $allocatedModel->save();
        }

        if($request->hasFile('image')){
            foreach ($request->image as $fileData){
                $image = $fileData->hashName();
                $destinationPath = 'uploads/chat-image/';
                if (!file_exists(''.$destinationPath)) {
                    mkdir(''.$destinationPath, 0777, true);
                }
                $fileData->storeAs($destinationPath, $image);
                $allocatedModel = new UserChat();
                $allocatedModel->image = $image;
                $allocatedModel->type = 'image';
                $allocatedModel->message         = $message;
                $allocatedModel->user_one        = $this->user->id;
                $allocatedModel->user_id         = $userID;
                $allocatedModel->from            = $this->user->id;
                $allocatedModel->to              = $userID;
                $allocatedModel->save();
            }
        }
        // Notify User
        $notifyUser = User::withoutGlobalScope('active')->where('id',$allocatedModel->user_id)->first();
        if($notifyUser){

            $this->conversations($notifyUser, 'chat', $conversationid, $message);

        }
        $lastLiID = '';
        return Reply::dataOnly(['chatData' => $this->index(), 'dataUserID' => $this->user->id,   'liID' => $lastLiID]);
    }

    /**
     * @return mixed
     */
    public function userListLatest($term = null)
    {
        $result = User::userListLatest($this->user->id, $term );

        return $result;
    }



    public function getUserSearch()
    {
        $term = Input::get('term');
        $this->userLists = $this->userListLatest($term);

        $users = '';

        $users = view('admin.user-chat.ajax-user-list', $this->data)->render();

        return Reply::dataOnly(['userList' => $users]);
    }


    public function create() {
        $user = $this->user;
        $this->employeearray = Employee::where('company_id',$user->company_id)->get();
        return view('admin.user-chat.create', $this->data);
    }
    public function addMemberToChat(Request $request){
        $user = $this->user;
        $seelctedusers = explode(',',$request->selectedusers);
        foreach ($seelctedusers as $seelctedu){
            $conversionDetail = UserChat::conversionDetail($seelctedu, $user->id);
            $chatDetails = UserChat::chatDetail($seelctedu, $user->id);
            $chatdata = $this->userChatData($chatDetails,$conversionDetail);
        }
        $menulist = $this->menulist();
        return Reply::dataOnly(['menulist' => $menulist,'user' => $seelctedu,'chatdata' => $chatdata]);
    }
    public function userChatDemo(){
        $this->members = User::allEmployees($this->user->id);
        $this->clients = User::allClients();
        $this->messageSetting = MessageSetting::first();
        return view('admin.user-chat.demo', $this->data);
    }

    public function groupCreate() {
        $user = $this->user;
        $this->employeearray = Employee::where('company_id',$user->company_id)->get();
        return view('admin.user-chat.group-create', $this->data);
    }
    public function groupchatBox(Request $request){
        $user = $this->user;
        $page = !empty($request->page) ? $request->page : 0;
        $groupid = !empty($request->groupid) ? $request->groupid : '';
        $mailcontent = $this->groupchatloop($groupid,$page);
        $dataarray =     $employeearray =    $employeearray = array();
        $group = GroupChat::find($groupid);
        $dataarray['group'] = $group;
        $dataarray['chatdata'] = $mailcontent;

        $conversation =  Conversation::where('module','group')->where('moduleid',$group->id)->first();
        $usersarray= explode(',',$conversation->users);
       $employeearray = Employee::where('company_id',$user->company_id)->whereIn('user_id',$usersarray)->get();
        $employeenamws = Employee::where('company_id',$user->company_id)->whereIn('user_id',$usersarray)->pluck('name')->toArray();
        if($group->added_by==$user->id){
            $employeenamws[] = 'You';
        }else{
            $employeenamws[] = get_user_name($group->added_by);
        }
        $dataarray['joinedusers'] = $usersarray;
        $dataarray['groupemployeearray'] = $employeearray;
        $dataarray['employeenames'] = $employeenamws;
        $messageview = View::make('admin.user-chat.groupchat-box',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function createGroupChat(Request $request)
    {
        $user = $this->user;
        return view('admin.user-chat.create-group-chat', $this->data);
    }

    public function groupChatStore(Request $request){
        $user = $this->user;
        $groupchat = new GroupChat();
        $groupchat->company_id = $user->company_id;
        $groupchat->added_by = $user->id;
        $groupchat->name = $request->name;
        $groupchat->joined_users = $user->id;
        $groupchat->save();

        $conversation = new Conversation();
        $conversation->users = $user->id;
        $conversation->moduleid = $groupchat->id;
        $conversation->module = 'group';
        $conversation->save();

        $groupchatmessage = new GroupChatUsers();
        $groupchatmessage->company_id = $groupchat->company_id;
        $groupchatmessage->group_id = $groupchat->id;
        $groupchatmessage->user_id = $user->id;
        $groupchatmessage->status = '1';
        $groupchatmessage->save();

        return Reply::success(__('messages.groupCreated'));
    }
    public function groupchatloop($groupid,$page){
        $user = $this->user;
        $count = pagecount();
        $skip = 0;
        if($page){
            $skip = $page*$count;
        }
        $mailcontent = '';
        if(!empty($groupid)){
            $projectsarray = GroupChatMessage::where('group_id', $groupid);
            $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->pluck('id')->toArray();
            $projectsids = array_reverse($projectsarray);
            $projectsarray = GroupChatMessage::whereIn('id',$projectsids)->get();
            $dataarray['user'] = $user;
            $dataarray['groupmessagesarray'] = $projectsarray;
            $messageview = View::make('admin.user-chat.group-chatloop',$dataarray);
            $mailcontent = $messageview->render();
        }
        return $mailcontent;
    }
    public function groupChatData(Request $request){
        $user = $this->user;
        $count = 100;
        $skip = 0;
        $page = !empty($request->page) ? $request->page : 0;
        if($page){
            $skip = $page*$count;
        }

        $groupid = !empty($request->groupid) ? $request->groupid : '';
        $mailcontent = '';
        if(!empty($groupid)){
            $projectsarray = GroupChatMessage::where('group_id', $groupid);
            $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','asc')->get();
            $dataarray['user'] = $user;
            $dataarray['groupmessagesarray'] = $projectsarray;
            $messageview = View::make('admin.user-chat.group-chatloop',$dataarray);
            $mailcontent = $messageview->render();
        }
        $menulist = $this->menulist();
        return Reply::dataOnly(['menulist' => $menulist,'chatdata' => $mailcontent]);
    }
    public function groupChatSubmit(Request $request){

        $user = $this->user;
        $projectid = $request->groupid;
        $message = $request->message;
        $groupid = GroupChat::find($projectid);
        if(!empty($message)) {
            $createlog = new GroupChatMessage();
            $createlog->company_id = $user->company_id;
            $createlog->user_id = $user->id;
            $createlog->group_id = $groupid->id;
            $createlog->message = $message;
            $createlog->type = 'text';
            $createlog->save();
        }
        if($request->hasFile('image')){
            foreach ($request->image as $fileData){
                $image = $fileData->hashName();
                $destinationPath = 'uploads/group-chat-image/';
                if (!file_exists(''.$destinationPath)) {
                    mkdir(''.$destinationPath, 0777, true);
                }
                $fileData->storeAs($destinationPath, $image);
                $createlog = new GroupChatMessage();
                $createlog->company_id = $groupid->company_id;
                $createlog->user_id = $user->id;
                $createlog->group_id = $groupid->id;
                $createlog->message = $message;
                $createlog->image = $image;
                $createlog->type = 'image';
                $createlog->save();
            }
        }

        $date = date('Y-m-d H:i:s');
         Conversation::where('module','group')->where('moduleid',$groupid->id)->update(['lastupdated'=>$date]);
        $pm = explode(',',$groupid->joined_users);
        // Notify User
        $notifyUserarray = User::withoutGlobalScope('active')->whereIn('id',$pm)->get();
        foreach($notifyUserarray as $notifyUser){
            if($notifyUser){
                $this->conversations($notifyUser, 'groupchat', $groupid->id, $message);
            }
        }
        return 'success';
    }
    public function groupChatJoinUser(Request $request){
        $user = $this->user;
        $groupid = $request->groupid;
        $page = 0;
        $selectedusers = explode(',',$request->selectedusers);
        $groupdetails = GroupChat::find($groupid);
        $groupusers = explode(',',$groupdetails->joined_users);
        $groupusers  = array_merge($selectedusers,$groupusers);
        $groupusers = array_filter(array_unique($groupusers));
        $joined_users = implode(',',$groupusers);
        $groupdetails->joined_users = $joined_users;
        $groupdetails->save();

        $conversation =  Conversation::where('module','group')->where('moduleid',$groupid)->first();
        $conversation->users =  $joined_users;
        $conversation->save();

        if(count($groupusers)>0){
            foreach ($groupusers as $groupus){
                $groupchatmessage = GroupChatUsers::where('group_id',$groupdetails->id)->where('user_id',$groupus)->first();
                if(empty($groupchatmessage)){
                    $groupchatmessage = new GroupChatUsers();
                    $groupchatmessage->company_id = $groupdetails->company_id;
                    $groupchatmessage->group_id = $groupdetails->id;
                    $groupchatmessage->user_id = $groupus;
                    $groupchatmessage->status = '1';
                    $groupchatmessage->save();
                }
            }
        }

        $menulist = $this->menulist();
        $mailcontent = $this->groupchatloop($groupid,$page);
        return Reply::dataOnly(['menulist' => $menulist,'chatdata' => $mailcontent]);
    }
    public function projectChatBox(Request $request){
        $user = $this->user;
        $page = !empty($request->page) ? $request->page : 0;
        $projectid = !empty($request->projectid) ? $request->projectid : '';
        $mailcontent = $this->pulseloop($projectid,$page);
        $dataarray = array();
        $dataarray['project'] = Project::find($projectid);
        $dataarray['chatdata'] = $mailcontent;
        $messageview = View::make('pulse.pulsechat-box',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function projectPulseData(Request $request){
        $user = $this->user;
        $count = 100;
        $skip = 0;
        $page = !empty($request->page) ? $request->page : 0;
        if($page){
            $skip = $page*$count;
        }

        $projectid = !empty($request->projectid) ? $request->projectid : '';
        $mailcontent = $this->pulseloop($projectid,$page);
        return $mailcontent;
    }
    public function pulseloop($projectid,$page){
        $user = $this->user;
        $count = 100;
        $skip = 0;
        if($page){
            $skip = $page*$count;
        }
        $mailcontent = '';
        if(!empty($projectid)){
            $projectsarray = ProjectsLogs::where('project_id', $projectid);
            $dontsohw = array('projects','project_category');
            $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
            $projectsarray = $projectsarray->orderBy('id','asc')->get();
            $dataarray['user'] = $user;
            $dataarray['projectsarray'] = $projectsarray;
            $messageview = View::make('pulse.project-pulseloop',$dataarray);
            $mailcontent = $messageview->render();
        }
        $menulist = $this->menulist();
        return Reply::dataOnly(['menulist' => $menulist,'chatdata' => $mailcontent]);
    }
    public function projectPulseMessageSubmit(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $message = $request->message;
        $projectId = Project::find($projectid);
        $medium = $request->medium ?: 'web';

        $date = date('Y-m-d H:i:s');
        Conversation::where('module','project')->where('moduleid',$projectid)->update(['lastupdated'=>$date]);
        $createlog = new ProjectsLogs();
        $createlog->company_id = $projectId->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $projectId->id;
        $createlog->module = 'chat';
        $createlog->project_id = $projectId->id ?: 0;
        $createlog->subproject_id =   0;
        $createlog->segment_id =   0;
        $createlog->heading =   'Chat';
        $createlog->modulename = 'chat';
        $createlog->description = $message;
        $createlog->medium = $medium;
        $createlog->save();

        if($request->hasFile('image')){
            foreach ($request->image as $fileData){
                $image = $fileData->hashName();
                $destinationPath = 'uploads/project-logs-files/';
                if (!file_exists(''.$destinationPath)) {
                    mkdir(''.$destinationPath, 0777, true);
                }
                $fileData->storeAs($destinationPath, $image);
                $createlogfile = new ProjectsLogFiles();
                $createlogfile->company_id = $user->company_id;
                $createlogfile->user_id = $user->id;
                $createlogfile->projectlog_id = $createlog->id;
                $createlogfile->filename = $fileData->getClientOriginalName();
                $createlogfile->hashname = $fileData->hashName();
                $createlogfile->size = $fileData->getSize();
                $createlogfile->save();
            }
        }
        $pm = ProjectMember::where('project_id',$projectid)->where('share_project','1')->pluck('user_id')->toArray();
        $pm[] = $user->id;
        $notifyUserarray = User::withoutGlobalScope('active')->whereIn('id',$pm)->get();
        foreach($notifyUserarray as $notifyUser){
            if($notifyUser){
                $this->conversations($notifyUser, 'projectpulse', $projectid, $message);
            }
        }
        return $createlog->id;
    }
    public function storeImage(Request $request){

        $user = $this->user;
        $message = '';
        $chatmodule = $request->chatmodule;
        if($chatmodule=='chat'){
            $userID = $request->userid;
            if($request->hasFile('image')){
                foreach ($request->image as $fileData){
                    $image = $fileData->hashName();
                    $destinationPath = 'uploads/chat-image/';
                    if (!file_exists(''.$destinationPath)) {
                        mkdir(''.$destinationPath, 0777, true);
                    }
                    $fileData->storeAs($destinationPath, $image);
                    $allocatedModel = new UserChat();
                    $allocatedModel->image = $image;
                    $allocatedModel->type = 'image';
                    $allocatedModel->message         = $message;
                    $allocatedModel->user_one        = $user->id;
                    $allocatedModel->user_id         = $userID;
                    $allocatedModel->from            = $user->id;
                    $allocatedModel->to              = $userID;
                    $allocatedModel->save();
                }
            }
        }
        if($chatmodule=='groupchat'){
            $projectid = $request->groupchatid;
            $groupid = GroupChat::find($projectid);
            if($request->hasFile('image')){
                foreach ($request->image as $fileData){
                    $image = $fileData->hashName();
                    $destinationPath = 'uploads/group-chat-image/';
                    if (!file_exists(''.$destinationPath)) {
                        mkdir(''.$destinationPath, 0777, true);
                    }
                    $fileData->storeAs($destinationPath, $image);
                    $createlog = new GroupChatMessage();
                    $createlog->company_id = $groupid->company_id;
                    $createlog->user_id = $user->id;
                    $createlog->group_id = $groupid->id;
                    $createlog->message = $message;
                    $createlog->image = $image;
                    $createlog->type = 'image';
                    $createlog->save();
                }
            }
        }
        if($chatmodule=='projectpulse'){
            $projectid = $request->projectid;
            $projectlogid = $request->projectlogid;
            $createlog = ProjectsLogs::find($projectlogid);
            if($request->hasFile('image')){
                foreach ($request->image as $fileData){
                    $image = $fileData->hashName();
                    $destinationPath = 'uploads/project-logs-files/';
                    if (!file_exists(''.$destinationPath)) {
                        mkdir(''.$destinationPath, 0777, true);
                    }
                    $fileData->storeAs($destinationPath, $image);
                    $createlogfile = new ProjectsLogFiles();
                    $createlogfile->company_id = $createlog->company_id;
                    $createlogfile->user_id = $user->id;
                    $createlogfile->projectlog_id = $createlog->id;
                    $createlogfile->filename = $fileData->getClientOriginalName();
                    $createlogfile->hashname = $fileData->hashName();
                    $createlogfile->size = $fileData->getSize();
                    $createlogfile->save();
                }
            }
        }
        return 'success';
    }
}
