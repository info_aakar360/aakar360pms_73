<?php

namespace App\Http\Controllers\Admin;


use App\Helper\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use App\Transaction;

class InitialIncomeExpenseHeadBalanceController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Initial Income Expense Head Balance';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = Transaction::class;
    public $parentRoute = 'admin.initial_income_expense_head_balance';
    public $parentView = "admin.initial-income-expense-head-balance";

    public $voucher_type="IIEHBV";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->items = Transaction::where('voucher_type', $this->voucher_type)->orderBy('id', 'desc')->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'branch_id' => 'required|numeric|min:1',
            'income_expense_head_id' => 'required|numeric|min:1',
            'amount' => 'required|numeric|min:1',
            'voucher_date' => 'required',
        ]);
        $initial_head_balance_exist_or_not = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->withTrashed()
            ->where('branch_id', '=', $request->branch_id)
            ->where('income_expense_head_id', '=', $request->income_expense_head_id)
            ->withTrashed()
            ->get();
        if (count($initial_head_balance_exist_or_not) > 0) {
            return Reply::error(__('This Branch Initial Balance already exit. Try another one'));
        }
        $transaction = new $this->parentModel;
        $DrCr = $transaction->isDebitByIncomeExpenseHeadID($request->income_expense_head_id);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $Dr = 0;
        $Cr = 0;
        if ($DrCr) {
            $Dr = $request->amount;
        } else {
            $Cr = $request->amount;
        }
        $voucher_info = Transaction::withTrashed()
            ->orderBy('voucher_no', 'desc')
            ->get()
            ->first();
        if (!empty($voucher_info)) {
            $voucher_no = $voucher_info->id + 1;
        } else {
            $voucher_no = 1;
        }
        $ts = Transaction::create([
            'voucher_no' => $voucher_no,
            'branch_id' => $request->branch_id,
            'income_expense_head_id' => $request->income_expense_head_id,
            'voucher_type' => $this->voucher_type,
            'date' => date('Y-m-d',strtotime($request->voucher_date)),
            'particulars' => $request->particulars,
            'dr' => $Dr,
            'cr' => $Cr,
            'created_by' => \Auth::user()->id,
        ]);
        return Reply::success(__('Successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($request) {
                $q->where('voucher_no', '=', $request->id);
            })
            ->get();
        if (count($this->items)<1) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get()->first();
        if (empty($this->item)) {
            return Reply::error(__('Item not found'));
        }
        $date = new \DateTime($this->item->voucher_date);
        $voucher_date = $date->format('m-d-Y'); // 31-07-2012 '2008-11-11'
        $this->item['amount'] = $this->item->cr;
        $this->item['date'] = date('d-m-Y',strtotime($this->item->date));
        if ($this->item->dr > 0) {
            $this->item['amount'] = $this->item->dr;
        }
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'branch_id' => 'required|numeric|min:1',
            'income_expense_head_id' => 'required|numeric|min:1',
            'amount' => 'required|numeric|min:1',
            'voucher_date' => 'required',
        ]);
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get()->first();
        if ( ($request->branch_id !=$items->branch_id) or ($request->income_expense_head_id
                !=$items->income_expense_head_id) ){
            $initial_head_balance_exist_or_not = Transaction::where('voucher_type', '=', $this->voucher_type)
                ->withTrashed()
                ->where('branch_id', '=', $request->branch_id)
                ->where('income_expense_head_id', '=', $request->income_expense_head_id)
                ->withTrashed()
                ->get();
            if (count($initial_head_balance_exist_or_not) > 0) {
                return Reply::error(__('Initial Balance already exit. Try another one'));
            }
        }
        $transaction = new $this->parentModel;
        $DrCr = $transaction->isDebitByIncomeExpenseHeadID($request->income_expense_head_id);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $Dr = 0;
        $Cr = 0;
        if ($DrCr) {
            $Dr = $request->amount;
        } else {
            $Cr = $request->amount;
        }
        $items->branch_id = $request->branch_id;
        $items->income_expense_head_id = $request->income_expense_head_id;
        $items->particulars = $request->particulars;
        $items->date = date('Y-m-d',strtotime($request->voucher_date));
        $items->dr = $Dr;
        $items->cr = $Cr;
        $items->updated_by = \Auth::user()->id;
        $items->save();
        return Reply::success(__('Successfully Updated'));
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        $item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($item) == 0) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format') . ' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Initial Ledger Balance Report',
            'voucher_type' => 'INITIAL LEDGER BALANCE'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
        return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get()->first();
        if (empty($items)) {
            return Reply::error(__('Item not found'));
        }
        $items->deleted_by = \Auth::user()->id;
        $items->save();
        $items->delete();
        return Reply::success(__('Successfully Trashed'));
    }

    public function trashed()
    {
        $this->items = Transaction::onlyTrashed()->where('voucher_type', $this->voucher_type)->orderBy('id', 'desc')->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }


    public function restore($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->onlyTrashed()
            ->get()
            ->first();
        $items->restore();
        $items->updated_by = \Auth::user()->id;
        $items->save();
        return Reply::success(__('Successfully Restored'));
    }

    public function kill($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->withTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->withTrashed()
            ->get()
            ->first();
        $items->forceDelete();
        return Reply::error(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('voucher_date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('voucher_date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->onlyTrashed()
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }


//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
}
