<?php

namespace App\Http\Controllers\Admin;

use App\AppProject;
use App\ClientDetails;
use App\Employee;
use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Supplier\StoreSupplierRequest;
use App\Http\Requests\Admin\Supplier\UpdateSupplierRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\Supplier;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageSuppliersController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.suppliers';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';
        $this->middleware(function ($request, $next) {
            if (!in_array('suppliers', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = Auth::user()->company_id;
        $this->suppliers = Supplier::where('added_by', $company_id)->get();
        if ($company_id == '' || $company_id == null) {
            $this->suppliers = Supplier::where('company_id', $this->companyid)->get();
        }

        $this->totalSupplier = count($this->suppliers);

        return view('admin.suppliers.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        return view('admin.suppliers.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSupplierRequest $request)
    {
        $user = $this->user;
        $company = company();
        if(!empty($projectid)){
            $projectdetails = AppProject::where('id',$projectid)->first();
            $usercompany = $projectdetails->company_id;
        }else{
            $usercompany = $user->company_id;
        }
        $prevemp = Employee::where('company_id',$usercompany)->where('mobile',$request->mobile)->where('user_type','supplier')->first();
        if(!empty($prevemp)){
            return Reply::error( __('Supplier Already Exists'));
        }
        $alruser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('mobile',$request->mobile)->first();
        if(empty($alruser->id)){
            $alruser = new User();
            $alruser->name = $request->name;
            $alruser->mobile = $request->mobile;
            $alruser->login = 'disable';
            $alruser->status = 'inactive';
            $alruser->save();
        }
        $employee = new Employee();
        $employee->company_id = $usercompany;
        $employee->added_by = $this->user->id;
        $employee->user_id = $alruser->id;
        $employee->name = $request->name;
        $employee->mobile = $request->mobile;
        $employee->user_type = 'supplier';
        $employee->save();
        $employeedetail = new EmployeeDetails();
        $employeedetail->company_id = $company->id;
        $employeedetail->user_id = $alruser->id;
        $employeedetail->employee_id = $employee->id;
        $employeedetail->address = $request->address;
        $employeedetail->company_name = $request->company_name;
        $employeedetail->save();

        return Reply::redirect(route('admin.suppliers.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->supplier = Employee::find($id);
        $this->supplierdetails = EmployeeDetails::where('employee_id',$id)->first();
        return view('admin.suppliers.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSupplierRequest $request, $id)
    {
        $user = $this->user;
        $employee = Employee::findOrFail($id);
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->mobile = $request->mobile;
        $employee->status = 'active';
        $employee->save();
        $employeedetails = EmployeeDetails::where('employee_id', '=', $employee->id)->first();
        $employeedetails->company_name = $request->company_name;
        $employeedetails->address = $request->address?$request->address:'';
        $employeedetails->website = $request->website?$request->website:'';
        $employeedetails->gst = $request->gst_number?$request->gst_number:'';
        $employeedetails->note = $request->note?$request->note:'';
        $employeedetails->save();
        return Reply::redirect(route('admin.suppliers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmployeeDetails::where('employee_id',$id)->delete();
        Employee::where('id',$id)->where('user_type','supplier')->delete();
        return Reply::success(__('messages.supplierDeleted'));
    }

    public function data(Request $request)
    {

        $users = Employee::where('user_type','supplier');
        if ($request->supplier != 'all' && $request->supplier != '') {
            $users = $users->where('id', $request->supplier);
        }
        if (!empty($request->startDate) && !empty($request->endDate) && $request->startDate !== 'null' && $request->endDate !== 'null') {
            $startDate = Carbon::createFromFormat($this->global->date_format, $request->startDate)->format('Y-m-d');
            $endDate = Carbon::createFromFormat($this->global->date_format, $request->endDate)->format('Y-m-d');
            $users->where(function ($q) use ($startDate, $endDate) {
                $q->whereBetween(DB::raw('DATE(employee.`created_at`)'), [$startDate, $endDate]);
                $q->orWhereBetween(DB::raw('DATE(employee.`updated_at`)'), [$startDate, $endDate]);
            });
        }
        $users = $users->get();
        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.suppliers.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="' . route('admin.suppliers.edit', $row->id) . '">' . ucfirst($row->name) . '</a>';
                }
            )->editColumn(
                'company_name',
                function ($row) {
                    $company_name = EmployeeDetails::where('employee_id',$row->id)->first();
                    if(!empty($company_name->company_name)){
                        return ucfirst($company_name->company_name);
                    }else{
                        return 'NA';
                    }

                }
            )->editColumn(
                'email',
                function ($row) {
                    return ucfirst($row->email);
                }
            )->editColumn(
                'mobile',
                function ($row) {
                    return ucfirst($row->mobile);
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }


    public function export($status, $client)
    {
        /*$rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'client')
            ->where('roles.company_id', company()->id)
            ->leftJoin('client_details', 'users.id', '=', 'client_details.user_id')
            ->select(
                'users.id',
                'client_details.name',
                'client_details.email',
                'client_details.mobile',
                'client_details.company_name',
                'client_details.address',
                'client_details.website',
                'client_details.created_at'
            )
            ->where('client_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($client != 'all' && $client != '') {
            $rows = $rows->where('users.id', $client);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('clients', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('clients file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');*/
    }

}
