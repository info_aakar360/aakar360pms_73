<?php

namespace App\Http\Controllers\Admin;

use App\CostItems;
use App\Project;
use App\Http\Requests\Milestone\StoreMilestone;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\Helper\Reply;
use App\Segment;
use App\Title;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Currency;

class ManageProjectMilestonesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.projects-milestone');
        $this->pageIcon = 'icon-layers';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id){
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);

            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.milestones.index', $this->data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function milestoneslist(Request $request, $projectid,$subprojectid){
        $user = $this->user;
        $project = Project::find($projectid);
        if(empty($project->id)){
            return redirect(route('admin.milestone.show','all'));
        }
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        $this->project = $project;
        $this->subproject = Title::find($subprojectid);
        return view('admin.projects.milestones.milestoneslist', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        return view('admin.projects.milestones.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user;
       $title = $request->title;
        $maxcostitemid = ProjectCostItemsProduct::where('title',$request->sub_project)->where('project_id',$request->project)->max('inc');
        $newid = $maxcostitemid+1;
        $costitemdetails = CostItems::where('company_id',$user->company_id)->where('cost_item_name',$title)->first();
        if(empty($costitemdetails->id)){
            $costitemdetails = new CostItems();
            $costitemdetails->company_id = $user->company_id;
            $costitemdetails->cost_item_name = $title;
            $costitemdetails->save();
        }
        $positionid = $request->activity;
        $procostprosiion = ProjectCostItemsPosition::where('id',$positionid)->first();
        $proprogetdat = new ProjectCostItemsProduct();
        $proprogetdat->title = $request->sub_project ?: 0;
        $proprogetdat->project_id = $request->project;
        $proprogetdat->category = $procostprosiion->itemid;
        if($procostprosiion->parent){
            $proprogetdat->category = $procostprosiion->parent.','.$procostprosiion->itemid;
        }
        $proprogetdat->cost_items_id = $costitemdetails->id;
        $proprogetdat->position_id = $procostprosiion->id;
        $proprogetdat->unit = $costitemdetails->unit;
        $proprogetdat->start_date = !empty($request->start_date) ? date('Y-m-d',strtotime($request->start_date)) : date('Y-m-d');
        $proprogetdat->deadline = !empty($request->due_date) ? date('Y-m-d',strtotime($request->due_date)) : date('Y-m-d');
        $proprogetdat->inc = $newid;
        $proprogetdat->milestone = '1';
        $proprogetdat->ordertype = 'estimate';
        $proprogetdat->status = $request->status;
        $proprogetdat->description = $request->status;
        $proprogetdat->save();
        return Reply::success(__('messages.milestoneSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::find($id);
            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.milestones.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->milestone = ProjectMilestone::findOrFail($id);
        $this->currencies = Currency::all();
        return view('admin.projects.milestones.edit', $this->data);
    }
    public function editdata($id)
    {
        $this->milestone = ProjectMilestone::findOrFail($id);
        $this->currencies = Currency::all();
        return view('admin.projects.milestones.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMilestone $request, $id)
    {
        $milestone = ProjectMilestone::findOrFail($id);
        $milestone->project_id = $request->project_id;
        $milestone->milestone_title = $request->milestone_title;
        $milestone->summary = $request->summary;
        $milestone->cost = ($request->cost == '') ? '0' : $request->cost;
        $milestone->currency_id = $request->currency_id;
        $milestone->status = $request->status;
        $milestone->save();

        return Reply::success(__('messages.milestoneSuccess')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProjectMilestone::destroy($id);
        return Reply::success(__('messages.deleteSuccess'));
    }

    public function data()
    {

        $milestones = ProjectCostItemsProduct::where('milestone', 1)->get();

        return DataTables::of($milestones)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'.route('admin.milestone.editdata',[$row->id]).'" class="btn btn-info btn-circle"
                        data-toggle="tooltip" data-milestone-id="'.$row->id.'"  data-original-title="'.__('app.edit').'"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                        <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                        data-toggle="tooltip" data-milestone-id="'.$row->id.'" data-original-title="'.__('app.delete').'"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('status', function($row){
                if($row->status == 'complete'){
                    return '<label class="label label-success">'.$row->status.'</label>';
                }
                else{
                    return '<label class="label label-danger">'.$row->status.'</label>';
                }
            })
            ->editColumn('cost_items_id', function($row) {
                return get_cost_name($row->cost_items_id);
            })
            ->editColumn('project_id', function($row) {
                return get_project_name($row->project_id);
            })
            ->rawColumns(['status', 'action', 'project_id', 'cost_items_id'])
            ->make(true);
    }

    public function detail($id)
    {
        $this->milestone = ProjectMilestone::findOrFail($id);
        return view('admin.projects.milestones.detail', $this->data);
    }

    public function getProjectsInfo(Request $request){

        $projectid = $request->projectid;
        $subprojectid = $request->subprojectid ?: 0;
        $projectsarray = array();
        if($projectid){
            $projectsarray['titles'] = Title::where('project_id',$projectid)->pluck('title','id');
            $projectsarray['segments'] = Segment::where('projectid',$projectid)->pluck('name','id');
            $projectsarray['activitylist'] = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->orderBy('inc')->pluck('itemname','id');
        }
        return  json_encode($projectsarray);
    }
/////
}
