<?php

namespace App\Http\Controllers\Admin;

use App\AppProject;
use App\BoqCategory;
use App\Contractors;
use App\DailyLogFiles;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\TimeLogs\StoreTimeLog;
use App\LogTimeFor;
use App\ManpowerAttendance;
use App\ManpowerCategory;
use App\ManpowerLog;
use App\ManpowerLogFiles;
use App\ManpowerLogReply;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectsLogs;
use App\Segment;
use App\Task;
use App\Title;
use App\User;
use App\Workers;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use  View;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageManPowerLogController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Labour Attendance Logs';
        $this->pageIcon = 'icon-clock';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('manpowerlog', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });

    }

    public function index()
    {

        $user = $this->user;
        $projectlist = explode(',', $user->projectlist);
        $this->user = $user;
        $this->projectarray = $this->projectslist;
        $this->employees = Employee::getAllEmployees($user);
        $this->contractorsarray = Employee::getAllContractors($user);
        return view('admin.manpower-logs.index', $this->data);
    }

    public function create()
    {
        $user = $this->user;
        $projectlist = explode(',', $user->projectlist);
        $this->user = $user;
        $this->projectarray = $this->projectslist;
        $this->employees = Employee::getAllEmployees($user);
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->manpowercategoryarray = ManpowerCategory::where('company_id', $user->company_id)->get();
        $this->tasks = Task::where('company_id', $user->company_id)->get();
        return view('admin.manpower-logs.create', $this->data);
    }

    public function edit($id)
    {
        $user = $this->user;
        $projectlist = explode(',', $user->projectlist);
        $this->user = $user;
        $manpower = ManpowerLog::findOrFail($id);
        $projectid = $manpower->project_id ?: 0;
        $subprojectid = $manpower->title_id ?: 0;
        $activityid = $manpower->activity_id ?: 0;
        $this->manpower = $manpower;
        $this->employees = Employee::getAllEmployees($user);
        $this->projectarray = Project::whereIn('id', $projectlist)->get();
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->manpowercategoryarray = ManpowerCategory::where('company_id', $user->company_id)->get();
        $this->titlesarray = !empty($projectid) ? Title::where('project_id', $projectid)->get() : array();
        $this->activitiesarray = !empty($projectid) ? ProjectCostItemsPosition::where('project_id', $projectid)->where('title', $subprojectid)->where('position', 'row')->orderBy('inc','asc')->get() : array();
        $this->costitemsarray = !empty($activityid) ? ProjectCostItemsProduct::where('project_id', $projectid)->where('title', $subprojectid)->where('position_id',$activityid)->orderBy('inc','asc')->get() : array();
        $this->segmentsarray = !empty($subprojectid) ? Segment::where('projectid', $projectid)->where('titleid', $subprojectid)->get() : array();
        $this->categories = BoqCategory::where('company_id', $user->company_id)->get();
        $this->tasks = Task::where('company_id', $user->company_id)->get();
        $this->files = ManpowerLogFiles::where('manpower_id', $id)->where('reply_id', '0')->get();
        return view('admin.manpower-logs.edit', $this->data);
    }


    public function data(Request $request)
    {
        $user = $this->user;
        $contractorId = $request->contractor;
        $projectId = $request->projectId;
        $projectlist = explode(',', $user->projectlist);
        $timeLogs = ManpowerLog::leftjoin('users', 'users.id', '=', 'manpower_logs.added_by');
        $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'manpower_logs.project_id');
        $timeLogs = $timeLogs->select('manpower_logs.*', 'projects.project_name');
        if ($contractorId!='all') {
            $timeLogs->where('manpower_logs.contractor', $contractorId);
        }
        if (!empty($projectId)) {
            $timeLogs->where('manpower_logs.project_id', '=', $projectId);
        } else if (!empty($projectlist)) {
            $timeLogs->whereIn('manpower_logs.project_id', $projectlist);
        }
        $timeLogs = $timeLogs->orderBy('id','desc')->get();

        return DataTables::of($timeLogs)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a href="' . route('admin.man-power-logs.reply', $row->id) . '" class="btn btn-info btn-circle"
                   data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                if ($row->added_by == $user->id) {
                    $actionlink .= '<a href="' . route('admin.man-power-logs.edit', [$row->id]) . '" data-cat-id="' . $row->id . '" class="btn btn-info btn-circle"
                               data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    $actionlink .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                   data-toggle="tooltip" data-time-id="' . $row->id . '" data-original-title="Delete">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>';
                }
                return $actionlink;
            })
            ->editColumn('project_name', function ($row) {
                return '<a href="javascript:void(0);">' . ucfirst($row->project_name) . '</a>';
            })
            ->addColumn('cattitle', function ($row) {
                $categorylist = ManpowerAttendance::join('manpower_category','manpower_category.id','=','manpower_attendance.manpower_category')
                    ->where('manpower_attendance.manpower_id',$row->id)
                    ->groupBy('manpower_category.title')->pluck('manpower_category.title')->toArray();
                return !empty($categorylist) ? implode(',',$categorylist) : '';
            })
            ->editColumn('contractor', function ($row) {
                if ($row->contractor) {
                    return '<a href="javascript:void(0);">' . ucfirst(get_users_contractor_name($row->contractor,$row->company_id)) . '</a>';
                } else {
                    return 'Departmental';
                }
            })
            ->editColumn('total_price', function ($row) {
                $totalprice = ManpowerAttendance::join('manpower_category','manpower_category.id','=','manpower_attendance.manpower_category')
                    ->where('manpower_attendance.manpower_id',$row->id)
                    ->where('manpower_attendance.totalprice','<>','')
                    ->sum('manpower_attendance.totalprice');
                return $totalprice;
            })
            ->rawColumns(['cattitle','contractor', 'action', 'project_name', 'total_price'])
            ->removeColumn('project_id')
            ->removeColumn('task_id')
            ->make(true);
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'manpowercategory' => 'required',
                'manpower' => 'required',
                'workinghours' => 'required',
                'workdate' => 'required',
                'project_id' => 'required',
                'activity' => 'required',
                'costitem' => 'required'],

            ['manpowercategory.required' => 'Manpower category field is required',
                'manpower.required' => 'Manpower field is required',
                'workinghours.required' => 'Working hours field is required',
                'workdate.required' => 'Work date is required',
                'project_id.required' => 'Project field is required',
                'activity.required' => 'Activity field is required',
                'costitem.required' => 'Task field is required']
        );
        $user = $this->user;
        $projectid = $request->project_id;
        if(!empty($request->project_id)){
            $projectid = Project::find($request->project_id);
            $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $startdate =  !empty($request->workdate) ? date('Y-m-d', strtotime($request->workdate)) : '';
        $productissueuni = ManpowerLog::where('company_id',$companyid)->where('work_date',$startdate)->orderBy('inc','desc')->max('inc');
        $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
        $unique_id = 'LA-'.$newid;

        $timeLog = new ManpowerLog();
        $timeLog->unique_id = $unique_id;
        $timeLog->company_id = $companyid;
        $timeLog->contractor = $request->contractor ?: 0;
        $timeLog->manpower_category = $request->manpowercategory;
        $timeLog->manpower = $request->manpower;
        $timeLog->workinghours = $request->workinghours;
        $timeLog->work_date = $startdate;
        $timeLog->description = $request->description;
        if ($request->task_id) {
            $task = Task::find($request->task_id);
            $timeLog->project_id = $task->project_id;
            $timeLog->title_id = $task->title ?: 0;
            $timeLog->activity_id = $task->activity_id ?: 0;
            $timeLog->costitem_id = $task->cost_item_id;
        } else {
            $timeLog->project_id = $request->project_id;
            $timeLog->title_id = $request->title_id ?: 0;
            $timeLog->activity_id = $request->activity ?: 0;
            $timeLog->costitem_id = $request->costitem ?: 0;
        }
        $timeLog->segment_id = $request->segment_id ?: 0;
        $timeLog->task_id = $request->task_id ?: 0;
        $timeLog->added_by = $this->user->id;
        $timeLog->save();
        return Reply::dataOnly(['manpowerID' => $timeLog->id]);
    }

    public function storedata(Request $request)
    {
        $user = $this->user;
        $projectid = $request->project_id;
        if(!empty($request->project_id)){
            $projectid = Project::find($request->project_id);
            $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $startdate =  !empty($request->workdate) ? date('Y-m-d', strtotime($request->workdate)) : '';
        $productissueuni = ManpowerLog::where('company_id',$companyid)->where('work_date',$startdate)->orderBy('inc','desc')->max('inc');
        $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
        $unique_id = 'LA-'.$newid;

        $timeLog = new ManpowerLog();
        $timeLog->unique_id = $unique_id;
        $timeLog->company_id = $companyid;
        $timeLog->contractor = $request->contractor ?: 0;
        $timeLog->manpower_category = $request->category ?: 0;
        $timeLog->manpower = $request->manpower ?: 0;
        $timeLog->workinghours = $request->workinghours ?: 0;
        $timeLog->work_date = $startdate;
        $timeLog->description = $request->description;
        if (!empty($request->task_id)) {
            $task = Task::find($request->task_id);
            $timeLog->project_id = $task->project_id ?: 0;
            $timeLog->title_id = $task->title ?: 0;
            $timeLog->costitem_id = $task->cost_item_id ?: 0;
        } else {
            $timeLog->project_id = $request->project_id ?: 0;
            $timeLog->title_id = $request->title_id ?: 0;
            $timeLog->costitem_id = $request->costitem ?: 0;
        }
        $timeLog->segment_id = $request->segment_id ?: 0;
        $timeLog->task_id = $request->task_id ?: 0;
        $timeLog->added_by = $this->user->id;
        $timeLog->save();
        return Reply::dataOnly(['manpowerID' => $timeLog->id]);
    }

    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'manpowercategory' => 'required',
                'manpower' => 'required',
                'workinghours' => 'required',
                'workdate' => 'required',
                'project_id' => 'required',
                'activity' => 'required',
                'costitem' => 'required'],

            ['manpowercategory.required' => 'Manpower category field is required',
                'manpower.required' => 'Manpower field is required',
                'workinghours.required' => 'Working hours field is required',
                'workdate.required' => 'Work date is required',
                'project_id.required' => 'Project field is required',
                'activity.required' => 'Activity field is required',
                'costitem.required' => 'Task field is required']
        );
        $user = $this->user;
        $projectid = $request->project_id;
        if(!empty($request->project_id)){
            $projectid = Project::find($request->project_id);
            $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $timeLog = ManpowerLog::findorfail($id);
        $timeLog->company_id = $companyid;
        $timeLog->contractor = $request->contractor;
        $timeLog->manpower_category = $request->manpowercategory;
        $timeLog->manpower = $request->manpower;
        $timeLog->workinghours = $request->workinghours;
        $timeLog->work_date = !empty($request->workdate) ? date('Y-m-d', strtotime($request->workdate)) : '';
        $timeLog->description = $request->description;
        $timeLog->project_id = $request->project_id;
        $timeLog->title_id = $request->title_id;
        $timeLog->activity_id = $request->activity;
        $timeLog->costitem_id = $request->costitem;
        $timeLog->segment_id = $request->segment_id;
        $timeLog->added_by = $this->user->id;
        $timeLog->save();
        return Reply::dataOnly(['manpowerID' => $timeLog->id]);
    }

    public function destroy($id)
    {
        ManpowerLog::destroy($id);
        return Reply::success(__('messages.timeLogDeleted'));
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $storage = storage();
            $companyid = $this->user->company_id;
            foreach ($request->file as $fileData) {
                $file = new ManpowerLogFiles();
                $file->added_by = $this->user->id;
                $file->company_id = $companyid;
                $file->task_id = $request->task_id ?: 0;
                $file->reply_id = $request->reply_id ?: 0;
                $file->manpower_id = $request->manpower_id;
                switch ($storage) {
                    case 'local':
                        $destinationPath = 'uploads/manpower-log-files/' . $request->manpower_id;
                        if (!file_exists('' . $destinationPath)) {
                            mkdir('' . $destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        $filename = $fileData->hashName();
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid . '/manpower-log-files/' . $request->manpower_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'task-files')
                            ->first();

                        if (!$dir) {
                            Storage::cloud()->makeDirectory('manpower-log-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->manpower_id)
                            ->first();

                        if (!$directory) {
                            Storage::cloud()->makeDirectory($dir['path'] . '/' . $request->manpower_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('manpower-log-files/' . $request->manpower_id . '/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/manpower-log-files/' . $request->manpower_id . '/' . $fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }
//        return Reply::redirect(route('admin.man-power-logs.index'), __('modules.projects.projectUpdated'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function stopTimer(Request $request)
    {
        $timeId = $request->timeId;
        $timeLog = ManpowerLog::findOrFail($timeId);
        $timeLog->end_time = Carbon::now();
        $timeLog->edited_by_user = $this->user->id;
        $timeLog->save();

        $timeLog->total_hours = ($timeLog->end_time->diff($timeLog->start_time)->format('%d') * 24) + ($timeLog->end_time->diff($timeLog->start_time)->format('%H'));

        if ($timeLog->total_hours == 0) {
            $timeLog->total_hours = round(($timeLog->end_time->diff($timeLog->start_time)->format('%i') / 60), 2);
        }
        $timeLog->total_minutes = ($timeLog->total_hours * 60) + ($timeLog->end_time->diff($timeLog->start_time)->format('%i'));

        $timeLog->save();

        $this->activeTimers = ManpowerLog::whereNull('end_time')
            ->get();
        $view = view('admin.projects.man-power-logs.active-timers', $this->data)->render();
        return Reply::successWithData(__('messages.timerStoppedSuccessfully'), ['html' => $view, 'activeTimers' => count($this->activeTimers)]);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $id
     */
    public function export($startDate, $endDate, $id, $employee = null)
    {

        $projectName = 'projects.project_name'; // Set default name for select in mysql
        $timeLogs = ManpowerLog::join('users', 'users.id', '=', 'manpower_logs.added_by');

        $this->logTimeFor = LogTimeFor::first();

        // Check for apply join Task Or Project
        if ($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task') {
            $timeLogs = $timeLogs->join('tasks', 'tasks.id', '=', 'manpower_logs.task_id');
            $projectName = 'tasks.heading as project_name';
        } else {
            $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'manpower_logs.project_id');
        }

        // Fields selecting  For excel
        $timeLogs = $timeLogs->select('manpower_logs.id', 'users.name', $projectName, 'manpower_logs.start_time', 'manpower_logs.end_time', 'manpower_logs.memo', 'manpower_logs.total_minutes');

        // Condition according start_date
        if (!is_null($startDate)) {
            $timeLogs->where(DB::raw('DATE(manpower_logs.`start_time`)'), '>=', "$startDate");
        }

        // Condition according start_date
        if (!is_null($endDate)) {
            $timeLogs->where(DB::raw('DATE(manpower_logs.`end_time`)'), '<=', "$endDate");
        }

        // Condition according employee
        if (!is_null($employee) && $employee !== 'all') {
            $timeLogs->where('manpower_logs.added_by', $employee);
        }

        // Condition according select id
        if (!is_null($id) && $id !== 'all') {
            if ($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task') {
                $timeLogs->where('manpower_logs.task_id', '=', $id);
            } else {
                $timeLogs->where('manpower_logs.project_id', '=', $id);
            }
        }
        $attributes = ['total_minutes', 'duration', 'timer'];
        $timeLogs = $timeLogs->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'User', 'Log For', 'Start Time', 'End Time', 'Memo', 'Total Hours'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($timeLogs as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('timelog', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Time Log');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('time log file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });

        })->download('xlsx');
    }

    public function exportReport($projectId = null, $subproject = null, $segment = null, $curmonth = null, $contractor = null)
    {

        $user = $this->user;
        $datesarray[] = '';
        $day = date('t', strtotime($curmonth));
        $month = date('m', strtotime($curmonth));
        $year = date('Y', strtotime($curmonth));
        for ($x = 1; $x <= $day; $x++) {
            $date = date('d M Y', mktime(0, 0, 0, $month, $x, $year));
            $datesarray[] = $date;
        }
        $exportArray[] = $datesarray;
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        $manpowercategoryarray = \App\ManpowerCategory::where('company_id', $user->company_id)->get();
        if (!empty($manpowercategoryarray)) {
            foreach ($manpowercategoryarray as $manpowercategory) {
                $listarray = array();
                $listarray[] = ucwords($manpowercategory->title);
                for ($x = 1; $x <= $day; $x++) {
                    $date = date('Y-m-d', mktime(0, 0, 0, $month, $x, $year));
                    $mancount = \App\ManpowerLog::where('manpower_category', $manpowercategory->id)->where('work_date', $date);
                    if (!empty($contractor)) {
                        $mancount = $mancount->where('contractor', $contractor);
                    }
                    if (!empty($projectId)) {
                        $mancount = $mancount->where('project_id', $projectId);
                    }
                    if (!empty($subproject)) {
                        $mancount = $mancount->where('title_id', $subproject);
                    }
                    if (!empty($segment)) {
                        $mancount = $mancount->where('segment_id', $segment);
                    }
                    $listarray[] = $mancount->count();
                }
                $exportArray[] = $listarray;
            }
        }

        // Generate and return the spreadsheet
        Excel::create('manpowerreport', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Labour Attendance report');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('Labour Attendance report');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));

                });

            });

        })->download('xlsx');
    }

    public function manpowerReport()
    {
        $user = $this->user;
        $contractors = Employee::getAllContractors($user);
        $this->projectarray = $this->projectslist;
        $this->contractorsarray = $contractors;
        $this->manpowercategoryarray = ManpowerCategory::where('company_id', $user->company_id)->get();
        $dataarray = array();
        $companyid = $user->company_id;
        $dataarray['manpowercategoryarray'] = ManpowerCategory::where('company_id', $companyid)->get();
        $dataarray['contractor'] =  '';
        $dataarray['curmonth'] = date('m');
        $dataarray['curyear'] = date('Y');
        $dataarray['companyid'] = $companyid;
        $messageview = View::make('admin.manpower-logs.reporthtml', $dataarray);
        $mailcontent = $messageview->render();
        $this->reporthtml = $mailcontent;
        return view('admin.manpower-logs.report', $this->data);
    }

    public function manpowerReportHtml(Request $request)
    {
        $user = $this->user;
        if(!empty($request->project_id)){
        $projectid = Project::find($request->project_id);
        $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $dataarray = array();
        $dataarray['manpowercategoryarray'] = ManpowerCategory::where('company_id', $companyid)->get();
        $dataarray['projectid'] = $request->project_id;
        $dataarray['subprojectid'] = $request->subproject_id;
        $dataarray['segmentid'] = $request->segment_id;
        $dataarray['contractor'] = $request->contractor;
        $dataarray['curmonth'] = $request->month ?: date('m');
        $dataarray['curyear'] = $request->year ?: date('Y');
        $dataarray['companyid'] = $companyid;
        $messageview = View::make('admin.manpower-logs.reporthtml', $dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }

    public function manpowerReportPdf(Request $request)
    {
        $contractors = User::find($request->contractor);
        $dataarray = array();
        $exporttype = $request->exporttype;
        $this->projectid = $request->project_id;
        $this->subprojectid = $request->subproject_id;
        $this->segmentid = $request->segment_id;
        $this->contractor = $contractors;
        $this->curmonth = $request->month ?: date('m');
        $this->curyear = $request->year ?: date('Y');
        if ($exporttype == 'pdf') {
            $pdf = PDF::loadView('admin.manpower-logs.reporthtml', $this->data)->setPaper('a4', 'landscape');
            return $pdf->download('progress-report.pdf');
        }
        if ($exporttype == 'excell') {
            $report = View::make('admin.manpower-logs.reportexcell', $this->data)->render();
            $exportArray = json_decode($report);
            Excel::create('Man-power-report', function ($excel) use ($exportArray) {
                $excel->setTitle('Man power report');
                $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
                $excel->setDescription('Progress report');
                $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                    $sheet->fromArray($exportArray, null, 'A1', false, false);
                    $sheet->row(1, function ($row) {
                        $row->setFont(array(
                            'bold' => true
                        ));
                    });
                });
            })->download('xlsx');
        }
    }

    public function reply($id)
    {
        $this->manpower = ManpowerLog::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = ManpowerLogFiles::where('manpower_id', $id)->where('reply_id', '0')->get();
        $this->replies = ManpowerLogReply::where('manpower_id', $id)->get();
        return view('admin.manpower-logs.reply', $this->data);
    }

    public function replyPost(Request $request, $id)
    {
        $user = $this->user;
        $mentionusers = !empty($request->mentionusers) ? array_unique(array_filter(explode(',', $request->mentionusers))) : null;
        $manpower = ManpowerLog::find($id);

        $pi = new ManpowerLogReply();
        $pi->company_id = $user->company_id;
        $pi->comment = $request->comment;
        $pi->manpower_id = $manpower->id;
        $pi->added_by = $this->user->id;
        $pi->mentionusers = !empty($mentionusers) ? implode(',', $mentionusers) : null;
        $pi->save();
        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $pi->id;
        $createlog->module = 'manpower_reply';
        $createlog->modulename = 'manpower_comment';
        $createlog->project_id = $manpower->project_id ?: '';
        $createlog->subproject_id = $manpower->title ?: '';
        $createlog->segment_id = $manpower->segment ?: '';
        $createlog->heading = $request->comment;
        if (empty($manpowerid)) {
            $createlog->description = 'Manpower log reply created by ' . $user->name . ' for the project ' . get_project_name($manpower->project_id);
        } else {
            $createlog->description = 'Manpower log reply updated by ' . $user->name . ' for the project ' . get_project_name($manpower->project_id);
        }
        $createlog->medium = 'web';
        $createlog->mentionusers = !empty($mentionusers) ? implode(',', $mentionusers) : null;
        $createlog->save();

        return Reply::dataOnly(['manpowerID' => $manpower->id, 'replyID' => $pi->id]);
    }
    public function addRow(Request $request){
        $user = $this->user;
        $count = $request->count;
        $dataarray = array();
        $companyid = $user->company_id;
        $dataarray['count'] = $count;
        $dataarray['companyid'] = $companyid;
        $dataarray['manpowercategoryarray'] = ManpowerCategory::where('company_id', $companyid)->get();;
        $messageview = View::make('admin.manpower-logs.addrow', $dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    /*
     * workerlist
     * */
    public function workerList(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $contractor = $request->contractor;
        $contractoruser = get_employee_userid($contractor);
        $startdate = !empty($request->startdate) ? date('Y-m-d', strtotime($request->startdate)) : '';
        $projectdetails = Project::find($projectid);
       /* $manpower = ManpowerLog::where('company_id',$projectdetails->company_id)->where('project_id',$projectdetails->id)->where('contractor',$contractoruser)->where('work_date',$startdate)->first();*/
        $data = array();
        $data['user'] = $user;
        $data['startdate'] = $startdate;
        $data['project'] = $projectdetails;
        if(!empty($contractor)){
            $data['contractorarray'] = Employee::where('company_id',$projectdetails->company_id)->where('id',$contractor)->where('user_type','contractor')->get();
        }else{
            $data['contractorarray'] = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->get();
        }
        $data['mancategoryarray'] = ManpowerCategory::where('company_id',$projectdetails->company_id)->get();
        return view('admin.manpower-logs.workers_list', $data)->render();
    }
    public function workerCreate(Request $request,$id){
        $id = explode('|',$id);
        $projectid = $id[0];
        $contractor = $id[1];
        $data = array();
        $projectdetails = Project::find($projectid);
        $data['project'] = $projectdetails;
        $data['contractor'] = Employee::find($contractor);
        $data['mancategoryarray'] = ManpowerCategory::where('company_id',$projectdetails->company_id)->get();
        $data['contractorarray'] = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->get();
        return view('admin.manpower-logs.create-worker', $data);
    }
    /*
     * workerattendance
     * */
    public function workerAttendance(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $contractor = $request->contractor;
        if($contractor>0){
            $contractoruser = get_employee_userid($contractor);
        }else{
            $contractoruser = 0;
        }
        $startdate = !empty($request->startdate) ? date('Y-m-d', strtotime($request->startdate)) : '';
        $workerid = $request->workerid;
        $projectdetails = AppProject::where('id',$projectid)->first();
        $companyid = $projectdetails->company_id;
        $workers =  Workers::find($workerid);

        $manpowerattendance = ManpowerLog::where('company_id',$companyid)->where('project_id',$projectid)->where('contractor',$contractoruser)->where('work_date',$startdate)->where('worker_id',$workers->id)->first();
        if(empty($manpowerattendance)){

            $productissueuni = ManpowerLog::where('company_id',$companyid)->where('work_date',$startdate)->orderBy('inc','desc')->max('inc');
            $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
            $unique_id = 'LA-'.$newid;
            $manpowerattendance = new ManpowerLog();
            $manpowerattendance->unique_id = $unique_id;
            $manpowerattendance->company_id = $companyid;
            $manpowerattendance->added_by = $user->id;
            $manpowerattendance->contractor = $contractoruser ?: 0;
            $manpowerattendance->work_date =$startdate;
            $manpowerattendance->project_id = $projectid;
            $manpowerattendance->worker_id = $workers->id;
            $manpowerattendance->title_id =  0;
            $manpowerattendance->task_id =  0;
            $manpowerattendance->segment_id =  0;
            $manpowerattendance->activity_id =  0;
            $manpowerattendance->costitem_id = 0;
            $manpowerattendance->manpower_category = $workers->category;
            $manpowerattendance->manpower = 1;
            $manpowerattendance->salaryperday = $workers->salary;
            $manpowerattendance->salarytype = $workers->salarytype;
            $manpowerattendance->hourspershift = $workers->workinghours;
            $manpowerattendance->workinghours = $workers->workinghours;
            $manpowerattendance->workingminutes = $workers->workingminutes;
            $manpowerattendance->save();
        }
        return Reply::dataOnly(['manpowerID' => $manpowerattendance->id,'workerID' => $workers->id]);
    }

    public function workerAttendancePost(Request $request){
        $user = $this->user;
        $workerattendance = $request->workerattendance;
        $noofworkers = $request->noofworkers;
        $workinghours = $request->workinghours;
        $shift = $request->shift;


        $workerattendance = ManpowerLog::find($workerattendance);
        $workerattendance->manpower = $noofworkers;
        $workerattendance->shift = $shift;
        $workerattendance->workinghours = $workinghours;
        if(!empty($request->workerid)){
            $workerid = $request->workerid;
            $workersinfo = Workers::find($workerid);
            $workerattendance->salaryperday = $workersinfo->salary;
            $workerattendance->hourspershift = $workersinfo->workinghours;
        }
        $workerattendance->save();

        $totalprice = labour_totalprice($workerattendance);
        $workerattendance->totalprice = $totalprice;
        $workerattendance->save();

        return Reply::dataOnly(['attendanceid' => $workerattendance->id,'price' => $totalprice]);
    }
    public function labourAttendancePost(Request $request){
        $user = $this->user;
        $workerid = $request->workerid;
        $workerattendance = $request->workerattendance;
        $noofworkers = $request->noofworkers;
        $workinghours = $request->workinghours;
        $shift = $request->shift;
        $salarytype = $request->salarytype;

        $workersinfo = Workers::find($workerid);
        $workerattendance = ManpowerLog::find($workerattendance);
        $workerattendance->manpower = $noofworkers;
        $workerattendance->shift = $shift;
        $workerattendance->workinghours = $workinghours;
        $workerattendance->salaryperday = $workersinfo->salary;
        $workerattendance->salarytype = $salarytype;
        $workerattendance->hourspershift = $workersinfo->workinghours;
        $workerattendance->save();

        $totalprice = labour_totalprice($workerattendance);
        $workerattendance->totalprice = $totalprice;
        $workerattendance->save();

        return $totalprice;
    }
    /*
     * workercreateonetimepost
     * */
    public function workerCreateOneTimePost(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $empid =  $request->empid;
        $contractor =  $request->contractor;
        $category = $request->category;
        $workinghours = $request->workinghours;
        $salary = $request->salary;
        $salarytype = $request->salarytype;
        $startdate = !empty($request->startdate) ? date('Y-m-d', strtotime($request->startdate)) : '';
        $count = $request->count;
        $projectdetails = AppProject::find($projectid);
        $companyid = $projectdetails->company_id;

        $manpowerattendance = ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('contractor',$contractor)->where('work_date',$startdate)->where('worker_id','0')->first();
        if(empty($manpowerattendance)){
            $productissueuni = ManpowerLog::where('company_id',$companyid)->where('work_date',$startdate)->orderBy('inc','desc')->max('inc');
            $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
            $unique_id = 'LA-'.$newid;

            $manpowerattendance = new ManpowerLog();
            $manpowerattendance->unique_id = $unique_id;
            $manpowerattendance->company_id = $companyid;
            $manpowerattendance->added_by = $user->id;
            $manpowerattendance->contractor = $contractor ?: 0;
            $manpowerattendance->work_date =$startdate;
            $manpowerattendance->project_id = $projectdetails->id;
            $manpowerattendance->worker_id = 0;
            $manpowerattendance->title_id =  0;
            $manpowerattendance->task_id =  0;
            $manpowerattendance->segment_id =  0;
            $manpowerattendance->activity_id =  0;
            $manpowerattendance->costitem_id = 0;
        }
        $manpowerattendance->description = $request->description;
        $manpowerattendance->manpower_category = $category;
        $manpowerattendance->salarytype = $salarytype;
        $manpowerattendance->workinghours = $workinghours;
        $manpowerattendance->hourspershift = $workinghours;
        $manpowerattendance->salaryperday = $salary;
        $manpowerattendance->save();
        $totalprice = labour_totalprice($manpowerattendance);
        $manpowerattendance->totalprice = $totalprice;
        $manpowerattendance->save();

        $data['count'] = $count;
        $data['manpowerattendance'] = $manpowerattendance;
        return $totalprice;
    }
    public function workerCreateOneTime(Request $request,$id){
        $id = explode('|',$id);
        $projectid = $id[0];
        $contractor = $id[1];
        $count = $id[2];
        $startdate = $id[3];
        $count = $count+1;
        $data = array();
        $projectdetails = Project::find($projectid);
        $data['project'] = $projectdetails;
        $data['contractor'] = Employee::find($contractor);
        $data['startdate'] = $startdate;
        $data['mancategoryarray'] = ManpowerCategory::where('company_id',$projectdetails->company_id)->get();
        $data['contractorarray'] = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->get();
        $data['count'] = $count;
        return view('admin.manpower-logs.create-worker-one-time', $data);
    }
    /*
     * workercreatepost
     * */
    public function workerCreatePost(Request $request){
        $user = $this->user;
        $projectid = $request->projectid;
        $name = $request->workername ?: '';
        $contractor = explode('|',$request->contractor);
        $empid = $contractor[0];
        $contractorid = $contractor[1];
        $category = $request->category;
        $workinghours = $request->workinghours;
        $salary = $request->salary;
        $salarytype = $request->salarytype;

        $projectdetails = AppProject::where('id',$projectid)->first();
        $usercompany = $projectdetails->company_id;

        if(!empty($name)){
            if($empid=='0'){
                $employee = new Employee();
                $employee->company_id = $usercompany;
                $employee->added_by = $this->user->id;
                $employee->user_id = 0;
                $employee->name = $name;
                $employee->email = '';
                $employee->mobile = '';
                $employee->user_type = 'employee';
                $employee->save();
            }
        }

        $employee = new Workers();
        $employee->company_id = $usercompany;
        $employee->added_by = $user->id;
        $employee->project_id = $projectdetails->id;
        if(!empty($name)){
            $employee->name = $name;
            $employee->type = 'worker';
        }else{
            $employee->type = 'category';
        }
        $employee->emp_id = trim($empid);
        $employee->contractor = trim($contractorid);
        $employee->category = $category;
        $employee->workinghours = $workinghours;
        $employee->salary = $salary;
        $employee->salarytype = $salarytype;
        $employee->save();

        return Reply::success('Worker created successfully');
    }
    public function removeAttendance(Request $request){
        $attendid = $request->attendid;
        ManpowerLog::where('id',$attendid)->delete();
        return 'success';
    }
    /*
     * workerallowncess
     * */
    public function workerAllowncess(Request $request, $id){
        $attendance = ManpowerLog::find($id);
        $data['attendance'] = $attendance;
         return view('admin.manpower-logs.worker-allowances', $data);
    }
    public function workerAllowncessPost(Request $request){

        $attendanceid = $request->attendanceid;
        $attendance = ManpowerLog::find($attendanceid);
        $attendance->overtimehours = $request->overtimehours;
        $attendance->overtimeamount = $request->overtimeamount;
        $attendance->overtimetotal = $request->overtimetotal;
        $attendance->latefinehours = $request->latefinehours;
        $attendance->latefineamount = $request->latefineamount;
        $attendance->latefinetotal = $request->latefinetotal;
        $attendance->allowances = !empty($request->allowances) ? json_encode($request->allowances) : '';
        $attendance->deductions = !empty($request->deductions) ? json_encode($request->deductions) : '';
        $attendance->description = $request->description;
        $attendance->save();

        $totalprice = labour_totalprice($attendance);
        $attendance->totalprice = $totalprice;
        $attendance->save();

        return Reply::dataOnly(['manpowerid' => $attendance->id,'price' => $totalprice]);
    }
}
