<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\Tasks\StoreTask;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskReminder;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\PunchItem;
use App\PunchItemFiles;
use App\PunchItemReply;
use App\Submittals;
use App\SubmittalsContractors;
use App\SubmittalsFile;
use App\SubmittalsWorkflow;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\EmployeeDocs;
use App\Http\Requests\EmployeeDocs\CreateRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ManageSubmittalsController extends AdminBaseController
{
    use ProjectProgress;

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Submittals';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
    }
    public function index()
    {
        $userdetails = $this->user;
        $projectlist = explode(',',$userdetails->projectlist);
        $this->user = $userdetails;
        $this->projects = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->employees =Employee::getAllEmployees($userdetails);
        $this->taskBoardStatus = TaskboardColumn::all();
        $submittalsarray = $submittalslist = $submittalsusers = array();
        $submittalslist = Submittals::where('private','0')->pluck('id')->toArray();
        $submittalsusers = Submittals::where('private','1')->where(function($q) use ($userdetails) {
            $q->where('submittal_manager', $userdetails->id)
                ->orWhere('rec_contractor',  $userdetails->id)
                ->orWhere('received_from',  $userdetails->id)
                ->orwhereRaw('FIND_IN_SET(?,distribution)', [$userdetails->id]);
        })->pluck('id')->toArray();
        $submittalsarray = array_filter(array_unique(array_merge($submittalslist,$submittalsusers)));
        $this->items = Submittals::whereIn('id',$submittalsarray)->orderBy('issuedate','asc')->get();
        return view('admin.submittals.index', $this->data);
    }

    public function submittalsData(Request $request)
    {
//        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();

        $tasks = Submittals::join('users', 'users.id', '=', 'submittals.user_id')
            ->select('submittals.*', 'users.name', 'users.image');

        $tasks->get();

        return DataTables::of($tasks)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $action = '<a href="' . route('admin.submittals.edit', $row->id) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                return $action;
            })
            ->editColumn('due_date', function ($row) {
                if ($row->due_date->isPast()) {
                    return '<span class="text-danger">' . $row->due_date->format($this->global->date_format) . '</span>';
                }
                return '<span class="text-success">' . $row->due_date->format($this->global->date_format) . '</span>';
            })
            ->editColumn('name', function ($row) {
                return ($row->image) ? '<img src="' . asset('user-uploads/avatar/' . $row->image) . '"
                alt="user" class="img-circle" width="30"> ' . ucwords($row->name) : '<img src="' . asset('default-profile-2.png') . '"
                alt="user" class="img-circle" width="30"> ' . ucwords($row->name);
            })
            ->editColumn('clientName', function ($row) {
                return ($row->clientName) ? ucwords($row->clientName) : '-';
            })
            ->editColumn('created_by', function ($row) {
                if (!is_null($row->created_by)) {
                    return ($row->created_image) ? '<img src="' . asset('user-uploads/avatar/' . $row->created_image) . '"
                    alt="user" class="img-circle" width="30"> ' . ucwords($row->created_by) : '<img src="' . asset('default-profile-2.png') . '"
                    alt="user" class="img-circle" width="30"> ' . ucwords($row->created_by);
                }
                return '-';
            })
            ->editColumn('heading', function ($row) {
                return '<a href="javascript:;" data-task-id="' . $row->id . '" class="show-task-detail">' . ucfirst($row->heading) . '</a>';
            })
            ->editColumn('column_name', function ($row) {
                return '<label class="label" style="background-color: ' . $row->label_color . '">' . $row->column_name . '</label>';
            })
            ->editColumn('project_name', function ($row) {
                if (is_null($row->project_id)) {
                    return "";
                }
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->rawColumns(['column_name', 'action', 'project_name', 'clientName', 'due_date', 'name', 'created_by', 'heading'])
            ->removeColumn('project_id')
            ->removeColumn('image')
            ->removeColumn('created_image')
            ->removeColumn('label_color')
            ->make(true);
    }
    public function editSubmittals($id)
    {
        $userdetails = $this->user;
        $submittals = Submittals::findOrFail($id);

        $this->submittaltype = 'update';
        $this->submittals = $submittals;   $projectlist = explode(',',$userdetails->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->employees =Employee::getAllEmployees($userdetails);
        $this->files = SubmittalsFile::where('submittals_id',$id)->get();
        $this->titlelist = Title::where('project_id',$submittals->projectid)->get();
        $this->costitemlist =  ProjectCostItemsProduct::where('project_id',$submittals->projectid)->where('title',$submittals->titleid)->pluck('cost_items_id');
        $this->submittedworkflowarray =  SubmittalsWorkflow::where('submittals_id',$submittals->id)->get();
        return view('admin.submittals.edit', $this->data);
    }

    public function updateSubmittals(Request $request, $id)
    {
        $task = Submittals::findOrFail($id);
        $task->title = $request->title;
        $task->submittal_manager = $request->submittal_manager;
        $task->status = $request->status;
        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->description = $request->description;
        $task->private = $request->private ?: 0;
        $task->draft = $request->draft ?: 0;
        $task->added_by = $this->user->id;
        $task->company_id = $this->user->company_id ?: 0;
        $task->projectid = $request->project_id ?: '';
        $task->titleid = $request->title_id ?: '';
        $task->costitemid = $request->costitem ?: '';
        $task->type = $request->type ?: '';
        $task->leadtime = $request->leadtime ?: '';
        $task->designreviewtime = $request->designreviewtime ?: '';
        $task->internalreviewtime = $request->internalreviewtime ?: '';
        $task->scheduletask = $request->scheduletask ?: '';
        $task->submitdate = carbon_date($request->submitdate);
        $task->issuedate = carbon_date($request->issuedate);
        $task->receiveddate =  carbon_date($request->receiveddate);
        $task->finalduedate = carbon_date($request->finalduedate);
        $task->onsitedate = carbon_date($request->onsitedate);
        $task->planreturndate = carbon_date($request->planreturndate);
        $task->planinternaldate = carbon_date($request->planinternaldate);
        $task->plansubmitdate = carbon_date($request->plansubmitdate);
        $task->anticipateddeliverydate = carbon_date($request->anticipateddeliverydate);
        $task->confirmdeliverydate = carbon_date($request->confirmdeliverydate);
        $task->actualdeliverydate = carbon_date($request->actualdeliverydate);
        $task->save();
        $workflowusers = $request->workflowusers ?: array();
        $workflowrole = $request->role ?: array();
        $workflowduedate = $request->duedate ?: array();
        if(count($workflowusers)>0){
            $x=0;
                foreach($workflowusers as $id => $value) {
                $submitalworkflow = SubmittalsWorkflow::find($id);
                if(!empty($submitalworkflow->id)){
                    $submitalworkflow->company_id = $this->user->company_id ?: 0;
                    $submitalworkflow->added_by = $this->user->id;
                    $submitalworkflow->user_id = $workflowusers[$id];
                    $submitalworkflow->role = $workflowrole[$id];
                    $submitalworkflow->duedate = carbon_date($workflowduedate[$id]);
                     $submitalworkflow->save();

                }else{
                    if(!empty($workflowusers[$x])&&!empty($workflowrole[$x])&&!empty($workflowduedate[$x])){
                        $submitalworkflow = new SubmittalsWorkflow();
                        $submitalworkflow->submittals_id = $task->id;
                        $submitalworkflow->added_by = $this->user->id;
                        $submitalworkflow->company_id = $this->user->company_id;
                    }
                    $submitalworkflow->user_id = $workflowusers[$x];
                    $submitalworkflow->role = $workflowrole[$x];
                    $submitalworkflow->duedate = carbon_date($workflowduedate[$x]);
                     $submitalworkflow->save();
                    $x++;
                }
            }
        }
        return Reply::dataOnly(['submittalsID' => $task->id]);
    }

    public function revisionSubmittals($id)
    {
        $submittals = Submittals::findOrFail($id);
       /* if ( $submittals->submittal_manager !=  Auth::user()->id){
            return redirect(route('admin.submittals.index'));
        }*/
        $this->submittaltype = 'revision';
        $this->submittals = $submittals;
        $this->submittals = $submittals;   $projectlist = explode(',',$userdetails->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->employees =Employee::getAllEmployees($userdetails);
        $this->files = SubmittalsFile::where('submittals_id',$id)->get();
        $this->titlelist = Title::where('project_id',$submittals->projectid)->get();
        $this->costitemlist =  ProjectCostItemsProduct::where('project_id',$submittals->projectid)->where('title',$submittals->titleid)->pluck('cost_items_id');
        $this->submittedworkflowarray =  SubmittalsWorkflow::where('submittals_id',$submittals->id)->get();
        return view('admin.submittals.edit', $this->data);
    }
    public function destroy(Request $request, $id)
    {
        $taskFiles = SubmittalsFile::where('submittals_id', $id)->get();

        foreach ($taskFiles as $file) {
            Files::deleteFile($file->hashname, 'submittals-files/' . $file->id);
            $file->delete();
        }
        SubmittalsWorkflow::where('submittals_id',$id)->delete();
        Submittals::destroy($id);
        //calculate project progress if enabled

        return Reply::success(__('Submittals deleted successfully'));
    }

    public function createSubmittals()
    {
        $userdetails = $this->user;
        $projectlist = explode(',',$userdetails->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->employees =Employee::getAllEmployees($userdetails);
        $submittalscount = Submittals::where('revisesubmittals_id','0')->max('number');
        $this->newnumber = $submittalscount+1;
        return view('admin.submittals.create', $this->data);
    }

    public function projectTitles(Request $request){
        $projectid = $request->projectid;
        $titleoption = '<option value="">Select Title</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
        }
        return $titleoption;
    }
    public function costitemBytitle(Request $request){
        $projectid = $request->projectid;
        $title = $request->title;
        $titleoption = '<option value="">Select Task</option>';
        if($projectid){
            $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->pluck('cost_items_id');
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item.'">'.get_cost_name($item).'</option>';
            }
        }
        return $titleoption;
    }

    public function storeSubmittals(Request $request)
    {

        $task = new Submittals();
        $task->title = $request->title;
        $task->number = $request->number;
        $task->revision = $request->revision;
        $task->submittal_manager = $request->submittal_manager;
        $task->ballincourt =  0;
       $task->status = $request->status;
        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->description = $request->description;
        $task->draft = $request->draft ?: 0;
        $task->private = $request->private ?: 0;
        $task->added_by = $this->user->id;
        $task->projectid = $request->project_id ?: '';
        $task->titleid = $request->title_id ?: '';
        $task->costitemid = $request->costitem ?: '';
        $task->type = $request->type ?: '';
        $task->leadtime = $request->leadtime ?: '';
        $task->designreviewtime = $request->designreviewtime ?: '';
        $task->internalreviewtime = $request->internalreviewtime ?: '';
        $task->scheduletask = $request->scheduletask ?: '';
        $task->submitdate = carbon_date($request->submitdate);
        $task->issuedate = carbon_date($request->issuedate);
        $task->receiveddate =  carbon_date($request->receiveddate);
        $task->finalduedate = carbon_date($request->receiveddate);
        $task->onsitedate = carbon_date($request->onsitedate);
        $task->planreturndate = carbon_date($request->planreturndate);
        $task->planinternaldate = carbon_date($request->planinternaldate);
        $task->plansubmitdate = carbon_date($request->plansubmitdate);
        $task->anticipateddeliverydate = carbon_date($request->anticipateddeliverydate);
        $task->confirmdeliverydate = carbon_date($request->confirmdeliverydate);
        $task->actualdeliverydate = carbon_date($request->actualdeliverydate);
        $task->revisesubmittals_id = 0;
        $task->save();
        $workflowusers = $request->workflowusers ?: array();
        $workflowrole = $request->role ?: array();
        $workflowduedate = $request->duedate ?: array();
        if(count($workflowusers)>0){
        for($x=0;$x<=count($workflowusers);$x++) {
                if(!empty($workflowusers[$x])&&!empty($workflowrole[$x])&&!empty($workflowduedate[$x])){
                    $submitalworkflow = new SubmittalsWorkflow();
                    $submitalworkflow->added_by = $this->user->id;
                    $submitalworkflow->company_id = $this->user->company_id;
                    $submitalworkflow->submittals_id = $task->id;
                    $submitalworkflow->user_id = $workflowusers[$x];
                    $submitalworkflow->role = $workflowrole[$x];
                    $submitalworkflow->duedate = carbon_date($workflowduedate[$x]);
                    if($workflowduedate[0]){
                        $submitalworkflow->sentdate = date('Y-m-d');
                    }
                    $submitalworkflow->save();
                }
            }
        }
        return Reply::dataOnly(['submittalsID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskCreatedSuccessfully'));
    }
    public function reviseSubmittalsPost($id)
    {
        $prevsubmital =  Submittals::find($id);

        $submittalsrevice = Submittals::where('revisesubmittals_id',$prevsubmital->id)->max('revision');
        $newrevisecount = $submittalsrevice+1;
        $task = new Submittals();
        $task->title = $prevsubmital->title;
        $task->number = $prevsubmital->number;
        $task->revision = $newrevisecount;
        $task->submittal_manager = $prevsubmital->submittal_manager;
        $task->ballincourt =  0;
       $task->status = 'Open';
        $task->rec_contractor = $prevsubmital->rec_contractor;
        $task->received_from = $prevsubmital->received_from;
        $task->distribution = $prevsubmital->distribution;
        $task->location = $prevsubmital->location;
        $task->description = $prevsubmital->description;
        $task->draft = $prevsubmital->draft ?: 0;
        $task->private = $prevsubmital->private ?: 0;
        $task->added_by = $this->user->id;
        $task->projectid = $prevsubmital->project_id ?: '';
        $task->titleid = $prevsubmital->title_id ?: '';
        $task->costitemid = $prevsubmital->costitem ?: '';
        $task->type = $prevsubmital->type ?: '';
        $task->leadtime = $prevsubmital->leadtime ?: '';
        $task->designreviewtime = $prevsubmital->designreviewtime ?: '';
        $task->internalreviewtime = $prevsubmital->internalreviewtime ?: '';
        $task->scheduletask = $prevsubmital->scheduletask ?: '';
        $task->submitdate = $prevsubmital->submitdate;
        $task->issuedate = $prevsubmital->issuedate;
        $task->receiveddate =  $prevsubmital->receiveddate;
        $task->finalduedate = $prevsubmital->receiveddate;
        $task->onsitedate = $prevsubmital->onsitedate;
        $task->planreturndate = $prevsubmital->planreturndate;
        $task->planinternaldate = $prevsubmital->planinternaldate;
        $task->plansubmitdate = $prevsubmital->plansubmitdate;
        $task->anticipateddeliverydate = $prevsubmital->anticipateddeliverydate;
        $task->confirmdeliverydate = $prevsubmital->confirmdeliverydate;
        $task->actualdeliverydate = $prevsubmital->actualdeliverydate;
        $task->revisesubmittals_id = $prevsubmital->id;
        $task->save();
        $workflowarray = SubmittalsWorkflow::where('submittals_id',$prevsubmital->id)->get();
        if(count($workflowarray)>0){
        foreach($workflowarray as $workflow) {
                    $submitalworkflow = new SubmittalsWorkflow();
                    $submitalworkflow->added_by = $this->user->id;
                    $submitalworkflow->company_id = $this->user->company_id;
                    $submitalworkflow->submittals_id = $task->id;
                    $submitalworkflow->user_id = $workflow->user_id;
                    $submitalworkflow->role =  $workflow->role;
                    $submitalworkflow->duedate = $workflow->duedate;
                    $submitalworkflow->save();
            }
        }
        $filesarray = SubmittalsFile::where('submittals_id',$prevsubmital->id)->where('workflow_id','0')->get();
        if(count($filesarray)>0){
        foreach($filesarray as $files) {
                    $submitalfiles = new SubmittalsFile();
                    $submitalfiles->added_by = $this->user->id;
                    $submitalfiles->company_id = $this->user->company_id;
                    $submitalfiles->submittals_id = $task->id;
                    $submitalfiles->user_id = $files->user_id;
                    $submitalfiles->workflow_id = 0;
                    $submitalfiles->filename = $files->filename;
                    $submitalfiles->hashname = $files->hashname;
                    $submitalfiles->size = $files->size;
                    $submitalfiles->dropbox_link = $files->dropbox_link;
                    $submitalfiles->google_url = $files->google_url;
                    $submitalfiles->external_link = $files->external_link;
                    $submitalfiles->external_link_name = $files->external_link_name;
                    $submitalfiles->save();
            }
        }
        return  redirect(route('admin.submittals.revisionSubmittals',[$task->id]));
    }

    public function show($id)
    {
        $this->task = Task::with('board_column')->findOrFail($id);
        $view = view('admin.submittals.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }


    /**
     * @param $startDate
     * @param $endDate
     * @param $projectId
     * @param $hideCompleted
     */
    public function export($startDate, $endDate, $projectId, $hideCompleted)
    {

        $tasks = PunchItem::join('users', 'users.id', '=', 'submittals_item.assign_to')
            ->select('submittals_item.*', 'users.name', 'users.image');

//        $tasks->where(function ($q) use ($startDate, $endDate) {
//            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);
//
//            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
//        });
//
//        if ($projectId != 0) {
//            $tasks->where('tasks.project_id', '=', $projectId);
//        }
//
//        if ($hideCompleted == '1') {
//            $tasks->where('tasks.status', '=', 'incomplete');
//        }

        $attributes =  ['image', 'due_date'];

        $tasks = $tasks->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project', 'Title', 'Assigned To', 'Status', 'Due Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($tasks as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $submittals = Submittals::where('id',$request->submittals_id)->first();
                $file = new SubmittalsFile(); 
                $file->company_id = $this->user->company_id;
                $file->user_id = $this->user->id;
                $file->submittals_id = $request->submittals_id;
                $file->workflow_id = $request->workflow_id ?: 0;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/submittals-files/'.$submittals->id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('submittals-files/'.$submittals->id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'submittals-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('submittals-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$submittals->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->submittals_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('submittals-files/'.$submittals->title.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/submittals-files/'.$request->submittals_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->submittals_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('admin.submittals.index'), __('modules.projects.projectUpdated'));
    }

    public function destroyImage(Request $request, $id)
    {
        $file = SubmittalsFile::findOrFail($id);
        $storage = config('filesystems.default');
        switch($storage) {
            case 'local':
                File::delete('user-uploads/submittals-files/'.$id.'/'.$file->hashname);
                break;
            case 's3':
                File::delete('user-uploads/submittals-files/'.$id.'/'.$file->hashname);
                Storage::disk('s3')->delete('submittals-files/'.$id.'/'.$file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('submittals-files/'.$id.'/'.$file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('submittals-files/'.$id.'/'.$file->filename);
                break;
        }
        $file->delete();
        return Reply::successWithData(__('messages.fileDeleted'));
    }

    public function removeFile($id){
        $file = SubmittalsFile::findOrFail($id);
        $storage = config('filesystems.default');
        switch($storage) {
            case 'local':
                File::delete('user-uploads/submittals-files/'.$id.'/'.$file->hashname);
                break;
            case 's3':
                File::delete('user-uploads/submittals-files/'.$id.'/'.$file->hashname);
                Storage::disk('s3')->delete('submittals-files/'.$id.'/'.$file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('submittals-files/'.$id.'/'.$file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('submittals-files/'.$id.'/'.$file->filename);
                break;
        }
        $file->delete();
        return Reply::success(__('image deleted successfully'));
    }

    public function removeWorkflow(Request $request)
    {
       $submittedworkflow = SubmittalsWorkflow::find($request->workflowid);
       if(!empty($submittedworkflow->id)){
           $submittedworkflow->delete();
           return Reply::success(__('Work deleted successfully'));
       }
        return Reply::fail(__('Work not deleted successfully'));
    }

    public function detailsSubmittals($id)
    {
        $user = $this->user;
        $this->submittals = Submittals::findOrFail($id);
        $this->employees = Employee::getAllEmployees($user);
        $this->files = SubmittalsFile::where('submittals_id',$id)->where('workflow_id',0)->get();
        $this->workflowarray = SubmittalsWorkflow::where('submittals_id',$id)->get();
        $this->distributecount = SubmittalsWorkflow::where('submittals_id',$id)->whereNull('response')->count();
        return view('admin.submittals.details', $this->data);
    }
    public function wrkResponse($id)
    {
        $user = $this->user;
        $submitflow = SubmittalsWorkflow::where('id',$id)->first();
        $this->submitwrkflow = $submitflow;
        $this->submittals = Submittals::findOrFail($submitflow->submittals_id);
        $this->employees = Employee::getAllEmployees($user);
        $this->files = SubmittalsFile::where('submittals_id',$id)->get();
        $this->workflowarray = SubmittalsWorkflow::where('submittals_id',$id)->get();
        $this->replies = array();
        $this->responsearray = array();
        return view('admin.submittals.wrkflowresponse', $this->data);
    }

    public function workflowPost(Request $request,$id)
    {
        $pi = SubmittalsWorkflow::find($id);
        $pi->response = $request->response;
        $pi->comments = $request->comment;
        $pi->save();
        return Reply::dataOnly(['submittalsID' => $id,'workflowID' => $pi->id]);
    }
    public function forwardWrkResponsePost(Request $request,$id)
    {
        $pi = SubmittalsWorkflow::find($id);
        $pi->forward_to = $request->forwardto ?: 0;
        $pi->returneddate =  carbon_date($request->returnby);
        $pi->response = 'Forward';
        $pi->comments = $request->comment;
        $pi->save();
        if(!empty($pi->forward_to)&&$pi->forward_to>0){
            $submitalworkflow = new SubmittalsWorkflow();
            $submitalworkflow->submittals_id = $pi->submittals_id;
            $submitalworkflow->user_id = $pi->forward_to;
            $submitalworkflow->role = $request->role;
            $submitalworkflow->duedate = $pi->duedate;
            $submitalworkflow->added_by = $this->user->id;
            $submitalworkflow->company_id = $this->user->company_id;
            $submitalworkflow->comments = "Forwarded by ".$this->user->name;
            $submitalworkflow->save();

            $submitalworkflow = new SubmittalsWorkflow();
            $submitalworkflow->submittals_id = $pi->submittals_id;
            $submitalworkflow->user_id = $pi->user_id;
            $submitalworkflow->role = 'Approver';
            $submitalworkflow->duedate = $pi->duedate;
            $submitalworkflow->added_by = $this->user->id;
            $submitalworkflow->company_id = $this->user->company_id;
            $submitalworkflow->comments = $pi->comments;
            $submitalworkflow->save();
        }
        return Reply::dataOnly(['submittalsID' => $id,'workflowID' => $pi->id]);
    }
    public function distributeSubmittals($id){
        if(empty($id)){
            return redirect(route('admin.submittals.index'));
        }
        $submittals = Submittals::find($id);
        $this->submittals = $submittals;
        $this->workflowarray = SubmittalsWorkflow::where('submittals_id',$submittals->id)->get();
        return view('admin.submittals.distributeworkflow', $this->data);
    }
    public function prepareSubmittals($sid,$id){
        if(empty($id)){
            return redirect(route('admin.submittals.index'));
        }
        $user = $this->user;
        $submittals = Submittals::find($sid);
        $this->submittals = $submittals;
        $this->workflow = SubmittalsWorkflow::where('submittals_id',$submittals->id)->where('id',$id)->first();
        $this->employees = Employee::getAllEmployees($user);
        return view('admin.submittals.preparesubmittals', $this->data);
    }
    public function storePrepareSubmittals(Request $request){
        $submittalsid = $request->submittalsid;
        $workflowid = $request->workflowid;
        $pi = Submittals::find($submittalsid);
        $pi->distributed_to = $request->contractor;
        $pi->status = "Close";
        $pi->distributed_on = date('Y-m-d');
        $pi->save();

        $submitalworkflow = new SubmittalsContractors();
        $submitalworkflow->contractor_id = $request->contractor;
        $submitalworkflow->submittals_id = $pi->id;
        $submitalworkflow->workflow_id = $workflowid;
        $submitalworkflow->added_by = $this->user->id;
        $submitalworkflow->company_id = $this->user->company_id;
        $submitalworkflow->comments = $request->comment;
        $submitalworkflow->save();
        return Reply::dataOnly(['submittalsID' => $pi->id,'contractorID' => $submitalworkflow->id]);
    }
}
