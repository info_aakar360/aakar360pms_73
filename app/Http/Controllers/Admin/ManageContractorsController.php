<?php

namespace App\Http\Controllers\Admin;

use App\AppProject;
use App\ClientDetails;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Admin\Contractors\StoreContractorsRequest;
use App\Http\Requests\Admin\Contractors\UpdateContractorsRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectMember;
use App\ProjectPermission;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\Contractors;
use App\RoleUser;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageContractorsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.contractors';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('contractors', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->contractors = Employee::where('company_id',$user->company_id)->where('user_type','contractor')->get();
        return view('admin.contractors.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = AppProject::whereIn('id',$projectlist)->get();
        return view('admin.contractors.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorsRequest $request)
    {

        $user = $this->user;
        $projectid = $request->project_id;
        $sharetoproject = $request->sharetoproject;
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile;

        if(!checkMobile($mobile)){
            $mobile = str_replace(" ", "", $mobile);
            $mobile = str_replace("-", "", $mobile);
            $len = strlen($mobile);
            $c = $len - 10;
            if($c){
                $mobile = substr($mobile, $c, 10);
            }
        }
        if($mobile==$user->mobile){
            return Reply::error('Member cannot be created with same mobile number');
        }
        if(!empty($projectid)){
            $projectdetails = AppProject::where('id',$projectid)->first();
            $usercompany = $projectdetails->company_id;
        }else{
            $usercompany = $user->company_id;
        }
        $prevemp = Employee::where('company_id',$usercompany)->where('mobile',$mobile)->where('user_type','contractor')->first();
        if(!empty($prevemp)){
            return Reply::error( __('messages.contractorExists'));
        }

        $alruser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('mobile',$mobile)->orWhere('email',$email)->first();
        if(empty($alruser->id)){
            $alruser = new User();
            $alruser->name = $name;
            $alruser->email = $email ? : '';
            $alruser->mobile = $mobile;
            $alruser->login = 'disable';
            $alruser->save();
        }

        $employee = new Employee();
        $employee->company_id = $usercompany;
        $employee->added_by = $this->user->id;
        $employee->user_id = $alruser->id;
        $employee->name = $name;
        $employee->email = $email;
        $employee->mobile = $mobile;
        $employee->user_type = 'contractor';
        $employee->gender = $request->gender;
        $employee->status = 'active';
        $employee->save();

        $user_type = "Contractor";
        $incomegroup = IncomeExpenseGroup::where('name',$user_type)->where('company_id',$usercompany)->first();
        $items = new IncomeExpenseHead();
        $items->company_id = $usercompany;
        $items->name = $request->name;
        $items->income_expense_type_id = !empty($incomegroup) ? $incomegroup->income_type : '';
        $items->income_expense_group_id = !empty($incomegroup) ? $incomegroup->id : '';
        $items->created_by = $user->id;
        $items->amount = !empty($request->amount) ? $request->amount : 0;
        $items->voucherdate = !empty($request->voucher_date) ? date('Y-m-d',strtotime($request->voucher_date)) : '';
        $items->particulars = $request->particulars;
        $items->emp_id = $employee->id;
        $items->save();

        if(!empty($projectid)){
            $prevproject = ProjectMember::where('user_id',$employee->user_id)->where('employee_id',$employee->id)->where('project_id',$projectid)->first();
            if(empty($prevproject->id)){
                $alreuser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->find($employee->user_id);
                $projectdetails = AppProject::where('id',$projectid)->first();
                $level = 0;
                if($projectdetails->added_by==$user->id){
                    $level = 1;
                }else{
                    $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectdetails->id)->first();
                    $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                    $level = $prolevel+1;
                }
                $pm = new ProjectMember();
                $pm->company_id = $projectdetails->company_id;
                $pm->project_id = $projectdetails->id;
                $pm->user_id = $employee->user_id;
                $pm->employee_id = $employee->id;
                $pm->user_type = 'contractor';
                $pm->assigned_by = $user->id;
                $pm->share_project = '0';
                $pm->level = $level;
                if($sharetoproject=='1'){
                    $pm->share_project = '1';
                    if(!empty($alreuser)&&!empty($alreuser->fcm)){
                        $notifmessage['title'] = 'Project Shared';
                        $notifmessage['body'] = 'You have been added to ' . ucwords(get_project_name($projectid)) . ' project by ' . $user->name;
                        $notifmessage['activity'] = 'projects';
                        sendFcmNotification($alreuser->id, $notifmessage);
                    }
                }
                $pm->save();

                return Reply::redirect(route('admin.contractors.index'));

//                , __('messages.userAssigned'));

            }else{
                return Reply::redirect(route('admin.contractors.index'), __('messages.useralreadyAssigned'));

            }
        }
        return Reply::redirect(route('admin.contractors.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->contractor = Employee::find($id);
        return view('admin.contractors.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorsRequest $request, $id)
    {
        $user = $this->user;
        $contractors = Employee::find($id);
        $contractors->name = trim($request->name);
        $contractors->email = trim($request->email);
        $contractors->mobile = trim($request->mobile);
        $contractors->gender = $request->gender;
        $contractors->save();


        $usertype = 'Contractor';
        $items = IncomeExpenseHead::where('company_id',$contractors->company_id)->where('emp_id',$contractors->id)->first();
        if(empty($items)){
            $incomegroup = IncomeExpenseGroup::where('name',$usertype)->where('company_id',$contractors->company_id)->first();
            $items = new IncomeExpenseHead();
            $items->company_id = $contractors->company_id;
            $items->emp_id = $contractors->id;
            $items->income_expense_type_id = $incomegroup->income_type;
            $items->income_expense_group_id = $incomegroup->id;
        }
        $items->name = $request->name;
        $items->save();
        return Reply::redirect(route('admin.contractors.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::where('id',$id)->first();
        $projectmember = ProjectMember::where('employee_id',$id)->first();
        if(!empty($projectmember->id)){
            $projectmember->delete();
        }
        $employee->delete();
        ProjectPermission::where('company_id',$employee->company_id)->where('user_id',$employee->user_id)->where('project_id',$projectmember->project_id)->delete();

        return Reply::success(__('messages.contractorsDeleted'));
    }

    public function data(Request $request)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        if(!empty($request->projects)){
            $projectlist = explode(',',$request->projects);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->where('user_type','contractor')->get();
        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.contractors.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="' . route('admin.clients.projects', $row->user_id) . '">' . ucfirst($row->name) . '</a>';
                }
            )->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )->editColumn(
                'mobile',
                function ($row) {
                    return $row->mobile;
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if ($row->status == 'active') {
                        return '<label class="label label-success">' . __('app.active') . '</label>';
                    } else {
                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }
    public function export($status, $client)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'client')
            ->where('roles.company_id', company()->id)
            ->leftJoin('client_details', 'users.id', '=', 'client_details.user_id')
            ->select(
                'users.id',
                'client_details.name',
                'client_details.email',
                'client_details.mobile',
                'client_details.firmname',
                'client_details.address',
                'client_details.gst_no',
                'client_details.created_at'
            )
            ->where('client_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($client != 'all' && $client != '') {
            $rows = $rows->where('users.id', $client);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('clients', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('clients file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
