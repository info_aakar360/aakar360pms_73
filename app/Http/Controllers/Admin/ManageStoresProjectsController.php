<?php

namespace App\Http\Controllers\Admin;

use App\Bom;
use App\Company;
use App\Condition;
use App\CostItemDescription;
use App\Currency;
use App\Expense;
use App\Helper\Reply;
use App\Http\Requests\Admin\Indent\StoreIndentRequest;
use App\Http\Requests\Admin\Indent\UpdateIndentRequest;
use App\Http\Requests\Admin\Rfq\ConvertRfqRequest;
use App\Http\Requests\Project\StoreProject;
use App\Indent;
use App\IndentProducts;
use App\Payment;
use App\PoProducts;
use App\Product;
use App\ProductIssue;
use App\ProductLog;
use App\ProductReturns;
use App\ProjectActivity;
use App\ProjectAttachmentFiles;
use App\ProjectAttachmentDesigns;
use App\ProjectCategory;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectFile;
use App\ProjectMember;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\PurchaseInvoice;
use App\PurchaseOrder;
use App\Quotes;
use App\Rfq;
use App\RfqProducts;
use App\SchedulingDue;
use App\SourcingPackage;
use App\SourcingPackageProduct;
use App\Stock;
use App\Store;
use App\Task;
use App\TaskboardColumn;
use App\TenderAssign;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersCondition;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\TmpIndent;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectMilestone;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use mysql_xdevapi\Session;
use function PHPSTORM_META\type;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\ProjectProgress;
use App\TaskCategory;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\ProjectCostItemsProduct;
use App\ProductCategory;
use App\ProductBrand;
use App\Units;
use App\FileManager;
use App\Helper\Files;
use Illuminate\Support\Facades\Storage;

class ManageStoresProjectsController extends AdminBaseController
{

    use ProjectProgress;

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.projects';
        $this->pageIcon = 'icon-layers';
        $this->activeMenu = 'store';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules) && !in_array('stores', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storesIndex()
    {
        $user = $this->user;
        $this->clients = User::allClients();
        $allProject = Project::select('status', 'id', 'project_budget')->get();
        $this->totalProjects = $allProject->count();
        $this->overdueProjects = $allProject->filter(function ($value, $key) {
            return $value->completion_percent <> '100' && $value->deadline <  Carbon::today()->timezone($this->global->timezone);
        })->count();
        $this->finishedProjects = $allProject->filter(function ($value, $key) {
            return $value->completion_percent == '100';
        })->count();
        $this->inProcessProjects = $allProject->filter(function ($value, $key) {
            return $value->status == 'in progress';
        })->count();
        $this->onHoldProjects = $allProject->filter(function ($value, $key) {
            return $value->status == 'on hold';
        })->count();
        $this->canceledProjects = $allProject->filter(function ($value, $key) {
            return $value->status == 'canceled';
        })->count();
        $this->notStartedProjects = $allProject->filter(function ($value, $key) {
            return $value->status == 'not started';
        })->count();
        $this->projectBudgetTotal = $allProject->sum('project_budget');
        $this->projectEarningTotal = Payment::join('projects', 'projects.id', '=', 'payments.project_id')
            ->where('payments.status', 'complete')
            ->whereNotNull('projects.project_budget')
            ->whereNotNull('payments.project_id')
            ->sum('payments.amount');
        $this->projectListarray = Project::get();
        $this->stores = Store::whereIn('project_id',explode(',',$user->projectlist))->get();
        return view('admin.stores-projects.index', $this->data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function archive()
    {
        $this->totalProjects = Project::onlyTrashed()->count();
        $this->clients = User::allClients();
        return view('admin.stores-projects.archive', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->clients = User::allClients();
        $this->categories = ProjectCategory::all();
        $this->templates = ProjectTemplate::all();
        $this->currencies = Currency::all();
        $project = new Project();
        $this->fields = $project->getCustomFieldGroupsWithFields()->fields;
        return view('admin.stores-projects.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProject $request)
    {
        $memberExistsInTemplate = false;
        $project = new Project();
        $project->project_name = $request->project_name;
        if ($request->project_summary != '') {
            $project->project_summary = $request->project_summary;
        }
        $project->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        if (!$request->has('without_deadline')) {
            $project->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');
        }
        if ($request->notes != '') {
            $project->notes = $request->notes;
        }
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        $project->client_id = $request->client_id;
        if ($request->client_view_task) {
            $project->client_view_task = 'enable';
        } else {
            $project->client_view_task = "disable";
        }
        if (($request->client_view_task) && ($request->client_task_notification)) {
            $project->allow_client_notification = 'enable';
        } else {
            $project->allow_client_notification = "disable";
        }
        if ($request->manual_timelog) {
            $project->manual_timelog = 'enable';
        } else {
            $project->manual_timelog = "disable";
        }
        $project->project_budget = $request->project_budget;
        $project->currency_id = $request->currency_id;
        $project->hours_allocated = $request->hours_allocated;
        $project->status = $request->status;
        $project->save();
        if ($request->template_id) {
            $template = ProjectTemplate::findOrFail($request->template_id);
            foreach ($template->members as $member) {
                $projectMember = new ProjectMember();
                $projectMember->user_id    = $member->user_id;
                $projectMember->project_id = $project->id;
                $projectMember->save();
                if ($member->user_id == $this->user->id) {
                    $memberExistsInTemplate = true;
                }
            }
            foreach ($template->tasks as $task) {
                $projectTask = new Task();
                $projectTask->user_id     = $task->user_id;
                $projectTask->project_id  = $project->id;
                $projectTask->heading     = $task->heading;
                $projectTask->description = $task->description;
                $projectTask->due_date    = Carbon::now()->addDay()->format('Y-m-d');
                $projectTask->status      = 'incomplete';
                $projectTask->save();
            }
        }
        if ($request->get('custom_fields_data')) {
            $project->updateCustomFieldData($request->get('custom_fields_data'));
        }
        if ($request->has('default_project_member') && $request->default_project_member == 'true' && !$memberExistsInTemplate) {
            $member = new ProjectMember();
            $member->user_id = $this->user->id;
            $member->project_id = $project->id;
            $member->save();
            $this->logProjectActivity($project->id, ucwords($this->user->name) . ' ' . __('messages.isAddedAsProjectMember'));
        }
        $this->logSearchEntry($project->id, 'Project: ' . $project->project_name, 'admin.projects.show', 'project');
        $this->logProjectActivity($project->id, ucwords($project->project_name) . ' ' . __("messages.addedAsNewProject"));
        if($project->id){
            $projectName = new FileManager();
            $projectName->filename = $request->project_name;
            $projectName->project_id = $project->id;
            $projectName->locked = '1';
            $projectName->parent = '0';
            $projectName->type = 'folder';
            $projectName->user_id = $this->user->id;
            $projectName->save();
            $dir = public_path('user-uploads/project-files/'.$request->project_name);
            $ddir = public_path('user-uploads/project-files/'.$request->project_name.'/Drawings');
            if($projectName->id){
                mkdir($dir);
                mkdir($ddir);
                $drawingsFolder = new FileManager();
                $drawingsFolder->filename = 'Drawings';
                $drawingsFolder->project_id = $project->id;
                $drawingsFolder->locked = '1';
                $drawingsFolder->parent = $projectName->id;
                $drawingsFolder->type = 'folder';
                $drawingsFolder->user_id = $this->user->id;
                $drawingsFolder->save();
            }
        }
        return Reply::dataOnly(['projectID' => $project->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? "0" : (count($this->openTasks) / count($this->project->tasks)) * 100);
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if (is_null($this->project->deadline)) {
            $this->daysLeft = 0;
        } else {
            if ($this->project->deadline->isPast()) {
                $this->daysLeft = 0;
            } else {
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            }
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $hour = intdiv($this->hoursLogged, 60);
        $minute = 0;
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($id, 10);
        $this->earnings = Payment::where('status', 'complete')
            ->where('project_id', $id)
            ->sum('amount');
        $this->expenses = Expense::where(['project_id' => $id, 'status' => 'approved'])->sum('price');
        $this->milestones = ProjectMilestone::with('currency')->where('project_id', $id)->get();
        if ($this->project->status == 'in progress') {
            $this->statusText = __('app.inProgress');
            $this->statusTextColor = 'text-info';
            $this->btnTextColor = 'btn-info';
        } else if ($this->project->status == 'on hold') {
            $this->statusText = __('app.onHold');
            $this->statusTextColor = 'text-warning';
            $this->btnTextColor = 'btn-warning';
        } else if ($this->project->status == 'not started') {
            $this->statusText = __('app.notStarted');
            $this->statusTextColor = 'text-warning';
            $this->btnTextColor = 'btn-warning';
        } else if ($this->project->status == 'canceled') {
            $this->statusText = __('app.canceled');
            $this->statusTextColor = 'text-danger';
            $this->btnTextColor = 'btn-danger';
        } else if ($this->project->status == 'finished') {
            $this->statusText = __('app.finished');
            $this->statusTextColor = 'text-success';
            $this->btnTextColor = 'btn-success';
        }
        return view('admin.stores-projects.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->clients = User::allClients();
        $this->categories = ProjectCategory::all();
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $this->currencies = Currency::all();
        return view('admin.stores-projects.edit', $this->data);
    }

    public function dataSimple(Request $request)
    {
        $this->userDetail = auth()->user();
        $projects = Project::select('projects.id', 'projects.project_name', 'projects.category_id', 'projects.status', 'projects.created_at', 'projects.added_by');
//        if (!$this->user->can('view_projects') && !$this->user->can('view_stores')) {
//            $projects = $projects->join('project_members', 'project_members.project_id', '=', 'projects.id');
//            $projects = $projects->where('project_members.user_id', '=', $this->userDetail->id);
//        }
        if (!is_null($request->status) && $request->status != 'all') {
            if ($request->status == 'incomplete') {
                $projects->where('completion_percent', '<', '100');
            } elseif ($request->status == 'complete') {
                $projects->where('completion_percent', '=', '100');
            }
        }
        $projects->get();
        return DataTables::of($projects)
            ->addIndexColumn()
            ->editColumn('project_name', function ($row) {
                return '<a href="' . route('admin.stores.projects.stores', [$row->id]) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->editColumn('category_id', function ($row) {
                $cat_name = 'NA';
                $cat = ProjectCategory::find($row->category_id);
                if($cat !== null){
                    $cat_name = $cat->category_name;
                }
                return ucwords($cat_name);
            })
            ->editColumn('status', function ($row) {
                return ucwords($row->status);
            })
            ->editColumn('added_by', function ($row) {
                $us = User::find($row->added_by);
                if($us) {
                    return ucwords($us->name);
                }
            })
            ->rawColumns(['project_name', 'members', 'completion_percent'])
            ->make(true);
    }

    public function updateSimple(StoreSimpleProject $request, $id)
    {
        $project = Project::findOrFail($id);
        $project->project_name = $request->project_name;
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        $project->status = $request->status;
        $project->added_by = Auth::user()->id;
        $project->save();
        $this->logProjectActivity($project->id, ucwords($project->project_name) . __('modules.projects.projectUpdated'));
        return Reply::redirect(route('admin.stores-projects.index'), __('messages.projectUpdated'));
    }

    public function storeSimple(StoreSimpleProject $request)
    {
        $project = new Project();
        $project->project_name = $request->project_name;
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        $project->client_view_task = "disable";
        $project->allow_client_notification = "disable";
        $project->manual_timelog = "disable";
        $project->status = $request->status;
        $project->added_by = Auth::user()->id;
        $project->save();
        if ($request->get('custom_fields_data')) {
            $project->updateCustomFieldData($request->get('custom_fields_data'));
        }
        $this->logSearchEntry($project->id, 'Project: ' . $project->project_name, 'admin.projects.show', 'project');
        $this->logProjectActivity($project->id, ucwords($project->project_name) . ' ' . __("messages.addedAsNewProject"));
        return Reply::dataOnly(['projectID' => $project->id]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProject $request, $id)
    {
        $project = Project::findOrFail($id);
        $project->project_name = $request->project_name;
        if ($request->project_summary != '') {
            $project->project_summary = $request->project_summary;
        }
        $project->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        if (!$request->has('without_deadline')) {
            $project->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');
        } else {
            $project->deadline = null;
        }
        if ($request->notes != '') {
            $project->notes = $request->notes;
        }
        if ($request->category_id != '') {
            $project->category_id = $request->category_id;
        }
        if ($request->client_view_task) {
            $project->client_view_task = 'enable';
        } else {
            $project->client_view_task = "disable";
        }
        if (($request->client_view_task) && ($request->client_task_notification)) {
            $project->allow_client_notification = 'enable';
        } else {
            $project->allow_client_notification = "disable";
        }
        if ($request->manual_timelog) {
            $project->manual_timelog = 'enable';
        } else {
            $project->manual_timelog = "disable";
        }
        $project->client_id = ($request->client_id == 'null' || $request->client_id == '') ? null : $request->client_id;
        $project->feedback = $request->feedback;
        if ($request->calculate_task_progress) {
            $project->calculate_task_progress = $request->calculate_task_progress;
            $project->completion_percent = $this->calculateProjectProgress($id);
        } else {
            $project->calculate_task_progress = "false";
            $project->completion_percent = $request->completion_percent;
        }
        $project->project_budget = $request->project_budget;
        $project->currency_id = $request->currency_id;
        $project->hours_allocated = $request->hours_allocated;
        $project->status = $request->status;
        $project->save();
        // To add custom fields data
        if ($request->get('custom_fields_data')) {
            $project->updateCustomFieldData($request->get('custom_fields_data'));
        }
        $this->logProjectActivity($project->id, ucwords($project->project_name) . __('modules.projects.projectUpdated'));
        return Reply::redirect(route('admin.stores-projects.index'), __('messages.projectUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::withTrashed()->findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('messages.projectDeleted'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function archiveDestroy($id)
    {
        Project::destroy($id);
        return Reply::success(__('messages.projectArchiveSuccessfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function archiveRestore($id)
    {
        $project = Project::withTrashed()->findOrFail($id);
        $project->restore();
        return Reply::success(__('messages.projectRevertSuccessfully'));
    }

    public function data(Request $request)
    {
        $projects = Project::with('members', 'members.user', 'client', 'currency')->select('id', 'project_name', 'start_date', 'deadline', 'client_id', 'completion_percent', 'status', 'project_budget', 'currency_id');
        if (!is_null($request->status) && $request->status != 'all') {
            if ($request->status == 'incomplete') {
                $projects->where('completion_percent', '<', '100');
            } elseif ($request->status == 'complete') {
                $projects->where('completion_percent', '=', '100');
            }
        }
        if (!is_null($request->status) && $request->status != 'all') {
            $projects->where('status', $request->status);
        }
        if (!is_null($request->client_id) && $request->client_id != 'all') {
            $projects->where('client_id', $request->client_id);
        }
        $projects->get();
        return DataTables::of($projects)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $action = '<div class="btn-group m-r-10">
                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info btn-outline  dropdown-toggle waves-effect waves-light" type="button">'.trans('app.action').' <span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="'.route('admin.projects.edit', [$row->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></li>
                  <li><a href="'.route('admin.projects.show', [$row->id]) . '"><i class="fa fa-search" aria-hidden="true"></i> View Project Details</a></li>
                  <li><a href="'.route('admin.projects.gantt', [$row->id]) . '"><i class="fa fa-bar-chart" aria-hidden="true"></i> '.trans('modules.projects.viewGanttChart').'</a></li>
                  <li><a href="'.route('front.gantt', [md5($row->id)]) . '" target="_blank"><i class="fa fa-line-chart" aria-hidden="true"></i> '.trans('modules.projects.viewPublicGanttChart').'</a></li>
                  <li><a href="javascript:;" data-user-id="' . $row->id . '" class="archive"><i class="fa fa-archive" aria-hidden="true"></i> Archive</a></li>
                  <li><a href="javascript:;" data-user-id="' . $row->id . '" class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> Delete</a></li>';
                $action .= '</ul> </div>';
                return $action;
            })
            ->addColumn('members', function ($row) {
                $members = '';
                if (count($row->members) > 0) {
                    foreach ($row->members as $member) {
                        $members .= ($member->user->image) ? '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('user-uploads/avatar/' . $member->user->image) . '"
                        alt="user" class="img-circle" width="30"> ' : '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('default-profile-2.png') . '"
                        alt="user" class="img-circle" width="30"> ';
                    }
                } else {
                    $members .= __('messages.noMemberAddedToProject');
                }
                $members .= '<br><br><a class="font-12" href="' . route('admin.project-members.show', $row->id) . '"><i class="fa fa-plus"></i> ' . __('modules.projects.addMemberTitle') . '</a>';
                return $members;
            })
            ->editColumn('project_name', function ($row) {
                $name = '<a href="' . route('admin.projects.show', $row->id) . '">' . ucfirst($row->project_name) . '</a>';
                if ($row->paused) {
                    $name .= '<br><label class="label label-warning">' . __('modules.projects.pause') . '</label>';
                }
                return $name;
            })
            ->editColumn('start_date', function ($row) {
                if($row->start_date == null){
                    return 'NA';
                }
                return $row->start_date->format($this->global->date_format);
            })
            ->editColumn('deadline', function ($row) {
                if ($row->deadline) {
                    return $row->deadline->format($this->global->date_format);
                }
                return '-';
            })
            ->editColumn('client_id', function ($row) {
                if (is_null($row->client_id)) {
                    return "--";
                }
                return ucwords($row->client->name);
            })
            ->editColumn('status', function ($row) {
                if ($row->status == 'in progress') {
                    $status = '<label class="label label-info">' . __('app.inProgress') . '</label>';
                } else if ($row->status == 'on hold') {
                    $status = '<label class="label label-warning">' . __('app.onHold') . '</label>';
                } else if ($row->status == 'not started') {
                    $status = '<label class="label label-warning">' . __('app.notStarted') . '</label>';
                } else if ($row->status == 'canceled') {
                    $status = '<label class="label label-danger">' . __('app.canceled') . '</label>';
                } else if ($row->status == 'finished') {
                    $status = '<label class="label label-success">' . __('app.finished') . '</label>';
                }
                return $status;
            })
            ->editColumn('completion_percent', function ($row) {
                if ($row->completion_percent < 50) {
                    $statusColor = 'danger';
                    $status = __('app.progress');
                } elseif ($row->completion_percent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                    $status = __('app.progress');
                } else {
                    $statusColor = 'success';
                    $status = __('app.progress');
                    if ($row->completion_percent >= 100) {
                        $status = __('app.completed');
                    }
                }
                $pendingPayment = 0;
                $projectEarningTotal = Payment::join('projects', 'projects.id', '=', 'payments.project_id')
                    ->where('payments.status', 'complete')
                    ->whereNotNull('projects.project_budget')
                    ->where('payments.project_id', $row->id)
                    ->sum('payments.amount');
                $pendingPayment = ($row->project_budget - $projectEarningTotal);
                $pendingAmount = '';
                if ($pendingPayment > 0) {
                    $pendingAmount = $row->currency->currency_symbol . $pendingPayment;
                }
                $progress = '<h5>' . $status . '<span class="pull-right">' . $row->completion_percent . '%</span></h5><div class="progress">
                    <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_percent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $row->completion_percent . '%" role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
                if ($pendingAmount != '') {
                    $progress .= '<small class="text-danger">' . __('app.unpaid') . ' ' . __('app.menu.payments') . ': ' . $pendingAmount . '</small>';
                }
                return $progress;
            })
            ->rawColumns(['project_name', 'action', 'completion_percent', 'members', 'status'])
            ->removeColumn('project_summary')
            ->removeColumn('notes')
            ->removeColumn('category_id')
            ->removeColumn('feedback')
            ->removeColumn('start_date')
            ->make(true);
    }

    public function archiveData(Request $request)
    {
        $projects = Project::select('id', 'project_name', 'start_date', 'deadline', 'client_id', 'completion_percent');
        if (!is_null($request->status) && $request->status != 'all') {
            if ($request->status == 'incomplete') {
                $projects->where('completion_percent', '<', '100');
            } elseif ($request->status == 'complete') {
                $projects->where('completion_percent', '=', '100');
            }
        }
        if (!is_null($request->client_id) && $request->client_id != 'all') {
            $projects->where('client_id', $request->client_id);
        }
        $projects->onlyTrashed()->get();
        return DataTables::of($projects)
            ->addColumn('action', function ($row) {
                return '
                      <a href="javascript:;" class="btn btn-info btn-circle revert"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Restore"><i class="fa fa-undo" aria-hidden="true"></i></a>
                       <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->addColumn('members', function ($row) {
                $members = '';
                if (count($row->members) > 0) {
                    foreach ($row->members as $member) {
                        $members .= ($member->user->image) ? '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('user-uploads/avatar/' . $member->user->image) . '"
                        alt="user" class="img-circle" width="30"> ' : '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('default-profile-2.png') . '"
                        alt="user" class="img-circle" width="30"> ';
                    }
                } else {
                    $members .= __('messages.noMemberAddedToProject');
                }
                return $members;
            })
            ->editColumn('project_name', function ($row) {
                return ucfirst($row->project_name);
            })
            ->editColumn('start_date', function ($row) {
                return $row->start_date->format('d M, Y');
            })
            ->editColumn('deadline', function ($row) {
                if ($row->deadline) {
                    return $row->deadline->format($this->global->date_format);
                }
                return '-';
            })
            ->editColumn('client_id', function ($row) {
                if (is_null($row->client_id)) {
                    return "";
                }
                return ucwords($row->client->name);
            })
            ->editColumn('completion_percent', function ($row) {
                if ($row->completion_percent < 50) {
                    $statusColor = 'danger';
                    $status = __('app.progress');
                } elseif ($row->completion_percent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                    $status = __('app.progress');
                } else {
                    $statusColor = 'success';
                    $status = __('app.progress');
                    if ($row->completion_percent >= 100) {
                        $status = __('app.completed');
                    }
                }
                return '<h5>' . $status . '<span class="pull-right">' . $row->completion_percent . '%</span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_percent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $row->completion_percent . '%" role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->removeColumn('project_summary')
            ->removeColumn('notes')
            ->removeColumn('category_id')
            ->removeColumn('feedback')
            ->removeColumn('start_date')
            ->rawColumns(['project_name', 'action', 'completion_percent', 'members'])
            ->make(true);
    }

    public function export($status = null, $clientID = null)
    {
        $projects = Project::leftJoin('users', 'users.id', '=', 'projects.client_id')
            ->leftJoin('project_category', 'project_category.id', '=', 'projects.category_id')
            ->select(
                'projects.id',
                'projects.project_name',
                'users.name',
                'project_category.category_name',
                'projects.start_date',
                'projects.deadline',
                'projects.completion_percent',
                'projects.created_at'
            );
        if (!is_null($status) && $status != 'all') {
            if ($status == 'incomplete') {
                $projects = $projects->where('completion_percent', '<', '100');
            } elseif ($status == 'complete') {
                $projects = $projects->where('completion_percent', '=', '100');
            }
        }
        if (!is_null($clientID) && $clientID != 'all') {
            $projects = $projects->where('client_id', $clientID);
        }
        $projects = $projects->get();
        $exportArray = [];
        $exportArray[] = ['ID', 'Project Name', 'Client Name', 'Category', 'Start Date', 'Deadline', 'Completion Percent', 'Created at'];
        foreach ($projects as $row) {
            $exportArray[] = $row->toArray();
        }
        Excel::create('Projects', function ($excel) use ($exportArray) {
            $excel->setTitle('Projects');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('Projects file');
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function gantt($ganttProjectId = '')
    {
        $data = array();
        $links = array();
        $projects = Project::select('id', 'project_name', 'start_date', 'deadline', 'completion_percent');
        if($ganttProjectId != '')
        {
            $projects = $projects->where('id', '=', $ganttProjectId);
        }
        $projects = $projects->get();
        $id = 0; //count for gantt ids
        foreach ($projects as $project) {
            $id = $id + 1;
            $projectId = $id;
            // TODO::ProjectDeadline to do
            $projectDuration = 0;
            if ($project->deadline) {
                $projectDuration = $project->deadline->diffInDays($project->start_date);
            }
            $data[] = [
                'id' => $projectId,
                'text' => ucwords($project->project_name),
                'start_date' => $project->start_date->format('Y-m-d H:i:s'),
                'duration' => $projectDuration,
                'progress' => $project->completion_percent / 100
            ];
            $tasks = Task::projectOpenTasks($project->id);
            foreach ($tasks as $key => $task) {
                $id = $id + 1;
                $taskDuration = $task->due_date->diffInDays($task->start_date);
                $data[] = [
                    'id' => $id,
                    'text' => ucfirst($task->heading),
                    'start_date' => (!is_null($task->start_date)) ? $task->start_date->format('Y-m-d H:i:s') : $task->due_date->format('Y-m-d H:i:s'),
                    'duration' => $taskDuration,
                    'parent' => $projectId,
                    'users' => [
                        ucwords($task->user->name)
                    ]
                ];
                $links[] = [
                    'id' => $id,
                    'source' => $project->id,
                    'target' => $task->id,
                    'type' => 1
                ];
            }
            $ganttData = [
                'data' => $data,
                'links' => $links
            ];
        }
        $this->ganttProjectId = $ganttProjectId;
        return view('admin.stores-projects.gantt', $this->data);
    }

    public function ganttData($ganttProjectId = '')
    {
        $data = array();
        $links = array();
        $projects = Project::select('id', 'project_name', 'start_date', 'deadline', 'completion_percent');
        if($ganttProjectId != '')
        {
            $projects = $projects->where('id', '=', $ganttProjectId);
        }
        $projects = $projects->get();
        $id = 0; //count for gantt ids
        foreach ($projects as $project) {
            $id = $id + 1;
            $projectId = $id;
            // TODO::ProjectDeadline to do
            $projectDuration = 0;
            if ($project->deadline) {
                $projectDuration = $project->deadline->diffInDays($project->start_date);
            }
            $data[] = [
                'id' => $projectId,
                'text' => ucwords($project->project_name),
                'start_date' => $project->start_date->format('Y-m-d H:i:s'),
                'duration' => $projectDuration,
                'progress' => $project->completion_percent / 100,
                'project_id' => $project->id,
                'dependent_task_id' => null
            ];
            $tasks = Task::projectOpenTasks($project->id);
            foreach ($tasks as $key => $task) {
                $id = $id + 1;
                $taskDuration = $task->due_date->diffInDays($task->start_date);
                $taskDuration = $taskDuration + 1;
                $data[] = [
                    'id' => $task->id,
                    'text' => ucfirst($task->heading),
                    'start_date' => (!is_null($task->start_date)) ? $task->start_date->format('Y-m-d') : $task->due_date->format('Y-m-d'),
                    'duration' => $taskDuration,
                    'parent' => $projectId,
                    'users' => [
                        ucwords($task->user->name)
                    ],
                    'taskid' => $task->id,
                    'dependent_task_id' => $task->dependent_task_id
                ];
                $links[] = [
                    'id' => $id,
                    'source' => $task->dependent_task_id != '' ? $task->dependent_task_id : $projectId,
                    'target' => $task->id,
                    'type' => $task->dependent_task_id != '' ? 0 : 1
                ];
            }
        }
        $ganttData = [
            'data' => $data,
            'links' => $links
        ];
        return response()->json($ganttData);
    }

    public function updateTaskDuration(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
        $task->save();
        return Reply::success('messages.taskUpdatedSuccessfully');
    }

    public function updateStatus(Request $request, $id)
    {
        $project = Project::find($id)
            ->update([
                'status' => $request->status
            ]);
        return Reply::dataOnly(['status' => 'success']);
    }

    public function ajaxCreate(Request $request, $projectId)
    {
        $this->projectId = $projectId;
        $this->projects = Project::all();
        $this->employees = ProjectMember::byProject($projectId);
        $this->pageName = 'ganttChart';
        $this->parentGanttId = $request->parent_gantt_id;
        $this->cost_item_id = $request->cost_item_id;
        $this->categories = TaskCategory::all();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('project_id', $projectId)
                ->get();
        }else {
            $this->allTasks = [];
        }
        return view('admin.tasks.ajax_create', $this->data);
    }

    public function burndownChart(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->totalTask = $this->project->tasks->count();
        $datesArray = [];
        $startDate = $request->startDate ? Carbon::createFromFormat('Y-m-d', $request->startDate) : Carbon::parse($this->project->start_date);
        if ($this->project->deadline){
            $endDate = $request->endDate ? Carbon::createFromFormat('Y-m-d', $request->endDate) : Carbon::parse($this->project->deadline);
        }
        else{
            $endDate = $request->endDate ? Carbon::parse($request->endDate) : Carbon::now();
        }
        for ($startDate; $startDate<= $endDate; $startDate->addDay()){
            $datesArray[] = $startDate->format($this->global->date_format);
        }
        $uncompletedTasks = [];
        $createdTasks = [];
        $deadlineTasks = [];
        $deadlineTasksCount = [];
        $this->datesArray = json_encode($datesArray);
        foreach ($datesArray as $key => $value){
            if (Carbon::createFromFormat($this->global->date_format, $value)->lessThanOrEqualTo(Carbon::now())){
                $uncompletedTasks[$key] = $this->project->tasks->filter(function($task) use($value){
                    if (is_null($task->completed_on)){
                        return true;
                    }
                    return $task->completed_on ? $task->completed_on->greaterThanOrEqualTo(Carbon::createFromFormat($this->global->date_format, $value)) : false;
                })->count();
                $createdTasks[$key] = $this->project->tasks->filter(function ($task) use($value){
                    return Carbon::createFromFormat($this->global->date_format, $value)->startOfDay()->equalTo($task->created_at->startOfDay());
                })->count();
                if ($key > 0){
                    $uncompletedTasks[$key] += $createdTasks[$key];
                }
            }
            $deadlineTasksCount[] = $this->project->tasks->filter(function($task) use($value){
                return Carbon::createFromFormat($this->global->date_format, $value)->startOfDay()->equalTo($task->due_date->startOfDay());
            })->count();
            if ($key == 0){
                $deadlineTasks[$key] = $this->totalTask - $deadlineTasksCount[$key];
            }
            else{
                $newKey = $key -1;
                $deadlineTasks[$key] = $deadlineTasks[$newKey] - $deadlineTasksCount[$key];
            }
        }
        $this->uncompletedTasks = json_encode($uncompletedTasks);
        $this->deadlineTasks = json_encode($deadlineTasks);
        if ($request->ajax()){
            return $this->data;
        }
        $this->startDate = $request->startDate ? Carbon::parse($request->startDate)->format('Y-m-d') : Carbon::parse($this->project->start_date)->format('Y-m-d');
        $this->endDate = $endDate->format('Y-m-d');
        return view('admin.stores-projects.burndown', $this->data);
    }

    public function Boq(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->titles = Title::where('project_id',$id)->get();
        $this->id = $id;
        return view('admin.stores-projects.boq', $this->data);
    }
    public function BoqTitle(Request $request, $id,$title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $postitionarray = ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->first();
        if(empty($postitionarray->id)){
            $columsarray = array(
                'costitem'=>'Task',
                'description'=>'Description',
                'contractor'=>'Contractor',
                'startdate'=>'Start date',
                'enddate'=>'End date',
                'rate'=>'Rate',
                'unit'=>'Unit',
                'qty'=>'Qty',
                'worktype'=>'Work Type',
                'markuptype'=>'MarkupType',
                'markupvalue'=>'Markup value',
                'adjustment'=>'Adjustment',
                'finalrate'=>'Final rate',
                'totalamount'=>'Total amount'
            );
            $col =1; foreach($columsarray as $colkey => $colums){
                $columsarray = new ProjectCostItemsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->position = 'col';
                $columsarray->itemid = 0;
                $columsarray->itemname = $colums;
                $columsarray->itemslug = $colkey;
                $columsarray->collock = 0;
                $columsarray->level = 0;
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $categoryarray = DB::table('project_cost_items_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
            ->where('project_id',$id)
            ->where('title',$title)
            ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->where('parent','0')->orderby('id','asc')->get();
        $col =1; foreach($boqlevel1categories as  $colums){
            $prevcol = ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('itemname',$colums->title)->first();
            if(empty($prevcol->id)){
                $columsarray = new ProjectCostItemsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->position = 'row';
                $columsarray->itemid = $colums->id;
                $columsarray->itemname = $colums->title;
                $columsarray->itemslug = '';
                $columsarray->collock = 0;
                $columsarray->level = 1;
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $this->categories = BoqCategory::get();
        $this->unitsarray = Units::get();
        $this->title = $title;
        $this->id = $id;
        return view('admin.stores-projects.boqtitle', $this->data);
    }

    public function addBoqCostItemRow(Request $request){
        $costitem = $request->value;
        $costitemslist = \App\CostItems::orderBy("id",'asc')->get();
        $unitsarray = \App\Units::orderBy("id",'asc')->get();
        $costitemdetails = CostItems::where('cost_item_name',$costitem)->first();
        $proprogetdat = new ProjectCostItemsProduct();
        $proprogetdat->title = $request->titleid;
        $proprogetdat->project_id = $request->projectid;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_id = $costitemdetails->id;
        $proprogetdat->unit = $costitemdetails->unit;
        $proprogetdat->start_date = date('Y-m-d');
        $proprogetdat->deadline = date('Y-m-d');
        $proprogetdat->save();
        $catvalue =  explode(',',$request->category);
        $catitem = $catvalue[1];
        $contenthtml = '';
        $colpositionarray = \App\ProjectCostItemsPosition::where('project_id',$request->projectid)->where('title',$request->titleid)->where('position','col')->orderBy('inc','asc')->get();
        $contenthtml .= '<tr data-depth="2" class="collpse level2 catrow'.$proprogetdat->category.'">
            <td></td>
            <td></td>
            <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>';
            foreach ($colpositionarray as $colposition) {
                switch ($colposition->itemslug) {
                    case  'costitem':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '"  class="cell-inp updateproject" data-item="costitem" list="costitem' . $proprogetdat->id . '"  value="' . $costitem . '">
                           <datalist id="costitem' . $proprogetdat->id . '">';
                            foreach ($costitemslist as $costitem) {
                                $contenthtml .= '<option data-value="' . $costitem->id . '" >' . $costitem->cost_item_name . '</option>';
                            }
                            $contenthtml .= '</datalist>
                        </td>';
                    break;
                    case 'description':
                        $contenthtml .= '<td><textarea data-itemid="' . $proprogetdat->id . '" data-item="description" onkeydown="textAreaAdjust(this)" style="height: 25px;" class="cell-inp updateproject" ></textarea></td>';
                    break;
                    case 'contractor':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="contractor" type="text" class="cell-inp updateproject" name="contractor"  ></td>';
                    break;
                    case 'startdate':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="start_date" type="text" class="cell-inp datepicker updateproject" name="startdate"  ></td>';
                    break;
                    case 'enddate':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="deadline" type="text" class="cell-inp datepicker updateproject" name="deadline" ></td>';
                    break;
                    case 'rate':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="rate" type="text" class="cell-inp updateproject ratevalue' . $proprogetdat->id . '"  data-cat="' . $catitem . '" name="rate"  ></td>';
                    break;
                    case 'unit':
                        $contenthtml .= '<td>
                            <input  data-itemid="' . $proprogetdat->id . '" data-item="unit" class="cell-inp updateproject" list="unitdata' . $proprogetdat->id . '"  value="' . get_unit_name($proprogetdat->unit) . '">
                            <datalist id="unitdata' . $proprogetdat->id . '">';
                                foreach ($unitsarray as $units) {
                                    $contenthtml .= '<option data-value="' . $units->id . '" >' . $units->name . '</option>';
                                }
                            $contenthtml .= '</datalist>
                        </td>';
                    break;
                    case 'qty':
                        $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="qty" type="text" class="cell-inp updateproject qty' . $proprogetdat->id . '"  ></td>';
                    break;
                    case 'worktype':
                        $contenthtml .= '<td> <input data-itemid="' . $proprogetdat->id . '" data-item="worktype" class="cell-inp updateproject" list="type' . $proprogetdat->id . '" >
                            <datalist id="type' . $proprogetdat->id . '">
                               <option value="workforce">Workforce</option>
                               <option value="equipment">Equipment</option>
                               <option value="material">Material</option>
                               <option value="commitment">Commitment</option>
                               <option value="owner-cost">Owner Cost</option>
                               <option value="professional">Professional</option>
                               <option value="other">Other</option>
                            </datalist>
                        </td>';
                    break;
                    case 'markuptype':
                        $contenthtml .= '<td>
                            <input  data-itemid="' . $proprogetdat->id . '" data-item="markuptype" onchange="calmarkup(' . $proprogetdat->id . ')"  class="cell-inp updateproject markuptype' . $proprogetdat->id . '" list="markuptype' . $proprogetdat->id . '"  >
                            <datalist id="markuptype' . $proprogetdat->id . '">
                               <option value="percent">Percent</option>
                               <option value="amt">Amount</option>
                            </datalist>
                        </td>';
                    break;
                    case 'markupvalue':
                        $contenthtml .= '<td>
                            <input type="text" data-item="markupvalue" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject markupvalue' . $proprogetdat->id . '" data-cat="' . $catitem . '" name="markupvalue" onchange="calmarkup(' . $proprogetdat->id . ')" >
                        </td>';
                    break;
                    case 'adjustment':
                        $contenthtml .= '<td>
                            <input type="text" data-item="adjustment" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject adjustment' . $proprogetdat->id . '"  data-cat="' . $catitem . '" name="adjustment" >
                        </td>';
                    break;
                    case 'finalrate':
                        $contenthtml .= '<td>
                            <input type="text" data-item="finalrate" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject totalrate' . $proprogetdat->id . ' finalrate' . $catitem . '"  data-cat="' . $catitem . '" name="finalrate" >
                        </td>';
                    break;
                    case 'totalamount':
                        $contenthtml .= '<td>
                            <input type="text" data-item="totalamount" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject grandvalue totalamount' . $proprogetdat->id . ' finalamount' . $catitem . '"  data-cat="' . $catitem . '" name="finalamount" >
                        </td>';
                    break;
                }
            }
        $contenthtml .= '</tr>';
        return $contenthtml;
    }

    public function addBoqCostItemCategory(Request $request){
        $contenthtml = '';
        $contenthtml .= '<tr  data-depth="0" class="collpse level0">
            <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
            <td></td>
            <td></td>
            <td>'.$request->category.'</td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
            <td><input type="text" class="cell-inp"></td>
        </tr>';
        return $contenthtml;
    }
    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('admin.stores-projects.project-boq-create', $this->data);
    }
    public  function boqChangeColPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
         if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function boqChangeCostitemPosition(Request $request){
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)){
           $x=1; foreach ($positionarray as $position){
                ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->where('id',$position)->update(['inc'=>$x]);
           $x++; }
        }
    }
    public  function projectBoqLock(Request $request){
        $positionarray = $request->lockcolms;
        $projectid = $request->projectid;
        $title = $request->title;
        if(!empty($positionarray)&&count($positionarray)>0){
            ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$title)->update(['collock'=>0]);
        }
        return redirect(url('admin/stores-projects/boq/'.$projectid.'/'.$title));
    }
    public  function updateCostItem(Request $request){
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $projectid = $request->projectid;
        $title = $request->title;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'start_date':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'deadline':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
        }
        ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->where('id',$itemid)->update($dataarray);
    }
    public function getItemLavel(Request $request){
        $this->categories = BoqCategory::get();
        $costitemlavel = CostItemsLavel::where('boq_category_id', $request->category_id)->groupby('cost_items_id')->get();
        $html = '<option value="">Please select @lang(\'app.task\')</option>';
        foreach ($costitemlavel as $cil){
            $ciid = $cil->cost_items_id;
            $costitem = CostItems::where('id',$ciid)->first();
            $html .= '<option value="'.$costitem->id.'">'.$costitem->cost_item_name.'</option>';
        }
        return $html;
    }
    public function getChild(Request $request){
        $getchild = BoqCategory::where('parent', $request->category_id)->get();
        $id = $request->id + 1;
        $html = '<select name="category[]" class="form-control" onchange="getChild(this.value,id)">
            <option value="">Please select Category</option>';
        foreach ($getchild as $cil){
            $html .= '<option value="'.$cil->id.'">'.$cil->title.'</option>';
        }
        $html .= '</select>';
        if(count($getchild) !== 0) {
            return $html;
        }else{
            return '';
        }
    }

    public function getProduct(Request $request){
        $this->categories = BoqCategory::get();
        $costitem = CostItemsProduct::where('cost_items_id', $request->cost_item_id)->get();
        $html = '
            <table class="table">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Category Name</td>
                        <td>Brand Name</td>
                        <td>Unit</td>
                        <td>Quantity</td>
                        <td>Wastage</td>
                        <td>Rate</td>
                        <td>Cost</td>
                    </tr>
                </thead>
                <tbody>';
                $sr = 1;
                foreach ($costitem as $cil){
                    $ciid = $cil->cost_items_id;
                    $pc = $cil->product_category_id;
                    $pb = $cil->product_brand_id;
                    $pu = $cil->unit;
                    $pcn = ProductCategory::where('id',$pc)->first();
                    $pbn = ProductBrand::where('id',$pb)->first();
                    $pun = Units::where('id',$pu)->first();
                        $html .='<tr>
                            <td>'.csrf_field().'
                                <input type="hidden" name="project_id" class="form-control" value="'.$request->id.'">
                                <input type="hidden" name="cost_item_id['.$sr.']" class="form-control" value="'.$ciid.'">'.$sr .'
                            </td>
                            <td><input type="hidden" name="product_category_id['.$sr.']" class="form-control" value="'.$pc.'">'.$pcn->name.'</td>
                            <td><input type="hidden" name="product_brand_id['.$sr.']" class="form-control" value="'.$pb.'">'.$pbn->name.'</td>
                            <td><input type="hidden" name="units['.$sr.']" class="form-control" value="'.$pu.'">'.$pun->name.'</td>
                            <td><input type="text" name="qty['.$sr.']" id="qty'.$sr.'" class="form-control"></td>
                            <td><input type="text" name="wastage['.$sr.']" class="form-control"></td>
                            <td><input type="text" data-cat-id="'.$sr.'" onchange="getCost('.$sr.');" id="rate'.$sr.'" name="rate['.$sr.']" class="form-control"></td>
                            <td><input type="text" name="cost['.$sr.']" id="cost'.$sr.'" class="form-control" readonly></td>
                        </tr>';
               $sr++; }
               $html .= '</tbody>
            </table>
            <div class="col-lg-12">
                <button type="button" id="saveProduct" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
            </div>';
        return $html;
    }

    public function getCostItem(Request $request){
        $this->categories = BoqCategory::get();
        $costitemlavel = CostItemsLavel::where('boq_category_id', $request->category_id)->groupby('cost_items_id')->get();
        $html = '<option value="">Please select @lang(\'app.task\')</option>';
        foreach ($costitemlavel as $cil){
            $ciid = $cil->cost_items_id;
            $costitem = CostItems::where('id',$ciid)->first();
            $html .= '<option value="'.$costitem->id.'">'.$costitem->cost_item_name.'</option>';
        }
        return $html;
    }

    public function addCostItems(Request $request){
        $title = $request->title;
        $pc = '';
            if($request->category !== null) {
                $pc = implode(',', $request->category);
            }
        $category = $pc;
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $product_category_id = $request->product_category_id;
        $product_brand_id = $request->product_brand_id;
        $units = $request->units;
        $qty = $request->qty;
        $wastage = $request->wastage;
        $rate = $request->rate;
        $cost = $request->cost;
        $start_date = date('Y-m-d',strtotime($request->start_date));
        $deadline = date('Y-m-d',strtotime($request->deadline));
        foreach ($qty as $key=>$value){
            if($qty !== "" && $qty > 0) {
                $pcip = new ProjectCostItemsProduct();
                $pcip->title = $title;
                $pcip->category = $category;
                $pcip->project_id = $project_id;
                $pcip->cost_items_id = $cost_item_id[$key];
                $pcip->product_category_id = $product_category_id[$key];
                $pcip->product_brand_id = $product_brand_id[$key];
                $pcip->start_date = $start_date;
                $pcip->deadline = $deadline;
                $pcip->unit = $units[$key];
                $pcip->qty = $qty[$key];
                $pcip->wastage = $wastage[$key];
                $pcip->rate = $rate[$key];
                $pcip->cost = $cost[$key];
                $pcip->save();
            }
        }
        return Reply::success('Added Successfully');
    }

    public function viewBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::get();
        $this->projectproduct = ProjectCostItemsProduct::where('project_id',$id)->groupBy('title')->get();
        $this->title = Title::where('id',$title)->first();
        $this->id = $id;
        return view('admin.stores-projects.view-boq', $this->data);
    }

    public function editBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::get();
        $this->projectproduct = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->get();
        $this->ci = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->groupBy('cost_items_id')->get();
        $this->pc = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->groupBy('title')->first();
        $this->title = Title::where('id',$title)->first();
        $this->id = $id;
        return view('admin.stores-projects.edit-boq', $this->data);
    }

    public function updateBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $title = $request->title;
        $category = json_encode($request->category);
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $product_category_id = $request->product_category_id;
        $product_brand_id = $request->product_brand_id;
        $units = $request->units;
        $qty = $request->qty;
        $wastage = $request->wastage;
        $rate = $request->rate;
        $cost = $request->cost;
        foreach ($qty as $key=>$value){
            $check = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('cost_items_id',$cost_item_id[$key])->first();
            if($check){
                $delete = ProjectCostItemsProduct::find($check->id)->delete();
                if($qty !== "" && $qty > 0) {
                    $pcip = new ProjectCostItemsProduct();
                    $pcip->title = $title;
                    $pcip->category = $check->category;
                    $pcip->project_id = $project_id;
                    $pcip->cost_items_id = $cost_item_id[$key];
                    $pcip->product_category_id = $product_category_id[$key];
                    $pcip->product_brand_id = $product_brand_id[$key];
                    $pcip->unit = $units[$key];
                    $pcip->qty = $qty[$key];
                    $pcip->wastage = $wastage[$key];
                    $pcip->rate = $rate[$key];
                    $pcip->cost = $cost[$key];
                    $pcip->save();
                }
            }else {
                if ($qty !== "" && $qty > 0) {
                    $pcip = new ProjectCostItemsProduct();
                    $pcip->title = $title;
                    $pcip->category = $category;
                    $pcip->project_id = $project_id;
                    $pcip->cost_items_id = $cost_item_id[$key];
                    $pcip->product_category_id = $product_category_id[$key];
                    $pcip->product_brand_id = $product_brand_id[$key];
                    $pcip->unit = $units[$key];
                    $pcip->qty = $qty[$key];
                    $pcip->wastage = $wastage[$key];
                    $pcip->rate = $rate[$key];
                    $pcip->cost = $cost[$key];
                    $pcip->save();
                }
            }
        }
       return redirect()->route('admin.stores-projects.boq',[$id])->with('success', 'Successfully updated!');
    }
    public function scheduling(Request $request, $ganttProjectId = '')
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($ganttProjectId);
        $this->employees = User::allEmployees();
        $this->projectcostitems = ProjectCostItemsProduct::join('cost_items', 'cost_items.id', '=', 'project_cost_items_product.cost_items_id')
            ->select('project_cost_items_product.id as pid','cost_items.id','cost_items.cost_item_name')->groupBy('project_cost_items_product.cost_items_id')->get();
        $data = array();
        $links = array();
            $categoryarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
                ->where('project_id',$ganttProjectId)
                ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        foreach ($categoryarray as $category) {
            $catdetails = BoqCategory::find($category);
            if ($catdetails !== null) {
                $data[] = [
                    'id' => $catdetails->id,
                    'parent' => $catdetails->parent,
                    'text' => $catdetails->text,
                    'start_date' => '2020-03-03',
                    'deadline' => '2020-04-03',
                    'duration' => '30',
                    /* 'users' => [
                         ucwords($task->user->name)
                     ],*/
                ];
                $links[] = [
                    'id' => $catdetails->id,
                    'source' => $catdetails->id,
                    'target' => $catdetails->parent,
                    'type' => 0
                ];
            }
        }
        $ganttData = [
            'data' => $data,
            'links' => $links
        ];

        $assignarray = DB::table('project_cost_items_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `assign_to`"))
            ->where('project_id',$ganttProjectId)
            ->where('assign_to', '<>', '')
            ->first();
        $resources = array();
        $assignarray = array_unique(array_filter(explode(',',$assignarray->assign_to)));
        foreach ($assignarray as $assign){
            $resources[] = [
                'id' => $assign,
                'text' => 'Santosh',
                'parent' => null,
            ];
        }
        $this->ganttProjectId = $ganttProjectId;
        $this->ganttData = $ganttData;
        $this->resourceData = json_encode($resources);
        $baselinedata = ProjectCostItemsProduct::where('project_id',$ganttProjectId)->first();
        $this->baselinedate = !empty($baselinedata->baselinedate) ? $baselinedata->baselinedate : '';
        $this->baselinelabel = !empty($baselinedata->baselinelabel) ? $baselinedata->baselinelabel : '';
        return view('admin.stores-projects.scheduling', $this->data);
    }

    public function schedulingData(Request $request,$ganttProjectId = '')
    {
        $data = array();
        $links = array();
        $resources = array();
        if($ganttProjectId != '')
        {
            $costitemsprojects = ProjectCostItemsProduct::where('project_id', '=', $ganttProjectId)->groupBy('cost_items_id')->get();
            $startdatesarray =  $deaddatesarray = array();
            foreach($costitemsprojects as $projects) {
                $catarray = array_filter(explode(',', $projects->category));
                foreach ($catarray as $cats) {
                        $startdatesarray[$cats][] = $projects->start_date;
                        $deaddatesarray[$cats][] = $projects->deadline;
                }
            }
            foreach($costitemsprojects as $projects){
                $catarray = array_filter(explode(',',$projects->category));
                    $category = end($catarray);
                    $costitems = CostItems::find($projects->cost_items_id);
                $projectDuration = 0;
                if ($projects->deadline) {
                    $projectDuration = Carbon::parse($projects->deadline)->diffInDays(Carbon::parse($projects->start_date));
                }
                $assigned = !empty($projects->assign_to) ? explode(',',$projects->assign_to) : array();
                $linkedcosts = ProjectCostItemsProduct::where('project_id',$ganttProjectId)->whereRaw('FIND_IN_SET(?,category)', [$category])->where('linked',1)->pluck('id')->toArray();
                $nolinkedcosts = ProjectCostItemsProduct::where('project_id',$ganttProjectId)->whereRaw('FIND_IN_SET(?,category)', [$category])->where('linked',0)->pluck('id')->toArray();
                $deadlinedates = ProjectCostItemsProduct::where('project_id',$ganttProjectId)->whereIn('id',$linkedcosts)->pluck('deadline')->toArray();
                $deadnolinkeddates = ProjectCostItemsProduct::where('project_id',$ganttProjectId)->whereIn('id',$nolinkedcosts)->pluck('deadline')->toArray();
                $maxdate = max($deaddatesarray[$category]);
                $colorcode = '';
                   if(in_array($projects->id,$linkedcosts)){
                       if(!empty($deadlinedates)&&in_array($maxdate,$deadlinedates)){
                           $colorcode = 'rgba(255, 0, 0,0.50)';
                       }else{
                           $colorcode = 'rgb(128, 255, 128,0.40)';
                       }
                   }
                   if(in_array($projects->id,$nolinkedcosts)){
                       if(in_array($maxdate,$deadnolinkeddates)){
                           $colorcode = 'rgba(255, 0, 0,0.40)';
                       }else{
                           $colorcode = 'rgb(128, 255, 128,0.40)';
                       }
                   }
                $resourcework =  ProjectCostItemsProduct::selectRaw("count(*) as count,min(start_date) as start_date,min(deadline) as deadline")->where('project_id',$projects->project_id)
                    ->where('assign_to', '<>', '')
                    ->whereRaw('FIND_IN_SET(?,assign_to)', [$assigned])->first();
                   $quantity = ProjectCostItemsFinalQty::where('project_id',$ganttProjectId)->where('cost_item_id',$projects->cost_items_id)->first();
                   if(!empty($projects->baselinedate)&&!empty($projects->planned_start)&&!empty($projects->planned_end)){
                       $data[] = [
                           'id' => 'p'.$projects->id,
                           'parent' =>  (int)$category,
                           'text' => $costitems->cost_item_name,
                           'planned_start' => !empty($projects->planned_start) ? $projects->planned_start : '',
                           'planned_end' => !empty($projects->planned_end) ? $projects->planned_end : '',
                           'start_date' => !empty($projects->start_date) ? $projects->start_date : '',
                           'deadline' => !empty($projects->deadline) ? $projects->deadline : '',
                           'qty' => !empty($quantity->qty) ? $quantity->qty : 0,
                           'duration' => $projectDuration,
                           'linktype' => 'project',
                           'work' =>  $resourcework->count,
                           'color' => $colorcode,
                           'users' => $assigned,
                           /*'users' => [
                              ucwords($task->user->name)
                          ],*/
                       ];
                   }else{
                       $data[] = [
                           'id' => 'p'.$projects->id,
                           'parent' =>  (int)$category,
                           'text' => $costitems->cost_item_name,
                           'start_date' => !empty($projects->start_date) ? $projects->start_date : '',
                           'deadline' => !empty($projects->deadline) ? $projects->deadline : '',
                           'duration' => $projectDuration,
                           'qty' => !empty($projects->qty) ? $projects->qty : 0,
                           'linktype' => 'project',
                           'work' =>  $resourcework->count,
                           'color' => $colorcode,
                           'users' => $assigned,
                           /*'users' => [
                              ucwords($task->user->name)
                          ],*/
                       ];
                   }
                $links[] = [
                    'id' =>  $projects->id,
                    'target' => 'p'.$projects->id,
                    'source' => 'p'.$projects->target,
                    'type' =>  $projects->type,
                ];
            }
        $categoryarray = DB::table('project_cost_items_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
            ->where('project_id',$ganttProjectId)
            ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        foreach ($categoryarray as $category){
            $catdetails = BoqCategory::find($category);
            $mindate = min($startdatesarray[$catdetails->id]);
            $maxdate = max($deaddatesarray[$catdetails->id]);
            $projectDuration = 0;
            if ($maxdate) {
                $projectDuration = Carbon::parse($maxdate)->diffInDays(Carbon::parse($mindate));
            }
            $data[] = [
                'id' =>  $catdetails->id,
                'parent' =>  $catdetails->parent,
                'text' => $catdetails->title,
                'start_date' => $mindate,
                'deadline' => $maxdate,
                'duration' => $projectDuration,
                'work' => 0,
                'linktype' => 'category',
                'color' => 'rgba(0,0,0,0.70)',
                "open" => true,
            ];
           /* $links[] = [
                'id' =>  $catdetails->id,
                'source' => $catdetails->id,
                'target' => $catdetails->parent,
                'type' => ''
            ];*/
        }
        $assignarray = DB::table('project_cost_items_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `assign_to`"))
            ->where('project_id',$ganttProjectId)
            ->where('assign_to', '<>', '')
            ->first();

        $assignarray = array_unique(array_filter(explode(',',$assignarray->assign_to)));
        foreach ($assignarray as $assign){
            $resources[] = [
                'id' => $assign,
                'text' => $assign,
                'parent' => null,
            ];
        }
        $ganttData = [
            'data' => $data,
            'links' => $links,
            'resource' => $resources
        ];

        }
        return response()->json($ganttData);
    }
    public function costData(Request $request){
        $id = $request->id;
        if(strpos($id, 'p') !== false){
            $id = ltrim($id, 'p');
             $projectcostitem = ProjectCostItemsProduct::find($id);
            $projectcostitem->start_date = date('d-m-Y',strtotime($projectcostitem->start_date));
            $projectcostitem->deadline = date('d-m-Y',strtotime($projectcostitem->deadline));
            return $projectcostitem;
        }
    }
    public function costDataUpdate(Request $request){
        $assignto = '';
        if(!empty($request->assign_to)){
            $assignto = array_unique(array_filter($request->assign_to));
            $assignto = implode(',',$assignto);
        }
        $id = $request->project_cost_id;
        $projectcost = ProjectCostItemsProduct::find($id);
        $projectcost->assign_to = $assignto;
        if(!empty($projectcost->baselinedate)){
            $projectcost->planned_start =  $projectcost->start_date;
            $projectcost->planned_end = $projectcost->deadline;
            $projectcost->start_date = date('Y-m-d',strtotime($request->start_date));
            $projectcost->deadline = date('Y-m-d',strtotime($request->deadline));
        }else{
            $projectcost->planned_start =  '';
            $projectcost->planned_end =  '';
            $projectcost->start_date = date('Y-m-d',strtotime($request->start_date));
            $projectcost->deadline = date('Y-m-d',strtotime($request->deadline));
        }
        $projectcost->type = $request->linktype ?: 0;
        $projectcost->target = $request->target;
        $projectcost->linked = !empty($request->target) ? '1' : '0';
        $projectcost->save();
        ProjectCostItemsProduct::where('id',$request->target)->update([
            'linked' => 1
        ]);
        return redirect(url('admin/stores-projects/scheduling/'.$projectcost->project_id));
    }
    public function baselineUpdate(Request $request){

        $data[] = array();
        $id = $request->projectid;
        $baselinedate = !empty($request->baselinedate) ? date('Y-m-d',strtotime($request->baselinedate)) : '';
        $baselinelabel = !empty($request->baselinelabel) ? $request->baselinelabel : '';
        $projectcost = ProjectCostItemsProduct::where('project_id',$id)->update([
            'baselinedate' => $baselinedate,
            'baselinelabel' => $baselinelabel
        ]);
        $data['status'] = 'success';
        return response()->json($data);
    }
    public function resourseData(Request $request){
        $data = array();
        $links = array();
        $projectId = $request->projectId;
        if($projectId != '')
        {
            $assignarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(assign_to SEPARATOR ',')) as `assign_to`"))
                ->where('project_id',$projectId)
                ->where('assign_to', '<>', '')
                ->first();
            $assignarray = array_unique(array_filter(explode(',',$assignarray->assign_to)));
            foreach ($assignarray as $assign){
                $resourcework =  ProjectCostItemsProduct::selectRaw("count(*) as count,min(start_date) as start_date,min(deadline) as deadline")->where('project_id',$projectId)
                    ->where('assign_to', '<>', '')
                    ->whereRaw('FIND_IN_SET(?,assign_to)', [$assign])->first();
                $users = User::where('id',$assign)->first();
                $username = !empty($users->name) ? $users->name : '';
                $data[] = [
                    'id' => $assign,
                    'text' => $username,
                    'start_date' => $resourcework->start_date,
                    'work' => $resourcework->count,
                    'parent' => null,
                ];
            }
        }
        return response()->json($data);
    }
    public function schedulingDue(Request $request, $projectId)
    {
        $due = new SchedulingDue();
        $due->title = $request->title;
        $due->project_id = $request->project_id;
        $due->cost_item_id = $request->cost_item_id;
        $due->dependent_id = $request->dependent_id;
        $due->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');;
        $due->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');;
        $due->assign_to = implode(',',$request->assign_to);
        $due->save();
        return back();
    }

    public function ProjectFinalQty(Request $request, $id){
        $title = $request->title;
        $category = $request->category;
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $qty = $request->total_qty;
        $total_amount = $request->total_amount;
        $description = $request->description;
        $coid = $request->coid;
        foreach ($qty as $key=>$value) {
            ProjectCostItemsFinalQty::where('title', $title)->where('project_id', $id)->where('cost_item_id', $cost_item_id[$key])->delete();
        }
        foreach ($qty as $key=>$value) {
            if ($value != '' && $value != "0") {
                $pcip = new ProjectCostItemsFinalQty();
                $pcip->title = $title;
                $pcip->category = $category[$key];
                $pcip->project_id = $project_id;
                $pcip->cost_item_id = $cost_item_id[$key];
                $pcip->qty = $value;
                $pcip->total_amount = $total_amount[$key];
                $pcip->save();
            }
        }
        foreach ($description as $key=>$des){
            $cid = new CostItemDescription();
            $cid->boq_id = $id;
            $cid->cost_item_id = $key;
            $cid->description = $des;
            $cid->save();
        }
        return redirect()->route('admin.stores-projects.boq',[$id])->with('success', 'Successfully updated!');
    }
    
    public function titleCreate(Request $request, $id){
        $title = $request->title;
        $t = new Title();
        $t->title = $title;
        $t->project_id = $id;
        $t->save();
        return redirect()->route('admin.stores-projects.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function getFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                        foreach($this->files as $file){
                            $path = $file->id;
                            $html .= '<tr>
                                <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                            $html .='</tr>';
                        }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                        foreach($this->files as $file){
                            $path = $file->id;
                            $html .= '<tr>
                                <td>';
                                if($file->type == 'file') {
                                    $html .= '<i class="ti-file"></i> ' . $file->filename;
                                }else {
                                    $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                        <i class="ti-folder"></i> ' . $file->filename . '
                                    </a>';
                                }
                                $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
                                if($file->type == 'file') {
                                    $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                }
                                if($file->type == 'folder') {
                                    $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                }
                                $html .='</td>
                            </tr>';
                        }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function showFiles(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $this->files = FileManager::where('parent',$parent)->where('project_id',$id)->get();
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->id = $id;
        return view('admin.projects.project-files.show', $this->data);
    }

    public function selectFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentFiles();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteFile($id)
    {
        $project = ProjectAttachmentFiles::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function getDesignFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                            foreach($this->files as $file){
                                $path = $file->id;
                                $html .= '<tr>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                    $html .='</td>
                                    <td>'.$file->type.'</td>
                                    <td>'.$file->size .'Bytes</td>
                                    <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                                    $html .='</td>
                                </tr>';
                            }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openDesignFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
                            foreach($this->files as $file){
                                $path = $file->id;
                                $html .= '<tr>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<i class="ti-file"></i> ' . $file->filename;
                                    }else {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
                                    }
                                    $html .='</td>
                                    <td>'.$file->type.'</td>
                                    <td>'.$file->size .'Bytes</td>
                                    <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                    <td>';
                                    if($file->type == 'file') {
                                        $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');" data-toggle="tooltip" data-original-title="Select File" class="btn btn-info btn-circle"><i class="fa fa-check"></i></a>';
                                    }
                                    if($file->type == 'folder') {
                                        $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');" data-toggle="tooltip" data-original-title="Open" class="btn btn-info btn-circle"><i class="fa fa-folder-open-o"></i></a>';
                                    }
                                    $html .='</td>
                                </tr>';
                            }
                        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function showDesignFiles(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->files = ProjectAttachmentDesigns::where('project_id',$id)->get();
        $this->id = $id;
        return view('admin.stores-projects.project-files.show-design', $this->data);
    }

    public function selectDesignFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentDesigns();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteDesignFile($id)
    {
        $project = ProjectAttachmentDesigns::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function projectEditFile(Request $request, $projectId, $fileId){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($projectId);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $file = FileManager::findOrFail($fileId);
        $parent = $file->parent;
        $filePath = $file->hashname;
        $mime = explode('.', $filePath);
        while ($parent){
            $filep = \App\FileManager::findOrFail($parent);
            $filePath = $filep->filename.'/'.$filePath;
            $parent = $filep->parent;
        }
        $this->urlx = $filePath;
        $this->queryUrl = '&user='.Auth::user()->id.'&project='.$projectId.'&file_id='.$fileId.'&username='.Auth::user()->name;
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->file = $file;
        $this->id = $projectId;
        return view('admin.stores-projects.project-files.edit-file', $this->data);
    }

    public function sourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return view('admin.stores-projects.sourcing-package', $this->data);
    }

    public function addSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$id)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->id = $id;
        return view('admin.stores-projects.sourcing-package-create', $this->data);
    }

    public function storeSourcingPackages(Request $request, $id){
        $source = new SourcingPackage();
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            $pro = $request->products;
            for($j = 0; $j < count($pro); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $pro[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('admin.stores-projects.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function editSourcingPackages(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->id = $pid;
        return view('admin.stores-projects.sourcing-package-edit', $this->data);
    }

    public function updateSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $source = SourcingPackage::findOrFail($id);
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            SourcingPackageProduct::where('sourcing_package_id',$source->id)->delete();
            $cfp = $request->products;
            for($j = 0; $j < count($cfp); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $cfp[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('admin.stores-projects.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function getProducts(Request $request){
        $html = '';
        $cfk = $request->create_from;
        for($k = 0; $k < count($cfk); $k++ ) {
            $cats = ProjectCostItemsProduct::where('title', $cfk[$k])->where('project_id', $request->project_id)->groupBy('title')->get();
            foreach ($cats as $cat) {
                $xcat = explode(',', $cat->category);

                for ($i = 0; $i < count($xcat); $i++) {
                    $name = BoqCategory::where('id', $xcat[$i])->first();
                    if($name->id !== null) {
                        $html .= '<option value="' . $name->id . '">' . $name->title . '</option>';
                    }
                }
            }
        }
        return $html;
    }

    public function filterData(Request $request){
        $html = '';
        $cf = $request->create_from;
        $html .= '
                <input type="hidden" name="project_id" value="'.$request->project_id.'"/>
                <input type="hidden" name="title" value="'.$request->title.'"/>';
        foreach($request->create_from as $caf) {
            $html .= '<input type="hidden" name="create_from[]" value="' . $caf . '"/>';
        }
        foreach($request->category as $c) {
            $html .= '<input type="hidden" name="category[]" value="' . $c . '"/>';
        }
        foreach($request->type as $t) {
            $html .= '<input type="hidden" name="type[]" value="' . $t . '"/>';
        }
        $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Product</b></div>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Quantity</b></div>
                </div>';
        for($j = 0; $j < count($cf); $j++ ) {
            $products = ProjectCostItemsProduct::where('title',$cf[$j])->where('project_id', $request->project_id)
                ->join('category', 'category.id', '=', 'project_cost_items_product.product_category_id')
                ->join('product_type', 'product_type.product_id', '=', 'project_cost_items_product.product_category_id')
                ->where('product_type.type',$request->type[$j])
                ->select('category.name as name', 'category.id as id')
                ->get();
            foreach ($products as $product) {
                $fqty = DB::table('project_cost_item_final_qty')
                    ->where('title',$cf[$j])
                    ->where('project_id', $request->project_id)
                    ->sum('qty');
                $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;"> 
                        <input type="checkbox" name="products[]" value="' . $product->id . '" class="form-check-input"> 
                        ' . $product->name . '
                    </label>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;">';
                        if($fqty) $html .= $fqty;
                    $html .= '</label>
                </div>';
            }
        }
        return $html;
    }

    public function deleteSourcingPackages($id)
    {
        $project = SourcingPackage::findOrFail($id);
        $project->forceDelete();
        $sp = SourcingPackageProduct::where('sourcing_package_id',$id)->delete();
        return Reply::success(__('Package deleted successfully'));
    }

    public function createTender(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->pid = $pid;
        $this->id = $id;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        return view('admin.stores-projects.create-tender', $this->data);
    }

    public function getBrandsTag(Request $request){
        $q = $_GET['query'];
        $query = ProductBrand::where('name','LIKE','%'.$q.'%')->select('id', 'name')->get();
        $return = array();
        if($query){
            foreach($query as $q) {
                $return[] = $q;
            }
        }
        $json = json_encode($return);
        return $json;
    }

    public function saveTenders(Request $request){
        $tender = new Tenders();
        $tender->company_id = $this->user->company_id;
        $tender->project_id = $request->project_id;
        $tender->sourcing_package_id = $request->sourcing_package_id;
        $tender->name = $request->tender_name;
        $tender->save();
        if($tender->id){
            $cfp = $request->conditions;
            for($j = 1; $j < count($cfp); $j++ ) {
                $sourcep = new TendersCondition();
                $sourcep->tender_id = $tender->id;
                $sourcep->condition_id = $cfp[$j];
                $sourcep->terms = $request->terms[$j];
                $sourcep->save();
            }
            foreach($request->products as $key=>$pi ) {
                $tp = new TendersProduct();
                $tp->tender_id = $tender->id;
                $tp->products = $pi;
                $tp->qty = $request->qty[$key];
                $tp->brands = $request->brand_ids[$key];
                $tp->save();
            }
        }
        return Reply::dataOnly(['taskID' => $tender->id]);
    }

    public function storeTenderFiles(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.cloud');
                $file = new TendersFiles();
                $file->user_id = $this->user->id;
                $file->tender_id = $request->task_id;
                $file->company_id = $this->user->company_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/tender-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('tender-files/'.$request->task_id, $fileData, $fileData->getClientOriginalName(), 'public');
                    break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'tender-files')
                            ->first();
                        if(!$dir) {
                            Storage::cloud()->makeDirectory('tender-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();
                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());
                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());
                    break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('tender-files/'.$request->task_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/tender-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                    break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::redirect(route('admin.stores-projects.create-tender'), __('modules.projects.projectUpdated'));
    }

    public function tendersList(Request $request, $pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$id)->first();
        $this->tenders = Tenders::where('company_id',$this->user->company_id)
            ->where('project_id',$pid)->where('sourcing_package_id',$id)->get();
        $this->id = $id;
        $this->pid = $pid;
        return view('admin.stores-projects.tenders-list', $this->data);
    }

    public function editTender(Request $request, $pid, $id, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$sid)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        return view('admin.stores-projects.edit-tender', $this->data);
    }

    public function assignTender(Request $request, $pid, $sid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$id)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->sid = $sid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$sid)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$sid)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        $this->employees = User::allEmployees();
        return view('admin.stores-projects.assign-tender', $this->data);
    }

    public function storeAssignTender(Request $request){
        foreach($request->user_id as $user) {
            $tenderAssign = new TenderAssign();
            $tenderAssign->company_id = $this->user->company_id;
            $tenderAssign->project_id = $request->project_id;
            $tenderAssign->tender_id = $request->tender_id;
            $tenderAssign->sourcing_id = $request->sourcing_id;
            $tenderAssign->user_id = $user;
            $tenderAssign->save();
        }
        return back();
    }

    public function bidding(Request $request, $company_id, $project_id, $tender_id, $sourcing_id, $user_id){
        $this->assign_tender = TendersCondition::where('company_id', $company_id)
            ->where('project_id', $project_id)
            ->where('tender_id', $tender_id)
            ->where('sourcing_id', $sourcing_id)
            ->where('user_id', $user_id)
            ->first();
        if($this->assign_tender){
            $this->tender = Tenders::where('company_id',$company_id)
                ->where('id',$tender_id)->first();
            $this->company_id = $company_id;
            $this->project_id = $project_id;
            $this->tender_id = $tender_id;
            $this->sourcing_id = $sourcing_id;
            $this->user_id = $user_id;
            $this->conditions = Condition::all();
            $this->brands = ProductBrand::all();
            $this->packages = SourcingPackage::where('company_id',$company_id)->get();
            $this->costitems = ProjectCostItemsProduct::where('project_id',$project_id)
                ->join('title','title.id','=','project_cost_items_product.title')
                ->select('title.id','title.title')
                ->groupBy('project_cost_items_product.title')
                ->get();
            $this->package = SourcingPackage::where('id',$sourcing_id)->first();
            $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$sourcing_id)
                ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
                ->select('category.name as name', 'category.id as id')
                ->get();
            $this->cons = TendersCondition::where('tender_id', $tender_id)->get();
            $this->pros = TendersProduct::where('tender_id', $tender_id)->get();
            $this->files = TendersFiles::where('tender_id', $tender_id)->get();
        }
        return view('admin.stores-projects.bidding', $this->data);
    }

    public function tenderBiddings(Request $request){
        $this->tenders = Tenders::where('company_id',$this->user->company_id)->get();
        return view('admin.stores-projects.tenders-biddings', $this->data);
    }

    public function tenderBiddingDetails(Request $request, $id){
        $supIds = TenderBidding::where('company_id',$this->user->company_id)->where('tender_id', $id)->pluck('user_id');
        $this->users = User::whereIn('id', $supIds)->get();
        $this->tenders = TenderBidding::where('company_id',$this->user->company_id)->where('tender_id',$id)->get();
        $this->tender = Tenders::where('company_id',$this->user->company_id)->where('id',$id)->first();
        $this->products = TenderBiddingProduct::where('tender_id', $id)->groupBy('products')->get();
        return view('admin.stores-projects.tender-bidding-detail', $this->data);
    }

    public function productIssue(Request $request, $pid, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->products = ProductCategory::get();
        $this->store = Store::where('id',$sid)->first();
        $this->issuedProducts = ProductIssue::where('project_id',$pid)->where('store_id',$sid)->groupBy('unique_id')->get();
        $this->issuedBy = ProductIssue::where('project_id',$pid)->where('store_id',$sid)->groupBy('issued_by')->get();
        return view('admin.stores-projects.product-issue', $this->data);
    }

    public function productLog(Request $request, $pid, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->products = Bom::where('project_id',$pid)->where('store_id',$sid)->groupBy('product_id')->get();
        $this->store = Store::where('id',$sid)->first();
        $this->issuedProducts = ProductIssue::where('project_id',$pid)->groupBy('unique_id')->get();
        $this->productLogs = ProductLog::where('project_id',$pid)->where('store_id',$sid)->get();
        return view('admin.stores-projects.product-log', $this->data);
    }

    public function createIssuedProduct(Request $request, $pid, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->products = Bom::where('project_id',$pid)->where('store_id',$sid)->where('company_id',$this->companyid)->get();
        $this->store = Store::where('id',$sid)->first();
        $this->indents = Indent::where('project_id',$pid)->where('store_id',$sid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $costarray = array();
        $costarray['projectid'] = $pid;
        $costarray['subprojectid'] = 0;
        $costarray['segmentid'] = 0;
        $costarray['parent'] = 0;
        $costarray['level'] = 0;
        $this->projectcostitems = getBoqTaskList($costarray);
        return view('admin.stores-projects.create-product-issue', $this->data);
    }
    public function storeIssuedProduct(Request $request){
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $task_id = $request->task_id;
        $unique_id = 'PI-'.date("d-m-Y").'-'.count($product_id);
        $unit_id = '';
        if(isset($request->unit_id)) {
            $unit_id = $request->unit_id;
        }
        $quantity       =   !empty($request->quantity)?$request->quantity:0;
        if(count(array_unique($quantity)) === 1 && end($quantity) === '0'){
            return Reply::error('Please Enter Valid Quantity');
        }
        $recQty         =   $request->recQty;
        $issuedQty      =   $request->issuedQty;
        $issued_by      =   $this->user->id;
        if($request->indent_no !== ''){
            foreach($quantity as $key=>$value ){
                $scheck = Stock::where('cid',$product_id[$key])->where('store_id', $request->store_id)->where('project_id',$project_id)->sum('stock');
                $stock= Stock::where('cid',$product_id[$key])->where('store_id', $request->store_id)->where('project_id',$project_id)->first();
                $bal = $finalQty = 0;
                if($scheck !== null){
                    
                    if(!empty($issuedQty)){
                        $finalQty = (!empty($issuedQty[$key])?$issuedQty[$key]:0) + $quantity[$key];
                    }
                    if($quantity[$key] > $scheck){
                        return Reply::error(__('Issued Quantity greater then available stock'));
                    }else{
                        $recqty = !empty($recQty[$key]) ? $recQty[$key]  : 0;
                        if($finalQty <= $recqty ){

                            $value = !empty($value) ? $value : 0;
                            $bal = $stock->stock - $value;
                            $stock->stock = $bal;
                            $stock->save();
                            if(!empty($quantity[$key])) {
                                $pro = new ProductIssue();
                                $pro->project_id = $project_id;
                                $pro->product_id = (!empty($product_id[$key])) ? $product_id[$key] : '';
                                $pro->unique_id = $unique_id;
                                $pro->task_id = $task_id;
                                $pro->store_id = $request->store_id;
                                $pro->unit_id = (!empty($unit_id[$key])) ? $unit_id[$key] : '';
                                $pro->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                                $pro->issued_by = $issued_by;
                                $pro->company_id = $this->user->company_id;
                                $pro->remark = $request->remark;
                                $pro->save();
                                if($pro){
                                    $pl = new ProductLog();
                                    $pl->store_id = $request->store_id;
                                    $pl->project_id = $request->project_id;
                                    $pl->company_id = $this->user->company_id;
                                    $pl->created_by = $this->user->id;
                                    $pl->module_id = $pro->id;
                                    $pl->module_name = 'product_issue';
                                    $pl->product_id = (!empty($product_id[$key])) ? $product_id[$key] : '';
                                    $pl->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                                    $pl->balance_quantity = $bal;
                                    $pl->transaction_type = 'minus';
                                    $pl->remark = 'Product Issued';
                                    $pl->save();
                                }
                            }
                        }else{
                            return Reply::error(__('Please Enter Valid Quantity'));
                        }
                    }
                }

            }
        }else {
            foreach ($product_id as $key => $value) {
                if($quantity[$key] !== '') {
                    $scheck = Stock::where('cid',$value)->where('store_id', $request->store_id)->where('project_id',$project_id)->first();
                    $bal = 0;
                    if($scheck !== null){
                        if($quantity[$key] > $scheck->stock){
                            return Reply::success(__('Issued Quantity greater then stock'));
                        }else{
                            $bal = (int)$scheck->stock - (!empty($quantity[$key])) ? (int)$quantity[$key] : 0;
                            $scheck->stock = $bal;
                            $scheck->save();
                        }
                    }
                    $pro = new ProductIssue();
                    $pro->project_id = $project_id;
                    $pro->product_id = (!empty($value)) ? $value : '';
                    $pro->unique_id = $unique_id;
                    $pro->store_id = $request->store_id;
                    $pro->unit_id = (!empty($unit_id[$key])) ? $unit_id[$key] : '';
                    $pro->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                    $pro->issued_by = $issued_by;
                    $pro->company_id = $this->user->company_id;
                    $pro->save();

                    if($pro){
                        $pl = new ProductLog();
                        $pl->store_id = $request->store_id;
                        $pl->project_id = $request->project_id;
                        $pl->company_id = $this->user->company_id;
                        $pl->created_by = $this->user->id;
                        $pl->module_id = $pro->id;
                        $pl->module_name = 'product_issue';
                        $pl->product_id = (!empty($product_id[$key])) ? $product_id[$key] : '';
                        $pl->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                        $pl->balance_quantity = $bal;
                        $pl->transaction_type = 'minus';
                        $pl->remark = 'Product Issued';
                        $pl->save();
                    }
                }
            }
        }
        return Reply::success(__('Product issued successfully'));
    }

    public function issuedProductDetail(Request $request,$pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        $this->store = Store::where('id',$this->issuedProducts[0]->store_id)->first();
        return view('admin.stores-projects.product-issued-detail', $this->data);
    }

    public function returnIssuedProduct(Request $request,$pid, $sid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->store = Store::where('id',$sid)->first();
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        return view('admin.stores-projects.return-issued-product', $this->data);
    }

    public function storeReturnedProduct(Request $request){
        $id = $request->id;
        $quantity = $request->quantity;
        $returned_quantity = $request->returned_quantity;
        foreach($id as $key=>$value ){
            $qty = $quantity[$key] - $returned_quantity[$key];
            $pro = ProductIssue::find($value);
            $pro->quantity = $qty;
            $pro->save();
        }

        foreach ($returned_quantity as $k=>$rq){
            $pro = Stock::where('cid',$request->product_id[$k])->where('store_id',$request->store_id)->where('project_id',$request->project_id)->first();
            $qty = $pro->stock + $rq;
            $pro->stock = $qty;
            $pro->save();

            if($pro){
                $pl = new ProductLog();
                $pl->store_id = $request->store_id;
                $pl->project_id = $request->project_id;
                $pl->company_id = $this->user->company_id;
                $pl->created_by = $this->user->id;
                $pl->module_id = $pro->id;
                $pl->module_name = 'product_return';
                $pl->product_id = (!empty($request->product_id[$k])) ? $request->product_id[$k] : '';
                $pl->quantity = (!empty($rq)) ? $rq : '';
                $pl->balance_quantity = (!empty($qty)) ? $qty : '';
                $pl->transaction_type = 'plus';
                $pl->remark = 'Issue return';
                $pl->save();
            }
        }

        return Reply::success(__('Issue returned successfully'));
    }

    public function showStoresProducts(Request $request, $id, $sid)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->products = Bom::where('store_id',$sid)->where('project_id',$id)->get();
        $this->store = Store::where('id',$sid)->first();
        $this->projectid = $id;
        $this->storeid = $sid;
        $this->units = Units::get();
        return view('admin.stores-projects.show-store-products', $this->data);
    }

    public function bom($pid,$sid){
        $this->products = Product::where('company_id',$this->user->company_id)->get();
        $this->pid = $pid;
        $this->sid = $sid;
        return view('admin.stores-projects.bom', $this->data);
    }

    public  function storeBom(Request $request){

        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $trade_id = $request->trade_id;
        $unit_id = $request->unit_id;
        foreach ($product_id as $key=>$value) {
            $check = Bom::where('project_id', $project_id)->where('store_id', $store_id)->where('product_id',$value)->first();
            if($check === null){
                $bom = new Bom();
                $bom->project_id = $project_id;
                $bom->company_id = $this->user->company_id;
                $bom->store_id = $store_id;
                $bom->product_id = $value;
                $bom->trade_id = $trade_id[$key];
                $bom->unit_id = $unit_id[$key];
                $bom->opening_stock = 0;
                $bom->save();
            }
            $productProject = Product::find($value);
            if(empty($productProject->project_id)){
                $productProject->project_id = $project_id;
                $productProject->save();
            }else{
                $newProduct = new Product();
                $newProduct->company_id     =   $productProject->company_id;
                $newProduct->category_id    =   !empty($productProject->category_id)?$productProject->category_id:0;
                $newProduct->project_id     =   $project_id;
                $newProduct->trade_id       =   !empty($productProject->trade_id)?$productProject->trade_id:0;
                $newProduct->unit_id        =   !empty($productProject->unit_id)?$productProject->unit_id:0;
                $newProduct->name           =   $productProject->name;
                $newProduct->price          =   !empty($productProject->price)?$productProject->price:0;
                $newProduct->taxes          =   !empty($productProject->taxess)?$productProject->taxess:0;
                $newProduct->save();
            }
        }
        return Reply::success(__('Material added successfully'));
    }

    public  function updateBom(Request $request){
        $id = $request->id;
        $unit_id = $request->unit_id;
        $est_qty = $request->est_qty;
        $est_rate = $request->est_rate;
        $opening_stock = $request->opening_stock;
        $sku = time();
        foreach ($id as $key=>$value){
            $bom = Bom::find($value);
            $bom->unit_id = $unit_id[$key];
            $bom->est_qty = $est_qty[$key];
            $bom->est_rate = $est_rate[$key];
            $bom->opening_stock = $opening_stock[$key];
            $bom->save();
            if($bom && !empty($bom)) {
                if(!empty($opening_stock[$key])) {
                    $stockCheck = Stock::where('cid', $bom->product_id)->where('store_id', $bom->store_id)->where('project_id', $bom->project_id)->first();
                    if ($stockCheck && !empty($stockCheck)) {
                        $stockCheck->stock = (!empty($opening_stock[$key])) ? (int)$opening_stock[$key] : '';
                        $stockCheck->quantity = (!empty($opening_stock[$key])) ? (int)$opening_stock[$key] : '';
                        $stockCheck->save();
                        if ($stockCheck && !empty($stockCheck)) {
                            $pl = ProductLog::where('id',$value)->where('store_id', $bom->store_id)->where('project_id', $bom->project_id)->first();
                            if(!empty($pl)) {
                                $pl->store_id = $bom->store_id;
                                $pl->project_id = $bom->project_id;
                                $pl->company_id = $this->user->company_id;
                                $pl->created_by = $this->user->id;
                                $pl->module_id = $bom->id;
                                $pl->module_name = 'opening_stock';
                                $pl->product_id = $bom->product_id;
                                $pl->quantity = (!empty($opening_stock[$key])) ? $opening_stock[$key] : '';
                                $pl->balance_quantity = $stockCheck->stock;
                                $pl->transaction_type = 'plus';
                                $pl->remark = 'Opening Stock added ';
                                $pl->save();
                            }
                        }
                    } else {
                        $stock = new Stock();
                        $stock->sku = $sku;
                        $stock->cid = $bom->product_id;
                        $stock->quantity = (!empty($opening_stock[$key])) ? $opening_stock[$key] : '';
                        $stock->stock = (!empty($opening_stock[$key])) ? $opening_stock[$key] : '';
                        $stock->unit = (!empty($unit_id[$key])) ? $unit_id[$key] : '';
                        $stock->store_id = $bom->store_id;
                        $stock->project_id = $bom->project_id;
                        $stock->company_id = $this->user->company_id;
                        $stock->save();

                        if ($stock) {
                            $pl = new ProductLog();
                            $pl->store_id = $bom->store_id;
                            $pl->project_id = $bom->project_id;
                            $pl->company_id = $this->user->company_id;
                            $pl->created_by = $this->user->id;
                            $pl->module_id = $bom->id;
                            $pl->module_name = 'opening_stock';
                            $pl->product_id = $bom->product_id;
                            $pl->quantity = (!empty($opening_stock[$key])) ? $opening_stock[$key] : '';
                            $pl->balance_quantity = (!empty($opening_stock[$key])) ? $opening_stock[$key] : '';
                            $pl->transaction_type = 'plus';
                            $pl->remark = 'Opening Stock added ';
                            $pl->save();
                        }
                    }
                }
            }
        }
        return Reply::success(__('Material updated successfully'));
    }

    public function showStoresStock(Request $request,$id, $sid)
    {

        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);

        $this->products = Stock::where('store_id',$sid)->where('project_id',$id)->get();
        $this->store = Store::where('id',$sid)->first();
        $this->units = Units::get();
        return view('admin.stores-projects.show-store-stock', $this->data);
    }
    public function stockProjectData (Request $request, $pid, $sid)
    {
        $pos = Stock::where('store_id', $sid)->where('project_id', $pid)->groupBy('cid')->get();
        $store_id = $sid;
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($pid, $user, $store_id,$request) {
                $ret = '';
                $ret .= '<a href="' . route('admin.inventory.purchase-history', [$row->cid, $store_id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('admin.stores.projects.updateStock', [$pid, $store_id,$row->cid,$row->id]) . '" class="btn btn-primary btn-circle"
                      data-toggle="tooltip" data-original-title="Update Stock"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('admin.stores.projects.productWiseLog', [$pid, $store_id, $row->cid]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="Product Log"><i class="fa fa-book" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'cid',
                function ($row) use ($pid, $store_id) {
                    $ret = '<a href="' . route('admin.stores.projects.productWiseLog', [$pid, $store_id, $row->cid]) . '" >'.get_local_product_name($row->cid).'</a>&nbsp;';
                    return $ret;
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'unit_id',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'est_qty',
                function ($row) use ($pid, $sid) {
                    return get_est_qty($pid, $sid, $row->cid);
                }
            )
            ->editColumn(
                'est_rate',
                function ($row) use ($pid, $sid) {
                    return get_est_rate($pid, $sid, $row->cid);
                }
            )
            ->editColumn(
                'indented_qty',
                function ($row) use ($pid, $sid) {
                    return get_indented_qty($pid, $sid, $row->cid);
                }
            )
//            ->editColumn(
//                'issued_project',
//                function ($row) {
//                    return get_pbrand_name($row->bid);
//                }
//            )
            ->editColumn(
                'stock',
                function ($row) use ($pid, $sid) {
                    return getProjectStoreStock($pid, $sid, $row->cid);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action','cid'])
            ->make(true);
    }

    public function indentCreate($leadID = null, Request $request, $pid, $sid)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->lproducts = Product::all();
        $this->projects = Project::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->pid = $pid;
        $this->sid = $sid;
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('admin.stores-projects.create-indent', $this->data);
    }

    public function indentCreateNew(Request $request, $pid, $sid)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);

        $this->stores = Store::where('company_id',$this->companyid)->first();
        $this->products = Bom::where('project_id',$pid)->where('store_id',$sid)->where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->lproducts = Product::where('company_id',$this->companyid)->get();

        $this->pid = $pid;
        $this->sid = $sid;
        $this->store = Store::where('id',$sid)->where('company_id',$this->companyid)->first();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        return view('admin.stores-projects.create-indent', $this->data);
    }

    public function indentGetBrands(Request $request){
        $pid = $request->pid;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        $pro = Product::where('id',$pid)->where('company_id',$this->companyid)->first();
        $brands = ProductBrand::where('company_id',$this->companyid)->get();
        $html = array();
        $html['estQty'] = get_est_qty($project_id, $store_id, $pid);
        $html['reqQty'] = get_req_qty($project_id, $store_id, $pid);
        $html['unit'] = $pro->unit_id;
        $html['unitData'] = get_unit_name($pro->unit_id);
        if(count($brands)){
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
            foreach($brands as $brand){
                $html['brands'] .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
            }
        }else{
            $html['brands'] = '<option value="">Select Brand</option>';
            $html['brands'] .= '<option value="add_brand">Add Brand</option>';
        }

        return $html;
    }
    public function indentBrand(Request $request)
    {
        $category = new ProductBrand();
        $category->name = $request->brand;
        $category->save();
        $bid = $category->id;
        $brand = ProductBrand::where('id', $bid)->first();
        $html = '<option value="'.$brand->id.'" selected>'.$brand->name.'</option>';
        return $html;
    }
    public function indentStoreTmp(Request $request){
        $sid = null;
        $html = array();
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }

            $store_id = $request->store_id;
            $project_id = $request->project_id;
            $tmpdata = new TmpIndent();
            $tmpdata->session_id = $sid;
            $tmpdata->cid = $request->cid;
            $tmpdata->bid = $request->bid;
            $tmpdata->qty = $request->qty;
            if(isset($request->unit)) {
                $tmpdata->unit = $request->unit;
            }
            $tmpdata->dated = $request->dated;
            $tmpdata->remark = $request->remark;
            $tmpdata->save();

        $allData = TmpIndent::where('session_id', $sid)->get();

        $html['data'] = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html['data'] .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html['data'] .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html['data'] .= '</tbody></table>';
        return $html;
    }

    public function indentDeleteTmp(Request $request){
        $sid = null;
        $store_id = $request->store_id;
        $project_id = $request->project_id;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        TmpIndent::where('id', $request->did)->delete();
        $allData = TmpIndent::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Estimated Quantity</th><th>Requested Quantity</th><th>Required Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_procat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.get_est_qty($project_id, $store_id, $data->cid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function indentStore(StoreIndentRequest $request)
    {

        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $sr = 1;
        $ind = Indent::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->indent_no);
            $sr = $in[2];
            $sr++;
        }
        $indent_no = 'IND/'.date("Y").'/'.$sr;
        $indent = new Indent();
        $indent->indent_no = $indent_no;
        $indent->store_id = $request->store_id;
        $indent->project_id = $request->project_id;
        $indent->remark = $request->remarkx;
        $indent->company_id = $this->user->company_id;
        $indent->save();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.stores.projects.indents',[$request->project_id, $request->store_id]));
    }

    public function indentEdit(Request $request, $pid, $sid, $id)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);

        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->lproducts = Bom::where('company_id',$this->companyid)->get();
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->pid = $pid;
        $this->sid = $sid;
        $this->store = Store::where('id',$sid)->first();
        if($request->session()->has('indentTmpDataSession')) {
            $ssid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $ssid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $ssid = null;
        if($request->session()->has('indentTmpDataSession')){
            $ssid = $request->session()->get('indentTmpDataSession');
        }else{
            $ssid = uniqid();
            $request->session()->put('indentTmpDataSession', $ssid);
        }
        $all_data = IndentProducts::where('indent_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $ssid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $ssid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('admin.stores-projects.edit-indent', $this->data);
    }

    public function indentUpdate(UpdateIndentRequest $request, $id)
    {
        dd($request->all());
        foreach ($all_data as $data){
            $indPro = new IndentProducts();
            $indPro->indent_id = $indent->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        return Reply::redirect(route('Indent Updated Successfully!!'));
    }

    public function indentDestroy($id)
    {
        DB::beginTransaction();
        Indent::destroy($id);
        IndentProducts::where('indent_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.indentDeleted'));
    }
    public function indentApprove($id, $val)
    {
        DB::beginTransaction();
        $ind = Indent::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('Indent updated successfully');
    }

    public function indentData(Request $request, $pid, $sid)
    {

        $indents = Indent::join('stores', 'stores.id', '=', 'indents.store_id');
        if($pid){
            $indents = $indents->where('stores.project_id', $pid);
        }
        if ($sid != 'all' && $sid != '') {
            $indents = $indents->where('store_id', $sid);
        }
        if ($request->status != 'all' && $request->status != '') {
            $indents = $indents->where('status', $request->status);
        }
        $indents = $indents->select(['indents.*'])->get();

        $user = Auth::user();
        return DataTables::of($indents)
            ->addColumn('action', function ($row) use ($user, $pid, $sid) {
                $ret = '';
                if($row->status == 0){
                    $ret .= '<a href="' . route('admin.stores.projects.indent.edit', [$pid, $sid, $row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }

                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="' . route('admin.stores.projects.indent.gconvert', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To RFQ"><i class="fa fa-send" aria-hidden="true"></i></a>&nbsp;';
                }
                //add Conditin for Grn View
                if($row->status != 3 && $row->approve == 1){
                    $ret .= '<a href="' . route('admin.indent.convertGrn', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Convert To GRN"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';

                }
                if($row->approve == 1) {
                    $ret .= '<a href="' . route('admin.stores.projects.convertProductIssue', [$row->project_id, $row->store_id, $row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="Issue Product"><i class="fa fa-plus" aria-hidden="true"></i></a>&nbsp;';
                }
                $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('admin.indent.viewIndent', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Indent"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending RFQ';
                    }else if($row->status == 1){
                        return 'Pending PO';
                    }else if($row->status == 2){
                        return 'Pending Purchase';
                    }else{
                        return 'Purchase Done';
                    }
                }
            )
            ->editColumn(
                'approve',
                function ($row) {
                    if($row->approve == 0){
                        return 'Pending Approval';
                    }else if($row->approve == 1){
                        return 'Approved';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function indentConvert($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        $all_data = IndentProducts::where('indent_id',$id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
        $this->indent = Indent::where('id', $id)->first();
        return view('admin.indent.convert', $this->data);
    }
    public function indentPostConvert($id, ConvertRfqRequest $request){

        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }
        $indent = Indent::find($id);
        $rfq = new Rfq();
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq->rfq_no = $rfq_no;
        $rfq->indent_no = $indent->indent_no;
        $rfq->store_id = $indent->store_id;
        $rfq->indent_id = $id;
        $rfq->project_id = $indent->project_id;
        $rfq->remark = $indent->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->save();
        //IndentProducts::where('indent_id', $id)->delete();
        $all_data = TmpIndent::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->indent_id = $id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        $indent->status = 1;
        $indent->save();
        TmpIndent::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    public function indentExport($status, $indent)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'indent')
            ->where('roles.company_id', company()->id)
            ->leftJoin('indent_details', 'users.id', '=', 'indent_details.user_id')
            ->select(
                'users.id',
                'indent_details.name',
                'indent_details.email',
                'indent_details.mobile',
                'indent_details.company_name',
                'indent_details.address',
                'indent_details.website',
                'indent_details.created_at'
            )
            ->where('indent_details.company_id', company()->id);
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }
        if ($indent != 'all' && $indent != '') {
            $rows = $rows->where('users.id', $indent);
        }
        $rows = $rows->get()->makeHidden(['image']);
        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];
        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }
        // Generate and return the spreadsheet
        Excel::create('indents', function ($excel) use ($exportArray) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('indents file');
            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function stores($id)
    {
        $this->userDetail = auth()->user();
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $isMember = ProjectMember::checkIsMember($id, $this->user->id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);
        // TODO::ProjectDeadline to do
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if ($this->project->deadline) {
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $minute = 0;
        $hour = intdiv($this->hoursLogged, 60);
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->stores = Store::where('project_id',$this->project->id)->get();
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);
        return view('admin.stores-projects.show-stores', $this->data);
    }
    public function indents($id, $sid)
    {
        $this->indents = Indent::where('project_id', $id)->where('id',$sid)->get();
        $this->stores = Store::where('project_id', $id)->get();
        $this->userDetail = auth()->user();
        $this->store = Store::where('id',$sid)->first();
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $isMember = ProjectMember::checkIsMember($id, $this->user->id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);
        // TODO::ProjectDeadline to do
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if ($this->project->deadline) {
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $minute = 0;
        $hour = intdiv($this->hoursLogged, 60);
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);
        return view('admin.stores-projects.show-indents', $this->data);
    }

    public function rfqs(Request $request, $pid, $sid)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $pid)->get();
        $this->userDetail = auth()->user();
        $this->store = Store::where('id',$sid)->first();
        $this->project = Project::findOrFail($pid)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $isMember = ProjectMember::checkIsMember($pid, $this->user->id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);
        // TODO::ProjectDeadline to do
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if ($this->project->deadline) {
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $minute = 0;
        $hour = intdiv($this->hoursLogged, 60);
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($pid, 10, $this->userDetail->id);
        return view('admin.stores-projects.show-rfqs', $this->data);
    }

    public function quotations(Request $request, $pid, $sid)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $pid)->get();
        $this->store = Store::where('id', $sid)->first();
        $this->userDetail = auth()->user();
        $this->project = Project::findOrFail($pid)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $isMember = ProjectMember::checkIsMember($pid, $this->user->id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);
        // TODO::ProjectDeadline to do
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if ($this->project->deadline) {
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $minute = 0;
        $hour = intdiv($this->hoursLogged, 60);
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($pid, 10, $this->userDetail->id);
        return view('admin.stores-projects.show-quotes', $this->data);
    }

    public function purchaseOrders($id)
    {
        $this->indents = Indent::all();
        $this->stores = Store::where('project_id', $id)->get();
        $this->userDetail = auth()->user();
        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;
        $isMember = ProjectMember::checkIsMember($id, $this->user->id);
        $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);
        $this->openTasks = Task::projectOpenTasks($this->project->id, $this->userDetail->id);
        $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);
        // TODO::ProjectDeadline to do
        $this->daysLeft = 0;
        $this->daysLeftFromStartDate = 0;
        $this->daysLeftPercent = 0;
        if ($this->project->deadline) {
            $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d') + ($this->project->deadline->diff(Carbon::now())->format('%m') * 30) + ($this->project->deadline->diff(Carbon::now())->format('%y') * 12);
            $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d') + ($this->project->deadline->diff($this->project->start_date)->format('%m') * 30) + ($this->project->deadline->diff($this->project->start_date)->format('%y') * 12);
            $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
        }
        $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);
        $minute = 0;
        $hour = intdiv($this->hoursLogged, 60);
        if (($this->hoursLogged % 60) > 0) {
            $minute = ($this->hoursLogged % 60);
            $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
        } else {
            $this->hoursLogged = $hour;
        }
        $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id', 'desc')->limit(10)->get();
        $this->activities = ProjectActivity::getProjectActivities($id, 10, $this->userDetail->id);
        return view('admin.stores-projects.show-po', $this->data);
    }

    public function getIndentData(Request $request){
        $poid = $request->pid;
        $store = $request->store_id;
        $project = $request->project_id;
        $podata = Indent::where('project_id',$project)->where('store_id',$store)->where('indent_no',$poid)->first();
        $products = Product::where('company_id',$this->companyid)->get();
        if(!empty($podata) && $podata->id !== null){
            $tmpData = IndentProducts::where('indent_id',$podata->id)->get();
        }
        $html = '<table class="table">
            <thead>
                <th>S.No.</th>
                <th>Product</th>
                <th>Unit</th>
                <th>Brand</th>
                <th>Required Quantity</th>
                <th>Required Date</th>
                <th>Issued Quantity</th>
                <th>To Be Issue Quantity</th>
            </thead>
        <tbody>';
        if(!empty($tmpData) && count($tmpData)){
            $i = 1;
            foreach($tmpData as $data){
                $html .= '<tr>
                    <td>'.$i.'</td>
                    <td><input type="text" readonly value="'.get_local_product_name($data->cid).'" class="form-control"><input type="hidden" name="product_id[]" value="'.$data->cid.'"> </td>
                    <td><input type="text" readonly value="'.get_unit_name($data->unit).'" class="form-control"><input type="hidden" name="unit_id[]" value="'.$data->unit.'"></td>
                    <td><input type="text" readonly value="'.get_pbrand_name($data->bid).'" class="form-control"><input type="hidden" name="brand[]" value="'.$data->bid.'"></td>
                    <td><input type="text" readonly value="'.$data->quantity.'" class="form-control"></td>
                    <td><input type="text" readonly value="'.$data->expected_date.'" class="form-control"></td>
                    <td><input type="text" readonly value="'.get_issued_qty($project,$store,$data->cid).'" class="form-control"></td>
                    <td><input type="number" name="quantity[]" placeholder="To Be Issue Quantity" class="form-control "></td>
                    </tr>';
                $i++;
            }
        }else {
            $html .= '<tr>
                           <td>1</td>
                           <td>
                                <div class="skuData" data-key="0" style="display: none;"></div>
                                <select class="selectpicker form-control product" name="product_id[]" data-key="0" data-style="form-control">
                                    <option value="">Please select Product</option>';
                                    foreach ($products as $category){
                                        $html .= '<option value="'.$category->id.'">'.$category->name.'</option>';
                                    }
                                '</select>
                            </td>
                            <td style="width: 15%;">
                                <input type="text" style="display: none;" readonly value="" class="form-control unitId" name="unit_id[]" data-key="0">
                                <input type="text" readonly class="form-control unitName" value="" name="unitName" data-key="0">
                            </td>
                            <td><input type="text" readonly class="form-control dataBrand" data-key="0"></td>
                            <td><input type="text" readonly class="form-control dataIqty" data-key="0"></td>
                            <td class="dataQty" data-key="0"><input type="number" name="quantity[]" placeholder="To Be Issue Quantity" class="form-control"></td>
                           
                        </tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function productData(Request $request){
        $product_id = $request->pid;
        $project_id = $request->project_id;
        $store_id = $request->store_id;
        $br = Stock::where('project_id',$project_id)->where('store_id',$store_id)->where('cid',$product_id)->first();
        $html = array();
        $html['brand'] = get_pbrand_name($br->bid);
        $html['unit_id'] = $br->unit;
        $html['unit_name'] = get_unit_name($br->unit);
        $html['brand'] = get_pbrand_name($br->bid);
        $html['iqty'] = get_issued_qty($project_id,$store_id,$product_id);
        return $html;
    }

    public function productPopup(Request $request){

        $pid = $request->pid;
        $project_id = $request->project_id;
        $store_id = $request->store_id;
        $key = $request->key;
        $stocks = Stock::where('project_id',$project_id)->where('store_id',$store_id)->where('cid',$pid)->get();
        //dd(count($stocks));
        $html = '<table class="table">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Product Name</td>
                    <td>Available Stock</td>
                </tr>
            </thead>
            <tbody>';
                if($stocks) {
                    foreach ($stocks as $stock) {
                        $html .= '<tr>
                        <td>
                            <input type="radio" class="sel_product" name="sel_product" value="' . $stock->cid . '" data-unit-id="'.$stock->unit.'" data-sku-id="' . $stock->sku . '" data-id="' . $stock->id . '" data-key="' . $key . '">
                        </td>
                        <td>' . get_local_product_name($stock->cid) . '</td>
                        <td>' . $stock->stock . '</td>
                    </tr>';
                    }
                }
        $html .= '</tbody></table>';
        if(count($stocks)>0) {
            return $html;
        }else{
            return '<span style="text-align: center;">No Stock Available</span>';
        }
    }

    public function convertProductIssue(Request $request, $pid, $sid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->products = ProductCategory::get();
        $this->units = Units::get();
        $this->store = Store::where('id',$sid)->first();
        $indent = Indent::where('id',$id)->first();
        $this->ino = $indent->indent_no;
        $this->issuedProducts = ProductIssue::where('project_id',$pid)->groupBy('unique_id')->get();
        $this->podata = Indent::where('id',$id)->first();
        $this->tmpData = IndentProducts::where('indent_id',$this->podata->id)->get();
        return view('admin.stores-projects.convert-product-issue', $this->data);
    }

    public function productLogData(Request $request, $pid, $sid)
    {
        if(!empty($request->product_id)) {
            $logs = ProductLog::where('project_id', $pid)->where('store_id', $sid)
                ->where('product_id', $request->product_id)
                ->orderBy('id', 'ASC')->get();
            $openingstock = ProductLog::where('project_id', $pid)->where('store_id', $sid)
                ->where('product_id', $request->product_id)->where('module_name','opening_stock')
                ->orderBy('id', 'DESC')->first();
            $html = array();
            $totalIssued = 0;
            $totalReceived = 0;
            $totalBalance = 0;
            $tableData = '';
            foreach ($logs as $key=>$log) {
                $stock = Stock::where('project_id', $pid)->where('store_id', $sid)
                    ->where('cid', $log->product_id)
                    ->orderBy('id', 'DESC')->first();
                $k = $key+1;
                $bal = 0;
                if ($log->transaction_type == 'plus') {
                    $totalReceived += (int)$log->quantity;
                } else {
                    $totalIssued += (int)$log->quantity;
                }
                if(!empty($openingstock)){
                    $tableData .= '<tr>
                        <td>-</td>
                        <td>-</td>
                        <td>
                            Opening Stock added
                        </td>
                        <td>-</td>
                        <td><div class="bg-success" style="text-align: center; color: white;">'.$openingstock->quantity.'</div></td>
                        <td>-</td>
                    </tr>';
                }
                $tableData .= '<tr>
                    <td>'.$k.'</td>
                    <td>'.Carbon::parse($log->created_at)->format('d-m-Y').'</td>
                    <td>';
                    if($log->module_name == 'update-stock'){
                        if($log->transaction_type == 'minus') {
                            $tableData .= 'Shortage';
                        }else {
                            $tableData .= 'Access';
                        }
                    }else {
                        if ($log->transaction_type == 'minus') {
                            $tableData .= 'Issued';
                        } else {
                            $tableData .= 'Received';
                        }
                    }
                    $tableData .= ' By '.get_user_name($log->created_by).'<br>';
                    $tableData .= (!empty($log->remark)) ? 'Remark : '.$log->remark : '';
                    $tableData .= '</th>
                    <td>'; ($log->transaction_type == 'minus') ? $tableData .= '<div class="bg-danger" style="text-align: center; color: white;">'.$log->quantity.'</div>' : $tableData .= '-';  $tableData .= '</td>
                    <td>'; ($log->transaction_type == 'plus') ? $tableData .= '<div class="bg-success" style="text-align: center; color: white;">'.$log->quantity.'</div>' : $tableData .= '-';  $tableData .= '</td>
                    <td>'.$log->balance_quantity.'</td>
                </tr>';
                if(!empty($stock)) {
                    $totalBalance += $stock->stock;
                }
            }
            $html['html'] = $tableData;
            $html['issued'] = $totalIssued;
            $html['received'] = $totalReceived;
            $html['balance'] = $totalBalance;
            return $html;
        }
    }

    public function productWiseLog(Request $request, $pid, $sid, $id)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->products = Bom::where('project_id',$pid)->where('store_id',$sid)->where('product_id', $id)->groupBy('product_id')->first();
        $this->store = Store::where('id',$sid)->first();
        $this->issuedProducts = ProductIssue::where('project_id',$pid)->groupBy('unique_id')->get();
        $this->productLogs = ProductLog::where('project_id', $pid)->where('store_id', $sid)
            ->where('product_id', $id)->where('company_id',$this->companyid)
            ->orderBy('id', 'ASC')->get();
        $this->openingstock = ProductLog::where('project_id', $pid)->where('store_id', $sid)
            ->where('product_id', $id)->where('module_name','opening_stock')
            ->orderBy('id', 'DESC')->get();
        $this->product = Product::where('id', $id)->first();
        $this->stock = Stock::where('project_id', $pid)->where('store_id', $sid)->where('cid', $id)
            ->sum('stock');

        return view('admin.stores-projects.product-wise-log', $this->data);
    }

    public function updateStock(Request $request, $pid, $sid, $cid,$id)
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->projectId = $pid;
        $this->sid = $sid;
        $this->cid = $cid;
        $this->stockid = $id;
        $this->products = Bom::where('project_id',$pid)->where('store_id',$sid)->groupBy('product_id')->get();
        $this->store = Store::where('id',$sid)->first();
        $this->issuedProducts = ProductIssue::where('project_id',$pid)->groupBy('unique_id')->get();
        $this->productLogs = ProductLog::where('project_id', $pid)->where('store_id', $sid)
            ->where('product_id', $cid)
            ->orderBy('id', 'DESC')->get();
        $this->stock = Stock::where('id',$id)->orderBy('id', 'DESC')->first();
        $this->totalstock = Stock::where('cid',$cid)->where('project_id',$pid)->where('store_id', $sid)->sum('stock');

        return view('admin.stores-projects.update-stock', $this->data);
    }

    public function storeUpdateStock(Request $request, $pid, $sid,$id){
        //dd($id);
        if(!empty($request->quantity)){
            $type = '';
            $stock = Stock::findOrFail($id);
            if($request->type == 'shortage'){
                $type = 'minus';
                $stock->stock = $stock->stock - (int)$request->quantity;
            }
            if($request->type == 'access'){
                $type = 'plus';
                $stock->stock = $stock->stock + (int)$request->quantity;
            }
            $stock->save();

            $pl = new ProductLog();
            $pl->store_id = $stock->store_id;
            $pl->project_id = $stock->project_id;
            $pl->company_id = $this->user->company_id;
            $pl->created_by = $this->user->id;
            $pl->module_id = $stock->id;
            $pl->module_name = 'update-stock';
            $pl->product_id = $stock->cid;
            $pl->quantity = $request->quantity;
            $pl->balance_quantity = $stock->stock;
            $pl->transaction_type = $type;
            $pl->remark = $request->remark;
            $pl->save();

            if($pl){
                return Reply::redirect(route('admin.stores.projects.showStoresStock',[$pid, $sid]), __('Updated Successfully'));
            }
        }
    }

    public function deleteBom(Request $request,$id){
        $user = $this->user;
        $projectde = Indent::where('project_id', $request->projectid)->where('store_id', $request->storeid)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please delete Indent Information.';
        }
        $projectde = Rfq::where('project_id', $request->projectid)->where('store_id', $request->storeid)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please delete Rfq first.';
        }
        $rfq  = Rfq::join('stores', 'stores.id', '=', 'rfqs.store_id')->where('rfqs.project_id', $request->projectid)->where('rfqs.store_id', $request->storeid)->first();
        if($rfq){
            $projectde = Quotes::where('rfq_id',$rfq->id)->first();
            if (!empty($projectde->id)) {
                $errormsg = 'Please Remove Quotation first.';
            }

            $projectde  = PurchaseOrder::where('project_id',$request->projectid)->where('rfq_id',$rfq->id)->first();
            if (!empty($projectde->id)) {
                $errormsg = 'Please Remove Purchase Order first.';
            }
        }

        $projectde  = PurchaseInvoice::where('project_id', $request->projectid)->where('store_id', $request->storeid)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Invoice first.';
        }
        $projectde  = ProductReturns::where('project_id', $request->projectid)->where('store_id', $request->storeid)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Retrun first.';
        }
        $projectde  = ProductIssue::where('project_id', $request->projectid)->where('store_id', $request->storeid)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Issue first.';
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            $bom  = Bom::find($id);
            if($bom){
                $stockupdate = Stock::where('cid',$bom->product_id)->where('project_id', $request->projectid)->where('store_id', $request->storeid)->delete();
                $rolog = ProductLog::where('product_id',$bom->product_id)->where('project_id', $request->projectid)->where('store_id', $request->storeid)->delete();
            }
            $pl = Bom::where('id',$id)->delete();
            if($pl){
                return Reply::success(__('Deleted Successfully'));
            }
        }

    }

    public function applyIssuedFilter(Request $request, $pid, $sid){
        $fromdate = !empty($request->start_date) ? Carbon::parse($request->start_date)->format('Y-m-d') : '';
        $todate = !empty($request->end_date) ? Carbon::parse($request->end_date)->format('Y-m-d') : '';
        $issued_by = $request->issued_by;
        $issuedProducts = ProductIssue::where('project_id',$pid)->where('store_id',$sid);
        if(!empty($issued_by)){
            $issuedProducts = $issuedProducts->where('issued_by',$issued_by);
        }
        if (!empty($fromdate) && !empty($todate)) {
            $issuedProducts = $issuedProducts->where('created_at', '>=', $fromdate." 00:00:00")->where('created_at', '<=', $todate." 23:59:59");
        }
        $issuedProducts = $issuedProducts->groupBy('unique_id')->get();
        $html = '';
        foreach($issuedProducts as $key=>$product) {
            $k = $key + 1;
            $qcount = ProductIssue::where('unique_id', $product->unique_id)->get();
            $html .= '<tr>
                <td>' . $k . '</td>
                <td>' . $product->unique_id . '</td>
                <td>' . count($qcount) . '</td>
                <td>' . get_user_name($product->issued_by) . '</td>
                <td>' . Carbon::parse($product->created_at)->format('d-m-Y') . '</td>
                <td>
                    <a href="' . route('admin.stores.projects.issuedProductDetail', [$pid, $product->unique_id]) . '" class="btn btn-success btn-circle"
                       data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="View Issued Products">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                    <a href="' . route('admin.stores.projects.returnIssuedProduct', [$pid, $sid, $product->unique_id]) . '" class="btn btn-warning btn-circle"
                       data-toggle="tooltip" data-task-id="' . $product->unique_id . '" data-original-title="Return Issued Products">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>';
        }
        return $html;
    }
}

