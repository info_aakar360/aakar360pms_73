<?php

namespace App\Http\Controllers\Admin;

use App\AppProject;
use App\ClientDetails;
use App\Employee;
use App\Exports\SupplierExport;
use App\Helper\Reply;
use App\Http\Requests\Admin\Client\StoreClientRequest;
use App\Http\Requests\Admin\Client\UpdateClientRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\Project;
use App\ProjectMember;
use App\ProjectPermission;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageClientsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.clients';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('clients', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = AppProject::whereIn('id',$projectlist)->get();
        $this->clients = Employee::where('company_id',$user->company_id)->where('user_type','client')->get();
        return view('admin.clients.index', $this->data);
    }

    /**p
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = AppProject::whereIn('id',$projectlist)->get();
        return view('admin.clients.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        $user = $this->user;
        $company = company();
        /*  if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
              return Reply::error(__('messages.upgradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
          }

          if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
              return Reply::error(__('messages.downGradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
          }*/
        $projectid = $request->project_id;
        $sharetoproject = $request->sharetoproject;
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile;

        if(!checkMobile($mobile)){
            $mobile = str_replace(" ", "", $mobile);
            $mobile = str_replace("-", "", $mobile);
            $len = strlen($mobile);
            $c = $len - 10;
            if($c){
                $mobile = substr($mobile, $c, 10);
            }
        }

        
        if($mobile==$user->mobile){
            return Reply::error('Member cannot be created with same mobile number');
        }
        if(!empty($projectid)){
            $projectdetails = AppProject::where('id',$projectid)->first();
            $usercompany = $projectdetails->company_id;
        }else{
            $usercompany = $user->company_id;
        }
        $prevemp = Employee::where('company_id',$usercompany)->where('mobile',$mobile)->where('user_type','client')->first();
        if(!empty($prevemp)){
            return Reply::error( __('messages.clientExists'));
        }

        $alruser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('mobile',$mobile)->orWhere('email',$email)->first();
        if(empty($alruser->id)){
            $alruser = new User();
            $alruser->name = $name;
            $alruser->email = $email ? : '';
            $alruser->mobile = $mobile;
            $alruser->login = 'disable';
            $alruser->save();
        }

        $employee = new Employee();
        $employee->company_id = $usercompany;
        $employee->added_by = $this->user->id;
        $employee->user_id = $alruser->id;
        $employee->name = $name;
        $employee->email = $email ? : '';
        $employee->mobile = $mobile;
        $employee->user_type = 'client';
        $employee->gender = $request->gender;
        $employee->status = 'active';
        $employee->save();

        $user_type = "Client";
        $incomegroup = IncomeExpenseGroup::where('name',$user_type)->where('company_id',$usercompany)->first();
        $items = new IncomeExpenseHead();
        $items->company_id = $usercompany;
        $items->name = $request->name;
        $items->income_expense_type_id = !empty($incomegroup) ? $incomegroup->income_type : '';
        $items->income_expense_group_id = !empty($incomegroup) ? $incomegroup->id : '';
        $items->created_by = $user->id;
        $items->amount = !empty($request->amount) ? $request->amount : 0;
        $items->voucherdate = !empty($request->voucher_date) ? date('Y-m-d',strtotime($request->voucher_date)) : '';
        $items->particulars = $request->particulars;
        $items->emp_id = $employee->id;
        $items->save();

        if(!empty($projectid)){
            $prevproject = ProjectMember::where('user_id',$employee->user_id)->where('employee_id',$employee->id)->where('project_id',$projectid)->first();
            if(empty($prevproject->id)){
                $alreuser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->find($employee->user_id);
                $projectdetails = AppProject::where('id',$projectid)->first();
                $level = 0;
                if($projectdetails->added_by==$user->id){
                    $level = 1;
                }else{
                    $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectdetails->id)->first();
                    $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                    $level = $prolevel+1;
                }
                $pm = new ProjectMember();
                $pm->company_id = $projectdetails->company_id;
                $pm->project_id = $projectdetails->id;
                $pm->user_id = $employee->user_id;
                $pm->employee_id = $employee->id;
                $pm->user_type = 'client';
                $pm->assigned_by = $user->id;
                $pm->share_project = '0';
                $pm->level = $level;
                if($sharetoproject=='1'){
                    $pm->share_project = '1';
                    if(!empty($alreuser)&&!empty($alreuser->fcm)){
                        $notifmessage['title'] = 'Project Shared';
                        $notifmessage['body'] = 'You have been added to ' . ucwords(get_project_name($projectid)) . ' project by ' . $user->name;
                        $notifmessage['activity'] = 'projects';
                        sendFcmNotification($alreuser->fcm, $notifmessage);
                    }
                }
                $pm->save();
                return Reply::redirect(route('admin.clients.index'));
//                , __('messages.userAssigned'));

            }else{
                return Reply::redirect(route('admin.clients.index'), __('messages.useralreadyAssigned'));

            }
        }
        return Reply::redirectWithError(route('admin.clients.create'),'Client already exists.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->client = Employee::findOrFail($id);
        return view('admin.clients.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $client = Employee::find($id);
        if(empty($client)){
            return redirect(route('admin.clients.show'));
        }
        $this->client = Employee::find($id);

        return view('admin.clients.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, $id)
    {

        $client = Employee::find($id);
        $client->name = $request->input('name');
        $client->email = $request->input('email');
        $client->mobile = $request->input('mobile');
        $client->gender = $request->input('gender');
        $client->save();

        $usertype = 'Client';
        $items = IncomeExpenseHead::where('company_id',$client->company_id)->where('emp_id',$client->id)->first();
        if(empty($items)){
            $incomegroup = IncomeExpenseGroup::where('name',$usertype)->where('company_id',$client->company_id)->first();
            $items = new IncomeExpenseHead();
            $items->company_id = $client->company_id;
            $items->emp_id = $client->id;
            $items->income_expense_type_id = $incomegroup->income_type;
            $items->income_expense_group_id = $incomegroup->id;
        }
        $items->name = $request->name;
        $items->save();

        return Reply::redirect(route('admin.clients.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::where('id',$id)->first();
        $projectmember = ProjectMember::where('employee_id',$id)->first();
        if(!empty($projectmember->id)){
            $projectmember->delete();
        }
        $employee->delete();
        ProjectPermission::where('company_id',$employee->company_id)->where('user_id',$employee->user_id)->where('project_id',$projectmember->project_id)->delete();

        return Reply::success(__('messages.clientDeleted'));
    }

    public function data(Request $request)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        if(!empty($request->projects)){
            $projectlist = explode(',',$request->projects);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('employee.company_id',$companylist)
            ->where('employee.user_type','client')->get();
        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.clients.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
 
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="' . route('admin.clients.projects', $row->id) . '">' . ucfirst($row->name) . '</a>';
                }
            )->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )->editColumn(
                'mobile',
                function ($row) {
                    return $row->mobile;
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if ($row->status == 'active') {
                        return '<label class="label label-success">' . __('app.active') . '</label>';
                    } else {
                        return '<label class="label label-danger">' . __('app.deactive') . '</label>';
                    }
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function showProjects($id)
    {

        $this->client = User::fromQuery('SELECT users.id, client_details.company_id FROM users JOIN client_details ON client_details.user_id = users.id WHERE client_details.user_id = ' . $id)->first();

        if (!$this->client) {
            abort(404);
        }

        $this->clientDetail = ClientDetails::where('user_id', '=', $this->client->id)->first();

        if (!is_null($this->clientDetail)) {
            $this->clientDetail = $this->clientDetail->withCustomFields();
            $this->fields = $this->clientDetail->getCustomFieldGroupsWithFields()->fields;
        }

        return view('admin.clients.projects', $this->data);
    }
    public function showInvoices($id)
    {

        $this->client = User::with('client_detail')->fromQuery('SELECT users.id, client_details.company_id FROM users JOIN client_details ON client_details.user_id = users.id WHERE client_details.user_id = ' . $id)->first();

        $this->clientDetail = $this->client ? $this->client->client_detail : abort(404);

        if (!is_null($this->clientDetail)) {
            $this->clientDetail = $this->clientDetail->withCustomFields();
            $this->fields = $this->clientDetail->getCustomFieldGroupsWithFields()->fields;
        }

        $this->invoices = Invoice::select('invoices.invoice_number', 'invoices.total', 'currencies.currency_symbol', 'invoices.issue_date', 'invoices.id')
            ->leftJoin('projects', 'projects.id', '=', 'invoices.project_id')
            ->join('currencies', 'currencies.id', '=', 'invoices.currency_id')
            ->where(function ($query) use ($id) {
                $query->where('projects.client_id', $id)
                    ->orWhere('invoices.client_id', $id);
            })
            ->get();

        return view('admin.clients.invoices', $this->data);
    }

    public function export($status, $client)
    {
        /*$rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'client')
            ->where('roles.company_id', company()->id)
            ->leftJoin('client_details', 'users.id', '=', 'client_details.user_id')
            ->select(
                'users.id',
                'client_details.name',
                'client_details.email',
                'client_details.mobile',
                'client_details.company_name',
                'client_details.address',
                'client_details.website',
                'client_details.created_at'
            )
            ->where('client_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($client != 'all' && $client != '') {
            $rows = $rows->where('users.id', $client);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('clients', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('clients file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');*/
        return Excel::download(new SupplierExport, 'Supplier.xlsx');
    }

    public function gdpr($id)
    {
        $this->client = User::withoutGlobalScope('active')->findOrFail($id);
        $this->clientDetail = ClientDetails::where('user_id', '=', $this->client->id)->first();
        $this->allConsents = PurposeConsent::with(['user' => function ($query) use ($id) {
            $query->where('client_id', $id)
                ->orderBy('created_at', 'desc');
        }])->get();

        return view('admin.clients.gdpr', $this->data);
    }

    public function consentPurposeData($id)
    {
        $purpose = PurposeConsentUser::select('purpose_consent.name', 'purpose_consent_users.created_at', 'purpose_consent_users.status', 'purpose_consent_users.ip', 'users.name as username', 'purpose_consent_users.additional_description')
            ->join('purpose_consent', 'purpose_consent.id', '=', 'purpose_consent_users.purpose_consent_id')
            ->leftJoin('users', 'purpose_consent_users.updated_by_id', '=', 'users.id')
            ->where('purpose_consent_users.client_id', $id);

        return DataTables::of($purpose)
            ->editColumn('status', function ($row) {
                if ($row->status == 'agree') {
                    $status = __('modules.gdpr.optIn');
                } else if ($row->status == 'disagree') {
                    $status = __('modules.gdpr.optOut');
                } else {
                    $status = '';
                }

                return $status;
            })
            ->make(true);
    }

    public function saveConsentLeadData(SaveConsentUserDataRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $consent = PurposeConsent::findOrFail($request->consent_id);

        if ($request->consent_description && $request->consent_description != '') {
            $consent->description = $request->consent_description;
            $consent->save();
        }

        // Saving Consent Data
        $newConsentLead = new PurposeConsentUser();
        $newConsentLead->client_id = $user->id;
        $newConsentLead->purpose_consent_id = $consent->id;
        $newConsentLead->status = trim($request->status);
        $newConsentLead->ip = $request->ip();
        $newConsentLead->updated_by_id = $this->user->id;
        $newConsentLead->additional_description = $request->additional_description;
        $newConsentLead->save();

        $url = route('admin.clients.gdpr', $user->id);

        return Reply::redirect($url);
    }
}
