<?php

namespace App\Http\Controllers\Admin;

use App\FileManager;
use App\Helper\Reply;
use App\Http\Requests\StoreFileLink;
use App\Notifications\FileUpload;
use App\ProjectCategory;
use App\ProjectFile;
use App\Project;
use App\ProjectMember;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use DB;

class ManageDrawingsController extends AdminBaseController
{

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    /**
     * ManageProjectFilesController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.drawings';
        $this->activeMenu = 'pms';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $allprojects = $this->projectslist;
        $userid = $user->id;
        foreach ($allprojects as $allproject){
            $prfilemanager =  FileManager::where('project_id',$allproject->id)->where('filename',$allproject->project_name)->where('type','folder')->where('parent','0')->where('pathtype','outline')->orderBy('type', 'DESC')->first();
            if(empty($prfilemanager->id)){
                $newfolder = new FileManager();
                $newfolder->project_id = $allproject->id;
                $newfolder->filename = $allproject->project_name;
                $newfolder->user_id = $userid;
                $newfolder->type = 'folder';
                $newfolder->parent = '0';
                $newfolder->pathtype = 'outline';
                $newfolder->save();
            }
        }

        $parent = 0;
        $filemanager = array();
        if(!empty($_GET['path'])){
            $parent = $_GET['path'];
            $filemanager = \App\FileManager::where('id',$parent)->first();
        }
        $this->projectAll = $allprojects;
        $this->files = FileManager::whereIn('project_id',$projectlist)->where('parent',$parent)->where('user_id',$userid)->where('pathtype','outline')->orderBy('filename', 'ASC')->get();
        $this->foldername = FileManager::whereIn('project_id',$projectlist)->where('id',$parent)->where('user_id',$userid)->where('pathtype','outline')->orderBy('filename', 'ASC')->get();
        $this->categories = ProjectCategory::all();
        $this->filemanager = $filemanager;

        return view('admin.projects.project-files.show', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = new FileManager();
        $file->user_id = $this->user->id;
        $file->company_id = $this->user->company_id;
        $file->name = $request->name;
        $file->description = $request->description;
        $file->revisionfileid = $request->revisionid ?: 0;
        $file->filename = '';
        $file->external_link = '';
        $file->parent = $request->parent;
        $file->project_id = $request->project_id;
        $file->pathtype = $request->pathtype ?: 'outline';
        $file->foldertype =  'public';
        $file->save();
        return Reply::dataOnly(['fileID' => $file->id]);
    }
    public function storeImage(Request $request)
    {
        /*if(isset($request->project_id)) {
            $this->project = Project::with('members', 'members.user')->findOrFail($request->project_id);
        }*/
        $fileid = $request->file_id;
        $projectid = $request->project_id;
        if ($request->hasFile('file')) {
            $storage = storage();
            if($fileid){
                $file = FileManager::find($fileid);
            }else{
                $file = new FileManager();
            }
            $file->user_id = $this->user->id;
            if(isset($request->project_id)) {
                $file->project_id = $request->project_id;
            }
            $folderid = $file->parent;
            $pathtype =  'outline';
            $npid = 0;
            switch($storage) {
                case 'local':
                    if(!empty($projectid)&&!empty($folderid)) {
                        $x = 1;
                        $parentfolder = '';
                        $pid = $folderid;
                        if($pid){
                            $pn = FileManager::where('project_id', $projectid)->where('pathtype', $pathtype)->where('id', $pid)->where('type', 'folder')->first();
                            $npid = 0;
                            if($pn === null){
                                while($pid != 0){
                                    $pnx = FileManager::where('id', $pid)->where('pathtype', $pathtype)->where('type', 'folder')->first();
                                    $pid =$pnx->parent;
                                }
                                $npid = 0;
                                while($x==1) {
                                    $pn = FileManager::where('id', $pnx->id)->where('pathtype', $pathtype)->where('type', 'folder')->first();
                                    if($pn === null){
                                        $x=0;
                                    }else{
                                        $pnn = $pn->replicate();
                                        $pnn->project_id = $projectid;
                                        $pnn->parent = $npid;
                                        $pnn->save();
                                        $pnx = FileManager::where('project_id', $pnx->project_id)->where('pathtype', $pathtype)->where('type', 'folder')->where('parent', $pnx->id)->first();
                                        $npid = $pnn->id;
                                        if($pnx === null){
                                            $x=0;
                                        }
                                    }
                                }
                            }
                            $x=1;
                            while($x==1) {
                                $parentname = FileManager::where('id', $pid)->where('pathtype', $pathtype)->first();
                                if($parentname !== null){
                                    if($parentfolder != '') {
                                        $parentfolder = $parentname->id . '/' . $parentfolder;
                                    }else{
                                        $parentfolder= $parentname->id;
                                    }
                                    $pid = $parentname->parent;
                                }else{
                                    $x = 0;
                                }
                            }
                        }
                        $image =  $request->file;
                        $extension = $image->getClientOriginalExtension();
                        if(!empty($request->extension)){
                            $extension = $request->extension;
                        }
                        $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentfolder;
                        if (!file_exists( ''.$destinationPath)) {
                            mkdir( ''.$destinationPath, 0777, true);
                        }
                        $image->storeAs($destinationPath, $image->hashName());
                        /* $img = Image::make($im    age->getRealPath());
                         $img->save($destinationPath.'/'.$image->hashName());*/

                        if($extension=='png'||$extension=='jpg'||$extension=='jpeg'){

                            $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentfolder.'/thumbnail';
                            if (!file_exists(''.$destinationPath)) {
                                mkdir(''.$destinationPath, 0777, true);
                            }
                            $img1 = Image::make($image->getRealPath());
                            $img1->resize(100, 100, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save(''.$destinationPath.'/'.$image->hashName());
                        }
                    }else {
                        $parentname =  FileManager::where('pathtype', $pathtype)->where('id', $folderid)->first();
                        $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentname->id;
                        $request->file->move($destinationPath, $request->file->hashName());
                    }
                    break;
                case 's3':
                    if(!empty($request->project_id)&&!empty($folderid)) {
                        $x = 1;
                        $parentfolder = '';
                        $pid = $folderid;
                        if($pid){
                            $pn = FileManager::where('project_id', $request->project_id)->where('id', $pid)->where('type', 'folder')->first();
                            $npid = 0;
                            if($pn === null){
                                while($pid != 0){
                                    $pnx = FileManager::where('id', $pid)->where('type', 'folder')->first();
                                    $pid =$pnx->parent;
                                }
                                $npid = 0;
                                while($x==1) {
                                    $pn = FileManager::where('id', $pnx->id)->where('type', 'folder')->first();
                                    if($pn === null){
                                        $x=0;
                                    }else{
                                        $pnn = $pn->replicate();
                                        $pnn->project_id = $request->project_id;
                                        $pnn->parent = $npid;
                                        $pnn->save();
                                        $pnx = FileManager::where('project_id', $pnx->project_id)->where('type', 'folder')->where('parent', $pnx->id)->first();
                                        $npid = $pnn->id;
                                        if($pnx === null){
                                            $x=0;
                                        }
                                    }
                                }
                            }
                            $x=1;
                            while($x==1) {
                                $parentname = FileManager::where('id', $pid)->first();
                                if($parentname !== null){
                                    if($parentfolder != '') {
                                        $parentfolder = $parentname->id . '/' . $parentfolder;
                                    }else{
                                        $parentfolder= $parentname->id;
                                    }
                                    $pid = $parentname->parent;
                                }else{
                                    $x = 0;
                                }
                            }
                        }
                        $image =  $request->file;
                        $extension = $image->getClientOriginalExtension();
                        if(!empty($request->extension)){
                            $extension = $request->extension;
                        }
                        Storage::disk('s3')->putFileAs('project-files/'.$projectid.'/'.$parentfolder, $image, $image->hashName(), 'public');

                        if($extension=='png'||$extension=='jpg'||$extension=='jpeg'){
                            $img = Image::make($image->getRealPath());
                            $img->resize(100, 100, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                            Storage::disk('s3')->put('project-files/'.$projectid.'/'.$parentfolder.'/thumbnail/'.$image->hashName(), $img->stream()->detach());
                        }
                    }else {
                        $parentname = FileManager::where('id', $folderid)->first();
                        Storage::disk('s3')->putFileAs('project-files/'.$projectid.'/'. $parentname->id, $request->file, $request->file->hashName(), 'public');
                    }
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', 'project-files')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('project-files');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', $request->parent)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->parent)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $file->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());

                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $file->dropbox_link = $dropboxResult['url'];
                    break;
            }

            $file->filename = $request->file->getClientOriginalName();
            $file->hashname = $request->file->hashName();
            $file->size = $request->file->getSize();
            $file->pathtype = $request->pathtype ?: 'outline';
            $file->type =  'file';
            $file->foldertype =  'public';
            $file->save();
            return Reply::success(__('messages.fileUploaded'));
        }

        /*$parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $pathtype = $request->pathtype ?: 'photos';
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->where('pathtype',$pathtype)->orderBy('type', 'DESC')->get();
        if($request->view == 'list') {
            $view = view('admin.manage-photos.ajax-list', $this->data)->render();
        } else {
            $view = view('admin.manage-photos.thumbnail-list', $this->data)->render();
        }
        return redirect()->back();*/
    }
    public function revisionFiles(Request $request, $id,$fileid){
        $user = $this->user;
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $b = FileManager::where('id',$fileid)->orderby('id','desc')->get();
        $a = FileManager::where('parent',$parent)->where('project_id',$id)->where('pathtype','outline')->where('revisionfileid',$fileid)->orderby('id','desc')->get();
        $this->files = $a->merge($b);
        $this->foldername = FileManager::where('id',$parent)->first();
        return view('admin.projects.project-files.revisionfiles', $this->data);
    }
    public function storeMultiple(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $file = new FileManager();
                $file->user_id = $this->user->id;
                if(isset($request->project_id)) {
                    $file->project_id = $request->project_id;
                }
                switch($storage) {
                    case 'local':
                        if(isset($request->project_id)) {
                            $proname = FileManager::where('project_id',$request->project_id)->where('parent','0')->first();
                            $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                            $parentfolder = $parentname->id;

                            $fileData->storeAs('user-uploads/project-files/'.$proname->id.'/'.$parentfolder, $fileData->hashName());
                        }else {
                            $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                            $fileData->storeAs('user-uploads/project-files/' . $parentname->id, $fileData->hashName());
                        }
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('project-files/'.$request->parent, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'project-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('project-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->parent)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->parent)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->type = 'file';
                $file->revisionfileid = $request->revisionid;
                $file->pathtype = $request->pathtype ?: 'outline';
                $file->foldertype =  'public';
                $file->parent = $request->parent;
                $file->save();
                if(isset($request->project_id)) {
                    $this->logProjectActivity($request->project_id, __('messages.newFileUploadedToTheProject'));
                }
            }

        }
        return redirect(route('admin.manage-files.index'), __('modules.projects.projectUpdated'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->project = Project::findOrFail($id);
        return view('admin.files.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $file = FileManager::findOrFail($id);
        FileManager::where('parent',$file->id)->delete();
        FileManager::where('revisionfileid',$file->id)->delete();
        $file->delete();
        return Reply::success(__('messages.fileDeleted'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function download($id) {
        $storage = storage();
        $file = FileManager::findOrFail($id);
        switch($storage) {
            case 'local':
                $parent = FileManager::where('id',$file->parent)->first();
                if($parent->parent !== '0'){
                    $second = FileManager::where('id',$parent->parent)->first();
                    return response()->download('user-uploads/project-files/'.$second->id.'/'.$parent->id.'/'.$file->hashname, $file->filename);
                }
//                return response()->download('user-uploads/project-files/'.$file->parent.'/'.$file->hashname, $file->filename);
                break;
            case 's3':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $fs = Storage::getDriver();
                $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                return Response::stream(function() use($stream) {
                    fpassthru($stream);
                }, 200, [
                    "Content-Type" => $ext,
                    "Content-Length" => $file->size,
                    "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                ]);
                break;
            case 'google':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $dir = '/';
                $recursive = false; // Get subdirectories also?
                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                $directory = $contents->where('type', '=', 'dir')
                    ->where('filename', '=', 'project-files')
                    ->first();

                $direct = $directory['path'];
                $recursive = false;
                $contents = collect(Storage::cloud()->listContents($direct, $recursive));
                $directo = $contents->where('type', '=', 'dir')
                    ->where('filename', '=', $file->parent)
                    ->first();

                $readStream = Storage::cloud()->getDriver()->readStream($directo['path']);
                return response()->stream(function () use ($readStream) {
                    fpassthru($readStream);
                }, 200, [
                    'Content-Type' => $ext,
                    'Content-disposition' => 'attachment; filename="'.$file->filename.'"',
                ]);
                break;
            case 'dropbox':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $fs = Storage::getDriver();
                $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                return Response::stream(function() use($stream) {
                    fpassthru($stream);
                }, 200, [
                    "Content-Type" => $ext,
                    "Content-Length" => $file->size,
                    "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                ]);
                break;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function thumbnailShow(Request $request)
    {
        $this->project = Project::with('files')->findOrFail($request->id);
        $this->icon($this->project);
        $view = view('admin.manage-files.thumbnail-list', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * @param $projects
     */
    private function icon($projects) {
        foreach ($projects->files as $project) {
            if (is_null($project->external_link)) {
                $ext = pathinfo($project->filename, PATHINFO_EXTENSION);
                if ($ext == 'png' || $ext == 'jpe' || $ext == 'jpeg' || $ext == 'jpg' || $ext == 'gif' || $ext == 'bmp' ||
                    $ext == 'ico' || $ext == 'tiff' || $ext == 'tif' || $ext == 'svg' || $ext == 'svgz' || $ext == 'psd' || $ext == 'csv')
                {
                    $project->icon = 'images';
                } else {
                    $project->icon = $this->mimeType[$ext];
                }

            }
        }
    }


    public function storeLink(StoreFileLink $request)
    {
        $file = new FileManager();
        $file->user_id = $this->user->id;
        $file->company_id = $this->user->company_id;
        if(isset($request->project_id)) {
            $file->project_id = $request->project_id;
        }
        $file->parent = $request->parent;
        $file->external_link = $request->external_link;
        $file->name = $request->filename;
        $file->filename = $request->filename;
        $file->revisionfileid = $request->revisionid ?: 0;
        $file->pathtype = $request->pathtype ?: 'outline';
        $file->foldertype =  'public';
        $file->save();
        return Reply::success(__('messages.fileUploaded'));
    }

    public function createFolder(Request $request){
        $folder = new FileManager();
        $folder->filename = $request->foldername;
        $folder->user_id = $this->user->id;
        if(isset($request->project_id)) {
            $folder->project_id = $request->project_id;
        }
        $folder->parent = $request->parent;
        $folder->type = 'folder';
        $folder->name = $request->name;
        $folder->description = $request->description;
        $folder->pathtype = 'outline';
        $folder->foldertype = $request->foldertype ?: 'public';
        $folder->save();
        return Reply::success('Folder Created Successfully');
    }

    public function editFolder(Request $request){
        $folder = FileManager::find($request->fileid);
        $folder->filename = $request->foldername;
        $folder->save();
        return Reply::success('Folder name updated');
    }
    public function showName(Request $request){
        $folder = FileManager::find($request->fileid);
        return $folder;
    }
    public function editName(Request $request){
        $folder = FileManager::find($request->fileid);
        $folder->name = $request->name;
        $folder->description = $request->description;
        $folder->foldertype = $request->foldertype ?: 'public';
        $folder->save();
        return Reply::success('Name updated Successfully');
    }
    public function movePathType(Request $request){
        $pathtype = $request->pathtype;
        $folder = FileManager::find($request->fileid);
        $folder->pathtype = $pathtype;
        $folder->save();
        return Reply::success(__('messages.fileMoved'));
    }
}
