<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\Product;
use App\Project;
use App\SubAsset;
use App\User;
use App\WorkOrderFiles;
use App\WorkRequest;
use App\Indent;
use App\Store;
use App\ProductCategory;
use App\ProductBrand;
use App\Units;
use App\IndentProducts;
use App\Location;
use App\SubLocation;
use App\Http\Requests\WorkOrder\WorkOrderRequest;
use App\Asset;
use App\TmpIndent;
use App\Wrformfield;
use App\Wrupdates;


class WorkRequestController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Work Request';
        $this->pageIcon = 'icon-user';
    }

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pageTitle = 'Work Request';
        $this->categories = WorkRequest::where('request_status',null)->get();
        $this->users =  User::all();
        $this->activeMenu = 'maintenance';
        return view('admin.work-request.index', $this->data);
    }
    public function allWorkOrder()
    {
        $this->pageTitle = 'Work Order';

        $this->categories = WorkRequest::where('request_status','<>','')->get();
        $this->users =  User::all();
        $this->activeMenu = 'maintenance';



        return view('admin.work-request.allworkorder', $this->data);
    }



    public function changeStatus(WorkOrderRequest $request, $id, $status)
    {
        $this->pageTitle = 'Edit Work Order';
        $this->activeMenu = 'maintenance';
        $woupdate =  WorkRequest::where('id',$id)->first();
        $woupdate->request_status = $status;
        $woupdate->save();

        return back();
    }

        public function woUpdate(WorkOrderRequest $request)
    {
        $this->pageTitle = 'Edit Work Order';
        $this->activeMenu = 'maintenance';

        $wo_id=$request->wo_id;
        $woupdatess=$request->woupdatess;

        $woupdate = new Wrupdates();

        $woupdate->wo_update = $woupdatess;
        $woupdate->wr_id = $wo_id;
        $woupdate->save();



        return view('admin.work-request.editworkorder');



    }


    public function timer($status,$value)
    {


    }

    public function editOrder(WorkOrderRequest $request)
    {
        $this->pageTitle = 'Edit Work Order';
        $this->activeMenu = 'maintenance';
        $wo_id=$request->id;
        $id = $request->id;
        $this->categories = WorkRequest::where('request_status','<>','')->get();
        $this->users =  User::all();


        $this->workrequests = WorkRequest::all();
        $this->workrequest = WorkRequest::where('id',$id)->first();

        $this->indents = Indent::where('work_ord_id',$this->workrequest->id)->first();


        $this->indents = Indent::where('work_ord_id',$this->workrequest->id)->get()->pluck('id');

            $indent_id_array = array($this->indents);




//dd($indent_id_array);

       // $this->indentproduct = IndentProducts::where('indent_id',$this->indents->id)->first();
        $this->users =  User::all();
        $this->asset=Asset::all();
        $this->subasset=SubAsset::all();
        $this->location=Location::all();
        $this->sublocation=SubLocation::all();
        $this->formfield = Wrformfield::where('wr_id',$id)->get();
        $this->woupdates = Wrupdates::where('wr_id',$id)->get();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->stores = Store::all();

        $this->stores = Store::all();
        $this->projects = Project::all();
        $this->products = ProductCategory::all();
        $this->lproducts = Product::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('indentTmpDataSession')) {
            $sid = $request->session()->get('indentTmpDataSession');
            TmpIndent::where('session_id', $sid)->delete();
            $request->session()->forget('indentTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('indentTmpDataSession', $sid);
        }
        //$string = Array();
        $string = implode(',', $indent_id_array);
        //echo $string;
       // dd();
        //dd($string);
        $genres = explode(',',$string);

        $genres = is_array($genres) ? $genres : [$genres];


       //dd($genres);

        $all_data = IndentProducts::whereIn('indent_id', $genres)->get();


        foreach ($all_data as $data){

            $tmp = new TmpIndent();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpIndent::where('session_id', $sid)->get();
      //  $this->indent = Indent::where('id', $this->indents->id)->first();

        return view('admin.work-request.editworkorder', $this->data);

    }


    public function woStore(WorkOrderRequest $request)
    {
        $this->pageTitle = 'Edit Work Order';
        $this->activeMenu = 'maintenance';


        $id = $request->id;
        $this->workrequest = WorkRequest::where('id',$id)->first();
        $this->indents = Indent::where('work_ord_id',$this->workrequest->work_ord_id)->first();
        $this->indentproduct = IndentProducts::where('indent_id',$this->indents->id)->first();
        $this->users =  User::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->stores = Store::all();
        $this->work_order_id= $id;



        return view('admin.work-request.wostore', $this->data);


    }


    public function woStoreUpdate(WorkOrderRequest $request){

            if($request->store_id != null){

            $sid = null;
            if($request->session()->has('indentTmpDataSession')){
            $sid = $request->session()->get('indentTmpDataSession');
            }
            $sr = 1;
            $ind = Indent::orderBy('id', 'DESC')->first();
            if($ind !== null){
                $in = explode('/', $ind->indent_no);
                $sr = $in[2];
                $sr++;
            }
            $indent_no = 'IND/'.date("Y").'/'.$sr;
            $indent = new Indent();
            $indent->indent_no = $indent_no;
            $indent->store_id = $request->store_id;
            $indent->remark = $request->remark;
            $indent->work_ord_id = $request->id;
            $indent->save();

            $all_data = TmpIndent::where('session_id', $sid)->get();
            foreach ($all_data as $data){
                $indPro = new IndentProducts();
                $indPro->indent_id = $indent->id;
                $indPro->cid = $data->cid;
                $indPro->bid = $data->bid;
                $indPro->quantity = $data->qty;
                $indPro->unit = $data->unit;
                $indPro->remarks = $data->remark;
                $indPro->expected_date = $data->dated;
                $indPro->save();
            }
            TmpIndent::where('session_id', $sid)->delete();


                return Reply::success(__('Products Updated Succesfully'));


}

}

        public function storeImage(WorkOrderRequest $request)
    {
        if ($request->hasFile('imageFile')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $file = new WorkOrderFiles();
                $file->user_id = $this->user->id;
                $file->task_id = $request->task_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/work-order-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('work-order-files/'.$request->task_id, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'work-order-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('work-order-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('work-order-files/'.$request->task_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/work-order-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }

        if ($request->hasFile('pdfFile')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $file = new WorkOrderFiles();
                $file->user_id = $this->user->id;
                $file->task_id = $request->task_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/work-order-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('work-order-files/'.$request->task_id, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'work-order-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('work-order-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('work-order-files/'.$request->task_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/work-order-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('admin.punch-items.index'), __('modules.projects.projectUpdated'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCat()
    {
        $this->categories = BoqCategory::all();
        return view('admin.boq-category.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    public function addWorkRequest(WorkOrderRequest $request)
    {
        $category = new WorkRequest();
        $category->title = $request->title;
        $category->description = $request->description;
        $category->user_name = $request->user_name;
        $category->user_phone = $request->user_phone;
        $category->user_email = $request->user_email;
        $category->request_image = $request->request_image;
        $category->request_pdf = $request->request_pdf;
        $category->request_priority = $request->request_priority;
        $category->location_id = $request->location_id;
        $category->sub_location_id = $request->sub_location_id;
        $category->project_id = $request->project_id;
        $category->timer_status = '0';


        $this->status = 'success';
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCat(StoreBoqCategory $request)
    {
        $category = new BoqCategory();
        $category->category_name = $request->category_name;
        $category->save();
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryAdded'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
   public function statusWorkOrder($id)
    {
        $this->workrequests = WorkRequest::all();
        $this->workrequest = WorkRequest::where('id',$id)->first();
        return view('admin.work-request.status', $this->data);

    }
   public function assignWorkRequest($id)
    {
        $this->workrequests = WorkRequest::all();
        $this->users =  User::all();
        $this->workrequest = WorkRequest::where('id',$id)->first();
        return view('admin.work-request.assign', $this->data);
    }
    public function statusUpdateWorkOrder(Request $request, $id)
    {
        $workrequest_status = WorkRequest::find($id);
        $workrequest_status->status_id = $request->status;
        $this->status='success';
        $workrequest_status->save();
        return Reply::success(__('messages.statusUpdated'));
    }

   public function assignUpdateWorkOrder(Request $request,$id)
    {

        $workrequest_status = WorkRequest::find($id);

        $workrequest_status->assigned_emp = $request->assigned_emp;

        $this->status='success';
        $workrequest_status->save();
        return Reply::success(__('messages.WRupdated'));
    }

    public function editWorkOrder($id)
    {
        $this->pageTitle = 'Edit Work Request';
        $this->activeMenu = 'maintenance';
        $this->workrequests = WorkRequest::all();
        $this->workrequest = WorkRequest::where('id',$id)->first();
        $this->indents = Indent::where('work_ord_id',$this->workrequest->work_ord_id)->first();
        $this->indentproduct = IndentProducts::where('indent_id',$this->indents->id)->first();
        $this->users =  User::all();
        $this->asset=Asset::all();
        $this->subasset=SubAsset::all();
        $this->location=Location::all();
        $this->sublocation=SubLocation::all();


        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->stores = Store::all();
        return view('admin.work-request.edit', $this->data);


    }
    public  function recurring(){
        $this->pageTitle = 'Work Order Recurring';
        $this->categories = WorkRequest::where('recurring_status','<>', '')->get();
        $this->users =  User::all();
        $this->activeMenu = 'maintenance';
        return view('admin.work-request.recurring', $this->data);

    }
    public function updateWorkOrder(WorkOrderRequest $request,$id)
    {

        if($request->store_id != null){

            $sid = null;
            if($request->session()->has('indentTmpDataSession')){
                $sid = $request->session()->get('indentTmpDataSession');
            }
            $sr = 1;
            $ind = Indent::orderBy('id', 'DESC')->first();
            if($ind !== null){
                $in = explode('/', $ind->indent_no);
                $sr = $in[2];
                $sr++;
            }
            $indent_no = 'IND/'.date("Y").'/'.$sr;
            $indent = new Indent();
            $indent->indent_no = $indent_no;
            $indent->store_id = $request->store_id;
            $indent->remark = $request->remark;
            $indent->work_ord_id = $request->work_order_id;
            $indent->save();

            $all_data = TmpIndent::where('session_id', $sid)->get();
            foreach ($all_data as $data){
                $indPro = new IndentProducts();
                $indPro->indent_id = $indent->id;
                $indPro->cid = $data->cid;
                $indPro->bid = $data->bid;
                $indPro->quantity = $data->qty;
                $indPro->unit = $data->unit;
                $indPro->remarks = $data->remark;
                $indPro->expected_date = $data->dated;
                $indPro->save();
            }
            TmpIndent::where('session_id', $sid)->delete();





        }

        $category = WorkRequest::find($id);
        $category->title = $request->title;
        $category->description = $request->description;
        $category->user_name = $request->user_name;
        $category->user_phone = $request->user_phone;
        $category->user_email = $request->user_email;
        $category->request_image = $request->request_image;
        $category->assigned_emp = $request->assigned_emp;

//        $category->request_pdf = $request->request_pdf;
//        $category->request_priority = $request->request_priority;

        $category->asset_id = $request->asset_id;
        $category->subasset_id = $request->subasset_id;
        $category->request_priority = $request->request_priority;


        $category->recurring_status = $request->recurring_status;
        $category->recurring_mode = $request->recurring_mode;

        if($request->recurring_mode == '0' && $request->recurring_status == '1' ){


            $category->recurring_time = $request->recurring_number."/".$request->recurring_type;
        }else{
            $category->recurring_time = $request->recurring_time;

        }

            $form_field = new Wrformfield();


        if($request->fieldname != null  &&$request->fieldtype !=null){
         foreach(array_combine($request->fieldname,$request->fieldtype) as $name => $type ){



        if( $type == 'multiple'){

            foreach($request->multi_id as $col ){
                $form_field = new Wrformfield();
                $form_field->wr_id =$id;
                $form_field->field_type =$type;
                $form_field->field_title =$name;
                $form_field->multiple ='YES';
                $form_field->response ="";
                $form_field->parent_id =$col;
                $form_field->child_id='0';
                $form_field->save();

                  foreach(array_combine($request->multi_row_id,$request->multi_row_title) as $rows =>$title ){
                      $row = explode("_",$rows);


                      if($row[0] == $col ){
                          $form_field = new Wrformfield();
                          $form_field->wr_id =$id;
                          $form_field->field_type =$type;
                          $form_field->field_title =$title;
                          $form_field->multiple ='YES';
                          $form_field->response ="";
                          $form_field->parent_id =$col;
                          $form_field->child_id=$row[1];
                          $form_field->save();
                      }

                 }

            }


        }else{
            $form_field = new Wrformfield();
            $form_field->wr_id =$id;
            $form_field->field_type =$type;
            $form_field->field_title =$name;
            $form_field->multiple ='NO';
            $form_field->response ="";
            $form_field->parent_id ="0";
            $form_field->child_id="0";
            $form_field->save();
        }
         }
        }

        $category->status_id = $request->status_id;
        $category->request_status = '1';
        $this->status='success';
        $category->save();

        return Reply::dataOnly(['projectID' => $category->id]);


    }
    public function deleteWorkOrder($id)
    {
        WorkRequest::destroy($id);
        $categoryData = WorkRequest::all();
        return Reply::successWithData(__('messages.WrDeleted'),['data' => $categoryData]);
    }
   public function createWR()
    {
        $this->pageTitle = 'Create Work Request';
        $this->activeMenu = 'maintenance';
        $this->categories = WorkRequest::all();
        $this->location = Location::all();
        $this->sublocation = SubLocation::all();
        $this->users =  User::all();
        $this->projects =  Project::all();

        return view('admin.work-request.create-wr', $this->data);
    }

    public function getSubLocation(WorkOrderRequest $request)
    {

        $id = $request->id;

        $data = array();
        // $category->name = $state_id->name;
        $data['sublocation'] = SubLocation::where('location_id',$id)->get();
        $option ="";
       foreach ( $data['sublocation'] as $d){
           $option .= "<option value=".$d['id'].">".$d['name']."</option>";
       }
        $data['status']='success';
        $data['option']=$option;



        return json_encode($data);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::all();
        $this->category = BoqCategory::where('id',$id)->first();
        return view('admin.boq-category.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = BoqCategory::find($id);
        $category->title = $request->title;
        $category->parent = $request->parent;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BoqCategory::destroy($id);
        $categoryData = BoqCategory::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }
}
