<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Role\StoreRole;
use App\Http\Requests\Role\StoreUserRole;
use App\Http\Requests\Role\UpdateRole;
use App\Module;
use App\ModuleSetting;
use App\Package;
use App\Permission;
use App\PermissionRole;
use App\Project;
use App\ProjectMember;
use App\ProjectPermission;
use App\Role;
use App\RoleUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ManageRolePermissionController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.rolesPermission';
        $this->pageIcon = 'ti-lock';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }
    public function employees(){
        $user = $this->user;
        $this->roles = Role::whereNotIn('name', ['admin', 'employee', 'client'])->get();
        $this->totalPermissions = Permission::count();
        $projectlist = explode(',',$user->projectlist);
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->employeeslist = Employee::getAllEmployees($user);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->user = $user;
        return view('admin.role-permission.employees', $this->data);
    }

    public function employeedata(Request $request)
    {

        $user = $this->user;
        $type = $request->type;

        switch ($type){
            case 'employee':
                $users =  Employee::getAllEmployees($user);
                break;
            case 'contractors':
                $users =  Employee::getAllContractors($user);
                break;
            case 'clients':
                $users =  Employee::getAllClients($user);
                break;
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                return '<a href="' . route('admin.role-permission.permission', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-eye" aria-hidden="true"></i></a> ';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if ($row->status == 'active') {
                        return '<label class="label label-success">' . __('app.active') . '</label>';
                    } else {
                        return '<label class="label label-danger">' . __('app.inactive') . '</label>';
                    }
                }
            )
            ->editColumn('name', function ($row) {

                return  '<div class="row"><div class="col-sm-3 col-xs-4">'.get_employee_images($row->id).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employee.show', $row->id) . '">'.ucwords($row->name).'</a></div></div>';

            })
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }
    public function clients(){
        $this->roles = Role::whereNotIn('name', ['admin', 'employee', 'client'])->get();
        $this->totalPermissions = Permission::count();
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->clientslist = Employee::getAllClients($user);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        return view('admin.role-permission.clients', $this->data);
    }
    public function contractors(){
        $this->roles = Role::whereNotIn('name', ['admin', 'employee', 'client'])->get();
        $this->totalPermissions = Permission::count();
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->contractorslist = Employee::getAllContractors($user);
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        return view('admin.role-permission.contractors', $this->data);
    }
    public function permission(Request $request,$id,$projectid=false){
        $user = $this->user;
        $this->roles = Role::whereNotIn('name', ['admin', 'employee', 'client'])->get();
        $this->totalPermissions = Permission::count();
        $projectlist = explode(',',$user->projectlist);
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->user = $user;
        $employee = Employee::find($id);
        $this->employee = $employee;
        $projectmember = !empty($projectid) ? ProjectMember::where('project_id',$projectid)->where('user_id',$employee->user_id)->where('employee_id',$employee->id)->where('company_id',$employee->company_id)->first() : '';
        $pm = !empty($projectid) ?  ProjectPermission::where('project_id', $projectid)->where('user_id', $employee->user_id)->get()->pluck('permission_id')->toArray() : '';

        $this->sharetoproject = !empty($projectmember->share_project) ? 1 : 0;
        $this->checkpermission = !empty($projectmember->check_permission) ? 1 : 0;
        $this->level = !empty($projectmember->level) ? $projectmember->level : '';
        $this->sharepermissions = $pm;
        $this->projectid = $projectid;
        return view('admin.role-permission.permission', $this->data);
    }
    public function savePermission(Request $request){
        $user = $this->user;
        try{
            $employee = Employee::find($request->employee_id);
            $projectid = $request->project_id;
            $projectsdetails = Project::find($projectid);
            if(empty($request->permissions)){
                return Reply::error("Please select the atleast one permission");
            }
            if(!empty($projectsdetails->id)){
                if(!empty($employee->id)){
                    $sharetoproject = $request->sharetoproject;
                    $checkpermission = $request->checkpermission;
                    $sendnotification = 1;
                    $user_type = $employee->user_type;
                    /*  $level = 0;
                      if($projectsdetails->added_by==$user->id){
                          $level = 1;
                      }else{
                          $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectsdetails->id)->first();
                          $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                          $level = $prolevel+1;
                      }*/
                    $projectmember = ProjectMember::where('project_id',$projectsdetails->id)->where('employee_id',$employee->id)->where('user_id',$employee->user_id)->where('company_id',$employee->company_id)->first();
                    if(empty($projectmember)){
                        $projectmember = new ProjectMember();
                        $projectmember->project_id = $projectid;
                        $projectmember->user_id = $employee->user_id;
                        $projectmember->employee_id = $employee->id;
                        $projectmember->company_id = $employee->company_id;
                        $projectmember->assigned_by = $user->id;
                        $projectmember->user_type = $employee->user_type;
                    }
                    $projectmember->share_project = 0;
                    if($sharetoproject=='1'){
                        $projectmember->share_project = '1';
                    }
                    $projectmember->check_permission = 0;
                    if($checkpermission=='1'){
                        $projectmember->check_permission = '1';
                    }
                    if($employee->added_by==$user->id){
                        $level = $projectmember->level;
                        $prevproject = ProjectMember::where('user_id',$employee->added_by)->where('project_id',$projectsdetails->id)->first();
                        $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                        $level = $prolevel+1;
                        $projectmember->level = $level;
                    }
                    $projectmember->save();
                    if($sendnotification=='1'){
                        $usersdata = User::where('id',$employee->user_id)->first();
                        if($sharetoproject=='1'&&!empty($usersdata->fcm)) {
                            $notifmessage = array();
                            $notifmessage['title'] = 'Project Shared';
                            $notifmessage['body'] = 'You have been added to ' . $projectsdetails->project_name . ' project by ' . $user->name;
                            $notifmessage['activity'] = 'projects';
                            sendFcmNotification($usersdata->fcm, $notifmessage);
                        }
                    }
                }
                $member = User::find($employee->user_id);

                ProjectPermission::where('user_id', $member->id)->where('project_id', $projectsdetails->id)->delete();
                if(!empty($request->role_id)){
                    $selectedroles = !empty($request->role_id) ? explode(',',$request->role_id) : array();
                    if(!empty($selectedroles)){
                        foreach ($selectedroles as $selectedrole){
                            $permissions =  $request->permissions;
                            $permissions = array_filter(array_unique($permissions));
                            foreach ($permissions as  $permission){
                                $p = new ProjectPermission();
                                $p->company_id = $projectsdetails->company_id;
                                $p->project_id = $projectsdetails->id;
                                $p->user_id = $member->id;
                                $p->permission_id = $permission;
                                $p->role_id = $selectedrole;
                                $p->save();
                            }
                        }
                    }
                }else{
                    $permissions =  $request->permissions;
                    $permissions = array_filter(array_unique($permissions));
                    foreach ($permissions as  $permission){
                        $p = new ProjectPermission();
                        $p->company_id = $projectsdetails->company_id;
                        $p->project_id = $projectsdetails->id;
                        $p->user_id = $member->id;
                        $p->permission_id = $permission;
                        $p->save();
                    }
                }
                return Reply::success("Permission Updated Successfully");
            }
           return Reply::error("Please select the project");
        } catch (\Exception $e) {
            return Reply::error($e->getMessage());
            }
    }
    public function index(){
        $this->roles = Role::whereNotIn('name', ['admin', 'employee', 'client'])->get();
        $this->totalPermissions = Permission::count();
        $user = Auth::user();
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        return view('admin.role-permission.index', $this->data);
    }
    public function store(Request $request){
        $roleId = $request->roleId;
        $permissionId = $request->permissionId;

        if($request->assignPermission == 'yes'){
            $rolePermission = new PermissionRole();
            $rolePermission->permission_id = $permissionId;
            $rolePermission->role_id = $roleId;
            $rolePermission->save();
        }
        else{
            PermissionRole::where('role_id', $roleId)->where('permission_id', $permissionId)->delete();
        }

        return Reply::dataOnly(['status' => 'success']);
    }

    public function assignAllPermission(Request $request){
        $roleId = $request->roleId;
        $user = Auth::user();
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        $mods = json_decode($mods);
        if (($key = array_search('roles_permission', $mods)) !== false) {
            unset($mods[$key]);
        }
        //dd();
        $ackageModules = ModuleSetting::whereIn('module_name',$mods )->pluck('module_name')->toArray();
        $modulesData = Module::whereIn('module_name', $ackageModules)->get()->pluck('id')->toArray();
        $permissions = Permission::whereIn('module_id', $modulesData)->get();

        $role = Role::findOrFail($roleId);
        $role->perms()->sync([]);
        $role->attachPermissions($permissions);
        return Reply::dataOnly(['status' => 'success']);
    }
    public function removeAllPermission(Request $request){
        $roleId = $request->roleId;
        $role = Role::findOrFail($roleId);
        $role->perms()->sync([]);

        return Reply::dataOnly(['status' => 'success']);
    }

    public function showMembers($id){
        $this->role = Role::findOrFail($id);
        $this->employees = User::doesntHave('role', 'and', function($query) use ($id){
            $query->where('role_user.role_id', $id);
        })
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at')
            ->distinct('users.id')
            ->where('roles.name', '<>', 'client')
            ->get();

        return view('admin.role-permission.members', $this->data);
    }

    public function storeRole(StoreRole $request){
        $roleUser = new Role();
        $roleUser->name = $request->name;
        $roleUser->display_name = ucwords($request->name);
        $roleUser->save();
        return Reply::success(__('messages.roleCreated'));
    }

    public function assignRole(StoreUserRole $request){
        $employeeRole = Role::where('name', 'employee')->first();
        foreach($request->user_id as $user){
            RoleUser::where('user_id', $user)->delete();

            $roleUser = new RoleUser();
            $roleUser->user_id = $user;
            $roleUser->role_id = $employeeRole->id;
            $roleUser->save();

            $roleUser = new RoleUser();
            $roleUser->user_id = $user;
            $roleUser->role_id = $request->role_id;
            $roleUser->save();
        }
        return Reply::success(__('messages.roleAssigned'));
    }

    public function detachRole(Request $request){
        $user = User::findOrFail($request->userId);
        $user->detachRole($request->roleId);
        return Reply::dataOnly(['status' => 'success']);
    }

    public function deleteRole(Request $request){
        Role::whereId($request->roleId)->delete();
        return Reply::dataOnly(['status' => 'success']);
    }

    public function create(){
        $this->roles = Role::all();
        return view('admin.role-permission.create', $this->data);
    }

    public function update(UpdateRole $request, $id){
        $roleUser = Role::findOrFail($id);
        $roleUser->name = $request->value;
        $roleUser->display_name = ucwords($request->value);
        $roleUser->save();

        return Reply::successWithData(__('messages.roleUpdated'), ['display_name' => $roleUser->display_name]);
    }
}
