<?php

namespace App\Http\Controllers\Admin;

use App\Bom;
use App\ClientDetails;
use App\Employee;
use App\GrnFiles;
use App\Helper\Reply;
use App\Http\Requests\Admin\Rfq\StoreRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Http\Requests\Grn\StoreGrnRequest;
use App\Http\Requests\PaymentRequest;
use App\Indent;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PiProducts;
use App\PoProducts;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\ProductIssue;
use App\ProductLog;
use App\ProductReturns;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\PurchaseInvoice;
use App\PurchaseOrder;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\QuoteProducts;
use Barryvdh\DomPDF\Facade as PDF;
use App\Quotes;
use App\Role;
use App\Rfq;
use App\RfqProducts;
use App\Segment;
use App\Stock;
use App\Store;
use App\Supplier;
use App\Title;
use App\TmpInvoice;
use App\TmpRfq;
use App\Transactions;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Dropbox\Client;
use Yajra\DataTables\Facades\DataTables;

class MemberInventoryController extends AdminBaseController
{

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.inventory';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';

        $this->middleware(function ($request, $next) {
            if (!in_array('inventory', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$company_id = Auth::user()->company_id;
        $this->pos = PurchaseOrder::all();
        $this->stores = Store::all();

        $this->pos = count($this->pos);

        return view('admin.inventory.index', $this->data);
    }

    public function stock()
    {
        $this->pageTitle = 'app.menu.stock';
        $this->stores = Store::all();
        $this->categories = Product::where('company_id',$this->companyid)->get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        return view('admin.inventory.stock', $this->data);
    }
    public function purchaseHistory($bid, $store = 0)
    {
        $this->pageTitle = 'app.menu.purchaseHistory';
        $this->bid = $bid;
        $this->store = $store;
        return view('admin.inventory.purchase-history', $this->data);
    }
    public function purchaseInvoices(){
        $this->pageTitle = 'app.menu.pinvoices';
        $this->stores = Store::all();
        $this->suppliers = Supplier::where('company_id',$this->companyid)->get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        return view('admin.inventory.purchase-invoices', $this->data);
    }
    public function pReturns(){
        $this->pageTitle = 'app.menu.return';
        $this->stores = Store::all();
        $this->suppliers = Supplier::where('company_id',$this->companyid)->get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        return view('admin.inventory.returns', $this->data);
    }
    public function pReturn($invId){
        $this->pageTitle = 'app.menu.return';
        $this->tmpData = Stock::where('inv_id',$invId)->get();
        $this->inv = PiProducts::join('purchase_invoice','purchase_invoice.id','=','pi_products.pi_id')->where('pi_products.pi_id',$invId)->first();
        return view('admin.inventory.pro-return',$this->data);
    }
    public function viewReturn($invId){
        $this->pageTitle = 'app.menu.return';
        $this->tmpData = ProductReturns::where('id', $invId)->first();
        /*$this->inv = PurchaseInvoice::where('id',$this->tmpData->invoice_id)->first();*/
        $this->inv = ProductReturns::join('purchase_invoice','purchase_invoice.id','=','product_return.invoice_id')->where('product_return.id',$invId)->first();
        return view('admin.inventory.view-return', $this->data);
    }
    public function iDetails($invId){
        $this->pageTitle = 'app.menu.return';
        $this->tmpData = Stock::where('inv_id', $invId)->get();
        $this->inv = PurchaseInvoice::find($invId);
        return view('admin.inventory.details', $this->data);
    }
    public function makePayment($invId){
        $this->pageTitle = 'app.menu.makePayment';
        $this->inv = PurchaseInvoice::find($invId);
        $this->tmpData = Transactions::where('invoice_id', $invId)->get();
        return view('admin.inventory.payment', $this->data);
    }

    public function submitPayment($invId, PaymentRequest $request){
        //dd($request->all());
        if($request->totamount < $request->amount){
            return Reply::error('Invalid Payment Amount');
        }else{
            $this->pageTitle = 'app.menu.makePayment';
            $inv = PurchaseInvoice::find($invId);
            $transact = new Transactions();
            $transact->invoice_id = $inv->id;
            $transact->amount = $request->amount;
            $transact->dated = $request->dated;
            $transact->status = $request->status;
            $transact->save();
            $inv->status = $request->status;
            $inv->save();
            return Reply::redirect(route('admin.inventory.invoices'));
        }

    }
    public function postReturn(Request $request, $invId){
        $inv = PurchaseInvoice::find($invId);
        $st = Stock::where('inv_id',$inv->id)->get();
        $productid = $request->cid;
        $retrunQty = $request->retQty;
        $maxQty = $request->max;

        if(!empty($st)){
            foreach ($st as $key=>$stvalue){
                if($retrunQty[$key] <= $maxQty[$key]){
                    if(!empty($stvalue) && $stvalue->cid == $productid[$key]){
                        $ret = new ProductReturns();
                        $ret->invoice_id = $inv->id;
                        $ret->cid = $stvalue->cid;
                        $ret->bid = !empty($stvalue->bid)?$stvalue->bid:0;
                        $ret->quantity = $retrunQty[$key];
                        $ret->unit = $stvalue->unit;
                        $ret->price = $stvalue->price;
                        $ret->store_id = $inv->store_id;
                        $ret->project_id = $inv->project_id;
                        $ret->supplier_id = $inv->supplier_id;
                        $ret->company_id = $this->user->company_id;
                        $ret->save();
                        if($ret) {
                            $stvalue->stock = (int)$stvalue->stock - (int)$retrunQty[$key];
                            $stvalue->save();
                            $pl = new ProductLog();
                            $pl->store_id = $inv->store_id;
                            $pl->project_id = $inv->project_id;
                            $pl->company_id = $this->user->company_id;
                            $pl->created_by = $this->user->id;
                            $pl->module_id = $ret->id;
                            $pl->module_name = 'product_return';
                            $pl->product_id = $stvalue->cid;
                            $pl->quantity = $retrunQty[$key];
                            $pl->indent_id = $inv->indent_id;
                            $pl->balance_quantity = $stvalue->stock - $retrunQty[$key];
                            $pl->transaction_type = 'minus';
                            $pl->remark = 'Purchase returned';
                            $pl->save();
                        }
                    }
                }else{
                    return Reply::error('Invalid Return Qquantity');
                }
            }
        }
        return Reply::redirect(route('admin.inventory.returns'));
    }
    public function showAll($id)
    {
        //$company_id = Auth::user()->company_id;
        $supIds = Quotes::where('rfq_id', $id)->pluck('supplier_id');
        $this->suppliers = Supplier::whereIn('id', $supIds)->get();
        $this->products = RfqProducts::where('rfq_id', $id)->get();
        $this->rfq = Rfq::find($id);
        return view('admin.quotes.show-all', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->projects = Project::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        return view('admin.rfq.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $bids = ProductCategory::where('id', $pid)->first()->brands;
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = '<option value="">Select Brand</option>';
        foreach($brands as $brand){
            $html .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        }
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRfqRequest $request)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq = new Rfq();
        $rfq->rfq_no = $rfq_no;
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->save();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('rfqTmpDataSession', $sid);
        }
        $all_data = RfqProducts::where('rfq_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpRfq();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpRfq::where('session_id', $sid)->get();
        $this->rfq = Rfq::where('id', $id)->first();
        return view('admin.rfq.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRfqRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $rfq = Rfq::find($id);
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        RfqProducts::where('rfq_id', $id)->delete();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        Rfq::destroy($id);
        RfqProducts::where('rfq_id', $id)->delete();
        DB::commit();
        return Reply::success(__('messages.rfqDeleted'));
    }
    public function invoicedestroy($id)
    {
        $projectde  = ProductReturns::where('invoice_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Product Return first.';
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            PurchaseInvoice::destroy($id);
            ProductLog::where('module_id',$id)->delete();
            Stock::where('inv_id',$id)->delete();
            return Reply::success(__('invoice Deleted Successfully'));
        }

    }
    public function purchaseDelete($id)
    {
        $pro_return = ProductReturns::find($id);
        if(!empty($pro_return) && !empty($pro_return->invoice_id)){
            $stock = Stock::where('inv_id',$pro_return->invoice_id)->where('cid',$pro_return->cid)->first();
            if(!empty($stock)){
                $stock->stock =  $stock->stock + $pro_return->quantity;
                $stock->save();
            }
        }
        ProductReturns::destroy($id);
        ProductLog::where('module_id',$id)->where('module_name','product_return')->delete();

        return Reply::success(__('Purchase Deleted Successfully'));
    }
    public function stockData (Request $request)
    {
        $pos = null;
        $pos = Stock::select(['sku','cid', 'bid', 'quantity', 'unit', 'price', 'tax', 'stock', 'supplier_id', 'store_id']);
        if(!empty($request->store_id)){
            $pos->where('store_id', $request->store_id);
        }
        if(!empty($request->project_id)){
            $pos->where('project_id', $request->project_id);
        }
        if(!empty($request->category_id)){
            $pos->where('cid', $request->category_id);
        }
        $store_id = $request->store_id;
        $user = Auth::user();
        $project_id = 0;
        $proid = Store::where('id',$store_id)->select('project_id')->first();
        if($proid !== null){
            $project_id = $proid->project_id;
        }
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user, $store_id) {
                $ret = '';
                if($store_id == 0) {
                    $ret = '<a href="' . route('admin.inventory.purchase-history', [$row->cid, $store_id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }else{
                    $ret = '<a href="' . route('admin.inventory.purchase-history', [$row->cid, $store_id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Purchase History"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'cid',
                function ($row) {
                    $cat = Product::where('id',$row->cid)->first();
                    if ($cat !== null) {
                        return $cat->name;
                    }
                    return 'NA';
                }
            )
            ->editColumn(
                'unit',
                function ($row) { return get_unit_name($row->unit); }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'est_qty',
                function ($row) use ($project_id, $store_id) {
                    return get_est_qty($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'est_rate',
                function ($row) use ($project_id, $store_id) {
                    return get_est_rate($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'indented_qty',
                function ($row) use ($project_id, $store_id) {
                    return get_indented_qty($project_id, $store_id, $row->cid);
                }
            )
            ->editColumn(
                'issued_project',
                function ($row) use ($project_id) {
                    return get_project_name($project_id);
                }
            )
            ->editColumn(
                'stock',
                function ($row) use ($store_id) {
                    return getStock($store_id, $row->bid);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function purchaseData (Request $request)
    {
        $pos = Stock::select(['sku','cid', 'bid', 'quantity', 'unit', 'price', 'tax', 'stock', 'supplier_id', 'store_id'])
            ->where('cid', $request->bid)
            ->orderBy('id', 'DESC');
        if($request->store_id != 0){
            $pos->where('store_id', $request->store_id);
        }
        $user = Auth::user();
        return DataTables::of($pos)
//            ->addColumn('action', function ($row) use ($user) {
//                $ret = '<a href="' . route('member.inventory.purchase-history', [$row->bid]) . '" class="btn btn-info btn-circle"
//                      data-toggle="tooltip" data-original-title="View detail"><i class="fa fa-info-circle" aria-hidden="true"></i></a>&nbsp;';
//                return $ret;
//            })
            ->editColumn(
                'cid',
                function ($row) {
                    return get_local_product_name($row->cid);
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->addIndexColumn()
//            ->rawColumns(['action'])
            ->make(true);
    }

    public function invoiceData(Request $request)
    {
        $pos = PurchaseInvoice::query();
        if($request->store_id != 0){
            $pos->where('store_id', $request->store_id);
        }
        if($request->supplier_id != 0){
            $pos->where('supplier_id', $request->supplier_id);
        }
        if($request->project_id != 0){
            $pos->where('project_id', $request->project_id);
        }
//        $pos->where('po_id', '=', 'NA');
        $pos->orderBy('id', 'DESC');
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {

                $ret = '<a href="' . route('admin.inventory.return', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Return Product"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';
                if($row->status !== 2) {
                    $ret .= '<a href="' . route('admin.purchase.payment', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="Make Payment"><i class="fa fa-inr" aria-hidden="true"></i></a>&nbsp;';
                }
                $ret .= '<a href="' . route('admin.inventory.viewInvoice', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="View GRN"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                if($row->approve == 1){
                    $ret .= '<a href="' . route('admin.inventory.edit-grn', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="UnApprove"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending Payment';
                    }elseif($row->status == 1){
                        return 'Partially Paid';
                    }else{
                        return 'Fully Paid';
                    }
                }
            )
            ->editColumn(
                'dated',
                function ($row) {
                    return Carbon::parse($row->dated)->format('d-m-Y');
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function returnData(Request $request)
    {
        //$pos = ProductReturns::query();
        $pos = ProductReturns::where('company_id',$this->companyid)->whereIn('project_id',explode(',',$this->user->projectlist));
        if($request->store_id != 0){
            $pos->where('store_id',$request->store_id);
        }
        if($request->supplier_id != 0){
            $pos->where('supplier_id',$request->supplier_id);
        }
        if($request->project_id != 0){
            $pos->where('project_id',$request->project_id);
        }
        $pos->orderBy('id', 'DESC');
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="' . route('admin.inventory.editReturn', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';

                $ret .= '<a href="' . route('admin.inventory.viewReturn', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View Product"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a  class="btn btn-danger btn-circle sa-params" data-user-id="'.$row->id.'"
                      data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->editColumn(
                'invoice_id',
                function ($row) {
                    $inv = PurchaseInvoice::find($row->invoice_id);
                    return $inv->invoice_no;
                }
            )
            ->editColumn(
                'cid',
                function ($row) {
                    return get_local_product_name($row->cid);
                }
            )
            ->editColumn(
                'bid',
                function ($row) {
                    return get_pbrand_name($row->bid);
                }
            )
            ->editColumn(
                'unit',
                function ($row) {
                    return get_unit_name($row->unit);
                }
            )
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format('d-m-Y');
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function export($status, $rfq)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'rfq')
            ->where('roles.company_id', company()->id)
            ->leftJoin('rfq_details', 'users.id', '=', 'rfq_details.user_id')
            ->select(
                'users.id',
                'rfq_details.name',
                'rfq_details.email',
                'rfq_details.mobile',
                'rfq_details.company_name',
                'rfq_details.address',
                'rfq_details.website',
                'rfq_details.created_at'
            )
            ->where('rfq_details.company_id', company()->id);
        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }
        if ($rfq != 'all' && $rfq != '') {
            $rows = $rows->where('users.id', $rfq);
        }
        $rows = $rows->get()->makeHidden(['image']);
        $exportArray = [];
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }
        Excel::create('rfqs', function ($excel) use ($exportArray) {
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('rfqs file');
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function rfqLink($id){
        $this->rfq = Rfq::find($id);
        return view('admin.rfq.links', $this->data);
    }

    public function linkData(Request $request, $rfqId)
    {
        $suppliers = Supplier::all();
        $user = Auth::user();
        return DataTables::of($suppliers)
            ->addColumn('action', function ($row) use ($user, $rfqId){
                $ret = '<a href="javascript:;" onClick="copyLink(\'id'.$row->id.'\')" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>&nbsp;';
                return $ret;
            })
            ->addColumn('link', function($row) use ($rfqId){
                return '<span id="id'.$row->id.'">'.route('front.submitQuotation', [$rfqId, $row->id]).'</span>';
            })
            ->addColumn('status', function($row) use ($rfqId){
                $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                if($quotes === null){
                    return 'Not Submitted';
                }
                return 'Submitted';
            })
            ->editColumn(
                'updated_at',
                function ($row) use ($rfqId){
                    $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                    if($quotes === null){
                        return 'Never';
                    }
                    return Carbon::parse($quotes->updated_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['company_name', 'action', 'link'])
            ->make(true);
    }

    public function postPO(Request $request){
        $sr = 1;
        $pox = PurchaseOrder::orderBy('id', 'DESC')->first();
        if($pox !== null){
            $in = explode('/', $pox->po_number);
            $sr = $in[2];
            $sr++;
        }
        $po_no = 'PO/'.date("Y").'/'.$sr;
        $po = new PurchaseOrder();
        $po->po_number = $po_no;
        $po->quote_id = $request->quote;
        $po->rfq_id = $request->rfq;
        $po->supplier_id = $request->supplier;
        $po->remark = $request->remark;
        $po->payment_terms = $request->payment_terms;
        $po->dated = $request->dated;
        $po->company_id = $this->user->company_id;
        $po->save();
        foreach($request->cid as $key=>$cid){
            $popro = new PoProducts();
            $popro->po_id = $po->id;
            $popro->cid = (!empty($cid)) ? $cid : '';
            $popro->bid = (!empty($request->bid[$key])) ? $request->bid[$key] : '';
            $popro->quantity = (!empty($request->quantity[$key])) ? $request->quantity[$key] : '';
            $popro->unit = (!empty($request->unit[$key])) ? $request->unit[$key] : '';
            $popro->price = (!empty($request->price[$key])) ? $request->price[$key] : '';
            $popro->amount = (!empty($request->amount[$key])) ? $request->amount[$key] : '';
            $popro->save();
        }
        $rfq = Rfq::find($request->rfq);
        if($rfq !== null) {
            $rfq->status =  1;
            $rfq->save();
        }
        $ind = Indent::where('indent_no', $rfq->indent_no)->first();
        if($ind !== null){
            $ind->status = 2;
            $ind->save();
        }
        $quote = Quotes::find($request->quote);
        if($quote !== null){
            $quote->status = 1;
            $quote->save();
        }
        return Reply::redirect(route('admin.rfq.index'));
    }

    public function add($poId){
        $this->pageTitle = 'Add Inventory';
        $this->pageIcon = 'icon-people';
        $po = PurchaseOrder::find($poId);
        $this->po = $po;
        $this->purinvoices = PurchaseInvoice::where('po_id',$po->po_number)->pluck('id')->toArray();
        $this->tmpData = PoProducts::where('po_id', $poId )->get();
        $quote = Quotes::find($this->po->quote_id);
        $this->supplier = Supplier::find($this->po->supplier_id);
        $this->rfq = Rfq::find($quote->rfq_id);
        $this->quote = $quote;
        return view('admin.inventory.add-po', $this->data);
    }
    public function postAdd($poId, Request $request){
        //dd($request);
        if($request->invoice_no == '' || $request->inv_dated == '' || $request->inv_remark == ''){
            return Reply::error('Mandatory field(s) cannot be left blank.');
        }
        $grn = PurchaseInvoice::orderBy('id','DESC')->first();
        if($grn == null){
            $grnNo = 'GRN-'.date("d-m-Y").'-1';
        }else {
            $grnNo = 'GRN-' . date("d-m-Y") . '-' . $grn->id;
        }
        $pi = new PurchaseInvoice();
        $pi->invoice_no = $grn;
        $pi->po_id = $request->po;
        $pi->supplier_id = $request->supplier;
        $pi->company_id = $this->user->company_id;
        $pi->store_id = $request->store_id;
        $pi->project_id = $request->project_id;
        $pi->dated = $request->inv_dated;
        $pi->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $pi->remark = $request->inv_remark;
        $pi->payment_terms = $request->payment_terms;
        $gt = 0;
        foreach($request->cid as $key=>$cid){
            $price = $request->price[$key] + ($request->price[$key]*$request->tax[$key]/100);
            $t = (is_numeric($price) && is_numeric($request->rec_qty[$key])) ? $price*$request->rec_qty[$key] : 0;
            $gt += $t;
        }
        $pi->gt = $gt;
        $pi->save();
        $sku = time();
        foreach($request->cid as $key=>$cid) {
            $indPro = new PiProducts();
            $indPro->pi_id = $pi->id;
            $indPro->product_id = (!empty($cid)) ? $cid : '';
            $indPro->bid = (!empty($request->bid[$key])) ? $request->bid[$key] : '';
            $indPro->quantity = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
            $indPro->unit_id = (!empty($request->unit[$key])) ? $request->unit[$key] : '';
            $indPro->date = (!empty($request->inv_dated)) ? $request->inv_dated : '';
            $indPro->remark = $request->inv_remark;
            $indPro->save();
            $stockCheck = Stock::where('cid',$cid)->where('store_id',$request->store_id)->where('project_id',$request->project_id)->first();

            if(!empty($stockCheck)){
                $stockCheck->stock = (int)$stockCheck->stock+(int)$request->rec_qty[$key];
                $stockCheck->save();
                if($stockCheck){
                    $pl = new ProductLog();
                    $pl->store_id = $request->store_id;
                    $pl->project_id = $request->project_id;
                    $pl->company_id = $this->user->company_id;
                    $pl->created_by = $this->user->id;
                    $pl->module_id = $pi->id;
                    $pl->module_name = 'grn';
                    $pl->product_id = $cid;
                    $pl->quantity = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
                    $pl->balance_quantity = $stockCheck->stock;
                    $pl->transaction_type = 'plus';
                    $pl->remark = $request->inv_remark;
                    $pl->save();
                }
            }else{
                $stock = new Stock();
                $stock->sku = $sku;
                $stock->po = $request->po;
                $stock->invoice_no = $request->invoice_no;
                $stock->inv_id = $pi->id;
                $stock->cid = (!empty($cid)) ? $cid : '';
                $stock->bid = (!empty($request->bid[$key])) ? $request->bid[$key] : '';
                $stock->quantity = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
                $stock->stock = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
                $stock->unit = (!empty($request->unit[$key])) ? $request->unit[$key] : '';
                $stock->price = (!empty($request->price[$key])) ? $request->price[$key] : '';
                $stock->tax = (!empty($request->tax[$key])) ? $request->tax[$key] : '';
                $stock->store_id = $request->store_id;
                $stock->project_id = $request->project_id;
                $stock->supplier_id = $request->supplier;
                $stock->company_id = $this->user->company_id;
                $stock->save();

                if($stock){
                    $pl = new ProductLog();
                    $pl->store_id = $request->store_id;
                    $pl->project_id = $request->project_id;
                    $pl->company_id = $this->user->company_id;
                    $pl->created_by = $this->user->id;
                    $pl->module_id = $pi->id;
                    $pl->module_name = 'grn';
                    $pl->product_id = $cid;
                    $pl->quantity = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
                    $pl->balance_quantity = (!empty($request->rec_qty[$key])) ? $request->rec_qty[$key] : '';
                    $pl->transaction_type = 'plus';
                    $pl->remark = $request->inv_remark;
                    $pl->save();
                }
            }
        }
        Quotes::where('id', $request->quote)->update(['status'=>2]);
        Rfq::where('id', $request->rfq)->update(['status'=>2]);
        $rfq = Rfq::find($request->rfq);
        if($rfq !== null) {
            Indent::where('indent_no', $rfq->indent_no)->update(['status' => 3]);
        }
        return Reply::redirect(route('admin.purchase-order.index'));
    }
    public function addNew(Request $request){
        $this->pageTitle = 'Add Purchase Invoice';
        $this->pageIcon = 'icon-people';
        $this->products = Product::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->suppliers = Supplier::where('company_id',$this->companyid)->get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        if($request->session()->has('piTmpDataSession')) {
            $sid = $request->session()->get('piTmpDataSession');
            TmpInvoice::where('session_id', $sid)->delete();
            $request->session()->forget('piTmpDataSession');
        }
        return view('admin.inventory.add-new', $this->data);
    }
    public function storeTmp(Request $request){
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('piTmpDataSession', $sid);
        }
        $tmpdata = new TmpInvoice();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->price = floatval($request->price);
        $tmpdata->tax = floatval($request->tax);
        $total = floatval($request->price)+(floatval($request->price)*floatval($request->tax)/100);
        $tmpdata->amount = floatval($request->qty)*$total;
        $tmpdata->save();

        $allData = TmpInvoice::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Product</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->price.'</td><td>'.$data->tax.'</td><td>'.$data->amount.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="9">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function deletePayment($id){
        DB::beginTransaction();
        $tran = Transactions::find($id);
        if($tran !== null){
            $inv = PurchaseInvoice::find($tran->invoice_id);
            $status = 0;
            $tran->delete();
            $trans = Transactions::where('invoice_id', $inv->id)->orderBy('id', 'DESC')->first();
            if($trans !== null){
                $status = $trans->status;
            }
            $inv->status = $status;
            $inv->save();
        }
        DB::commit();
        return Reply::success(__('messages.paymentDeleted'));
    }
    public function deleteTmp(Request $request){
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }
        TmpInvoice::where('id', $request->did)->delete();
        $allData = TmpInvoice::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->price.'</td><td>'.$data->tax.'</td><td>'.$data->amount.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="9">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }
    public function postAddNew(Request $request){

        $grnid = 1;
        if($request->store_id == '' || $request->supplier_id == '' || $request->inv_dated == ''){
            return Reply::error('Mandatory field(s) cannot be left blank.');
        }
        $grn = PurchaseInvoice::orderBy('id','DESC')->first();
        if(!empty($grn) || $grn !== null){
            $grnid = $grn->id;
        }

        $grnNo = 'GRN-'.date("d-m-Y").'-'.$grnid;
        $pi = new PurchaseInvoice();
        $pi->invoice_no = $grnNo;
        $pi->po_id = !empty($request->po_number)?$request->po_number:'';
        $pi->supplier_id = $request->supplier_id;
        $pi->company_id = $this->user->company_id;
        $pi->store_id = $request->store_id;
        $pi->project_id = $request->project_id;
        $pi->dated = $request->inv_dated;
        $pi->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $pi->remark = $request->inv_remark;
        $pi->payment_terms = $request->payment_terms;
        $gt = 0;
        $sid = null;
        if($request->session()->has('piTmpDataSession')){
            $sid = $request->session()->get('piTmpDataSession');
        }
        $tmpData = TmpInvoice::where('session_id', $sid)->get();
        foreach($tmpData as $tmp){
            $price = $tmp->price + ($tmp->price*$tmp->tax/100);
            $t = (is_numeric($price) && is_numeric($tmp->qty)) ? $price*$tmp->qty : 0;
            $gt += $t;
        }
        $pi->gt = $gt;
        $pi->save();

        $sku = time();
        foreach($tmpData as $temp) {
            $indPro = new PiProducts();
            $indPro->pi_id = $pi->id;
            $indPro->product_id = (!empty($temp->cid)) ? $temp->cid : '';
            $indPro->quantity = (!empty($temp->qty)) ? $temp->qty : '';
            $indPro->unit_id = (!empty($temp->unit)) ? $temp->unit : '';
            $indPro->date = $request->inv_dated;
            $indPro->price = $request->price;
            $indPro->tax = $request->tax;
            $indPro->remark = $request->remark;
            $indPro->save();
            $stockCheck = Stock::where('cid',$temp->cid)->where('store_id',$request->store_id)->where('project_id',$request->project_id)->first();

            if(!empty($stockCheck)){
                $stockCheck->stock = (int)$stockCheck->stock+(int)$temp->qty;
                $stockCheck->save();
                if($stockCheck){
                    $pl = new ProductLog();
                    $pl->store_id = $request->store_id;
                    $pl->project_id = $request->project_id;
                    $pl->company_id = $this->user->company_id;
                    $pl->created_by = $this->user->id;
                    $pl->module_id = $pi->id;
                    $pl->module_name = 'grn';
                    $pl->product_id = (!empty($temp->cid)) ? $temp->cid : '';
                    $pl->quantity = (!empty($temp->qty)) ? $temp->qty : '';
                    $pl->balance_quantity = $stockCheck->stock;
                    $pl->transaction_type = 'plus';
                    $pl->remark = $request->inv_remark;
                    $pl->save();
                }
            }else {
                $stock = new Stock();
                $stock->sku = $sku;
                $stock->po = $request->po_number;
                $stock->invoice_no = $request->invoice_no;
                $stock->inv_id = $pi->id;
                $stock->cid = (!empty($temp->cid)) ? $temp->cid : '';
                $stock->bid = (!empty($temp->bid)) ? $temp->bid : '';
                $stock->quantity = (!empty($temp->qty)) ? $temp->qty : '';
                $stock->stock = (!empty($temp->qty)) ? $temp->qty : '';
                $stock->unit = (!empty($temp->unit)) ? $temp->unit : '';
                $stock->price = (!empty($temp->price)) ? $temp->price : '';
                $stock->tax = (!empty($temp->tax)) ? $temp->tax : '';
                $stock->store_id = $request->store_id;
                $stock->project_id = $request->project_id;
                $stock->supplier_id = $request->supplier_id;
                $stock->company_id = $this->user->company_id;
                $stock->save();

                if ($stock) {
                    $pl = new ProductLog();
                    $pl->store_id = $request->store_id;
                    $pl->project_id = $request->project_id;
                    $pl->company_id = $this->user->company_id;
                    $pl->created_by = $this->user->id;
                    $pl->module_id = $pi->id;
                    $pl->module_name = 'grn';
                    $pl->product_id = (!empty($temp->cid)) ? $temp->cid : '';
                    $pl->quantity = (!empty($temp->qty)) ? $temp->qty : '';
                    $pl->balance_quantity = (!empty($temp->qty)) ? $temp->qty : '';
                    $pl->transaction_type = 'plus';
                    $pl->remark = $request->inv_remark;
                    $pl->save();
                }
            }
        }
        TmpInvoice::where('session_id', $sid)->delete();
        $request->session()->forget('piTmpDataSession');
        return Reply::redirect(route('admin.inventory.invoices'));
    }

    public function grnAgainstPo(){
        $this->pageTitle = 'app.menu.grn-against-po';
        $this->stores = Store::all();
        $this->suppliers = Supplier::where('company_id',$this->companyid)->get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        return view('admin.inventory.grn-against-po', $this->data);
    }

    public function againstPoData(Request $request)
    {
        $pos = PurchaseInvoice::query();
        if($request->store_id != 0){
            $pos->where('store_id', $request->store_id);
        }
        if($request->supplier_id != 0){
            $pos->where('supplier_id', $request->supplier_id);
        }
        if($request->project_id != 0){
            $pos->where('project_id', $request->project_id);
        }
        $pos->where('po_id', '<>', null);
        $pos->orderBy('id', 'DESC');
        $user = Auth::user();
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="' . route('admin.inventory.return', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Return Product"><i class="fa fa-reply" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('admin.purchase.payment', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="Make Payment"><i class="fa fa-inr" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="' . route('admin.inventory.viewGrnAgainstPo', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="View Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;';
                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="' . route('admin.indent.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 0){
                    $ret .= '<a href="javascript:;" class="btn btn-danger btn-circle approve-btn"
                      data-toggle="tooltip" data-key="1" data-id="'.$row->id.'" data-original-title="Refuse"><i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;';
                }
                if($row->status == 0 && $row->approve == 1){
                    $ret .= '<a href="javascript:;" class="btn btn-success btn-circle approve-btn"
                      data-toggle="tooltip" data-key="0" data-id="'.$row->id.'" data-original-title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;';
                }
                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'supplier_id',
                function ($row) {
                    return get_supplier_name($row->supplier_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending Payment';
                    }elseif($row->status == 1){
                        return 'Partially Paid';
                    }else{
                        return 'Fully Paid';
                    }
                }
            )
            ->editColumn(
                'dated',
                function ($row) {
                    return Carbon::parse($row->dated)->format('d-m-Y');
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function addNewGrn(Request $request){
        $this->pageTitle = 'Add Grn';
        $this->pageIcon = 'icon-people';
        $this->products = Bom::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->stores = Store::where('company_id',$this->companyid)->get();
        $this->suppliers = Supplier::where('company_id',$this->companyid)->get();
        $this->pos = PurchaseOrder::where('company_id',$this->companyid)->get();
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        if($request->session()->has('piTmpDataSession')) {
            $sid = $request->session()->get('piTmpDataSession');
            TmpInvoice::where('session_id', $sid)->delete();
            $request->session()->forget('piTmpDataSession');
        }
        return view('admin.inventory.add-new-grn', $this->data);
    }

    public function editGrn(Request $request,$id){
        $this->pageTitle = 'Edit Grn';
        $this->pageIcon = 'icon-people';
        $all_data = PiProducts::where('pi_id',$id)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseInvoice::where('id',$id)->first();
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this-> projects = Project::whereIn('id', $projecta)->get();
        $this->products = ProductCategory::where('company_id',$this->companyid)->get();
        $this->lproducts =  Bom::where('company_id',$this->companyid)->get();
        $this->brands = ProductBrand::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        return view('admin.inventory.edit-grn',$this->data);
    }

    public function postAddNewGrn(StoreGrnRequest $request){
        if($request->store_id == '' || $request->supplier_id == '' || $request->inv_dated == ''){
            return Reply::error('Mandatory field(s) cannot be left blank.');
        }
        $grn = PurchaseInvoice::orderBy('id','DESC')->first();
        if($grn == null){
            $grnNo = 'GRN-'.date("d-m-Y").'-1';
        }else {
            $grnNo = 'GRN-' . date("d-m-Y") . '-' . $grn->id;
        }
        $pi = new PurchaseInvoice();
        $pi->invoice_no = $grnNo;
        $pi->po_id = $request->po_number;
        $pi->indent_id = $request->indent_id;
        $pi->supplier_id = $request->supplier_id;
        $pi->company_id = $this->user->company_id;
        $pi->store_id = $request->store_id;
        $pi->project_id = $request->project_id;
        $pi->dated = $request->inv_dated;
        $pi->freight = is_numeric($request->freight) ? $request->freight : 0.00;
        $pi->remark = $request->inv_remark;
        $pi->payment_terms = $request->payment_terms;
        $pi->company_id = $this->user->company_id;
        $gt = 0;
        $sid = null;

        $cid = $request->product;
        $bid = $request->bid;
        $quantity = $request->quantity;
        $unit = $request->unit;
        $rprice = $request->price;
        $tax = $request->tax;
        $store_id = $request->store_id;
        $supplier_id = $request->supplier_id;

        foreach($rprice as $key=>$tmp){
            if($quantity[$key] !== '') {
                $price = $tmp + ($tmp * $tax[$key] / 100);
                $t = (is_numeric($price) && is_numeric($quantity[$key])) ? $price * $quantity[$key] : 0;
                $gt += $t;
            }
        }
        $pi->gt = $gt;
        $pi->save();

        $sku = time();
        foreach($quantity as $key=>$temp) {
            if($temp !== '') {
                $indPro = new PiProducts();
                $indPro->pi_id = $pi->id;
                $indPro->indent_id = $request->indent_id;
                $indPro->product_id = (!empty($cid[$key])) ? $cid[$key] : '';
                $indPro->quantity = (!empty($temp)) ? $temp : '';
                $indPro->bid = (!empty($bid[$key])) ? $bid[$key] : '';
                $indPro->unit_id = (!empty($unit[$key])) ? $unit[$key] : '';
                $indPro->date = $request->inv_dated;
                $indPro->price = (!empty($rprice[$key])) ? $rprice[$key] : '';
                $indPro->tax = (!empty($tax[$key])) ? $tax[$key] : '';
                $indPro->remark = $request->remark;
                $indPro->save();
                $stockCheck = Stock::where('cid',$cid[$key])->where('inv_id',$pi->id)->where('store_id',$request->store_id)->where('project_id',$request->project_id)->first();

                if(!empty($stockCheck)){
                    $stockCheck->stock = (int)$stockCheck->stock+(int)$temp;
                    $stockCheck->save();
                    if($stockCheck){
                        $pl = new ProductLog();
                        $pl->store_id = $request->store_id;
                        $pl->project_id = $request->project_id;
                        $pl->indent_id = $request->indent_id;
                        $pl->company_id = $this->user->company_id;
                        $pl->created_by = $this->user->id;
                        $pl->module_id = $pi->id;
                        $pl->module_name = 'grn';
                        $pl->product_id = (!empty($cid[$key])) ? $cid[$key] : '';
                        $pl->quantity = (!empty($temp)) ? $temp : '';
                        $pl->balance_quantity = $stockCheck->stock;
                        $pl->transaction_type = 'plus';
                        $pl->remark = $request->inv_remark;
                        $pl->save();
                    }
                }else {
                    $stock = new Stock();
                    $stock->sku = (!empty($sku)) ? $sku : '';
                    $stock->po = (!empty($request->po_number)) ? $request->po_number : '';
                    $stock->invoice_no = (!empty($grnNo)) ? $grnNo : '';
                    $stock->inv_id = (!empty($pi->id)) ? $pi->id : '';
                    $stock->cid = (!empty($cid[$key])) ? $cid[$key] : '';
                    $stock->bid = (!empty($bid[$key])) ? $bid[$key] : '';
                    $stock->quantity = (!empty($temp)) ? $temp : '';
                    $stock->stock = (!empty($temp)) ? $temp : '';
                    $stock->unit = (!empty($unit[$key])) ? $unit[$key] : '';
                    $stock->price = (!empty($rprice[$key])) ? $rprice[$key] : '';
                    $stock->tax = (!empty($tax[$key])) ? $tax[$key] : '';
                    $stock->store_id = (!empty($store_id)) ? $store_id : '';
                    $stock->project_id = (!empty($request->project_id)) ? $request->project_id : '';
                    $stock->supplier_id = (!empty($supplier_id)) ? $supplier_id : '';
                    $stock->company_id = $this->user->company_id;
                    $stock->save();

                    if ($stock) {
                        $pl = new ProductLog();
                        $pl->store_id = (!empty($request->store_id)) ? $request->store_id : '';
                        $pl->project_id = (!empty($request->project_id)) ? $request->project_id : '';
                        $pl->indent_id = (!empty($request->indent_id)) ? $request->indent_id : '';
                        $pl->company_id = $this->user->company_id;
                        $pl->created_by = $this->user->id;
                        $pl->module_id = (!empty($pi->id)) ? $pi->id : '';
                        $pl->module_name = 'grn';
                        $pl->product_id = (!empty($cid[$key])) ? $cid[$key] : '';
                        $pl->quantity = (!empty($temp)) ? $temp : '';
                        $pl->balance_quantity = (!empty($temp)) ? $temp : '';
                        $pl->transaction_type = 'plus';
                        $pl->remark = (!empty($request->inv_remark)) ? $request->inv_remark : '';
                        $pl->save();
                    }
                }
            }
        }
        return Reply::dataOnly(['taskID' => $pi->id]);
    }

    public function getPoData(Request $request){
        $poid = $request->pid;
        $html = array();
        $podata = PurchaseOrder::where('po_number',$poid)->first();
        $tmpData = PoProducts::where('po_id',$podata->id)->get();
        $html['date'] = $podata->dated;
        $html['payment_terms'] = $podata->payment_terms;
        $html['podata'] = '<table class="table">
                <thead><th>S.No.</th>
                <th>Category</th>
                <th>Brand</th>
                <th>Awarded Quantity</th>
                <th>Delivered Quantity</th>
                <th>Unit</th>
                <th>Price</th>
                <th>Tax (%)</th>
                <th>Received Quantity</th>
                </thead>
                <tbody>';
        if(count($tmpData)){
            $i = 1;
            foreach($tmpData as $data){
                $html['podata'] .= '<tr>
                    <td>'.$i.'</td>
                    <td>'.get_local_product_name($data->cid).'<input type="hidden" name="product[]" value="'.$data->cid.'"> </td>
                    <td>'.get_pbrand_name($data->bid).'<input type="hidden" name="bid[]" value="'.$data->bid.'"></td>
                    <td>'.$data->quantity.'</td>
                    <td>'.get_delivered_quantity($podata->po_number).'</td>
                    <td>'.get_unit_name($data->unit).'<input type="hidden" name="unit[]" value="'.$data->unit.'"></td>
                    <td>'.$data->price.'<input type="hidden" name="price[]" value="'.$data->price.'"></td>
                    <td>'.$data->tax.'<input type="hidden" name="tax[]" value="'.$data->tax.'"></td>
                    <td><input type="number" name="quantity[]" min="0" data-qty="'.$data->quantity.'" data-delqty="'.get_delivered_quantity($podata->po_number).'" class="form-control quantity"></td>
                    </tr>';
                $i++;
            }
        }else{
            $html['podata'] .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html['podata'] .= '</tbody></table>';
        return $html;
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $rfi = PurchaseInvoice::where('id',$request->task_id)->first();
                $file = new GrnFiles();
                $file->user_id = $this->user->id;
                $file->grn_id = $request->task_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/grn-files/'.$rfi->title, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('grn-files/'.$rfi->title, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'grn-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('grn-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();
                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$rfi->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());
                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());
                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('grn-files/'.$rfi->title.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::redirect(route('admin.inventory.grnAgainstPo'), __('GRN Updated Successfully'));
    }

    public function viewInvoice($poId){
        $this->pageTitle = 'View Invoice';
        $this->pageIcon = 'icon-people';
        $all_data = PiProducts::where('pi_id', $poId)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseInvoice::where('id', $poId)->first();
        return view('admin.inventory.view-invoice', $this->data);
    }

    public function viewGrnAgainstPo($poId){
        $this->pageTitle = 'View Invoice';
        $this->pageIcon = 'icon-people';
        $all_data = PiProducts::where('pi_id', $poId)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseInvoice::where('id', $poId)->first();
        return view('admin.inventory.view-grn-against-po', $this->data);
    }

    public function createReturn(){
        $this->pageTitle = 'Create Purchase Return';
        $this->pageIcon = 'icon-people';
        $this->grns = PurchaseInvoice::where('company_id',$this->user->company_id)->get();
        return view('admin.inventory.create-return', $this->data);
    }
    public function editReturn(Request $request,$id){
        $this->pageTitle = 'Edit Purchase Return';
        $this->pageIcon = 'icon-people';
        $this->retruns = ProductReturns::where('id',$id)->where('company_id',$this->companyid)->first();
        return view('admin.inventory.editPurchaseReturn', $this->data);
    }

    public function getPiProducts(Request $request){
        if(isset($request->pi_id)){
            $pi = PurchaseInvoice::find($request->pi_id);
            $piProducts = PiProducts::where('pi_id',$request->pi_id)->get();
            $html = ' 
            <div class="panel-heading row">
                <div class="col-md-2">
                    <label class="control-label">Invoice No</label>
                    <p><b>'.$pi->invoice_no.'</b></p>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Store</label>
                    <p><b>'.get_store_name($pi->store_id).'</b></p>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Project</label>
                    <p><b>'.get_project_name($pi->project_id).'</b></p>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Po Number</label>
                    <p><b>'.$pi->po_id.'</b></p>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Supplier</label>
                    <p><b>'.get_supplier_name($pi->supplier_id).'</b></p>
                </div>
                <div class="col-md-2">
                    <label class="control-label">Date</label>
                    <p><b>'.Carbon::parse($pi->dated)->format('d-m-Y').'</b></p>
                </div>
            </div>
            
            <input type="text" style="display: none;" name="pi_id" value="'.$request->pi_id.'">
            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Brand</th>
                        <th>Unit</th>
                        <th>Quantity</th>
                        <th>Return Quantity</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($piProducts as $pp){
                    $html .= '
                        <tr>
                            <td><input type="hidden" name="product_id[]" value="'.$pp->product_id.'" class="form-control">'.get_local_product_name($pp->product_id).'</td>
                            <td><input type="hidden" name="bid[]" value="'.$pp->bid.'" class="form-control">'.get_pbrand_name($pp->bid).'</td>
                            <td><input type="hidden" name="unit_id[]" value="'.$pp->unit_id.'" class="form-control">'.get_unit_name($pp->unit_id).'</td>
                            <td><input type="hidden" name="quantity[]" value="'.$pp->quantity.'" class="form-control">'.$pp->quantity.'</td>
                            <td><input type="number" name="return_quantity[]" class="form-control ret_qty" min="0" data-qty="'.$pp->quantity.'"></td>
                            <td><input type="text"   name="remark[]" class="form-control"></td>
                        </tr>
                    ';
                }
                $html .= '</tbody></table>';
            return $html;
        }
    }

    public function storeReturn(Request $request){
        $purId = PurchaseInvoice::find($request->pi_id);
        foreach ($request->product_id as $key=>$product) {
            if(!empty($request->return_quantity[$key])) {
                $pr = new ProductReturns();
                $pr->invoice_id = $request->pi_id;
                $pr->store_id = $purId->store_id;
                $pr->project_id = $purId->project_id;
                $pr->cid = $product;
                $pr->bid = !empty($request->bid[$key]) ? $request->bid[$key] : '';
                $pr->unit = !empty($request->unit_id[$key]) ? $request->unit_id[$key] : '';
                $pr->quantity = !empty($request->return_quantity[$key]) ? $request->return_quantity[$key] : '';
                $pr->remark = !empty($request->remark[$key]) ? $request->remark[$key] : '';
                $pr->company_id = $this->user->company_id;
                $pr->save();

                $stockCheck = Stock::where('cid', $product)->where('store_id', $purId->store_id)->where('project_id', $purId->project_id)->first();
                if (!empty($stockCheck)) {
                    if(!empty($request->return_quantity[$key]) && !empty($request->return_quantity[$key])>$stockCheck->quantity){
                        return Reply::Error('Return Quantity is greater than Available Quantity');
                    }
                    $stockCheck->stock = (int)$stockCheck->stock - (int)$request->return_quantity[$key];
                    $stockCheck->save();
                    if ($stockCheck) {
                        $pl = new ProductLog();
                        $pl->store_id = $purId->store_id;
                        $pl->project_id = $purId->project_id;
                        $pl->company_id = $this->user->company_id;
                        $pl->created_by = $this->user->id;
                        $pl->module_id = $request->pi_id;
                        $pl->module_name = 'purchase-return';
                        $pl->product_id = (!empty($product)) ? $product : '';
                        $pl->quantity = (!empty($request->return_quantity[$key])) ? $request->return_quantity[$key] : '';
                        $pl->balance_quantity = $stockCheck->stock;
                        $pl->transaction_type = 'minus';
                        $pl->remark = 'Purchase return against GRN';
                        $pl->save();
                    }
                }
            }
        }
        return Reply::redirect(route('admin.inventory.returns'), __('Returned Successfully'));
    }

    public function boqProductIssueIndex(){
        $this->pageTitle = 'Boq Product Issue';
        $this->pageIcon = 'icon-people';
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id',$projectlist)->get();
        $this->stores = Store::all();
        $this->subprojects = Title::all();
        $this->issuedProducts = ProductIssue::where('sub_project_id','<>', null)->orWhere('segment_id','<>', null)->orWhere('task_id','<>', null)->groupBy('unique_id')->get();
        return view('admin.boq-product-issue.index', $this->data);
    }

    public function boqProductIssue(){
        $this->pageTitle = 'Issue Product for BOQ Task';
        $this->pageIcon = 'icon-people';
        $this->projects = Project::all();
        $this->products = Product::all();
        $this->units = Units::all();
        $this->stores = Store::all();
        return view('admin.boq-product-issue.create', $this->data);
    }

    public function storeBoqProductIssue(Request $request){
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $unique_id = 'PI-'.date("d-m-Y").'-'.count($product_id);
        $unit_id = '';
        if(isset($request->unit_id)) {
            $unit_id = $request->unit_id;
        }
        $quantity = $request->quantity;
        $issued_by = $this->user->id;

        foreach ($product_id as $key => $value) {
            if($quantity[$key] !== '') {
                $scheck = Stock::where('cid',$value)->where('store_id', $request->store_id)->where('project_id',$project_id)->first();
                $bal = 0;
                if($scheck !== null){
                    if($quantity[$key] > $scheck->stock){
                        return Reply::success(__('Issued Quantity greater then stock'));
                    }else{
                        $bal = (int)$scheck->stock - (!empty($quantity[$key])) ? (int)$quantity[$key] : 0;
                        $scheck->stock = $bal;
                        $scheck->save();
                    }
                }
                $pro = new ProductIssue();
                $pro->project_id = $project_id;
                $pro->product_id = (!empty($value)) ? $value : '';
                $pro->unique_id = $unique_id;
                $pro->store_id = $request->store_id;
                $pro->sub_project_id = (!empty($request->sub_project_id)) ? $request->sub_project_id : '';
                $pro->segment_id = ( !empty($request->segment_id)) ? $request->segment_id : '';
                $pro->task_id = (!empty($request->task_id)) ? $request->task_id : '';
                $pro->unit_id = (!empty($unit_id[$key])) ? $unit_id[$key] : '';
                $pro->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                $pro->issued_by = $issued_by;
                $pro->company_id = $this->user->company_id;
                $pro->remark = $request->remark;
                $pro->save();

                if($pro){
                    $pl = new ProductLog();
                    $pl->store_id = $request->store_id;
                    $pl->project_id = $request->project_id;
                    $pl->company_id = $this->user->company_id;
                    $pl->created_by = $this->user->id;
                    $pl->module_id = $pro->id;
                    $pl->module_name = 'product_issue_for_boq';
                    $pl->product_id = (!empty($product_id[$key])) ? $product_id[$key] : '';
                    $pl->quantity = (!empty($quantity[$key])) ? $quantity[$key] : '';
                    $pl->balance_quantity = $bal;
                    $pl->transaction_type = 'minus';
                    $pl->remark = 'Product Issued for boq';
                    $pl->save();
                }
            }
        }
        return Reply::success(__('Product issued successfully'));
    }

    public function boqProductIssueData(Request $request){

        $user = $this->user;
        $pos = ProductIssue::where('sub_project_id','<>', null)->orWhere('segment_id','<>', null)->orWhere('task_id','<>', null);
        if($request->store_id != 0){
            $pos = $pos->where('store_id', $request->store_id);
        }
        if($request->sub_project_id != 0){
            $pos = $pos->where('sub_project_id', $request->sub_project_id);
        }
        if($request->segment_id != 0){
            $pos = $pos->where('segment_id', $request->segment_id);
        }
        if($request->task_id != 0){
            $pos = $pos->where('task_id', $request->task_id);
        }
        if($request->project_id != 0){
            $pos = $pos->where('project_id', $request->project_id);
        }
        $pos = $pos->orderBy('id', 'DESC')->groupBy('unique_id')->get();
        $user = $this->user;
        return DataTables::of($pos)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '<a href="'.route('admin.product-issue.issuedProductDetail',[$row->unique_id]).'" class="btn btn-success btn-circle"
                           data-toggle="tooltip" data-task-id="'.$row->unique_id.'" data-original-title="View Issued Products">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>';
//                        <a href="'.route('admin.product-issue.return',[$row->unique_id]).'" class="btn btn-warning btn-circle"
//                           data-toggle="tooltip" data-task-id="'.$row->unique_id.'" data-original-title="Return Issued Products">
//                            <i class="fa fa-minus" aria-hidden="true"></i>
//                        </a>';
                return $ret;
            })
            ->editColumn(
                'task_id',
                function ($row) {
                    return get_cost_name($row->store_id);
                }
            )
            ->editColumn(
                'quantity',
                function ($row) {
                    $qcount = \App\ProductIssue::where('unique_id',$row->unique_id)->get();
                    return count($qcount);
                }
            )
            ->editColumn(
                'unique_id',
                function ($row) {
                    return $row->unique_id;
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format('d-m-Y');
                }
            )
            ->editColumn(
                'issued_by',
                function ($row) {
                    return get_user_name($row->issued_by);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function boqGetTitlesByProjects(Request $request){
        $projectid = $request->projectid;
        $projectsarray = array();
        if($projectid){
            $subprojectarray =  Title::where('project_id',$projectid)->get();
            $subprojects = '<option value="">Select Sub Project</option>';
            if(count($subprojectarray)>0){
                foreach($subprojectarray as $subprojectar){
                    $subprojects .= '<option value="'.$subprojectar->id.'">'.$subprojectar->title.'</option>';
                }
            }
            $projectsarray['titles'] = $subprojects;
            $segementsarary = Segment::where('projectid',$projectid)->get();
            $segmenthtml = '<option value="">Select Segment</option>';
            if(count($segementsarary)>0){
                foreach($segementsarary as $segementsar){
                    $segmenthtml .= '<option value="'.$segementsar->id.'">'.$segementsar->title.'</option>';
                 }
            }
            $projectsarray['segments'] =$segmenthtml;
            $costimeslist = '<option value="">Select Task</option>';
                $activityesarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('position','row')->orderBy('inc','asc')->get();

            if(count($activityesarray)>0){
                foreach($activityesarray as $activityesa){
                    $costimeslist .= '<option value="'.$activityesa->id.'" disabled>'.get_dots_by_level($activityesa->level).' '.$activityesa->itemname.'</option>';

                    $projectcostitemsarray =  ProjectCostItemsProduct::join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')
                        ->where('project_cost_items_product.project_id',$projectid)
                        ->where('project_cost_items_product.position_id',$activityesa->id)
                        ->pluck('cost_items.cost_item_name','project_cost_items_product.id');
                    if(count($projectcostitemsarray)>0){
                        foreach($projectcostitemsarray as $projectcostitemsa => $projectcosname){

                            $costimeslist .= '<option value="'.$projectcostitemsa.'">--'.$projectcosname.'</option>';
                        }
                    }
                }
            }

            $projectsarray['cositems'] = $costimeslist;
        }
        return   $projectsarray;
    }

    public function inventoryApprove($id, $val){
        DB::beginTransaction();
        $ind = PurchaseInvoice::find($id);
        $ind->approve = $val;
        $ind->save();
        DB::commit();
        return Reply::success('Invoice updated successfully');
    }
    public function boqGetCostitemByTitle(Request $request){
        $projectid = $request->projectid;
        $subprojectid = $request->subproject_id;
        $projectsarray = array();
        if($projectid){
            $costimeslist = '<option value="">Select Task</option>';
            $activityesarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('position','row');
            if(!empty($subprojectid)){
                $activityesarray = $activityesarray->where('title_id',$subprojectid);
            }
            $activityesarray = $activityesarray->orderBy('inc','asc')->get();

            if(count($activityesarray)>0){
                foreach($activityesarray as $activityesa){
                    $costimeslist .= '<option value="'.$activityesa->id.'" disabled>'.get_dots_by_level($activityesa->level).' '.$activityesa->itemname.'</option>';

                    $projectcostitemsarray =  ProjectCostItemsProduct::join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')
                        ->where('project_cost_items_product.project_id',$projectid)
                        ->where('project_cost_items_product.position_id',$activityesa->id);
                    if(!empty($subprojectid)){
                        $projectcostitemsarray = $projectcostitemsarray->where('title_id',$subprojectid);
                    }
                    $projectcostitemsarray = $projectcostitemsarray->pluck('cost_items.cost_item_name','project_cost_items_product.id');
                    if(count($projectcostitemsarray)>0){
                        foreach($projectcostitemsarray as $projectcostitemsa => $projectcosname){

                            $costimeslist .= '<option value="'.$projectcostitemsa.'">--'.$projectcosname.'</option>';
                        }
                    }
                }
            }
            $projectsarray['cositems'] = $costimeslist;
        }
        return  $projectsarray;
    }
    public function invReport(Request $request){
        $user = $this->user;
        $dataarray = array();
        $projectlist = explode(',',$user->projectlist);
        $this->projectarray = Project::whereIn('id',$projectlist)->get();
        $this->category = ProductCategory::where('company_id',$user->company_id)->get();
        $this->productarray = Product::where('company_id',$user->company_id)->get();
        $this->date = $request->date;
        if($request->reporttype == 'inventory'){

        }if($request->reporttype == 'material'){

        }if($request->reporttype == 'material-category'){

        }
        $messageview = View::make('admin.inventory.reporthtml',$dataarray);
        $mailcontent = $messageview->render();
        $this->reporthtml = $mailcontent;
        return view('admin.inventory.inventory-report',$this->data);
    }
    public function invReportPDF(Request $request){
        $user = $this->user;
        $this->category = ProductCategory::where('company_id',$user->company_id)->get();
        $this->project  = $request->project;
        $this->product  = $request->product;
        $this->date  = $request->date;
        if($request->exporttype == 'smrpdf'){
            $products =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project)->where('product_id',$request->product)->where('company_id',$this->companyid)
                ->orderBy('id', 'ASC')->get();
            $this->productLogs = $products;
            $pdf = PDF::loadView('admin.inventory.smr-report',$this->data);
            return $pdf->download('material-report.pdf');
        }if($request->exporttype == 'mcrpdf'){
            $this->category = $request->category;
            $productarray = Product::where('category_id',$request->category)->pluck('id');
            $productgroup =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project)->whereIn('product_id',$productarray)->where('company_id',$this->companyid)
                ->orderBy('id', 'ASC')->groupBy('product_id')->pluck('product_id');
            $products =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project)->whereIn('product_id',$productarray)->where('company_id',$this->companyid)
                ->orderBy('id', 'ASC')->get();
            $this->productLogs = $products;
            $this->productgroup = $productgroup;
            $pdf = PDF::loadView('admin.inventory.mcr-report',$this->data);
            return $pdf->download('material-category-report.pdf');
        }else{
            $products =  Stock::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project)->where('company_id',$user->companyid)->get();
            $this->products = $products;
            $pdf = PDF::loadView('admin.inventory.stockPDF',$this->data);
            return $pdf->download('inventory-report.pdf');
        }


    }
}
