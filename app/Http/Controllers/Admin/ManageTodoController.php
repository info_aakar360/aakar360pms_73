<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Tasks\StoreTask;
use App\Http\Requests\Todo\StoreTodo;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\Segment;
use App\SubTask;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\Title;
use App\Todo;
use App\TodoFiles;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageTodoController extends AdminBaseController
{

    use ProjectProgress;

    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.todo';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if(!in_array('tasks',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->orderBy('id','desc')->get();
        $this->categories = TaskCategory::orderBy('id','desc')->get();
        return view('admin.todo.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->projectlist = Project::whereIn('id',$projectlist)->orderBy('id','desc')->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->allTodos = Todo::where('company_id',$user->company_id)->get();
        return view('admin.todo.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTodo $request)
    {

        $user = $this->user;
        $task = new Todo();
        $task->company_id = $user->company_id;
        $task->title = $request->title;
        if($request->description != ''){
            $task->description = $request->description;
        }
        $task->start_date = date('Y-m-d',strtotime($request->start_date));
        $task->due_date = date('Y-m-d',strtotime($request->due_date));
        $task->assign_to = $request->assign_to;
        $task->project_id = $request->project_id ?: 0;
        $task->subproject_id = $request->subproject_id ?: 0;
        $task->segment_id = $request->segment_id ?: 0;
        $task->costitem_id = $request->costitem_id ?: 0;
        $task->priority = $request->priority;
        $task->task_category_id = $request->category_id;
        $task->status = $request->status;
        $task->added_by = $user->id;
        $task->dependent_todo_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_todo_id') && $request->dependent_todo_id != '' ? $request->dependent_todo_id : null;
        $task->save();

        if ($request->has('repeat') && $request->repeat == 'yes') {
            $repeatCount = $request->repeat_count;
            $repeatType = $request->repeat_type;
            $repeatCycles = $request->repeat_cycles;
            $startDate = date('Y-m-d',strtotime($request->start_date));
            $dueDate = date('Y-m-d',strtotime($request->due_date));

            if(!empty($repeatCycles)){
            for ($i = 1; $i < $repeatCycles; $i++) {
                $repeatStartDate = date('Y-m-d',strtotime($startDate));
                $repeatDueDate = date('Y-m-d',strtotime($dueDate));

                if ($repeatType == 'day') {
                    $repeatStartDate = date('Y-m-d',strtotime('+'.$repeatCount.' days',strtotime($repeatStartDate)));
                    $repeatDueDate = date('Y-m-d',strtotime('+'.$repeatCount.' days',strtotime($repeatDueDate)));
                } else if ($repeatType == 'week') {
                    $repeatStartDate = date('Y-m-d',strtotime('+'.$repeatCount.' week',strtotime($repeatStartDate)));
                    $repeatDueDate = date('Y-m-d',strtotime('+'.$repeatCount.' week',strtotime($repeatDueDate)));
                } else if ($repeatType == 'month') {
                    $repeatStartDate = date('Y-m-d',strtotime('+'.$repeatCount.' months',strtotime($repeatStartDate)));
                    $repeatDueDate = date('Y-m-d',strtotime('+'.$repeatCount.' months',strtotime($repeatDueDate)));
                } else if ($repeatType == 'year') {
                    $repeatStartDate = date('Y-m-d',strtotime('+'.$repeatCount.' year',strtotime($repeatStartDate)));
                    $repeatDueDate = date('Y-m-d',strtotime('+'.$repeatCount.' year',strtotime($repeatDueDate)));
                }

                $newTask = new Todo();
                $newTask->title = $request->title;
                if ($request->description != '') {
                    $newTask->description = $request->description;
                }
                $newTask->start_date = $repeatStartDate;
                $newTask->due_date = $repeatDueDate;
                $newTask->added_by = $this->user->id;
                $newTask->project_id = $request->project_id ?: 0;
                $newTask->subproject_id = $request->subproject_id ?: 0;
                $newTask->segment_id = $request->segment_id ?: 0;
                $newTask->costitem_id = $request->costitem_id;
                $newTask->task_category_id = $request->category_id;
                $newTask->priority = $request->priority;
                $newTask->added_by = $this->user->id;
                $newTask->recurring_todo_id = $task->id;
                $newTask->status = $request->status;
                $newTask->save();
            }
            }
        }
        return Reply::dataOnly(['todoID' => $task->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->projectlist = Project::orderBy('id','desc')->get();
        if(is_numeric($id)){
            $this->project = Project::findOrFail($id);

            $this->categories = TaskCategory::all();
            $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
            if($completedTaskColumn)
            {
                $this->allTasks = Todo::where('board_column_id', $completedTaskColumn->id)
                    ->where('project_id', $id)
                    ->get();
            }else {
                $this->allTasks = [];
            }
        }else{
            $this->allTasks = [];
        }
        return view('admin.projects.todo.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);

        $todo = Todo::findOrFail($id);
        $this->todo = $todo;
        $this->projectlist = Project::whereIn('id',$projectlist)->orderBy('id','desc')->get();
        $itemnoslug =  $titlearray = ProjectCostItemsPosition::where('project_id',$todo->project_id)->where('title',$todo->subproject_id)->where('position','row')->get();
         $this->categories = $itemnoslug;
        $this->employees = Employee::getAllEmployees($user);
        $this->allTodos = Todo::where('company_id',$user->company_id)->get();
         $this->attachmentlist = TodoFiles::where('todo_id',$todo->id)->where('reply_id','0')->get();
        $this->titlelist = Title::where('project_id', $todo->project_id)->get();
        $this->segmentlist = Segment::where('projectid', $todo->project_id)->where('titleid', $todo->subproject_id)->get();
        $this->costitemlist = ProjectCostItemsProduct::join('cost_items', 'cost_items.id', '=', 'project_cost_items_product.cost_items_id')
            ->where('project_cost_items_product.project_id', $todo->project_id)
            ->where('project_cost_items_product.title', $todo->subproject_id)
            ->orderBy('project_cost_items_product.inc','asc')->pluck('cost_items.cost_item_name', 'project_cost_items_product.id');
        $this->activitiesarray = !empty($todo->project_id) ? ProjectCostItemsPosition::where('project_id', $todo->project_id)->where('title', $todo->subproject_id)->where('position', 'row')->orderBy('inc','asc')->get() : array();
         return view('admin.todo.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Todo::findOrFail($id);
        $task->title = $request->title;
        if($request->description != ''){
            $task->description = $request->description;
        }
        $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date)->format('Y-m-d');
        $task->assign_to = $request->assign_to;
        $task->project_id = $request->project_id ?: 0;
        $task->subproject_id = $request->subproject_id ?: 0;
        $task->segment_id = $request->segment_id ?: 0;
        $task->priority = $request->priority;
        $task->task_category_id = $request->category_id;
        $task->costitem_id = $request->costitem_id;
        $task->added_by = $this->user->id;
        $task->status = $request->status;
        $task->dependent_todo_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_todo_id') && $request->dependent_todo_id != '' ? $request->dependent_todo_id : null;
        $task->save();
        return Reply::dataOnly(['todoID' => $task->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Todo::findOrFail($id);

        $taskFiles = TodoFiles::where('todo_id', $id)->get();

        $task->delete();

        return Reply::success(__('messages.todoDeletedSuccessfully'));
    }

    public function changeStatus(Request $request) {
        $taskId = $request->taskId;
        $status = $request->status;
        $taskBoardColumn = TaskboardColumn::where('slug', $status)->first();
        $task = Todo::with('project')->findOrFail($taskId);
        $task->board_column_id = $taskBoardColumn->id;
//        $task->status = $status;

        if($taskBoardColumn->slug == 'completed'){
            $task->completed_on = Carbon::now()->format('Y-m-d H:i:s');
            $task->save();

            // send task complete notification
            $notifyUser = User::withoutGlobalScope('active')->findOrFail($task->user_id);
            $notifyUser->notify(new TaskCompleted($task));

            if($task->project_id != null){
                if($task->project->client_id != null  && $task->project->allow_client_notification == 'enable') {
                    $notifyClient = User::findOrFail($task->project->client_id);
                    $notifyClient->notify(new TaskCompleted($task));
                }
            }

            $admins = User::allAdmins($task->user_id);

            Notification::send($admins, new TaskCompleted($task));
        }else{
            $task->completed_on = null;
        }

        $task->save();

        if($task->project != null) {
            if($task->project->calculate_task_progress == "true") {
                //calculate project progress if enabled
                $this->calculateProjectProgress($task->project_id);
            }

            $this->project = Project::findOrFail($task->project_id);
            $this->project->todo = Todo::whereProjectId($this->project->id)->orderBy($request->sortBy, 'desc')->get();
        }
        $this->task = $task;

        $view = view('admin.projects.todo.task-list-ajax', $this->data)->render();

        return Reply::successWithData(__('messages.taskUpdatedSuccessfully'), ['html' => $view, 'textColor' => $task->board_column->label_color, 'column' => $task->board_column->column_name]);
    }

    public function sort(Request $request) {
        $projectId = $request->projectId;
        $this->sortBy = $request->sortBy;
        $taskBoardColumn = TaskboardColumn::where('slug', 'completed')->first();
        $this->project = Project::findOrFail($projectId);
        if($request->sortBy == 'due_date'){
            $order = "asc";
        }
        else{
            $order = "desc";
        }

        $todo = Todo::whereProjectId($projectId)->orderBy($request->sortBy, $order);

        if($request->hideCompleted == '1'){
            $todo->where('board_column_id', '!=', $taskBoardColumn->id);
        }

        $this->project->todo = $todo->get();

        $view = view('admin.projects.todo.task-list-ajax', $this->data)->render();

        return Reply::dataOnly(['html' => $view]);
    }

    public function checkTask($taskID){
        $task = Todo::findOrFail($taskID);
        $subTask = SubTodo::where(['todo_id' => $taskID, 'status' => 'incomplete'])->count();

        return Reply::dataOnly(['taskCount' => $subTask, 'lastStatus' => $task->board_column->slug]);
    }

    public function data(Request $request) {
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $projectId = $request->projectId;
        $todo = Todo::leftJoin('projects', 'projects.id', '=', 'todo.project_id')
             ->select('todo.*', 'projects.project_name');
        if(!empty($projectId)){
                $todo =    $todo->where('projects.id', $projectId);
        }elseif(!empty($projectlist)){
                $todo =    $todo->whereIn('projects.id', $projectlist);
        }
        $todo = $todo->orderBy('todo.start_date','asc')->get();
        return DataTables::of($todo)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'.route('admin.todo.edit',$row->id).'" class="btn btn-info btn-circle edit-task"
                      data-toggle="tooltip" data-task-id="'.$row->id.'" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        &nbsp;&nbsp;<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-todo-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'start_date',
                function ($row) {
                    return Carbon::parse($row->start_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'due_date',
                function ($row) {
                    return Carbon::parse($row->due_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'private',
                function ($row) {
                    return !empty($row->private) ? 'Private' : 'Public';
                }
            )
            ->editColumn(
                'assign_to',
                function ($row) {
                    $dis = '';
                    $distributionarra = explode(',',$row->assign_to);
                    foreach ($distributionarra as $distribut){
                        $dis .= '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_users_employee_name($distribut,$row->company_id)).'</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn('title', function($row){
                return '<a href="javascript:;" data-task-id="'.$row->id.'" class="show-task-detail">'.ucfirst($row->title).'</a>';
            })
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_user_name($row->added_by)).'</a></div></div>';
                }
            )
            ->rawColumns(['assign_to', 'action', 'start_date', 'due_date', 'added_by', 'title'])
            ->removeColumn('project_id')
            ->removeColumn('image')
            ->removeColumn('created_image')
            ->make(true);
    }

    /**
     * @param $projectId
     */
    public function export($projectId) {

        $todo = Todo::leftJoin('projects', 'projects.id', '=', 'todo.project_id')
            ->select('todo.id', 'projects.project_name', 'todo.title', 'users.name', 'users.image',  'todo.due_date')
            ->where('projects.id', $projectId);

        $attributes =  ['image', 'due_date'];

        $todo = $todo->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project','Title','Assigned TO', 'Status','Due Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($todo as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));

                });

            });

        })->download('xlsx');
    }
    public function dependentTaskLists($projectId, $taskId = null)
    {
        $this->allTasks = Todo::where('project_id', $projectId);

        if($taskId != null)
        {
            $this->allTasks = $this->allTasks->where('id', '!=', $taskId);
        }

        $this->allTasks = $this->allTasks->get();
        $list = view('admin.todo.dependent-todo-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }
    public function storeImage(Request $request)
    {

        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $company = $this->user->company_id;
                $file = new TodoFiles();
                $file->company_id = $company;
                $file->added_by = $this->user->id;
                $file->todo_id = $request->todo_id;
                $file->reply_id = $request->reply_id ?: 0;
                switch($storage) {
                    case 'local':
                        $destinationPath = 'uploads/todo-files/'.$file->todo_id;
                        if (!file_exists(''.$destinationPath)) {
                            mkdir(''.$destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('/todo-files/'.$file->todo_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'todo-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('todo-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->todo_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->todo_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->todo_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('todo-files/'.$request->todo_id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/todo-files/'.$request->todo_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }

        }
       /* return Reply::redirect(route('admin.todo.index'), __('modules.projects.projectUpdated'));*/

    }
    public function removeFile($id){
        $inspectionFiles = TodoFiles::findOrFail($id);
        $storage = storage();
        $companyid = $inspectionFiles->company_id;
        switch ($storage) {
            case 'local':
                $path = 'uploads/todo-files/' . $inspectionFiles->todo_id.'/'.$inspectionFiles->hashname;
                if(file_exists($path)){
                    unlink($path);
                }
              break;
            case 's3':
                Storage::disk('s3')->delete($companyid.'/todo-files/' . $inspectionFiles->todo_id.'/'.$inspectionFiles->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('todo-files/' . $inspectionFiles->todo_id.'/'.$inspectionFiles->hashname);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('todo-files/' . $inspectionFiles->todo_id.'/'.$inspectionFiles->hashname);
                break;
        }
        $inspectionFiles->delete();
        return Reply::success(__('image deleted successfully'));
    }
}
