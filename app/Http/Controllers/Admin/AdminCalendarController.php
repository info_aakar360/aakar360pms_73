<?php

namespace App\Http\Controllers\Admin;

use App\Indent;
use App\InspectionName;
use App\Meetings;
use App\Observations;
use App\Project;
use App\PunchItem;
use App\Rfi;
use App\Submittals;
use App\Task;
use App\TaskboardColumn;
use App\Todo;
use Illuminate\Http\Request;

class AdminCalendarController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.calender';
        $this->pageIcon = 'icon-calender';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if(!in_array('projects',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }
    public function index() {
        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();
        $this->tasks = Task::where('tasks.board_column_id', $taskBoardColumn->id)->get();
        return view('admin.task-calendar.index', $this->data);
    }

    public function show($id) {
        $this->task = Task::findOrFail($id);
        return view('admin.task-calendar.show', $this->data);
    }
    public function calenderIndex(Request $request) {

        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        if($request->isMethod('post')){
            $this->indentlist = Indent::join("indent_products","indent_products.indent_id","=","indents.id")
                ->select("indents.*","indent_products.expected_date")->where('indents.project_id',$request->project)->get();
            $this->todolist = Todo::where('project_id',$request->project)->get();
            $this->tasks = Task::where('project_id',$request->project)->get();
            $this->issueslist = PunchItem::where('projectid',$request->project)->get();
            $this->rfilist = Rfi::where('projectid',$request->project)->get();
            $this->submittalslist = Submittals::where('projectid',$request->project)->get();

        }else{
            $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
            $companylist[] = $user->company_id;
            $companylist = array_unique(array_filter($companylist));
            $this->indentlist = Indent::join("indent_products","indent_products.indent_id","=","indents.id")
                ->select("indents.*","indent_products.expected_date")->whereIn('indents.company_id',$companylist)->get();
            $this->todolist = Todo::whereIn('company_id',$companylist)->get();
            $this->tasks = Task::whereIn('company_id',$companylist)->get();
            $this->issueslist = PunchItem::whereIn('company_id',$companylist)->get();
            $this->rfilist = Rfi::whereIn('company_id',$companylist)->get();
            $this->submittalslist = Submittals::whereIn('company_id',$companylist)->get();
          }
        $this->inspectionlist = InspectionName::join('inspection_assign_form','inspection_assign_form.inspection_id','=','inspection_name.id')
            ->select('inspection_name.*','inspection_assign_form.due_date')->whereRaw('FIND_IN_SET('.$user->id.',inspection_assign_form.assign_to)')->get();
        $this->observationslist = Observations::where('start_date','<>','')->where('due_date','<>','')->whereRaw('FIND_IN_SET('.$user->id.',assign_to)')->get();
        $this->meetingslist = Meetings::join('meeting_details','meeting_details.meeting_id','=','meetings.id')
            ->select('meetings.*','meeting_details.meeting_date','meeting_details.title as meetingtitle','meeting_details.start_time','meeting_details.finish_time')->where('meetings.company_id',$user->company_id)->get();

        return view('admin.taskscalendar.index', $this->data);
    }
}
