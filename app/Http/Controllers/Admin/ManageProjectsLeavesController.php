<?php

namespace App\Http\Controllers\Admin;

use App\AssignWorkRules;
use App\Designation;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Leaves\StoreLeave;
use App\Http\Requests\Leaves\UpdateLeave;
use App\Http\Requests\LeaveRules\LeaveRulesRequest;
use App\Http\Requests\LeaveRules\UpdateLeaveRulesRequest;
use App\Http\Requests\LeaveRules\StoreWorkdayRequest;
use App\Leave;
use App\LeaveAssigned;
use App\LeaveRulesSettings;
use App\LeavesRules;
use App\LeaveType;
use App\Notifications\LeaveStatusApprove;
use App\Notifications\LeaveStatusReject;
use App\Notifications\LeaveStatusUpdate;
use App\Project;
use App\ProjectWorkWeek;
use App\Skill;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ManageProjectsLeavesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.projects');
        $this->pageIcon = 'icon-logout';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('projects', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
        for ($m = 1; $m <= 12; $m++) {
            $month[] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
        }

        $this->months = $month;
        $this->currentMonth = date('F');
    }
    public  function selectWorkweekProjects(){
        $user = $this->user;
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->user = $user;
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        return view('admin.projects.leaves.work_week_projects',$this->data);
    }
    public  function createWorkWeek($id){
        $user = $this->user;
        $this->user = $user;
        $this->projects = Project::find($id);
        $workweek = ProjectWorkWeek::where('project_id',$id)->first();
        $this->workweek =  !empty($workweek) ? array_filter(explode(',',$workweek->weekdays)) : '';
        return view('admin.projects.leaves.create_work_week',$this->data);
    }
    public function storeWorkWeek(Request $request){

        $store_workday = ProjectWorkWeek::where('project_id',$request->projectid)->first();
        if(empty($store_workday->id)){
            $store_workday = new ProjectWorkWeek();
            $store_workday->project_id = $request->projectid;
        }
        $store_workday->weekdays = !empty($request->weekvalue) ? implode(',',array_filter($request->weekvalue)) : '';
        $store_workday->save();
        return Reply::success(__('messages.addLeaveRulesSettings'));
    }
}
