<?php

namespace App\Http\Controllers\Admin;

use App\AwardContractCategory;
use App\AwardContractFiles;
use App\AwardContractProducts;
use App\AwardedContracts;
use App\BoqCategory;
use App\ContractInvoice;
use App\Contractors;
use App\CostItems;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Tax\StoreTax;
use App\InspectionFile;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\Segment;
use App\Tax;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersCategory;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\Units;
use App\User;
use View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\FileManager;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\Facades\DataTables;

class AwardedContractorController extends AdminBaseController
{
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    /**
     * ManageProjectFilesController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.tenders';
        $this->activeMenu = 'pms';

    }
    public function awardedContracts(){
        $user = $this->user;
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->tendersarray = Tenders::orderBy('id','desc')->get();
        return view('admin.tenders.awardedContracts', $this->data);
    }
    public function awardedData(Request $request)
    {
        $user = $this->user;
        $contractor = $request->contractor;
        $inspectionarray = AwardedContracts::where('company_id',$user->company_id);
        if(!empty($contractor)){
            $inspectionarray = $inspectionarray->where('contractor_id',$contractor);
        }
        $inspectionarray = $inspectionarray->get();
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a href="'.route('admin.awardedcontracts.awardedBiddingProducts',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Awared Products details"><i class="fa fa-eye" aria-hidden="true"></i></a>';

                /*    if($row->tendertype=='direct'){
                      $actionlink .= '<a href="'.route('admin.awardedcontracts.awardedtenderBidding',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                                         data-toggle="tooltip" data-original-title="Awared Tender details"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

                      $actionlink .= ' ';
                  }*/
                  if($row->user_id==$user->id){
                      $contractinvoice = ContractInvoice::where('awarded_contract_id',$row->id)->first();
                      if(empty($contractinvoice)){
                          $actionlink .= ' ';
                          $actionlink .='<a href="javascript:;" data-cat-id="'.$row->id.'" class="btn btn-sm btn-danger btn-circle delete-category" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                      }
                }
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn('project_name', function ($row) {
                if (!is_null($row->project_id)) {
                    return  ucfirst(get_project_name($row->project_id));
                }
                else {
                    return "--";
                }
            })
            ->editColumn(
                'contractor_id',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">  '.ucwords(get_users_contractor_name($row->contractor_id,$row->company_id)).'</a></div></div>';
                }
            )
            ->editColumn(
                'distribution',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->contractor_id) . '">'.ucwords(get_users_employee_name($row->distribution,$row->company_id)).'</a></div></div>';
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_user_name($row->user_id)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'project_name','action','contractor_id','distribution','added_by'])
            ->make(true);
    }

    public function deleteAwardContractor($id)
    {
        AwardedContracts::destroy($id);
        AwardContractFiles::where('awarded_contracts_id',$id)->delete();
        AwardContractProducts::where('awarded_contract_id',$id)->delete();
        AwardContractCategory::where('awarded_contract_id',$id)->delete();
        return Reply::success(__('Contract deleted successfully'));
    }
    public function awardCreate()
    {
        $awardedcontract = request()->session()->get('awardedcontract');
        $awardedcontract =  !empty($awardedcontract) ? AwardedContracts::find($awardedcontract) : '';
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $this->pageTitle = 'app.menu.awardnewcontracts';
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
         $this->titlelist = !empty($awardedcontract) ? Title::where('project_id',$awardedcontract->project_id)->get()  : array();
        $this->segmentlist = !empty($awardedcontract) ?  Segment::where('projectid',$awardedcontract->project_id)->where('titleid',$awardedcontract->subproject_id)->get()  : array();
        $this->awardedcontract =$awardedcontract;
        return view('admin.tenders.awardcreate', $this->data);
    }

    public function contractproductsloop(Request $request){
        $contractid = $request->contractid ?: 0;
        $projectid = $request->projectid ?: 0;
        $subprojectid = $request->subprojectid ?: 0;
        $segmentid = $request->segmentid ?: 0;
        $awardedcontract  = AwardedContracts::find($contractid);
        $user = $this->user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->contractorarray = Employee::getAllContractors($user);
        $this->userarray = Employee::getAllEmployees($user);
        $this->awardedcontract = $awardedcontract;
        $this->projectid = $projectid;
        $this->subprojectid = $subprojectid;
        $this->segmentid = $segmentid;
         $messageview = View::make('admin.tenders.contractproductsloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function awardedStore(Request $request)
    {
        $user = $this->user;
        $awardedid = $request->awardedID;
        if(!empty($awardedid)){
            $awaredcontracts =   AwardedContracts::find($awardedid);
        }else{
            $awaredcontracts =  new AwardedContracts();
        }
        $awaredcontracts->user_id = $user->id;
        $awaredcontracts->company_id = $user->company_id;
        $awaredcontracts->contractor_id = $request->contractor;
        $awaredcontracts->tendertype = 'direct';
        $awaredcontracts->title = $request->title;
        $awaredcontracts->number = $request->number;
        $awaredcontracts->status = $request->status;
        $awaredcontracts->project_id = 0;
        $awaredcontracts->subproject_id = 0;
        $awaredcontracts->segment_id = 0;
        $awaredcontracts->distribution = !empty($request->distribution) ? implode(',',$request->distribution) : '';
        $awaredcontracts->save();
        request()->session()->put('awardedcontract',$awaredcontracts->id);
        return Reply::dataOnly(['awardedID' => $awaredcontracts->id]);
    }
    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = storage();
                $file = new AwardContractFiles();
                $file->company_id = $this->user->company_id;
                $file->added_by = $this->user->id;
                $file->awarded_contracts_id = $request->awarded_contracts_id;
                switch($storage) {
                    case 'local':
                        $destinationPath = 'uploads/awarded-contracts-files/'.$file->awarded_contracts_id;
                        if (!file_exists(''.$destinationPath)) {
                            mkdir(''.$destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                         break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('awarded-contracts-files/'.$request->awarded_contracts_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'tender-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('tender-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->tender_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->tender_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->tender_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('tender-files/'.$request->tender_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/tender-files/'.$request->tender_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->tender_id, __('messages.newFileUploadedToTheProject'));
            }

        }
     /*   return Reply::redirect(route('admin.tenders.index'), __('modules.projects.projectUpdated'));*/
    }

    public function projectTitles(Request $request){

        $awardedcontract = $request->awardedcontract;
        $projectid = $request->projectid;
        $titleid = $request->titleid;
        $segmentid = $request->segmentid;
        $awarded = AwardedContracts::where('id',$awardedcontract)->first();
        $awarded->project_id = $projectid;
        $awarded->subproject_id = $titleid ?: 0;
        $awarded->segment_id = $segmentid ?: 0;
        $awarded->save();
        $titleoption = '<option value="">Select Sub Project</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
        }
        return $titleoption;
    }
    public function segmentList(Request $request){

        $awardedcontract = $request->awardedcontract;
        $tender = $request->tender;
        $projectid = $request->projectid;
        $titleid = $request->titleid;
        $segmentid = $request->segmentid;
        $awarded = AwardedContracts::where('id',$awardedcontract)->first();
        $awarded->project_id = $projectid;
        $awarded->subproject_id = $titleid ?: 0;
        $awarded->segment_id = $segmentid ?: 0;
        $awarded->save();
        $titleoption = '';
        Tenders::where('id',$tender)->update(['project_id'=>$projectid]);
        if($projectid&&$titleid){
            $titlearray = Segment::where('projectid',$projectid)->where('titleid',$titleid)->get();
            if(count($titlearray)>0){
                $titleoption = '<option value="">Select Segment</option>';
                foreach ($titlearray as $item) {
                    $titleoption .= '<option value="'.$item->id.'">'.$item->name.'</option>';
                }
            }
        }

        return $titleoption;
    }

    public function boqCostItems(Request $request){
        $projectid = $request->projectid;
        $titleid = $request->titleid ?: 0;
        $awardedcontract = $request->awardedcontract ?: 0;
        $segmentid = $request->segmentid;

        $projectdetails = Project::find($projectid);
        $segmentoption = '';
        $taskoption = '<option value="">Select Task</option>';
        $catoption = '<option value="">Select Activity</option>';
        $subprojectoption = '<option value="">Select Sub Project</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $subprojectoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
            if($projectdetails->segment=='enable'){
            $segmentsarray = Segment::where('projectid',$projectid)->where('titleid',$titleid)->get();
                $segmentoption = '<option value="">Select Segment</option>';
                if(count($segmentsarray)>0){
                    foreach ($segmentsarray as $item) {
                        $segmentoption .= '<option value="'.$item->id.'">'.$item->name.'</option>';
                    }
                }
            }

            if(!empty($segmentid)){

                $titlearray = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->pluck('cost_items_id','id');
                foreach ($titlearray as $item =>$value) {
                    $costitemname = get_cost_name($value);
                    $taskoption .= '<option value="'.$item.'" data-name="'.$costitemname.'" >'.$costitemname.'</option>';
                }
                $categoryarray = ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->where('position','row')->get();
                foreach ($categoryarray as $colums) {
                    $catoption .= '<option value="'.$colums->id.'" data-name="'.$colums->itemname.'" >'.$colums->itemname.'</option>';
                }


            }else{
                $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->pluck('cost_items_id','id');
                foreach ($titlearray as $item =>$value) {
                    $costitemname = get_cost_name($value);
                    $taskoption .= '<option value="'.$item.'" data-name="'.$costitemname.'" >'.$costitemname.'</option>';
                }
                $categoryarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$titleid)->where('position','row')->get();
                    foreach ($categoryarray as $colums) {
                        $catoption .= '<option value="'.$colums->id.'" data-name="'.$colums->itemname.'" >'.$colums->itemname.'</option>';
                    }

            }
        }
        return array('titles'=>$subprojectoption,'segments'=>$segmentoption,'costitem'=>$taskoption,'costcat'=>$catoption);
    }
    public function boqItemCatRow(Request $request){
        $awardedcontract = $request->awardedcontract;
        $projectid = $request->projectid;
        $titleid = $request->titleid ?: 0;
        $segmentid = $request->segmentid ?: 0;
        $awarded = AwardedContracts::where('id',$awardedcontract)->first();
        if(empty($awarded->id)){
            return 'failed';
        }
        $awarded->project_id = $projectid;
        $awarded->subproject_id = $titleid;
        $awarded->segment_id = $segmentid;
        $awarded->save();
        $parent = $request->parent;
        $itemid = $request->itemid;
        $catid = $request->catid;
        $level = $request->level;
        $maxinc = AwardContractCategory::where('awarded_contract_id',$awardedcontract)->where('level',$level)->max('inc');
        $newid = (int)$maxinc+1;
        $tendercategory = new AwardContractCategory();
        $tendercategory->awarded_contract_id = $awarded->id;
        $tendercategory->product_category = $catid;
        $tendercategory->category = $itemid;
        $tendercategory->level = $level;
        $tendercategory->parent = $parent;
        $tendercategory->inc = $newid;
        $tendercategory->save();
    }
    public function boqItemRow(Request $request){

        $user = $this->user;
        $projectid = $request->projectid;
        $titleid = $request->titleid ?: 0;
        $segmentid = $request->segmentid ?: '';
        $showrate = $request->showrate;
        $rowid = $request->rowid;
        $catid = $request->catid;
        $awardedcontract = $request->awardedcontract;
        $unitsarray = Units::all();
        if(!empty($segmentid)){
            $costitemrow = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->where('id',$rowid)->first();
            $titlearray = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$titleid)->where('segment',$segmentid)->pluck('cost_items_id','id');
        }else{
        $costitemrow = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->where('id',$rowid)->first();
        $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$titleid)->pluck('cost_items_id','id');
        }
        $maxinc = AwardContractProducts::where('awarded_contract_id',$awardedcontract)->max('inc');
        $newid = (int)$maxinc+1;
        $tendercategory = new AwardContractProducts();
        $tendercategory->company_id = $user->company_id;
        $tendercategory->added_by = $user->id;
        $tendercategory->awarded_contract_id = $awardedcontract;
        $tendercategory->awarded_category = $catid;
        $tendercategory->category = $costitemrow->position_id;
        $tendercategory->products = $costitemrow->id;
        $tendercategory->cost_items = $costitemrow->cost_items_id;
        $tendercategory->qty = $costitemrow->qty ?: 0;
        $tendercategory->unit = $costitemrow->unit ?: 0;
        $tendercategory->rate = $costitemrow->rate ?: 0;
        $tendercategory->inc = $newid;
        $tendercategory->save();
    }
    public function edit($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $this->tenders = Tenders::find($id);
        $this->files = TendersFiles::where('tender_id',$id)->get();
        $this->employees = User::all();
        return view('admin.tenders.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $memberExistsInTemplate = false;
        $project = Tenders::find($id);
        $project->name = $request->title;
        $project->number = $request->number;
        $project->distribution = !empty($request->distribution) ? implode(',',$request->distribution) : '';
        $project->accept_submition = !empty($request->accept_submition) ? '1' : '0';
        $project->enable_blind = !empty($request->enable_blind) ? '1' : '0';
        $project->include_bid_doc = !empty($request->include_bid_doc) ? '1' : '0';
        $project->send_count_emails = !empty($request->send_count_emails) ? '1' : '0';
        $project->enable_prebid_rfi = !empty($request->enable_prebid_rfi) ? '1' : '0';
        $project->enable_prebid_walkthrough = !empty($request->enable_prebid_walkthrough) ? '1' : '0';
        $project->count_email = !empty($request->count_email) ? $request->count_email : '0';
        if ($request->prebid_rfi_date != '') {
            $project->prebid_rfi_date = date('Y-m-d', strtotime($request->prebid_rfi_date));
        }
        if ($request->due_date != '') {
            $project->deadline = date('Y-m-d', strtotime($request->due_date));
        }
        if ($request->prebid_walkthrough_date != '') {
            $project->prebid_walkthrough_date = date('Y-m-d', strtotime($request->prebid_walkthrough_date));
        }
        if ($request->anticipated_date != '') {
            $project->anticipated_date = date('Y-m-d', strtotime($request->anticipated_date));
        }
        if ($request->bidding_information != '') {
            $project->bidding_information = $request->bidding_information;
        }
        if ($request->project_information != '') {
            $project->project_information = $request->project_information;
        }
        if ($request->walkthough_information != '') {
            $project->walkthough_information = $request->walkthough_information;
        }
        $project->status = $request->status;
        $project->added_by =  $this->user->id;
        $project->save();

        return Reply::dataOnly(['tenderID' => $project->id]);
    }
    public function editCostItem($id)
    {
        if(empty($id)){
            return redirect(route('admin.tenders.index'));
        }
        $tenders = Tenders::find($id);
        $this->tenders = $tenders;
        $this->employees = User::all();
        $this->costitemslist = CostItems::all();
        $this->unitsarray = Units::all();
        $this->projectlist = Project::all();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$tenders->title_id)->get();
        $categoryarray = array();
        if(!empty($tenders->project_id)&&!empty($tenders->title_id)){
            $categoryarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
                ->where('project_id',$tenders->project_id)
                ->where('title',$tenders->title_id)
                ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
            $this->categoryarray =  $boqlevel1categories;
        }
        $costitemsarray = ProjectCostItemsProduct::where('project_id',$tenders->project_id)->where('title',$tenders->title_id)->pluck('cost_items_id','id');
        $this->costitemsarray =  $costitemsarray;
        return view('admin.tenders.editcostitem', $this->data);
    }
    public function destroy($id)
    {
        $projectde = Tenders::where('id',$id)->first();
        if(!empty($projectde->id)){
            $projectde->delete();
            return Reply::success(__('messages.tenderDeleted'));
        }
    }
    public function boqItemRemoveRow($id)
    {
        AwardContractProducts::where('id',$id)->delete();
        return Reply::success(__('messages.tenderDeleted'));
    }
    public function boqItemCatRemoveRow($id)
    {
        AwardContractCategory::where('id',$id)->delete();

        AwardContractProducts::where('category',$id)->delete();

        return Reply::success(__('messages.tenderDeleted'));
    }
    public function removeFile($id){
        $inspectionFiles = TendersFiles::findOrFail($id);
        $storage = config('filesystems.default');
        switch ($storage) {
            case 'local':
                File::delete($inspectionFiles->hashname, 'tender-files/' . $inspectionFiles->tender_id.'/'.$inspectionFiles->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete('tender-files/' . $inspectionFiles->tender_id.'/'.$inspectionFiles->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('tender-files/' . $inspectionFiles->tender_id.'/'.$inspectionFiles->hashname);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('tender-files/' . $inspectionFiles->tender_id.'/'.$inspectionFiles->hashname);
                break;
        }
        $inspectionFiles->delete();
        return Reply::success(__('Image deleted successfully'));
    }
    public function contractorsList(Request $request,$id){
        $tendercostitem =  TendersProduct::where('tender_id',$id)->first();
        if(!empty($tendercostitem->id)){
            $users = Contractors::where('status', 'active')->get();
            return DataTables::of($users)
                ->addColumn('checkbox', function ($row) {
                    return '<input type="checkbox" class="contractors" name="contractors[]" value="'.$row->id.'" />';
                 })
                ->addIndexColumn()
                ->rawColumns(['checkbox'])
                ->make(true);
        }else{
            return Reply::error('Task not available');
        }
    }
    public function sendContractorsMail(Request $request){
        $user = $this->user;
        $tender = $request->tender;
        $contractors = $request->contractors;
        if($tender&&$contractors){
            $contractors = explode(',',$contractors);
            $contractorsarray = Contractors::whereIn('id',$contractors)->get();
            $sendmail['success'] = false;
            foreach ($contractorsarray as $contractor){
                $mailarray = array();
                $mailarray['email'] = $contractor->email;
                $mailarray['subject'] = 'Aakar360 Contractor email';
                $mailarray['message'] = 'Contractor email testing';
                $sendmail = $user->sendEmail($mailarray);
            }
              return $sendmail;
        }else{
            return Reply::error("Tendor Information not found");
        }
    }

    public function awardedtenderDetails($id){
        $user = $this->user;
        $awardedcontract = AwardedContracts::where('id',$id)->first();
        if(empty($awardedcontract->id)){
            return redirect(route('admin.tenders.awardedContracts'));
        }
        $this->pageTitle = 'app.menu.awardedcontracts';
        $this->contractorsarray = Employee::getAllContractors($user);
        $this->tenders = Tenders::find($awardedcontract->tender_id);
        $this->files = array();
        $this->tenderawarded =$awardedcontract;
        return view('admin.tenders.awardedtenderdetails', $this->data);
    }
    public function awardedtenderBidding($id){
        $user = $this->user;
        $awardedcontract = AwardedContracts::where('id',$id)->first();
        if(empty($awardedcontract->id)){
            return redirect(route('admin.tenders.awardedContracts'));
        }
        if($awardedcontract->tendertype!='direct'){
            return redirect(route('admin.tenders.awardedContracts'));
        }
        $this->contractorsarray = User::getAllContractors($user);
        $tenders = Tenders::find($awardedcontract->tender_id);
        $supliers =  TenderBidding::where('tender_id',$tenders->id)->where('project_id',$tenders->project_id)->pluck('user_id')->toArray();
        $this->tenders = $tenders;
        $this->tenderawarded =$awardedcontract;
        $this->supplierlist = User::whereIn('id',$supliers)->get();
        $this->employees = User::all();
        $this->costitemslist = CostItems::all();
        $this->unitsarray = Units::all();
        $this->projectlist = Project::all();
        $this->titlelist = Title::where('project_id',$tenders->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$tenders->project_id)->where('titleid',$tenders->title_id)->get();
        $this->files = TendersFiles::where('tender_id',$awardedcontract->tender_id)->get();
        $categoryarray = array();
        if(!empty($tenders->project_id)&&!empty($tenders->title_id)){
            $categoryarray = DB::table('project_cost_items_product')
                ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
                ->where('project_id',$tenders->project_id)
                ->where('title',$tenders->title_id)
                ->first();
            $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
            $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
            $this->categoryarray =  $boqlevel1categories;
        }
        $costitemsarray = ProjectCostItemsProduct::where('project_id',$tenders->project_id)->where('title',$tenders->title_id)->pluck('cost_items_id','id');
        $this->costitemsarray =  $costitemsarray;
        return view('admin.tenders.awardedtenderbidding', $this->data);
    }

    public function awardedBiddingProducts($id){
        $user = $this->user;
        $projectlist = explode(',',$user->projectlist);
        $awardedcontract = AwardedContracts::where('id',$id)->first();
        if(empty($awardedcontract->id)){
            return redirect(route('admin.awardedcontracts.awardedContracts'));
        }
        $this->pageTitle = 'app.menu.awardedcontracts';
        $this->awardedcontract = $awardedcontract;
        $this->contractorsarray = Employee::getAllContractors($user);
        $tenders = Tenders::find($awardedcontract->tender_id);
        $supliers =  TenderBidding::where('project_id',$awardedcontract->project_id)->pluck('user_id')->toArray();
        $this->tenders = $tenders;
        $this->supplierlist = User::whereIn('id',$supliers)->get();
        $this->employees = Employee::getAllEmployees($user);
        $this->costitemslist = CostItems::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->titlelist = Title::where('project_id',$awardedcontract->project_id)->get();
        $this->segmentlist = Segment::where('projectid',$awardedcontract->project_id)->where('titleid',$awardedcontract->subproject_id)->get();
        $this->files = AwardContractFiles::where('awarded_contracts_id',$awardedcontract->id)->get();

        return view('admin.tenders.awardedbiddingproducts', $this->data);
    }
    public function awardedContractsclear()
    {
        request()->session()->forget('awardedcontract');
        return redirect(route('admin.awardedcontracts.awardedContracts'));
    }

    public  function updateCostItem(Request $request){
         $user = $this->user;
        $itemid = $request->itemid;
        $unit = $request->unit;
        $qty = $request->qty ?: 0;
        $rate = $request->rate ?: 0;
        $finalrate = $rate*$qty;
         $awardedproductus = AwardContractProducts::find($itemid);
         if(!is_numeric($unit)&&!empty($unit)){
           $units = Units::where('company_id',$user->company_id)->where('name',$unit)->first();
              if(empty($units)){
                  $units = new Units();
                  $units->name = $unit;
                  $units->company_id = $user->company_id;
                  $units->symbol = '';
                  $units->save();
              }
             $unit = $units->id;
         }
        $awardedproductus->unit = $unit ?: 0;
        $awardedproductus->rate = $rate;
        $awardedproductus->qty = $qty;
        $awardedproductus->finalrate = $finalrate;
        $awardedproductus->save();

    }
}
