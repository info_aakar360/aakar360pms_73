<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use App\Transaction;
use Illuminate\Support\Facades\DB;


class ContraVoucherController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Journal Voucher';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = Transaction::class;
    public $parentRoute = 'admin.contra_voucher';
    public $parentView = "admin.cnt-voucher";
    public $voucher_type = "Contra";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->items = Transaction::where('voucher_type', $this->voucher_type)
            ->orderBy('voucher_no', 'desc')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'branch_id' => 'required',
            'bank_cash_id' => 'required',
            'bank_cash_id_cr' => 'required',
            'amount' => 'required',
            'voucher_date' => 'required',
        ]);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $voucher_info = Transaction::withTrashed()
            ->orderBy('voucher_no', 'desc')
            ->get()
            ->first();
        if (!empty($voucher_info)) {
            $voucher_no = $voucher_info->voucher_no + 1;
        } else {
            $voucher_no = 1;
        }
        $check_number_dr = null;
        $check_number_cr = null;
        if ($request->bank_cash_id > 1) {
            $check_number_dr = $request->cheque_number;
        } else {
            $check_number_cr = $request->cheque_number;
        }
        Transaction::create([
            'voucher_no' => $voucher_no,
            'branch_id' => $request->branch_id,
            'bank_cash_id' => $request->bank_cash_id,
            'cheque_number' => $check_number_dr,
            'voucher_type' => $this->voucher_type,
            'date' => $voucher_date,
            'particulars' => $request->particulars,
            'dr' => $request->amount,
            'created_by' => \Auth::user()->id,
        ]);
        Transaction::create([
            'voucher_no' => $voucher_no,
            'branch_id' => $request->branch_id,
            'bank_cash_id' => $request->bank_cash_id_cr,
            'cheque_number' => $check_number_cr,
            'voucher_type' => $this->voucher_type,
            'date' => $voucher_date,
            'particulars' => $request->particulars,
            'cr' => $request->amount,
            'created_by' => \Auth::user()->id,
        ]);
        return Reply::success(__('Successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        $this->item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($this->item)=='on') {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (empty($this->items)) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'branch_id' => 'required',
            'bank_cash_id' => 'required',
            'bank_cash_id_cr' => 'required',
            'amount' => 'required',
            'voucher_date' => 'required',
        ]);
        if ($request->bank_cash_id == $request->bank_cash_id_cr && $request->bank_cash_id != 0 && $request->bank_cash_id_cr != 0) {
            return Reply::error(__('Bank Cash ( Dr ) and Bank Cash ( Cr ) should not same'));
        }
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        $dr_id = $items[0]->id;
        $cr_id = $items[1]->id;
        $dr_items = Transaction::find($dr_id);
        $dr_items->voucher_no = $id;
        $dr_items->branch_id = $request->branch_id;
        $dr_items->bank_cash_id = $request->bank_cash_id;
        $dr_items->dr = $request->amount;
        $dr_items->voucher_date = $voucher_date;
        $dr_items->particulars = $request->particulars;
        $dr_items->updated_by = \Auth::user()->id;
        $dr_items->save();

        $cr_items = Transaction::find($cr_id);
        $cr_items->voucher_no = $id;
        $cr_items->branch_id = $request->branch_id;
        $cr_items->bank_cash_id = $request->bank_cash_id_cr;
        $cr_items->cr = $request->amount;
        $cr_items->voucher_date = $voucher_date;
        $cr_items->particulars = $request->particulars;
        $cr_items->updated_by = \Auth::user()->id;
        $cr_items->save();
        return Reply::success(__('Successfully Updated'));
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        $item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($item) == 0) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        $now = new \DateTime();
        $date = $now->format('Y-m-d' . ' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Contra Voucher Report',
            'voucher_type' => 'CONTRA VOUCHER'
        );
        // return view('admin.dr-voucher.pdf');
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
         return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($items) < 1) {
            return Reply::error(__('Item not found'));
        }
        foreach ($items as $item) {
            $item->deleted_by = \Auth::user()->id;
            $item->save();
        }
        foreach ($items as $item) {
            $item->delete();
        }
        return Reply::success(__('Successfully Trashed'));
    }

    public function trashed()
    {
        $this->items = Transaction::where('voucher_type', $this->voucher_type)
            ->onlyTrashed()
            ->orderBy('deleted_at', 'desc')
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

    public function restore($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->onlyTrashed()
            ->get();
        foreach ($items as $item) {
            $item->restore();
            $item->updated_by = \Auth::user()->id;
            $item->save();
        }
        return Reply::success(__('Successfully Restored'));
    }

    public function kill($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->withTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->withTrashed()
            ->get();
        foreach ($items as $item) {
            $item->forceDelete();
        }
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('cheque_number', '=', $search)
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('cheque_number', '=', $search)
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->onlyTrashed()
            ->orderBy('created_at', 'desc')
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->withTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->withTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            return Reply::error(__('Something is wrong. Try again'));
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            return Reply::error(__('Something is wrong. Try again'));
        }
        return redirect()->back();
    }
}
