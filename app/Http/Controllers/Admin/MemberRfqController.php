<?php

namespace App\Http\Controllers\Admin;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Rfq\StoreRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\Project;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Quotes;
use App\Role;
use App\Rfq;
use App\RfqProducts;
use App\Store;
use App\Supplier;
use App\TmpRfq;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberRfqController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.rfq';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';
        $this->middleware(function ($request, $next) {
            if (!in_array('rfq', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->stores = Store::all();
        $this->categories = ProductCategory::all();
        $this->projects = Project::all();
        return view('admin.rfq.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        return view('admin.rfq.create', $this->data);
    }

    public function getBrands(Request $request){
        $pid = $request->pid;
        $bids = ProductCategory::where('id', $pid)->first()->brands;
        $bid_arr = explode(',', $bids);
        $brands = ProductBrand::whereIn('id', $bid_arr)->get();
        $html = '<option value="">Select Brand</option>';
        foreach($brands as $brand){
            $html .= '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        }
        return $html;
    }
    public function storeTmp(Request $request){
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('rfqTmpDataSession', $sid);
        }
        $tmpdata = new TmpRfq();
        $tmpdata->session_id = $sid;
        $tmpdata->cid = $request->cid;
        $tmpdata->bid = $request->bid;
        $tmpdata->qty = $request->qty;
        $tmpdata->unit = $request->unit;
        $tmpdata->dated = $request->dated;
        $tmpdata->remark = $request->remark;
        $tmpdata->save();

        $allData = TmpRfq::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function deleteTmp(Request $request){
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        TmpRfq::where('id', $request->did)->delete();
        $allData = TmpRfq::where('session_id', $sid)->get();

        $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Due Date</th><th>Remark</th><th>Action</th></thead><tbody>';
        if(count($allData)){
            $i = 1;
            foreach($allData as $data){
                $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td><td><a href="javascript:void(0);" class="btn btn-danger deleteRecord" style="color: #ffffff;" data-key="'.$data->id.'">Delete</a></td></tr>';
                $i++;
            }
        }else{
            $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRfqRequest $request)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $sr = 1;
        $ind = Rfq::orderBy('id', 'DESC')->first();
        if($ind !== null){
            $in = explode('/', $ind->rfq_no);
            $sr = $in[2];
            $sr++;
        }
        $rfq_no = 'RFQ/'.date("Y").'/'.$sr;
        $rfq = new Rfq();
        $rfq->rfq_no = $rfq_no;
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->company_id = $this->user->company_id;
        $rfq->save();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        if($request->session()->has('rfqTmpDataSession')) {
            $sid = $request->session()->get('rfqTmpDataSession');
            TmpRfq::where('session_id', $sid)->delete();
            $request->session()->forget('rfqTmpDataSession');
        }
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }else{
            $sid = uniqid();
            $request->session()->put('rfqTmpDataSession', $sid);
        }
        $all_data = RfqProducts::where('rfq_id', $id)->get();
        foreach ($all_data as $data){
            $tmp = new TmpRfq();
            $tmp->session_id = $sid;
            $tmp->cid = $data->cid;
            $tmp->bid = $data->bid;
            $tmp->qty = $data->quantity;
            $tmp->unit = $data->unit;
            $tmp->dated = $data->expected_date;
            $tmp->remark = $data->remarks;
            $tmp->save();
        }
        $this->tmpData = TmpRfq::where('session_id', $sid)->get();
        $this->rfq = Rfq::where('id', $id)->first();
        return view('admin.rfq.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRfqRequest $request, $id)
    {
        $sid = null;
        if($request->session()->has('rfqTmpDataSession')){
            $sid = $request->session()->get('rfqTmpDataSession');
        }
        $rfq = Rfq::find($id);
        $rfq->store_id = $request->store_id;
        $rfq->remark = $request->remark;
        $rfq->payment_terms = $request->payment_terms;
        $rfq->save();
        RfqProducts::where('rfq_id', $id)->delete();
        $all_data = TmpRfq::where('session_id', $sid)->get();
        foreach ($all_data as $data){
            $indPro = new RfqProducts();
            $indPro->rfq_id = $rfq->id;
            $indPro->cid = $data->cid;
            $indPro->bid = $data->bid;
            $indPro->quantity = $data->qty;
            $indPro->unit = $data->unit;
            $indPro->remarks = $data->remark;
            $indPro->expected_date = $data->dated;
            $indPro->save();
        }
        TmpRfq::where('session_id', $sid)->delete();
        return Reply::redirect(route('admin.rfq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projectde  = Quotes::where('rfq_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Order first.';
        }
        $projectde  = PurchaseOrder::where('po_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Order first.';
        }
        $projectde  = PurchaseInvoice::where('po_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove GRN first.';
        }
        $projectde  = ProductReturns::where('invoice_id', $id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Product Return first.';
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            Rfq::destroy($id);
            RfqProducts::where('rfq_id', $id)->delete();
            return Reply::success(__('messages.rfqDeleted'));
        }
    }

    public function data(Request $request)
    {
        $rfqs = Rfq::join('stores', 'stores.id', '=', 'rfqs.store_id');
        if ($request->store != 'all' && $request->store != '') {
            $rfqs = $rfqs->where('store_id', $request->store);
        }

        if ($request->status != 'all' && $request->status != '') {
            $rfqs = $rfqs->where('status', $request->status);
        }
        if ($request->project_id != 'all' && $request->project_id != '') {
            $rfqs = $rfqs->where('purchase_order.project_id', $request->project_id);
        }
        $rfqs = $rfqs->select(['rfqs.*'])->get();
        $user = Auth::user();
        return DataTables::of($rfqs)
            ->addColumn('action', function ($row) use ($user) {
                $ret = '';
                if($user->can('edit_rfq')){
                    $ret .= '<a href="' . route('member.rfq.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                }
                 $ret .= '<a  class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;';

                if($row->status == 0) {
                    $ret .= '<a href="' . route('member.rfq.rfqLink', [$row->id]) . '" class="btn btn-warning btn-circle"
                      data-toggle="tooltip" data-original-title="RFQ Link"><i class="fa fa-link" aria-hidden="true"></i></a>&nbsp;';
                }

                return $ret;
            })
            ->editColumn(
                'store_id',
                function ($row) {
                    return get_store_name($row->store_id);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 0){
                        return 'Pending PO';
                    }else if($row->status == 1){
                        return 'Pending Purchase';
                    }else{
                        return 'Purchase Done';
                    }
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }



    public function export($status, $rfq)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'rfq')
            ->where('roles.company_id', company()->id)
            ->leftJoin('rfq_details', 'users.id', '=', 'rfq_details.user_id')
            ->select(
                'users.id',
                'rfq_details.name',
                'rfq_details.email',
                'rfq_details.mobile',
                'rfq_details.company_name',
                'rfq_details.address',
                'rfq_details.website',
                'rfq_details.created_at'
            )
            ->where('rfq_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($rfq != 'all' && $rfq != '') {
            $rows = $rows->where('users.id', $rfq);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('rfqs', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('rfqs file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function rfqLink($id){
        $this->rfq = Rfq::find($id);

        return view('admin.rfq.links', $this->data);
    }

    public function linkData(Request $request, $rfqId)
    {
        $suppliers = Supplier::all();
        $user = Auth::user();
        return DataTables::of($suppliers)
            ->addColumn('action', function ($row) use ($user, $rfqId){
                $ret = '<a href="javascript:;" onClick="copyLink(\'id'.$row->id.'\')" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>&nbsp;';
                $ret .= '<a href="https://api.whatsapp.com/send?text='.urlencode(route('front.submitQuotation', [$rfqId, $row->id])).'" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>&nbsp;';

                return $ret;
            })
            ->addColumn('link', function($row) use ($rfqId){
                return '<span id="id'.$row->id.'">'.route('front.submitQuotation', [$rfqId, $row->id]).'</span>';
            })
            ->addColumn('status', function($row) use ($rfqId){
                $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                if($quotes === null){
                    return 'Not Submitted';
                }
                return 'Submitted';
            })
            ->editColumn(
                'updated_at',
                function ($row) use ($rfqId){
                    $quotes = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $row->id)->first();
                    if($quotes === null){
                        return 'Never';
                    }
                    return Carbon::parse($quotes->updated_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['company_name', 'action', 'link'])
            ->make(true);
    }
}
