<?php

namespace App\Http\Controllers\Admin;

use App\AssignWorkRules;
use App\Designation;
use App\Employee;
use App\Helper\Reply;
use App\Http\Requests\Leaves\StoreLeave;
use App\Http\Requests\Leaves\UpdateLeave;
use App\Http\Requests\LeaveRules\LeaveRulesRequest;
use App\Http\Requests\LeaveRules\UpdateLeaveRulesRequest;
use App\Http\Requests\LeaveRules\StoreWorkdayRequest;
use App\Leave;
use App\LeaveAssigned;
use App\LeaveRulesSettings;
use App\LeavesRules;
use App\LeaveType;
use App\Notifications\LeaveStatusApprove;
use App\Notifications\LeaveStatusReject;
use App\Notifications\LeaveStatusUpdate;
use App\Skill;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ManageLeavesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.leaves');
        $this->pageIcon = 'icon-logout';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('leaves', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->leaves = Leave::where('status', '<>', 'rejected')->get();
        $this->pendingLeaves = Leave::where('status', 'pending')
            ->orderBy('leave_date', 'asc')
            ->get();
        return view('admin.leaves.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->employees = User::allEmployees();
        $this->empdata = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->leaveTypes = LeaveType::all();
        return view('admin.leaves.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeave $request)
    {
        $user = $this->user;

        if ($request->duration == 'multiple') {
            $dates = explode(',', $request->multi_date);
            foreach ($dates as $date) {
                $leave = new Leave();

                $leave->company_id = $user->company_id;
                $leave->user_id = $request->user_id;
                $leave->leave_type_id = $request->leave_type_id;
                $leave->duration = $request->duration;
                $leave->leave_date = Carbon::createFromFormat($this->global->date_format, $date)->format('Y-m-d');
                $leave->reason = $request->reason;
                $leave->status = $request->status?$request->status:'0';
                $leave->save();
            }

            return Reply::redirect(route('admin.leaves.index'), __('messages.leaveAssignSuccess'));
        } else {
            $leave = new Leave();
            $leave->company_id = $user->company_id;
                $leave->user_id = $request->user_id;
            $leave->leave_type_id = $request->leave_type_id;
            $leave->duration = $request->duration;
            $leave->leave_date = Carbon::createFromFormat($this->global->date_format, $request->leave_date)->format('Y-m-d');
            $leave->reason = $request->reason;
            $leave->status = $request->status?$request->status:'0';
            $leave->save();
            return Reply::redirect(route('admin.leaves.index'), __('messages.leaveAssignSuccess'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->leave = Leave::findOrFail($id);
        return view('admin.leaves.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->employees = User::allEmployees();
        $this->empdata = Employee::where('company_id',$this->companyid)->get();
        $this->leaveTypes = LeaveType::all();
        $this->leave = Leave::findOrFail($id);
        $view = view('admin.leaves.edit', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLeave $request, $id)
    {
        $leave = Leave::findOrFail($id);
        $oldStatus = $leave->status;
        if(starts_with($request->user_id,'emp-')){
            $emp_id = ltrim($request->user_id,'emp-');
            $leave->emp_id = $emp_id;
        }else{
            $leave->user_id = $request->user_id;
        }
        $leave->leave_type_id = $request->leave_type_id;
        $leave->leave_date = Carbon::createFromFormat($this->global->date_format, $request->leave_date)->format('Y-m-d');
        $leave->reason = $request->reason;
        $leave->status = $request->status;
        $leave->save();

        if ($oldStatus != $request->status) {
            //      Send notification to user
            if(!empty($leave->user_id)){
                $notifyUser = User::withoutGlobalScope('active')->findOrFail($leave->user_id);
                $notifyUser->notify(new LeaveStatusUpdate($leave));
            }else{
                $notifyUser = Employee::findOrFail($leave->emp_id);
                $notifyUser->notify(new LeaveStatusUpdate($leave));
            }

        }

        return Reply::redirect(route('admin.leaves.index'), __('messages.leaveAssignSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Leave::destroy($id);
        return Reply::success('messages.leaveDeleteSuccess');
    }

    public function leaveAction(Request $request)
    {
        $leave = Leave::findOrFail($request->leaveId);
        $leave->status = $request->action;
        if (!empty($request->reason)) {
            $leave->reject_reason = $request->reason;
        }
        $leave->save();

        //      Send notification to user
        $notifyUser = User::withoutGlobalScope('active')->findOrFail($leave->user_id);

        if ($request->action == 'approved') {
            $notifyUser->notify(new LeaveStatusApprove($leave));
        } else {
            $notifyUser->notify(new LeaveStatusReject($leave));
        }

        return Reply::success(__('messages.leaveStatusUpdate'));
    }

    public function rejectModal(Request $request)
    {
        $this->leaveAction = $request->leave_action;
        $this->leaveID = $request->leave_id;
        return view('admin.leaves.reject-reason-modal', $this->data);
    }

    public function allLeave()
    {
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->fromDate = Carbon::today()->subDays(30);
        $this->toDate = Carbon::today();
        $this->pendingLeaves = Leave::where('status', 'pending')->count();
        return view('admin.leaves.all-leaves', $this->data);
    }


    public function data(Request $request, $employeeId = null)
    {
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $startDt = '';
        $endDt = '';

        if (!is_null($startDate)) {
            $startDate = Carbon::createFromFormat($this->global->date_format, $startDate)->format('Y-m-d');
            $startDt = 'DATE(leaves.`leave_date`) >= ' . '"' . $startDate . '"';
        }

        if (!is_null($endDate)) {
            $endDate = Carbon::createFromFormat($this->global->date_format, $endDate)->format('Y-m-d');
            $endDt = 'DATE(leaves.`leave_date`) <= ' . '"' . $endDate . '"';
        }

        $leavesList = Leave::select('leaves.id', 'employee.name', 'leaves.leave_date', 'leaves.status', 'leave_types.type_name', 'leave_types.color')
            ->where('leaves.status', '<>', 'rejected')
            ->whereRaw($startDt)
            ->whereRaw($endDt)
            ->join('employee', 'employee.id', '=', 'leaves.emp_id')
            ->join('leave_types', 'leave_types.id', '=', 'leaves.leave_type_id');

        if ($employeeId != 0) {
            $leavesList->where('leaves.emp_id', $employeeId);
        }

        $leaves = $leavesList->get();

        return DataTables::of($leaves)
            ->addColumn('employee', function ($row) {
                return ucwords($row->name);
            })
            ->addColumn('date', function ($row) {
                return $row->leave_date->format('Y-m-d');
            })
            ->addColumn('status', function ($row) {
                $label = $row->status == 'pending' ? 'warning' : 'success';
                return '<div class="label label-' . $label . '">' . $row->status . '</div>';
            })
            ->addColumn('leave_type', function ($row) {
                return '<div class="label-' . $row->color . ' label">' . $row->type_name . '</div>';
            })
            ->addColumn('action', function ($row) {
                if ($row->status == 'pending') {
                    return '<a href="javascript:;"
                            data-leave-id=' . $row->id . '
                            data-leave-action="approved" 
                            class="btn btn-success  leave-action"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.approved') . '">
                                Approved
                            </a>
                            <a href="javascript:;" 
                            data-leave-id=' . $row->id . '
                            data-leave-action="rejected"
                            class="btn btn-danger  leave-action-reject"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.reject') . '">
                                Reject
                            </a>
                            
                            <a href="javascript:;"
                            data-leave-id=' . $row->id . '
                            class="btn btn-info  show-leave"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.details') . '">
                                detail
                            </a>';
                }

                return '<a href="javascript:;"
                        data-leave-id=' . $row->id . '
                        class="btn btn-info btn-circle show-leave"
                        data-toggle="tooltip"
                        data-original-title="' . __('app.details') . '">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>';
            })
            ->addIndexColumn()
            ->rawColumns(['date', 'status', 'leave_type', 'action'])
            ->make(true);
    }

    public function pendingLeaves()
    {
        $pendingLeaves = Leave::with('type', 'user')->where('status', 'pending')
            ->orderBy('leave_date', 'asc')
            ->get();
        $this->pendingLeaves = $pendingLeaves->each->append('leaves_taken_count');

        $this->allowedLeaves = LeaveType::sum('no_of_leaves');

        return view('admin.leaves.pending', $this->data);
    }

    public function leaveRules(){

        return view('admin.leaves.leaves_rules',$this->data);
    }

    public function addLeaveRules(){
        return view('admin.leaves.create_leave_rules', $this->data)->render();
    }
    public function editLeaveRules($id){
        $this->leaveRules = LeavesRules::find($id);
        return view('admin.leaves.edit_leave_rules', $this->data);
    }
    public function deleteLeaveRule($id){
        $leaves = LeavesRules::find($id);
        $leaves->delete();
        return Reply::success(__('Deleted Successfully'));
    }

    public function leaveDataOfRules(){

        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->where('employee.company_id',$this->companyid)
            ->where('user_type','employee')
            ->select('employee.id','employee.name',
                'employee_details.address','employee_details.department_id as deptid')->groupBy('employee.id')->get();


        $leavedata = Employee::join('leave_assigneds','leave_assigneds.user_id','=','employee.id')
            ->join('leaves_rules','leaves_rules.id','=','leave_assigneds.leave_rule_id')->select('leave_assigneds.user_id','leaves_rules.leave_rule_name','leave_assigneds.effective_date')->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<input type="checkbox" class="sub_chk" data-id="'.$row->id.'" />';
            })->rawColumns(['checkbox'])
            ->addIndexColumn()
            ->editColumn('name', function ($row){
                return $row->name;
            })->editColumn('department', function ($row)  {
                $team = Team::where('id',$row->deptid)->first();
                if(!empty($team)){
                    return $team->team_name;
                }
                return 'NA';
            })->editColumn('manager', function ($row)  {

                return '';
            })->editColumn('type', function ($row)  {
                return $row->type;
            })->editColumn('effective_date', function ($row) use($leavedata){
                foreach ($leavedata as $work){
                    if($row->id == $work->user_id){
                        return $work->effective_date;
                    }

                }
            })
            ->addColumn('rulesapplied', function ($row) use($leavedata){
                foreach ($leavedata as $work){
                    if($row->id == $work->user_id){
                        return $work->leave_rule_name;
                    }

                }
            })
            ->rawColumns(['action','name','department','manager','type','rulesapplied'])

            ->make(true);
    }
    public function assignLeaveRules(Request $request){
        $this->ids = $request->ids;
        $this->rules = LeavesRules::all();
        return view('admin.leaves.assign_leave_rules', $this->data)->render();
    }

    public function postAssignLeaveRules(Request $request){

        $date =  Carbon::parse($request->effective_date)->format('Y-m-d');
        $user_ids = explode(',', $request->ids);

        foreach ($user_ids as $user_id){
            $leave_assign = LeaveAssigned::updateOrCreate(
                ['user_id' => $user_id],
                ['leave_rule_id' => $request->rules_id,'effective_date'=>$date]
            );

        }

        return Reply::success(__('messages.assignLeaveRules'));
        abort(403);
    }

    public function  storeLeaveRules(LeaveRulesRequest $request){
        $leave_rules = new LeavesRules();
        $leave_rules->leave_rule_name = $request->name;
        $leave_rules->leave_rules_alias = str_replace(' ', '', $request->name);
        $leave_rules->leave_rules_descriptionn =  $request->description;
        $leave_rules->leave_allowed_in_year = $request->leave_allowed_in_year?$request->leave_allowed_in_year:'';
        $leave_rules->leave_allowed_in_month = $request->leave_allowed_in_month?$request->leave_allowed_in_month:'';
        $leave_rules->continue_leave_allow = $request->continue_leave_allowed?$request->continue_leave_allowed:'';
        $leave_rules->max_leave_to_carry_forword = $request->max_carry_forword?$request->max_carry_forword:'';
        $leave_rules->all_remaining_leaves = $request->all_remaining_leave?$request->all_remaining_leave:'';
        $leave_rules->weekend_bet_leave = $request->weekends_between_leave?$request->weekends_between_leave:'';
        $leave_rules->hollyday_bet_leaves = $request->hollyday_between_leave?$request->hollyday_between_leave:'';
        $leave_rules->allow_probation = $request->allowed_under_probation;
        $leave_rules->leave_carrey_forward =  $request->enbale_carry_forword?$request->enbale_carry_forword:'';
        $leave_rules->leave_backdated_allow = $request->backend_leave_allow?$request->backend_leave_allow:'';
        $leave_rules->backdated_allow_upto = $request->allow_leave_days;
        $leave_rules->save();
        return Reply::success(__('messages.addLeaveRules'));
        abort(403);
    }
    public function  updateLeaveRules(UpdateLeaveRulesRequest $request){
        $leave_rules = LeavesRules::find($request->id);
        $leave_rules->leave_rule_name = $request->name;
        $leave_rules->leave_rules_alias = str_replace(' ', '', $request->name);
        $leave_rules->leave_rules_descriptionn =  $request->description;
        $leave_rules->weekend_bet_leave = $request->weekends_between_leave;
        $leave_rules->hollyday_bet_leaves = $request->hollyday_between_leave;
        $leave_rules->allow_probation = $request->allowed_under_probation;
        $leave_rules->leave_carrey_forward =  $request->enbale_carry_forword;
        $leave_rules->leave_backdated_allow = $request->enbale_carry_forword;
        $leave_rules->backdated_allow_upto = $request->allow_leave_days;
        $leave_rules->save();
        return Reply::success(__('messages.addLeaveRules'));
        abort(403);
    }
    public function leavesRulesData(){
        $roules = LeavesRules::all();

        return DataTables::of($roules)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->leave_rule_name;
            })

            ->addColumn('action', function ($row) {
                if($row->leave_rule_name !== "Loss Of Pay"){
                    return '<a  href="' . route('admin.leave.editLeaveRules',[$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }else{
                    return '<a  href="' . route('admin.leave.editLeaveRules',[$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }

            })
            ->rawColumns(['name', 'action'])

            ->make(true);
    }

    public function createLeaveRules(){

        /*$leave_rules = LeavesRules::where('leave_rules_alias',$alias)->get();
        $this->leave_rules = $leave_rules[0];*/
        return view('admin.leaves.create_leave_rules',$this->data);
    }

    public  function  leaveRulesSettings($id){
        $this->rulesSettings = LeaveRulesSettings::find($id);
        return view('admin.leaves.leave_settings',$this->data);
    }

    public  function storeLeaveRulesSettings(Request $request){

        $workday = array_combine($request->filed_id , $request->filed_class);
        $store_workday = LeaveRulesSettings::find($request->id);
        $store_workday->name = $request->name;
        $store_workday->company_id = $this->user->company_id;
        $store_workday->description = $request->description;
        $store_workday->field_id_class = json_encode($workday);
        $store_workday->save();
        return Reply::redirect(route('admin.leave.leaveRulesSettingsList'),__('messages.addLeaveRulesSettings'));

    }

    public  function leaveRulesSettingsList(){
        return view('admin.leaves.leave_rule_setting_list',$this->data);
    }
    public  function createWorkDay(){
        return view('admin.leaves.create_work_day',$this->data);
    }
    public function storeWorkday(StoreWorkdayRequest $request){
        $workday = array_combine($request->filed_id , $request->filed_class);
        $store_workday = new LeaveRulesSettings();
        $store_workday->name = $request->name;
        $store_workday->company_id = $this->user->company_id;
        $store_workday->description = $request->description;
        $store_workday->field_id_class = json_encode($workday);
        $store_workday->save();
        return Reply::success(__('messages.addLeaveRulesSettings'));
        abort(403);
    }
    public function workWeakData(){

        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->where('employee.company_id',$this->companyid)
            ->where('employee.user_type','employee')
            ->select('employee.id','employee.name','employee_details.address','employee_details.department_id as deptid')->groupBy('employee.id')->get();

        $wordata = Employee::join('assign_work_rules','assign_work_rules.user_id','=','employee.id')
            ->join('leave_rules_settings','leave_rules_settings.id','=','assign_work_rules.workrules_id')->select('assign_work_rules.user_id','assign_work_rules.effective_date','leave_rules_settings.name','leave_rules_settings.type')->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<input type="checkbox" class="sub_chk" data-id="'.$row->id.'" />';
            })->rawColumns(['checkbox'])
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })->editColumn('department', function ($row)  {
                $team = Team::where('id',$row->deptid)->first();
                if(!empty($team)){
                    return $team->team_name;
                }
                return 'NA';
            })->editColumn('location', function ($row)  {

                return $row->address;
            })->editColumn('type', function ($row) use($wordata) {
                foreach ($wordata as $work){
                    return $work->type;
                }
            })->editColumn('effective_date', function ($row) use($wordata) {
                foreach ($wordata as $work){
                    if($row->id == $work->user_id) {
                        return $work->effective_date;
                    }
                }
            })
            ->addColumn('rulesapplied', function ($row) use($wordata){
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->name;
                    }
                }
            })
            ->rawColumns(['action','name','department','location','type','effective_date','rulesapplied'])
            ->make(true);
    }

    public function deleteWorkDay($id){
        $workday  =  LeaveRulesSettings::find($id);
        $workday->delete();
        return Reply::success(__('Deleted Successfully!!'));
        abort(403);
    }
    public function postAssignRule(Request $request){
        $date =  Carbon::parse($request->effective_date)->format('Y-m-d');
        $user_ids = explode(',', $request->ids);
        foreach ($user_ids as $user_id){
            $leave_assign = AssignWorkRules::updateOrCreate(
                ['user_id' => $user_id],
                ['workrules_id' => $request->rules_id,'effective_date'=>$date]
            );
        }
        return Reply::success(__('messages.addLeaveRulesSettings'));
        abort(403);
    }

    public  function leaveBalance(){
        $this->leaverules = LeavesRules::join('leave_assigneds','leave_assigneds.leave_rule_id','=','leaves_rules.id')
            ->join('users','users.id','=','leave_assigneds.user_id')->groupBy('leave_assigneds.leave_rule_id')->get();
        return view('admin.leaves.leave_balance',$this->data);
    }
    public function leaveBalanceData(Request $request){
        $leaveTypes = LeaveType::byUser(Auth::id());
        $allowedLeaves = LeaveType::sum('no_of_leaves');
        $leaverules = LeavesRules::join('leave_assigneds','leave_assigneds.leave_rule_id','=','leaves_rules.id')
            ->join('users','users.id','=','leave_assigneds.user_id')->groupBy('leave_assigneds.leave_rule_id')->get();

        $users = User::join('employee_details','employee_details.user_id','=','users.id')
            ->select('users.id','users.name','employee_details.address','employee_details.department_id as deptid')->groupBy('users.id')->get();

        $datatable = Datatables::of($users);
        $datatable->addIndexColumn();
        $datatable->editColumn('name', function ($row)  {

            return $row->name;
        });
        $datatable->editColumn('department', function ($row)  {
            $team = Team::where('id',$row->deptid)->first();
            if(!empty($team)){
                return $team->team_name;
            }
            return 'NA';
        });
        $datatable->editColumn('location', function ($row)  {
            return $row->address;

        });

        foreach($leaverules as $leaverule){
            $leaveremaining =  (int)$leaverule->leave_allowed_in_year - (int)$allowedLeaves;
            $datatable->addColumn($leaverule->leave_rule_name, function($leveobj) use($leaveremaining){

                return $leaveremaining; //reference to the field name which is the same as column name.

            });

        }

        return $datatable->make(true);
    }

    public function assignRule(Request $request){
        $this->ids = $request->ids;
        $this->workrules = LeaveRulesSettings::all();
        return view('admin.leaves.assign_work_day',$this->data)->render();
    }
    public function leaveRulesSettingsdata(){
        $roules = LeaveRulesSettings::where('company_id',$this->companyid)->get();
        return DataTables::of($roules)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })->editColumn('description', function ($row)  {

                return $row->description;
            })

            ->addColumn('action', function ($row) {
                return '<a  href="' . route('admin.leave.leaveRulesSettings',$row->id) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Create"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id=" '. $row->id .' " data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name','description', 'action'])

            ->make(true);
    }

}
