<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\SubAsset;


use App\Location;
use App\Type;
use Illuminate\Http\Request;

class ManageTypesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Types';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }
    public function index()
    {
        $user = $this->user;
        $this->types = Type::where('company_id',$user->company_id)->get();
        return view('admin.types.index', $this->data);
    }


    public function store(Request $request)
    {
        $user = $this->user;
        $type = new Type();
        $type->company_id = $user->company_id;
        $type->title = $request->title;
        $type->symbol = $request->symbol;
        $type->save();
        return Reply::success(__('messages.typeCreated'));
    }

    public function edit($id)
    {
        $this->type = Type::find($id);
        $this->status='success';
        return view('admin.types.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $type = Type::find($id);
        $type->title = $request->title;
        $type->symbol = $request->symbol;
        $type->save();
        return Reply::success(__('messages.typeUpdated'));
    }
    public function destroy($id)
    {
        Type::destroy($id);
        return Reply::success(__('messages.typeDeleted'));
    }
    public function updateSubAsset(Request $request,$id)
    {
        $category = SubAsset::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->asset_id = $request->asset_id;
        $category->status == 'success';

        $category->save();

        return Reply::success(__('messages.assetUpdate'));
    }


    public function updateassetCat(Request $request, $id)
    {
        $category = AssetCategory::find($id);
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('messages.categoryAdded'));
    }



    public function deleteAsset($id)
    {
        Asset::destroy($id);
        $categoryData = Asset::all();

        return Reply::successWithData(__('messages.assetDeleted'),['data' => $categoryData]);
    }
    public function deleteSubAsset($id)
    {
        SubAsset::destroy($id);
        $categoryData = SubAsset::all();

        return Reply::successWithData(__('messages.subassetDeleted'),['data' => $categoryData]);
    }
}
