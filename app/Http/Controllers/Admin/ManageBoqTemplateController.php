<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\BoqTemplatePosition;
use App\Contractors;
use App\CostItems;
use App\Employee;
use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\Http\Requests\Boqtemplate\UpdateStoreBoqTemplateRequest;
use App\Project;
use App\ProjectCategory;
use App\BoqTemplateProduct;
use App\ProjectMember;
use App\SubAsset;
use App\Boqtemplate;
use App\Task;
use App\Title;
use App\Type;
use App\Units;
use App\User;
use View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ManageBoqTemplateController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Boq template';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    } 
    public function index()
    {
        $user = $this->user;
        $this->boqtemplatearray = Boqtemplate::where('company_id',$user->company_id)->get();
        $this->projectcategoryarray = ProjectCategory::all();
        return view('admin.boqtemplate.index', $this->data);
    }
    public function data(Request $request)
    {
        $user = $this->user;
        $category = $request->category;
        $inspectionarray = Boqtemplate::where('company_id',$user->company_id);
        if(!empty($category)){
            $inspectionarray = Boqtemplate::where('category',$category);
        }
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a type="button" class="btn btn-default editBoqtemplate" data-boqtemplate-id="'.$row->id.'"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                if($row->user_id==$user->id){
                    $actionlink .='<a href="javascript:;" data-boqtemplate-id="'.$row->id.'" class="btn btn-sm btn-danger btn-circle sa-params"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                return $actionlink;
            })->addColumn('boqlist', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a class="btn btn-default" href="'.route('admin.boq-template.addboq',[$row->id]).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-list" aria-hidden="true"></i></a>';
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'description',
                function ($row) {
                    return substr(strip_tags($row->description),0,50);
                }
            )
            ->editColumn(
                'category',
                function ($row) {
                    return get_project_category_name($row->category);
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-3 col-xs-4">'.get_users_images($row->user_id).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->user_id) . '">'.ucwords(get_user_name($row->user_id)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'boqlist', 'action','type', 'description', 'added_by'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $user = $this->user;
        $boqtemplate = new Boqtemplate();
        $boqtemplate->user_id = $user->id;
        $boqtemplate->company_id = $user->company_id;
        $boqtemplate->name = $request->name;
        $boqtemplate->category = $request->category;
        $boqtemplate->description = $request->description;
        $boqtemplate->save();
        return Reply::success(__('messages.boqtemplateCreated'));
    }

    public function edit($id)
    {
        $boqtemplate =  Boqtemplate::find($id);
        $this->boqtemplate = $boqtemplate;
        $this->projectcategoryarray = ProjectCategory::all();
        return view('admin.boqtemplate.edit', $this->data);
    }

    public function update(UpdateStoreBoqTemplateRequest $request,$id)
    {
        $user = $this->user;
        $boqtemplate = Boqtemplate::find($id);
        $boqtemplate->user_id = $user->id;
        $boqtemplate->company_id = $user->company_id;
        $boqtemplate->name = $request->name;
        $boqtemplate->category = $request->category;
        $boqtemplate->description = $request->description;
        $boqtemplate->save();

        return Reply::success(__('messages.boqtemplateUpdated'));
    }
    public function destroy($id)
    {
        Boqtemplate::destroy($id);
        $categoryData = Boqtemplate::all();
        return Reply::successWithData(__('messages.boqtemplateDeleted'),['data' => $categoryData]);
    }
    public function addboq(Request $request, $id){

        $user = $this->user;
        $boqtemplate = Boqtemplate::where('id',$id)->first();
        if(empty($boqtemplate)){
            return redirect(route('admin.boq-template.index'));
        }
        $this->boqtemplate = $boqtemplate;
        $this->userarray = Employee::getAllEmployees($user);
        $this->id = (int)$id;
        $columsarray = array(
            'task'=>'Task',
            'description'=>'Description',
            'qty'=>'Qty',
            'rate'=>'Rate',
            'unit'=>'Unit',
            'contractrate'=>'Contract rate',
            'contractamount'=>'Contract amount'
        );
        $col =1; foreach($columsarray as $colkey => $colums){
            $postitionnew = BoqTemplatePosition::where('template_id',$id)->where('itemslug',$colkey)->where('position','col')->first();
            if(empty($postitionnew->id)){
                $postitionnew = new BoqTemplatePosition();
            $postitionnew->user_id = $user->id;
            $postitionnew->company_id = $user->company_id;
            $postitionnew->template_id = $id;
            $postitionnew->position = 'col';
            $postitionnew->itemid = 0;
            $postitionnew->itemname = $colums;
            $postitionnew->itemslug = $colkey;
            $postitionnew->collock = 0;
            $postitionnew->level = 0;
            $postitionnew->parent = 0;
            $postitionnew->catlevel = '';
            $postitionnew->inc = $col;
            $postitionnew->save();
                $col++;
            }
        }
        $this->columsarray =  array('task','description','rate','unit','qty','contractrate','contractamount');
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->typesarray = Type::where('company_id',$user->company_id)->get();
        return view('admin.boqtemplate.addBoq', $this->data);
    }
    public function addBoqCostItemRow(Request $request){

        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $user = $this->user;
        $maxcostitemid = BoqTemplateProduct::where('template_id',$request->templateid)->max('inc');
        $newid = $maxcostitemid+1;
        $costitemproduct = $request->itemid;
        $costitemslist = \App\BoqTemplateProduct::where('template_id',$request->templateid)->orderBy("id",'asc')->get();
        $unitsarray = \App\Units::where('company_id',$user->company_id)->orderBy("id",'asc')->get();
        $typesarray = \App\Type::where('company_id',$user->company_id)->orderBy("id",'asc')->get();
        $costitem = CostItems::find($costitemproduct);
        $proprogetdat = new BoqTemplateProduct();
        $proprogetdat->user_id = $user->id;
        $proprogetdat->company_id = $user->company_id;
        $proprogetdat->template_id = $request->templateid;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_id = $costitem->id;
        $proprogetdat->unit = get_cost_item_unit_name($costitem->cost_items_id);
        $proprogetdat->description = $costitem->description;
        $proprogetdat->position_id = $request->positionid;
        $proprogetdat->rate = $costitem->rate;
        $proprogetdat->qty = $costitem->qty;
        $proprogetdat->inc = $newid;
        $proprogetdat->save();
        return 'success';
    }
    public function boqLoop(Request $request){

        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $boqtemplate = $request->templateid;
        $user = $this->user;
        $this->user = $user;
        $this->categories = BoqCategory::where('company_id',$user->company_id)->get();
        $this->unitsarray = Units::where('company_id',$user->company_id)->get();
        $this->typesarray = Type::where('company_id',$user->company_id)->get();
        $this->template = Boqtemplate::find($boqtemplate);
        $this->columsarray =  array('task','description','rate','unit','qty','contractrate','contractamount');
        $messageview = View::make('admin.boqtemplate.boqloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function taskDestroy($id)
    {
        BoqTemplateProduct::where('id',$id)->delete();
        return Reply::success(__('messages.taskDelete'));
    }
    public function activityDestroy($id)
    {
        $position = BoqTemplatePosition::where('id',$id)->first();
        if(!empty($position->id)){
            $category = '';
            if(!empty($position->parent)){
                $category .= $position->parent;
                $category .= ',';
            }
            $category .= $position->itemid;
            BoqTemplateProduct::where('position_id',$position->id)->delete();
            $position->delete();
            return Reply::success(__('messages.activityDeleted'));
        }
    }
    public function addBoqCostItemCategory(Request $request){

        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $user = $this->user;
        $parent = $request->parent ?: 0;
        $maxpositionarray = \App\BoqTemplatePosition::where('template_id',$request->templateid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new BoqTemplatePosition();
        $columsarray->user_id = $user->id;
        $columsarray->company_id = $user->company_id;
        $columsarray->template_id = $request->templateid;
        $columsarray->position = 'row';
        $columsarray->itemid = $request->itemid;
        $columsarray->itemname = $request->itemname;
        $columsarray->level = $request->level;
        $columsarray->catlevel = $request->catlevel;
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();

    }
    public function projectBoqCreate(Request $request, $id){
        $user = $this->user;
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->where('company_id',$user->company_id)->get();
        $this->id = $id;
        return view('admin.projects.project-boq-create', $this->data);
    }
   /* public  function boqChangeColPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->templateid;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->boqtemplate;
        $title = $request->title;
        $boqtemplate = $request->boqtemplate;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }*/
    public  function boqChangeCostitemPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->boqtemplate;
        $title = $request->title;
        $boqtemplate = $request->boqtemplate;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function  BoqLock(Request $request){
        if(empty($request->template_id)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->lockcolms;
        $boqtemplate = $request->template_id;
        if(!empty($positionarray)&&count($positionarray)>0){
            BoqTemplatePosition::where('template_id',$boqtemplate)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            BoqTemplatePosition::where('template_id',$boqtemplate)->update(['collock'=>0]);
        }
        return redirect(route('admin.boq-template.addBoq',[$boqtemplate]));
    }
    public  function updateCostItem(Request $request){
        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $user = $this->user;
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $title = $request->title;
        $boqtemplate = $request->templateid;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('company_id',$user->company_id)->where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'worktype':
                $worktype = Type::where('title',$itemvalue)->first();
                if(!empty($worktype->id)){
                    $dataarray[$item] = $worktype->id;
                }
                break;
        }
        BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$itemid)->update($dataarray);
        $projectcostitem = BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$itemid)->first();
        $marktype = $projectcostitem->marktype;
        $markvalue = $projectcostitem->markvalue;
        $rate = $projectcostitem->rate;
        $qty = $projectcostitem->qty;
        $finalrate = $rate;
        $totalamount = 0;
        if(!empty($qty)&&!empty($rate)){
            $totalamount = $qty*$rate;
            $projectcostitem->finalrate = $rate;
            $projectcostitem->finalamount = $totalamount;
        }
        if(!empty($marktype)&&!empty($markvalue)){
            if($marktype=='percent'){
                $percent = ($markvalue/100)*$rate;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            if($marktype=='amount'){
                $percent =$markvalue;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            $totalamount = $finalrate;
            $projectcostitem->finalrate = $finalrate;
            $projectcostitem->finalamount = $totalamount;
        }
        $projectcostitem->save();
    }
}
