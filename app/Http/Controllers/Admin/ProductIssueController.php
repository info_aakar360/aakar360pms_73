<?php

namespace App\Http\Controllers\Admin;

use App\AppProductIssue;
use App\AppProductLog;
use App\Bom;
use App\Helper\Reply;
use App\Http\Requests\CostItems\StoreCostItems;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\Indent;
use App\Product;
use App\ProductBrand;
use App\ProductIssue;
use App\ProductIssueReturn;
use App\ProductLog;
use App\Project;
use App\Stock;
use App\Store;
use App\Units;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductIssueController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Issue';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'store';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->issuedProducts = ProductIssue::where('company_id',$this->companyid)->groupBy('unique_id')->get();
        return view('admin.product-issue.index', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->stores = Store::get();
        $user           = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this->projects = Project::whereIn('id', $projecta)->get();
        $this->indents = Indent::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->products = Bom::where('company_id',$this->companyid)->get();
        return view('admin.product-issue.create', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $indent = Indent::where('indent_no',$request->indent_no)->first();
        $project_id = $request->project_id;
        $projectId = Project::find($project_id);
        $store_id = $request->store_id;
        $productissueuni = AppProductIssue::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
        $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
        $unique_id = 'Prod-PI-'.date("d-m-Y").'-'.$newid;
        $product_id = $request->product_id;
        $unit_id = $request->unit_id;
        $quantity = $request->quantity;
        $brand = $request->brand;
        $issued_by = $this->user->id;
        foreach ($product_id as $key => $value) {
            if(!empty($quantity[$key])) {
                $scheck = Stock::where('cid',$value)->where('store_id', $request->store_id)->where('project_id',$project_id)->first();

                /*if(!empty($indent)){
                    $scheck = Stock::where('cid',$value)->where('store_id', $request->store_id)->where('project_id',$project_id)->where('indent_id',$indent->id)->first();
                }*/
                if(!empty($scheck) || $scheck !== null){
                    if($quantity[$key] > $scheck->stock){
                        return Reply::error(__('Issued Quantity greater then stock'));
                    }else{
                        $bal = $scheck->stock - $quantity[$key];
                        $scheck->stock = $bal;
                        $scheck->save();
                        $pro = new AppProductIssue();
                        $pro->unique_id = $unique_id;
                        $pro->project_id = $projectId->id;
                        $pro->company_id = $projectId->company_id;
                        $pro->store_id = $store_id;
                        $pro->product_id = $value;
                        $pro->quantity = $quantity[$key];
                        $pro->unit_id = $unit_id[$key];
                        $pro->date = date('Y-m-d');
                        $pro->remark = '';
                        $pro->issued_by = $issued_by;
                        $pro->inc = $newid;
                        $pro->save();
                        $pl = new ProductLog();
                        $pl->store_id = $request->store_id;
                        $pl->project_id = $projectId->id;
                        $pl->company_id = $projectId->company_id;
                        $pl->created_by = $this->user->id;
                        $pl->module_id = $pro->id;
                        $pl->module_name = 'product_issue';
                        $pl->product_id = $value;
                        $pl->quantity = $quantity[$key];
                        $pl->balance_quantity = $bal;
                        $pl->transaction_type = 'minus';
                        $pl->remark = '';
                        $pl->save();

                    }
                }

            }
        }
        return Reply::success(__('Product issued successfully'));
    }

    public function returns($id)
    {
        $this->stores = Store::get();
        $this->products = ProductIssue::groupBy('unique_id')->get();
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        $this->issueretrun = ProductIssueReturn::where('unique_id',$id)->get();
        return view('admin.product-issue.return', $this->data);
    }

    public function storeReturn(Request $request)
    {
        $bal = '';
        $user = $this->user;
        $idarray = $request->id;
        $quantity = $request->quantity;
        $product_id = $request->product_id;
        $store =  $request->store_id;
        $project = $request->project_id;
        $projectdetails = Project::find($project);
        $returned_quantity = $request->returned_quantity;
        $productissueuni = ProductIssueReturn::where('company_id',$projectdetails->company_id)->orderBy('id','desc')->max('inc');
        $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
        $unique_id = 'Prod-PIR-'.date("d-m-Y").'-'.$newid;
        foreach($idarray as $key=>$value ){
                $scheck = Stock::where('cid',$product_id[$key])->where('store_id', $store)->where('project_id',$project)->first();
                if(!empty($scheck) && $scheck !== null){
                    $bal = $scheck->stock + $returned_quantity[$key];
                    $scheck->stock = $bal;
                    $scheck->save();

                }
                if ($quantity[$key] < $returned_quantity[$key]) {
                    return Reply::error(__('Issued Return Quantity greater then available stock'));
                } else {
                    $qty = $quantity[$key] - $returned_quantity[$key];
                    $productissues = AppProductIssue::where('id',$value)->where('product_id',$product_id[$key])->first();
                    if(!empty($productissues)){
                        $issueqty = $productissues->quantity ?: 0;
                        if($issueqty>=$returned_quantity[$key]){
                            $pro = new ProductIssueReturn();
                            $pro->unique_id = $productissues->unique_id;
                            $pro->project_id = $projectdetails->id;
                            $pro->company_id = $projectdetails->company_id;
                            $pro->store_id = $store;
                            $pro->issue_id = $productissues->id;
                            $pro->unit_id = $productissues->unit_id;
                            $pro->product_id = $product_id[$key];
                            $pro->qty = $returned_quantity[$key];
                            $pro->remark = '';
                            $pro->added_by = $user->id;
                            $pro->inc = $newid;
                            $pro->save();

                            if ($pro) {
                                $pl = new AppProductLog();
                                $pl->store_id = $store;
                                $pl->project_id = $projectdetails->id;
                                $pl->company_id = $projectdetails->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pro->id;
                                $pl->module_name = 'product_issue_return';
                                $pl->product_id = $product_id[$key];
                                $pl->quantity = $returned_quantity[$key];
                                if(!empty($bal)){
                                    $pl->balance_quantity = $bal;
                                }
                                $pl->transaction_type = 'plus';
                                $pl->remark = '';
                                $pl->save();
                            }

                        }
                    }
                }

        }
        return Reply::success(__('Product returned successfully'));
    }

    public function issuedProductDetail(Request $request, $id){
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        $this->issueretrun = ProductIssueReturn::where('unique_id',$id)->get();
        return view('admin.product-issue.product-issued-detail', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::get();
        $this->cats = BoqCategory::all();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        $this->units = Units::all();
        return view('admin.cost-items.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CostItems::find($id);
        $category->company_id = $this->user->company_id;
        $category->cost_item_name = $request->cost_item_name;
        $category->cost_item_description = $request->cost_item_description;
        $category->unit = $request->unit;
        $category->type = $request->type;
        $category->save();
        //dd();
        $cost_item_lavel = $request->cost_item_lavel;
        $costlavel = CostItemsLavel::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsLavel::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('Task Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        ProductIssueReturn::where('unique_id',$id)->delete();
        ProductIssue::where('unique_id',$id)->delete();

        return Reply::success('Product Issue Deleted Successfully!!');
    }

    public function data()
    {
        $products = CostItems::select('id', 'cost_item_name', 'cost_item_description')
            ->get();

        return DataTables::of($products)
            ->addColumn('action', function($row){
                return '<a href="'.route('admin.cost-items.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('cost_item_name', function ($row) {
                return ucfirst($row->cost_item_name);
            })
            ->editColumn('cost_item_description', function ($row) {
                return ucfirst($row->cost_item_description);
            })

            ->rawColumns(['action'])
            ->make(true);
    }

    public function productAdd($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproducts = CostItemsProduct::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('admin.cost-items.product-add', $this->data);
    }

    public function productEdit($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproduct = CostItemsProduct::where('id', $id)->first();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('admin.cost-items.product-edit', $this->data);
    }

    public function getCost(Request $request){
        $qty = $request->qty;
        $rate = $request->rate;

        $final = $qty*$rate;

        return $final;
    }

    public function storeProduct(Request $request, $id)
    {
        $cost_item_lavel = $request->product_category_id;
        $costlavel = CostItemsProduct::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsProduct::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('Task Updated Successfully'));
    }

    public function productUpdate(Request $request, $id)
    {

        $costlavel = CostItemsProduct::find($id);
        $costlavel->product_category_id = $request->product_category_id;
        $costlavel->product_brand_id = $request->product_brand_id;
        $costlavel->unit = $request->unit;
        $costlavel->qty = $request->qty;
        $costlavel->wastage = $request->wastage;
        $costlavel->rate = $request->rate;
        $costlavel->cost = $request->cost;
        $costlavel->save();

        return Reply::success(__('Task Updated Successfully'));
    }

    public function destroyProduct($id)
    {
        CostItemsProduct::destroy($id);
        $categoryData = CostItemsProduct::all();
        return Reply::successWithData(__('Deleted Successfully'),['data' => $categoryData]);
    }
}
