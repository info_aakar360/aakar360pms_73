<?php

namespace App\Http\Controllers\Admin;

use App\AdhocAction;
use App\Admin\EmployeeTaxDetail;
use App\Company;
use App\CompanyPyslipFormat;
use App\DeclarationSettings;
use App\Designation;
use App\Employee;
use App\EmployeeDetails;
use App\EmployeeLopDetail;
use App\GeneratedUserPayslip;
use App\Holiday;
use App\LeaveRulesSettings;
use App\LeavesRules;
use App\Package;
use App\PaypalInvoice;
use App\PayrollPaySettings;
use App\PayrollRegister;
use App\PayrollVerify;
use App\PaySlipFormat;
use App\PfandEsiSettings;
use App\PtSettings;
use App\RazorpayInvoice;
use App\RecurringComponent;
use App\SalaryBaseComponent;
use App\SalaryComponentVariable;
use App\SalaryStructureSettings;
use App\State;
use App\StripeInvoice;
use App\Team;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\Payroll\StorePayrollSetttings;
use App\Http\Requests\Payroll\UpdatePayrollSetttings;
use App\Http\Requests\Payroll\StorePtSetttings;
use Carbon\Carbon;
use App\Helper\Reply;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;


class ManagePayrollController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.payroll';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('payroll',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.payroll.pay_settings',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $payrolldata = PayrollPaySettings::all();
        if(($payrolldata)->count() == 0) {
            $payrollsettings = new PayrollPaySettings();
            $payrollsettings->effective_date = $todaydate . '' . $request->effective_date;
            $payrollsettings->pay_frequncy = $request->pay_frequncy;
            $payrollsettings->pay_structure = $request->pay_structure;
            $payrollsettings->pay_cycle = $request->pay_cycle;
            $payrollsettings->input_cycle = $request->input_cycle;
            $payrollsettings->payout_date = $request->payout_date;
            $payrollsettings->doj_date = $request->doj_date;
            $payrollsettings->include_weekly_leave = $request->include_weekly_leave;
            $payrollsettings->include_hollyday_leave = $request->include_hollyday_leave;
            $payrollsettings->save();
            return Reply::success(__('messages.addPayrollPaySettings'));
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function pfEsiSettings(){
        $this->componets = RecurringComponent::select('id','component_name')->get();
        $this->variables = SalaryComponentVariable::select('id','component_name')->get();
        $this->pfesisettings = PfandEsiSettings::where('company_id',$this->user->company_id)->first();
        return view('admin.payroll.pf_esi_settings',$this->data);
    }

    public function storePfEsiSettings(StorePayrollSetttings $request){
        $alldata = PfandEsiSettings::where('company_id',$this->user->company_id)->first();
        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $formula = '';
        if($request->custum_formula_select){
            $formula =  implode(',',$request->custum_formula_select)?implode(',',$request->custum_formula_select):false;
        }
        if(empty($alldata)) {
            $pfesisettings = new PfandEsiSettings();
            $pfesisettings->company_id = $this->user->company_id;
            $pfesisettings->effective_date = $todaydate . '' . $request->effective_date;
            $pfesisettings->pf_applicable = $request->fund_status;
            $pfesisettings->min_pf_calc_amount = $request->min_calc_amount;
            $pfesisettings->emp_pf_contribution = $request->emp_pf_contribution;
            $pfesisettings->fixed_amount = $request->fixed_amount?$request->fixed_amount:'';
            $pfesisettings->custum_formula_select = $formula;
            $pfesisettings->custum_formula_percent = $request->custum_formula_percent;
            $pfesisettings->employer_pf_contibution = $request->employer_pf_contibution;
            $pfesisettings->enable_min_ceilig = $request->enable_min_ceilig;
            $pfesisettings->lop_dependent = $request->lop_dependent;
            $pfesisettings->emploer_ctc_contribution = $request->emploer_ctc_contribution;
            $pfesisettings->allow_override = $request->allow_override;
            $pfesisettings->admin_ctc_charges = $request->amin_ctc_charges;
            $pfesisettings->save();
            $lastid = $pfesisettings->id;
            Session::put('pfid', $lastid);
            return Reply::success(__('messages.addPFESISettings'));
            abort(403);
        }else{
            if($alldata->company_id == $this->user->company_id){
                $pfesisettings = PfandEsiSettings::find($alldata->id);
                $pfesisettings->company_id = $this->user->company_id;
                $pfesisettings->effective_date = $todaydate . '' . $request->effective_date;
                $pfesisettings->pf_applicable = $request->fund_status;
                $pfesisettings->min_pf_calc_amount = $request->min_calc_amount;
                $pfesisettings->emp_pf_contribution = $request->emp_pf_contribution;
                $pfesisettings->fixed_amount = $request->fixed_amount?$request->fixed_amount:'';
                $pfesisettings->custum_formula_select = $formula;
                $pfesisettings->custum_formula_percent = $request->custum_formula_percent;
                $pfesisettings->employer_pf_contibution = $request->employer_pf_contibution;
                $pfesisettings->enable_min_ceilig = $request->enable_min_ceilig;
                $pfesisettings->lop_dependent = $request->lop_dependent;
                $pfesisettings->emploer_ctc_contribution = $request->emploer_ctc_contribution;
                $pfesisettings->allow_override = $request->allow_override;
                $pfesisettings->admin_ctc_charges = $request->amin_ctc_charges;
                $pfesisettings->save();
                $lastid = $pfesisettings->id;
                Session::put('pfid', $lastid);
                return Reply::success(__('Updated Successfully!!'));
                abort(403);
            }
        }
    }
    public function storeEsiSettings(Request $request){
        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $alldata = PfandEsiSettings::where('company_id',$this->user->company_id)->first();
        $id = $alldata->company_id?$alldata->company_id:Session::get('pfid');
        if($alldata->count() == 1 && $alldata->company_id == $this->user->company_id) {
                $pfesisettings = PfandEsiSettings::find($id);
                $pfesisettings->employer_contribution = $request->employer_cont;
                $pfesisettings->esi_applicable = $request->esi_status;
                $pfesisettings->esi_effective_date = $todaydate . '' . $request->effective_date;
                $pfesisettings->esi_max_amount = $request->max_month_gross;
                $pfesisettings->esi_employer = $request->employer_cont;
                $pfesisettings->esi_employee = $request->employee_contribution;
                $pfesisettings->include_empl_ctc = $request->include_empl_ctc;
                $pfesisettings->allow_esi_override = $request->allow_esi_override;
                $pfesisettings->save();
                return Reply::success(__('messages.addPFESISettings'));
                abort(403);
            }
    }

    public  function editPfEsiSettings($id){
        $this->pfEsi = PfandEsiSettings::find($id);
        return view('admin.payroll.pfesisettingsedit',$this->data);
    }
    public function pfEsiData(Request $request){
        $employee = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->where('employee.company_id',$this->companyid)
            ->where('user_type','employee')
            ->get();

        $pfesidata = PfandEsiSettings::all();

        return DataTables::of($pfesidata)
            ->addIndexColumn()
            ->editColumn('name', function ($row) use($employee,$request) {
                if($request->periodtype == "monthly"){
                    foreach ($employee as $users) {
                        if ($users->monthly_rate >= $row->min_pf_calc_amount) {
                            return $users->name;
                        }
                    }
                }
            })
            ->editColumn('pfapplicable', function ($row) use($employee,$request) {
                if($request->periodtype == "monthly"){
                    foreach ($employee as $users) {
                        if ($users->monthly_rate >= $row->min_pf_calc_amount) {
                            return $row->pf_applicable;
                        }
                    }
                }
            })
            ->editColumn('pfnumber', function ($row)  {
                return $row->pf_number;
            })
            ->editColumn('pfdatejoined', function ($row)  {
                return $row->pf_joining_date;
            })
            ->editColumn('uan', function ($row)  {
                return $row->uan;
            })
            ->editColumn('epsnumber', function ($row)  {
                return $row->esi_number;
            })
            ->editColumn('pfoverride', function ($row)  {
                return $row->allow_override;
            })
            ->editColumn('pfemployer', function ($row)  {
                return $row->employer_pf_contibution;
            })
            ->editColumn('pfemployee', function ($row)  {
                return $row->emp_pf_contribution;
            })
            ->editColumn('vps', function ($row)  {
                return $row->vfo;
            })
            ->editColumn('includeedli', function ($row)  {
                return "";
            })->editColumn('esiapplicable', function ($row)use($request,$employee)  {
                if($request->periodtype == "monthly"){
                    foreach ($employee as $users) {
                        if ($users->monthly_rate <= $row->esi_max_amount) {
                            return $row->esi_applicable;
                        }
                    }
                }
            }
            )->editColumn('esinumber', function ($row)  {
                return $row->esi_number;
            })->editColumn('esioverride', function ($row)  {
                return $row->allow_esi_override;
            })->editColumn('esiemployee', function ($row)  {
                return $row->esi_employee;
            })->editColumn('esiemployer', function ($row)  {
                return $row->employer_contribution;
            })
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.payroll.editPfEsiSettings',[$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function updatePfEsiSettings(UpdatePayrollSetttings $request){

        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $pfesisettings = PfandEsiSettings::find($request->id);
        $pfesisettings->effective_date = $todaydate . '' . $request->effective_date;
        $pfesisettings->pf_applicable = $request->pf_applicable;
        $pfesisettings->pf_number = $request->pf_number;
        $pfesisettings->pf_joining_date = $todaydate . '' .$request->pf_joining_date ;
        $pfesisettings->uan = $request->uan ;
        $pfesisettings->eps_number = $request->eps_number;
        $pfesisettings->vfo = $request->vfo;
        $pfesisettings->esi_applicable = $request->esi_applicable;
        $pfesisettings->esi_number = $request->esi_number;
        $pfesisettings->allow_override = $request->pf_override;
        $pfesisettings->emp_pf_contribution = $request->pf_employee;
        $pfesisettings->employer_pf_contibution = $request->pf_employer;
        $pfesisettings->allow_esi_override = $request->esi_override;
        $pfesisettings->esi_employee = $request->esi_employee;
        $pfesisettings->esi_employer = $request->esi_employer;

        $pfesisettings->save();


        return Reply::success(__('messages.addPFESISettings'));
        abort(403);
    }

    public function updatePfStatus(Request $request){
        $pfstatuschange = PfandEsiSettings::find($request->id);
        $pfstatuschange->pf_applicable = $request->status;
        $pfstatuschange->save();
        return Reply::success(__('messages.updatePfStatus'));
    }
    public function updateEsiStatus(Request $request){
        $pfstatuschange = PfandEsiSettings::find($request->id);
        $pfstatuschange->esi_applicable = $request->status;
        $pfstatuschange->save();
        return Reply::success(__('messages.updateEsiStatus'));
    }
    public function pfEsiOverview(){
        return view('admin.payroll.pf_esi_overview',$this->data);
    }



    public function paySlipSettings(){
        return view('admin.payroll.pay_slip_settings',$this->data);
    }

    public function ptSettings(){
        $this->states = State::all();
        return view('admin.payroll.pt_settings',$this->data);
    }
    public function storePtSettings(StorePtSetttings $request){
        $todaydate = Carbon::today()->timezone($this->global->timezone);

        PtSettings::updateOrCreate(
            ['effective_date' => $todaydate.''.$request->effective_date,'pt_state'=> $request->pt_state,'state_override' => $request->state_override,'pt_applicable'=>$request->pt_applicable]);
        return Reply::success(__('messages.addPtSettings'));
        abort(403);
    }

    public function ptOverview(){
        return view('admin.payroll.pt_overview',$this->data);
    }

    public function ptData(Request $request){
        $employee = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        $ptdata = PtSettings::all();
        $states = State::find($ptdata->pt_state);
    
        return DataTables::of($ptdata)
            ->addIndexColumn()
            ->editColumn('name', function ($row) use($employee,$request) {
                if($request->periodtype == "monthly"){
                    foreach ($employee as $user) {
                        if ($user->monthly_rate > $row->min_pf_calc_amount) {
                            return $user->name;
                        }
                    }
                }
            })
            ->editColumn('address', function ($row) use($employee) {

                  return "";

            })
            ->editColumn('ptapplicable', function ($row) use($employee,$request) {
                if($request->periodtype == "monthly"){
                    foreach ($employee as $user) {
                        if ($user->monthly_rate > $row->min_pf_calc_amount) {
                            return "Yes";
                        }
                    }
                }
            })
            ->editColumn('ptstate', function ($row)  use($states){
                if(!empty($states)){
                    return $state->name;
                }
                
            })
            ->editColumn('ptamount', function ($row)  {
                return "";
            })

            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.payroll.editptSettings', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'address','ptapplicable','ptstate','ptamount','action' ])
            ->make(true);
    }

    public  function editptSettings($id){
        $this->pgData = PtSettings::find($id);
        return view('admin.payroll.edit_pt_settings',$this->data);
    }

    public  function updatePtSettings(Request $request){
        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $ptsettings = PtSettings::find($request->id);
        $ptsettings->effective_date = $request->$todaydate.''.$request->effective_date;
        $ptsettings->pt_state = $request->pt_state;
        $ptsettings->state_override = $request->state_override;
        $ptsettings->pt_applicable = $request->pt_applicable;
        $ptsettings->save();
        return Reply::success(__('messages.addPtSettings'));
        abort(403);
    }



    public function declarationSettings(){
        $this->users= User::all();
        return view('admin.payroll.declaration_settings',$this->data);
    }

    public function paySlipformat(){
        $format = PaySlipFormat::where('compony_id',$this->user->company_id)->get();

        return DataTables::of($format)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return $row->name;
            })
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.payroll.formatOverview', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name','action' ])
            ->make(true);
    }

    public function formatOverview($id)
    {
        $this->checkformat = CompanyPyslipFormat::where('company_id',$this->user->company_id)->where('payslip_id',$id)->first();
        return view('admin.payroll.format'.$id,$this->data);
    }

    public function storeDecSettings(Request $request){
        $employee = '';
        if($request->select_employee){
            $employee = implode(',',$request->select_employee)?implode(',',$request->select_employee):false;
        }
        $todaydate = Carbon::today()->timezone($this->global->timezone);
        $dec_settings = DeclarationSettings::first();
        if(!empty($dec_settings->id)){
            $dec_settings = DeclarationSettings::find($dec_settings->id);
            $dec_settings->monthly_dec_from = $request->montly_select_from;
            $dec_settings->monthly_dec_to =	$request->montly_select_to;
            $dec_settings->cutoff_dec_month = $request->cutoff_select_month;
            $dec_settings->cutoff_dec_month_date = $request->cutoff_select;
            $dec_settings->yearly_dec_from = $todaydate.''.$request->from;
            $dec_settings->yearly_dec_to = $todaydate.''.$request->to;
            $dec_settings->apply_to_emp_id = $employee;
            $dec_settings->check_all = $request->checkall;
            $dec_settings->proof_submission =$request->proof_submission?$request->proof_submission:'';
            $dec_settings->save();
        }else{
            $dec_settings = new DeclarationSettings();
            $dec_settings->monthly_dec_from = $request->montly_select_from;
            $dec_settings->monthly_dec_to =	$request->montly_select_to;
            $dec_settings->cutoff_dec_month = $request->cutoff_select_month;
            $dec_settings->cutoff_dec_month_date = $request->cutoff_select;
            $dec_settings->yearly_dec_from = $todaydate.''.$request->from;
            $dec_settings->yearly_dec_to = $todaydate.''.$request->to;
            $dec_settings->apply_to_emp_id =$employee;
            $dec_settings->check_all = $request->checkall;
            $dec_settings->proof_submission =$request->proof_submission?$request->proof_submission:'';
            $dec_settings->save();
        }

        return Reply::success(__('messages.addDecSettings'));
        abort(403);
    }

    public function generatePayslip(Request $request){
        $storemonthpayroll = new GeneratedUserPayslip();
        $storemonthpayroll->user_id = $request->id;
        $storemonthpayroll->month = $request->month;
        $storemonthpayroll->year = $request->year;
        $storemonthpayroll->save();
        // check data deleted or not
        if ($storemonthpayroll == 1) {
            $success = true;
            $message = "Generated successfully";
        } else {
            $success = true;
            $message = "not found";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function paySlips(Request $request,$id){

        $this->payRegister = PayrollRegister::join('employee','employee.id','=','payroll_registers.user_id')->join('employee_details','employee_details.employee_id','=','payroll_registers.user_id')->where('payroll_registers.user_id',$id)->where('payroll_registers.company_id',$this->user->company_id)->first();
        $this->designation = Designation::where('id',$this->payRegister->designation_id)->first();
        $this->team = Team::where('id',$this->payRegister->department_id)->first();
        $this->company = Company::where('id',$this->payRegister->company_id)->first();
        $this->pfesidata = PfandEsiSettings::first();
        $this->ahdocearning = AdhocAction::where('user_id',$this->user->id)->where('compo_type','earning')->first();
        $this->ahdocdiduction = AdhocAction::where('user_id',$this->user->id)->where('compo_type','diduction')->first();
        $this->taxdetail = EmployeeTaxDetail::first();
        $salary = SalaryStructureSettings::first();
        $this->salarysettings = $salary;
        $this->ptSettings = PtSettings::first();
        $this->allowance = explode(',',$salary->allowance);
        $this->amount = explode(',',$salary->amount);
        $this->totalAllowance = array_sum($this->amount);
        $this->salary = $salary;

        return view('admin.payroll.salary_slips',$this->data);
    }

    public function Components(){
        return view('admin.payroll.components',$this->data);
    }

    public function storePaySlipFormat(Request $request){
        $checkformat = CompanyPyslipFormat::where('company_id',$this->user->company_id)->where('payslip_id',$request->pay_slip_id)->first();
        if(!empty($checkformat)){
            $checkformat->company_id    =    $this->user->company_id;
            $checkformat->payslip_id    =    $request->pay_slip_id;
            $checkformat->save();
            return Reply::success(__('messages.defaultFormatUpdate'));
        }else{
            $newcheckformat                =    new CompanyPyslipFormat();
            $newcheckformat->company_id    =    $this->user->company_id;
            $newcheckformat->payslip_id    =    $request->pay_slip_id;
            $newcheckformat->save();
            return Reply::success(__('messages.defaultFormatSave'));
        }
        abort(403);
    }
}
