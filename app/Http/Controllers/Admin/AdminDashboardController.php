<?php

namespace App\Http\Controllers\Admin;

use App\AttendanceSetting;
use App\Currency;
use App\DashboardWidget;
use App\Helper\Reply;
use App\Indent;
use App\InspectionName;
use App\LeadFollowUp;
use App\Leave;
use App\LogTimeFor;
use App\Meetings;
use App\Observations;
use App\Project;
use App\ProjectActivity;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectsHoliday;
use App\ProjectTimeLog;
use App\PunchItem;
use App\Rfi;
use App\Submittals;
use App\Task;
use App\TaskboardColumn;
use App\Ticket;
use App\Todo;
use App\Traits\CurrencyExchange;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\Notifications;

class AdminDashboardController extends AdminBaseController
{
    use CurrencyExchange, Notifications;

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.dashboard';
        $this->pageIcon = 'icon-speedometer';
        $this->activeMenu = 'pms';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $user = $this->user;
       /* $this->notify($user, 0, "new-notification","Testing...");*/
        $projectarray = explode(',',$user->projectlist);
        if(!empty($request->project_id)) {
            $projectid = $request->project_id;
        }else{
            $projectid =  !empty($projectarray[0]) ? $projectarray[0] : '';
        }
        $this->projectarray = Project::whereIn('id',$projectarray)->get();
        $this->projectid = $projectid;
        $this->notstarted = Task::where('project_id',$projectid)->where('status', 'notstarted')->count();
        $this->inprogress = Task::where('project_id',$projectid)->where('status', 'inprogress')->count();
        $this->delayed = Task::where('project_id',$projectid)->where('status', 'delayed')->count();
        $this->completed = Task::where('project_id',$projectid)->where('status', 'completed')->count();
        $this->inproblem = Task::where('project_id',$projectid)->where('status', 'inproblem')->count();

        $this->indentlist = Indent::join("indent_products","indent_products.indent_id","=","indents.id")
            ->select("indents.*","indent_products.expected_date")->where('indents.project_id',$projectid)->get();
        $this->todolist = Todo::where('project_id',$projectid)->get();
        $this->tasks = Task::where('project_id',$projectid)->get();
        $this->issueslist = PunchItem::where('projectid',$projectid)->get();
        $this->rfilist = Rfi::where('projectid',$projectid)->get();
        $this->submittalslist = Submittals::where('projectid',$projectid)->get();
        $this->inspectionlist = InspectionName::join('inspection_assign_form','inspection_assign_form.inspection_id','=','inspection_name.id')
            ->select('inspection_name.*','inspection_assign_form.due_date')->whereRaw('FIND_IN_SET('.$user->id.',inspection_assign_form.assign_to)')->get();
        $this->observationslist = Observations::where('start_date','<>','')->where('due_date','<>','')->whereRaw('FIND_IN_SET('.$user->id.',assign_to)')->get();
        $this->meetingslist = Meetings::join('meeting_details','meeting_details.meeting_id','=','meetings.id')
            ->select('meetings.*','meeting_details.meeting_date','meeting_details.title as meetingtitle','meeting_details.start_time','meeting_details.finish_time')->where('meetings.company_id',$user->company_id)->get();
        $this->activitylist = ProjectCostItemsPosition::where('position','row')->where('project_id',$projectid)->orderBy('id','asc')->get();
         $currentdate = date('Y-m-d');
        $projecttasklist = ProjectCostItemsProduct::where('project_id',$projectid)->where('start_date','<=',$currentdate)->get();
        $plannedprogress = 0;
        foreach ($projecttasklist as $projecttask){
            $dates  = datediffrencedays($projecttask->start_date,$projecttask->deadline);
            if($dates>0){
                $singledaypercent = 100/$dates;
                $percentadays = datediffrencedays($projecttask->start_date,$currentdate);
                $plannedprogress += $singledaypercent*$percentadays;
            }
        }
        if($plannedprogress>0&&count($projecttasklist)>0){
            $plannedprogress = $plannedprogress/count($projecttasklist);
        }
        $this->plannedprogress = $plannedprogress;
        $this->projectholidays = ProjectsHoliday::where('project_id',$projectid)->get();
        $this->myleavesarray = Leave::where('user_id',$user->id)->get();
        return view('admin.dashboard.index', $this->data);
    }
    public function indexold()
    {
        $user = $this->user;
        // Getting Attendance setting data
        $this->attendanceSettings = AttendanceSetting::first();

        $taskBoardColumn = TaskboardColumn::all();

        $incompletedTaskColumn = $taskBoardColumn->filter(function ($value, $key) {
            return $value->slug == 'incomplete';
        })->first();

        $completedTaskColumn = $taskBoardColumn->filter(function ($value, $key) {
            return $value->slug == 'completed';
        })->first();

        //Getting Maximum Check-ins in a day
        $this->maxAttandenceInDay = $this->attendanceSettings->clockin_in_day;

        $this->counts = DB::table('users')
            ->select(
                DB::raw('(select count(client_details.id) from `client_details` inner join role_user on role_user.user_id=client_details.user_id inner join users on client_details.user_id=users.id inner join roles on roles.id=role_user.role_id WHERE roles.name = "client" AND roles.company_id = ' . $this->user->company_id . ' AND client_details.company_id = ' . $this->user->company_id . ' and users.status = "active") as totalClients'),
                DB::raw('(select count(DISTINCT(users.id)) from `users` inner join role_user on role_user.user_id=users.id inner join roles on roles.id=role_user.role_id WHERE roles.name = "employee" AND users.company_id = ' . $this->user->company_id . ' and users.status = "active") as totalEmployees'),
                DB::raw('(select count(projects.id) from `projects` WHERE projects.company_id = ' . $this->user->company_id . ') as totalProjects'),
                DB::raw('(select count(invoices.id) from `invoices` where status = "unpaid" AND invoices.company_id = ' . $this->user->company_id . ') as totalUnpaidInvoices'),
                DB::raw('(select sum(project_time_logs.total_minutes) from `project_time_logs` WHERE project_time_logs.company_id = ' . $this->user->company_id . ') as totalHoursLogged'),
                DB::raw('(select count(tasks.id) from `tasks` where tasks.board_column_id=' . $completedTaskColumn->id . ' AND tasks.company_id = ' . $this->user->company_id . ') as totalCompletedTasks'),
                DB::raw('(select count(tasks.id) from `tasks` where tasks.board_column_id=' . $incompletedTaskColumn->id . ' AND tasks.company_id = ' . $this->user->company_id . ') as totalPendingTasks'),
                DB::raw('(select count(attendances.id) from `attendances` inner join users as atd_user on atd_user.id=attendances.user_id where DATE(attendances.clock_in_time) = CURDATE()  AND attendances.company_id = ' . $this->user->company_id . ' and atd_user.status = "active") as totalTodayAttendance'),
                //                DB::raw('(select count(issues.id) from `issues` where status="pending") as totalPendingIssues'),
                DB::raw('(select count(tickets.id) from `tickets` where (status="open" or status="pending") AND tickets.company_id = ' . $this->user->company_id . ') as totalUnResolvedTickets'),
                DB::raw('(select count(tickets.id) from `tickets` where (status="resolved" or status="closed") AND tickets.company_id = ' . $this->user->company_id . ') as totalResolvedTickets')
            )
            ->first();

        $timeLog = intdiv($this->counts->totalHoursLogged, 60) . ' ' .__('modules.hrs');

        if (($this->counts->totalHoursLogged % 60) > 0) {
            $timeLog .= ($this->counts->totalHoursLogged % 60) . ' '. __('modules.mins');
        }

        $this->counts->totalHoursLogged = $timeLog;

        $this->pendingTasks = Task::with('project')
            ->where('tasks.board_column_id', $incompletedTaskColumn->id)
            ->where(DB::raw('DATE(due_date)'), '<=', Carbon::today()->format('Y-m-d'))
            ->orderBy('due_date', 'desc')
            ->get();
        $this->pendingLeadFollowUps = LeadFollowUp::with('lead')->where(DB::raw('DATE(next_follow_up_date)'), '<=', Carbon::today()->format('Y-m-d'))
            ->join('leads', 'leads.id', 'lead_follow_up.lead_id')
            ->where('leads.next_follow_up', 'yes')
            ->where('leads.company_id', company()->id)
            ->get();

        $this->newTickets = Ticket::where('status', 'open')
            ->orderBy('id', 'desc')->get();

        $this->projectActivities = ProjectActivity::with('project')
            ->join('projects', 'projects.id', '=', 'project_activity.project_id')
            ->whereNull('projects.deleted_at')->select('project_activity.*')
            ->limit(15)->orderBy('id', 'desc')->get();
        $this->userActivities = UserActivity::with('user')->limit(15)->orderBy('id', 'desc')->get();

        $this->feedbacks = Project::with('client')->whereNotNull('feedback')->limit(5)->get();

        $locale = strtolower($this->global->locale);


        $darSkyLangs = array('ar', 'az', 'be', 'bg', 'bs', 'ca', 'cs', 'da', 'de', 'el', 'en', 'es', 'et', 'fi', 'fr', 'he', 'hr', 'hu', 'id', 'is', 'it', 'ja', 'ka', 'ko', 'kw', 'lv', 'nb', 'nl', 'no', 'pl', 'pt', 'ro', 'ru', 'sk', 'sl', 'sr', 'sv', 'tet', 'tr', 'uk', 'x-pig-latin', 'zh', 'zh-tw');

        if (!in_array($locale, $darSkyLangs)) {
            $locale = 'en';
        }

        // if(!is_null($this->global->latitude)){
        //     // get current weather
        //     $client = new Client();
        //     $res = $client->request('GET', 'https://api.darksky.net/forecast/4bc1f8da9ffda5c9cdcadda4dc8c5611/'.$this->global->latitude.','.$this->global->longitude.'?units=auto&exclude=minutely,daily&lang='.$locale, ['verify' => false]);
        //     $weather = $res->getBody();
        //     $this->weather = json_decode($weather, true);
        // }

        // earning chart
        $this->currencies = Currency::all();
        $this->currentCurrencyId = $this->global->currency_id;

        $this->fromDate = Carbon::today()->timezone($this->global->timezone)->subDays(60);
        $this->toDate = Carbon::today()->timezone($this->global->timezone);
        $invoices = DB::table('payments')
            ->join('currencies', 'currencies.id', '=', 'payments.currency_id')
            ->where('payments.paid_on', '>=', $this->fromDate)
            ->where('payments.paid_on', '<=', $this->toDate)
            ->where('payments.status', 'complete')
            ->groupBy('payments.paid_on')
            ->orderBy('payments.paid_on', 'ASC')
            ->get([
                DB::raw('DATE_FORMAT(payments.paid_on,"%Y-%m-%d") as date'),
                DB::raw('sum(payments.amount) as total'),
                'currencies.currency_code',
                'currencies.is_cryptocurrency',
                'currencies.usd_price',
                'currencies.exchange_rate'
            ]);

        $chartData = array();
        foreach ($invoices as $chart) {
            if ($chart->currency_code != $this->global->currency->currency_code) {
                if ($chart->is_cryptocurrency == 'yes') {
                    if ($chart->exchange_rate == 0) {
                        if ($this->updateExchangeRates()) {
                            $usdTotal = ($chart->total * $chart->usd_price);
                            $chartData[] = ['date' => $chart->date, 'total' => floor($usdTotal / $chart->exchange_rate)];
                        }
                    } else {
                        $usdTotal = ($chart->total * $chart->usd_price);
                        $chartData[] = ['date' => $chart->date, 'total' => floor($usdTotal / $chart->exchange_rate)];
                    }
                } else {
                    if ($chart->exchange_rate == 0) {
                        if ($this->updateExchangeRates()) {
                            $chartData[] = ['date' => $chart->date, 'total' => floor($chart->total / $chart->exchange_rate)];
                        }
                    } else {
                        $chartData[] = ['date' => $chart->date, 'total' => floor($chart->total / $chart->exchange_rate)];
                    }
                }
            } else {
                $chartData[] = ['date' => $chart->date, 'total' => round($chart->total, 2)];
            }
        }

        $this->chartData = json_encode($chartData);
        $this->leaves = Leave::where('status', '<>', 'rejected')->get();


        $this->logTimeFor = LogTimeFor::first();

        $this->activeTimerCount = ProjectTimeLog::with('user')
            ->whereNull('end_time')
            ->join('users', 'users.id', '=', 'project_time_logs.user_id');

        if ($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task') {
            $this->activeTimerCount = $this->activeTimerCount->join('tasks', 'tasks.id', '=', 'project_time_logs.task_id');
            $projectName = 'tasks.heading as project_name';
        } else {
            $this->activeTimerCount = $this->activeTimerCount->join('projects', 'projects.id', '=', 'project_time_logs.project_id');
            $projectName = 'projects.project_name';
        }

        $this->activeTimerCount = $this->activeTimerCount
            ->select('project_time_logs.*', $projectName, 'users.name')
            ->count();

        $this->widgets       = DashboardWidget::all();
        $this->activeWidgets = DashboardWidget::where('status', 1)->get()->pluck('widget_name')->toArray();
        $this->notstarted = Task::where('company_id',$user->company_id)->where('status', 'notstarted')->count();
        $this->inprogress = Task::where('company_id',$user->company_id)->where('status', 'inprogress')->count();
        $this->delayed = Task::where('company_id',$user->company_id)->where('status', 'delayed')->count();
        $this->completed = Task::where('company_id',$user->company_id)->where('status', 'completed')->count();
        $this->inproblem = Task::where('company_id',$user->company_id)->where('status', 'inproblem')->count();

        $this->indentlist = Indent::join("indent_products","indent_products.indent_id","=","indents.id")
            ->select("indents.*","indent_products.expected_date")->where('indents.company_id',$user->company_id)->get();
        $this->todolist = Todo::where('company_id',$user->company_id)->get();
        $this->tasks = Task::where('company_id',$user->company_id)->get();
        $this->issueslist = PunchItem::where('company_id',$user->company_id)->get();
        $this->rfilist = Rfi::where('company_id',$user->company_id)->get();
        $this->submittalslist = Submittals::where('company_id',$user->company_id)->get();
        $this->inspectionlist = InspectionName::join('inspection_assign_form','inspection_assign_form.inspection_id','=','inspection_name.id')
            ->select('inspection_name.*','inspection_assign_form.due_date')->whereRaw('FIND_IN_SET('.$user->id.',inspection_assign_form.assign_to)')->get();
        $this->observationslist = Observations::where('start_date','<>','')->where('due_date','<>','')->whereRaw('FIND_IN_SET('.$user->id.',assign_to)')->get();
        $this->meetingslist = Meetings::join('meeting_details','meeting_details.meeting_id','=','meetings.id')
            ->select('meetings.*','meeting_details.meeting_date','meeting_details.title as meetingtitle','meeting_details.start_time','meeting_details.finish_time')->where('meetings.company_id',$user->company_id)->get();

        return view('admin.dashboard.index', $this->data);
    }

    public function widget(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        DashboardWidget::where('status', 1)->update(['status' => 0]);

        foreach ($data as $key => $widget) {
            DashboardWidget::where('widget_name', $key)->update(['status' => 1]);
        }

        return Reply::redirect(route('admin.dashboard'), __('messages.updatedSuccessfully'));
    }
}
