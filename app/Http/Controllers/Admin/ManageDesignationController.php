<?php

namespace App\Http\Controllers\Admin;

use App\Designation;
use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\Designation\StoreRequest;
use App\Http\Requests\Designation\UpdateRequest;
use App\ManpowerCategory;
use Yajra\DataTables\DataTables;

class ManageDesignationController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.designation');
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('employees',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->groups = Designation::with('members', 'members.user')->get();
        return view('admin.designation.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.designation.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function quickCreate()
    {
        $user = $this->user;
        $this->teams = ManpowerCategory::where('company_id',$user->company_id)->get();
        return view('admin.designation.quick-create', $this->data);
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        $user = $this->user;
        $group = new ManpowerCategory();
        $group->company_id = $user->company_id;
        $group->title = $request->designation_name;
        $group->save();

        return Reply::redirect(route('admin.designations.index'), 'Designation created successfully.');
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function quickStore(StoreRequest $request)
    {
        $user = $this->user;
        $group = new ManpowerCategory();
        $group->company_id = $user->company_id;
        $group->title = $request->designation_name;
        $group->save();

        $designations = ManpowerCategory::where('company_id',$user->company_id)->get();
        $teamData = '';

        foreach ($designations as $team){
            $selected = '';

            if($team->id == $group->id){
                $selected = 'selected';
            }

            $teamData .= '<option '.$selected.' value="'.$team->id.'"> '.$team->title.' </option>';
        }

        return Reply::successWithData('Group created successfully.', ['designationData' => $teamData]);
    }

    /**
     * Display the specified resource.
     *[
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->designation = Designation::with('members', 'members.user')->findOrFail($id);
        return view('admin.designation.edit', $this->data);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return array
     */
    public function update(UpdateRequest $request, $id)
    {
        $group = ManpowerCategory::find($id);
        $group->title = $request->designation_name;
        $group->save();

        return Reply::redirect(route('admin.designations.index'), __('messages.updatedSuccessfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmployeeDetails::where('designation_id', $id)->update(['designation_id' => NULL]);
        ManpowerCategory::destroy($id);
        return Reply::dataOnly(['status' => 'success']);
    }

    public function getDesignationData(){
        $designations = Designation::with('members', 'members.user')->where('company_id',$this->companyid)->get();
        return DataTables::of($designations)
            ->addIndexColumn()
            ->editColumn('name', function ($row) {
                return $row->name.'<label class="label label-success">'. sizeof($row->members).'Member </label>';
            })
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.designations.edit', [$row->id]) . '" class="btn btn-info"><i class="icon-settings" aria-hidden="true"></i>Manage</a>
                <a href="javascript:;" class="btn btn-danger sa-params" data-user-id="'.$row->id.'" ><i class="fa fa-times"></i>Delete</a>'; })
            ->rawColumns(['name','action'])
            ->make(true);
    }
}
