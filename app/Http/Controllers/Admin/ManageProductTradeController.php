<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\BoqCategory;
use App\Http\Requests\ProductTrade\StoreProductTrade;
use App\ProductTrade;
use Illuminate\Http\Request;

class ManageProductTradeController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Trade';
        $this->pageIcon = 'icon-user';

        $this->pageTitle = 'app.menu.po';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';

        $this->middleware(function ($request, $next) {
            if (!in_array('po', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->trades = ProductTrade::all();
        return view('admin.product-trade.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->trades = ProductTrade::all();
        return view('admin.product-trade.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTrade()
    {
        $this->trades = ProductTrade::all();
        return view('admin.product-trade.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductTrade $request)
    {
        $category = new ProductTrade();
        $category->name = $request->name;
        $category->company_id = $this->user->company_id;
        $category->save();

        return Reply::success(__('Trade added successfully'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTrade(StoreProductTrade $request)
    {
        $category = new ProductTrade();
        $category->name = $request->name;
        $category->company_id = $this->user->company_id;
        $category->save();
        $categoryData = ProductTrade::all();
        return Reply::successWithData(__('Trade added successfully'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->trades = ProductTrade::all();
        $this->trade = ProductTrade::where('id',$id)->first();
        return view('admin.product-trade.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductTrade $request, $id)
    {
        $category = ProductTrade::find($id);
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('Trade edited successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductTrade::destroy($id);
        $categoryData = ProductTrade::all();
        return Reply::successWithData(__('Trade deleted successfully'),['data' => $categoryData]);
    }
}
