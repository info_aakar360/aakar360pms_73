<?php

namespace App\Http\Controllers\Admin;

use App\AppProductLog;
use App\GdprSetting;
use App\GlobalSetting;
use App\Helper\Reply;
use App\Project;
use App\ProjectMember;
use App\ProjectsLogs;
use App\Traits\Notifications;
use App\User;
use Illuminate\Http\Request;
use View;
use App\Setting;
use Carbon\Carbon;
use Froiden\Envato\Traits\AppBoot;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;

class PulseController extends AdminBaseController
{
    use Notifications;
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.pulse';
        $this->pageIcon = 'icon-calender';
        $this->activeMenu = 'pms';
    }
    public function index(){
        $user = $this->user;
        $this->userList = User::where('company_id',$user->company_id)->get();
        return view('pulse.pulse', $this->data);
    }
    public function mentionsindex(){
        $user = $this->user;
        $this->userList = User::where('company_id',$user->company_id)->get();
        return view('pulse.mentions', $this->data);
    }
    public function projectPulseindex(){
        $user = $this->user;
        $this->userList = User::where('company_id',$user->company_id)->get();
        $prarray = array_filter(explode(',',$user->projectlist));
         $projectsarray = Project::whereIn('id', $prarray)->get();
        $this->projectList = $projectsarray;
        return view('pulse.project-pulse', $this->data);
    }
    public function data(Request $request){
        $user = $this->user;
        $count = 20;
        $skip = 0;
        $page = !empty($request->page) ? $request->page : 0;
        if($page){
            $skip = $page*$count;
        }
        $prarray = array_filter(explode(',',$user->projectlist));
        if (!empty($projectid)) {
            $projectsarray = ProjectsLogs::where('project_id', $projectid);
        }else{
            $projectsarray = ProjectsLogs::whereIn('project_id',$prarray);
        }
        $dontsohw = array('projects','project_category','chat');
        $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
        $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
        $dataarray['projectsarray'] = $projectsarray;
        $messageview = View::make('pulse.pulseloop',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function mentionsData(Request $request){

        $user = $this->user;
        $count = 20;
        $skip = 0;
        $page = !empty($request->page) ? $request->page : 0;
        if($page){
            $skip = $page*$count;
        }
        $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
        $pm = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
        $prarray = array_filter(array_merge($project,$pm));
        if (!empty($projectid)) {
            $projectsarray = ProjectsLogs::where('project_id', $projectid)->where('mentionusers','<>','')->whereRaw('FIND_IN_SET(?,mentionusers)', [$user->id]);
        }else{
            $projectsarray = ProjectsLogs::whereIn('project_id',$prarray)->where('mentionusers','<>','');
        }
        $dontsohw = array('projects','project_category','chat');
        $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
        $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
        $dataarray['projectsarray'] = $projectsarray;
        $messageview = View::make('pulse.mentionloop',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;

    }

}
