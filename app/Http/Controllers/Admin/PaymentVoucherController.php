<?php

namespace App\Http\Controllers\Admin;


use App\AccountPayments;
use App\Helper\Reply;
use App\IncomeExpenseHead;
use Google\Service\Adsense\Resource\AccountsPayments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class PaymentVoucherController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Payment Voucher';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = Transaction::class;
    public $parentRoute = 'admin.payment_voucher';
    public $parentView = "admin.payment-voucher";
    public $voucher_type = "JV";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $this->paymentsarray  =  AccountPayments::where('company_id',$user->company_id)->get();
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->user;
        $this->defaultledgers  =  IncomeExpenseHead::where('company_id',$user->company_id)->where('default','1')->get();
        $this->ledgersarray  =  IncomeExpenseHead::where('company_id',$user->company_id)->where('default','0')->get();
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = $this->user;
        $request->validate([
            'ledger_from' => 'required',
            'ledger_to' => 'required',
            'amount' => 'required',
            'type' => 'required',
        ]);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $user = $this->user;
        $accountspayments = new AccountPayments();
        $accountspayments->company_id = $user->company_id;
        $accountspayments->ledger_from = $request->ledger_from;
        $accountspayments->ledger_to = $request->ledger_to;
        $accountspayments->amount = $request->amount;
        $accountspayments->type = $request->type;
        $accountspayments->voucher_date = $voucher_date;
        $accountspayments->particulars = $request->particulars;
        $accountspayments->created_by = $user->id;
        $accountspayments->save();

        return Reply::redirect(route($this->parentRoute.'.ledgerlog'),__('Payment Successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $id = $request->id;
        $item = AccountPayments::where('id', '=', $id)->first();
        if (empty($item)) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute.'.ledgerlog');
        }
        $this->payment = $item;
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user;
        $this->items =  AccountPayments::find($id);
        if(empty($this->items)){
            return redirect()->route($this->parentRoute);
        }

        $this->defaultledgers  =  IncomeExpenseHead::where('company_id',$user->company_id)->where('default','1')->get();
        $this->ledgersarray  =  IncomeExpenseHead::where('company_id',$user->company_id)->where('default','0')->get();
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $request->validate([
            'ledger_from' => 'required',
            'ledger_to' => 'required',
            'amount' => 'required',
            'type' => 'required',
        ]);
        $date = new \DateTime($request->voucher_date);
        $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
        $user = $this->user;
        $accountspayments = AccountPayments::find($id);
        $prevamount = $accountspayments->amount;

        $accountspayments->company_id = $user->company_id;
        $accountspayments->ledger_from = $request->ledger_from;
        $accountspayments->ledger_to = $request->ledger_to;
        $accountspayments->amount = $request->amount;
        $accountspayments->type = $request->type;
        $accountspayments->voucher_date = $voucher_date;
        $accountspayments->particulars = $request->particulars;
        $accountspayments->created_by = $user->id;
        $accountspayments->save();

        return Reply::redirect(route($this->parentRoute),__('Payment Successfully Updated'));
    }

    public function pdf(Request $request)
    {
        $id = $request->id;
        $item = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->get();
        if (count($item) == 0) {
            Session::flash('error', "Item not found");
            return redirect()->route($this->parentRoute);
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format') . ' h:i:s');
        $extra = array(
            'current_date_time' => $date,
            'module_name' => 'Payment Voucher Report',
            'voucher_type' => 'JOURNAL VOUCHER'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items' => $item, 'extra' => $extra])->setPaper('a4', 'landscape');
         return $pdf->download($extra['current_date_time'] . '_' . $extra['module_name'] . '.pdf');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = AccountPayments::find($id);
        $items->delete();
        return Reply::success(__('Payment Voucher Deleted'));
    }


    public function restore($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->onlyTrashed()
            ->get();
        foreach ($items as $item) {
            $item->restore();
            $item->updated_by = \Auth::user()->id;
            $item->save();
        }
        return Reply::success(__('Successfully Restored'));
    }

    public function kill($id)
    {
        $items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->withTrashed()
            ->where(function ($q) use ($id) {
                $q->where('voucher_no', '=', $id);
            })
            ->withTrashed()
            ->get();
        foreach ($items as $item) {
            $item->forceDelete();
        }
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $search = $request["search"];
        $this->items = Transaction::where('voucher_type', '=', $this->voucher_type)
            ->onlyTrashed()
            ->where(function ($q) use ($search) {
                $q->where('voucher_no', '=', $search)
                    ->orWhere('voucher_date', 'like', date("Y-m-d", strtotime($search)))
                    ->orWhere('dr', '=', $search)
                    ->orWhere('cr', '=', $search)
                    ->orWhere('particulars', 'like', '%' . $search . '%')
                    ->orWhereHas('BankCash', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('IncomeExpenseHead', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    })
                    ->orWhereHas('Branch', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
            })
            ->onlyTrashed()
            ->orderBy('created_at', 'desc')
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->destroy($id);
            }
            return redirect()->back();
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->withTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->withTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $items = Transaction::where('voucher_type', '=', $this->voucher_type)
                    ->onlyTrashed()
                    ->where(function ($q) use ($id) {
                        $q->where('voucher_no', '=', $id);
                    })
                    ->onlyTrashed()
                    ->get();
                if (count($items) < 1) {
                    continue;
                }
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function ledgerLog(Request $request){
        $user = $this->user;
        $this->ledgersarray  =  IncomeExpenseHead::where('company_id',$user->company_id)->get();
        $ledgerfromarray = $request->ledgerfrom;
        $startdate = !empty($request->startdate) ? date('Y-m-d',strtotime($request->startdate)) : '';
        $enddate = !empty($request->enddate) ? date('Y-m-d',strtotime($request->enddate)) : '';
        $this->openingbalancearray  = array();
        $this->paymentsarray  = array();
        $this->sumamount = 0;
        $this->runningbalance = 0;
        $this->paymentdate = $startdate;
        $inspectionarray = array();
        if(!empty($ledgerfromarray)||(!empty($startdate)&&!empty($enddate))){
            $ledgerinfo = IncomeExpenseHead::select('name','amount','id','voucherdate')->where('company_id',$user->company_id)->whereIn('id',$ledgerfromarray)->get()->toArray();
            $inspectionarray = AccountPayments::where('company_id',$user->company_id);
            if(!empty($ledgerfromarray)){
                $inspectionarray = $inspectionarray->whereIn('ledger_from',$ledgerfromarray);
            }
            if(!empty($startdate)){
                $inspectionarray = $inspectionarray->where('voucher_date','>=',$startdate);
            }
            if(!empty($enddate)){
                $inspectionarray = $inspectionarray->where('voucher_date','<=',$enddate);
            }
            $inspectionarray = $inspectionarray->orderBy('voucher_date','asc')->get();
            $this->openingbalancearray  = $ledgerinfo;
            $this->paymentsarray  = $inspectionarray;
            $recurring = array();
            $recurring['startdate'] = $startdate;
            $recurring['enddate'] = $enddate;
            $recurring['ledgerids'] = $ledgerfromarray;
            $this->runningbalance = running_balance($recurring);
        }
        return view($this->parentView . '.ledgerlog', $this->data);
    }
    public function ledgerLogData(Request $request){
        $user = $this->user;
        $ledgerfrom = $request->ledgerfrom;
        $startdate = !empty($request->startdate) ? date('Y-m-d') : '';
        $enddate = !empty($request->enddate) ? date('Y-m-d') : '';
        $inspectionarray = array();
        if(!empty($ledgerfrom)||(!empty($startdate)&&!empty($enddate))){
            $inspectionarray = AccountPayments::where('company_id',$user->company_id);
            if(!empty($ledgerfrom)){
                $inspectionarray = $inspectionarray->where('ledger_from',$ledgerfrom);
            }
            if(!empty($startdate)){
                $inspectionarray = $inspectionarray->where('voucher_date','>=',$startdate);
            }
            if(!empty($enddate)){
                $inspectionarray = $inspectionarray->where('voucher_date','<=',$enddate);
            }
            $inspectionarray = $inspectionarray->orderBy('voucher_date','asc')->get();
        }

        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->editColumn(
                'ledger_from',
                function ($row) {
                    return !empty($row->ledgerfrom) ? $row->ledgerfrom->name : '';
                }
            )
            ->editColumn(
                'ledger_to',
                function ($row) {
                    return !empty($row->ledgerto) ? $row->ledgerto->name : '';
                }
            )
            ->editColumn(
                'voucher_date',
                function ($row) {
                    return !empty($row->voucher_date) ? date('d M Y',strtotime($row->voucher_date)) : '';
                }
            )
            ->editColumn(
                'particulars',
                function ($row) {
                    $htmlcontent = '';
                    if($row->created_by){
                        $htmlcontent .= 'Created By : '.get_user_name($row->created_by);
                        $htmlcontent = '</br>';
                    }
                    if($row->particulars){
                        $htmlcontent .= 'Remark : '.$row->particulars;
                    }
                    return $htmlcontent;
                }
            )
            ->editColumn(
                'dr',
                function ($row) {
                    if($row->type=='dr'){
                        return $row->amount;
                    }
                }
            )
            ->editColumn(
                'cr',
                function ($row) {
                    if($row->type=='cr'){
                        return $row->amount;
                    }
                }
            )
            ->editColumn(
                'balance',
                function ($row) {
                    return $row->balance;
                }
            )
            ->rawColumns([ 'ledger_from','ledger_to', 'voucher_date', 'particulars','dr', 'cr', 'balance'])
            ->make(true);
    }
}
