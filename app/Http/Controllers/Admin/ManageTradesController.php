<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\SubAsset;


use App\Location;
use App\Trade;
use Illuminate\Http\Request;

class ManageTradesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Trades';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }
    public function index()
    {
        $companyid = $this->user->company_id;
        $this->trades = Trade::where('company_id',$companyid)->get();
        return view('admin.trades.index', $this->data);
    }


    public function store(Request $request)
    {
        $trade = new Trade();
        $trade->company_id = $this->user->company_id;
        $trade->title = $request->title;
        $trade->symbol = $request->symbol;
        $trade->save();
        return Reply::success(__('messages.tradeAdded'));
    }

    public function edit($id)
    {
        $this->trade = Trade::find($id);
        $this->status='success';
        return view('admin.trades.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $trade = Trade::find($id);
        $trade->company_id = $this->user->company_id;
        $trade->title = $request->title;
        $trade->symbol = $request->symbol;
        $trade->save();

        return Reply::success(__('messages.tradeUpdate'));
    }
    public function updateSubAsset(Request $request,$id)
    {
        $category = SubAsset::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->asset_id = $request->asset_id;
        $category->status == 'success';
        $category->save();

        return Reply::success(__('messages.assetUpdate'));
    }

    public function destroy($id)
    {
        Trade::destroy($id);
        $categoryData = Trade::all();
        return Reply::successWithData(__('messages.tradeDelete'),['data' => $categoryData]);
    }
}
