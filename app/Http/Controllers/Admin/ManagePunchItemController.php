<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\Tasks\StoreTask;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskReminder;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectsLogs;
use App\PunchItem;
use App\PunchItemFiles;
use App\PunchItemReply;
use App\Rfi;
use App\Segment;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\EmployeeDocs;
use App\Http\Requests\EmployeeDocs\CreateRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ManagePunchItemController extends AdminBaseController
{
    use ProjectProgress;

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.issue';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('tasks', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $userdetails = $this->user;
        $this->user = $userdetails;
        $this->projectlist = $this->projectslist;
        $this->clients = Employee::getAllClients($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        $this->taskBoardStatus = TaskboardColumn::all();
        $this->items = PunchItem::where('company_id', $userdetails->company_id)->get();
        return view('admin.punch-items.index', $this->data);
    }

    public function issueData(Request $request)
    {
        $user = $this->user;
        $assignid = $request->user;
        $selectdate = $request->selectdate;
        $status = $request->status;
        $project = $request->project;
        $projectlist = explode(',', $user->projectlist);
        $inspectionarray = PunchItem::query();
        if (!empty($selectdate)) {
            $inspectionarray = $inspectionarray->where('due_date', '<=', $selectdate);
        }
        if(!empty($project)){
                $inspectionarray = $inspectionarray->where('projectid',$project);
            }elseif(!empty($projectlist)){
            $inspectionarray = $inspectionarray->whereIn('projectid',$projectlist);
        }
        if (!empty($status)) {
            $inspectionarray = $inspectionarray->where('status', $status);
        }
        if (!empty($assignid)) {
            $inspectionarray = $inspectionarray->whereRaw('FIND_IN_SET(?,assign_to)', [$assignid]);
        }

        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a href="' . route('admin.issue.reply', $row->id) . '" class="btn btn-info btn-circle"
                           data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                if ($row->added_by == $user->id) {
                    $actionlink .= '<a href="' . route('admin.issue.edit', [$row->id]) . '" data-cat-id="' . $row->id . '" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                    $actionlink .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                           data-toggle="tooltip" data-task-id="' . $row->id . '" data-original-title="Delete">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>';
                }
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'start_date',
                function ($row) {
                    return Carbon::parse($row->start_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'due_date',
                function ($row) {
                    return Carbon::parse($row->due_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'private',
                function ($row) {
                    return !empty($row->private) ? 'Private' : 'Public';
                }
            )
            ->editColumn(
                'assign_to',
                function ($row) {
                    $dis = '';
                    $distributionarra = explode(',', $row->assign_to);
                    foreach ($distributionarra as $distribut) {
                        $dis .= '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">' . ucwords(get_users_employee_name($distribut,$row->company_id)) . '</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'distribution',
                function ($row) {
                    $dis = '';
                    $distributionarra = explode(',', $row->distribution);
                    foreach ($distributionarra as $distribut) {
                        $dis .= '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">' . ucwords(get_users_employee_name($distribut,$row->company_id)) . '</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'projectid',
                function ($row) {
                    return  get_project_name($row->projectid);
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">' . ucwords(get_user_name($row->added_by)) . '</a></div></div>';
                }
            )
            ->rawColumns([ 'action','start_date', 'due_date', 'added_by','projectid', 'assign_to', 'distribution'])
            ->rawColumns(['action', 'start_date', 'due_date', 'added_by', 'assign_to', 'distribution'])
            ->make(true);
    }
    public function edit($id)
    {
        $punchitem = PunchItem::findOrFail($id);
        if (empty($punchitem)) {
            return redirect(route('admin.punch-item.index'));
        }
        $userdetails = $this->user;
        $this->user = $userdetails;
        $this->projectlist = $this->projectslist;
        $this->clients = Employee::getAllClients($userdetails, $punchitem->projectid);
        $this->employees = Employee::getAllEmployees($userdetails, $punchitem->projectid);
        $this->punchitem = $punchitem;
        $this->titlelist = Title::where('project_id', $punchitem->projectid)->get();
        $this->segmentlist = Segment::where('projectid', $punchitem->projectid)->where('titleid', $punchitem->titleid)->get();
        $this->costitemlist = ProjectCostItemsProduct::join('cost_items', 'cost_items.id', '=', 'project_cost_items_product.cost_items_id')
            ->where('project_cost_items_product.project_id', $punchitem->projectid)
            ->where('project_cost_items_product.title', $punchitem->titleid)
            ->orderBy('project_cost_items_product.inc','asc')->pluck('cost_items.cost_item_name', 'project_cost_items_product.id');
        $this->activitiesarray = !empty($punchitem->projectid) ? ProjectCostItemsPosition::where('project_id', $punchitem->projectid)->where('title', $punchitem->titleid)->where('position', 'row')->orderBy('inc','asc')->get() : array();
        $this->files = PunchItemFiles::where('task_id', $id)->get();
        return view('admin.punch-items.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $user = $this->user;

        $task = PunchItem::findOrFail($id);
//        $task = new PunchItem();
        $task->title = $request->title;
        if ($request->description != '') {
            $task->description = $request->description;
        }
        $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');

        $task->assign_to = $request->assign_to;
        $task->priority = $request->priority;
        $task->status = $request->status;
        $task->type = $request->type;
        if ($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        if ($request->private != '') {
            $task->private = $request->private;
        }
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;
        $task->schimpact = $request->schimpact ?: '';
        $task->schimpact_days = $request->schimpact_days ?: '';
        $task->costimpact = $request->costimpact ?: '';
        $task->costimpact_days = $request->costimpact_days ?: '';
        $task->projectid = $request->project_id ?: 0;
        $task->titleid = $request->subproject_id ?: 0;
        $task->segmentid = $request->segment_id ?: 0;
        $task->costitemid = $request->costitem ?: '';
        $task->activity_id = $request->activity ?: '';
        $task->save();

        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $task->id;
        $createlog->module = 'punch_item';
        $createlog->project_id = $task->projectid ?: '';
        $createlog->subproject_id = $task->titleid ?: '';
        $createlog->segment_id = $task->segmentid ?: '';
        $createlog->heading = 'Issue updated for ' . $task->title;
        $createlog->description = ' Issue Raised by ' . $user->name . ' with title ' . $task->title . ' for the project ' . get_project_name($task->projectid);
        $createlog->medium = 'web';
        $createlog->save();
        return Reply::dataOnly(['taskID' => $task->id]);
    }

    public function destroy(Request $request, $id)
    {
        $task = PunchItem::findOrFail($id);
        $task->delete();
        return Reply::success(__('Punch item deleted successfully'));
    }

    public function create()
    {
        $userdetails = $this->user;
        $this->user = $userdetails;
        $this->projectlist = $this->projectslist;
        $this->clients = Employee::getAllClients($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        return view('admin.punch-items.create', $this->data);
    }

    public function membersList($projectId)
    {
        $this->members = ProjectMember::byProject($projectId);
        $list = view('admin.punch-items.members-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function dependentTaskLists($projectId, $taskId = null)
    {
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if ($completedTaskColumn) {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('project_id', $projectId);

            if ($taskId != null) {
                $this->allTasks = $this->allTasks->where('id', '!=', $taskId);
            }

            $this->allTasks = $this->allTasks->get();
        } else {
            $this->allTasks = [];
        }

        $list = view('admin.punch-items.dependent-task-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            ['title' => 'required',
                'status' => 'required',
                'start_date' => 'required',
                'due_date' => 'required',
                'project_id' => 'required',
                'activity' => 'required',
                'costitem' => 'required'],

            ['title.required' => 'Title name feild is required',
                'status.required' => 'Status feild is required',
                'start_date.required' => 'Start date feild is required',
                'due_date.required' => 'Due date feild is required',
                'project_id.required' => 'Project feild is required',
                'activity.required' => 'Activity feild is required',
                'costitem.required' => 'Task feild is required']
        );

        $user = $this->user;
        $projectdetails = Project::find($request->project_id);
        $task = new PunchItem();
        $task->company_id = $projectdetails->company_id;
        $task->title = $request->title;
        if ($request->description != '') {
            $task->description = $request->description;
        }
        $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');

        $task->assign_to = $request->assign_to;
        $task->priority = $request->priority;
        $task->status = $request->status;
        $task->type = $request->type;
        if ($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        if ($request->private != '') {
            $task->private = $request->private;
        }
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;
        $task->schimpact = $request->schimpact ?: '';
        $task->schimpact_days = $request->schimpact_days ?: '';
        $task->costimpact = $request->costimpact ?: '';
        $task->costimpact_days = $request->costimpact_days ?: '';
        if (!empty($request->task_id)) {
            $taskdetails = Task::find($request->task_id);
            $task->task_id = $taskdetails->id ?: 0;
            $task->projectid = $taskdetails->project_id ?:  '0';
            $task->titleid = $taskdetails->title_id ?: '0';
            $task->segmentid = $taskdetails->segment_id ?: '0';
            $task->costitemid = $taskdetails->costitem ?:  '0';
        } else {
            $task->projectid = $request->project_id ?: '0';
            $task->titleid = $request->subproject_id ?: '0';
            $task->segmentid = $request->segment_id ?: '0';
            $task->costitemid = $request->costitem ?:  '0';
            $task->activity_id = $request->activity ?: '0';
        }
        $task->save();

        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $task->id;
        $createlog->module = 'punch_item';
        $createlog->project_id = $task->projectid ?: '';
        $createlog->subproject_id = $task->titleid ?: '';
        $createlog->segment_id = $task->segmentid ?: '';
        $createlog->heading = 'Issue created for ' . $task->title;
        $createlog->description = ' Issue Raised by ' . $user->name . ' with title ' . $task->title . ' for the project ' . get_project_name($task->projectid);
        $createlog->medium = 'web';
        $createlog->save();

        return Reply::dataOnly(['taskID' => $task->id]);
    }

    public function ajaxCreate($columnId)
    {
        $this->projects = Project::all();
        $this->columnId = $columnId;
        $this->employees = User::allEmployees();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if ($completedTaskColumn) {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)->get();
        } else {
            $this->allTasks = [];
        }
        return view('admin.punch-items.ajax_create', $this->data);
    }

    public function remindForTask($taskID)
    {
        $task = Task::with('user')->findOrFail($taskID);

        // Send  reminder notification to user
        $notifyUser = $task->user;
        $notifyUser->notify(new TaskReminder($task));

        return Reply::success('messages.reminderMailSuccess');
    }

    public function show($id)
    {
        $this->task = Task::with('board_column')->findOrFail($id);
        $view = view('admin.punch-items.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function showFiles($id)
    {
        $this->taskFiles = TaskFile::where('task_id', $id)->get();
        return view('admin.punch-items.ajax-file-list', $this->data);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $projectId
     * @param $hideCompleted
     */
    public function export($startDate, $endDate, $projectId, $hideCompleted)
    {

        $tasks = PunchItem::join('users', 'users.id', '=', 'punch_item.assign_to')
            ->select('punch_item.*', 'users.name', 'users.image');

//        $tasks->where(function ($q) use ($startDate, $endDate) {
//            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);
//
//            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
//        });
//
//        if ($projectId != 0) {
//            $tasks->where('tasks.project_id', '=', $projectId);
//        }
//
//        if ($hideCompleted == '1') {
//            $tasks->where('tasks.status', '=', 'incomplete');
//        }

        $attributes = ['image', 'due_date'];

        $tasks = $tasks->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project', 'Title', 'Assigned To', 'Status', 'Due Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($tasks as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold' => true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $storage = storage();
            $company = $this->user->company_id;
            $punchitem = PunchItem::find($request->task_id);
            foreach ($request->file as $fileData) {
                $file = new PunchItemFiles();
                $file->company_id = $punchitem->company_id;
                $file->user_id = $this->user->id;
                $file->task_id = $punchitem->id;
                $file->reply_id = $request->reply_id ?: 0;
                switch ($storage) {
                    case 'local':
                        $destinationPath = 'uploads/punch-files/' . $file->task_id;
                        if (!file_exists('' . $destinationPath)) {
                            mkdir('' . $destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('/punch-files/' . $file->task_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'punch-files')
                            ->first();

                        if (!$dir) {
                            Storage::cloud()->makeDirectory('punch-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if (!$directory) {
                            Storage::cloud()->makeDirectory($dir['path'] . '/' . $request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('punch-files/' . $request->task_id . '/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/punch-files/' . $request->task_id . '/' . $fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }
        }
        return Reply::success("Issue updated successfully");
    }

    public function destroyImage(Request $request, $id)
    {
        $file = PunchItemFiles::findOrFail($id);
        $storage = storage();
        $company = company()->id;
        switch ($storage) {
            case 'local':
                File::delete('uploads/punch-files/' . $id . '/' . $file->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete($company . '/punch-files/' . $id . '/' . $file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('punch-files/' . $id . '/' . $file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('punch-files/' . $id . '/' . $file->filename);
                break;
        }
        $file->delete();
        return Reply::successWithData(__('messages.fileDeleted'));
    }

    public function removeFile($id)
    {

        $file = PunchItemFiles::findOrFail($id);
        $storage = storage();
        $company = company()->id;
        switch ($storage) {
            case 'local':
                File::delete('uploads/punch-files/' . $id . '/' . $file->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete($company . '/punch-files/' . $id . '/' . $file->hashname);
                break;
            case 'google':
                Storage::disk('google')->delete('punch-files/' . $id . '/' . $file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('punch-files/' . $id . '/' . $file->filename);
                break;
        }
        $file->delete();
        return Reply::success(__('image deleted successfully'));
    }

    public function reply($id)
    {
        $userdetails = $this->user;
        $projectlist = explode(',', $userdetails->projectlist);
        $this->user = $userdetails;
        $this->projectlist = Project::whereIn('id', $projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        $this->punchitem = PunchItem::findOrFail($id);
        $this->files = PunchItemFiles::where('task_id', $id)->where('reply_id', '0')->get();
        $this->replies = PunchItemReply::where('punch_item_id', $id)->get();
        return view('admin.punch-items.reply', $this->data);
    }

    public function comment($id)
    {
        $userdetails = $this->user;
        $this->punchitem = PunchItem::findOrFail($id);
        $this->employees = Employee::getAllEmployees($userdetails);
        $this->files = PunchItemFiles::where('task_id', $id)->get();
        $this->replies = PunchItemReply::where('punch_item_id', $id)->get();
        return view('admin.punch-items.comment', $this->data);
    }

    public function replyPost(Request $request, $id)
    {
        if(empty($request->comment)){
            return Reply::error('Please fill Comment');
        }
        $user = $this->user;

        $mentionusers = !empty($request->mentionusers) ? array_unique(array_filter(explode(',', $request->mentionusers))) : null;
        $punchitem = PunchItem::find($id);
        $punchitem->status = $request->status ?: 'open';
        $punchitem->save();

        $pi = new PunchItemReply();
        $pi->comment = $request->comment;
        $pi->punch_item_id = $punchitem->id;
        $pi->added_by = $this->user->id;
        $pi->mentionusers = !empty($mentionusers) ? implode(',', $mentionusers) : null;
        $pi->save();

        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $pi->id;
        $createlog->module = 'punch_item';
        $createlog->project_id = $punchitem->project_id ?: '';
        $createlog->subproject_id = $punchitem->title ?: '';
        $createlog->segment_id = $punchitem->segment ?: '';
        $createlog->heading = 'Issue updated for ' . $punchitem->title;
        $createlog->description = ' Issue Raised by ' . $user->name . ' with title ' . $punchitem->title . ' for the project ' . get_project_name($punchitem->projectid);
        $createlog->medium = 'web';
        $createlog->mentionusers = !empty($mentionusers) ? implode(',', $mentionusers) : null;
        $createlog->save();

        return Reply::dataOnly(['taskID' => $punchitem->id, 'replyID' => $pi->id]);
    }

    public function commentPost(Request $request, $id)
    {
        $pi = new PunchItemReply();
        $pi->comment = $request->comment;
        $pi->punch_item_id = $id;
        $pi->added_by = $this->user->id;
        $pi->save();
        return Reply::success(__('Comment added successfully'));
    }
}
