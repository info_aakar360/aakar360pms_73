<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\Http\Requests\Units\StoreUnitRequest;
use App\Http\Requests\UnitStore\StoreUnitsrequest;
use App\SubAsset;


use App\Location;
use App\Type;
use App\Units;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class ManageUnitsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Units';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    public function index()
    {
        $user = $this->user;
        $this->unitsarray = Units::where('company_id', $user->company_id)->get();
        return view('admin.units.index', $this->data);
    }


    public function store(StoreUnitRequest $request)
    {

        $user = $this->user;
        $type = new Units();
        $type->company_id = $user->company_id;
        $type->name = $request->name;
        $type->symbol = $request->symbol;
        $type->measuretype = $request->measuretype;
        $type->save();
        return Reply::success(__('messages.unitAdded'));
    }

    public function edit($id)
    {
        $this->type = Units::find($id);
        $this->status = 'success';
        return view('admin.units.edit', $this->data);
    }

    public function update(StoreUnitRequest $request, $id)
    {
        $user = $this->user;
        $type = Units::find($id);
        $type->company_id = $user->company_id;
        $type->name = $request->name;
        $type->symbol = $request->symbol;
        $type->measuretype = $request->measuretype;
        $type->save();

        return Reply::success(__('messages.unitUpdate'));
    }

    public function destroy($id)
    {
        Units::destroy($id);
        return Reply::success(__('messages.unitDeleted'));
    }

    public function show($id)
    {

    }
}
