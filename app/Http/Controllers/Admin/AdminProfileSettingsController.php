<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminProfileSettingsController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-user';
        $this->pageTitle = 'app.menu.profileSettings';
    }

    public function index(){
        $this->userDetail = $this->user;
        return view('admin.profile.index', $this->data);
    }
    public function update(Request $request, $id) {

        $storage = storage();
        $password = trim($request->password);
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->gender = $request->input('gender');
        if(!empty($password)){
            $user->password = bcrypt($password);
        }
        $user->mobile = $request->input('mobile');
        $user->address = $request->input('address');

        if ($request->hasFile('image')) {
            if(!empty($user->image)){
                if (file_exists('uploads/avatar/'.$user->image)) {
                    unlink('uploads/avatar/'.$user->image);
                  }
            }
            $user->image = $request->image->hashName();
            $image = $request->image;
            $destinationPath = 'uploads/avatar/';
            $img = Image::make($image->getRealPath());
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$user->image);
        }

        $user->save();
        return Reply::success( __('messages.profileUpdated'));
    }
}
