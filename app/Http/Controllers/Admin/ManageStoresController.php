<?php

namespace App\Http\Controllers\Admin;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\ProductIssue;
use App\Project;
use App\PurchaseInvoice;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\Store;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageStoresController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.stores';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'store';
        $this->middleware(function ($request, $next) {
            if (!in_array('stores', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $company_id = Auth::user()->company_id;
        $this->stores = Store::where('added_by', $company_id)->get();
        if($company_id == '' || $company_id == null){
            $this->stores = Store::all();
        }
        $this->totalStores = count($this->stores);
        return view('admin.stores.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $this-> projects = Project::whereIn('id', $projecta)->get();
        return view('admin.stores.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStoreRequest $request)
    {
        //dd($request);
        $supplier = new Store();
        $supplier->company_name = $request->company_name;
        $supplier->website = $request->website;
        $supplier->contact_person = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->mobile;
        $supplier->address = $request->address;
        $supplier->gst_no = $request->gst_number;
        $supplier->note = $request->note;
        $company_id = $this->user->company_id;
        $supplier->added_by = $company_id;
        $supplier->save();
        return Reply::redirect(route('admin.stores.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->supplier = Store::find($id);
        $user = $this->user;
        $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
        $prarray = array_filter(explode(',',$user->projectlist));
        $this->projects = Project::whereIn('id', $prarray)->get();
        return view('admin.stores.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStoreRequest $request, $id)
    {
        $supplier = Store::find($id);
        $supplier->company_name = $request->company_name;
        $supplier->website = $request->website;
        $supplier->contact_person = $request->name;
        $supplier->email = $request->email;
        $supplier->phone = $request->mobile;
        $supplier->address = $request->address;
        $supplier->gst_no = $request->gst_number;
        $supplier->note = $request->note;
        $supplier->save();

        return Reply::redirect(route('admin.stores.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user;
        $projectde = Indent::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please delete Indent Information.';
        }
        $projectde = Rfq::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please delete Rfq first.';
        }
        $rfq  = Rfq::join('stores', 'stores.id', '=', 'rfqs.store_id')->where('project_id', $id)->where('store_id', '0')->first();
        $projectde = Quotes::where('rfq_id',$rfq->id)->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Quotation first.';
        }
        $projectde  = PurchaseOrder::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Order first.';
        }
        $projectde  = PurchaseInvoice::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Invoice first.';
        }
        $projectde  = ProductReturns::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Retrun first.';
        }
        $projectde  = ProductIssue::where('project_id', $id)->where('store_id', '0')->first();
        if (!empty($projectde->id)) {
            $errormsg = 'Please Remove Purchase Issue first.';
        }
        if(!empty($errormsg)){
            return Reply::error($errormsg);
        }else {
            Store::where('project_id', $id)->delete();
            return Reply::success(__('messages.storeDeleted'));
        }
    }

    public function data(Request $request, $id = false)
    {
        $user = $this->user;
        $projecta = explode(',',$user->projectlist);
        $users = Store::whereIn('project_id', $projecta);
        if ($request->name != 'all' && $request->name != '') {
            $users = $users->where('stores.company_name', $request->name);
        }if ($request->contact != 'all' && $request->contact != '') {
            $users = $users->where('stores.contact_person', $request->contact);
        }if ($request->number != 'all' && $request->number != '') {
            $users = $users->where('stores.phone', $request->number);
        }
        $users = $users->get();
        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                if($row->store_type !== 1){
                    return '<a href="' .route('admin.stores.edit',$row->id). '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }else{
                    return '<label class="label label-success">Default </label>';
                }
            })
            ->editColumn(
                'company_name',
                function ($row) {
                    return ucfirst($row->company_name);
                })->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )->editColumn(
                'phone',
                function ($row) {
                    return $row->phone;
                }
            )->editColumn(
                'contact_person',
                function ($row) {
                    return $row->contact_person;
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['company_name', 'action', 'status'])
            ->make(true);
    }

    public function export($status, $client)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'client')
            ->where('roles.company_id', company()->id)
            ->leftJoin('client_details', 'users.id', '=', 'client_details.user_id')
            ->select(
                'users.id',
                'client_details.name',
                'client_details.email',
                'client_details.mobile',
                'client_details.company_name',
                'client_details.address',
                'client_details.website',
                'client_details.created_at'
            )
            ->where('client_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($client != 'all' && $client != '') {
            $rows = $rows->where('users.id', $client);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('clients', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('clients file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
