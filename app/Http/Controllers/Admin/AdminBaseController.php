<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Company;
use App\EmailNotificationSetting;
use App\Employee;
use App\GdprSetting;
use App\Helper\Reply;
use App\LanguageSetting;
use App\ModuleSetting;
use App\Notification;
use App\Notifications\LicenseExpire;
use App\Package;
use App\PackageSetting;
use App\Project;
use App\ProjectActivity;
use App\PushNotificationSetting;
use App\StickyNote;
use App\Store;
use App\Traits\FileSystemSettingTrait;
use App\UniversalSearch;
use App\UserActivity;
use App\UserChat;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\ThemeSetting;
use Illuminate\Support\Facades\Auth;
use App\GlobalSetting;
use Illuminate\Support\Facades\Redirect;

class AdminBaseController extends Controller
{
    use FileSystemSettingTrait;

    /**
     * @var array
     */
    public $data = [];

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }
    /**
     * UserBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // Inject currently logged in user object into every view of user dashboard
        $this->middleware(function ($request, $next) {

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', Auth::user()->company_id)->first();
            $this->superadmin = GlobalSetting::with('currency')->first();
            $this->emailSetting = EmailNotificationSetting::all();
            $this->pushSetting = PushNotificationSetting::first();
            $this->companyName = $this->global->company_name;
            $this->companyid = $this->global->id;
            $this->storage = storage();
            $this->adminTheme = ThemeSetting::where('panel', 'admin')->first();
            $this->languageSettings = LanguageSetting::where('status', 'enabled')->get();
            $this->menutype = "development";
            App::setLocale($this->global->locale);
            Carbon::setLocale($this->global->locale);
            setlocale(LC_TIME, $this->global->locale . '_' . strtoupper($this->global->locale));
            $this->setFileSystemConfigs();
            $user = auth()->user();
            $this->user = $user;
            $projectlist  = explode(',',$user->projectlist);
            $projectarray = Project::whereIn('id',$projectlist)->get();
            $this->projectslist = $projectarray;
            $this->stores = Store::whereIn('project_id',explode(',',$this->user->projectlist))->get();
            //Auto Clock In / Clock Out Notification
            $rules = Employee::join('rules_users','rules_users.user_id','=','employee.id')
                ->join('attendance_rules','attendance_rules.id','=','rules_users.rules_id')->select('rules_users.user_id','attendance_rules.name','rules_users.effective_date')->first();
            if(!empty($rules)){
                $effective_date = Carbon::parse($rules->effective_date)->timezone($this->global->timezone);
                $now = Carbon::now()->timezone($this->global->timezone);
                if(!empty($effective_date) && $effective_date->lte($now) && !empty($rules->user_id)){
                    $attendances = Attendance::where('emp_id',$rules->user_id)->orWhere('user_id',$rules->user_id)->get();
                    foreach($attendances as $attendance){
                        $clockintime = Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->format('H:i:s');
                        $clockouttime = Carbon::parse($attendance->clock_out_time)->timezone($this->global->timezone)->format('H:i:s');
                        $start_datetime = new \DateTime(date('Y-m-d').' '.$clockintime);
                        $end_datetime = new \DateTime(date('Y-m-d').' '.$clockouttime);
                        $totaltime = $start_datetime->diff($end_datetime)->format('%h');
                        if($totaltime >= 10){
                            $this->notification = 'Logout Time'.$rules->user_id;
                        }
                    }
                }
            }
            //End Auto Clock In / Clock Out Notifications

            // For GDPR
            try {
                $this->gdpr = GdprSetting::first();
                $this->gdpr = GdprSetting::first();
                if (!$this->gdpr) {
                    $gdpr = new GdprSetting();
                    $gdpr->company_id = Auth::user()->company_id;
                    $gdpr->save();

                    $this->gdpr = $gdpr;
                }
            } catch (\Exception $e) {

            }
            $company = $this->global;
            $expireOn = $company->licence_expire_on;
            $currentDate = Carbon::now();

            if ((!is_null($expireOn) && $expireOn->lessThan($currentDate))) {
                $this->checkLicense($company);
            }

            $this->modules = $this->user->modules;
            $this->unreadMessageCount = UserChat::where('to', $this->user->id)->where('message_seen', 'no')->count();
            $this->unreadTicketCount = Notification::where('notifiable_id', $this->user->id)
                ->where('type', 'App\Notifications\NewTicket')
                ->whereNull('read_at')
                ->count();

            $this->unreadExpenseCount = Notification::where('notifiable_id', $this->user->id)
                ->where('type', 'App\Notifications\NewExpenseAdmin')
                ->whereNull('read_at')
                ->count();

            $this->unreadIssuesCount = Notification::where('notifiable_id', $this->user->id)
                ->where('type', 'App\Notifications\NewIssue')
                ->whereNull('read_at')
                ->count();

            $this->stickyNotes = StickyNote::where('user_id', $this->user->id)
                ->orderBy('updated_at', 'desc')
                ->get();

            if (config('filesystems.default') == 's3') {
                $this->url = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";
            }

            $this->menus = $this->getMenus();
            return $next($request);
        });
    }

    private function getMenus(){

        //PMS Menus
        $pms = array();
        if(in_array('projects', $this->user->modules)){
            $pms[] = array('title'=>__('app.menu.dashboard'),  'icon'=>'icon-speedometer','link'=>route('admin.dashboard'));
        }
        if(in_array('projects', $this->user->modules)){
            $submenu = array();
            /*$submenu[] = array('title'=>'Employees', 'link'=>route('admin.project-members.show','all'));*/

            $submenu[] = array('title' => __('app.menu.addprojects'), 'link' => route('admin.projects.index'));
            if(in_array('employees', $this->user->modules)){
                $submenu[] = array('title'=>'Roles n Permissions', 'link'=>route('admin.role-permission.employees'));
            }else  if(in_array('clients', $this->user->modules)){
                $submenu[] = array('title'=>'Roles n Permissions', 'link'=>route('admin.role-permission.clients'));
            }else if(in_array('contractors', $this->user->modules)){
                $submenu[] = array('title'=>'Roles n Permissions', 'link'=>route('admin.role-permission.contractors'));
            }
            if(in_array('activity', $this->user->modules)) {
                $submenu[] = array('title' =>__('app.activity'), 'link' => route('admin.activity.index'));
                $submenu[] = array('title' =>__('app.task'), 'link' => route('admin.task.index'));
               /* $submenu[] = array('title' =>__('app.menu.ratesheetresources'), 'link' => route('admin.resources.index'));*/
                $submenu[] = array('title' => __('app.units'), 'link' => route('admin.units.index'));

               /* $submenu[] = array('title' =>  __('app.menu.resourcetypes'), 'link' => route('admin.types.index'));*/
                /*$submenu[] = array('title' => 'Trade Master', 'link' => route('admin.trades.index'));*/
            }
            if(in_array('manpowerlog', $this->user->modules)) {
                $submenu[] = array('title' => 'Labour Attendance Category', 'link' => route('admin.manpower-category.index'));
            }
          /*  if(in_array('segment', $this->user->modules)) {
                $submenu[] = array('title' => 'Segments', 'link' => route('admin.segments.index'));
            }*/
            if(in_array('projects', $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.boqtemplate'), 'link' => route('admin.boq-template.index'));
                $submenu[] = array('title'=>__('app.menu.workweek'), 'link'=>route('admin.projectleaves.workweek'));
                $submenu[] = array('title'=>__('app.menu.holidays'), 'link'=>route('admin.projectholidays.holidays'));
            }

            $pms[] = array('title'=>__('app.menu.masters'), 'link'=>'#', 'icon'=>'icon-book-open', 'submenu'=>$submenu);
        }

        if(in_array('projects', $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title' => __('app.menu.boq'), 'link' => route('admin.projects.boq', 'all'));
          /*  if(in_array('segment', $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.segments'), 'link' => route('admin.projects.segments', 'all'));
            }*/
            if(in_array('boq', $this->user->modules)) {
               $submenu[] =array('title'=>__('app.menu.schedule'), 'link'=>route('admin.projects.scheduling','all'));
                $submenu[] = array('title' => __('app.menu.milestones'), 'link' => route('admin.milestone.show', 'all'));
                /*$submenu[] = array('title'=>__('app.menu.sourcing-packages'), 'link'=>route('admin.projects.sourcingPackages','all'));*/
                $submenu[] =  array('title'=>__('app.menu.changeorder').' (Amendments)', 'link'=>route('admin.changeorders.boq','all'));
                /*$submenu[] = array('title'=>'Take off', 'link'=>'#');*/
            }
            $pms[] = array('title'=>__('app.menu.projects'),'icon'=>'icon-graph', 'link'=>'#', 'submenu'=>$submenu);
        }

        if(in_array('projects', $this->user->modules)){
            $submenu = array();
            if(in_array("document", $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.documents'), 'icon'=>'icon-doc', 'link'=>route('admin.manage-files.index'));
            }
            if(in_array("drawing", $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.drawings'),'icon'=>'icon-docs', 'link'=>route('admin.manage-drawings.index'));
            }
            if(in_array("photo", $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.photos'),'icon'=>'icon-picture',  'link'=>route('admin.manage-photos.index'));
            }
            if(in_array('tasks', $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.tasksupdate'), 'link' => route('admin.all-tasks.index'));
            }
            if(in_array("punch_items", $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.issue'), 'link'=>route('admin.issue.index'));
            }
            if(in_array("rfi", $this->user->modules)){
                $submenu[] = array('title'=>'RFI', 'link'=>route('admin.rfi.index'));
            }
            if(in_array('inspection', $this->user->modules)){
                $submenu[] = array('title'=>'Inspection', 'link'=>route('admin.inspectionName.inspectionAssigned'));
                $submenu[] = array('title'=>'Observations', 'link'=>route('admin.observations.index'));
            }
            if(in_array('tasks', $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.calender'), 'link' => route('admin.taskscalender.index'));
                $submenu[] = array('title' => __('app.menu.todo'), 'link' => route('admin.todo.index'));
            }
           /* $submenu[] =  array('title'=>__('app.menu.timeLogs'), 'link'=>route('admin.all-time-logs.index'));*/
            if(in_array('manpowerlog', $this->user->modules)) {
                $submenu[] = array('title' =>__('app.menu.labourattendance'), 'link' => route('admin.man-power-logs.index'));
            }
            $submenu[] =  array('title'=>__('app.menu.weatherLogs'), 'link'=>route('admin.weather-logs.index'));

            $submenu[] = array('title'=>__('app.menu.meetings'), 'link'=>route('admin.meetings.index'));

           /* if(in_array("rfi", $this->user->modules)){
                $submenu[] = array('title'=>'Submittals', 'link'=>route('admin.submittals.index'));
            }*/
            $pms[] = array('title'=>'Manage', 'link'=>'#','icon'=>'icon-calender', 'submenu'=>$submenu);
        }

        if(in_array('contracts', $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>__('app.tenders'), 'link'=>route('admin.tenders.index'));
            $submenu[] = array('title'=>__('app.menu.awardedcontracts'), 'link'=>route('admin.awardedcontracts.awardedContracts'));

            /* if(in_array('invoices', $this->user->modules)){
                  $submenu[] = array('title'=>__('app.menu.invoices'), 'link'=>route('admin.invoices.show','all'));
              } */
            if(in_array("invoices", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.clientinvoices'), 'link' => route('admin.all-invoices.index'));
                $submenu[] = array('title' => __('app.menu.contractinvoices'), 'link' => route('admin.contract-invoices.index'));
            }
            $pms[] = array('title'=>'Contracts', 'link'=>'#', 'icon'=>'icon-tag', 'submenu'=>$submenu);
        }

        if(in_array('projects', $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>__('app.menu.progressReport'), 'link'=>route('admin.projects.progress-report'));
            $submenu[] = array('title'=>__('app.menu.labourattendanceReport'), 'link'=>route('admin.man-power-logs.man-report'));

            /* $submenu[] = array('title'=>__('app.menu.taskReport'), 'link'=>route('admin.task-report.index'));*/
            $pms[] = array('title'=>'Reports', 'link'=>'#','icon'=>'icon-calender',  'submenu'=>$submenu);
        }

        /* if(in_array('leads', $this->user->modules)){
             $pms[] = array('title'=>__('app.menu.lead'), 'link'=>route('admin.leads.index'));
         }*/


//        if(in_array("boq", $this->user->modules)){
//            $submenu = array();
//            $submenu[] = array('title'=>'Workforce', 'link'=>route('admin.product-type-workforce.workforce-index'));
//            $submenu[] = array('title'=>'Equipment', 'link'=>route('admin.product-type-equipment.equipment-index'));
//            $submenu[] = array('title'=>'Material', 'link'=>route('admin.product-type-material.material-index'));
//            $submenu[] = array('title'=>'Commitment', 'link'=>route('admin.product-type-commitment.commitment-index'));
//            $submenu[] = array('title'=>'Owner Cost', 'link'=>route('admin.product-type-owner-cost.owner-cost-index'));
//            $submenu[] = array('title'=>'Professional Services', 'link'=>route('admin.product-type-professional.professional-index'));
//            $submenu[] = array('title'=>'Other', 'link'=>route('admin.product-type-other.other-index'));
//            $pms[] = array('title'=>'Type', 'link'=>'#', 'submenu'=>$submenu);
//        }
        /*if(in_array("resources", $this->user->modules)){
            $pms[] = array('title'=>__('app.menu.resources'), 'link'=>route('admin.resources.index'));
        }*/
        /*  if(in_array('projects', $this->user->modules)){
              $pms[] = array('title'=>__('app.menu.members'), 'link'=>route('admin.project-members.show','all'));
          }
          if(in_array('projects', $this->user->modules)){
              $pms[] = array('title'=>__('app.menu.tasks'), 'link'=>route('admin.tasks.show','all'));
          }*/


        /* if(in_array('tasks', $this->user->modules)){
             $submenu = array();
             $submenu[] = array('title'=>__('app.menu.tasks'), 'link'=>route('admin.all-tasks.index'));
             $submenu[] = array('title'=>__('modules.tasks.taskBoard'), 'link'=>route('admin.taskboard.index'));
             $submenu[] = array('title'=>__('app.menu.taskCalendar'), 'link'=>route('admin.taskscalender.index'));
             $pms[] = array('title'=>__('app.menu.tasks'), 'link'=>'#', 'submenu'=>$submenu);
         }*/
       /* if((in_array("estimates", $this->user->modules)  || in_array("invoices", $this->user->modules)  || in_array("payments", $this->user->modules) || in_array("expenses", $this->user->modules)  )){
            $submenu = array();
            if(in_array('rfq', $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.rfq'), 'link'=>route('admin.rfq.index'));
            }

            if(in_array("estimates", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.estimates'), 'link' => route('admin.estimates.index'));
            }
            if(in_array("payments", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.payments'), 'link' => route('admin.payments.index'));
            }
            if(in_array("expenses", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.expenses'), 'link' => route('admin.expenses.index'));
            }
            if(in_array("invoices", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.credit-note'), 'link' => route('admin.all-credit-notes.index'));
            }
            if(in_array('projects', $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.burndown'), 'link'=>route('admin.projects.burndown-chart','all'));
            }
            $submenu[] = array('title'=>'DPR', 'link'=>'#');
            $submenu[] = array('title'=>__('app.menu.financeReport'), 'link'=>route('admin.finance-report.index'));
            $submenu[] = array('title'=>__('app.menu.incomeVsExpenseReport'), 'link'=>route('admin.income-expense-report.index'));
            if(in_array("activity", $this->user->modules)){
                $submenu[] = array('title'=>'Conditions', 'link'=>route('admin.condition.index'));
            }

            if(in_array("tickets", $this->user->modules)){
                $submenu[] = array('title'=>__('app.menu.tickets'), 'link'=>route('admin.tickets.index'));
            }
            $submenu[] = array('title'=>__('app.menu.timeLogReport'), 'link'=>route('admin.time-log-report.index'));

            $pms[] = array('title'=>__('app.menu.finance'), 'link'=>'#', 'submenu'=>$submenu);
        }*/

        //Store Menus
        $stores = array();
        /*
         $submenu[] = array('title'=>__('app.menu.leaveReport'), 'link'=>route('admin.leave-report.index'));
         $submenu[] = array('title'=>__('app.menu.attendanceReport'), 'link'=>route('admin.attendance-report.index'));
        if(in_array("suppliers", $this->user->modules)){
             $stores[] = array('title'=>__('app.menu.suppliers'), 'link'=>route('admin.suppliers.index'));
         }*/
        $submenu = array();
        if($this->menutype == "development"){
            if(in_array("products", $this->user->modules)){
                $submenu[] = array('title'=>'Product Category', 'link'=>route('admin.product-category.index'));
            }
        }
        if(in_array("products", $this->user->modules)){
            $submenu[] = array('title'=>'Product Brands', 'link'=>route('admin.product-brand.index'));
        }
        if(in_array("products", $this->user->modules)){
            $submenu[] = array('title'=>__('app.menu.products'), 'link'=>route('admin.products.index'));
        }
        if(in_array("suppliers", $this->user->modules)){
            $submenu[] = array('title'=>__('app.menu.suppliers'), 'link'=>route('admin.suppliers.index'));
        }
        if($this->menutype == "development") {
            if (in_array("stores", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.stores-stores'), 'link' => route('admin.stores.index'));
            }
            if (in_array("stores", $this->user->modules)) {
                $submenu[] = array('title' => __('app.menu.product-trade'), 'link' => route('admin.product-trade.index'));
            }
        }
        $stores[] = array('title'=>__('app.menu.masters'), 'link'=>'#', 'submenu'=>$submenu);
        if(in_array("stores", $this->user->modules) && count($this->stores) == 1 ){
            $stores[] = array('title'=>__('app.menu.stores'), 'link'=>route('admin.stores.projects.showStoresProducts', [$this->stores[0]->project_id, $this->stores[0]->id]));
        }else{
            $stores[] = array('title'=>__('app.menu.stores'), 'link'=>route('admin.stores.projects.storesIndex'));
        }
        if(in_array("indent", $this->user->modules)){
            $stores[] = array('title'=>__('app.menu.indent'), 'link'=>route('admin.indent.index'));
        }
        if(in_array('rfq', $this->user->modules)){
            $stores[] = array('title'=>__('app.menu.rfq'), 'link'=>route('admin.stores-rfq.index'));
        }
        if(in_array("quotes", $this->user->modules)){
            $stores[] = array('title'=>__('app.menu.quotes'), 'link'=>route('admin.quotations.index'));
        }
        if(in_array("po", $this->user->modules)){
            $stores[] = array('title'=>__('app.menu.po'), 'link'=>route('admin.purchase-order.index'));
        }
//        if(in_array("indent", $this->user->modules)){
//            $stores[] = array('title'=>__('Product Issue'), 'link'=>route('admin.stores.projects.productIssue'));
//        }


        $submenu = array();
        $submenu[] = array('title'=>__('app.menu.directmaterial'), 'link'=>route('admin.inventory.invoices'));
        $submenu[] = array('title'=>__('app.menu.materialagainstpo'), 'link'=>route('admin.inventory.grnAgainstPo'));
        $stores[] = array('title'=>__('app.menu.material'), 'link'=>'#', 'submenu'=>$submenu);


        if($this->menutype == "development") {
            $stores[] = array('title' => __('app.menu.gloalstore'), 'link' => route('admin.inventory.stock'));
        }
        /*$submenu[] = array('title'=>'Boq Product Issue', 'link'=>route('admin.inventory.boqProductIssueIndex'));*/
        $stores[] = array('title'=>__('app.menu.return'), 'link'=>route('admin.inventory.returns'));
        $stores[] = array('title'=>__('app.menu.materialissue'), 'link'=>route('admin.product-issue.index'));
        $stores[] = array('title'=>__('app.menu.report'), 'link'=>route('admin.inventory.Report'));


       /* $submenu = array();
        $submenu[] = array('title'=>__('app.menu.taskReport'), 'link'=>route('admin.task-report.index'));
        $submenu[] = array('title'=>__('app.menu.timeLogReport'), 'link'=>route('admin.time-log-report.index'));
        $submenu[] = array('title'=>__('app.menu.financeReport'), 'link'=>route('admin.finance-report.index'));
        $submenu[] = array('title'=>__('app.menu.incomeVsExpenseReport'), 'link'=>route('admin.income-expense-report.index'));
        $submenu[] = array('title'=>__('app.menu.leaveReport'), 'link'=>route('admin.leave-report.index'));
        $submenu[] = array('title'=>__('app.menu.attendanceReport'), 'link'=>route('admin.attendance-report.index'));
        $stores[] = array('title'=>__('app.menu.reports'), 'link'=>'#', 'submenu'=>$submenu);*/

        //HR Menus
        $hr = array();

        if(in_array("employees", $this->user->modules)){
            $hr[] = array('title'=>__('app.menu.dashboard'), 'link'=>route('admin.dashboard'));
        }

        $submenu = array();
        if(in_array('employees', $this->user->modules)){
//            $submenu[] = array('title'=>__('app.menu.users'), 'link'=>route('admin.employees.index'));
            $submenu[] = array('title'=>__('app.menu.employee'), 'link'=>route('admin.employee.index'));
        }
        if(in_array('clients', $this->user->modules)) {
            $submenu[] = array('title' => __('app.menu.client'), 'link' => route('admin.clients.index'));
        }
        if(in_array('contractors', $this->user->modules)) {
            $submenu[] = array('title' => __('app.contractor'), 'link' => route('admin.contractors.index'));
        }
        $submenu[] = array('title'=>__('Document Type'), 'link'=>route('admin.document-type.index'));
        $submenu[] = array('title'=>__('app.department'), 'link'=>route('admin.teams.index'));
        $submenu[] = array('title'=>__('app.menu.designation'), 'link'=>route('admin.designations.index'));
        $submenu[] = array('title'=>__('app.menu.rulesAttendace'), 'link'=>route('admin.attendances.rules'));
        if($this->menutype == "development") {
            $submenu[] = array('title' => __('modules.leaves.leaverules'), 'link' => route('admin.leave.leaverules'));
        }
        $submenu[] = array('title'=>__('modules.leaves.workday'), 'link'=>route('admin.leave.leaveRulesSettingsList'));
        $submenu[] = array('title'=>__('app.menu.holiday'), 'link'=>route('admin.holidays.index'));
        $submenu[] = array('title'=>__('modules.payrollsettings.payrollsettings'), 'link'=>route('admin.payroll.index'));
        $submenu[] = array('title' => __('modules.payrollsettings.payslipsettings'), 'link' => route('admin.payroll.paySlipSettings'));
        if($this->menutype == "development") {
            $submenu[] = array('title' => __('modules.payrollsettings.pfsettings'), 'link' => route('admin.payroll.pfEsiSettings'));
            $submenu[] = array('title' => __('modules.payrollsettings.ptsettings'), 'link' => route('admin.payroll.ptSettings'));
            $submenu[] = array('title' => __('modules.payrollsettings.declarationsettings'), 'link' => route('admin.payroll.declarationSettings'));
            $submenu[] = array('title' => __('modules.payrollsettings.component'), 'link' => route('admin.payroll.components'));
            $submenu[] = array('title' => __('modules.salarystructure.salarystructure'), 'link' => route('admin.component.salaryStructure'));
            $submenu[] = array('title' => __('modules.salarystructure.contsalarystructure'), 'link' => route('admin.component.contratorSalaryStructure'));
        }

        $hr[] = array('title'=>__('app.menu.masters'), 'link'=>'#', 'submenu'=>$submenu);

        if(in_array('attendance', $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>__('app.menu.singleAttendance'), 'link'=>route('admin.attendances.singleAttendance'));
            $submenu[] = array('title'=>__('app.menu.posAttendance'), 'link'=>route('admin.attendances.byCamera'));
            $submenu[] = array('title'=>__('app.menu.groupAttendace'), 'link'=>route('admin.attendances.create'));

          /*  $submenu[] = array('title'=>__('app.menu.attendanceReport'), 'link'=>route('admin.attendances.summary'));*/
            $hr[] = array('title'=>__('app.menu.attendance'), 'link'=>'#', 'submenu'=>$submenu);
        }


        if(in_array("leaves", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>__('app.menu.leaves'), 'link'=>route('admin.leave.all-leaves'));
            $submenu[] = array('title'=>__('modules.leaves.leaverulesbalance'), 'link'=>route('admin.leave.leaveBalance'));
            $hr[] = array('title'=>__('app.menu.leavesandholiday'), 'link'=>'#', 'submenu'=>$submenu);
        }
        if(in_array("payroll", $this->user->modules)){
            $submenu = array();
            if($this->menutype == "development") {
                $submenu[] = array('title' => __('modules.salarystructure.declaration'), 'link' => route('admin.declaration'));
            }
            $submenu[] = array('title'=>__('app.menu.salarySlip'), 'link'=>route('admin.employees.salary_slip'));
            $hr[] = array('title'=>__('app.menu.payroll'), 'link'=>'#', 'submenu'=>$submenu);
        }

        $submenu = array();
       /* $submenu[] = array('title'=>__('app.menu.financeReport'), 'link'=>route('admin.finance-report.index'));
        $submenu[] = array('title'=>__('app.menu.incomeVsExpenseReport'), 'link'=>route('admin.income-expense-report.index'));
        $submenu[] = array('title'=>__('app.menu.leaveReport'), 'link'=>route('admin.leave-report.index'));
        $submenu[] = array('title'=>__('modules.salarystructure.base'), 'link'=>route('admin.component.baseComponent'));*/
        if(in_array("attendance", $this->user->modules)){
            $submenu[] = array('title'=>__('app.menu.attendanceReport'), 'link'=>route('admin.attendances.summary'));
        }
        /* if(in_array("messages", $this->user->modules)){
             $submenu[] = array('title'=>__('app.menu.messages'), 'link'=>route('admin.user-chat.index'));
         }*/
        /*if(in_array("events", $this->user->modules)){
            $submenu[] = array('title'=>__('app.menu.Events'), 'link'=>route('admin.events.index'));
        }*/
        /*if(in_array("payroll", $this->user->modules)){
            $submenu[] = array('title'=>__('modules.salarystructure.declaration'), 'link'=>route('admin.declaration'));
        }if(in_array("notices", $this->user->modules)){
            $submenu[] = array('title'=>__('app.menu.noticeBoard'), 'link'=>route('admin.notices.index'));
        }*/
        $hr[] = array('title'=>__('app.menu.reports'), 'link'=>'#', 'submenu'=>$submenu);

        //Maintenance Menus
        $maintenance = array();
        if(in_array("location", $this->user->modules)){
            $maintenance[] = array('title'=>'Location', 'link'=>route('admin.location.index'));
        }
        if(in_array("asset", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'Category', 'link'=>route('admin.asset-category.index'));
            $submenu[] = array('title'=>'Asset', 'link'=>route('admin.asset-category.assets'));
            $submenu[] = array('title'=>'Sub Asset', 'link'=>route('admin.asset-category.sub-asset'));
            $maintenance[] = array('title'=>'Assets', 'link'=>'#', 'submenu'=>$submenu);
        }

        if(in_array("workrequest", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'Create Request', 'link'=>route('admin.create-work-request'));
            $submenu[] = array('title'=>'Requests', 'link'=>route('admin.work-order'));
            $maintenance[] = array('title'=>'Request', 'link'=>'#', 'submenu'=>$submenu);
        }

        if(in_array("workorder", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'Work Orders', 'link'=>route('admin.work-order.allworkorder'));
            $submenu[] = array('title'=>'Recurring Orders', 'link'=>route('admin.work-order.recurring'));

            $maintenance[] = array('title'=>'Work Order', 'link'=>'#', 'submenu'=>$submenu);
        }


        $submenu = array();
        $submenu[] = array('title'=>__('app.menu.taskReport'), 'link'=>route('admin.task-report.index'));
        $submenu[] = array('title'=>__('app.menu.timeLogReport'), 'link'=>route('admin.time-log-report.index'));
        $submenu[] = array('title'=>__('app.menu.financeReport'), 'link'=>route('admin.finance-report.index'));
        $submenu[] = array('title'=>__('app.menu.incomeVsExpenseReport'), 'link'=>route('admin.income-expense-report.index'));
        $submenu[] = array('title'=>__('app.menu.leaveReport'), 'link'=>route('admin.leave-report.index'));
        $submenu[] = array('title'=>__('app.menu.attendanceReport'), 'link'=>route('admin.attendance-report.index'));
        /*$submenu[] = array('title'=>__('app.menu.messages'), 'link'=>route('admin.user-chat.index'));*/
        $submenu[] = array('title'=>__('app.menu.allowancelessdiduction'), 'link'=>route('admin.employees.allowance'));
        $submenu[] = array('title'=>__('app.menu.overtime'), 'link'=>route('admin.employees.overtime'));
        $maintenance[] = array('title'=>__('app.menu.reports'), 'link'=>'#', 'submenu'=>$submenu);

        //Accounts Menus
        $accounts = array();
       /* if(in_array("accounts", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'All', 'link'=>route('admin.branch'));
            $submenu[] = array('title'=>'Create', 'link'=>route('admin.branch.create'));
            $submenu[] = array('title'=>'Trashed', 'link'=>route('admin.branch.trashed'));
            $accounts[] = array('title'=>'Branch', 'link'=>'#', 'submenu'=>$submenu);
        }*/

        if(in_array("accounts", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'Type', 'link'=>route('admin.income_expense_type'));
            $submenu[] = array('title'=>'Group', 'link'=>route('admin.income_expense_group'));
            $submenu[] = array('title'=>'Name', 'link'=>route('admin.income_expense_head'));
            $accounts[] = array('title'=>'Master', 'link'=>'#', 'submenu'=>$submenu);
        }

      /*  if(in_array("accounts", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'All', 'link'=>route('admin.bank_cash'));
            $submenu[] = array('title'=>'Create', 'link'=>route('admin.bank_cash.create'));
            $submenu[] = array('title'=>'Trashed', 'link'=>route('admin.bank_cash.trashed'));
            $accounts[] = array('title'=>'Bank Cash', 'link'=>'#', 'submenu'=>$submenu);
        }

        if(in_array("accounts", $this->user->modules)){
            $submenu = array();
            $submenu[] = array('title'=>'Bank or Cash', 'link'=>route('admin.initial_bank_cash_balance'));
            $submenu[] = array('title'=>'Ledger', 'link'=>route('admin.initial_income_expense_head_balance'));
            $accounts[] = array('title'=>'Initial Balance', 'link'=>'#', 'submenu'=>$submenu);
        }*/

        if(in_array("accounts", $this->user->modules)){
            $submenu = array();
           /* $submenu[] = array('title'=>'Credit', 'link'=>route('admin.cr_voucher'));
            $submenu[] = array('title'=>'Debit', 'link'=>route('admin.dr_voucher'));
            $submenu[] = array('title'=>'Contra', 'link'=>route('admin.contra_voucher'));*/
            $submenu[] = array('title'=>'Payment', 'link'=>route('admin.payment_voucher.create'));
            $submenu[] = array('title'=>'Journal', 'link'=>route('admin.jnl_voucher'));
            $accounts[] = array('title'=>'Voucher', 'link'=>'#', 'submenu'=>$submenu);
        }
        $accounts[] = array('title'=>'Ledger', 'link'=>route('admin.payment_voucher.ledgerlog'));
        $submenu = array();
        $submenu[] = array('title'=>__('Accounts'), 'link'=>route('admin.reports.accounts.ledger'));
        $submenu[] = array('title'=>__('General'), 'link'=>route('admin.reports.general.branch'));
        $accounts[] = array('title'=>__('app.menu.reports'), 'link'=>'#', 'submenu'=>$submenu);

        return array('pms'=>$pms, 'hr'=>$hr, 'store'=>$stores, 'maintenance'=>$maintenance, 'accounts'=>$accounts);

    }

    public function logProjectActivity($projectId, $text)
    {
        $activity = new ProjectActivity();
        $activity->project_id = $projectId;
        $activity->activity = $text;
        $activity->save();
    }

    public function logUserActivity($userId, $text)
    {
        $activity = new UserActivity();
        $activity->user_id = $userId;
        $activity->activity = $text;
        $activity->save();
    }

    public function logSearchEntry($searchableId, $title, $route, $type)
    {
        $search = new UniversalSearch();
        $search->searchable_id = $searchableId;
        $search->title = $title;
        $search->route_name = $route;
        $search->module_type = $type;
        $search->save();
    }

    public function checkLicense($company)
    {
        $packageSettingData = PackageSetting::first();
        $packageSetting = ($packageSettingData->status == 'active') ? $packageSettingData : null;
        $packages = Package::all();

        $trialPackage = $packages->filter(function ($value, $key) {
            return $value->default == 'trial';
        })->first();

        $defaultPackage = $packages->filter(function ($value, $key) {
            return $value->default == 'yes';
        })->first();

        $otherPackage = $packages->filter(function ($value, $key) {
            return $value->default == 'no';
        })->first();

        if ($packageSetting && !is_null($trialPackage)) {
            $selectPackage = $trialPackage;
        } elseif ($defaultPackage)
            $selectPackage = $defaultPackage;
        else {
            $selectPackage = $otherPackage;
        }

        // Set default package for license expired companies.
        if ($selectPackage) {
            $currentPackage = $company->package;
            ModuleSetting::where('company_id', $company->id)->delete();

            $moduleInPackage = (array) json_decode($selectPackage->module_in_package);
            $clientModules = ['projects', 'tickets', 'invoices', 'estimates', 'events', 'tasks', 'messages', 'payments', 'contracts', 'notices'];
            if ($moduleInPackage) {
                foreach ($moduleInPackage as $module) {

                    if (in_array($module, $clientModules)) {
                        $moduleSetting = new ModuleSetting();
                        $moduleSetting->company_id = $company->id;
                        $moduleSetting->module_name = $module;
                        $moduleSetting->status = 'active';
                        $moduleSetting->type = 'client';
                        $moduleSetting->save();
                    }

                    $moduleSetting = new ModuleSetting();
                    $moduleSetting->company_id = $company->id;
                    $moduleSetting->module_name = $module;
                    $moduleSetting->status = 'active';
                    $moduleSetting->type = 'employee';
                    $moduleSetting->save();

                    $moduleSetting = new ModuleSetting();
                    $moduleSetting->company_id = $company->id;
                    $moduleSetting->module_name = $module;
                    $moduleSetting->status = 'active';
                    $moduleSetting->type = 'admin';
                    $moduleSetting->save();
                }
            }

            if ($currentPackage->default == 'trial' && !is_null($packageSetting) && !is_null($defaultPackage)) {
                $company->package_id = $defaultPackage->id;
                $company->licence_expire_on = null;
            } elseif ($packageSetting && !is_null($trialPackage)) {
                $company->package_id = $selectPackage->id;
                $noOfDays = (!is_null($packageSetting->no_of_days) && $packageSetting->no_of_days != 0) ? $packageSetting->no_of_days : 30;
                $company->licence_expire_on = Carbon::now()->addDays($noOfDays)->format('Y-m-d');
            } elseif (is_null($packageSetting) && !is_null($defaultPackage)) {
                $company->package_id = $defaultPackage->id;
                $company->licence_expire_on = null;
            }
            $company->status = 'license_expired';
            $company->save();

            if ($company->company_email) {
                $companyUser = auth()->user();
                $companyUser->notify(new LicenseExpire(($companyUser)));
            }
        }
    }
}
