<?php

namespace App\Http\Controllers\Admin;

use App\BankCash;
use App\Helper\Reply;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;

class BankCashController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Branches';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

//    Important properties
    public $parentModel = BankCash::class;
    public $parentRoute = 'admin.bank_cash';
    public $parentView = "admin.bank-cash";


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->items = BankCash::orderBy('created_at', 'desc')->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->parentView . '.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:bank_cashes',
        ]);
        BankCash::create([
            'name' => $request->name,
            'account_number' => $request->account_number,
            'description' => $request->description,
            'created_by' => \Auth::user()->id,
        ]);
        return Reply::success(__('Created Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->item = BankCash::find($request->id);
        if (empty($this->item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        return view($this->parentView . '.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->item = BankCash::find($id);
        if (empty($this->item)) {
            return Reply::error(__('Item not found'));
        }
        return view($this->parentView . '.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'sometimes|string|unique:bank_cashes,name,'.$id,
        ]);
        $items = BankCash::find($id);
        $items->name = $request->name;
        $items->account_number = $request->account_number;
        $items->description = $request->description;
        $items->updated_by = \Auth::user()->id;
        $items->save();
        return Reply::success(__('Updated Successfully'));
    }

    public function pdf(Request $request)
    {
        $item = BankCash::find($request->id);
        if (empty($item)) {
            Session::flash('error', "Item not found");
            return redirect()->back();
        }
        $now = new \DateTime();
        $date = $now->format(Config('settings.date_format').' h:i:s');
        $extra=array(
            'current_date_time'=>$date,
            'module_name'=>'Bank Cash'
        );
        $pdf = PDF::loadView($this->parentView . '.pdf', ['items'=>$item, 'extra'=>$extra])->setPaper('a4', 'landscape');
        return $pdf->download($extra['current_date_time'].'_'.$extra['module_name'].'.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = BankCash::find($id);
        if (empty($items)) {
            return Reply::error(__('Item not found'));
        }
        if (count(BankCash::find($id)->Transactions)>0){
            return Reply::error(__('You can not delete it. Because it has some transaction.'));
        }
        $items->deleted_by = \Auth::user()->id;
        $items->save();
        $items->delete();
        return Reply::success(__('Trashed Successfully'));
    }

    public function trashed()
    {
        $this->items = BankCash::onlyTrashed()->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

    public function restore($id)
    {
        $items = BankCash::onlyTrashed()->where('id', $id)->first();
        $items->restore();
        $items->updated_by= \Auth::user()->id;
        $items->save();
        return Reply::success(__('Restored Successfully'));
    }

    public function kill($id)
    {
        $items = BankCash::withTrashed()->where('id', $id)->first();
        if (count(BankCash::withTrashed()->find($id)->Transactions)>0){
            return Reply::error(__('You can not permanently delete it. Because it has some transaction.'));
        }
        $items->forceDelete();
        return Reply::success(__('Permanently Deleted'));
    }

    public function activeSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = BankCash::where('name', 'like', '%' . $search . '%')
            ->orWhere('account_number', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->paginate(60);
        return view($this->parentView . '.index', $this->data);
    }

    public function trashedSearch(Request $request)
    {
        $request->validate([
            'search' => 'min:1'
        ]);
        $search = $request["search"];
        $this->items = BankCash::where('name', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('account_number', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->orWhere('description', 'like', '%' . $search . '%')
            ->onlyTrashed()
            ->paginate(60);
        return view($this->parentView . '.trashed', $this->data);
    }

//    Fixed Method for all
    public function activeAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 3 || $request->apply_comand_bottom == 3) {
            foreach ($request->items["id"] as $id) {
                $this->destroy($id);
            }
            return redirect()->back();
        }elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
    }

    public function trashedAction(Request $request)
    {
        $request->validate([
            'items' => 'required'
        ]);
        if ($request->apply_comand_top == 1 || $request->apply_comand_bottom == 1) {
            foreach ($request->items["id"] as $id) {
                $this->restore($id);
            }
        } elseif ($request->apply_comand_top == 2 || $request->apply_comand_bottom == 2) {
            foreach ($request->items["id"] as $id) {
                $this->kill($id);
            }
            return redirect()->back();
        } else {
            Session::flash('error', "Something is wrong.Try again");
            return redirect()->back();
        }
        return redirect()->back();
    }
}
