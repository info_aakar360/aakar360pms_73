<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ProductExport;
use App\Helper\Reply;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Product;
use App\ProductCategory;
use App\ProductTrade;
use App\Tax;
use App\Type;
use App\Units;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class AdminProductController extends AdminBaseController
{
    /**
     * AdminProductController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.products';
        $this->pageIcon = 'icon-basket';
        $this->activeMenu = 'store';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->trades = ProductTrade::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->totalProducts = Product::count();
        return view('admin.products.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->taxes = Tax::where('company_id',$this->companyid)->get();
        $this->pcategory = ProductCategory::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->types = Type::where('company_id',$this->companyid)->get();
        return view('admin.products.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $products = new Product();
        $products->name = $request->name;
        $products->price = $request->price?:'';
        $products->taxes = $request->tax ? json_encode($request->tax) : '';
        $products->category_id = $request->category_id?$request->category_id:'';
        $products->type_id = $request->type_id?$request->type_id:'';
        $products->unit_id = $request->unit_id;
        $products->save();

        return Reply::redirect(route('admin.products.index'), __('messages.productAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->product = Product::find($id);
        $this->taxes = Tax::where('company_id',$this->companyid)->get();
        $this->trades = ProductCategory::where('company_id',$this->companyid)->get();
        $this->units = Units::where('company_id',$this->companyid)->get();
        $this->types = Type::where('company_id',$this->companyid)->get();
        return view('admin.products.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $products = Product::find($id);
        $products->name = $request->name;
        $products->price = $request->price?:'';
        $products->taxes = $request->tax ? json_encode($request->tax) : null;
        $products->category_id = $request->category_id;
        $products->type_id = $request->type_id;
        $products->unit_id = $request->unit_id;
        $products->save();

        return Reply::redirect(route('admin.products.index'), __('messages.productUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return Reply::success(__('messages.productDeleted'));
    }

    /**
     * @return mixed
     */
    public function data(Request $request)
    {

        $products = Product::where('company_id',$this->companyid)->select('id', 'name', 'price', 'taxes', 'unit_id', 'trade_id');
        if(!empty($request->startDate)&&!empty($request->endDate) && $request->startDate !== 'null' && $request->endDate !== 'null'){
            $startDate = Carbon::createFromFormat($this->global->date_format,$request->startDate)->format('Y-m-d');
            $endDate = Carbon::createFromFormat($this->global->date_format, $request->endDate)->format('Y-m-d');
            $products->where(function ($q) use ($startDate, $endDate) {
                $q->whereBetween(DB::raw('DATE(products.`created_at`)'), [$startDate, $endDate]);
                $q->orWhereBetween(DB::raw('DATE(products.`updated_at`)'), [$startDate, $endDate]);
            });
        }
        if (!empty($request->name) != '' && $request->name !=  'null' && $request->name !==  'all') {
            $products->where('name', '=', $request->name);
        }
        if ($request->product_trade !== '' && $request->product_trade !==  'null' && $request->product_trade !==  'all') {
            $products->where('trade_id', '=', $request->product_trade);
        }if ($request->price != '' && $request->price !==  'null' ) {
            $products->where('price', '=', $request->price);
        }if ($request->unit != '' && $request->unit !==  'null' && $request->unit !==  'all') {
            $products->where('unit_id', '=', $request->unit);
        }

        return DataTables::of($products)
            ->addColumn('action', function ($row) {
                $button = '';
                $button .= '<a href="' . route('admin.products.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                $button .= '<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                  data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                return $button;
            })
            ->editColumn('name', function ($row) {
                return ucfirst($row->name);
            })
            ->editColumn('unit_id', function ($row) {
                return get_unit_name($row->unit_id);
            })
            ->editColumn('price', function ($row) {
                return $row->price;
            })
            ->editColumn('trade_id', function ($row) {
                return get_trade_name($row->trade_id);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'unit'])
            ->make(true);
    }

    public function export() {
        /*$attributes =  ['tax', 'taxes', 'price'];
        $products = Product::select('id', 'name', 'price')
            ->get()->makeHidden($attributes);

            // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Price'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($products as $row) {
            $rowArrayData = $row->toArray();
            $rowArrayData['total_amount'] = $this->global->currency->currency_symbol.$rowArrayData['total_amount'];
            $exportArray[] = $rowArrayData;
        }

        // Generate and return the spreadsheet
        Excel::create('Product', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Product');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('Product file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));

                });

            });
        })->download('xlsx');*/

        return Excel::download(new ProductExport, 'Product.xlsx');
    }
}
