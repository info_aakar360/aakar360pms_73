<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TransactionController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Transactions';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'accounts';
        $this->middleware(function ($request, $next) {
            if (!in_array('accounts', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function getUniqueBranches($branch_id)
    {

        if ($branch_id > 0) {
            $transaction_unique_branches = DB::table('transaction_branch_view')
                ->where('branch_id', $branch_id)
                ->get();
        } else {
            $transaction_unique_branches = DB::table('transaction_branch_view')
                ->get();
        }

        return $transaction_unique_branches;

    }

    public function getUniqueBankCashes($bank_cash_id)
    {
        if ($bank_cash_id > 0) {
            $transaction_unique_bank_cashes = DB::table('transaction_bank_cash_view')
                ->where('bank_cash_id', $bank_cash_id)
                ->get();
        } else {
            $transaction_unique_bank_cashes = DB::table('transaction_bank_cash_view')
                ->get();
        }

        return $transaction_unique_bank_cashes;
    }

}
