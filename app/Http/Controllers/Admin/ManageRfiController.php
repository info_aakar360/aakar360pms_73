<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\Rfi\StoreRfi;
use App\Http\Requests\Tasks\StoreTask;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskReminder;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\PunchItem;
use App\PunchItemFiles;
use App\PunchItemReply;
use App\Rfi;
use App\RfiFile;
use App\RfiReply;
use App\Segment;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\EmployeeDocs;
use App\Http\Requests\EmployeeDocs\CreateRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ManageRfiController extends AdminBaseController
{
    use ProjectProgress;
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',
        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'RFI';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('rfi', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $userdetails = $this->user;
        $projectlist = explode(',',$userdetails->projectlist);
        $this->user = $userdetails;
        $this->projects = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        $this->taskBoardStatus = TaskboardColumn::all();
        $rfiarray = $rfilist = $rfiusers = array();
        $rfilist = Rfi::where('private','0')->pluck('id')->toArray();
        $rfiusers = Rfi::where('private','1')->where(function($q) use ($userdetails) {
            $q->where('rfi_manager', $userdetails->id)
                ->orWhere('assign_to',  $userdetails->id)
                ->orWhere('rec_contractor',  $userdetails->id)
                ->orWhere('received_from',  $userdetails->id)
                ->orwhereRaw('FIND_IN_SET(?,distribution)', [$userdetails->id]);
        })->pluck('id')->toArray();
        $rfiarray = array_filter(array_unique(array_merge($rfilist,$rfiusers)));
        $this->items = Rfi::whereIn('id',$rfiarray)->get();
        return view('admin.rfi.index', $this->data);
    }

    public function rfiData(Request $request)
    {
        $user = $this->user;
        $assignid = $request->user;
        $selectdate = $request->selectdate;
        $status = $request->status;
        $inspectionarray = Rfi::where('company_id',$user->company_id);
        if(!empty($selectdate)){
            $inspectionarray = $inspectionarray->where('due_date','<=',$selectdate);
        }
        if(!empty($status)){
            $inspectionarray = $inspectionarray->where('status',$status);
        }
        if(!empty($assignid)){
            $inspectionarray = $inspectionarray->whereRaw('FIND_IN_SET(?,assign_to)', [$assignid]);
        }
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
               $actionlink .= '<a href="'.route('admin.rfi.details', $row->id).'" class="btn btn-info btn-circle"
                           data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                if ( $row->added_by ==  $user->id){
                    $actionlink .= '<a href="' . route('admin.rfi.editRfi', [$row->id]) . '" data-cat-id="' . $row->id . '" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }else{
                    $actionlink .= '<a href="javascript:void(0);" onclick="return alert(\'Invalid access\');" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }
                if($row->added_by==$user->id){
                    $actionlink .='<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                           data-toggle="tooltip" data-task-id="'.$row->id.'" data-original-title="Delete">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>';
                    }
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'start_date',
                function ($row) {
                    return Carbon::parse($row->start_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'due_date',
                function ($row) {
                    return Carbon::parse($row->due_date)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'private',
                function ($row) {
                    return !empty($row->private) ? 'Private' : 'Public';
                }
            )
            ->editColumn(
                'assign_to',
                function ($row) use ($user)  {
                    $dis = '';
                    $distributionarra = explode(',',$row->assign_to);
                    foreach ($distributionarra as $distribut){
                        $dis .= '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_users_employee_name($distribut,$row->company_id)).'</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'distribution',
                function ($row) use ($user)  {
                    $dis = '';
                    $distributionarra = explode(',',$row->distribution);
                    foreach ($distributionarra as $distribut){
                        $dis .= '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_users_employee_name($distribut,$row->company_id)).'</a></div></div>';
                    }
                    return $dis;
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-9 col-xs-8"><a href="javascript:void(0);">'.ucwords(get_user_name($row->added_by)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'action', 'start_date', 'due_date', 'added_by', 'assign_to', 'distribution'])
            ->make(true);
    }
    public function editRfi($id)
    {
        $userdetails = $this->user;
        $rfi = Rfi::findOrFail($id);
        $this->rfi = $rfi;
        if ( $rfi->added_by !=  $userdetails->id){
            return redirect(route('admin.rfi.index'));
        }
        $projectlist = explode(',',$userdetails->projectlist);
        $projectid = $rfi->projectid ?: 0;
        $subprojectid = $rfi->titleid ?: 0;
        $segmentid = $rfi->segmentid ?: 0;
        $activityid = $rfi->activityid ?: 0;
        $this->user = $userdetails;
        $this->clients = Employee::getAllClients($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->files = RfiFile::where('rfi_id',$id)->where('rfitype','rfi')->get();
        $this->titlesarray = !empty($projectid) ? Title::where('project_id', $projectid)->get() : array();
        $this->activitiesarray = !empty($projectid) ? ProjectCostItemsPosition::where('project_id', $projectid)->where('title', $subprojectid)->where('position', 'row')->orderBy('inc','asc')->get() : array();
        $this->costitemsarray = !empty($activityid) ? ProjectCostItemsProduct::where('project_id', $projectid)->where('title', $subprojectid)->where('position_id',$activityid)->orderBy('inc','asc')->get() : array();
          $this->segmentsarray = !empty($subprojectid) ? Segment::where('projectid', $projectid)->where('titleid', $subprojectid)->get() : array();
        return view('admin.rfi.edit', $this->data);
    }

    public function updateRfi(StoreRfi $request, $id)
    {
        $user = $this->user;
        if(!empty($request->project_id)){
            $projectid = Project::find($request->project_id);
            $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $task = Rfi::findOrFail($id);
        $task->company_id = $companyid;
        $task->title = $request->title;
        $task->subject = $request->subject;
        $task->draft = $request->draft ?: 0;
        $task->rfi_manager = $request->rfi_manager;
        if(!empty($request->assign_to)) {
            $task->assign_to = implode(',', $request->assign_to);
        }
        $task->due_date = !empty($request->due_date) ?  Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d') : '';
        $task->status = $request->status;

        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if(!empty($request->distribution)) {
                $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->drawing_no = $request->drawing_no;
        $task->question = $request->question;
        $task->private = $request->private ?: 0;
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;
        $task->schimpact = $request->schimpact ?: '';
        $task->schimpact_days = $request->schimpact_days ?: '';
        $task->costimpact = $request->costimpact ?: '';
        $task->costimpact_days = $request->costimpact_days ?: '';
        $task->projectid = $request->project_id ?: '0';
        $task->titleid = $request->subproject_id ?: '0';
        $task->segmentid = $request->segment_id ?: '0';
        $task->activityid = $request->activity ?: '0';
        $task->costitemid = $request->costitem ?: '0';
        $task->save();
        return Reply::dataOnly(['rfiID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskUpdatedSuccessfully'));
    }

    public function destroy(Request $request, $id)
    {
        $taskFiles = RfiFile::where('rfi_id', $id)->get();

        foreach ($taskFiles as $file) {
            Files::deleteFile($file->hashname, 'rfi-files/' . $file->rfi_id);
            $file->delete();
        }
        Rfi::destroy($id);
        RfiReply::where('rfi_id', $id)->delete();

        return Reply::success(__('RFI deleted successfully'));
    }

    public function createRfi()
    {
        $userdetails = $this->user;
        $projectlist = explode(',',$userdetails->projectlist);
        $this->user = $userdetails;
        $this->projectlist = Project::whereIn('id',$projectlist)->get();
        $this->clients = Employee::getAllClients($userdetails);
        $this->contractors = Employee::getAllContractors($userdetails);
        $this->employees = Employee::getAllEmployees($userdetails);
        return view('admin.rfi.create', $this->data);
    }

    public function membersList($projectId)
    {
        $this->members = ProjectMember::byProject($projectId);
        $list = view('admin.rfi.members-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }
    public function projectTitles(Request $request){
        $projectid = $request->projectid;
        $titleoption = '<option value="">Select Sub Project</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
        }
        return $titleoption;
    }
    public function costitemBytitle(Request $request){
        $projectid = $request->projectid;
        $title = $request->subprojectid ?: 0;
        $titleoption = '<option value="">Select Task</option>';
        if($projectid){
            $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.get_cost_name($item->cost_items_id).'</option>';
            }
        }
        return $titleoption;
    }
    public function dependentTaskLists($projectId, $taskId = null)
    {
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('project_id', $projectId);

            if($taskId != null)
            {
                $this->allTasks = $this->allTasks->where('id', '!=', $taskId);
            }

            $this->allTasks = $this->allTasks->get();
        }else {
            $this->allTasks = [];
        }

        $list = view('admin.rfi.dependent-task-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function storeRfi(StoreRfi $request)
    {
        $user = $this->user;
        if(!empty($request->project_id)){
            $projectid = Project::find($request->project_id);
            $companyid = $projectid->company_id;
        }else{
            $companyid = $user->company_id;
        }
        $task = new Rfi();
        $task->company_id = $companyid;
        $task->title = $request->title;
        $task->subject = $request->subject;
        $task->draft = $request->draft ?: 0;
        $task->rfi_manager = $request->rfi_manager;
        $task->assign_to =  $request->assign_to;
        $task->ballincourt =  $request->assign_to;
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
        $task->status = $request->status;
        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->drawing_no = $request->drawing_no;
        $task->question = $request->question;
        $task->private = $request->private ?: 0;
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;
        $task->schimpact = $request->schimpact ?: '';
        $task->schimpact_days = $request->schimpact_days ?: '';
        $task->costimpact = $request->costimpact ?: '';
        $task->costimpact_days = $request->costimpact_days ?: '';
        $task->projectid = $request->project_id ?: '0';
        $task->titleid = $request->subproject_id ?: '0';
        $task->segmentid = $request->segment_id ?: '0';
        $task->activityid = $request->activity ?: '0';
        $task->costitemid = $request->costitem ?: '0';
        $task->save();

        return Reply::dataOnly(['rfiID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskCreatedSuccessfully'));
    }

    public function ajaxCreate($columnId)
    {
        $this->projects = Project::all();
        $this->columnId = $columnId;
        $this->employees = User::allEmployees();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)->get();
        }else {
            $this->allTasks = [];
        }
        return view('admin.rfi.ajax_create', $this->data);
    }

    public function remindForTask($taskID)
    {
        $task = Task::with('user')->findOrFail($taskID);

        // Send  reminder notification to user
        $notifyUser = $task->user;
        $notifyUser->notify(new TaskReminder($task));

        return Reply::success('messages.reminderMailSuccess');
    }

    public function show($id)
    {
        $this->task = Task::with('board_column')->findOrFail($id);
        $view = view('admin.rfi.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function showFiles($id)
    {
        $this->taskFiles = TaskFile::where('task_id', $id)->get();
        return view('admin.rfi.ajax-file-list', $this->data);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $projectId
     * @param $hideCompleted
     */
    public function export($startDate, $endDate, $projectId, $hideCompleted)
    {

        $tasks = PunchItem::join('users', 'users.id', '=', 'punch_item.assign_to')
            ->select('punch_item.*', 'users.name', 'users.image');

//        $tasks->where(function ($q) use ($startDate, $endDate) {
//            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);
//
//            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
//        });
//
//        if ($projectId != 0) {
//            $tasks->where('tasks.project_id', '=', $projectId);
//        }
//
//        if ($hideCompleted == '1') {
//            $tasks->where('tasks.status', '=', 'incomplete');
//        }

        $attributes =  ['image', 'due_date'];

        $tasks = $tasks->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project', 'Title', 'Assigned To', 'Status', 'Due Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($tasks as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Aakar360 Mentors Pvt. Ltd.')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $rfi = Rfi::where('id',$request->rfi_id)->first();
                $file = new RfiFile();
                $storage = \storage();
                $companyid = $this->user->company_id;
                $rfitype = $request->rfitype;
                $file->company_id = $companyid;
                $file->user_id = $this->user->id;
                $file->rfi_id = $request->rfi_id;
                $file->rfitype = $request->rfitype;
                $file->reply_id = $request->reply_id ?: 0;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('uploads/rfi-files/'.$rfi->id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid.'/rfi-files/'.$rfi->id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'rfi-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('rfi-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$rfi->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->rfi_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('rfi-files/'.$rfi->title.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/rfi-files/'.$request->rfi_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->rfi_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('admin.rfi.index'), __('modules.projects.projectUpdated'));
    }

    public function destroyImage(Request $request, $id)
    {
        $user = $this->user;
        $file = Rfi::findOrFail($id);
        $companyid =
        Files::deleteFile($file->hashname,$user->company_id.'/rfi-files/'.$file->ref_id);
        Storage::disk('s3')->delete($file->hashname,$user->company_id.'/rfi-files/'.$file->rfi_id);
        Rfi::destroy($id);
        $this->taskFiles = Rfi::where('rfi_id', $file->rfi_id)->get();
        $view = view('admin.rfi.index', $this->data)->render();
        return Reply::successWithData(__('messages.fileDeleted'), ['html' => $view, 'totalFiles' => sizeof($this->taskFiles)]);
    }

    public function removeFile($id){
        $taskFiles = RfiFile::findOrFail($id);
        $user = $this->user;
        $taskFiles->delete();
        return Reply::success(__('image deleted successfully'));
    }

    public function reply($id)
    {
        $this->punchitem = PunchItem::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = PunchItemFiles::where('rfi_id',$id)->get();
        $this->replies = PunchItemReply::where('punch_item_id',$id)->get();
        return view('admin.rfi.reply', $this->data);
    }

    public function comment($id)
    {
        $this->punchitem = PunchItem::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = PunchItemFiles::where('task_id',$id)->get();
        $this->replies = PunchItemReply::where('punch_item_id',$id)->get();
        return view('admin.rfi.comment', $this->data);
    }

    public function detailsRfi($id)
    {
        $user = $this->user;
        $this->user = $user;
        $this->rfi = Rfi::findOrFail($id);
        $this->employees = Employee::getAllEmployees($user);
        $this->files = RfiFile::where('rfi_id',$id)->where('rfitype','rfi')->get();
        $this->replies = RfiReply::where('rfi_id',$id)->get();
        $this->responsearray = RfiReply::where('rfi_id',$id)->where('officialresponse','1')->get();
        return view('admin.rfi.details', $this->data);
    }

    public function replyPost(Request $request, $id)
    {
        if(empty($request->comment)){
            return Reply::dataOnly(['error' => 'Comment not found']);
        }
        $pi = new RfiReply();
        $pi->comment = $request->comment;
        $pi->rfi_id = $id;
        $pi->added_by = $this->user->id;
        $pi->save();
        return Reply::dataOnly(['rfiID' => $id,'replyID' => $pi->id]);
    }

    public function commentPost(Request $request, $id)
    {
        $pi = new PunchItemReply();
        $pi->comment = $request->comment;
        $pi->punch_item_id = $id;
        $pi->added_by = $this->user->id;

        $pi->save();

        return Reply::success(__('Comment added successfully'));
    }
    public function officialResponse(Request $request)
    {
        $replyid = $request->replyid;
        $rfi = $request->rfi;
        $pi = RfiReply::where('id',$replyid)->where('rfi_id',$rfi)->first();
        if(!empty($pi->id)){
            if($pi->officialresponse=='1'){
                $pi->officialresponse = 0;
            }else{
                $pi->officialresponse = 1;
            }
            $pi->save();
        }
        return Reply::success(__('Response Updated successfully'));
    }
}
