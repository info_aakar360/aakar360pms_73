<?php

namespace App\Http\Controllers\Auth;

use App\GlobalSetting;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Traits\SmtpSettings;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails, SmtpSettings;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        $this->setMailConfigs();
    }

    public function showLinkRequestForm()
    {
        $setting = GlobalSetting::first();
        return view('auth.passwords.email', compact('setting'));
    }
    public function validateGoogleRecaptcha($googleRecaptchaResponse)
    {
        $setting = GlobalSetting::first();

        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret'=> $setting->google_recaptcha_secret,
                    'response'=> $googleRecaptchaResponse,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        return $body->success;
    }
    public function googleRecaptchaMessage()
    {
        throw ValidationException::withMessages([
            'g-recaptcha-response' => [trans('auth.recaptchaFailed')],
        ]);
    }
    public function forgotPassword()
    {
        $setting = GlobalSetting::first();
        return view('auth.passwords.email', compact('setting'));
    }
    public function postForgotPassword(Request $request)
    {
        $setting = GlobalSetting::first();
        if(!is_null($setting->google_recaptcha_key))
        {
            // Checking is google recaptcha is valid
            $gRecaptchaResponseInput = 'g-recaptcha-response';
            $gRecaptchaResponse = $request->{$gRecaptchaResponseInput};
            $validateRecaptcha = $this->validateGoogleRecaptcha($gRecaptchaResponse);
            if(!$validateRecaptcha)
            {
                return Reply::error('Recaptcha not validated.');
            }
        }
        $username = $request->username;
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $userdetails = User::where('email',$username)->first();
        } else {
            $userdetails = User::where('mobile',$username)->first();
        }
        if(!empty($userdetails->id)){
            $otp = rand(1111,9999);
            $userdetails->otp = $otp;
            $userdetails->save();
            $mailarray = array();
            $message = "<#> Your Aakar360 App OTP is ".$otp." V+wRpavOIwp";
            $mailarray['email'] = $userdetails->email;
            $mailarray['subject'] = "OTP for Aakar360 Forgot password";
            $mailarray['message'] = $message;
            $userdetails->sendEmail($mailarray);
            if(!empty($userdetails->mobile)){
                send_sms($userdetails->mobile,$message);
            }
                request()->session()->put("forgotuser",$userdetails->id);
             return  Reply::success('OTP Sent Successfully');
        }
        return Reply::error('Invalid User try again');
    }
    public function otpPage()
    {
        $setting = GlobalSetting::first();
        $user = \request()->session()->get('forgotuser');
        if(empty($user)){
            return redirect(route('forgot-passsword'));
        }
        $userdetails = User::where('id',$user)->first();
        return view('auth.passwords.otp', compact('setting'));
    }
    public function postOtpPassword(Request $request)
    {
        $setting = GlobalSetting::first();
        if(!is_null($setting->google_recaptcha_key))
        {
            // Checking is google recaptcha is valid
            $gRecaptchaResponseInput = 'g-recaptcha-response';
            $gRecaptchaResponse = $request->{$gRecaptchaResponseInput};
            $validateRecaptcha = $this->validateGoogleRecaptcha($gRecaptchaResponse);
            if(!$validateRecaptcha)
            {
                return Reply::error('Recaptcha not validated.');
            }
        }
        $user = \request()->session()->get('forgotuser');
        $otp = $request->otp;
        if(empty($user)){
            return Reply::error('User not found.');
        }
        $userdetails = User::where('id',$user)->first();
        if(!empty($userdetails->id)){
            if($userdetails->otp===$otp){
                return  Reply::success('OTP validated Successfully');
            }
            return Reply::error('Invalid OTP');
        }
        return Reply::error('User not found.');
    }
    public function resetPassword()
    {
        $setting = GlobalSetting::first();
        $user = \request()->session()->get('forgotuser');
        if(empty($user)){
            return redirect(route('forgot-passsword'));
        }
        $userdetails = User::where('id',$user)->first();
        return view('auth.passwords.reset', compact('setting'));
    }
    public function postResetPassword(Request $request)
    {
        $setting = GlobalSetting::first();
        if(!is_null($setting->google_recaptcha_key))
        {
            // Checking is google recaptcha is valid
            $gRecaptchaResponseInput = 'g-recaptcha-response';
            $gRecaptchaResponse = $request->{$gRecaptchaResponseInput};
            $validateRecaptcha = $this->validateGoogleRecaptcha($gRecaptchaResponse);
            if(!$validateRecaptcha)
            {
                return Reply::error('Recaptcha not validated.');
            }
        }
        $user = \request()->session()->get('forgotuser');
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        if(empty($user)){
            return Reply::error('User not found.');
        }
        $userdetails = User::where('id',$user)->first();
        if(!empty($userdetails->id)){
            if($password===$password_confirmation){
                $userdetails->password = trim(bcrypt($password));
                $userdetails->save();
                request()->session()->forget("forgotuser");
                return  Reply::success('OTP Sent Successfully');
            }
            return Reply::error('Password not matched');
        }
        return Reply::error('User not found.');
    }

}
