<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\Currency;
use App\GlobalCurrency;
use App\GlobalSetting;
use App\Helper\Reply;
use App\InvitedUser;
use App\Notifications\NewUser;
use App\PermissionRole;
use App\ProjectMember;
use App\ProjectPermission;
use App\Role;
use App\RoleUser;
use App\TempUser;
use App\User;
use App\Http\Controllers\Controller;
use Google\Client;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/projects/select-projects';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //send welcome email notification
        $user->notify(new NewUser($user));
        $user->attachRole(2);

    }
    public function index()
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }
        $setting = GlobalSetting::first();
        return view('auth.register', compact('setting'));
    }
    public function sendotp(Request $request)
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }
        $mobile = $request->mobile;
        $otp = rand(1111,9999);
        request()->session()->put('loginotp',$otp);
        $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
        if(!empty($mobile)){
            send_sms($mobile,$message);
        }
        return Reply::success(__('messages.otpsent'));
    }
    public function validateGoogleRecaptcha($googleRecaptchaResponse)
    {
        $setting = GlobalSetting::first();

        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret'=> $setting->google_recaptcha_secret,
                    'response'=> $googleRecaptchaResponse,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        return $body->success;
    }
    public function googleRecaptchaMessage()
    {
        throw ValidationException::withMessages([
            'g-recaptcha-response' => [trans('auth.recaptchaFailed')],
        ]);
    }
    public function store(Request $request)
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }
        $setting = GlobalSetting::first();
        $otp = request()->session()->get('loginotp');
        if($otp!=$request->otp){
            return Reply::error('Invalid otp please try again.');
        }
        if(!is_null($setting->google_recaptcha_key))
        {
            // Checking is google recaptcha is valid
            $gRecaptchaResponseInput = 'g-recaptcha-response';
            $gRecaptchaResponse = $request->{$gRecaptchaResponseInput};
            $validateRecaptcha = $this->validateGoogleRecaptcha($gRecaptchaResponse);
            if(!$validateRecaptcha)
            {
                return Reply::error('Recaptcha not validated.');
            }
        }
        $name = trim($request->name);
        $email = strtolower(trim($request->email));
        $mobile = strtolower(trim($request->mobile));
        $password = trim($request->password);
        if(empty($name)){
            return Reply::error(__('messages.nameEmpty'));
        }
        if(empty($mobile)){
            return Reply::error(__('messages.mobileEmpty'));
        }
        if(empty($email)){
            return Reply::error(__('messages.emailEmpty'));
        }
        if(!checkMobile($mobile)){
            $mobile = str_replace(" ", "", $mobile);
            $mobile = str_replace("-", "", $mobile);
            $len = strlen($mobile);
            $c = $len - 10;
            if($c){
                $mobile = substr($mobile, $c, 10);
            }
        }
        if(!empty($email)&&!checkEmail($email)){
            return Reply::error( 'Invalid email. try again');
        }
        $users = User::withoutGlobalScope('active')->where('mobile', '=', $mobile)->orWhere('email', '=', $email)->first();
        if(!empty($users->id)&&$users->status=='active'&&$users->login=='enable'){
            $response['message'] = 'Account already exists. Please login with provided details';
            $response['status'] = 300;
            return $response;
        }
        $prevemail = User::where('email',$email)->where('login','enable')->first();
        if(empty($prevemail->id)){
            $prevemail =  User::where('mobile',$mobile)->where('login','enable')->first();
            if(empty($prevemail->id)){
                    $com = Company::where(function ($query) use ($mobile,$email) {
                        $query->where('company_phone', '=', $mobile)
                            ->orWhere('company_email', '=', $email);
                    })->first();

                    if(empty($com->id)){
                        $com = new Company();
                    }
                    $com->company_name = $name;
                    $com->company_email = $email;
                    $com->company_phone = $mobile;
                    $com->address = 'NA';
                    $com->package_type = 'annual';
                    $com->package_id = 1;
                    $com->save();

                $globalCurrency = GlobalCurrency::find(4);
                $currency = Currency::where('currency_code', $globalCurrency->currency_code)
                    ->where('company_id', $com->id)->first();

                if(is_null($currency)) {
                    $currency = new Currency();
                    $currency->currency_name = $globalCurrency->currency_name;
                    $currency->currency_symbol = $globalCurrency->currency_symbol;
                    $currency->currency_code = $globalCurrency->currency_code;
                    $currency->is_cryptocurrency = $globalCurrency->is_cryptocurrency;
                    $currency->usd_price = $globalCurrency->usd_price;
                    $currency->company_id = $com->id;
                    $currency->save();
                }
                $com->currency_id = $currency->id;
                $com->save();
                $uData = array();
                if($com) {
                    $appid = md5(microtime());
                    $adminRole = Role::where('name', 'admin')->where('company_id', $com->id)->withoutGlobalScope('active')->first();
                    $executive = Role::where('name', 'executive')->where('company_id', $com->id)->first();
                    $user = User::withoutGlobalScope('active')->where('mobile', '=', $mobile)->orWhere('email', '=', $email)->first();
                    if(empty($users->id)){
                        $user = new User();
                    }
                    $user->company_id = $com->id;
                    $user->name = trim($name);
                    $user->email = trim($email);
                    $user->mobile = trim($mobile);
                    $user->gender = trim($request->gender);
                    $user->password = trim(bcrypt($password));
                    $user->appid = $appid;
                    $user->status = 'active';
                    $user->login = 'enable';
                    $user->save();

                    if(empty($adminRole) && !empty($executive)){
                        $roleUser = new RoleUser();
                        $roleUser->user_id = $user->id;
                        $roleUser->role_id = $executive->id;
                        $roleUser->save();
                    }
                    $tmparray = TempUser::where(function ($query) use ($mobile,$email) {
                        $query->where('mobile', '=', $mobile)
                            ->orWhere('mobile', '=', $email);
                    })->get();
                    if ($user) {
                        $user->roles()->attach($adminRole->id);
                        if($tmparray){
                            foreach ($tmparray as $tmp){
                                if($tmp) {
                                    $previnvite = InvitedUser::where('company_id',$tmp->company_id)->where('user_id',$user->id)->first();
                                    if(empty($previnvite->id)){
                                        $iu = new InvitedUser();
                                        $iu->company_id = $tmp->company_id;
                                        $iu->user_id = $user->id;
                                        $iu->invited_by = $user->id;
                                        $iu->mobile = $email;
                                        $iu->save();
                                    }
                                    $prevproject = ProjectMember::where('user_id',$user->id)->where('company_id',$tmp->company_id)->where('project_id',$tmp->project_id)->first();
                                    if(empty($prevproject->id)){

                                        $pm = new ProjectMember();
                                        $pm->company_id = $tmp->company_id;
                                        $pm->project_id = $tmp->project_id;
                                        $pm->user_id = $user->id;
                                        $pm->user_type = $tmp->user_type;
                                        $pm->assigned_by = $tmp->invited_by;
                                        $pm->share_project = '1';
                                        $pm->save();
                                        $urole = Role::where('name', $tmp->user_type)->where('company_id', $tmp->company_id)->first();
                                        if($urole === null){
                                            return Reply::error('The company invited you do not have user role type '.$tmp->user_type);
                                         }
                                        $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                                        foreach ($perms as $perm) {
                                            $pprm = new ProjectPermission();
                                            $pprm->project_id = $tmp->project_id;
                                            $pprm->user_id = $user->id;
                                            $pprm->permission_id = $perm;
                                            $pprm->save();
                                        }
                                    }
                                }
                            }
                            TempUser::where('mobile',$mobile)->orWhere('mobile',$email)->delete();

                        }
                        //Set default roles permissions in PermissionRole table
                        $rsx = Role::where('company_id', 1)->where( function($query){
                            $query->where('name', 'admin');
                            $query->orWhere('name', 'administrative');
                            $query->orWhere('name', 'executive');
                            $query->orWhere('name', 'standard');
                        })->get();
                        foreach($rsx as $rs){
                            $permissions = PermissionRole::where('role_id', $rs->id)->get();
                            foreach ($permissions as $permission){
                                $newRole = Role::where('company_id', $com->id)->where('name', $rs->name)->first();
                                 if($newRole !== null) {
                                     $prevper = PermissionRole::where('role_id',$newRole->id)->where('permission_id',$permission->permission_id)->first();
                                     if(empty($prevper->id)){
                                         $perm = new PermissionRole();
                                         $perm->role_id = $newRole->id;
                                         $perm->permission_id = $permission->permission_id;
                                         $perm->save();
                                     }
                                }
                            }
                        }
                    }
                        Auth::loginUsingId($user->id);
                    return Reply::success(__('messages.accountCreated'));
                }
            }
            return Reply::error(__('messages.mobileExists'));
        }
        return Reply::error(__('messages.emailExists'));
    }
}
