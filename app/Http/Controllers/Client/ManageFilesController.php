<?php

namespace App\Http\Controllers\Client;

use App\FileManager;
use App\Helper\Reply;
use App\Http\Requests\StoreFileLink;
use App\Notifications\FileUpload;
use App\ProjectFile;
use App\Project;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use DB;

class ManageFilesController extends ClientBaseController
{

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    /**
     * ManageProjectFilesController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.files';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $this->projectAll = Project::all();
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $this->foldername = FileManager::where('id',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        return view('client.manage-files.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->project_id)) {
            $this->project = Project::with('clients', 'clients.user')->findOrFail($request->project_id);
        }
        if ($request->hasFile('file')) {
            $storage = config('filesystems.default');
            $file = new FileManager();
            $file->user_id = $this->user->id;
            if(isset($request->project_id)) {
                $file->project_id = $request->project_id;
            }
            switch($storage) {
                case 'local':
                    if(isset($request->project_id)) {
                        $proname = FileManager::where('project_id',$request->project_id)->where('parent','0')->first();
                        $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                        $parentfolder = $parentname->filename;

                        $request->file->storeAs('user-uploads/project-files/'.$proname->filename.'/'.$parentfolder, $request->file->hashName());
                    }else {
                        $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                        $request->file->storeAs('user-uploads/project-files/' . $parentname->filename, $request->file->hashName());
                    }
//                    $request->file->storeAs('user-uploads/project-files/'.$request->parent, $request->file->hashName());
                    break;
                case 's3':
                    Storage::disk('s3')->putFileAs('project-files/'.$request->parent, $request->file, $request->file->getClientOriginalName(), 'public');
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', 'project-files')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('project-files');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', $request->parent)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->parent)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $file->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());

                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $file->dropbox_link = $dropboxResult['url'];
                    break;
            }

            $file->filename = $request->file->getClientOriginalName();
            $file->hashname = $request->file->hashName();
            $file->size = $request->file->getSize();
            $file->parent = $request->parent;
            $file->save();
            if(isset($request->project_id)) {
                $this->logProjectActivity($request->project_id, __('messages.newFileUploadedToTheProject'));

//                foreach ($this->project->clients as $client) {
//                    $notifyUser = User::find($client->user->id);
//                    $notifyUser->notify(new FileUpload($file));
//                }
            }
        }

        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();

//        $this->projects = DB::table('project_files')
//            ->join('projects', 'projects.id', '=', 'project_files.project_id')
//            ->select('project_files.*', 'projects.project_name', 'projects.id as pid')
//            ->get();
//        if(isset($request->project_id)) {
//            $this->project = FileManager::findOrFail($request->parent);
//            $this->icon($request->project);
//
//            $this->projectsAll = Project::get();
//        }

        if($request->view == 'list') {
            $view = view('client.manage-files.ajax-list', $this->data)->render();
        } else {
            $view = view('client.manage-files.thumbnail-list', $this->data)->render();
        }
        return redirect()->back();
    }

    public function storeMultiple(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $file = new FileManager();
                $file->user_id = $this->user->id;
                if(isset($request->project_id)) {
                    $file->project_id = $request->project_id;
                }
                switch($storage) {
                    case 'local':
                        if(isset($request->project_id)) {
                            $proname = FileManager::where('project_id',$request->project_id)->where('parent','0')->first();
                            $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                                $parentfolder = $parentname->filename;

                                $fileData->storeAs('user-uploads/project-files/'.$proname->filename.'/'.$parentfolder, $fileData->hashName());
                        }else {
                            $parentname = FileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                            $fileData->storeAs('user-uploads/project-files/' . $parentname->filename, $fileData->hashName());
                        }
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('project-files/'.$request->parent, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'project-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('project-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->parent)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->parent)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->parent = $request->parent;
                $file->save();
                if(isset($request->project_id)) {
                    $this->logProjectActivity($request->project_id, __('messages.newFileUploadedToTheProject'));
                }
            }

        }
        return Reply::redirect(route('client.manage-files.index'), __('modules.projects.projectUpdated'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->project = Project::findOrFail($id);
        return view('client.files.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $storage = config('filesystems.default');
        $file = FileManager::findOrFail($id);
        switch($storage) {
            case 'local':
                File::delete('user-uploads/project-files/'.$file->parent.'/'.$file->hashname);
                break;
            case 's3':
                Storage::disk('s3')->delete('project-files/'.$file->parent.'/'.$file->filename);
                break;
            case 'google':
                Storage::disk('google')->delete('project-files/'.$file->parent.'/'.$file->filename);
                break;
            case 'dropbox':
                Storage::disk('dropbox')->delete('project-files/'.$file->parent.'/'.$file->filename);
                break;
        }
        FileManager::destroy($id);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
//        if(isset($request->project_id)) {
//            $this->project = Project::findOrFail($file->project_id);
//            $this->icon($file->parent);
//        }
        if($request->view == 'list') {
            $view = view('admin.manage-files.ajax-list', $this->data)->render();
        } else {
            $view = view('admin.manage-files.thumbnail-list', $this->data)->render();
        }
        return Reply::successWithData(__('messages.fileDeleted'), ['html' => $view]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function download($id) {
        $storage = config('filesystems.default');
        $file = FileManager::findOrFail($id);
        switch($storage) {
            case 'local':
                $parent = FileManager::where('id',$file->parent)->first();
                if($parent->parent !== '0'){
                    $second = FileManager::where('id',$parent->parent)->first();
                    return response()->download('user-uploads/project-files/'.$second->filename.'/'.$parent->filename.'/'.$file->hashname, $file->filename);
                }
//                return response()->download('user-uploads/project-files/'.$file->parent.'/'.$file->hashname, $file->filename);
                break;
            case 's3':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $fs = Storage::getDriver();
                $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                return Response::stream(function() use($stream) {
                    fpassthru($stream);
                }, 200, [
                    "Content-Type" => $ext,
                    "Content-Length" => $file->size,
                    "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                ]);
                break;
            case 'google':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $dir = '/';
                $recursive = false; // Get subdirectories also?
                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                $directory = $contents->where('type', '=', 'dir')
                    ->where('filename', '=', 'project-files')
                    ->first();

                $direct = $directory['path'];
                $recursive = false;
                $contents = collect(Storage::cloud()->listContents($direct, $recursive));
                $directo = $contents->where('type', '=', 'dir')
                    ->where('filename', '=', $file->parent)
                    ->first();

                $readStream = Storage::cloud()->getDriver()->readStream($directo['path']);
                return response()->stream(function () use ($readStream) {
                    fpassthru($readStream);
                }, 200, [
                    'Content-Type' => $ext,
                    'Content-disposition' => 'attachment; filename="'.$file->filename.'"',
                ]);
                break;
            case 'dropbox':
                $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                $fs = Storage::getDriver();
                $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                return Response::stream(function() use($stream) {
                    fpassthru($stream);
                }, 200, [
                    "Content-Type" => $ext,
                    "Content-Length" => $file->size,
                    "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                ]);
                break;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function thumbnailShow(Request $request)
    {
        $this->project = Project::with('files')->findOrFail($request->id);
        $this->icon($this->project);
        $view = view('client.manage-files.thumbnail-list', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * @param $projects
     */
    private function icon($projects) {
        foreach ($projects->files as $project) {
            if (is_null($project->external_link)) {
                $ext = pathinfo($project->filename, PATHINFO_EXTENSION);
                if ($ext == 'png' || $ext == 'jpe' || $ext == 'jpeg' || $ext == 'jpg' || $ext == 'gif' || $ext == 'bmp' ||
                    $ext == 'ico' || $ext == 'tiff' || $ext == 'tif' || $ext == 'svg' || $ext == 'svgz' || $ext == 'psd' || $ext == 'csv')
                {
                    $project->icon = 'images';
                } else {
                    $project->icon = $this->mimeType[$ext];
                }
    
            }
        }
    }


    public function storeLink(StoreFileLink $request) 
    {
        $file = new FileManager();
        $file->user_id = $this->user->id;
        $file->company_id = $this->user->company_id;
        if(isset($request->project_id)) {
            $file->project_id = $request->project_id;
        }
        $file->parent = $request->parent;
        $file->external_link = $request->external_link;
        $file->filename = $request->filename;
        $file->save();
        if(isset($request->project_id)) {
            $this->logProjectActivity($request->project_id, __('messages.newFileUploadedToTheProject'));
        }
        return Reply::success(__('messages.fileUploaded'));
    }

    public function createFolder(Request $request){
        $folder = new FileManager();
        $folder->filename = $request->foldername;
        $folder->user_id = $this->user->id;
        if(isset($request->project_id)) {
            $folder->project_id = $request->project_id;
        }
        $folder->parent = $request->parent;
        $folder->type = $request->type;
        $folder->save();

        return back()->with('message','Folder created');
    }

    public function editFolder(Request $request){
        $folder = FileManager::find($request->id);
        $folder->filename = $request->foldername;
        $folder->user_id = $this->user->id;
        if(isset($request->project_id)) {
            $folder->project_id = $request->project_id;
        }
        $folder->parent = $request->parent;
        $folder->type = $request->type;
        $folder->save();

        return back()->with('message','Folder name updated');
    }
}
