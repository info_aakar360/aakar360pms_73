<?php

namespace App\Http\Controllers\Client;

use App\Helper\Reply;
use App\Http\Requests\Boq\StoreBoqCategory;
use App\Condition;
use Illuminate\Http\Request;

class ManageConditionController extends ClientBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Condition';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->conditions = Condition::all();
        return view('client.condition.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->conditions = Condition::all();
        return view('client.condition.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCondition()
    {
        $this->conditions = Condition::all();
        return view('client.condition.index', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Condition();
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('Condition name added successfully'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCondition(Request $request)
    {
        $category = new Condition();
        $category->name = $request->name;
        $category->save();
        $categoryData = Condition::all();
        return Reply::successWithData(__('Condition name added successfully'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->conditions = Condition::all();
        $this->condition = Condition::where('id',$id)->first();
        return view('client.condition.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Condition::find($id);
        $category->name = $request->name;
        $category->save();

        return Reply::success(__('Condition name updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Condition::destroy($id);
        $categoryData = Condition::all();
        return Reply::successWithData(__('Condition name deleted successfully'),['data' => $categoryData]);
    }
}
