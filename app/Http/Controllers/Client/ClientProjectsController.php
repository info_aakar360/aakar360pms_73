<?php

namespace App\Http\Controllers\Client;

use App\BoqCategory;
use App\Condition;
use App\CostItems;
use App\CostItemsLavel;
use App\CostItemsProduct;
use App\FileManager;
use App\Helper\Reply;
use App\Issue;
use App\ModuleSetting;
use App\ProductBrand;
use App\ProductCategory;
use App\Project;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsProduct;
use App\ProjectFile;
use App\ProjectTimeLog;
use App\SchedulingDue;
use App\SourcingPackage;
use App\SourcingPackageProduct;
use App\Task;
use App\TenderAssign;
use App\TenderBidding;
use App\Tenders;
use App\TendersCondition;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class ClientProjectsController extends ClientBaseController
{

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',

        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.projects';
        $this->pageIcon = 'icon-layers';
        $this->middleware(function ($request, $next) {
            if(!in_array('projects',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.projects.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->userDetail = auth()->user();

        $this->project = Project::findOrFail($id)->withCustomFields();
        $this->fields = $this->project->getCustomFieldGroupsWithFields()->fields;

        // Check authorised user

        if($this->project->checkProjectClient())
        {
            $this->activeTimers = ProjectTimeLog::projectActiveTimers($this->project->id);

            $this->openTasks = Task::projectOpenTasks($this->project->id);
            $this->openTasksPercent = (count($this->openTasks) == 0 ? '0' : (count($this->openTasks) / count($this->project->tasks)) * 100);

            // TODO::ProjectDeadline to do
            $this->daysLeft = 0;
            $this->daysLeftFromStartDate = 0;
            $this->daysLeftPercent = 0;

            if($this->project->deadline){
                $this->daysLeft = $this->project->deadline->diff(Carbon::now())->format('%d')+($this->project->deadline->diff(Carbon::now())->format('%m')*30)+($this->project->deadline->diff(Carbon::now())->format('%y')*12);

                $this->daysLeftFromStartDate = $this->project->deadline->diff($this->project->start_date)->format('%d')+($this->project->deadline->diff($this->project->start_date)->format('%m')*30)+($this->project->deadline->diff($this->project->start_date)->format('%y')*12);

                $this->daysLeftPercent = ($this->daysLeftFromStartDate == 0 ? "0" : (($this->daysLeft / $this->daysLeftFromStartDate) * 100));
            }

            $this->hoursLogged = ProjectTimeLog::projectTotalMinuts($this->project->id);

            $hour = intdiv($this->hoursLogged, 60);

            if (($this->hoursLogged % 60) > 0) {
                $minute = ($this->hoursLogged % 60);
                $this->hoursLogged = $hour . 'hrs ' . $minute . ' mins';
            } else {
                $this->hoursLogged = $hour;
            }
            
            $this->recentFiles = ProjectFile::where('project_id', $this->project->id)->orderBy('id','desc')->limit(10)->get();
            $this->activities = ProjectActivity::getProjectActivities($id, 10);

            return view('client.projects.show', $this->data);
        }
        else{
            // If not authorised user
            return redirect(route('client.dashboard.index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data()
    {
        $projects = Project::select('projects.id', 'projects.project_name', 'projects.project_summary', 'projects.start_date', 'projects.deadline', 'projects.notes', 'projects.category_id', 'projects.client_id', 'projects.feedback', 'projects.completion_percent', 'projects.created_at', 'projects.updated_at')
            ->where('projects.client_id', '=', $this->user->id);

        return DataTables::of($projects)
            ->addColumn('action', function($row){
                return '<a href="'.route('client.projects.show', [$row->id]).'" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Project Details"><i class="fa fa-search" aria-hidden="true"></i></a>';
            })
            ->addColumn('members', function ($row) {
                $members = '';

                if (count($row->members) > 0) {
                    foreach ($row->members as $member) {
                        $members .= ($member->user->image) ? '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('user-uploads/avatar/' . $member->user->image) . '"
                        alt="user" class="img-circle" width="30"> ' : '<img data-toggle="tooltip" data-original-title="' . ucwords($member->user->name) . '" src="' . asset('default-profile-2.png') . '"
                        alt="user" class="img-circle" width="30"> ';
                    }
                }
                else{
                    $members.= __('messages.noMemberAddedToProject');
                }
                return $members;
            })

            ->editColumn('project_name', function($row){
                return '<a href="'.route('client.projects.show', $row->id).'">'.ucfirst($row->project_name).'</a>';
            })
            ->editColumn('start_date', function($row){
                return $row->start_date->format($this->global->date_format);
            })
            ->editColumn('deadline', function($row){
                if($row->deadline){
                    return $row->deadline->format($this->global->date_format);
                }
                return '-';
            })
            ->editColumn('completion_percent', function ($row) {
                if ($row->completion_percent < 50) {
                    $statusColor = 'danger';
                    $status = __('app.progress');
                }
                elseif ($row->completion_percent >= 50 && $row->completion_percent < 75) {
                    $statusColor = 'warning';
                    $status = __('app.progress');
                }
                else {
                    $statusColor = 'success';
                    $status = __('app.progress');

                    if($row->completion_percent >= 100){
                        $status = __('app.completed');
                    }
                }

                return '<h5>'.$status.'<span class="pull-right">' . $row->completion_percent . '%</span></h5><div class="progress">
                  <div class="progress-bar progress-bar-' . $statusColor . '" aria-valuenow="' . $row->completion_percent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $row->completion_percent . '%" role="progressbar"> <span class="sr-only">' . $row->completion_percent . '% Complete</span> </div>
                </div>';
            })
            ->rawColumns(['project_name', 'action', 'members', 'completion_percent'])
            ->removeColumn('project_summary')
            ->removeColumn('notes')
            ->removeColumn('category_id')
            ->removeColumn('feedback')
            ->removeColumn('client_id')
            ->make(true);
    }

    public function Boq(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::get();
        $this->projectproduct = Arr::pluck(ProjectCostItemsProduct::where('project_id',$id)->get(), 'title');
        $this->titles = Title::get();
        $this->id = $id;
        return view('client.projects.boq', $this->data);
    }

    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('client.projects.project-boq-create', $this->data);
    }

    public function getItemLavel(Request $request){
        $this->categories = BoqCategory::get();
        $costitemlavel = CostItemsLavel::where('boq_category_id', $request->category_id)->groupby('cost_items_id')->get();
        $html = '<option value="">Please select Task</option>';
        foreach ($costitemlavel as $cil){
            $ciid = $cil->cost_items_id;
            $costitem = CostItems::where('id',$ciid)->first();
            $html .= '<option value="'.$costitem->id.'">'.$costitem->cost_item_name.'</option>';
        }
        return $html;
    }

    public function getChild(Request $request){
        $getchild = BoqCategory::where('parent', $request->category_id)->get();
        $id = $request->id + 1;
        $html = '<select name="category[]" class="form-control" onchange="getChild(this.value,id)">
            <option value="">Please select Category</option>';
        foreach ($getchild as $cil){
            $html .= '<option value="'.$cil->id.'">'.$cil->title.'</option>';
        }
        $html .= '</select>';
        if(count($getchild) !== 0) {
            return $html;
        }else{
            return '';
        }
    }

    public function getProduct(Request $request){
        $this->categories = BoqCategory::get();
        $costitem = CostItemsProduct::where('cost_items_id', $request->cost_item_id)->get();
        $html = '
            <table class="table">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Category Name</td>
                        <td>Brand Name</td>
                        <td>Unit</td>
                        <td>Quantity</td>
                        <td>Wastage</td>
                        <td>Rate</td>
                        <td>Cost</td>
                    </tr>
                </thead>
                <tbody>';
        $sr = 1;
        foreach ($costitem as $cil){
            $ciid = $cil->cost_items_id;
            $pc = $cil->product_category_id;
            $pb = $cil->product_brand_id;
            $pu = $cil->unit;
            $pcn = ProductCategory::where('id',$pc)->first();
            $pbn = ProductBrand::where('id',$pb)->first();
            $pun = Units::where('id',$pu)->first();
            $html .='<tr>
                        <td>'.csrf_field().'
                            <input type="hidden" name="project_id" class="form-control" value="'.$request->id.'">
                            <input type="hidden" name="cost_item_id['.$sr.']" class="form-control" value="'.$ciid.'">'.$sr .'
                        </td>
                        <td><input type="hidden" name="product_category_id['.$sr.']" class="form-control" value="'.$pc.'">'.$pcn->name.'</td>
                        <td><input type="hidden" name="product_brand_id['.$sr.']" class="form-control" value="'.$pb.'">'.$pbn->name.'</td>
                        <td><input type="hidden" name="units['.$sr.']" class="form-control" value="'.$pu.'">'.$pun->name.'</td>
                        <td><input type="text" name="qty['.$sr.']" id="qty'.$sr.'" class="form-control"></td>
                        <td><input type="text" name="wastage['.$sr.']" class="form-control"></td>
                        <td><input type="text" data-cat-id="'.$sr.'" onchange="getCost('.$sr.');" id="rate'.$sr.'" name="rate['.$sr.']" class="form-control"></td>
                        <td><input type="text" name="cost['.$sr.']" id="cost'.$sr.'" class="form-control" readonly></td>
                    </tr>';
            $sr++; }
        $html .= '</tbody>
            </table>
            <div class="col-lg-12">
                <button type="button" id="saveProduct" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
            </div>';
        return $html;
    }

    public function getCostItem(Request $request){
        $this->categories = BoqCategory::get();
        $costitemlavel = CostItemsLavel::where('boq_category_id', $request->category_id)->groupby('cost_items_id')->get();
        $html = '<option value="">Please select Task</option>';
        foreach ($costitemlavel as $cil){
            $ciid = $cil->cost_items_id;
            $costitem = CostItems::where('id',$ciid)->first();
            $html .= '<option value="'.$costitem->id.'">'.$costitem->cost_item_name.'</option>';
        }
        return $html;
    }

    public function addCostItems(Request $request){
        $title = $request->title;
        $pc = '';
        if($request->category !== null) {
            $pc = implode(',', $request->category);
        }
        $category = $pc;
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $product_category_id = $request->product_category_id;
        $product_brand_id = $request->product_brand_id;
        $units = $request->units;
        $qty = $request->qty;
        $wastage = $request->wastage;
        $rate = $request->rate;
        $cost = $request->cost;
        foreach ($qty as $key=>$value){
            if($qty !== "" && $qty > 0) {
                $pcip = new ProjectCostItemsProduct();
                $pcip->title = $title;
                $pcip->category = $category;
                $pcip->project_id = $project_id;
                $pcip->cost_items_id = $cost_item_id[$key];
                $pcip->product_category_id = $product_category_id[$key];
                $pcip->product_brand_id = $product_brand_id[$key];
                $pcip->unit = $units[$key];
                $pcip->qty = $qty[$key];
                $pcip->wastage = $wastage[$key];
                $pcip->rate = $rate[$key];
                $pcip->cost = $cost[$key];
                $pcip->save();
            }
        }
        return Reply::success('Added Successfully');
    }

    public function viewBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::get();
        $this->projectproduct = ProjectCostItemsProduct::where('project_id',$id)->groupBy('title')->get();
        $this->title = Title::where('id',$title)->first();
        $this->id = $id;
        return view('client.projects.view-boq', $this->data);
    }

    public function editBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::get();
        //$this->projectproduct = ProjectCostItemsProduct::where('project_id',$id)->groupBy('title')->get();
        $this->projectproduct = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->get();
        $this->ci = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->groupBy('cost_items_id')->get();
        $this->pc = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->groupBy('title')->first();
        $this->title = Title::where('id',$title)->first();
        $this->id = $id;
        return view('client.projects.edit-boq', $this->data);
    }

    public function updateBoq(Request $request, $id, $title){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $title = $request->title;
        $category = json_encode($request->category);
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $product_category_id = $request->product_category_id;
        $product_brand_id = $request->product_brand_id;
        $units = $request->units;
        $qty = $request->qty;
        $wastage = $request->wastage;
        $rate = $request->rate;
        $cost = $request->cost;
        foreach ($qty as $key=>$value){
            $check = ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('cost_items_id',$cost_item_id[$key])->first();
            if($check){
                $delete = ProjectCostItemsProduct::find($check->id)->delete();
                if($qty !== "" && $qty > 0) {
                    $pcip = new ProjectCostItemsProduct();
                    $pcip->title = $title;
                    $pcip->category = $check->category;
                    $pcip->project_id = $project_id;
                    $pcip->cost_items_id = $cost_item_id[$key];
                    $pcip->product_category_id = $product_category_id[$key];
                    $pcip->product_brand_id = $product_brand_id[$key];
                    $pcip->unit = $units[$key];
                    $pcip->qty = $qty[$key];
                    $pcip->wastage = $wastage[$key];
                    $pcip->rate = $rate[$key];
                    $pcip->cost = $cost[$key];
                    $pcip->save();
                }
            }else {
                if ($qty !== "" && $qty > 0) {
                    $pcip = new ProjectCostItemsProduct();
                    $pcip->title = $title;
                    $pcip->category = $category;
                    $pcip->project_id = $project_id;
                    $pcip->cost_items_id = $cost_item_id[$key];
                    $pcip->product_category_id = $product_category_id[$key];
                    $pcip->product_brand_id = $product_brand_id[$key];
                    $pcip->unit = $units[$key];
                    $pcip->qty = $qty[$key];
                    $pcip->wastage = $wastage[$key];
                    $pcip->rate = $rate[$key];
                    $pcip->cost = $cost[$key];
                    $pcip->save();
                }
            }
        }
        return redirect()->route('client.projects.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function scheduling(Request $request, $ganttProjectId = '')
    {
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($ganttProjectId);
        $this->employees = User::allEmployees();
        $this->costitem = ProjectCostItemsProduct::join('cost_items', 'cost_items.id', '=', 'project_cost_items_product.cost_items_id')
            ->select('cost_items.id','cost_items.cost_item_name')->groupBy('project_cost_items_product.cost_items_id')->get();
        $data = array();
        $links = array();
        $projects = ProjectCostItemsProduct::join('scheduling_due','scheduling_due.project_id','=','project_cost_items_product.project_id');
        //$projects = $projects->where('scheduling_due.cost_item_id','=','project_cost_items_product.cost_items_id');
        $projects = $projects->where('project_cost_items_product.project_id', '=', $ganttProjectId);
        $projects = $projects->select('project_cost_items_product.id', 'project_cost_items_product.category', 'project_cost_items_product.cost_items_id', 'scheduling_due.start_date', 'scheduling_due.deadline', 'project_cost_items_product.completion_percent');
        //$projects = $projects->groupBy('project_cost_items_product.cost_items_id');
        $projects = $projects->get();
        $id = 0; //count for gantt ids
        foreach ($projects as $project) {
            $id = $id + 1;
            $projectId = $id;
            $cii = $project->cost_items_id;
            $costname = CostItems::where('id',$cii)->first();
            // TODO::ProjectDeadline to do
            $projectDuration = 0;
            if ($project->deadline) {
                $projectDuration = Carbon::parse($project->deadline)->diffInDays(Carbon::parse($project->start_date));
            }
            $data[] = [
                'id' => $projectId,
                'text' => ucwords($costname->cost_item_name),
                'start_date' => $project->start_date,
                'duration' => $projectDuration,
                'progress' => $project->completion_percent / 100,
                'project_id' => $ganttProjectId,
                'dependent_task_id' => null,
                'cost_items_id' => $cii,
            ];
            $tasks = Task::projectCostOpenTasks($project->id, $project->cost_item_id);
            foreach ($tasks as $key => $task) {
                $id = $id + 1;
                $taskDuration = $task->due_date->diffInDays($task->start_date);
                $taskDuration = $taskDuration + 1;
                $data[] = [
                    'id' => $task->id,
                    'text' => ucfirst($task->heading),
                    'start_date' => (!is_null($task->start_date)) ? $task->start_date->format('Y-m-d') : $task->due_date->format('Y-m-d'),
                    'duration' => $taskDuration,
                    'parent' => $projectId,
                    'users' => [
                        ucwords($task->user->name)
                    ],
                    'taskid' => $task->id,
                    'dependent_task_id' => $task->dependent_task_id
                ];
                $links[] = [
                    'id' => $id,
                    'source' => $task->dependent_task_id != '' ? $task->dependent_task_id : $projectId,
                    'target' => $task->id,
                    'type' => $task->dependent_task_id != '' ? 0 : 1
                ];
            }

        }
        $ganttData = [
            'data' => $data,
            'links' => $links
        ];
        $this->ganttProjectId = $ganttProjectId;
        return view('client.projects.scheduling', $this->data);
    }

    public function schedulingData($ganttProjectId = '')
    {
        $data = array();
        $links = array();
        $projects = ProjectCostItemsProduct::join('scheduling_due','scheduling_due.project_id','=','project_cost_items_product.project_id')
            ->where('scheduling_due.cost_item_id','=','project_cost_items_product.cost_items_id');
        if($ganttProjectId != '')
        {
            $projects = $projects->where('project_cost_items_product.project_id', '=', $ganttProjectId);
        }
        $projects = $projects->select('project_cost_items_product.id', 'project_cost_items_product.category', 'project_cost_items_product.cost_items_id', 'scheduling_due.start_date', 'scheduling_due.deadline', 'project_cost_items_product.completion_percent');
        $projects = $projects->groupBy('project_cost_items_product.cost_items_id');
        $projects = $projects->get();
        $id = 0; //count for gantt ids
        foreach ($projects as $project) {
            $id = $id + 1;
            $projectId = $id;
            $cii = $project->cost_items_id;
            $costname = CostItems::where('id',$cii)->first();
            // TODO::ProjectDeadline to do
            $projectDuration = 0;
            if ($project->deadline) {
                $projectDuration = Carbon::parse($project->deadline)->diffInDays(Carbon::parse($project->start_date));
            }
            $data[] = [
                'id' => $projectId,
                'text' => ucwords($costname->cost_item_name),
                'start_date' => $project->start_date,
                'duration' => $projectDuration,
                'progress' => $project->completion_percent / 100,
                'project_id' => $ganttProjectId,
                'dependent_task_id' => null,
                'cost_items_id' => $cii,
                'title' => $project->title,
            ];
            $links[] = [
                'id' => $id,
                'source' => $project->id,
                'target' => $project->id,
                'type' => $project->id
            ];
        }

        $ganttData = [
            'data' => $data,
            'links' => $links
        ];
        return response()->json($ganttData);
    }

    public function schedulingDue(Request $request, $projectId)
    {
        $due = new SchedulingDue();
        $due->title = $request->title;
        $due->project_id = $request->project_id;
        $due->cost_item_id = $request->cost_item_id;
        $due->dependent_id = $request->dependent_id;
        $due->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');;
        $due->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');;
        $due->assign_to = implode(',',$request->assign_to);
        $due->save();
        return back();
    }

    public function ProjectFinalQty(Request $request, $id){
        $title = $request->title;
        $category = $request->category;
        $project_id = $request->project_id;
        $cost_item_id = $request->cost_item_id;
        $qty = $request->total_qty;
        $total_amount = $request->total_amount;
        foreach ($qty as $key=>$value) {
            ProjectCostItemsFinalQty::where('title', $title)->where('project_id', $id)->where('cost_item_id', $cost_item_id[$key])->delete();
        }
        foreach ($qty as $key=>$value) {
            if ($value != '' && $value != "0") {
                $pcip = new ProjectCostItemsFinalQty();
                $pcip->title = $title;
                $pcip->category = $category[$key];
                $pcip->project_id = $project_id;
                $pcip->cost_item_id = $cost_item_id[$key];
                $pcip->qty = $value;
                $pcip->total_amount = $total_amount[$key];
                $pcip->save();
            }
        }
        return redirect()->route('client.projects.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function titleCreate(Request $request, $id){
        $title = $request->title;
        $t = new Title();
        $t->title = $title;
        $t->save();
        return redirect()->route('client.projects.boq',[$id])->with('success', 'Successfully updated!');
    }

    public function getFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
        foreach($this->files as $file){
            $path = $file->id;
            $html .= '<tr>
                                <td>';
            if($file->type == 'file') {
                $html .= '<i class="ti-file"></i> ' . $file->filename;
            }else {
                $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
            }
            $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
            if($file->type == 'file') {
                $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');"
                                               data-toggle="tooltip" data-original-title="Select File"
                                               class="btn btn-info btn-circle"><i
                                                        class="fa fa-check"></i></a>';
            }
            if($file->type == 'folder') {
                $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');"
                                               data-toggle="tooltip" data-original-title="Open"
                                               class="btn btn-info btn-circle"><i
                                                        class="fa fa-folder-open-o"></i></a>';
            }
            $html .='</tr>';
        }
        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
        foreach($this->files as $file){
            $path = $file->id;
            $html .= '<tr>
                                <td>';
            if($file->type == 'file') {
                $html .= '<i class="ti-file"></i> ' . $file->filename;
            }else {
                $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');">
                                        <i class="ti-folder"></i> ' . $file->filename . '
                                    </a>';
            }
            $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
            if($file->type == 'file') {
                $html .= '<a href="javascript:;" onclick="selectFile('.$file->id.');"
                                       data-toggle="tooltip" data-original-title="Select File"
                                       class="btn btn-info btn-circle"><i
                                        class="fa fa-check"></i></a>';
            }
            if($file->type == 'folder') {
                $html .= '<a href="javascript:;" onclick="openFolder('.$file->id.');"
                                       data-toggle="tooltip" data-original-title="Open"
                                       class="btn btn-info btn-circle"><i
                                        class="fa fa-folder-open-o"></i></a>';
            }
            $html .='</td>
                            </tr>';
        }
        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';

        return $html;
    }

    public function showFiles(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $this->files = FileManager::where('parent',$parent)->where('project_id',$id)->get();
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->id = $id;
        return view('client.projects.project-files.show', $this->data);
    }

    public function selectFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentFiles();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteFile($id)
    {
        $project = ProjectAttachmentFiles::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function getDesignFiles(){
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$parent)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
        foreach($this->files as $file){
            $path = $file->id;
            $html .= '<tr>
                                <td>';
            if($file->type == 'file') {
                $html .= '<i class="ti-file"></i> ' . $file->filename;
            }else {
                $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                            <i class="ti-folder"></i> ' . $file->filename . '
                                        </a>';
            }
            $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
            if($file->type == 'file') {
                $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');"
                                               data-toggle="tooltip" data-original-title="Select File"
                                               class="btn btn-info btn-circle"><i
                                                        class="fa fa-check"></i></a>';
            }
            if($file->type == 'folder') {
                $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');"
                                               data-toggle="tooltip" data-original-title="Open"
                                               class="btn btn-info btn-circle"><i
                                                        class="fa fa-folder-open-o"></i></a>';
            }
            $html .='</tr>';
        }
        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function openDesignFolder($id){
        $userid = $this->user->id;
        $this->files = FileManager::where('parent',$id)->where('user_id',$userid)->orderBy('type', 'DESC')->get();
        $html = '<ul class="list-group" id="files-list">
            <li class="list-group-item">
                <div class="table-responsive">
                    <table class="table table-stripped">
                        <thead>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </thead>
                        <tbody>';
        foreach($this->files as $file){
            $path = $file->id;
            $html .= '<tr>
                                <td>';
            if($file->type == 'file') {
                $html .= '<i class="ti-file"></i> ' . $file->filename;
            }else {
                $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');">
                                        <i class="ti-folder"></i> ' . $file->filename . '
                                    </a>';
            }
            $html .='</td>
                                <td>'.$file->type.'</td>
                                <td>'.$file->size .'Bytes</td>
                                <td><span class="m-l-10">'.date("d-m-Y", strtotime($file->created_at)).'</span></td>
                                <td>';
            if($file->type == 'file') {
                $html .= '<a href="javascript:;" onclick="selectDesignFile('.$file->id.');"
                                       data-toggle="tooltip" data-original-title="Select File"
                                       class="btn btn-info btn-circle"><i
                                        class="fa fa-check"></i></a>';
            }
            if($file->type == 'folder') {
                $html .= '<a href="javascript:;" onclick="openDesignFolder('.$file->id.');"
                                       data-toggle="tooltip" data-original-title="Open"
                                       class="btn btn-info btn-circle"><i
                                        class="fa fa-folder-open-o"></i></a>';
            }
            $html .='</td>
                            </tr>';
        }
        $html .='</tbody>
                    </table>
                </div>
            </li>
        </ul>';
        return $html;
    }

    public function showDesignFiles(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->files = ProjectAttachmentDesigns::where('project_id',$id)->get();
        $this->id = $id;
        return view('client.projects.project-files.show-design', $this->data);
    }

    public function selectDesignFile(Request $request, $id){
        $xid = explode('-',$id);
        $id = $xid[0];
        $pid = $xid[1];
        $paf = new ProjectAttachmentDesigns();
        $paf->project_id = $pid;
        $paf->file_id = $id;
        $paf->save();
    }

    public function deleteDesignFile($id)
    {
        $project = ProjectAttachmentDesigns::findOrFail($id);
        $project->forceDelete();
        return Reply::success(__('File deleted successfully'));
    }

    public function projectEditFile(Request $request, $projectId, $fileId){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($projectId);
        $parent = 0;
        if(isset($_GET['path'])){
            $parent = $_GET['path'];
        }
        $file = FileManager::findOrFail($fileId);
        $parent = $file->parent;
        $filePath = $file->hashname;
        $mime = explode('.', $filePath);
        while ($parent){
            $filep = \App\FileManager::findOrFail($parent);
            $filePath = $filep->filename.'/'.$filePath;
            $parent = $filep->parent;
        }
        $this->urlx = $filePath;
        $this->queryUrl = '&user='.Auth::user()->id.'&project='.$projectId.'&file_id='.$fileId.'&username='.Auth::user()->name;
        $this->foldername = FileManager::where('id',$parent)->first();
        $this->file = $file;
        $this->id = $projectId;
        return view('client.projects.project-files.edit-file', $this->data);
    }

    public function sourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return view('client.projects.sourcing-package', $this->data);
    }

    public function addSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$id)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->id = $id;
        return view('client.projects.sourcing-package-create', $this->data);
    }

    public function storeSourcingPackages(Request $request, $id){
        $source = new SourcingPackage();
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            $pro = $request->products;
            for($j = 0; $j < count($pro); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $pro[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('client.projects.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function editSourcingPackages(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->id = $pid;
        return view('client.projects.sourcing-package-edit', $this->data);
    }

    public function updateSourcingPackages(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $source = SourcingPackage::findOrFail($id);
        $source->company_id = $this->user->company_id;
        $source->title = $request->title;
        $t = '';
        if($request->category !== null) {
            $t = json_encode($request->category);
        }
        $category = $t;
        $cf = '';
        if($request->create_from !== null) {
            $cf = json_encode($request->create_from);
        }
        $create_from = $cf;
        $type = '';
        if($request->type !== null) {
            $type = json_encode($request->type);
        }
        $ty = $type;
        $source->create_from = $create_from;
        $source->type = $ty;
        $source->category = $category;
        $source->save();
        if($source->id){
            SourcingPackageProduct::where('sourcing_package_id',$source->id)->delete();
            $cfp = $request->products;
            for($j = 0; $j < count($cfp); $j++ ) {
                $sourcep = new SourcingPackageProduct();
                $sourcep->sourcing_package_id = $source->id;
                $sourcep->product_id = $cfp[$j];
                $sourcep->save();
            }
        }
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->id = $id;
        return Reply::redirect(route('client.projects.sourcingPackages',[$id]), __('Added Successfully'));
    }

    public function getProducts(Request $request){
        $html = '';
        $cfk = $request->create_from;
        for($k = 0; $k < count($cfk); $k++ ) {
            $cats = ProjectCostItemsProduct::where('title', $cfk[$k])->where('project_id', $request->project_id)->groupBy('title')->get();
            foreach ($cats as $cat) {
                $xcat = explode(',', $cat->category);

                for ($i = 0; $i < count($xcat); $i++) {
                    $name = BoqCategory::where('id', $xcat[$i])->first();
                    if($name->id !== null) {
                        $html .= '<option value="' . $name->id . '">' . $name->title . '</option>';
                    }
                }
            }
        }
        return $html;
    }

    public function filterData(Request $request){
        $html = '';
        $cf = $request->create_from;
        $html .= '
                <input type="hidden" name="project_id" value="'.$request->project_id.'"/>
                <input type="hidden" name="title" value="'.$request->title.'"/>';
        foreach($request->create_from as $caf) {
            $html .= '<input type="hidden" name="create_from[]" value="' . $caf . '"/>';
        }
        foreach($request->category as $c) {
            $html .= '<input type="hidden" name="category[]" value="' . $c . '"/>';
        }
        foreach($request->type as $t) {
            $html .= '<input type="hidden" name="type[]" value="' . $t . '"/>';
        }
        $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Product</b></div>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                    <div style="padding: 5px;"><b>Quantity</b></div>
                </div>';
        for($j = 0; $j < count($cf); $j++ ) {
            $products = ProjectCostItemsProduct::where('title',$cf[$j])->where('project_id', $request->project_id)
                ->join('category', 'category.id', '=', 'project_cost_items_product.product_category_id')
                ->join('product_type', 'product_type.product_id', '=', 'project_cost_items_product.product_category_id')
                ->where('product_type.type',$request->type[$j])
                ->select('category.name as name', 'category.id as id')
                ->get();
            foreach ($products as $product) {
                $fqty = DB::table('project_cost_item_final_qty')
                    ->where('title',$cf[$j])
                    ->where('project_id', $request->project_id)
                    ->sum('qty');
                $html .= '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;"> 
                        <input type="checkbox" name="products[]" value="' . $product->id . '" class="form-check-input"> 
                        ' . $product->name . '
                    </label>
                </div>
                <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                    <label style="padding: 5px;">';
                if($fqty) $html .= $fqty;
                $html .= '</label>
                </div>';
            }
        }
        return $html;
    }

    public function deleteSourcingPackages($id)
    {
        $project = SourcingPackage::findOrFail($id);
        $project->forceDelete();
        $sp = SourcingPackageProduct::where('sourcing_package_id',$id)->delete();
        return Reply::success(__('Package deleted successfully'));
    }

    public function createTender(Request $request,$pid, $id ){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->pid = $pid;
        $this->id = $id;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        return view('client.projects.create-tender', $this->data);
    }

    public function getBrandsTag(Request $request){
        $q = $_GET['query'];
        $query = ProductBrand::where('name','LIKE','%'.$q.'%')->select('id', 'name')->get();
        $return = array();
        if($query){
            // fetch object array
            foreach($query as $q) {
                $return[] = $q;
            }
        }
        $json = json_encode($return);
        return $json;
    }

    public function saveTenders(Request $request){
        $tender = new Tenders();
        $tender->company_id = $this->user->company_id;
        $tender->project_id = $request->project_id;
        $tender->sourcing_package_id = $request->sourcing_package_id;
        $tender->name = $request->tender_name;
        $tender->save();
        if($tender->id){
            $cfp = $request->conditions;
            for($j = 1; $j < count($cfp); $j++ ) {
                $sourcep = new TenderBidding();
                $sourcep->tender_id = $tender->id;
                $sourcep->condition_id = $cfp[$j];
                $sourcep->terms = $request->terms[$j];
                $sourcep->save();
            }
            foreach($request->products as $key=>$pi ) {
                $tp = new TendersProduct();
                $tp->tender_id = $tender->id;
                $tp->products = $pi;
                $tp->qty = $request->qty[$key];
                $tp->brands = $request->brand_ids[$key];
                $tp->save();
            }
        }
        return Reply::dataOnly(['taskID' => $tender->id]);
    }

    public function storeTenderFiles(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.cloud');
                $file = new TendersFiles();
                $file->user_id = $this->user->id;
                $file->tender_id = $request->task_id;
                $file->company_id = $this->user->company_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/tender-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('tender-files/'.$request->task_id, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'tender-files')
                            ->first();
                        if(!$dir) {
                            Storage::cloud()->makeDirectory('tender-files');
                        }
                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();
                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }
                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());
                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());
                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('tender-files/'.$request->task_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/tender-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
            }
        }
        return Reply::redirect(route('client.projects.create-tender'), __('modules.projects.projectUpdated'));
    }

    public function tendersList(Request $request, $pid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$id)->first();
        $this->tenders = Tenders::where('company_id',$this->user->company_id)
            ->where('project_id',$pid)->where('sourcing_package_id',$id)->get();
        $this->id = $id;
        $this->pid = $pid;
        return view('client.projects.tenders-list', $this->data);
    }

    public function editTender(Request $request, $pid, $id, $sid){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$sid)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('id',$id)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$id)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        return view('client.projects.edit-tender', $this->data);
    }

    public function assignTender(Request $request, $pid, $sid, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($pid);
        $this->tender = Tenders::where('company_id',$this->user->company_id)
            ->where('id',$id)->first();
        $this->id = $id;
        $this->pid = $pid;
        $this->sid = $sid;
        $this->conditions = Condition::all();
        $this->brands = ProductBrand::all();
        $this->packages = SourcingPackage::where('company_id',$this->user->company_id)->get();
        $this->costitems = ProjectCostItemsProduct::where('project_id',$pid)
            ->join('title','title.id','=','project_cost_items_product.title')
            ->select('title.id','title.title')
            ->groupBy('project_cost_items_product.title')
            ->get();
        $this->package = SourcingPackage::where('company_id',$this->user->company_id)->where('id',$sid)->first();
        $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$sid)
            ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
            ->select('category.name as name', 'category.id as id')
            ->get();
        $this->cons = TendersCondition::where('tender_id', $this->tender->id)->get();
        $this->pros = TendersProduct::where('tender_id', $this->tender->id)->get();
        $this->files = TendersFiles::where('tender_id', $this->tender->id)->get();
        $this->employees = User::allEmployees();
        return view('client.projects.assign-tender', $this->data);
    }

    public function storeAssignTender(Request $request){
        foreach($request->user_id as $user) {
            $tenderAssign = new TenderAssign();
            $tenderAssign->company_id = $this->user->company_id;
            $tenderAssign->project_id = $request->project_id;
            $tenderAssign->tender_id = $request->tender_id;
            $tenderAssign->sourcing_id = $request->sourcing_id;
            $tenderAssign->user_id = $user;
            $tenderAssign->save();
        }
        return back();
    }

    public function bidding(Request $request, $company_id, $project_id, $tender_id, $sourcing_id, $user_id){
        $this->assign_tender = TendersCondition::where('company_id', $company_id)
            ->where('project_id', $project_id)
            ->where('tender_id', $tender_id)
            ->where('sourcing_id', $sourcing_id)
            ->where('user_id', $user_id)
            ->first();
        if($this->assign_tender){
            $this->tender = Tenders::where('company_id',$company_id)
                ->where('id',$tender_id)->first();
            $this->company_id = $company_id;
            $this->project_id = $project_id;
            $this->tender_id = $tender_id;
            $this->sourcing_id = $sourcing_id;
            $this->user_id = $user_id;
            $this->conditions = Condition::all();
            $this->brands = ProductBrand::all();
            $this->packages = SourcingPackage::where('company_id',$company_id)->get();
            $this->costitems = ProjectCostItemsProduct::where('project_id',$project_id)
                ->join('title','title.id','=','project_cost_items_product.title')
                ->select('title.id','title.title')
                ->groupBy('project_cost_items_product.title')
                ->get();
            $this->package = SourcingPackage::where('id',$sourcing_id)->first();
            $this->packageProducts = SourcingPackageProduct::where('sourcing_package_id',$sourcing_id)
                ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
                ->select('category.name as name', 'category.id as id')
                ->get();
            $this->cons = TendersCondition::where('tender_id', $tender_id)->get();
            $this->pros = TendersProduct::where('tender_id', $tender_id)->get();
            $this->files = TendersFiles::where('tender_id', $tender_id)->get();
        }
        return view('client.projects.bidding', $this->data);
    }

    public function tenderBiddings(Request $request){
        $this->tenders = Tenders::where('company_id',$this->user->company_id)->get();
        return view('client.projects.tenders-biddings', $this->data);
    }

    public function tenderBiddingDetails(Request $request, $id){
        $this->tenders = TenderBidding::where('company_id',$this->user->company_id)->where('tender_id',$id)->get();
        $this->tender = Tenders::where('company_id',$this->user->company_id)->where('id',$id)->first();
        return view('client.projects.tender-bidding-detail', $this->data);
    }
}
