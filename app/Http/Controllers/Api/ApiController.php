<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ProjectActivity;
use App\Rfi;
use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{

    public function getRfi(Request $request)
    {
          $data = array();
          $data['rfi'] = Rfi::all('id','title','rfi_manager');
          return json_encode($data);
    }

    public function logProjectActivity($projectId, $text)
    {
        $activity = new ProjectActivity();
        $activity->project_id = $projectId;
        $activity->activity = $text;
        $activity->save();
    }

}