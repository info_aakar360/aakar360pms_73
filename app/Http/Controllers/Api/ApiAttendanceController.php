<?php

namespace App\Http\Controllers\Api;

use App\AdvanceRules;
use App\AppProject;
use App\Attendance;
use App\AttendanceRules;
use App\AttendanceSetting;
use App\Company;
use App\Designation;
use App\Employee;
use App\Helper\Reply;
use App\Holiday;
use App\Http\Controllers\Controller;
use App\Http\Requests\Attendance\StoreAttendance;
use App\Http\Requests\Attendance\GeneralRulesRequest;
use App\Leave;
use App\Project;
use App\RulesUser;
use App\Skill;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Dropbox\Client;
use Yajra\DataTables\Facades\DataTables;


class ApiAttendanceController extends Controller
{
    // Validate API_KEY
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    // Validate Token for validating user login
    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 300;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 300;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 300;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function __construct(Request $request) {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            // checking permission if user has access or not
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
            // Getting Attendance setting data
            $this->attendanceSettings = AttendanceSetting::first();
            //Getting Maximum Check-ins in a day
            if(!empty($this->attendanceSettings)){
                $this->maxAttandenceInDay = $this->attendanceSettings->clockin_in_day;
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            //dd($user);
            try{
                $project_id = $request->project_id;
                $proComId = Project::where('id',$project_id)->orderBy('id','DESC')->first();
                $companyid = $user->company_id;
                if(!empty($proComId->company_id)){
                    $companyid = $proComId->company_id;
                }
                $openDays = json_decode($this->attendanceSettings->office_open_days);
                $data['startDate'] = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
                $data['endDate'] = Carbon::now()->timezone($this->global->timezone);
                $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
                $eData = array();
                if($request->self == 'yes') {
                    //$employees = Attendance::singleAttendanceByDate($user->id, $date, $proComId->company_id)->get();
                    DB::statement("SET @attendance_date = '$date'");
                    $employees = Employee::join('users','users.id','=','employee.user_id')
                        ->leftJoin(
                            'attendances', function ($join) use ($user,$date,$companyid) {
                            $join->on('users.id', '=', 'attendances.user_id')
                                ->where(function($query) use($date){
                                    $query->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $date)->orWhere(DB::raw('DATE(attendances.clock_out_time)'), '=', $date)
                                        ->orWhere(DB::raw('DATE(attendances.admin_clock_in)'), '=', $date);
                                })
                                /*->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $date)*/
                                ->where('attendances.company_id', '=', $companyid);
                            }
                        )
                        ->select(
                            DB::raw("(select count('atd.id') from attendances as atd where atd.user_id = users.id and DATE(atd.clock_in_time)  =  '".$date."' and DATE(atd.clock_out_time)  =  '".$date."' ) as total_clock_in"),
                            DB::raw("(select count('atdn.id') from attendances as atdn where atdn.user_id = users.id and DATE(atdn.clock_in_time)  =  '".$date."' ) as clock_in"),
                            DB::raw("( select count('atd.id') from attendances as atd where atd.user_id = users.id and DATE(atd.admin_clock_in)  =  '".$date."' and DATE(atd.admin_clock_out)  =  '".$date."' ) as total_admin_clock_in"),
                            DB::raw("( select count('atdn.id') from attendances as atdn where atdn.user_id = users.id and DATE(atdn.admin_clock_in)  =  '".$date."' ) as clock_in_admin"),
                            'users.id',
                            'users.name',
                            'attendances.attendance_date',
                            'attendances.clock_in_ip',
                            'attendances.clock_in_time',
                            'attendances.clock_out_time',
                            'attendances.admin_clock_in',
                            'attendances.admin_clock_out',
                            'attendances.late',
                            'attendances.half_day',
                            'attendances.working_from',
                            'attendances.clock_in_image',
                            'attendances.clock_out_image',
                            'attendances.clock_in_lat',
                            'attendances.clock_in_lang',
                            'attendances.clock_out_lat',
                            'attendances.clock_out_lang',
                            'attendances.clock_out_address',
                            'attendances.clock_in_address',
                            'users.image',
                            DB::raw('@attendance_date as atte_date'),
                            'attendances.id as attendance_id'
                        )
                        ->where('employee.user_id', '=', $user->id)
                        ->where('employee.company_id',$companyid)
                        ->groupBy('employee.user_id')
                        ->orderBy('users.name', 'asc')->get();

                    //$employees = Attendance::where('company_id',$proComId->company_id)->where('user_id',$user->id)->where(DB::raw('DATE(clock_in_time)'), '=', $date)->get();
                    foreach ($employees as $employee){
                        $attStatus = 'Absent';
                        $attendancerules = '';
                        $totalDuration = '00:00:00 Hrs';
                        $totalDuration3 = '00:00:00 Hrs';

                        $rules = RulesUser::where('user_id',$employee->id)->first();
                        if(!empty($rules)){
                            $attendancerules = AttendanceRules::where('id',$rules->rules_id)->first();
                        }
                        if(!empty($employee->clock_in_time) && !empty($employee->clock_out_time)){
                            $totalDuration = Carbon::parse($employee->clock_out_time)->diff(Carbon::parse($employee->clock_in_time))->format('%H:%I:%S')." Hrs";
                            $attStatus = 'Present';
                        } if(!empty($employee->admin_clock_in) && !empty($employee->admin_clock_out)){
                            $totalDuration3 = Carbon::parse($employee->admin_clock_out)->diff(Carbon::parse($employee->admin_clock_in))->format('%H:%I:%S')." Hrs";
                            $attStatus = 'Present';
                        }
                        $eData[] = array(
                            'total_clock_in' => $totalDuration,
                            'total_admin_clock_in' => $totalDuration3,
                            'clock_in' => (!empty($employee->clock_in) ? $employee->clock_in : 0),
                            'clock_in_admin' => (!empty($employee->admin_clock_in) ? $employee->admin_clock_in : 0),
                            'id' => (!empty($employee->id) ? $employee->id : 0),
                            'name' => (!empty($employee->name) ? ucwords($employee->name) : ''),
                            'clock_in_ip' => (!empty($employee->clock_in_ip) ? $employee->clock_in_ip : ''),
                            'attendance_date' => (!empty($employee->attendance_date) ? date('d M Y', strtotime($employee->attendance_date))  : ''),
                            'clock_in_time' => (!empty($employee->clock_in_time) ? date('h:i A', strtotime(explode(' ', $employee->clock_in_time)[1]))  : ''),
                            'clock_out_time' => (!empty($employee->clock_out_time) ? date('h:i A', strtotime(explode(' ', $employee->clock_out_time)[1])) : ''),
                            'admin_clock_in' => (!empty($employee->admin_clock_in) ? date('h:i A', strtotime(explode(' ', $employee->admin_clock_in)[1]))  : ''),
                            'admin_clock_out' => (!empty($employee->admin_clock_out) ? date('h:i A', strtotime(explode(' ', $employee->admin_clock_out)[1])) : ''),
                            'late' => (!empty($employee->late) ? $employee->late : ''),
                            'half_day' => (!empty($employee->half_day) ? $employee->half_day : ''),
                            'working_from' => (!empty($employee->working_from) ? $employee->working_from : ''),
                            'clock_in_image' => (!empty($employee->clock_in_image) ?  uploads_url().'attendance-image/'.$employee->clock_in_image : ''),
                            'clock_out_image' => (!empty($employee->clock_out_image) ? uploads_url().'attendance-image/'.$employee->clock_out_image : ''),
                            'clock_in_lat' => (!empty($employee->clock_in_lat) ? $employee->clock_in_lat : ''),
                            'clock_in_lang' => (!empty($employee->clock_in_lang) ? $employee->clock_in_lang : ''),
                            'clock_out_lat' => (!empty($employee->clock_out_lat) ? $employee->clock_out_lat : ''),
                            'clock_out_lang' => (!empty($employee->clock_out_lang) ? $employee->clock_out_lang : ''),
                            'clock_out_address' => (!empty($employee->clock_out_address) ? $employee->clock_out_address : ''),
                            'clock_in_address' => (!empty($employee->clock_in_address) ? $employee->clock_in_address : ''),
                            'image' => (!empty($employee->image) ? uploads_url().'avatar/'.$employee->image : ''),
                            'designation_name' => (!empty($employee->designation_name) ? $employee->designation_name : ''),
                            'atte_date' => (!empty($employee->atte_date) ? date('d M Y', strtotime($employee->atte_date))  : ''),
                            'attendance_id' => (!empty($employee->attendance_id) ? $employee->attendance_id : 0),
                            'office_timing' => ((!empty($attendancerules) && !empty($attendancerules->shift_in_time)  && !empty($attendancerules->shift_out_time)) ? date('h:i', strtotime($attendancerules->shift_in_time)) .' - '. date('h:i', strtotime($attendancerules->shift_out_time)) : '12:00 AM-11:59 PM'),
                            'status' => $attStatus,
                        );
                    }
                }else {
                    //$employees = Attendance::attendanceByDate($date)->get();
                    $employees = Employee::where('company_id',$companyid)->where('user_type','employee')->get();
                    foreach ($employees as $key=>$employee){
                        $attendance = Attendance::where('company_id',$companyid)->where('user_id',$employee->user_id)
                            ->where('attendance_date',$date)->first();
                        $attendancerules = '';
                        $totalDuration1 = '00:00:00 Hrs';
                        $totalDuration2 = '00:00:00 Hrs';

                            $attStatus = 'Absent';

                        $rules = RulesUser::where('user_id',$employee->user_id)->first();
                        if(!empty($rules)){
                            $attendancerules = AttendanceRules::where('id',$rules->rules_id)->first();
                        }
                        if(!empty($attendance->clock_in_time) && !empty($attendance->clock_out_time)){
                            $totalDuration1 = Carbon::parse($attendance->clock_out_time)->diff(Carbon::parse($attendance->clock_in_time))->format('%H:%I:%S')." Hrs";
                            $attStatus = 'Present';
                        }elseif(!empty($attendance->admin_clock_in) && !empty($attendance->admin_clock_out)){
                            $totalDuration2 = Carbon::parse($attendance->admin_clock_out)->diff(Carbon::parse($attendance->admin_clock_in))->format('%H:%I:%S')." Hrs";
                            $attStatus = 'Present';
                        }
                        $eData[] = array(
                            'attendance_id' => (!empty($attendance->id) ? $attendance->id : 0),
                            'emp_id' => (int)$employee->id,
                            'id' => (int)$employee->user_id,
                            'company_id' => (int)$employee->company_id,
                            'name' => $employee->name,
                            'total_clock_in' => $totalDuration1,
                            'total_admin_clock_in' => $totalDuration2,
                            'attendance_date' => (!empty($attendance->attendance_date) ? date('d M Y', strtotime($attendance->attendance_date))  : ''),
                            'clock_in' => (!empty($attendance->clock_in_time) ? 1 : 0),
                            'clock_in_admin' => (!empty($attendance->admin_clock_in) ? 1 : 0),
                            'clock_in_ip' => (!empty($attendance->clock_in_ip) ? $attendance->clock_in_ip : ''),
                            'clock_in_time' => (!empty($attendance->clock_in_time) ? date('h:i A', strtotime($attendance->clock_in_time))  : ''),
                            'clock_out_time' => (!empty($attendance->clock_out_time) ? date('h:i A', strtotime($attendance->clock_out_time)) : ''),
                            'admin_clock_in'=>(!empty($attendance->admin_clock_in) ? date('h:i A', strtotime($attendance->admin_clock_in))  : ''),
                            'admin_clock_out'=>(!empty($attendance->admin_clock_out) ? date('h:i A', strtotime($attendance->admin_clock_out))  : ''),
                            'late' => (!empty($attendance->late) ? $attendance->late : ''),
                            'half_day' => (!empty($attendance->half_day) ? $attendance->half_day : ''),
                            'working_from' => (!empty($attendance->working_from) ? $attendance->working_from : ''),
                            'clock_in_image' => (!empty($attendance->clock_in_image) ?  uploads_url().'attendance-image/'.$attendance->clock_in_image : ''),
                            'clock_out_image' => (!empty($attendance->clock_out_image) ? uploads_url().'attendance-image/'.$attendance->clock_out_image : ''),
                            'clock_in_lat' => (!empty($attendance->clock_in_lat) ? $attendance->clock_in_lat : ''),
                            'clock_in_lang' => (!empty($attendance->clock_in_lang) ? $attendance->clock_in_lang : ''),
                            'clock_out_lat' => (!empty($attendance->clock_out_lat) ? $attendance->clock_out_lat : ''),
                            'clock_out_lang' => (!empty($attendance->clock_out_lang) ? $attendance->clock_out_lang : ''),
                            'clock_out_address' => (!empty($attendance->clock_out_address) ? $attendance->clock_out_address : ''),
                            'clock_in_address' => (!empty($attendance->clock_in_address) ? $attendance->clock_in_address : ''),
                            'image' => (!empty($attendance->image) ? uploads_url().'avatar/'.$attendance->image : ''),
                            'designation_name' => (!empty($attendance->designation_name) ? $attendance->designation_name : ''),
                            'atte_date' => (!empty($date) ? date('d M Y', strtotime($date))  : ''),
                            'office_timing' => ((!empty($attendancerules) && !empty($attendancerules->shift_in_time)  && !empty($attendancerules->shift_out_time)) ? date('h:i', strtotime($attendancerules->shift_in_time)) .' - '. date('h:i', strtotime($attendancerules->shift_out_time)) : '12:00 AM-11:59 PM'),
                            'status' => $attStatus,
                        );
                    }
                }
                $data['totalWorkingDays'] = $data['startDate']->diffInDaysFiltered(function(Carbon $date) use ($openDays){
                    foreach($openDays as $day){
                        if($date->dayOfWeek == $day){
                            return $date;
                        }
                    }
                }, $data['endDate']);
                $data['employees'] = $eData;
                $data['daysPresent'] = Attendance::countDaysPresentByUser($data['startDate'], $data['endDate'], $user->id);
                $data['daysLate'] = Attendance::countDaysLateByUser($data['startDate'], $data['endDate'], $user->id);
                $data['halfDays'] = Attendance::countHalfDaysByUser($data['startDate'], $data['endDate'], $user->id);
                $data['holidays'] = Count(Holiday::getHolidayByDates($data['startDate']->format('Y-m-d'), $data['endDate']->format('Y-m-d')));

                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $project_id = $request->project_id;
                $proComId = Project::find($project_id);
                $emplCheck = Employee::where('company_id',$proComId->company_id)->where('user_id',$user->id)->where('user_type','employee')->orderBy('id','DESC')->first();
                if(empty($emplCheck)){
                    $data['action'] = 3;
                    $response['status'] = 200;
                    $response['response'] = $data;
                    $response['message'] = 'You are not eligible for mark your attendance!';
                }else {
                    $startDate = Carbon::parse(date('Y-m-d'))->format('Y-m-d 00:00:00');
                    $endDate = Carbon::parse(date('Y-m-d'))->format('Y-m-d 23:59:00');
                    $attendance = Attendance::where('user_id', $user->id)->where('company_id',$proComId->company_id)
                        ->where('clock_in_time', '>=', $startDate)
                        ->where('clock_in_time', '<=', $endDate)
                        ->orderBy('id', 'DESC')
                        ->first();
                    if (!empty($attendance)) {
                        $attendanceout = Attendance::where('user_id', $user->id)->where('company_id',$proComId->company_id)
                            ->where('clock_in_time', '>=', $startDate)
                            ->where('clock_in_time', '<=', $endDate)
                            ->whereNotNull('clock_out_time')
                            ->first();
                        if (!empty($attendanceout)) {
                            $data['action'] = 2;
                            $data['clock_in_time'] = explode(' ', $attendanceout->clock_in_time)[1];
                            $data['clock_out_time'] = explode(' ', $attendanceout->clock_out_time)[1];
                            $response['status'] = 200;
                            $response['response'] = $data;
                            $response['message'] = 'Attendance already updated';
                        }else {
                            $data['action'] = 1;
                            $data['clock_in_time'] = explode(' ', $attendance->clock_in_time)[1];
                            $response['status'] = 200;
                            $response['response'] = $data;
                        }
                    } else {
                        $data['action'] = 0;
                        $response['status'] = 200;
                        $response['message'] = 'Success';
                        $response['response'] = $data;
                    }
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $project_id = $request->project_id;
                $proComId = Project::find($project_id);
                $date = Carbon::parse(date('Y-m-d'))->format('Y-m-d');
                $clockIn = Carbon::parse($request->clock_in_time)->format('H:i:s');

                $attendance = Attendance::where('user_id', $user->id)->where('company_id',$proComId->company_id)
                    ->where('attendance_date', '=', $date)
                    ->first();
                //dd($attendance);
              /*  $clockInCount = Attendance::getTotalUserClockIn($date, $user->id,$proComId->company_id);

                $startDate = Carbon::parse(date('Y-m-d'))->format('Y-m-d 00:00:00');
                $endDate = Carbon::parse(date('Y-m-d'))->format('Y-m-d 23:59:00');
              */
                $message = '';
                if (empty($attendance)) {
                    $attendance = new Attendance();
                    $attendance->user_id = $user->id;
                    $attendance->emp_id = $request->emp_id ?: 0;
                    $attendance->company_id = $proComId->company_id;
                    $attendance->attendance_date = $date;
                }
                if ($request->action == 1) {
                    if ($request->hasFile('file')) {
                        $storage = storage();
                        $image = $request->file;
                        switch($storage) {
                            case 'local':
                                $destinationPath = 'uploads/attendance-image/';
                                $image->storeAs($destinationPath, $image->hashName());
                                $attendance->clock_out_image = $image->hashName();
                                break;
                            case 's3':
                                $st = Storage::disk('s3')->putFileAs('attendance-image/', $request->file, $image->hashName(), 'public');
                                $attendance->clock_out_image = $image->hashName();
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', 'avatar')
                                    ->first();

                                if(!$dir) {
                                    Storage::cloud()->makeDirectory('avatar');
                                }

                                $directory = $dir['path'];
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->file)
                                    ->first();

                                if ( ! $directory) {
                                    Storage::cloud()->makeDirectory($dir['path'].'/');
                                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                    $directory = $contents->where('type', '=', 'dir')
                                        ->where('image', '=', $request->file)
                                        ->first();
                                }
                                Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());
                                $attendance->clock_out_image = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                                break;
                            case 'dropbox':
                                Storage::disk('dropbox')->putFileAs('attendance-image/'.'/', $request->file, $request->file->getClientOriginalName());
                                $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                                $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                    [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->file->getClientOriginalName()]]
                                );
                                $dropboxResult = $res->getBody();
                                $dropboxResult = json_decode($dropboxResult, true);
                                $attendance->clock_out_image = $dropboxResult['url'];
                                break;
                        }
                        //                $u->image= $request->image->getClientOriginalName();
                    }
                    $attendance->clock_out_time = $date . ' ' . $clockIn;
                    $attendance->clock_out_lat = $request->lat;
                    $attendance->clock_out_lang = $request->lang;
                    $attendance->clock_out_address = $request->address;
                    $attendance->save();
                    $message = 'Attendance Updated Successfully';
                } else {
                    /* Code for clock in time*/
                    $attendance->clock_in_time = $date . ' ' . $clockIn;
                    $attendance->clock_in_lat = $request->lat ?: '';
                    $attendance->clock_in_lang = $request->lang ?: '';
                    $attendance->clock_in_address = $request->address ?: '';
                    if ($request->hasFile('file')) {
                        $storage = storage();
                        $image = $request->file;
                        switch($storage) {
                            case 'local':
                                $destinationPath = 'uploads/attendance-image/';
                                $image->storeAs($destinationPath, $image->hashName());
                                $attendance->clock_in_image = $image->hashName();
                                break;
                            case 's3':
                                $st = Storage::disk('s3')->putFileAs('attendance-image/', $request->file, $image->hashName(), 'public');
                                $attendance->clock_in_image = $image->hashName();
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', 'avatar')
                                    ->first();

                                if(!$dir) {
                                    Storage::cloud()->makeDirectory('avatar');
                                }

                                $directory = $dir['path'];
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->file)
                                    ->first();

                                if ( ! $directory) {
                                    Storage::cloud()->makeDirectory($dir['path'].'/');
                                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                    $directory = $contents->where('type', '=', 'dir')
                                        ->where('image', '=', $request->file)
                                        ->first();
                                }
                                Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());
                                $attendance->clock_in_image = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                                break;
                            case 'dropbox':
                                Storage::disk('dropbox')->putFileAs('uploads/attendance-image//'.'/', $request->file, $request->file->getClientOriginalName());
                                $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                                $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                    [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->file->getClientOriginalName()]]
                                );
                                $dropboxResult = $res->getBody();
                                $dropboxResult = json_decode($dropboxResult, true);
                                $attendance->clock_in_image = $dropboxResult['url'];
                                break;
                        }
                    }
                    $attendance->save();
                    $message = 'Attendance Updated Successfully';
                }
                $response['status'] = 200;
                $response['message'] = $message;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'post-attendance', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        $attendance = Attendance::find($id);
//
//        $this->date = $attendance->clock_in_time->format('Y-m-d');
//        $this->row =  $attendance;
//        $this->clock_in = 1;
//        $this->userid = $attendance->user_id;
//        $this->total_clock_in  = Attendance::where('user_id', $attendance->user_id)
//            ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $this->date)
//            ->whereNull('attendances.clock_out_time')->count();
//        $this->type = 'edit';
//        return view('admin.attendance.attendance_mark', $this->data);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $attendance = Attendance::findOrFail($request->id);
        $date = Carbon::createFromFormat($this->global->date_format, $request->attendance_date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            $clockOut->setTimezone('UTC');
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance->user_id = $request->user_id;
        $attendance->attendance_date = $date;
        $attendance->clock_in_time = $date . ' ' . $clockIn;
        $attendance->clock_in_ip = $request->clock_in_ip;
        $attendance->clock_out_time = $clockOut;
        $attendance->clock_out_ip = $request->clock_out_ip;
        $attendance->working_from = $request->working_from;
        $attendance->late = ($request->has('late'))? 'yes' : 'no';
        $attendance->half_day = ($request->has('half_day'))? 'yes' : 'no';
        if ($request->file) {
            if (!file_exists('uploads/attendance-image')) {
                mkdir('uploads/attendance-image', 0777, true);
            }
            define('UPLOAD_DIR', 'uploads/attendance-image/');
            $img = $request->file;
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $dataimage = base64_decode($img);
            $file = UPLOAD_DIR . uniqid() . '.png';
            $success = file_put_contents($file, $dataimage);
            if($request->btnpressed == 'clockin'){
                $attendance->clock_in_image = $file;
            }
            if($request->btnpressed == 'clockout'){
                $attendance->clock_out_image = $file;
            }
        }
        $attendance->save();

        return Reply::success(__('messages.attendanceSaveSuccess'));

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Attendance::destroy($id);
        return Reply::success(__('messages.attendanceDelete'));
    }

    public function data(Request $request){
        //dd($request->all());
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceByDate($date);
        if (!empty($request->user_id)){
            $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);
        }

        return DataTables::of($attendances)
            ->editColumn('id', function ($row) use($request){
                return view('admin.attendance.attendance_list', ['row' => $row, 'global' => $this->global, 'maxAttandenceInDay' => $this->maxAttandenceInDay])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();
    }

    public function singleData(Request $request){

        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        if(!empty($request)){}
        $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);
        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_list', ['row' => $row, 'global' => $this->global, 'maxAttandenceInDay' => $this->maxAttandenceInDay])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();
    }


    public function refreshCount($startDate = null, $endDate = null, $userId = null){

        $openDays = json_decode($this->attendanceSettings->office_open_days);
        $startDate = Carbon::createFromFormat('!Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('!Y-m-d', $endDate)->addDay(1); //addDay(1) is hack to include end date

        $totalWorkingDays = $startDate->diffInDaysFiltered(function(Carbon $date) use ($openDays){
            foreach($openDays as $day){
                if($date->dayOfWeek == $day){
                    return $date;
                }
            }
        }, $endDate);
        $daysPresent = Attendance::countDaysPresentByUser($startDate, $endDate, $userId);
        $daysLate = Attendance::countDaysLateByUser($startDate, $endDate, $userId);
        $halfDays = Attendance::countHalfDaysByUser($startDate, $endDate, $userId);
        $daysAbsent = (($totalWorkingDays - $daysPresent) < 0) ? '0' : ($totalWorkingDays - $daysPresent);
        $holidays = Count(Holiday::getHolidayByDates($startDate->format('Y-m-d'), $endDate->format('Y-m-d')));

        return Reply::dataOnly(['daysPresent' => $daysPresent, 'daysLate' => $daysLate, 'halfDays' => $halfDays, 'totalWorkingDays' => $totalWorkingDays, 'absentDays' => $daysAbsent, 'holidays' => $holidays]);

    }

    public function employeeData($startDate = null, $endDate = null, $userId = null){
        $ant = []; // Array For attendance Data indexed by similar date
        $dateWiseData = []; // Array For Combine Data

        $attendances = Attendance::userAttendanceByDate($startDate, $endDate, $userId);// Getting Attendance Data
        $holidays = Holiday::getHolidayByDates($startDate, $endDate); // Getting Holiday Data

        // Getting Leaves Data
        $leavesDates = Leave::where('user_id', $userId)
            ->where('leave_date', '>=', $startDate)
            ->where('leave_date', '<=', $endDate)
            ->where('status', 'approved')
            ->select('leave_date', 'reason')
            ->get()->keyBy('date')->toArray();

        $holidayData = $holidays->keyBy('holiday_date');
        $holidayArray = $holidayData->toArray();

        // Set Date as index for same date clock-ins
        foreach($attendances as $attand){
            $ant[$attand->clock_in_date][] = $attand; // Set attendance Data indexed by similar date
        }

        $endDate = Carbon::parse($endDate)->timezone($this->global->timezone);
        $startDate = Carbon::parse($startDate)->timezone($this->global->timezone)->subDay();

        // Set All Data in a single Array
        for($date = $endDate; $date->diffInDays($startDate) > 0; $date->subDay()){

            // Set default array for record
            $dateWiseData[$date->toDateString()] = [
                'holiday' =>false,
                'attendance' =>false,
                'leave' =>false
            ];

            // Set Holiday Data
            if(array_key_exists($date->toDateString(), $holidayArray)){
                $dateWiseData[$date->toDateString()]['holiday'] = $holidayData[$date->toDateString()];
            }

            // Set Attendance Data
            if(array_key_exists($date->toDateString(), $ant)){
                $dateWiseData[$date->toDateString()]['attendance'] = $ant[$date->toDateString()];
            }

            // Set Leave Data
            if(array_key_exists($date->toDateString(), $leavesDates)){
                $dateWiseData[$date->toDateString()]['leave'] = $leavesDates[$date->toDateString()];
            }
        }

        // Getting View data
        $view = view('admin.attendance.user_attendance',['dateWiseData' => $dateWiseData,'global' => $this->global])->render();

        return Reply::dataOnly(['status' => 'success', 'data' => $view]);
    }

    public function employeeDataByRange(Request $request){
        $this->table = '';
        $date = Carbon::createFromFormat($this->global->date_format, $request->startDate)->format('Y-m-d');
        $this->startDate = Carbon::parse($request->startDate);
        $this->endDate = Carbon::parse($request->endDate);
        $this->different_day = $this->startDate->diffInDays($this->endDate);
        $attendances = Attendance::singleAttendanceByDate($request->user_id,$date);// Getting Attendance Data
        if(starts_with($request->user_id,'emp-')){
            $this->table = 'employee';
        }
        // Getting View data

        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_by_date_range_list', ['row' => $row,'startDate'=>$this->startDate,'endDate'=>$this->endDate,'different_day'=>$this->different_day, 'global' => $this->global, 'maxAttandenceInDay' => $this->maxAttandenceInDay,'table'=>$this->table])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->removeColumn('total_clock_in')
            ->removeColumn('clock_in')
            ->make();

    }

    public function attendanceByDate(){
        return view('admin.attendance.by_date', $this->data);
    }


    public function byDateData(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $attendances = Attendance::attendanceDate($date)->get();

        return DataTables::of($attendances)
            ->editColumn('id', function ($row) {
                return view('admin.attendance.attendance_date_list', ['row' => $row, 'global' => $this->global])->render();
            })
            ->rawColumns(['id'])
            ->removeColumn('name')
            ->removeColumn('clock_in_time')
            ->removeColumn('clock_out_time')
            ->removeColumn('image')
            ->removeColumn('attendance_id')
            ->removeColumn('working_from')
            ->removeColumn('late')
            ->removeColumn('half_day')
            ->removeColumn('clock_in_ip')
            ->removeColumn('designation_name')
            ->make();
    }

    public function dateAttendanceCount(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $checkHoliday = Holiday::checkHolidayByDate($date);
        $totalPresent = 0;
        $totalAbsent  = 0;
        $holiday  = 0;
        $holidayReason  = '';
        $totalEmployees = count(User::allEmployees());

        if(!$checkHoliday){
            $totalPresent = Attendance::where(DB::raw('DATE(`clock_in_time`)'), '=', $date)->count();
            $totalAbsent = ($totalEmployees-$totalPresent);
        }
        else{
            $holiday = 1;
            $holidayReason = $checkHoliday->occassion;
        }

        return Reply::dataOnly(['status' => 'success', 'totalEmployees' => $totalEmployees, 'totalPresent' => $totalPresent, 'totalAbsent' => $totalAbsent, 'holiday' => $holiday, 'holidayReason' => $holidayReason]);
    }

    public function checkHoliday(Request $request){
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $checkHoliday = Holiday::checkHolidayByDate($date);
        return Reply::dataOnly(['status' => 'success', 'holiday' => $checkHoliday]);
    }

    // Attendance Detail Show
    public function attendanceDetail(Request $request){

        // Getting Attendance Data By User And Date
        $this->attendances =  Attendance::attedanceByUserAndDate($request->date, $request->userID);
        return view('admin.attendance.attendance-detail', $this->data)->render();
    }

    public function salarydata(Request $request)
    {
        // Getting Attendance setting data
        $users = User::all();

        $startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $endDate = Carbon::now()->timezone($this->global->timezone);

        foreach ($users as $user){
            $totalpresent = Attendance::countDaysPresentByUser($startDate,$endDate,$user->id);
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                $image = ($row->image) ? '<img src="' . asset('uploads/avatar/' . $row->image) . '"
                                                                alt="user" class="img-circle" width="30"> ' : '<img src="' . asset('default-profile-2.png') . '"
                                                                alt="user" class="img-circle" width="30"> ';

                $designation = ($row->designation_name) ? ucwords($row->designation_name) : ' ';

                return  '<div class="row"><div class="col-sm-3 col-xs-4">'.$image.'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->id) . '">'.ucwords($row->name).'</a><br><span class="text-muted font-12">'.$designation.'</span></div></div>';

            })
            ->addColumn('present', function ($row) use ($totalpresent) {

                return  $totalpresent;

            })
            ->addColumn('absent', function ($row)  {

                return  0;
            })
            ->addColumn('holidays', function ($row) {

                return  0;

            })->addColumn('actualdays', function ($row) {

                return  0;

            })->addColumn('actualhour', function ($row) {

                return  0;

            })
            ->addColumn('allowance', function ($row)  {

                return  '<div class="row"><div class="col-sm-12 col-xs-4"><button class="btn btn-info">Add Allowance</button></div></div>';

            })
            ->addColumn('overtime', function ($row) {

                return  0;

            })
            ->addColumn('diduction', function ($row) {

                return  0;

            })
            ->addColumn('totalamount', function ($row) {

                return  0;

            })
            ->addColumn('action', function ($row){
                return '
                      <a href="' . route('admin.employees.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Salary Slip"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            })


            ->rawColumns(['name', 'present', 'absent', 'holidays','allowance','overtime','diduction','totalamount','action'])

            ->make(true);
    }

    public function export($startDate = null, $endDate = null, $employee = null) {
        //
    }

    public function summary()
    {
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $now = Carbon::now();
        $this->year = $now->format('Y');
        $this->month = $now->format('m');

        return view('admin.attendance.summary', $this->data);
    }

    public function summaryData(Request $request)
    {
        // dd($request->all());
        /*new employee part*/
        $newtableemployees = Employee::with(
            ['attendance' => function ($query) use ($request) {
                $query->whereRaw('MONTH(attendances.clock_in_time) = ?', [$request->month])
                    ->whereRaw('YEAR(attendances.clock_in_time) = ?', [$request->year]);
            }]
        )->select('employee.id', 'employee.name', 'employee.email', 'employee.created_at')
            ->where('user_type','employee');
        if ($request->userId == '0') {
            $newtableemployees = $newtableemployees->get();
        } else {
            $newtableemployees = $newtableemployees->where('employee.id', $request->userId)->get();
        }
        // dd($newtableemployees);
        $this->holidays = Holiday::whereRaw('MONTH(holidays.date) = ?', [$request->month])->whereRaw('YEAR(holidays.date) = ?', [$request->year])->get();
        $newfinal = [];
        $this->daysInMonth = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        $now = Carbon::now()->timezone($this->global->timezone);
        $requestedDate = Carbon::parse(Carbon::parse('01-'.$request->month.'-'.$request->year))->endOfMonth();
        foreach ($newtableemployees as $employee) {
            $dataTillToday = array_fill(1, $now->copy()->format('d'), 'Absent');
            $dataFromTomorrow = [];
            if (($now->copy()->addDay()->format('d') != $this->daysInMonth) && !$requestedDate->isPast()) {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), '-');
            } else {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), 'Absent');
            }
            $newfinal[$employee->id . '#' . $employee->name] = array_replace($dataTillToday, $dataFromTomorrow);
            foreach ($employee->attendance as $attendance) {
                $newfinal[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = '<a href="javascript:;" class="view-attendance" data-attendance-id="'.$attendance->id.'"><i class="fa fa-check text-success"></i></a>';
            }
            $image = ($employee->image) ? '<img src="' . asset('uploads/avatar/' . $employee->image) . '"
                                                            alt="user" class="img-circle" width="30"> ' : '<img src="' . asset('default-profile-2.png') . '"
                                                            alt="user" class="img-circle" width="30"> ';
            $newfinal[$employee->id . '#' . $employee->name][] = '<a class="userData" id="userID'.$employee->id.'" data-employee-id="'.$employee->id.'"  href="' . route('admin.employees.show', $employee->id) . '">' . $image . ' ' . ucwords($employee->name) . '</a>';
            foreach($this->holidays as $holiday) {
                $newfinal[$employee->id . '#' . $employee->name][$holiday->date->day] = 'Holiday';
            }
        }
        //dd($newfinal);
        $this->employeeAttendence = $newfinal;
        $view = view('admin.attendance.summary_data', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'data' => $view]);
    }

    public function detail($id)
    {
        $attendance = Attendance::find($id);
        $this->attendanceActivity = Attendance::userAttendanceByDate($attendance->clock_in_time->format('Y-m-d'), $attendance->clock_in_time->format('Y-m-d'), $attendance->user_id);

        $this->firstClockIn = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), $attendance->clock_in_time->format('Y-m-d'))
            ->where('user_id', $attendance->user_id)->orderBy('id', 'asc')->first();
        $this->lastClockOut = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), $attendance->clock_in_time->format('Y-m-d'))
            ->where('user_id', $attendance->user_id)->orderBy('id', 'desc')->first();

        $this->startTime = Carbon::parse($this->firstClockIn->clock_in_time)->timezone($this->global->timezone);

        if (!is_null($this->lastClockOut->clock_out_time)) {
            $this->endTime = Carbon::parse($this->lastClockOut->clock_out_time)->timezone($this->global->timezone);
        } elseif (($this->lastClockOut->clock_in_time->timezone($this->global->timezone)->format('Y-m-d') != Carbon::now()->timezone($this->global->timezone)->format('Y-m-d')) && is_null($this->lastClockOut->clock_out_time)) {
            $this->endTime = Carbon::parse($this->startTime->format('Y-m-d') . ' ' . $this->attendanceSettings->office_end_time, $this->global->timezone);
            $this->notClockedOut = true;
        } else {
            $this->notClockedOut = true;
            $this->endTime = Carbon::now()->timezone($this->global->timezone);
        }

        $this->totalTime = $this->endTime->diff($this->startTime, true)->format('%h.%i');


        return view('admin.attendance.attendance_info', $this->data);
    }

    public function mark(Request $request, $userid,$day,$month,$year)
    {
        $this->date = Carbon::createFromFormat('d-m-Y', $day.'-'.$month.'-'.$year)->format('Y-m-d');
        $this->row = Attendance::attendanceByUserDate($userid, $this->date);
        $this->clock_in = 0;
        $this->total_clock_in = Attendance::where('user_id', $userid)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $this->date)
            ->whereNull('attendances.clock_out_time')->count();

        $this->userid = $userid;
        $this->type = 'add';
        return view('admin.attendance.attendance_mark', $this->data);
    }

    public function storeMark(StoreAttendance $request)
    {
        $date = Carbon::createFromFormat($this->global->date_format, $request->attendance_date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        //$clockIn->setTimezone('UTC');
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            //$clockOut->setTimezone('UTC');
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance = Attendance::where('user_id', $request->user_id)
            ->where(DB::raw('DATE(`clock_in_time`)'), "$date")
            ->whereNull('clock_out_time')
            ->first();

        $clockInCount = Attendance::getTotalUserClockIn($date, $request->user_id);

        if (!is_null($attendance)) {
            $attendance->update([
                'user_id' => $request->user_id,
                'attendance_date' => $date,
                'clock_in_time' => $date . ' ' . $clockIn,
                'clock_in_ip' => $request->clock_in_ip,
                'clock_out_time' => $clockOut,
                'clock_out_ip' => $request->clock_out_ip,
                'working_from' => $request->working_from,
                'late' => ($request->has('late'))? 'yes' : 'no',
                'half_day' => ($request->has('half_day'))? 'yes' : 'no'
            ]);
        } else {

            // Check maximum attendance in a day
            if ($clockInCount < $this->attendanceSettings->clockin_in_day) {
                Attendance::create([
                    'user_id' => $request->user_id,
                    'attendance_date' => $date,
                    'clock_in_time' => $date . ' ' . $clockIn,
                    'clock_in_ip' => $request->clock_in_ip,
                    'clock_out_time' => $clockOut,
                    'clock_out_ip' => $request->clock_out_ip,
                    'working_from' => $request->working_from,
                    'late' => ($request->has('late'))? 'yes' : 'no',
                    'half_day' => ($request->has('half_day'))? 'yes' : 'no'
                ]);
            } else {
                return Reply::error(__('messages.maxColckIn'));
            }
        }

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    public function byCamera()
    {
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.attendance.by_punching_machine', $this->data);
    }

    public function singleAttendance(){

        return view('admin.attendance.single_attendance', $this->data);
    }

    public function singleAttendanceDateRange(){
        $this->startDate = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
        $this->endDate = Carbon::now()->timezone($this->global->timezone);
        /*$this->employees = User::allEmployees();*/
        $this->empdata = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.attendance.create_attandancee_by_date_range', $this->data);
    }

    public function rules(){
        $this->employees = User::all();
        $this->empdata = Employee::where('company_id',$this->companyid)->get();
        $this->skills = Skill::all();
        $this->departments = Team::all();
        $this->designations = Designation::all();
        $this->rules = AttendanceRules::where('attendance_rules.name', '<>', 'client')->get();
        return view('admin.attendance.rules',$this->data);
    }
    public function storeGeneralRules(GeneralRulesRequest $request){
        $date =  Carbon::today()->timezone($this->global->timezone)->format('Y-m-d');
        $shift_in_time = Carbon::createFromFormat($this->global->time_format, $request->shift_in_time, $this->global->timezone);
        $shift_in_time = $shift_in_time->format('H:i:s');
        if($request->shift_out_time != ''){
            $shift_out_time = Carbon::createFromFormat($this->global->time_format, $request->shift_out_time, $this->global->timezone);
            $shift_out_time = $shift_out_time->format('H:i:s');
            $shift_out_time = $date.' '.$shift_out_time;
        }
        else{
            $shift_out_time = null;
        }
        $attendancerules = new AttendanceRules();
        $attendancerules->name  = $request->rulename;
        $attendancerules->description = $request->description;
        $attendancerules->shift_in_time = $date.' '.$shift_in_time;
        $attendancerules->shift_out_time = $shift_out_time;
        $attendancerules->anomaly_grace_in = $request->anomaly_in_time;
        $attendancerules->anomaly_grace_out = $request->anomaly_out_time;
        $attendancerules->work_full_time = $request->full_day;
        $attendancerules->work_half_time = $request->half_day;
        $attendancerules->auto_clock_out = $request->auto_clock_out?$request->auto_clock_out:'';
        $attendancerules->save();
        session(['rules_id' => $attendancerules->id]);
        return Reply::success(__('messages.attendanceSaveSuccess'));
    }
    public function updateGeneralRules(GeneralRulesRequest $request,$id){

        $date =  Carbon::today()->timezone($this->global->timezone)->format('Y-m-d');
        $shift_in_time = Carbon::createFromFormat($this->global->time_format, $request->shift_in_time, $this->global->timezone);
        $shift_in_time = $shift_in_time->format('H:i:s');
        if($request->shift_out_time != ''){
            $shift_out_time = Carbon::createFromFormat($this->global->time_format, $request->shift_out_time, $this->global->timezone);
            $shift_out_time = $shift_out_time->format('H:i:s');
            $shift_out_time = $date.' '.$shift_out_time;
        }
        else{
            $shift_out_time = null;
        }
        $attendancerules = AttendanceRules::find($id);
        $attendancerules->name  = $request->rulename;
        $attendancerules->description = $request->description;
        $attendancerules->shift_in_time = $date.' '.$shift_in_time;
        $attendancerules->shift_out_time = $shift_out_time;
        $attendancerules->anomaly_grace_in = $request->anomaly_in_time;
        $attendancerules->anomaly_grace_out = $request->anomaly_out_time;
        $attendancerules->work_full_time = $request->full_day;
        $attendancerules->work_half_time = $request->half_day;
        $attendancerules->auto_clock_out = $request->auto_clock_out?$request->auto_clock_out:'';
        $attendancerules->save();
        session(['rules_id' => $attendancerules->id]);
        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    public function storeAdvanceRules(Request $request){

        $advancerules = new AdvanceRules();
        $advancerules->enable_overtime  = $request->overtime?$request->overtime:false;
        $advancerules->rules_id  = session()->get('rules_id');
        $advancerules->in_time_leave_allow  = $request->yearlyleave;
        $advancerules->in_time_penalty = $request->penaly;
        $advancerules->in_time_deduction = $request->leavededuction;
        $advancerules->out_time_leave_allow = $request->outyearlyleave;
        $advancerules->out_time_penalty = $request->outpenaly;
        $advancerules->out_time_deduction = $request->outleavededuction;
        $advancerules->work_time_leave_allow = $request->workyearlyleave;
        $advancerules->work_time_penalty = $request->workpenalty;
        $advancerules->work_time_deduction = $request->workleavededuction;

        $advancerules->save();
        return Reply::success(__('messages.attendanceSaveSuccess'));

    }
    public function updateAdvanceRules(Request $request,$id){

        $advancerules = AdvanceRules::find($id);
        $advancerules->enable_overtime  = $request->overtime?$request->overtime:false;
        $advancerules->rules_id  = session()->get('rules_id');
        $advancerules->in_time_leave_allow  = $request->yearlyleave;
        $advancerules->in_time_penalty = $request->penaly;
        $advancerules->in_time_deduction = $request->leavededuction;
        $advancerules->out_time_leave_allow = $request->outyearlyleave;
        $advancerules->out_time_penalty = $request->outpenaly;
        $advancerules->out_time_deduction = $request->outleavededuction;
        $advancerules->work_time_leave_allow = $request->workyearlyleave;
        $advancerules->work_time_penalty = $request->workpenalty;
        $advancerules->work_time_deduction = $request->workleavededuction;

        $advancerules->save();
        return Reply::success(__('messages.attendanceSaveSuccess'));

    }

    public function  rulesData(Request $request){
        $roules = AttendanceRules::all();
        return DataTables::of($roules)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('description', function ($row)  {

                return $row->description;
            })
            ->editColumn('shift_in_time', function ($row)  {

                return $row->shift_in_time;
            }) ->editColumn('shift_out_time', function ($row)  {

                return $row->shift_out_time;
            }) ->editColumn('anomaly_grace_in_time', function ($row)  {
                return $row->anomaly_grace_in;

            }) ->editColumn('anomaly_grace_out_time', function ($row)  {
                return $row->anomaly_grace_out;

            }) ->editColumn('work_full_time', function ($row)  {
                return $row->work_full_time;

            })
            ->editColumn('work_half_time', function ($row)  {

                return $row->work_half_time;
            })
            ->addColumn('action', function ($row) {
                return '<a   href="' . route('admin.attendances.editrules', [$row->id]) . '" class="btn btn-info btn-circle" 
                      data-toggle="tooltip" data-original-title="Edit Rules"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'description','shift_in_time','shift_out_time','anomaly_grace_in_time','anomaly_grace_out_time', 'work_full_time', 'work_half_time','action'])

            ->make(true);
    }

    public function attendaceRulesData(){

        $users = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->select('employee.id','employee.name','employee_details.address','employee_details.department_id as deptid')
            ->where('employee.company_id',$this->companyid)->where('user_type','employee')
            ->groupBy('employee.id')->get();


        $wordata = Employee::join('rules_users','rules_users.user_id','=','employee.id')
            ->join('attendance_rules','attendance_rules.id','=','rules_users.rules_id')->select('rules_users.user_id','attendance_rules.name','rules_users.effective_date')->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {

                return '<input type="checkbox" class="sub_chk" data-id="'.$row->id.'" />';
            })->rawColumns(['checkbox'])
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })->editColumn('department', function ($row)  {
                $team = Team::where('id',$row->deptid)->first();
                if(!empty($team)){
                    return $team->team_name;
                }
                return 'NA';
            })->editColumn('location', function ($row)  {

                return $row->address;

            })->editColumn('effective_date', function ($row) use($wordata)  {
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->effective_date;
                    }

                }

            })

            ->addColumn('rulesapplied', function ($row) use($wordata){
                foreach ($wordata as $work){
                    if($row->id == $work->user_id){
                        return $work->name;
                    }

                }

            })
            ->rawColumns(['action','name','department','location','rulesapplied'])

            ->make(true);

    }

    public function createRules(){
        return view('admin.attendance.create_new_rules',$this->data);
    }

    public function editRules($id){
        $this->rules  =  AttendanceRules::find($id);
        $this->adrules  =  AdvanceRules::where('rules_id',$id)->first();
        return view('admin.attendance.edit_rules',$this->data);
    }

    public function deleteRules($id){
        $rules = AttendanceRules::find($id);
        $rules->delete();

    }

    // Attendance Detail Show
    public function assignRules(Request $request){

        $this->ids = $request->ids;
        $this->rules = AttendanceRules::all();

        return view('admin.attendance.assign_rules', $this->data)->render();
    }

    public function  postAssignRules(Request $request){
        $date = Carbon::parse($request->effective_date)->format('Y-m-d');
        $emp_ids = explode(',', $request->ids);

        foreach ($emp_ids as $key=>$emp_id){
            RulesUser::updateOrCreate(
                ['user_id' => $emp_id],
                ['user_id' => $emp_id,'rules_id' => $request->rules_id,'effective_date'=>$date]
            );

        }

        return Reply::success(__('messages.attendanceSaveSuccess'));
        abort(403);
    }


    public function getData(Request $request){
        $value= array();
        $id = $request->id;
        $date = $request->date;
        $stadate  = Carbon::parse($date)->format('Y-m-d');
        $startdate = $stadate.' 00:00:01';
        $enddate = $stadate.' 23:59:59';
        $att = Attendance::where('created_at','>=',$startdate)->where('created_at','<=',$enddate)->where('user_id',$id)->first();
        $value['id'] = $att->id;
        $value['clock_in'] = Carbon::parse($att->clock_in_time)->format('h:i A');
        if($att !== null){
            return $value;
        }else {
            return '';
        }
    }
    public function updateAttendance(Request $request,$id){
        $attendance = Attendance::findOrFail($id);
        $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
        $clockIn = Carbon::createFromFormat($this->global->time_format, $request->clock_in_time, $this->global->timezone);
        $clockIn = $clockIn->format('H:i:s');
        if ($request->clock_out_time != '') {
            $clockOut = Carbon::createFromFormat($this->global->time_format, $request->clock_out_time, $this->global->timezone);
            $clockOut = $clockOut->format('H:i:s');
            $clockOut = $date . ' ' . $clockOut;
        } else {
            $clockOut = null;
        }

        $attendance->user_id = $request->user_id;
        $attendance->attendance_date = $date;
        $attendance->clock_in_time = $date . ' ' . $clockIn;
        $attendance->clock_in_ip = $request->clock_in_ip;
        $attendance->clock_out_time = $clockOut;
        $attendance->clock_out_ip = $request->clock_out_ip;
        $attendance->working_from = $request->working_from;
        $attendance->late = ($request->has('late'))? 'yes' : 'no';
        $attendance->half_day = ($request->has('half_day'))? 'yes' : 'no';
        if ($request->file) {
            if (!file_exists('uploads/attendance-image')) {
                mkdir('uploads/attendance-image', 0777, true);
            }
            define('UPLOAD_DIR', 'uploads/attendance-image/');
            $img = $request->file;
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $dataimage = base64_decode($img);
            $file = UPLOAD_DIR . uniqid() . '.png';
            $success = file_put_contents($file, $dataimage);
            if($request->btnpressed == 'clockin'){
                $attendance->clock_in_image = $file;
            }
            if($request->btnpressed == 'clockout'){
                $attendance->clock_out_image = $file;
            }
        }
        $attendance->save();

        return Reply::success(__('messages.attendanceSaveSuccess'));
    }

    public function groupAttendance(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                if(empty($request->clock_in)){
                    $response['message'] = 'Time In Required';
                    $response['status'] = 301;
                    return $response;
                }
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $clockIn = Carbon::parse($request->clock_in)->format('H:i:s');
                $clockOut = !empty($request->clock_out)?Carbon::parse($request->clock_out)->format('H:i:s'):null;
                if(!empty($projectid)){
                    $projectdetails = AppProject::where('id',$projectid)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
               /* $attendance = Attendance::where('user_id',$request->employee_id)->where('company_id',$usercompany)
                    ->where('admin_clock_in', '>=', $date.' '.$clockIn)
                    ->where('admin_clock_out', '<=', $date.' '.$clockOut)
                    ->first();*/
                $attendance = Attendance::where('user_id',$request->employee_id)->where('company_id',$usercompany)
                         ->where(function($query) use($date){
                             $query->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $date)->orWhere(DB::raw('DATE(attendances.clock_out_time)'), '=', $date)
                                 ->orWhere(DB::raw('DATE(attendances.admin_clock_in)'), '=', $date);
                         })->first();

                //$clockInCount = Attendance::getTotalUserClockIn($date, $user->id,$user->company_id);
                $message = '';
                if (!is_null($attendance) && !empty($attendance)) {
                    $attendance->user_id = $request->employee_id;
                    $attendance->emp_id = $request->emp_id ?: 0;
                    $attendance->attendance_date = $date;
                    $attendance->admin_clock_in = $date . ' ' . $clockIn;
                    $attendance->admin_clock_out = !empty($clockOut)?$date.' '.$clockOut:null;
                    $attendance->save();
                    $message = 'Attendance updated Successfully';
                } else {
                    $attendance = new Attendance();
                    $attendance->user_id = $request->employee_id;
                    $attendance->emp_id = $request->emp_id ?: 0;
                    $attendance->company_id = $usercompany;
                    $attendance->attendance_date = $date;
                    $attendance->admin_clock_in = $date . ' ' . $clockIn;
                    $attendance->admin_clock_out = !empty($clockOut)?$date . ' ' . $clockOut:null;
                    $attendance->clock_in_lat = $request->lat;
                    $attendance->clock_in_lang = $request->lang;
                    $attendance->clock_in_address = $request->address;
                    $attendance->save();
                    $message = 'Attendance added Successfully';
                }
                $response['status'] = 200;
                $response['message'] = $message;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'post-attendance', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }
    public function AttendancePostAbsent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                if(empty($request->date)){
                    $response['message'] = 'Date In Required';
                    $response['status'] = 301;
                    return $response;
                }
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $clockIn = Carbon::parse($request->clock_in)->format('H:i:s');
                $clockOut = !empty($request->clock_out)?Carbon::parse($request->clock_out)->format('H:i:s'):null;
                if(!empty($projectid)){
                    $projectdetails = AppProject::where('id',$projectid)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
               /* $attendance = Attendance::where('user_id',$request->employee_id)->where('company_id',$usercompany)
                    ->where('admin_clock_in', '>=', $date.' '.$clockIn)
                    ->where('admin_clock_out', '<=', $date.' '.$clockOut)
                    ->first();*/
                $attendance = Attendance::where('user_id',$request->employee_id)->where('company_id',$usercompany)->where('attendance_date',$date)->first();

                if (!is_null($attendance) && !empty($attendance)) {
                    $attendance->user_id = $request->employee_id;
                    $attendance->emp_id = $request->emp_id ?: 0;
                    $attendance->attendance_date = $date;
                    $attendance->admin_clock_in =null;
                    $attendance->admin_clock_out =  null;
                    $attendance->clock_in_time = null;
                    $attendance->clock_out_time =  null;
                    $attendance->save();
                    $message = 'Attendance updated Successfully';
                } else {
                    $attendance = new Attendance();
                    $attendance->user_id = $request->employee_id;
                    $attendance->emp_id = $request->emp_id ?: 0;
                    $attendance->company_id = $usercompany;
                    $attendance->attendance_date = $date;
                    $attendance->admin_clock_in = null;
                    $attendance->admin_clock_out =  null;
                    $attendance->clock_in_time = null;
                    $attendance->clock_out_time =  null;
                    $attendance->save();
                    $message = 'Attendance added Successfully';
                }
                $response['status'] = 200;
                $response['message'] = $message;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'post-attendance', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }
    public function groupAttendanceList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $project_id = $request->project_id;
                $proComId = Project::where('id',$project_id)->orderBy('id','DESC')->first();
                if(empty($proComId)){
                    $response['message'] = 'Project Not Found.';
                    $response['status'] = 301;
                    return $response;
                }
                $openDays = json_decode($this->attendanceSettings->office_open_days);
                $data['startDate'] = Carbon::today()->timezone($this->global->timezone)->startOfMonth();
                $data['endDate'] = Carbon::now()->timezone($this->global->timezone);
                $date = Carbon::createFromFormat($this->global->date_format, $request->date)->format('Y-m-d');
                $eData = array();
                //$employees = Attendance::attendanceByDate($date)->get();
                DB::statement("SET @attendance_date = '$date'");
                $employees = Employee::where('employee.company_id',$user->company_id)
                    ->join('users','users.id','=','employee.user_id')
                    ->leftJoin(
                        'attendances', function ($join) use ($user,$date,$proComId) {
                        $join->on('employee.user_id', '=', 'attendances.user_id')
                            ->where(DB::raw('DATE(attendances.admin_clock_in)'), '=', $date)
                            ->where('attendances.company_id', '=', $proComId->company_id);
                    })
                    ->where('employee.user_type', '=', 'employee')
                    ->select(
                        DB::raw("( select count('atd.id') from attendances as atd where atd.user_id = users.id and DATE(atd.admin_clock_in)  =  '".$date."' and DATE(atd.admin_clock_out)  =  '".$date."' ) as total_clock_in"),
                        DB::raw("( select count('atdn.id') from attendances as atdn where atdn.user_id = users.id and DATE(atdn.admin_clock_in)  =  '".$date."' ) as clock_in"),
                        'employee.id  as emp_id',
                        'employee.user_id  as id',
                        'employee.name',
                        'attendances.attendance_date',
                        'attendances.clock_in_ip',
                        'attendances.clock_in_time',
                        'attendances.admin_clock_in',
                        'attendances.clock_out_time',
                        'attendances.admin_clock_out',
                        'attendances.late',
                        'attendances.half_day',
                        'attendances.working_from',
                        'attendances.clock_in_image',
                        'attendances.clock_out_image',
                        'attendances.clock_in_lat',
                        'attendances.clock_in_lang',
                        'attendances.clock_out_lat',
                        'attendances.clock_out_lang',
                        'attendances.clock_out_address',
                        'attendances.clock_in_address',
                        'users.image',
                        DB::raw('@attendance_date as atte_date'),
                        'attendances.id as attendance_id'
                    )
                    ->get();
                foreach ($employees as $employee){
                    $attStatus = '';
                    $attendancerules = '';
                    $totalDuration1 = '00:00:00 Hrs';
                    if(!empty($employee->clock_in_time) && !empty($employee->clock_out_time)){
                        $attStatus = 'Present';
                    }else{
                        $attStatus = 'Absent';
                    }
                    $rules = RulesUser::where('user_id',$employee->id)->first();
                    if(!empty($rules)){
                        $attendancerules = AttendanceRules::where('id',$rules->rules_id)->first();
                    }
                    if(!empty($employee->clock_in_time) && !empty($employee->clock_out_time)){
                        $totalDuration1 = Carbon::parse($employee->clock_out_time)->diff(Carbon::parse($employee->clock_in_time))->format('%H:%I:%S')." Hrs";
                    }
                    $eData[] = array(
                        'total_clock_in' => $totalDuration1,
                        'clock_in' => (!empty($employee->clock_in) ? $employee->clock_in : 0),
                        'emp_id' => (!empty($employee->emp_id) ? $employee->emp_id : 0),
                        'id' => (!empty($employee->id) ? $employee->id : 0),
                        'name' => (!empty($employee->name) ? $employee->name : ''),
                        'clock_in_ip' => (!empty($employee->clock_in_ip) ? $employee->clock_in_ip : ''),
                        'attendance_date' => (!empty($employee->attendance_date) ? date('d M Y', strtotime($employee->attendance_date))  : ''),
                        'clock_in_time' => (!empty($employee->clock_in_time) ? date('h:i A', strtotime(explode(' ', $employee->clock_in_time)[1]))  : ''),
                        'clock_out_time' => (!empty($employee->clock_out_time) ? date('h:i A', strtotime(explode(' ', $employee->clock_out_time)[1])) : ''),
                        'admin_clock_in' => (!empty($employee->admin_clock_in) ? date('h:i A', strtotime(explode(' ', $employee->admin_clock_in)[1]))  : ''),
                        'admin_clock_out' => (!empty($employee->admin_clock_out) ? date('h:i A', strtotime(explode(' ', $employee->admin_clock_out)[1])) : ''),
                        'late' => (!empty($employee->late) ? $employee->late : ''),
                        'half_day' => (!empty($employee->half_day) ? $employee->half_day : ''),
                        'working_from' => (!empty($employee->working_from) ? $employee->working_from : ''),
                        'clock_in_image' => (!empty($employee->clock_in_image) ?  uploads_url().'attendance-image/'.$employee->clock_in_image : ''),
                        'clock_out_image' => (!empty($employee->clock_out_image) ? uploads_url().'attendance-image/'.$employee->clock_out_image : ''),
                        'clock_in_lat' => (!empty($employee->clock_in_lat) ? $employee->clock_in_lat : ''),
                        'clock_in_lang' => (!empty($employee->clock_in_lang) ? $employee->clock_in_lang : ''),
                        'clock_out_lat' => (!empty($employee->clock_out_lat) ? $employee->clock_out_lat : ''),
                        'clock_out_lang' => (!empty($employee->clock_out_lang) ? $employee->clock_out_lang : ''),
                        'clock_out_address' => (!empty($employee->clock_out_address) ? $employee->clock_out_address : ''),
                        'clock_in_address' => (!empty($employee->clock_in_address) ? $employee->clock_in_address : ''),
                        'image' => (!empty($employee->image) ? uploads_url().'avatar/'.$employee->image : ''),
                        'designation_name' => (!empty($employee->designation_name) ? $employee->designation_name : ''),
                        'atte_date' => (!empty($employee->atte_date) ? date('d M Y', strtotime($employee->atte_date))  : ''),
                        'attendance_id' => (!empty($employee->attendance_id) ? $employee->attendance_id : 0),
                        'office_timing' => ((!empty($attendancerules) && !empty($attendancerules->shift_in_time)  && !empty($attendancerules->shift_out_time)) ? date('h:i', strtotime($attendancerules->shift_in_time)) .' - '. date('h:i', strtotime($attendancerules->shift_out_time)) : '12:00 AM-11:59 PM'),
                        'status' => $attStatus,
                    );
                }

                $data['totalWorkingDays'] = $data['startDate']->diffInDaysFiltered(function(Carbon $date) use ($openDays){
                    foreach($openDays as $day){
                        if($date->dayOfWeek == $day){
                            return $date;
                        }
                    }
                }, $data['endDate']);
                $data['employees'] = $eData;
                $data['daysPresent'] = Attendance::countDaysPresentByUser($data['startDate'], $data['endDate'], $user->id);
                $data['daysLate'] = Attendance::countDaysLateByUser($data['startDate'], $data['endDate'], $user->id);
                $data['halfDays'] = Attendance::countHalfDaysByUser($data['startDate'], $data['endDate'], $user->id);
                $data['holidays'] = Count(Holiday::getHolidayByDates($data['startDate']->format('Y-m-d'), $data['endDate']->format('Y-m-d')));

                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}