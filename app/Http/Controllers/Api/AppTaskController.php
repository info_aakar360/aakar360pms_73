<?php

namespace App\Http\Controllers\Api;

use App\AppFileManager;
use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Employee;
use App\Helper\Files;
use App\Http\Controllers\Controller;
use App\IndentProducts;
use App\ManpowerLog;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\ProjectsLogs;
use App\PunchItem;
use App\Task;
use App\TaskFile;
use App\TaskPercentage;
use App\Traits\ProjectProgress;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;
use  View;
use Maatwebsite\Excel\Facades\Excel;

class AppTaskController extends Controller
{
    use AuthenticatesUsers;
    use ProjectProgress;

    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function appTasks(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $workid = $request->workid;
                if ($workid) {
                    $projects = Task::where('task_category_id', $workid)->get()->toArray();
                } else {
                    $projects = Task::get()->toArray();
                }
                $response['status'] = 200;
                $response['message'] = 'Tasks List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $new = 0;
            try {
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                $taskid = $request->taskid;
                $boqid = $request->boqid;
                $prevqty = $request->prevqty ?: 0;
                $memberExistsInTemplate = false;
                $heading = $request->heading;
                $costitem = CostItems::where('company_id', $projectdetails->company_id)->where('cost_item_name', $heading)->first();
                if (empty($costitem)) {
                    $costitem = new CostItems();
                    $costitem->company_id = $projectdetails->company_id;
                    $costitem->cost_item_name = $request->heading;
                    $costitem->unit = $request->unit_id ?: 0;
                    $costitem->save();
                }

                $task = new Task();
                $task->company_id = $projectdetails->company_id;
                $task->heading = $costitem->cost_item_name;
                if ($request->description != '') {
                    $task->description = $request->description;
                }
                $startDate = !empty($request->start_date) ? date('Y-m-d', strtotime($request->start_date)) : '';
                $dueDate = !empty($request->due_date) ? date('Y-m-d', strtotime($request->due_date)) : '';
                $task->start_date = $startDate;
                $task->due_date = $dueDate;
                $task->user_id = !empty($request->user_id) ? $request->user_id : '';
                $task->employee_id = $request->employee_id;
                $task->project_id = $projectid;
                $task->title = $subprojectid;
                $task->segment = $segmentid ?: 0;
                $task->boqinclude = $request->boq ?: 0;
                $task->task_category_id = $request->itemid;
                $task->priority = $request->priority ?: 'medium';
                $duedate = !empty($dueDate) ? strtotime($dueDate.' 23:59:59') : 0;
                $curdate = time();

                if (!empty($request->status) && $request->status == 'inproblem') {
                    $status = 'inproblem';
                } else if (!empty($request->status) && $request->status == 'completed') {
                    $status = 'completed';
                } else {
                        $status = 'notstarted';
                        if($duedate<$curdate&&$request->status!='completed'){
                            $status = 'delayed';
                        }else{
                            $status = $request->status ?: 'notstarted';
                        }
                }
                $task->status = $status;
                $task->unit_id = $request->unit_id ?: 0;
                $task->created_by = $user->id;
                $task->dependent_task_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_task_id') && $request->dependent_task_id != '' ? $request->dependent_task_id : null;
                if ($request->board_column_id) {
                    $task->board_column_id = $request->board_column_id;
                }
                if ($request->milestone_id != '') {
                    $task->milestone_id = $request->milestone_id;
                }
                $task->save();

                if ($request->boq == '1') {
                    if (!empty($segmentid)) {
                         $productcostitem = new ProjectSegmentsProduct();
                        $maxcostitemid = ProjectSegmentsProduct::where('title', $subprojectid)->where('project_id', $projectid)->where('segment', $segmentid)->max('inc');
                        $newid = $maxcostitemid + 1;
                        $productcostitem->inc = $newid;
                        $productcostitem->segment = $segmentid;
                    } else {
                        $productcostitem = new ProjectCostItemsProduct();
                        $maxcostitemid = ProjectCostItemsProduct::where('title', $subprojectid)->where('project_id', $projectid)->max('inc');
                        $newid = $maxcostitemid + 1;
                        $productcostitem->inc = $newid;
                    }

                    $productcostitem->cost_items_id = $costitem->id;
                    $productcostitem->category = $request->catlevel;
                    $productcostitem->project_id = $projectid;
                    $productcostitem->assign_to = $request->user_id;
                    $productcostitem->employee_id = $request->employee_id;
                    $contid = $request->contractor ?: 0;
                    $productcostitem->contractor_employee_id = $contid;
                    $productcostitem->contractor = get_employee_userid($contid) ?: 0;
                    $productcostitem->title = $subprojectid;
                    $productcostitem->position_id = !empty($request->position_id) ? $request->position_id : 0;
                    $productcostitem->description = $request->description;
                    $productcostitem->start_date = $startDate;
                    $productcostitem->deadline = $dueDate;
                    $productcostitem->unit = $request->unit_id ?: 0;
                    $productcostitem->rate = $request->rate ?: 0;
                    $productcostitem->qty = $request->quantity ?: 0;
                    $productcostitem->amount = $request->amount ?: 0;
                    $productcostitem->finalrate = $request->rate ?: 0;
                    $productcostitem->finalamount = $request->amount ?: 0;
                    $productcostitem->save();


                    $task->cost_item_id = $productcostitem->id ?: 0;
                    $task->save();

                }

                /*  $medium = $request->medium ?: 'android';
                  $createlog = new ProjectsLogs();
                  $createlog->company_id = $user->company_id;
                  $createlog->added_id = $user->id;
                  $createlog->module_id = $task->id;
                  $createlog->module = 'tasks';
                  $createlog->project_id = $projectid;
                  $createlog->subproject_id = $subprojectid ?: 0;
                  $createlog->segment_id = $segmentid ?: 0;
                  $createlog->heading = $task->heading;
                  if (!empty($taskid)) {
                      $createlog->modulename = 'task_updated';
                  }else{
                      $createlog->modulename = 'task_created';
                  }
                  $createlog->description = $task->heading.' task created by '.$user->name.' for the project '.get_project_name($productcostitem->project_id);
                  $createlog->medium = $medium;
                  $createlog->save();
                  //calculate project progress if enabled
                  $this->calculateProjectProgress($request->project_id);*/

                    if($request->status=='completed'){

                        $status= $request->status;
                        $qty=   $request->quantity;
                    $percentage = numberformat(100);
                    $task->percentage = $percentage;
                    $task->taskqty = $qty;
                    $task->status = $status;
                    $task->save();

                    $taskspercent = new TaskPercentage();
                    $taskspercent->company_id = $user->company_id;
                    $taskspercent->task_id = $task->id;
                    $taskspercent->added_by = $user->id;
                    $taskspercent->percentage = $percentage;
                    $taskspercent->todayqty = $qty;
                    $taskspercent->taskqty = $qty;
                    $taskspercent->comment = '';
                    $taskspercent->status = $status ?: '';
                    $taskspercent->mentionusers = '';
                    $taskspercent->save();

                    }

                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Tasks';
                        $notifmessage['body'] = 'New Task is created at '.get_project_name($projectid).' project by '.$user->name;
                        $notifmessage['activity'] = 'tasks';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }

                $response['status'] = 200;
                $response['message'] = 'Task Added Successfully';
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-task',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createMultipleTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $new = 0;
            try {
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = 'Please select project';
                    $response['status'] = 301;
                    return $response;
                }
                $taskarray = json_decode($request->task_array);
                if(empty($taskarray)){
                    $response['message'] = 'Please create atleast one task';
                    $response['status'] = 301;
                    return $response;
                }

                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                $positionid = $request->position_id ?: 0;

                foreach ($taskarray as $taskinfo){
                    $heading = $taskinfo->name;
                    $costitem = CostItems::where('company_id', $projectdetails->company_id)->where('cost_item_name', $heading)->first();
                    if (empty($costitem)) {
                        $costitem = new CostItems();
                        $costitem->company_id = $projectdetails->company_id;
                        $costitem->cost_item_name = $heading;
                        $costitem->unit = 0;
                        $costitem->save();
                    }

                    $task = new Task();
                    $task->company_id = $projectdetails->company_id;
                    $task->heading = $costitem->cost_item_name;
                     $task->description = '';
                    $startDate = !empty($taskinfo->start_date) ? date('Y-m-d', strtotime($taskinfo->start_date)) : '';
                    $dueDate = !empty($taskinfo->end_date) ? date('Y-m-d', strtotime($taskinfo->end_date)) : '';
                    $task->start_date = $startDate;
                    $task->due_date = $dueDate;
                    $task->user_id =  '0';
                    $task->employee_id =  '0';
                    $task->project_id = $projectdetails->id;
                    $task->title = $subprojectid;
                    $task->segment = $segmentid ?: 0;
                    $task->boqinclude = 1;
                    $task->task_category_id = $request->activity_id;
                    $task->priority = 'medium';
                    $duedate = !empty($dueDate) ? strtotime($dueDate.' 23:59:59') : '';
                    $curdate = time();

                    if (!empty($request->status) && $request->status == 'inproblem') {
                        $status = 'inproblem';
                    } else if (!empty($request->status) && $request->status == 'completed') {
                        $status = 'completed';
                    } else {
                            $status = 'notstarted';
                            if(!empty($duedate)){
                                if($duedate<$curdate&&$request->status!='completed'){
                                    $status = 'delayed';
                                }else{
                                    $status = $request->status ?: 'notstarted';
                                }
                            }
                    }
                    $task->status = $status;
                    $task->unit_id =   0;
                    $task->created_by = $user->id;
                    $task->save();

                    if ($task->boqinclude == '1') {

                        if (!empty($segmentid)) {
                            $projectposition = ProjectSegmentsPosition::where('id',$positionid)->first();

                             $productcostitem = new ProjectSegmentsProduct();
                            $maxcostitemid = ProjectSegmentsProduct::where('title', $subprojectid)->where('project_id', $projectid)->where('segment', $segmentid)->max('inc');
                            $newid = $maxcostitemid + 1;
                            $productcostitem->inc = $newid;
                            $productcostitem->segment = $segmentid;
                        } else {
                            if(!empty($positionid)){
                                $projectposition = ProjectCostItemsPosition::where('id',$positionid)->first();
                            }else{
                                $projectposition = ProjectCostItemsPosition::where('project_id',$projectdetails->id)->where('itemname','General')->first();
                            }
                            $productcostitem = new ProjectCostItemsProduct();
                            $maxcostitemid = ProjectCostItemsProduct::where('title', $subprojectid)->where('project_id', $projectid)->max('inc');
                            $newid = $maxcostitemid + 1;
                            $productcostitem->inc = $newid;
                        }

                        $productcostitem->cost_items_id = $costitem->id;
                        $productcostitem->category = !empty($projectposition->catlevel) ? $projectposition->catlevel : '' ;
                        $productcostitem->project_id = $projectdetails->id;
                        $productcostitem->assign_to = '0';
                        $productcostitem->employee_id = '0';
                        $contid =  0;
                        $productcostitem->contractor_employee_id = $contid;
                        $productcostitem->contractor =  0;
                        $productcostitem->title = $subprojectid;
                        $productcostitem->position_id =  !empty($projectposition->id) ? $projectposition->id : 0;
                        $productcostitem->description = '';
                        $productcostitem->start_date = $startDate;
                        $productcostitem->deadline = $dueDate;
                        $productcostitem->unit = 0;
                        $productcostitem->rate = 0;
                        $productcostitem->qty =   0;
                        $productcostitem->amount =   0;
                        $productcostitem->finalrate =  0;
                        $productcostitem->finalamount =  0;
                        $productcostitem->save();

                        $task->cost_item_id = $productcostitem->id ?: 0;
                        $task->save();

                    }
                }

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Tasks';
                    $notifmessage['body'] = 'New Task is created at '.get_project_name($projectid).' project by '.$user->name;
                    $notifmessage['activity'] = 'tasks';
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Task Added Successfully';
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-task',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function editTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $new = 0;
            try {

                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                $taskid = $request->taskid;
                $boqid = $request->boqid;
                $prevqty = $request->prevqty ?: 0;
                $memberExistsInTemplate = false;
                $heading = $request->heading;
                $costitem = CostItems::where('company_id', $projectdetails->company_id)->where('cost_item_name', $heading)->first();
                if (empty($costitem)) {
                    $costitem = new CostItems();
                    $costitem->company_id = $projectdetails->company_id;
                    $costitem->cost_item_name = $request->heading;
                    $costitem->unit = $request->unit_id ?: 0;
                    $costitem->save();
                }
                $task = Task::find($taskid);
                $task->heading = $costitem->cost_item_name;
                if ($request->description != '') {
                    $task->description = $request->description;
                }
                $startDate = !empty($request->start_date) ? date('Y-m-d', strtotime($request->start_date)) : '';
                $dueDate = !empty($request->due_date) ? date('Y-m-d', strtotime($request->due_date)) : '';
                $task->start_date = $startDate;
                $task->due_date = $dueDate;
                $task->user_id = !empty($request->user_id) ? $request->user_id : '';
                $task->project_id = $projectid;
                $task->title = $subprojectid;
                $task->segment = $segmentid ?: 0;
                $task->boqinclude = $request->boq ?: 0;
                $task->task_category_id = $request->itemid;
                $task->priority = $request->priority ?: 'medium';
                $task->unit_id = $request->unit_id ?: 0;
                $task->created_by = $user->id;
                $task->save();


                if ($request->boq == '1') {
                    if (!empty($segmentid)) {
                        if (!empty($boqid)) {
                            $productcostitem = ProjectSegmentsProduct::find($boqid);
                        } else {
                            $productcostitem = new ProjectSegmentsProduct();
                        }
                        $maxcostitemid = ProjectSegmentsProduct::where('title', $subprojectid)->where('project_id', $projectid)->where('segment', $segmentid)->max('inc');
                        $newid = $maxcostitemid + 1;
                        $productcostitem->inc = $newid;
                        $productcostitem->segment = $segmentid;
                    } else {
                        if (!empty($boqid)) {
                            $productcostitem = ProjectCostItemsProduct::find($boqid);
                        } else {
                            $productcostitem = new ProjectCostItemsProduct();
                            $maxcostitemid = ProjectCostItemsProduct::where('title', $subprojectid)->where('project_id', $projectid)->max('inc');
                            $newid = $maxcostitemid + 1;
                            $productcostitem->inc = $newid;
                        }
                    }

                    $productcostitem->cost_items_id = $costitem->id;
                    $productcostitem->category = $request->catlevel;
                    $productcostitem->project_id = $projectid;
                    $productcostitem->assign_to = $request->user_id;
                    $contid = $request->contractor ?: 0;
                    $productcostitem->contractor_employee_id = $contid;
                    $productcostitem->contractor = get_employee_userid($contid) ?: 0;
                    $productcostitem->title = $subprojectid;
                    $productcostitem->position_id = !empty($request->position_id) ? $request->position_id : 0;
                    $productcostitem->description = $request->description;
                    $productcostitem->start_date = $startDate;
                    $productcostitem->deadline = $dueDate;
                    $productcostitem->unit = $request->unit_id ?: 0;
                    $productcostitem->rate = $request->rate ?: 0;
                    $productcostitem->qty = $request->quantity ?: 0;
                    $productcostitem->amount = $request->amount ?: 0;
                    $productcostitem->finalrate = $request->rate ?: 0;
                    $productcostitem->finalamount = $request->amount ?: 0;
                    $productcostitem->save();


                    $task->cost_item_id = $productcostitem->id ?: 0;

                }
                $percentage = 0;
                $quantity = 0;
                $quant = $request->quantity;
                $recenttage = TaskPercentage::where('task_id',$task->id)->where('moduletype','percentage')->first();
                if(!empty($recenttage)){
                    $percentage = $recenttage->percentage;
                    $quantity = ($percentage/100)*$quant;
                }
                $task->taskqty = numberformat($quantity);
                $task->percentage = $percentage;

                $curdate = time();
                $duedate = !empty($dueDate) ? strtotime($dueDate.' 23:59:59') : 0;
                if(!empty($request->status)&&$request->status=='inproblem'){
                    $status = 'inproblem';
                }else{
                    if($percentage>=100){
                        $status = 'completed';
                    }elseif($duedate<$curdate&&$request->status!='completed'){
                        $status = 'delayed';
                    }elseif($task->percentage&&$request->status!='inproblem'){
                        $status = 'inprogress';
                    }else{
                        $status = $request->status ?: 'notstarted';
                    }
                }

                $task->status = $status;

                $task->save();

                $response['status'] = 200;
                $response['message'] = 'Task Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-task',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->taskid;
                $task = Task::findOrFail($id);
                $manpower = ManpowerLog::where('task_id',$task->id)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please remove Attendance';
                    $response['status'] = 300;
                    return $response;
                }
                $manpower = PunchItem::where('task_id',$task->id)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please remove issues';
                    $response['status'] = 300;
                    return $response;
                }

                if ($request->has('recurring') && $request->recurring == 'yes') {
                    Task::where('recurring_task_id', $id)->delete();
                }

                $taskFiles = TaskFile::where('task_id', $task->id)->get();

                foreach ($taskFiles as $file) {
                    Files::deleteFile($file->hashname, 'task-files/' . $file->task_id);
                    $file->delete();
                }

                ProjectsLogs::where('module_id',$task->id)->where('module','tasks')->delete();
                TaskFile::where('task_id',$task->id)->delete();
                $manpowerreply = TaskPercentage::where('task_id',$task->id)->get();
                foreach ($manpowerreply as $manpowerre){
                    ProjectsLogs::where('module_id',$manpowerre->id)->where('module','task_percentage')->delete();
                    $manpowerre->delete();
                }
                if(!empty($task->segment)){
                    ProjectSegmentsProduct::where('id',$task->cost_item_id)->delete();
                }else{
                    ProjectCostItemsProduct::where('id',$task->cost_item_id)->delete();
                }

                $task->delete();
                $response['status'] = 200;
                $response['message'] = 'Task deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-tasks',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function boqCategories(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = BoqCategory::where('company_id', $user->company_id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Category List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updatePercentage(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->taskid;
                $prevqty = $request->prevqty ?: 0;
                $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                $tasks = Task::find($id);

                if ($tasks) {

                    $costitemproduct = ProjectCostItemsProduct::find($tasks->cost_item_id);
                    $totalqty = !empty($costitemproduct->qty) ? $costitemproduct->qty : 0;
                    $duedatefor=  date('Y-m-d',strtotime($tasks->due_date));
                    $duedate = !empty($tasks->due_date) ? strtotime($duedatefor.' 23:59:59') : 0;
                    $curdate = time();
                    $percentage = 0;
                    $quantity = 0;
                    if(!empty($request->percentage)&&$request->percentage>0){
                        $percentage = $request->percentage;
                        if($tasks->percentage!=$percentage){
                            $tasks->percentage = $percentage;
                        }else{
                            $percentage = 0;
                        }
                    }

                        if(!empty($request->status)&&$request->status=='inproblem'){
                            $status = 'inproblem';
                        }else{
                            if($percentage>=100){
                                $status = 'completed';
                            }elseif($duedate<$curdate&&$request->status!='completed'){
                                $status = 'delayed';
                            }elseif($tasks->percentage&&$request->status!='inproblem'){
                                $status = 'inprogress';
                            }else{
                                $status = $request->status ?: 'notstarted';
                            }
                        }

                    $tasks->status = $status;

                    $quantity = 0;
                    if ($request->quantity) {
                        $quantity = $request->quantity;
                        $tasks->taskqty =  $tasks->taskqty+$quantity;
                    }

                    $tasks->save();
                    $percentage =  numberformat($percentage);
                    $taskspercent = new TaskPercentage();
                    $taskspercent->company_id = $user->company_id;
                    $taskspercent->task_id = $tasks->id;
                    $taskspercent->added_by = $user->id;
                    $taskspercent->percentage = $percentage;
                    $taskspercent->todayqty = $quantity;
                    $taskspercent->taskqty = $tasks->taskqty;
                    $taskspercent->comment = $request->comment ?: '';
                    $taskspercent->moduletype = $request->moduletype ?: 'status';
                    $taskspercent->status = $status ?: '';
                    $taskspercent->mentionusers = $mentionusers;
                    $taskspercent->save();
                    $msgtext = 'Task status updated by ' . $user->name;
                    $headingtext = '';
                    if ($percentage > 0) {
                        $msgtext .= ' with ' . $percentage .' and quantity '. $quantity;
                        $headingtext .= 'Task Updated to '.$percentage.'%';
                    }
                    if (!empty($status)) {
                        if($headingtext){
                            $headingtext .= ' with status '.$status;
                        }else{
                            $headingtext .= $status;
                        }
                    }
                    $medium = !empty($request->medium) ? $request->medium : 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $user->company_id;
                    $createlog->added_id = $user->id;
                    $createlog->module_id = $taskspercent->id;
                    $createlog->module = 'task_percentage';
                    $createlog->project_id = $tasks->project_id;
                    $createlog->subproject_id = $tasks->title ?: 0;
                    $createlog->segment_id = $tasks->segment ?: 0;
                    if(!empty($percentage)){
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_percentage_updated';
                    }
                    if (!empty($request->comment)) {
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_comment';
                    }
                    if (!empty($status)) {
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_status';
                    }
                    $createlog->description = $msgtext;
                    $createlog->medium = $medium;
                    $createlog->mentionusers = $mentionusers;
                    $createlog->save();

                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $tasks->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Tasks';
                        $notifmessage['body'] = 'Status Updated in tasks  at '.get_project_name($tasks->project_id).' project by '.$user->name;
                        $notifmessage['activity'] = 'tasks';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }



                    $response['status'] = 200;
                    $response['percentageid'] = $taskspercent->id;
                    $response['message'] = 'Task updated';
                    return $response;
                }
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'tasks-percentage',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateComment(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->taskid;
                $prevqty = $request->prevqty ?: 0;
                $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                $tasks = Task::find($id);

                if (!empty($tasks)) {

                    $taskspercent = new TaskPercentage();
                    $taskspercent->company_id = $user->company_id;
                    $taskspercent->task_id = $tasks->id;
                    $taskspercent->added_by = $user->id;
                    $taskspercent->comment = $request->comment ?: '';
                    $taskspercent->moduletype = $request->moduletype ?: 'comment';
                    $taskspercent->mentionusers = $mentionusers;
                    $taskspercent->save();
                    $msgtext = 'Task Commented by ' . $user->name;
                    $headingtext = 'Task Comment';
                    $medium = !empty($request->medium) ? $request->medium : 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $user->company_id;
                    $createlog->added_id = $user->id;
                    $createlog->module_id = $taskspercent->id;
                    $createlog->module = 'task_percentage';
                    $createlog->project_id = $tasks->project_id;
                    $createlog->subproject_id = $tasks->title ?: 0;
                    $createlog->segment_id = $tasks->segment ?: 0;
                    if (!empty($request->comment)) {
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_comment';
                    }
                    $createlog->description = $msgtext;
                    $createlog->medium = $medium;
                    $createlog->mentionusers = $mentionusers;
                    $createlog->save();

                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $tasks->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Tasks';
                        $notifmessage['body'] = 'Comment in tasks  at '.get_project_name($tasks->project_id).' project by '.$user->name;
                        $notifmessage['activity'] = 'tasks';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }

                    $response['status'] = 200;
                    $response['percentageid'] = $taskspercent->id;
                    $response['message'] = 'Task updated';
                    return $response;
                }
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'tasks-percentage',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function taskTimelines(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $response = array();
                $alltimeline = collect();
                $id = $request->taskid;
                $task = Task::find($id);
                if (!empty($task->id)) {
                    $taskpercentagearray = TaskPercentage::select('*', 'id as percentid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($taskpercentagearray as $taskpercentage) {
                        $alltimeline = $alltimeline->push($taskpercentage);
                    }
                    $manpowerlogsarray = ManpowerLog::select('*', 'id as manpowerid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($manpowerlogsarray as $manpowerlogs) {
                        $alltimeline = $alltimeline->push($manpowerlogs);
                    }
                    $punchitemlogsarray = PunchItem::select('*', 'id as punchitemid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($punchitemlogsarray as $punchitemlogs) {
                        $alltimeline = $alltimeline->push($punchitemlogs);
                    }
                    $page = $request->page;
                    $response = array();
                    if($page=='all'){
                        $alltimelinearray = $alltimeline->sortByDesc('created_at');
                    }else{
                        $count = pagecount();
                        $skip = 0;
                        if($page){
                            $skip = $page*$count;
                        }
                        $alltimelinearray = $alltimeline->sortByDesc('created_at')->forPage($page,$count);
                    }
                    $awsurl = awsurl();
                    $url = uploads_url();
                    $companyid = $user->company_id;
                    $storage = storage();
                    $timelinearray = array();
                    if ($alltimelinearray) {
                        foreach ($alltimelinearray as $timeline) {
                            $tablename = $timeline->getTable();
                            $tablearray = array();
                            $tablearray['id'] = $timeline->id;
                            $tablearray['username'] = get_user_name($timeline->added_by);
                            $tablearray['userimage'] = get_users_image_link($timeline->added_by);
                            switch ($tablename) {
                                case 'task_percentage':
                                    $tablearray['module'] = 'taskupdate';
                                    if($timeline->moduletype!='percentage'){
                                        $tablearray['status'] = $timeline->status;
                                    }
                                    if ($timeline->comment) {
                                        $tablearray['comment'] = $timeline->comment;
                                    }
                                    if ($timeline->percentage) {
                                        $tablearray['percentage'] = $timeline->percentage;
                                    }
                                    $tablearray['quantity'] = $timeline->todayqty ?: '';
                                    $tablearray['unit_name'] = get_unit_name($task->unit_id);
                                    $tablearray['completed_quantity'] = $task->taskqty;
                                    $files = \App\TaskFile::where('task_id', $timeline->task_id)->where('task_percentage_id', $timeline->percentid)->get();
                                    foreach ($files as $file) {
                                        $fx = explode('.', $file->hashname);
                                        $ext = $fx[(count($fx) - 1)];
                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'/task-files/' . $timeline->task_id . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/task-files/' . $timeline->task_id . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d M Y h:i A');
                                         $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d M Y h:i A');
                                      break;
                                case 'manpower_logs':
                                    $tablearray['uniqueid'] = $timeline->unique_id;
                                    $tablearray['module'] = 'manpower';
                                    $tablearray['manpower'] = $timeline->manpower;
                                    $tablearray['workinghours'] = $timeline->workinghours;
                                    $tablearray['shift'] = $timeline->shift;
                                    $tablearray['salaryperday'] = $timeline->salaryperday;
                                    $tablearray['hourspershift'] = $timeline->hourspershift;
                                    $tablearray['totalprice'] = $timeline->totalprice;

                                    $files = \App\ManpowerLogFiles::where('manpower_id', $timeline->manpowerid)->where('reply_id','0')->get();
                                    foreach ($files as $file) {
                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'manpower-log-files/' . $timeline->manpowerid . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/manpower-log-files/' . $timeline->manpowerid . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d M Y h:i A');
                                        $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d M Y h:i A');
                                    break;
                                case 'punch_item':
                                    $tablearray['module'] = 'issue';
                                    $tablearray['title'] = $timeline->title;
                                    $tablearray['status'] = $timeline->status;
                                    $files = \App\PunchItemFiles::where('task_id', $timeline->punchitemid)->where('reply_id','0')->get();
                                    foreach ($files as $file) {

                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'punch-files/' . $timeline->punchitemid . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/punch-files/' . $timeline->punchitemid . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d M Y h:i A');
                                        $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d M Y h:i A');

                                    break;
                            }
                            $timelinearray[] = $tablearray;
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Task Percentage';
                    $response['responselist'] = $timelinearray;
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Task Not found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'tasks-timelines',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $taskid = $request->taskid;
                $tasks = Task::find($taskid);
                $percentageid = $request->percentageid;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                        $file = new TaskFile();
                        $file->user_id = $user->id;
                        $file->company_id = $user->company_id;
                        $file->task_id = $taskid ?: 0;
                        $file->task_percentage_id = $percentageid ?: 0;
                        switch($storage) {
                            case 'local':
                                $destinationPath = 'uploads/task-files/'.$taskid;
                                if (!file_exists($destinationPath)) {
                                    mkdir($destinationPath, 0777, true);
                                }
                                $fileData->storeAs($destinationPath, $fileData->hashName());
                                break;
                            case 's3':
                                Storage::disk('s3')->putFileAs('/task-files/'.$taskid, $fileData, $fileData->hashName(), 'public');
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', 'task-files')
                                    ->first();

                            if (!$dir) {
                                Storage::cloud()->makeDirectory('task-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $taskid)
                                ->first();

                            if (!$directory) {
                                Storage::cloud()->makeDirectory($dir['path'] . '/' . $taskid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $taskid)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/' . $taskid . '/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/' . $taskid . '/' . $fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }
                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();


                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $tasks->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Tasks';
                        $notifmessage['body'] = 'New Image has uploaded in tasks  at '.get_project_name($tasks->project_id).' project by '.$user->name;
                        $notifmessage['activity'] = 'tasks';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }


                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Task Image Uploaded';
                    return $response;

                }
                $response['status'] = 300;
                $response['message'] = 'Uploading failed. Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function activityList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $response = array();
            $projectid = $request->project_id;
            $subprojectid = $request->subproject_id ?: 0;
            $segmentid = $request->segment_id ?: 0;
            $activityarray = array();
            $activityarray['projectid'] = $projectid;
            $activityarray['subprojectid'] = $subprojectid;
            $activityarray['segmentid'] = $segmentid;
            $activityarray['level'] = 0;
            $activityarray['parent'] = 0;
            $activityarray['snorow'] = 0;
            $activitlist = $this->activityloop($activityarray);
            $response['status'] = 200;
            $response['message'] = 'Category List Fetched';
            $response['response'] = $activitlist;
            return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'activitylist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function taskbyActivity(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $response = array();
            $projectid = $request->project_id;
            $subprojectid = $request->subproject_id ?: 0;
            $segmentid = $request->segment_id ?: 0;
            $positionid = $request->position_id ?: 0;
            $activityarray = array();
            $activityarray['projectid'] = $projectid;
            $activityarray['subprojectid'] = $subprojectid;
            $activityarray['segmentid'] = $segmentid;
            $activityarray['positionid'] = $positionid;
            $activityarray['level'] = 0;
            $activityarray['parent'] = 0;
            $activitlist = $this->taskloop($activityarray);
            $response['status'] = 200;
            $response['message'] = 'Task List Fetched';
            $response['response'] = $activitlist;
            return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'activitylist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    protected function activityloop($actarray){
        $projectid = $actarray['projectid'];
        $subprojectid = $actarray['subprojectid'];
        $segmentid = $actarray['segmentid'];
        $level = $actarray['level'];
        $parent = $actarray['parent'];
        $snorow = $actarray['snorow'];
        if(!empty($segmentid)) {
            $projectcostitem = ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
        }else{
            $projectcostitem = ProjectCostItemsPosition::where('project_id',$projectid);
        }
        if(!empty($subprojectid)){
            $projectcostitem = $projectcostitem->where('title',$subprojectid);
        }
        $projectcostitemarray = $projectcostitem->where('position','row')->where('parent',$parent)->where('level',$level)->get();
        $categorynames = $catraray = array();
        $act=1;  foreach ($projectcostitemarray as $projectactivty){
            $explodeact =  array_filter(explode(',',$projectactivty->catlevel));
            $explodeact =  count($explodeact)+1;
            $catlevel = $projectactivty->itemid;
            if(!empty($projectactivty->catlevel)){
                $catlevel = $projectactivty->catlevel.','.$projectactivty->itemid;
            }
            if((int)$projectactivty->level==0){
                $snorow = $snorow+1;
                $actrow = $snorow;
            }else{
                $actrow = $snorow.'.'.$act;
            }
            $newsno = $actrow.'.0';
            $projectactivty->sno = $actrow;
            $projectactivty->save();
            $categorynames['id'] = $projectactivty->id;
            $categorynames['sno'] = $newsno;
            $categorynames['level'] = $projectactivty->level;
            $categorynames['dotcount'] = $explodeact;
            $categorynames['itemid'] = $projectactivty->itemid;
            $categorynames['name'] = $projectactivty->itemname;
            $categorynames['newlevel'] = (int)$projectactivty->level+1;
            $categorynames['catlevel'] = $catlevel;
            $catraray[] = $categorynames;

            $newlevel = (int)$projectactivty->level+1;
            $activityarray = array();
            $activityarray['projectid'] = $projectid;
            $activityarray['subprojectid'] = $subprojectid;
            $activityarray['segmentid'] = $segmentid;
            $activityarray['level'] = $newlevel;
            $activityarray['parent'] = $projectactivty->id;
            $activityarray['snorow'] = $actrow;
            $boqcategory = $this->activityloop($activityarray);
            $catraray = array_merge($catraray, $boqcategory);
            $act++;
        }
        return $catraray;
    }
    protected function taskloop($actarray){
        $projectid = $actarray['projectid'];
        $subprojectid = $actarray['subprojectid'];
        $segmentid = $actarray['segmentid'];
        $positionid = $actarray['positionid'];
        $level = $actarray['level'];
        $parent = $actarray['parent'];
        if(!empty($segmentid)) {
            $projectcostitem = ProjectSegmentsProduct::where('project_id',$projectid)->where('segment',$segmentid);
        }else{
            $projectcostitem = ProjectCostItemsProduct::where('project_id',$projectid);
        }
        if(!empty($subprojectid)){
            $projectcostitem = $projectcostitem->where('title',$subprojectid);
        }
        $projectcostitemarray = $projectcostitem->where('position_id',$positionid)->get();
          $boqcategoryarary = array();
        foreach ($projectcostitemarray as $costitem){
            $task = Task::where('project_id', $projectid)->where('title', $subprojectid)->where('cost_item_id', $costitem->id)->first();
            if ($task) {
                $boqsubcategoryarary = array();
                $boqsubcategoryarary['module'] = 'task';
                $boqsubcategoryarary['id'] = $task->id;
                $boqsubcategoryarary['boqid'] = $costitem->id;
                $boqsubcategoryarary['category'] = $costitem->category;
                $boqsubcategoryarary['position_id'] = (int)$costitem->position_id;
                $boqsubcategoryarary['heading'] = $task->heading;
                $boqsubcategoryarary['start_date'] = !empty(!empty($costitem->start_date)) ? date('d M Y', strtotime($costitem->start_date)) : '';
                $boqsubcategoryarary['due_date'] = !empty(!empty($costitem->deadline)) ? date('d M Y', strtotime($costitem->deadline)) : '';
                $boqsubcategoryarary['percentage'] = !empty($task) ? numberformat($task->percentage) : 0;
                $boqsubcategoryarary['quantity'] = !empty($costitem) ? $costitem->qty : 0;
                $boqsubcategoryarary['rate'] = !empty($costitem) ? $costitem->rate : 0;
                $boqsubcategoryarary['description'] = !empty($costitem) ? $costitem->description : '';
                $boqsubcategoryarary['contractor'] = !empty($costitem->contractor_employee_id) ?  $costitem->contractor_employee_id : '';
                $boqsubcategoryarary['contractor_name'] = !empty($costitem->contractor_employee_id) ? get_employee_name($costitem->contractor_employee_id) : '';
                $boqsubcategoryarary['project_name'] = !empty($costitem->project_id) ? get_project_name($costitem->project_id) : '';
                $boqsubcategoryarary['subproject_name'] = !empty($costitem->title) ? get_title($costitem->title) : '';
                $boqsubcategoryarary['segment_name'] = !empty($costitem->segment) ? get_segment($costitem->segment) : '';
                $boqsubcategoryarary['assign_to'] = !empty($costitem->assign_to) ? $costitem->assign_to : '';
                $boqsubcategoryarary['employee_id'] = !empty($costitem->employee_id) ? $costitem->employee_id : '';
                $assignnae = '';
                if(!empty($costitem->employee_id)){
                    $assignnae = get_employee_name($costitem->employee_id);
                }elseif (!empty($costitem->assign_to)){
                    $assignnae = get_user_name($costitem->assign_to);
                }
                $boqsubcategoryarary['assign_to_name'] = $assignnae;
                $deadline = !empty($costitem->deadline) ? date('d M Y', strtotime($costitem->deadline)) : '';
                $date1 = date('d M Y');
                if(!empty($task->percentage)) {
                    if($task->percentage == '100'){
                        $boqsubcategoryarary['status'] = 'completed';
                    } elseif($deadline < $date1){
                        $boqsubcategoryarary['status'] = 'delayed';
                    } else {
                        $boqsubcategoryarary['status'] = !empty($task->status) ? $task->status : '';
                    }
                } elseif($deadline < $date1){
                    $boqsubcategoryarary['status'] = 'delayed';
                }else {
                    $boqsubcategoryarary['status'] = !empty($task->status) ? $task->status : '';
                }
                $boqsubcategoryarary['unit_name'] = !empty($task->unit_id) ? get_unit_name($task->unit_id) : '';
                $boqsubcategoryarary['total_quantity'] = !empty($task->quantity) ? $task->quantity : '';
                $boqsubcategoryarary['rate'] = !empty($task->rate) ? get_unit_name($task->rate) : '';
                $boqsubcategoryarary['total_amount'] = !empty($task->amount) ? get_unit_name($task->amount) : '';
                $boqsubcategoryarary['remaining_quantity'] = !empty($task->completed_quantity) ? (!empty($task->quantity) ? ($task->quantity - $task->completed_quantity) : '' ) : '';
                $boqsubcategoryarary['completed_quantity'] = !empty($task->completed_quantity) ? $task->completed_quantity : '';
                $boqcategoryarary[] = $boqsubcategoryarary;
            }
        }
        return $boqcategoryarary;
    }
    public function boqList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = explode(',',$request->project_id);
                $search = $request->search;
                if (!empty($projectid)) {
                    $subprojectid = $request->subproject_id ?: 0;
                    $segmentid = $request->segment_id ?: 0;
                    $positionid = $request->positionid ?: 0;
                    $status = $request->status ? explode(',',$request->status) : array();
                    $boqarray = array();
                    $boqlist['projectid'] = $projectid;
                    $boqlist['subprojectid'] = $subprojectid;
                    $boqlist['segmentid'] = $segmentid;
                    $boqlist['level'] = 0;
                    $boqlist['parent'] = 0;
                    $boqlist['status'] = $status;
                    $boqlist['search'] = $search;
                    $boqlist['snorow'] = 0;
                    $boqlist['positionid'] = $positionid;
                    $boqarray = $this->boqdata($boqlist);
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'boqlist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 200;
            $response['message'] = 'BOQ List Fetched';
            $response['response'] = $boqarray;
            return $response;
        }
        return response()->json(['error' => 'Unauthorised'], 401);
    }
    public function boqTaskList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $search = $request->search;
                if (!empty($projectid)) {
                    $subprojectid = $request->subproject_id ?: 0;
                    $segmentid = $request->segment_id ?: 0;
                    $positionid = $request->positionid ?: 0;
                    $page = $request->page ?: 0;
                    $status = $request->status ? explode(',',$request->status) : array();
                    if (!empty($segmentid)) {
                        $costitemarray = \App\ProjectSegmentsProduct::where('project_id', $projectid)->where('title', $subprojectid)->where('segment', $segmentid)->where('position_id', $positionid)->get();
                    } else {
                        $positionarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('id',$positionid)->first();
                        $postnow = $positionarray->sno;
                        $costitemarray = \App\ProjectCostItemsProduct::join('tasks','tasks.cost_item_id','=','project_cost_items_product.id')
                            ->select('project_cost_items_product.*','tasks.company_id as company_id','tasks.id as taskid','tasks.heading','tasks.task_category_id','tasks.percentage','tasks.status','tasks.taskqty')
                            ->where('project_cost_items_product.project_id', $projectid)->where('project_cost_items_product.title', $subprojectid)
                            ->where('project_cost_items_product.position_id', $positionid)->where('tasks.boqinclude', '1');
                        if(!empty($status)){
                            $costitemarray = $costitemarray->whereIn('tasks.status', $status);
                        }
                        if(!empty($search)){
                            $costitemarray = $costitemarray->where('tasks.heading','LIKE','%'.$search.'%');
                        }
                    }
                    $count = pagecount();
                    $page = $request->page;
                    if($page=='all'){
                        $r=1;
                        $costitemarray = $costitemarray->get();
                    }else{
                        $skip = 0;
                        $r= 1;
                        if($page){
                            $skip = $page*$count;
                            $r=$page*$count;
                        }
                        $totalcostitemarray = $costitemarray->offset($skip)->take($count)->get();
                    }
                    $costitemcount = $costitemarray->count();
                    $boqcategoryarary = array();
                    foreach ($totalcostitemarray as $costitem) {
                        if ($costitem) {
                            $newsnorow= !empty($costitem->sno) ? $costitem->sno : '';
                            if($postnow){
                                $newsnorow = $postnow.'.'.$r;
                                $costitem->sno = $newsnorow;
                                $costitem->save();
                            }
                            $boqsubcategoryarary = array();
                            $boqsubcategoryarary['module'] = 'task';
                            $boqsubcategoryarary['id'] = $costitem->taskid;
                            $boqsubcategoryarary['boqid'] = $costitem->id;
                            $boqsubcategoryarary['sno'] = $newsnorow;
                            $boqsubcategoryarary['category'] = $costitem->task_category_id;
                            $boqsubcategoryarary['position_id'] = (int)$costitem->position_id;
                            $boqsubcategoryarary['heading'] = $costitem->heading;
                            $boqsubcategoryarary['start_date'] = !empty(!empty($costitem->start_date)) ? date('d M Y', strtotime($costitem->start_date)) : '';
                            $boqsubcategoryarary['due_date'] = !empty(!empty($costitem->deadline)) ? date('d M Y', strtotime($costitem->deadline)) : '';
                            $boqsubcategoryarary['percentage'] = !empty($costitem->percentage) ? numberformat($costitem->percentage) : 0;
                            $boqsubcategoryarary['quantity'] = !empty($costitem) ? $costitem->qty : 0;
                            $boqsubcategoryarary['rate'] = !empty($costitem) ? $costitem->rate : 0;
                            $boqsubcategoryarary['description'] = !empty($costitem) ? $costitem->description : '';
                            $boqsubcategoryarary['contractor'] = !empty($costitem->contractor_employee_id) ?  $costitem->contractor_employee_id : '';
                            $boqsubcategoryarary['contractor_name'] = !empty($costitem->contractor_employee_id) ? get_employee_name($costitem->contractor_employee_id) : '';
                            $boqsubcategoryarary['project_id'] = $costitem->project_id;
                            $boqsubcategoryarary['project_name'] = !empty($costitem->project_id) ? get_project_name($costitem->project_id) : '';
                            $boqsubcategoryarary['subproject_id'] = $costitem->title;
                            $boqsubcategoryarary['subproject_name'] = !empty($costitem->title) ? get_title($costitem->title) : '';
                            $boqsubcategoryarary['segment_id'] = !empty($costitem->segment) ? $costitem->segment : '0';
                            $boqsubcategoryarary['segment_name'] = !empty($costitem->segment) ? get_segment($costitem->segment) : '';
                            $boqsubcategoryarary['assign_to'] = !empty($costitem->assign_to) ? $costitem->assign_to : '';
                            $boqsubcategoryarary['employee_id'] = !empty($costitem->employee_id) ? $costitem->employee_id : '';
                            $assignnae = '';
                            if(!empty($costitem->employee_id)){
                                $assignnae = get_employee_name($costitem->employee_id);
                            }elseif (!empty($costitem->assign_to)){
                                $assignnae = get_user_name($costitem->assign_to);
                            }
                            $boqsubcategoryarary['assign_to_name'] = $assignnae;
                            $boqsubcategoryarary['status'] = !empty($costitem->status) ? $costitem->status : '';
                            $boqsubcategoryarary['unit_name'] = !empty($costitem->unit) ? get_unit_name($costitem->unit) : '';
                            $boqsubcategoryarary['total_quantity'] = !empty($costitem->qty) ? $costitem->qty : 0;
                            $boqsubcategoryarary['total_amount'] = !empty($costitem->amount) ?  $costitem->amount : '';
                            $taskqty = $costitem->taskqty ?: 0;
                            $boqsubcategoryarary['remaining_quantity'] =  numberformat($costitem->qty - $taskqty) ?: 0;
                            $boqsubcategoryarary['completed_quantity'] = !empty($costitem->taskqty) ? numberformat($costitem->taskqty) : '';
                            $boqcategoryarary[] = $boqsubcategoryarary;
                            $r++;  }
                        }
                    $response['status'] = 200;
                    $response['message'] = 'BOQ List Fetched';
                    $response['response'] = $boqcategoryarary;
                    $totaltask = !empty($costitemcount) ? $costitemcount : 0;
                    $skip = $page*$count;
                    if($totaltask<9){
                        $skip = $totaltask;
                    }
                    $response['totaltask'] = $totaltask;
                    $response['fetched'] = $skip;
                    $response['pending'] = $totaltask-$skip;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'boqlist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 200;
            $response['message'] = 'BOQ List Fetched';
            return $response;
        }
        return response()->json(['error' => 'Unauthorised'], 401);
    }
    protected function boqdata($boqarray)
    {
        $projectid = $boqarray['projectid'];
        $subprojectid = $boqarray['subprojectid'];
        $segmentid = $boqarray['segmentid'];
        $level = $boqarray['level'];
        $status = !empty($boqarray['status']) ? $boqarray['status'] : array();
        $search = !empty($boqarray['search']) ? $boqarray['search'] : '';
        $snorow = !empty($boqarray['snorow']) ? $boqarray['snorow'] : 0;
        $parent = $boqarray['parent'];
        if (!empty($segmentid)) {
            $subcategorycostitemarray = \App\ProjectSegmentsPosition::whereIn('project_id', $projectid)->where('title', $subprojectid)
                ->where('segment', $segmentid)->where('position', 'row')->where('parent', $parent)->where('level', $level)
                ->orderBy('inc', 'asc')->get();
        } else {
            $subcategorycostitemarray = \App\ProjectCostItemsPosition::whereIn('project_id', $projectid)
                ->where('title', $subprojectid)->where('position', 'row')->where('parent', $parent)
                ->where('level', $level)->orderBy('inc', 'asc')->get();
        }
        $boqcategoryarary = array();
        $act = 1;
        if (!empty($subcategorycostitemarray)) {
            foreach ($subcategorycostitemarray as $subcategorycostitem) {

                if ((int)$subcategorycostitem->level == 0) {
                    $snorow = $snorow + 1;
                    $actrow = $snorow;
                } else {
                    $actrow = $snorow . '.' . $act;
                }
                $newsno = $actrow . '.0';
                $subcategorycostitem->sno = $actrow;
                $subcategorycostitem->save();

                $costitemarray = array();
                if (!empty($segmentid)) {
                    $costitemarray = \App\ProjectSegmentsProduct::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('segment', $segmentid)->where('position_id', $subcategorycostitem->id)->get();
                } else {
                    $costitemarray = \App\ProjectCostItemsProduct::join('tasks', 'tasks.cost_item_id', '=', 'project_cost_items_product.id')
                        ->select('project_cost_items_product.*', 'tasks.company_id as company_id', 'tasks.id as taskid', 'tasks.heading', 'tasks.percentage', 'tasks.status', 'tasks.taskqty')
                        ->whereIn('project_cost_items_product.project_id', $projectid)->where('project_cost_items_product.title', $subprojectid)
                        ->where('project_cost_items_product.position_id', $subcategorycostitem->id)->where('tasks.boqinclude', '1');
                    if (!empty($status)) {
                        $costitemarray = $costitemarray->whereIn('tasks.status', $status);
                    }
                    if (!empty($search)) {
                        $costitemarray = $costitemarray->where('tasks.heading', 'LIKE', '%' . $search . '%');
                    }
                    $costitemarray = $costitemarray->get();
                }
                if (count($costitemarray) > 0 ||(count($costitemarray) > 0 &&!empty($search))||(count($costitemarray) > 0&&!empty($status))||(empty($status)&&empty($search))) {
                    $showlist = true;
                    if(count($costitemarray) <= 0&&$subcategorycostitem->itemname=='General'){
                        $showlist = false;
                    }
                    if($showlist){
                        $boqsubcategoryarary = array();
                        $boqsubcategoryarary['module'] = 'activity';
                        $boqsubcategoryarary['id'] = $subcategorycostitem->id;
                        $boqsubcategoryarary['sno'] = !empty($newsno) ? $newsno : '';
                        $boqsubcategoryarary['itemid'] = $subcategorycostitem->itemid;
                        $boqsubcategoryarary['itemname'] = $subcategorycostitem->itemname;
                        $boqsubcategoryarary['level'] = $subcategorycostitem->level;
                        $boqsubcategoryarary['catlevel'] = $subcategorycostitem->catlevel;
                        $boqsubcategoryarary['count'] = !empty($subcategorycostitem->catlevel) ? count(array_filter(explode(',', $subcategorycostitem->catlevel))) : 0;
                        $boqsubcategoryarary['parent'] = $subcategorycostitem->parent;
                        $boqcategoryarary[] = $boqsubcategoryarary;
                    }
                }
                    $newlevel = (int)$subcategorycostitem->level + 1;
                    $boqlist['projectid'] = $projectid;
                    $boqlist['subprojectid'] = $subprojectid;
                    $boqlist['segmentid'] = $segmentid;
                    $boqlist['level'] = $newlevel;
                    $boqlist['parent'] = $subcategorycostitem->id;
                    $boqlist['status'] = $status;
                    $boqlist['snorow'] = $actrow;
                    $boqcategory = $this->boqdata($boqlist);
                    $boqcategoryarary = array_merge($boqcategoryarary, $boqcategory);

                    $act++;
            }
            return $boqcategoryarary;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function boq(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status']==401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $boqsubcategoryarary = array();
                $taskid = $request->taskid;
                $costitemid = $request->boqid;  
                    if(!empty($taskid)&&!empty($costitemid)){
                    $costitem = \App\ProjectCostItemsProduct::where('id',$costitemid)->first();
                        if(!empty($costitem->id)){
                            $task = Task::where('id',$taskid)->where('cost_item_id',$costitem->id)->first();
                            if(!empty($task->id)){
                                $boqsubcategoryarary['id'] = $task->id;
                                $boqsubcategoryarary['boqid'] = $costitem->id;
                                $boqsubcategoryarary['category'] = $costitem->category;
                                $boqsubcategoryarary['heading'] = $task->heading;
                                $boqsubcategoryarary['start_date'] = !empty(!empty($task->start_date)) ? date('d M Y', strtotime($task->start_date)) : '';
                                $boqsubcategoryarary['due_date'] = !empty(!empty($task->due_date)) ? date('d M Y', strtotime($task->due_date)) : '';
                                $boqsubcategoryarary['percentage'] = !empty($task) ? numberformat($task->percentage) : 0;
                                $boqsubcategoryarary['quantity'] = !empty($costitem) ? $costitem->qty : 0;
                                $boqsubcategoryarary['rate'] = !empty($costitem) ? $costitem->rate : 0;
                                $boqsubcategoryarary['description'] = !empty($costitem) ? $costitem->description : '';
                                $boqsubcategoryarary['contractor'] = !empty($costitem->contractor_employee_id) ?  $costitem->contractor_employee_id : '';
                                $boqsubcategoryarary['contractor_name'] = !empty($costitem->contractor_employee_id) ? get_employee_name($costitem->contractor_employee_id) : '';
                                $boqsubcategoryarary['project_id'] = $costitem->project_id;
                                $boqsubcategoryarary['project_name'] = !empty($costitem->project_id) ? get_project_name($costitem->project_id) : '';
                                $boqsubcategoryarary['subproject_id'] = $costitem->title;
                                $boqsubcategoryarary['subproject_name'] = !empty($costitem->title) ? get_title($costitem->title) : '';
                                $boqsubcategoryarary['segment_id'] = !empty($costitem->segment) ? $costitem->segment : '0';
                                 $boqsubcategoryarary['segment_name'] = !empty($costitem->segment) ? get_segment($costitem->segment) : '';
                                $boqsubcategoryarary['assign_to'] = !empty($costitem->assign_to) ?  $costitem->assign_to : '0';
                                $boqsubcategoryarary['employee_id'] = !empty($costitem->employee_id) ?  $costitem->employee_id : '0';
                                $assignnae = '';
                                if(!empty($costitem->employee_id)){
                                    $assignnae = get_employee_name($costitem->employee_id);
                                }elseif (!empty($costitem->assign_to)){
                                    $assignnae = get_user_name($costitem->assign_to);
                                }
                                $boqsubcategoryarary['assign_to_name'] = $assignnae;
                                $boqsubcategoryarary['status'] = !empty($task->status) ? $task->status : '';
                                $boqsubcategoryarary['unit_id'] = $costitem->unit;
                                $boqsubcategoryarary['unit_name'] = !empty($costitem->unit) ?  get_unit_name($costitem->unit) : '';
                                $boqsubcategoryarary['total_quantity'] = !empty($costitem->qty) ? $costitem->qty : 0;
                                $boqsubcategoryarary['total_amount'] = !empty($costitem->amount) ?  $costitem->amount : '0';
                                $taskqty = $task->taskqty ?: 0;
                                $boqsubcategoryarary['remaining_quantity'] =  numberformat($costitem->qty - $taskqty) ?: 0;
                                $boqsubcategoryarary['completed_quantity'] = !empty($task->taskqty) ? numberformat($task->taskqty) : '0';

                                $imagesarray = array();
                                $tasksfilesarray = TaskFile::where('task_id', $task->id)->where('task_percentage_id', '0')->get();
                                if(count($tasksfilesarray)>0){
                                $storage = storage();
                                $awsurl = awsurl();
                                $url = uploads_url();
                                    foreach ($tasksfilesarray as $tasksfiles){
                                        $image = array();
                                        $image['id'] = $tasksfiles->id;
                                        $image['task_id'] = $tasksfiles->task_id;
                                        $image['imagename'] = $tasksfiles->filename;
                                        $image['file_manager_id'] = !empty($tasksfiles->filemanager_id) ? $tasksfiles->filemanager_id : 0;

                                        $revisionlatestfile = AppFileManager::where('id',$tasksfiles->filemanager_id)->where('revisionfileid','<>','0')->orderBy('id','desc')->first();
                                        if(!empty($revisionlatestfile)){
                                            $link = '';
                                            if($revisionlatestfile->type=='file'){
                                                if(!empty($revisionlatestfile->external_link)){
                                                    $link = $revisionlatestfile->external_link;
                                                    $documents['filename'] = $revisionlatestfile->external_link_name;
                                                }else{
                                                    $url = uploads_url();
                                                    $awsurl = awsurl();
                                                    $hashname = $revisionlatestfile->hashname;
                                                    $goggleurl = $revisionlatestfile->google_url;
                                                    $dropbox_link = $revisionlatestfile->dropbox_link;
                                                    if(empty($revisionid)&&$revisionlatestfile->revisionfileid==0){
                                                        $latestrevisionfile = AppFileManager::where('revisionfileid',$revisionlatestfile->id)->orderBy('id','desc')->first();
                                                        if(!empty($latestrevisionfile->id)){
                                                            $hashname = $latestrevisionfile->hashname;
                                                            $goggleurl = $latestrevisionfile->google_url;
                                                            $dropbox_link = $latestrevisionfile->dropbox_link;
                                                        }
                                                    }
                                                    switch ($storage){
                                                        case 'local':
                                                            $folder = '';
                                                            $profolder = $revisionlatestfile->project_id;
                                                            $x=1;
                                                            $p = $revisionlatestfile->parent;
                                                            while($x==1){
                                                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                                                if($fn !== null){
                                                                    $folder = $fn->id.'/'.$folder;
                                                                    $p = $fn->parent;
                                                                }else{
                                                                    $x=0;
                                                                }
                                                            }
                                                            $link = $url.'project-files/'.$profolder.'/'.$folder.$hashname;
                                                            $thumbnaillink = $url.'project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                                                            break;
                                                        case 's3':
                                                            $folder = '';
                                                            $profolder = $revisionlatestfile->project_id;
                                                            $x=1;
                                                            $p = $revisionlatestfile->parent;
                                                            while($x==1){
                                                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                                                if($fn !== null){
                                                                    $folder = $fn->id.'/'.$folder;
                                                                    $p = $fn->parent;
                                                                }else{
                                                                    $x=0;
                                                                }
                                                            }
                                                            $link = $awsurl.'project-files/'.$profolder.'/'.$folder.$hashname;
                                                            $thumbnaillink = $awsurl.'project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                                                            break;
                                                        case 'google':
                                                            $link =  $goggleurl;
                                                            $thumbnaillink =  $goggleurl;
                                                            break;
                                                        case 'dropbox':
                                                            $link =  $dropbox_link;
                                                            $thumbnaillink =  $dropbox_link;
                                                            break;
                                                    }
                                                }
                                            }
                                            $image['imageurl'] = $link;
                                        }else{
                                            $imagename = $tasksfiles->hashname;
                                            if($imagename){
                                                $id = $tasksfiles->task_id;
                                                if($storage){
                                                    switch($storage){
                                                        case 'local':
                                                            $image['imageurl'] = $url.'task-files/'.$id.'/'.$imagename;
                                                            break;
                                                        case 's3':
                                                            $image['imageurl'] =  $awsurl.'/task-files/'.$id.'/'.$imagename;
                                                            break;
                                                        case 'google':
                                                            $image['imageurl'] =  $tasksfiles->google_url;
                                                            break;
                                                        case 'dropbox':
                                                            $image['imageurl'] =  $tasksfiles->dropbox_link;
                                                            break;
                                                    }
                                                }
                                            }
                                        }


                                        $image['created_at'] = Carbon::parse($tasksfiles->created_at)->format('d-m-Y');
                                        $imagesarray[] = $image;
                                    }
                                }
                                $boqsubcategoryarary['imagesarray'] = $imagesarray;

                                $response['status'] = 200;
                                $response['message'] = 'BOQ Info';
                                $response['response'] = $boqsubcategoryarary;
                            }else{
                                $response['status'] = 300;
                                $response['message'] = 'Task not found';
                            }
                        }else{
                            $response['status'] = 300;
                            $response['message'] = 'Task not found';
                        }
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'Task not found';
                    }
                     return $response;

                    } catch (\Exception $e) {
                        $userid = !empty($user->id) ? $user->id : 0;
                        app_log($e,'boq',$userid);
                        $response['status'] = 500;
                        $response['message'] = $e->getMessage();
                        $response['line'] = $e->getLine();
                        $response['file'] = $e->getFile();
                        return $response;
                    }
            $response['status'] = 300;
            $response['message'] = 'Task not found';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function boqUpdate(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                if ($request->taskid && $request->boqid) {
                    $taskid = $request->taskid;
                    $costitemid = $request->boqid;
                    $parameter = $request->parameter;
                    $value = $request->value;
                    $unit_id = $request->unit_id;
                    if($parameter!='contractor'){
                        $taskdetails = Task::find($taskid);
                         if ($parameter == 'employee_id') {
                                $taskdetails->employee_id = $value;
                                $userid = get_employee_userid($value);
                             $taskdetails->user_id = $userid;
                        }elseif ($parameter == 'user_id') {
                            $taskdetails->user_id = $value;
                        }elseif ($parameter == 'start_date') {
                            $taskdetails->start_date = !empty($value) ? date('Y-m-d',strtotime($value)) : '';
                        }else{
                           $taskdetails->$parameter = $value;
                        }
                        $taskdetails->save();
                    }

                    $costdetails = ProjectCostItemsProduct::find($costitemid);
                    $prevqty = $costdetails->qty;
                    if ($parameter == 'due_date') {
                        $costdetails->deadline = !empty($value) ? date('Y-m-d',strtotime($value)) : '';
                    } else  if ($parameter == 'start_date') {
                        $costdetails->start_date = !empty($value) ? date('Y-m-d',strtotime($value)) : '';
                    } elseif ($parameter == 'user_id') {
                            $costdetails->assign_to = $value;
                    } elseif ($parameter == 'employee_id') {
                            $costdetails->employee_id = $value;
                        $userid = get_employee_userid($value);
                        $costdetails->assign_to = $userid;
                    } elseif($parameter == 'quantity') {
                        $rate = $costdetails->rate ?: 0;
                        $costdetails->qty = $value;
                        $costdetails->amount = $value*$rate;
                    } elseif($parameter == 'rate') {
                        $qty = $costdetails->qty ?: 0;
                        $costdetails->rate = $value;
                        $costdetails->amount = $value*$qty;
                    } elseif($parameter == 'contractor') {
                        $employeeid = Employee::find($value);
                        $costdetails->contractor = $employeeid->user_id;
                        $costdetails->contractor_employee_id = $employeeid->id;
                    }else {
                        $costdetails->$parameter = $value;
                    }
                    if(!empty($request->unit_id)){
                        $costdetails->unit = !empty($unit_id) ? $unit_id : $costdetails->unit;
                    }
                    $costdetails->save();

                    if($parameter == 'quantity'||$parameter == 'due_date') {

                        $newpercent = 0;
                        $qty = 0;
                        if($parameter == 'quantity'){
                            $getrecentpercent = TaskPercentage::where('task_id', $taskdetails->id)->where('moduletype', 'percentage')->orderBy('id', 'desc')->first();
                            if(!empty($getrecentpercent)){

                                $qty = $value;

                                $getpercentage = $getrecentpercent->percentage;
                                $getqty = $getrecentpercent->taskqty;
                                if($prevqty>0&&$getqty>0){
                                    $newpercent = ($getqty/$value)*100;
                                    $qty = $getqty;
                                }else{
                                    $qty = ($getpercentage / 100) * $value;
                                    $newpercent = $getpercentage;
                                }
                            }

                            if ($newpercent > 100) {
                                $newpercent = 100;
                            }
                        }
                        $dueda = date('Y-m-d',strtotime($taskdetails->due_date));

                        $duedate = !empty($taskdetails->due_date) ? strtotime($dueda.' 23:59:59') : '';
                        $curdate = time();

                        if(!empty($taskdetails->status)&&$taskdetails->status=='inproblem'){
                            $status = 'inproblem';
                        }else  if(!empty($taskdetails->status)&&$taskdetails->status=='completed'){
                            $status = 'completed';
                        }else{
                            if($newpercent>=100){
                                $status = 'completed';
                            }elseif(!empty($duedate)&&$duedate<$curdate){
                                $status = 'delayed';
                            }elseif(!empty($taskdetails->percentage)){
                                $status = 'inprogress';
                            }else{
                                $status =  'notstarted';
                            }
                        }
                        $percentage = numberformat($newpercent);
                        $taskdetails->status = $status;
                        if($parameter == 'quantity') {
                            $taskdetails->percentage = $percentage;
                            $taskdetails->taskqty = $qty;
                        }

                        $taskdetails->save();

                        $taskspercent = new TaskPercentage();
                        $taskspercent->company_id = $user->company_id;
                        $taskspercent->task_id = $taskdetails->id;
                        $taskspercent->added_by = $user->id;
                        $taskspercent->percentage = $percentage;
                        $taskspercent->todayqty = $qty;
                        $taskspercent->taskqty = $qty;
                        $taskspercent->comment = '';
                        $taskspercent->status = $status ?: '';
                        $taskspercent->moduletype = 'percentage';
                        $taskspercent->mentionusers = '';
                        $taskspercent->save();

                    }
                    $response['status'] = 200;
                    $response['message'] = 'Task Updated Successfully';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'boqupdate',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function costItemList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
                 try{
                    $user = $validate['user'];
                    $pi = CostItems::where('company_id', $user->company_id)->get();
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $pi;
                    $response['message'] = 'Cositem list Successfully';
                    return $response;
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'costitemlist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
                 }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function taskStatusList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                    $statusarray = array();
                    $statusarray[] = array('id'=>1,'slug'=>'notstarted','name'=>'Not Started');
                    $statusarray[] = array('id'=>2,'slug'=>'inprogress','name'=>'In Progress');
                    $statusarray[] = array('id'=>3,'slug'=>'inproblem','name'=>'In Problem');
                    $statusarray[] = array('id'=>4,'slug'=>'delayed','name'=>'Delayed');
                    $statusarray[] = array('id'=>5,'slug'=>'completed','name'=>'Completed');
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $statusarray;
                    $response['message'] = 'Status list Successfully';
                    return $response;
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'task-status-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function progressReport(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{

                $projectid = $request->project_id;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not found';
                    $response['status'] = 301;
                    return $response;
                }
                $subprojectid = $request->subproject_id ?: 0;

                $startdate = str_replace('/','-',$request->start_date);
                $startdate =  !empty($startdate) ? date('Y-m-d',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $enddate = !empty($enddate) ? date('Y-m-d',strtotime($enddate)) : '';

                $startdate = date('Y-m-d 00:00:01',strtotime($startdate));
                $enddate = date('Y-m-d 23:59:59',strtotime($enddate));
                $segmentid = $request->segment_id ?: 0;
                $reponselist = array();
                if($projectid&&$startdate&&$enddate){
                    $companyinfo = Company::where('id',$projectdetails->company_id)->first();
                    $reponselist['companyname'] = ucwords($companyinfo->company_name);
                    $reponselist['companylogo'] = !empty($companyinfo->logo) ? uploads_url().'app-logo/'.$companyinfo->logo : '';
                    $reponselist['projectname'] = $projectdetails->project_name;
                    $reponselist['startdate'] = date('d M Y',strtotime($startdate));
                    $reponselist['enddate'] = date('d M Y',strtotime($enddate));

                    $contractorarray = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->pluck('user_id')->toArray();
                    $contr = array('0');
                    $contractorarray = array_merge($contractorarray,$contr);
                    $reportarray = array();
                    foreach($contractorarray as $contractor){
                     $progreearray = array();
                     $progreearray['company_id'] = $companyinfo->id;
                     $progreearray['projectid'] = $projectid;
                     $progreearray['subprojectid'] = $subprojectid;
                     $progreearray['segmentid'] = $segmentid;
                     $progreearray['level'] = '0';
                     $progreearray['parent'] = '0';
                     $progreearray['contractor'] = 0;
                     $progreearray['startdate'] = $startdate;
                     $progreearray['enddate'] = $enddate;
                     $progreearray['contractor'] = $contractor;
                     $progreearray['exportype'] = 'mobile';
                    if(!empty($segmentid)){
                        $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
                    }else{
                        $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
                    }
                    if($subprojectid){
                        $proproget = $proproget->where('title',$subprojectid);
                    }
                    $proproget = $proproget->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();

                    $progreearray['categoryarray'] = $proproget;

                    $report = $this->progreesreportdata($progreearray);
                        $reportarray  = array_merge($reportarray,$report);
                    }
                    $reponselist['progressreport'] = $reportarray;
                    $taskdata = Task::select('status', DB::raw('count(*) as count'))->where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid);
                    $taskdata = $taskdata->groupBy('status')->pluck('count','status')->toArray();
                    $notstarted = !empty($taskdata['notstarted']) ? $taskdata['notstarted'] : 0;
                    $inprogress = !empty($taskdata['inprogress']) ? $taskdata['inprogress'] : 0;
                    $delayed = !empty($taskdata['delayed']) ? $taskdata['delayed'] : 0;
                    $completed = !empty($taskdata['completed']) ? $taskdata['completed'] : 0;
                    $inproblem = !empty($taskdata['inproblem']) ? $taskdata['inproblem'] : 0;
                    $statusarray = array();
                    $statusarray[] = array('id'=>1,'slug'=>'notstarted','name'=>'Not Started','image'=>asset('uploads/not-started.png'),'count'=>$notstarted);
                    $statusarray[] = array('id'=>2,'slug'=>'inprogress','name'=>'In Progress','image'=>asset('uploads/in-progress.png'),'count'=>$inprogress);
                    $statusarray[] = array('id'=>3,'slug'=>'inproblem','name'=>'In Problem','image'=>asset('uploads/in-problem.png'),'count'=>$inproblem);
                    $statusarray[] = array('id'=>4,'slug'=>'delayed','name'=>'Delayed','image'=>asset('uploads/delay.png'),'count'=>$delayed);
                    $statusarray[] = array('id'=>5,'slug'=>'completed','name'=>'Completed','image'=>asset('uploads/completed.png'),'count'=>$completed);

                    $reponselist['statuslist'] = $statusarray;

                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $reponselist;
                    $response['message'] = 'Report list fetched Successfully';
                    return $response;
                }
                $response['status'] = 301;
                $response['message'] = 'Project Or date range not found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'progress-report',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        return response()->json(['error' => 'Unauthorised'], 401);
    }
    protected function progreesreportdata($reportarray){

        $projectid = $reportarray['projectid'];
        $subprojectid = $reportarray['subprojectid'] ?: 0;
        $segmentid = $reportarray['segmentid'];
        $level = $reportarray['level'];
        $parent = $reportarray['parent'];
        $contractor = $reportarray['contractor'];
        $start_date = $reportarray['startdate'];
        $end_date = $reportarray['enddate'];
        $proproget = $reportarray['categoryarray'];
        $companyid = $reportarray['company_id'];
         $progressreport = array();
        foreach ($proproget as $propro) {
            if(!empty($segmentid)){
                $projectcostitemsarray = \App\Task::join('project_segments_product','project_segments_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_segments_product.unit','project_segments_product.finalrate','project_segments_product.qty','project_segments_product.contractor','project_segments_product.description')
                    ->where('project_segments_product.position_id',$propro->id)->where('project_segments_product.project_id',$projectid)
                    ->where('project_segments_product.segment',$segmentid);
                if(!empty($subprojectid)||$subprojectid==0){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_segments_product.title',$subprojectid);
                }
            }else{
                $projectcostitemsarray = \App\Task::join('project_cost_items_product','project_cost_items_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_cost_items_product.unit','project_cost_items_product.finalrate','project_cost_items_product.qty','project_cost_items_product.contractor','project_cost_items_product.description')
                    ->where('project_cost_items_product.position_id',$propro->id)->where('project_cost_items_product.project_id',$projectid);
                if(!empty($subprojectid)||$subprojectid==0){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.title',$subprojectid);
                }
                if($contractor){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor',$contractor);
                }else{
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor','0');
                }
            }
            if(!empty($start_date)&&!empty($end_date)){
                $projectcostitemsarray = $projectcostitemsarray->where('task_percentage.created_at','>=',$start_date)->where('task_percentage.created_at','<=',$end_date);
            }
            $projectcostitemsarray = $projectcostitemsarray->where('task_percentage.created_at','>=',$start_date)->where('task_percentage.created_at','<=',$end_date);
            $projectcostitemsarray = $projectcostitemsarray->groupBy('task_percentage.task_id')->get();
           if(count($projectcostitemsarray)>0) {

                $catvalue = $propro->itemid;
                if (!empty($propro->catlevel)) {
                    $catvalue = $propro->catlevel . ',' . $propro->itemid;
                }
                $catarary = array();
                $explodeact = explode(',', $catvalue);
                $catarary['module'] = 'activity';
                $costarray['start_date'] = '';
                $costarray['due_date'] = '';
                $catarary['name'] = ucwords($propro->itemname);
                $catarary['level'] = $propro->level;
                $catarary['dotcount'] = count($explodeact);
                $contractorarray =  $tasklist = array();
                $contractorarray['contractorname'] = !empty($contractor) ? get_users_contractor_name($contractor,$companyid) : 'Departmental';

                foreach ($projectcostitemsarray as $projectcostitems) {
                    if ($projectcostitems->id) {
                        $totalquantity = !empty($projectcostitems->qty) ? $projectcostitems->qty : 0;
                        $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
                        $progress = $prevcumiliquantity = 0;
                         $prevcumiliquantity = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->sum('todayqty');
                          $cumiliquantitytill = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->orderBy('id','desc')->first();
                         $cumiliquantitypercenttill = !empty($cumiliquantitytill) ? $cumiliquantitytill->percentage : 0;
                         if(!empty($cumiliquantity)&&!empty($totalquantity)){
                            $cumiliquantitypercenttill =   ($cumiliquantity/$totalquantity)*100;
                        }
                        $progress = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->sum('todayqty');
                         $percentagetable = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->orderBy('id','desc')->first();
                          $percentage = !empty($percentagetable) ? $percentagetable->percentage : 0;
                        $percentage = $percentage-$cumiliquantitypercenttill;
                         if(!empty($progress)&&!empty($totalquantity)){
                            $percentage =  !empty($progress) ? ($progress/$totalquantity)*100 : 0;
                        }
                        $cumiliquantity = $prevcumiliquantity+$progress;
                        $cumiliquantitypercent = $cumiliquantitypercenttill+$percentage;
                        $rate = $projectcostitems->finalrate;
                        $cumiliquantitytillamt = $prevcumiliquantity*$rate;
                        $cumiliquantityamt = $cumiliquantity*$rate;
                        $progressamt = $progress*$rate;
                            $costarray = array();
                            $costarray['module'] = 'task';
                             if($projectcostitemsarray[0]->id==$projectcostitems->id){
                                 $costarray['contractorname'] = !empty($projectcostitems->contractor) ? get_users_contractor_name($projectcostitems->contractor,$projectcostitems->company_id) : 'Departmental';
                           }
                            $costarray['id'] =   ucwords($projectcostitems->id);
                            $costarray['name'] =   ucwords($projectcostitems->heading);
                            $costarray['unit'] =   get_unit_name($projectcostitems->unit);
                            $costarray['totalquanity'] =   $totalquantity;
                            $costarray['quantitytill'] =   $prevcumiliquantity ?: 0;
                            $costarray['quantitytillpercent'] =   !empty($cumiliquantitypercenttill) ? numberformat($cumiliquantitypercenttill).'%' : '0%';
                            $costarray['progress'] =   numberformat($progress);
                            $costarray['percentage'] =   numberformat($percentage).'%';
                            $costarray['cumiliquantity'] =  numberformat($cumiliquantity);
                            $costarray['cumilipercentage'] =   numberformat($cumiliquantitypercent).'%';
                            $costarray['rate'] =  numberformat($rate);
                            $costarray['quantitytillamt'] =  numberformat($cumiliquantitytillamt);
                            $costarray['progressamt'] =  numberformat($progressamt);
                            $costarray['quantityamt'] = numberformat( $cumiliquantityamt);
                            $tasklist[] = $costarray;
                    }
                }
               $contractorarray['tasklist'] =  $tasklist;
                $catarary['contractorarray'][] =  $contractorarray;
                $progressreport[] =  $catarary;
            }
            $progreearray = array();
            $newlevel = (int)$propro->level+1;
            $parent = (int)$propro->id;
            $progreearray['company_id'] = $companyid;
            $progreearray['projectid'] = $projectid;
            $progreearray['subprojectid'] = $subprojectid;
            $progreearray['segmentid'] = $segmentid;
            $progreearray['level'] = $newlevel;
            $progreearray['parent'] = $parent;
            $progreearray['contractor'] = $contractor ?: 0;
            $progreearray['startdate'] = $start_date;
            $progreearray['enddate'] = $end_date;
            if(!empty($segmentid)){
                $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
            }else{
                $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
            }
            if($subprojectid){
                $proproget = $proproget->where('title',$subprojectid);
            }
            $proproget = $proproget->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

            $progreearray['categoryarray'] = $proproget;
            $report = $this->progreesreportdata($progreearray);
            $progressreport  = array_merge($progressreport,$report);
        }
        return $progressreport;
    }

    public function progressReportExport(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid =   $request->project_id;
                $subprojectid =   $request->subproject_id ?: 0;
                $segmentid =   $request->segment_id ?: 0;
                $projectdetails = Project::find($request->project_id);
                /* Task array */
                $tasksarray = Task::where('project_id',$projectid)->pluck('id')->toArray();

                $dataarray = array();
                $dataarray['company'] = Company::find($user->company_id);
                $projectlist = explode(',',$user->projectlist);
                $dataarray['projectarray'] = Project::whereIn('id',$projectlist)->get();
                $dataarray['contractorsarray'] = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->pluck('name','user_id')->toArray();

                $exporttype = $request->exporttype;
                $dataarray['projectid'] = $projectdetails->id;
                $dataarray['projectdetails'] = $projectdetails;
                $dataarray['subprojectid'] = $request->subproject_id;
                $dataarray['segmentid'] = $request->segment_id;

                $startdate = str_replace('/','-',$request->start_date);
                $startdate =  !empty($startdate) ? date('Y-m-d',strtotime($startdate)) : '';
                $dataarray['startdate'] =  $startdate;
                $start_date = $startdate.' 00:00:01';

                $enddate = str_replace('/','-',$request->end_date);
                $enddate = !empty($enddate) ? date('Y-m-d',strtotime($enddate)) : '';
                $dataarray['enddate'] = $enddate;
                $end_date = $enddate.' 23:59:59';

                $dataarray['dateformat'] = $this->global->date_format;

                $tasksimagesarray = TaskFile::whereIn('task_id',$tasksarray)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->get();
                $dataarray['tasksimagesarray'] = $tasksimagesarray;

                $dataarray['contractorarray'] = Employee::where('company_id',$projectdetails->company_id)->where('user_type','contractor')->pluck('name','user_id')->toArray();

                $dataarray['curmonth'] = date('m');
                $dataarray['curyear'] = date('Y');
                $taskdata = Task::select('status', DB::raw('count(*) as count'))->where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid);
                $taskdata = $taskdata->groupBy('status')->pluck('count','status')->toArray();
                $dataarray['notstarted']  = !empty($taskdata['notstarted']) ? $taskdata['notstarted'] : 0;
                $dataarray['inprogress'] = !empty($taskdata['inprogress']) ? $taskdata['inprogress'] : 0;
                $dataarray['delayed']  = !empty($taskdata['delayed']) ? $taskdata['delayed'] : 0;
                $dataarray['completed'] = !empty($taskdata['completed']) ? $taskdata['completed'] : 0;
                $dataarray['inproblem'] = !empty($taskdata['inproblem']) ? $taskdata['inproblem'] : 0;

                $dataarray['issueslist'] = PunchItem::where('projectid',$projectid)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->get();
                $dataarray['indentslist'] = IndentProducts::join('indents','indents.id','=','indent_products.indent_id')
                                            ->select('indent_products.*','indents.indent_no')->where('indents.project_id',$projectid)->where('indents.created_at','>=',$start_date)
                                            ->where('indents.created_at','<=',$end_date)->get();

                $dataarray['exportype'] = 'mobile';
                $reporttype = array_filter(explode(',',$request->reporttype));
                $reporttext = array();
                foreach($reporttype as $reportt){
                    switch ($reportt){
                        case 'progressreport':
                        $reporttext[] = 'Progress Report';
                        break;
                        case 'photos':
                        $reporttext[] = 'Photos';
                        break;
                        case 'issues':
                        $reporttext[] = 'Issues';
                        break;
                        case 'indent':
                        $reporttext[] = 'Indent';
                        break;
                        case 'labourattendance':
                            $reporttext[] = 'Labour Attendance';
                            break;
                    }
                }
                $dataarray['reporttype'] = $reporttype;
                $dataarray['reporttext'] = $reporttext;
                $dataarray['user'] = $user;
                $filename = '';
                if ($exporttype == 'pdf') {
                    $pdf = PDF::loadView('admin.projects.reports.progress-reportpdf', $dataarray);
                    $filename = uniqid();
                    $filename .= '.pdf';
                    if (!file_exists('pdflogfiles/')) {
                        mkdir('pdflogfiles/', 0755);
                    }
                    $pdf->save('pdflogfiles/'.$filename);
                }
            if ($exporttype == 'excell') {
                $filename = uniqid();
                $report = View::make('admin.projects.reports.progress-reportexcell', $dataarray)->render();
                $exportArray = json_decode($report);
                Excel::create($filename, function ($excel) use ($user, $exportArray) {
                    $excel->setTitle('Progress report');
                    $excel->setCreator('Worksuite')->setCompany($user->company_id);
                    $excel->setDescription('Progress report');
                    $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                        $sheet->fromArray($exportArray, null, 'A1', false, false);
                        $sheet->row(1, function ($row) {
                            $row->setFont(array(
                                'bold' => true
                            ));
                        });
                    });
                })->store('xlsx', 'pdflogfiles/');
                $filename .= '.xlsx';
            }

            $response = array();
            $response['status'] = 200;
            $response['responselist'] = url('pdflogfiles/' . $filename);
            $response['message'] = 'Status list Successfully';
            return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'progress-report-export',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function taskAttachments(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $startdate = str_replace('/','-',$request->start_date);
                $startdate =  !empty($startdate) ? date('Y-m-d',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $enddate = !empty($enddate) ? date('Y-m-d',strtotime($enddate)) : '';

                $response = array();
                $tasksarray = Task::where('project_id', $projectid)->pluck('id')->toArray();
                $tasksfilesarray = TaskFile::whereIn('task_id', $tasksarray)->where('created_at','>=',$startdate.' 00:00:01')->where('created_at','<=',$enddate.' 23:59:59')->get();
                $imagesarray = array();
                $storage = storage();
                $awsurl = awsurl();
                $url = uploads_url();
                foreach ($tasksfilesarray as $tasksfiles){
                    $imagename = $tasksfiles->hashname;
                    $fx = explode('.', $imagename);
                    $ext = $fx[(count($fx)-1)];
                    if($ext=='jpg'||$ext=='jpeg'||$ext=='png'){
                        $image = array();
                        $image['id'] = $tasksfiles->id;
                        $image['task_id'] = $tasksfiles->task_id;
                        $image['ext'] = $ext;
                        $image['imagename'] = $tasksfiles->filename;
                        if($imagename){
                            $id = $tasksfiles->task_id;
                            if($storage){
                                switch($storage){
                                    case 'local':
                                        $image['imageurl'] = $url.'task-files/'.$id.'/'.$imagename;
                                        break;
                                    case 's3':
                                        $image['imageurl'] =  $awsurl.'/task-files/'.$id.'/'.$imagename;
                                        break;
                                    case 'google':
                                        $image['imageurl'] =  $tasksfiles->google_url;
                                        break;
                                    case 'dropbox':
                                        $image['imageurl'] =  $tasksfiles->dropbox_link;
                                        break;
                                }
                            }
                        }
                        $image['created_at'] = Carbon::parse($tasksfiles->created_at)->format('d-m-Y');
                        $imagesarray[] = $image;
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Images array';
                $response['response'] = $imagesarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'task-attachments',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeTaskAttachments(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $taskid =  $request->task_id;
                $taskdetails = Task::findOrfail($taskid);
                if(empty($taskdetails)){
                    $response['status'] = 300;
                    $response['message'] = 'Task Not Found';
                    return $response;
                }
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $filearray = !empty($request->file_id) ? explode(',',$request->file_id) : array();
                if(count($filearray)>0){
                    foreach ($filearray as $item) {
                        $taskfile = TaskFile::where('task_id',$taskdetails->id)->where('filemanager_id',$item)->first();
                        if(empty($taskfile)){
                            $filemanager = FileManager::where('id',$item)->first();
                            if(!empty($filemanager)){
                                switch ($storage){
                                        case 'local':
                                            $folder = '';
                                            $profolder = $filemanager->project_id;
                                            $x=1;
                                            $p = $filemanager->parent;
                                            while($x==1){
                                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                                if($fn !== null){
                                                    $folder = $fn->id.'/'.$folder;
                                                    $p = $fn->parent;
                                                }else{
                                                    $x=0;
                                                }
                                            }
                                            $sourcepath =  'uploads/project-files/'.$profolder.'/'.$folder.$filemanager->hashname;
                                            $desinationpath =  'uploads/task-files/'.$taskdetails->id.'/';
                                            if(!file_exists($desinationpath)){
                                                mkdir( $desinationpath, 0777, true);
                                            }
                                            $desinationpathlink =  $desinationpath.$filemanager->hashname;
                                            if(file_exists($sourcepath)){
                                                copy($sourcepath,$desinationpathlink);
                                            }
                                        break;
                                }
                                $file = new TaskFile();
                                $file->user_id = $user->id;
                                $file->company_id = $taskdetails->company_id;
                                $file->task_id = $taskdetails->id ?: 0;
                                $file->task_percentage_id = 0;
                                $file->filename = $filemanager->name;
                                $file->hashname = $filemanager->hashname;
                                $file->filemanager_id = $filemanager->id;
                                $file->size = $filemanager->size;
                                $file->description = $filemanager->description;
                                $file->google_url = $filemanager->google_url;
                                $file->dropbox_link = $filemanager->dropbox_link;
                                $file->external_link_name = $filemanager->external_link_name;
                                $file->external_link = $filemanager->external_link;
                                $file->save();
                            }
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Attachment Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-task-attachments',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteTaskAttachments(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->task_id;
                $imagesarray = !empty($request->imagesid) ? explode(',',$request->imagesid) : array();
                $task = Task::findOrFail($id);
                if(empty($task)){
                    $response['message'] = 'Task Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                TaskFile::where('task_id', $task->id)->where('task_percentage_id', '0')->whereIn('id',$imagesarray)->delete();
                $response['status'] = 200;
                $response['message'] = 'Task Attachments deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium;
                app_log($e,'delete-task-attachment',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}