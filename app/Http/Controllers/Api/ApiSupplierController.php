<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\Company;
use App\Employee;
use App\EmployeeDetails;
use App\Http\Controllers\Controller;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\Traits\EmployeeRegister;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiSupplierController extends Controller
{
    use EmployeeRegister;

    // Validate API_KEY
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    // Validate Token for validating user login
    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 300;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 300;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 300;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request) {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            // checking permission if user has access or not
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }

        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            if(!empty($request->project_id)){
                $projectdetails = AppProject::where('id',$request->project_id)->first();
                $usercompany = $projectdetails->company_id;
            }else{
                $usercompany = $user->company_id;
            }
            try{
                $response = $suplData = array();
                $supplier  = Employee::where('user_type','supplier')->where('company_id',$usercompany)->orderBy('name')->get();
                foreach ($supplier as $us) {
                    $company_name = '';
                    $company = EmployeeDetails::where('employee_id',$us->id)->first();
                    if(!empty($company->company_name)){
                        $company_name =  ucfirst($company->company_name);
                    }
                    $suplData[] = array(
                        'supplier_id' => $us->id,
                        'company_name' => $company_name,
                        'supplier_name' => $us->name,
                        'mobile' => $us->mobile,
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Supplier List';
                $response['response'] = $suplData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'supplier-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                if(!empty($request->project_id)){
                    $projectdetails = AppProject::where('id',$request->project_id)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
                $name = $request->supplier_name;
                $email = $request->email;
                $mobile = $request->mobile;
                $user_type = 'supplier';
                $password = trim($request->password);
                $gender = $request->gender;
                $address = $request->address;
                $fcm = $request->fcm;
                $dataarray = array();
                $dataarray['user'] = $user;
                $dataarray['company_id'] = $usercompany;
                $dataarray['name'] = $name;
                $dataarray['email'] = $email ? : '';
                $dataarray['mobile'] = $mobile ? : '';
                $dataarray['user_type'] = $user_type;
                $dataarray['password'] = $request->password ? : '';
                $dataarray['gender'] = $request->gender ? : '';
                $dataarray['gender'] = $request->gender ? : '';
                $dataarray['address'] = $request->address ? : '';
                $dataarray['fcm'] = $request->fcm ? : '';
                $dataarray['projectid'] = !empty($projectdetails) ? $projectdetails->id : 0;
                $dataarray['sharetoproject'] = 0;
                $payload =  $this->createemployee($dataarray);

                $employee = $payload['response'];
                $empid = $employee['id'] ?: 0;
                $userid = $employee['user_id'] ?: 0;

                $employeedetail = new EmployeeDetails();
                $employeedetail->company_id = $usercompany;
                $employeedetail->user_id = $userid;
                $employeedetail->employee_id = $empid;
                $employeedetail->address = $request->address;
                $employeedetail->company_name = !empty($request->company_name) ? $request->company_name : '';
                $employeedetail->save();

                $response['status'] = 200;
                $response['message'] = 'Supplier Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $response = array();
                if (!empty($request->project_id)) {
                    $projectdetails = AppProject::where('id', $request->project_id)->first();
                    $usercompany = $projectdetails->company_id;
                } else {
                    $usercompany = $user->company_id;
                }

                $employee = Employee::findOrFail($request->supplier_id);
                $employee->name = $request->supplier_name;
                $employee->mobile = $request->mobile;
                $employee->save();
                if (!empty($request->company_name)) {
                    $employeedetails = EmployeeDetails::where('employee_id', '=', $employee->id)->first();
                    $employeedetails->company_name =  $request->company_name;
                    $employeedetails->save();
                }

                $response['status'] = 200;
                $response['message'] = 'Supplier Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                EmployeeDetails::where('employee_id',$request->supplier_id)->delete();
                Employee::where('id',$request->supplier_id)->where('user_type','supplier')->delete();
                $response['status'] = 200;
                $response['message'] = 'Supplier Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;

    }
}
