<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Http\Controllers\Controller;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectSegmentsPosition;
use App\ProjectsLogs;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use DB;

class AppWorkController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }
    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function appWorks(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $companyid = $user->company_id;
                $projects = BoqCategory::where('company_id', $companyid)->orderBy('title','asc')->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Activity List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createWork(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::where('id',$projectid)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;

                $catlevel = $request->catlevel ?: '';
                $parent = $request->parent ?: 0;

                $parentitemid =  0;
                $level = (int)$request->level ?: 0;

                $title =  $request->title;
                $work = BoqCategory::where('company_id',$usercompany)->where('title',$title)->first();
                if(empty($work)){
                    $work = new BoqCategory();
                    $work->company_id = $usercompany;
                }
                if(!empty($segmentid)){
                    $maxpositionarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                    $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                    $columsarray = new ProjectSegmentsPosition();
                    $columsarray->segment = $segmentid ?: 0;
                    $columsarray->inc = $newid;
                    $columsarray->level = $level ?: 0;
                }else{
                    $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                    $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                    $columsarray = new ProjectCostItemsPosition();
                    $columsarray->inc = $newid;
                    $columsarray->level = $level ?: 0;
                }
                $work->title = $title;
                $work->parent = $parentitemid ?: 0;
                $work->save();


                $columsarray->project_id = $projectid;
                $columsarray->title = $subprojectid;
                $columsarray->position = 'row';
                $columsarray->itemid = $work->id;
                $columsarray->itemname = $work->title;
                $columsarray->itemslug = filter_string($work->title);
                $columsarray->collock = 0;
                $columsarray->parent = $parent;
                $columsarray->catlevel = $catlevel;
                $columsarray->save();


                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $usercompany;
                $createlog->added_id = $user->id;
                $createlog->module_id = $columsarray->id;
                $createlog->module = 'boq_categories';
                $createlog->project_id = $projectid;
                $createlog->subproject_id = $subprojectid;
                $createlog->segment_id = $segmentid;
                $createlog->heading =  $work->title;
                $createlog->modulename = 'activity_created';
                $createlog->description = $work->title . ' activity created by ' . $user->name . ' for the project ' . get_project_name($projectid);
                $createlog->medium = $medium;
                $createlog->save();

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Activity';
                    $notifmessage['body'] = 'New Activity is created at '.get_project_name($projectid).' project by '.$user->name;
                    $notifmessage['activity'] = 'activity';
                    sendFcmNotification($project_members->id, $notifmessage);
                }

                $response['status'] = 200;
                $response['activity_id'] = $work->id;
                $response['position_id'] = $columsarray->id;
                $response['message'] = 'Activity Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function editWork(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->workid;
                $projectid = $request->project_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::where('id',$projectid)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                $title =  $request->title;

                $work = BoqCategory::where('company_id',$usercompany)->where('id',$workid)->first();
                $work->title = $title;
                $work->save();

                if(!empty($segmentid)){
                    $columsarray = \App\ProjectSegmentsPosition::where('id',$id)->first();
                }else{
                    $columsarray = \App\ProjectCostItemsPosition::where('id',$id)->first();
                }
                $columsarray->itemid = $work->id;
                $columsarray->itemname = $work->title;
                $columsarray->itemslug = filter_string($work->title);
                $columsarray->save();

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $usercompany;
                $createlog->added_id = $user->id;
                $createlog->module_id = $columsarray->id;
                $createlog->module = 'boq_categories';
                $createlog->project_id = $projectid;
                $createlog->subproject_id = $subprojectid;
                $createlog->segment_id = $segmentid;
                $createlog->heading =  $work->title;
                $createlog->modulename = 'activity_updated';
                $createlog->description = $work->title . ' activity updated by ' . $user->name . ' for the project ' . get_project_name($projectid);
                $createlog->medium = $medium;
                $createlog->save();


                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Activity';
                    $notifmessage['body'] = 'Activity is Updated at '.get_project_name($projectid).' project by '.$user->name;
                    $notifmessage['activity'] = 'activity';
                    sendFcmNotification($project_members->id, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Activity Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function selectActivity(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->activity_id;
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                if(empty($workid)){
                    $response['message'] = 'Please select Activity';
                    $response['status'] = 301;
                    return $response;
                }
                if($request->level==''){
                    $level = 0;
                }elseif($request->level=='0'){
                    $level = 1;
                }elseif($request->level>'0'){
                    $level = (int)$request->level+1;
                }
                $work = BoqCategory::find($workid);
                $catlevel =  '';
                if(!empty($request->catlevel)){
                    $catlevel = $request->catlevel;
                }

                if(!empty($segmentid)){
                    $maxpositionarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                    $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                    $columsarray = new ProjectSegmentsPosition();
                    $columsarray->segment = $segmentid ?: 0;
                    $columsarray->inc = $newid;
                    $columsarray->level = $level;
                }else{
                    $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                    $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                    $columsarray = new ProjectCostItemsPosition();
                    $columsarray->inc = $newid;
                    $columsarray->level = $level ?: 0;
                }
                $columsarray->project_id = $projectid;
                $columsarray->title = $subprojectid;

                $columsarray->position = 'row';
                $columsarray->itemid = $work->id;
                $columsarray->itemname = $work->title;
                $columsarray->itemslug = filter_string($work->title);
                $columsarray->collock = 0;
                $columsarray->parent = $request->parent ?: 0;
                $columsarray->catlevel = $catlevel;
                $columsarray->save();

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $columsarray->id;
                $createlog->module = 'boq_categories';
                $createlog->project_id = $projectid;
                $createlog->subproject_id = $subprojectid;
                $createlog->segment_id = $segmentid;
                $createlog->heading =  $work->title;
                if(empty($workid)) {
                    $createlog->modulename = 'activity_created';
                    $createlog->description = $work->title . ' activity created by ' . $user->name . ' for the project ' . get_project_name($projectid);
                }else{
                    $createlog->modulename = 'activity_updated';
                    $createlog->description = $work->title . ' activity updated by ' . $user->name . ' for the project ' . get_project_name($projectid);
                }
                $createlog->medium = $medium;
                $createlog->save();

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Activity';
                    $notifmessage['body'] = 'New Activity is created at '.get_project_name($projectid).' project by '.$user->name;
                    $notifmessage['activity'] = 'activity';
                    sendFcmNotification($project_members->id, $notifmessage);
                }

                $response['status'] = 200;
                $response['activity_id'] = $work->id;
                $response['position_id'] = $columsarray->id;
                $response['message'] = 'Activity Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteWork(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->workid;
                $task = BoqCategory::findOrFail($workid);
                $task->delete();

                $response['status'] = 200;
                $response['message'] = 'Activity removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteActivity(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->activityid;
                 $positiondetails  =  ProjectCostItemsPosition::where('id', $id)->first();
                $manpower = ProjectCostItemsPosition::where('project_id',$positiondetails->project_id)->where('title',$positiondetails->title)->where('parent',$positiondetails->id)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please remove sub activity';
                    $response['status'] = 300;
                    return $response;
                }
                $costproductsarray = ProjectCostItemsProduct::where('project_id',$positiondetails->project_id)->where('title',$positiondetails->title)->where('position_id', $id)->pluck('id')->toArray();
                if(!empty($costproductsarray)){
                    $tasksarray = Task::whereIn('cost_item_id',$costproductsarray)->first();
                    if(!empty($tasksarray->id)){
                        $response['message'] = 'Please remove task';
                        $response['status'] = 300;
                        return $response;
                    }
                }
                ProjectCostItemsPosition::where('id', $id)->delete();
                ProjectCostItemsProduct::where('position_id', $id)->delete();
                ProjectsLogs::where('module_id',$id)->where('module','boq_categories')->delete();

                $response['status'] = 200;
                $response['message'] = 'Activity removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appCostitems(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = CostItems::get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Works List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-costitems',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}