<?php

namespace App\Http\Controllers\Api;

use App\AppProductIssue;
use App\AppProductIssueReply;
use App\AppProductLog;
use App\AppStock;
use App\AppStore;
use App\Company;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductIssueFiles;
use App\ProductIssueReturn;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiProductIssue extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        $response = array();
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $products = json_decode(trim($request->product));
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }

                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }
                $productissueuni = AppProductIssue::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
                $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                $unique_id = 'MI-'.date("d-m-Y").'-'.$newid;
                $response = array();
                if(count($products)>0) {
                    foreach ($products as $product) {
                        if ($product->qty !== '') {
                            $scheck = AppStock::where('cid', $product->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                             if (empty($scheck)) {
                                $response['status'] = 300;
                                $response['message'] = get_local_product_name($product->product_id).' Material Stock Not Available';
                                return $response;
                            }
                          if ($scheck->stock < $product->qty) {
                               $response['status'] = 300;
                               $response['message'] = 'Available '.get_local_product_name($product->product_id).' Material stock is ' . $scheck->stock;
                               return $response;
                           }
                           if ($product->qty > $scheck->stock) {
                               $response['status'] = 200;
                               $response['message'] = 'Issued Quantity greater then available stock';
                               return $response;
                           }

                        }
                    }
                    foreach ($products as $product) {
                        if ($product->qty !== '') {
                            $scheck = AppStock::where('cid', $product->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                            $bal = 0;
                            if (empty($scheck)) {
                                $response['status'] = 300;
                                $response['message'] = 'Material Stock Not Available';
                                return $response;
                            }
                            if ($scheck->stock < $product->qty) {
                                $response['status'] = 300;
                                $response['message'] = 'Available Material stock is ' . $scheck->stock;
                                return $response;
                            }
                            if ($product->qty > $scheck->stock) {
                                $response['status'] = 200;
                                $response['message'] = 'Issued Quantity greater then available stock';
                                return $response;
                            } else {
                                $bal = $scheck->stock - $product->qty;
                                $scheck->stock = $bal;
                                $scheck->save();
                            }
                            $pro = new AppProductIssue();
                            $pro->unique_id = $unique_id;
                            $pro->project_id = $projectId->id;
                            $pro->company_id = $projectId->company_id;
                            $pro->store_id = $storeId->id;
                            $pro->product_id = $product->product_id;
                            $pro->quantity = $product->qty;
                            $pro->unit_id = $product->unit_id;
                            $pro->date = Carbon::parse($product->date)->format('Y-m-d');
                            $pro->remark = $product->remark;
                            $pro->issued_by = $user->id;
                            $pro->inc = $newid;
                            $pro->save();
                            if(!empty($product->images)){
                                $productimagsarry = array_filter(array_unique(explode(',',$product->images)));
                                foreach($productimagsarry as $productimags){
                                    $productimage =  ProductIssueFiles::find($productimags);
                                    if(!empty($productimage)){
                                        $productimage->issue_id =  $pro->id;
                                        $productimage->save();
                                        $sourcepath = 'uploads/product-issue-files/0/'.$productimage->hashname;
                                        $destinationPath = 'uploads/product-issue-files/'.$pro->id.'/'.$productimage->hashname;
                                        if(file_exists($sourcepath)){
                                            if (!file_exists('uploads/product-issue-files/'.$pro->id)) {
                                                mkdir('uploads/product-issue-files/'.$pro->id, 0777, true);
                                            }
                                            rename($sourcepath,$destinationPath);
                                        }
                                    }
                                }
                            }

                            if ($pro) {
                                $pl = new AppProductLog();
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $request->project_id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pro->id;
                                $pl->module_name = 'product_issue';
                                $pl->product_id = $product->product_id;
                                $pl->quantity = $product->qty;
                                $pl->balance_quantity = $bal;
                                $pl->transaction_type = 'minus';
                                $pl->remark = $request->remark;
                                if(!empty($pro->date)){
                                    $pl->date =  Carbon::parse($pro->date)->format('Y-m-d');
                                }
                                $pl->save();
                            }
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Material issued Successfully';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Materials Not Found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'add-product-issue',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $products = json_decode(trim($request->product));
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }

                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }
                $unique_id = 'MI-'.date("d-m-Y").'-'.rand(1111,9999);
                $response = array();
                if(count($products)>0) {
                    foreach ($products as $product) {
                        if (!empty($product->qty)) {
                            $scheck = AppStock::where('cid', $product->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                            $bal = 0;
                            if (empty($scheck)) {
                                $response['status'] = 300;
                                $response['message'] = 'Material Stock Not Available';
                                return $response;
                            }
                            $totalstock = $scheck->stock;
                            if(!empty($product->id)){
                                $pro =  AppProductIssue::find($product->id);
                                $stockqty = $pro->quantity;
                                $totalstock +=$stockqty;
                            }
                            if ($totalstock < $product->qty) {
                                $response['status'] = 300;
                                $response['message'] = 'Available Material stock is ' . $totalstock;
                                return $response;
                            }
                            if ($product->qty > $totalstock) {
                                $response['status'] = 300;
                                $response['message'] = 'Issued Quantity greater then available stock';
                                return $response;
                            } else {
                                $bal = $totalstock - $product->qty;
                                $scheck->stock = $bal;
                                $scheck->save();
                            }

                            $productissuereturn = ProductIssueReturn::where('issue_id',$product->id)->where('product_id',$product->product_id)->sum('qty');
                            if ((float)$product->qty < (float)$productissuereturn) {
                                $response['status'] = 200;
                                $response['message'] = 'Update quantity is less than issue returned quantity';
                                return $response;
                            }
                            if(!empty($product->id)){
                                $pro =  AppProductIssue::find($product->id);
                            }else{
                                $pro = new AppProductIssue();
                                $pro->unique_id = $unique_id;
                            }
                            $pro->project_id = $request->project_id;
                            $pro->store_id = $storeId->id;
                            $pro->product_id = $product->product_id;
                            $pro->quantity = $product->qty;
                            $pro->unit_id = $product->unit_id;
                            $pro->date = Carbon::parse($product->date)->format('Y-m-d');
                            $pro->remark = $product->remark;
                            $pro->issued_by = $user->id;
                            $pro->save();

                            if(!empty($product->images)){
                                $productimagsarry = array_filter(array_unique(explode(',',$product->images)));
                                foreach($productimagsarry as $productimags){
                                    $productimage =  ProductIssueFiles::find($productimags);
                                    if(!empty($productimage)){
                                        $productimage->issue_id =  $pro->id;
                                        $productimage->save();
                                        $sourcepath = 'uploads/product-issue-files/0/'.$productimage->hashname;
                                        $destinationPath = 'uploads/product-issue-files/'.$pro->id.'/'.$productimage->hashname;
                                        if(file_exists($sourcepath)){
                                            if (!file_exists('uploads/product-issue-files/'.$pro->id)) {
                                                mkdir('uploads/product-issue-files/'.$pro->id, 0777, true);
                                            }
                                            rename($sourcepath,$destinationPath);
                                        }
                                    }
                                }
                            }

                            if (!empty($pro)) {
                                $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                                    ->where('module_id', $pro->id)
                                    ->where('product_id', $product->product_id)
                                    ->where('module_name','product_issue')
                                    ->orderBy('id', 'DESC')->first();
                                if(empty($pl)){
                                    $pl = new AppProductLog();
                                }
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $request->project_id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pro->id;
                                $pl->module_name = 'product_issue';
                                $pl->product_id = $product->product_id;
                                $pl->quantity = $product->qty;
                                $pl->balance_quantity = $bal;
                                $pl->transaction_type = 'minus';
                                $pl->remark = $request->remark;
                                if(!empty($pro->date)){
                                    $pl->date =  Carbon::parse($pro->date)->format('Y-m-d');
                                }
                                $pl->save();
                            }
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Material issue Updated Successfully';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Materials Not Found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'add-product-issue',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function editSingle(Request $request)
    {
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }

                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }
                $issueid = $request->issue_id;
                $pro = AppProductIssue::where('id',$issueid)->orderBy('id','DESC')->first();
                if(empty($pro)){
                    $response['status'] = 301;
                    $response['message'] = 'Issue Not found';
                    return $response;
                }
                $response = array();
                    if (!empty($request->quantity)) {
                        $scheck = AppStock::where('cid', $request->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                        $bal = 0;
                        if (empty($scheck)) {
                            $response['status'] = 300;
                            $response['message'] = 'Material Stock Not Available';
                            return $response;
                        }
                        $totalstock = $scheck->stock;
                        if(!empty($pro->id)){
                            $stockqty = $pro->quantity;
                            $totalstock +=$stockqty;
                        }
                        if ($totalstock < $request->quantity) {
                            $response['status'] = 300;
                            $response['message'] = 'Available Material stock is ' . $totalstock;
                            return $response;
                        }
                        if ($request->quantity > $totalstock) {
                            $response['status'] = 300;
                            $response['message'] = 'Issued Quantity greater then available stock';
                            return $response;
                        } else {
                            $bal = $totalstock - $request->quantity;
                            $scheck->stock = $bal;
                            $scheck->save();
                        }

                        $productissuereturn = ProductIssueReturn::where('issue_id',$pro->id)->where('product_id',$pro->product_id)->sum('qty');
                        if ((float)$request->quantity < (float)$productissuereturn) {
                            $response['status'] = 200;
                            $response['message'] = 'Update quantity is less than issue returned quantity';
                            return $response;
                        }

                        $pro->project_id = $request->project_id;
                        $pro->store_id = $storeId->id;
                        $pro->product_id = $request->product_id;
                        $pro->quantity = $request->quantity;
                        $pro->unit_id = $request->unit_id;
                        $pro->date = Carbon::parse($request->date)->format('Y-m-d');
                        $pro->remark = $request->remark;
                        $pro->issued_by = $user->id;
                        $pro->save();

                        if(!empty($request->images)){
                            $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                            foreach($productimagsarry as $productimags){
                                $productimage =  ProductIssueFiles::find($productimags);
                                if(!empty($productimage)){
                                    $productimage->issue_id =  $pro->id;
                                    $productimage->save();
                                    $sourcepath = 'uploads/product-issue-files/0/'.$productimage->hashname;
                                    $destinationPath = 'uploads/product-issue-files/'.$pro->id.'/'.$productimage->hashname;
                                    if(file_exists($sourcepath)){
                                        if (!file_exists('uploads/product-issue-files/'.$pro->id)) {
                                            mkdir('uploads/product-issue-files/'.$pro->id, 0777, true);
                                        }
                                        rename($sourcepath,$destinationPath);
                                    }
                                }
                            }
                        }

                        if (!empty($pro)) {
                            $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                                ->where('module_id', $pro->id)
                                ->where('product_id', $request->product_id)
                                ->where('module_name','product_issue')
                                ->orderBy('id', 'DESC')->first();
                            if(empty($pl)){
                                $pl = new AppProductLog();
                            }
                            $pl->store_id = $storeId->id;
                            $pl->project_id = $request->project_id;
                            $pl->company_id = $projectId->company_id;
                            $pl->created_by = $user->id;
                            $pl->module_id = $pro->id;
                            $pl->module_name = 'product_issue';
                            $pl->product_id = $request->product_id;
                            $pl->quantity = $request->quantity;
                            $pl->balance_quantity = $bal;
                            $pl->transaction_type = 'minus';
                            $pl->remark = $request->remark;
                            if(!empty($pro->date)){
                                $pl->date =  Carbon::parse($pro->date)->format('Y-m-d');
                            }
                            $pl->save();
                        }
                    $response['status'] = 200;
                    $response['message'] = 'Material issue Updated Successfully';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Materials Not Found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'edit-product-issue-single',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function issueList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $permission_project_id = $request->permission_project_id;
                $permission_name = $request->permission_name;
                $medium = $request->medium ?: 'api';
                $responsearray = array();

                $issueslist = AppProductIssue::join('stores','product_issue.project_id','=','stores.project_id')->select('product_issue.*')->where('product_issue.project_id',$projectid);
                $startdate = str_replace('/','-',$request->start_date);
                $fromdate =  !empty($startdate) ? date('Y-m-d 00:00:01',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $todate = !empty($enddate) ? date('Y-m-d 23:59:59',strtotime($enddate)) : '';
                if(!empty($fromdate)){
                    $issueslist = $issueslist->where('product_issue.created_at','>=',$fromdate);
                }
                if(!empty($todate)){
                    $issueslist = $issueslist->where('product_issue.created_at','<=',$todate);
                }
                $issueslist = $issueslist->groupBy('product_issue.unique_id');
                $page = $request->page;
                if($page=='all'){
                    $issueslist = $issueslist->orderBy('product_issue.id','desc')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $issueslist = $issueslist->offset($skip)->take($count)->orderBy('product_issue.id','desc')->get();
                }
                $issueslistarray = array();
                foreach ($issueslist as $issues){
                    $productsname = Product::join('product_issue','product_issue.product_id','=','products.id')->where('product_issue.unique_id',$issues->unique_id)->pluck('products.name')->toArray();
                    $issuesarray = array();
                    $issuesarray['unique_id'] = $issues->unique_id;
                    $issuesarray['project_id'] = $issues->project_id;
                    $issuesarray['project_name'] = get_project_name($issues->project_id);
                    $issuesarray['product_name'] = !empty($productsname) ? implode(', ',$productsname) : '';
                    $issuesarray['issued_by'] = $issues->issued_by;
                    $issuesarray['issued_name'] = get_user_name($issues->issued_by);
                    $issuesarray['issued_date'] = date('d M Y',strtotime($issues->date));
                    $issuesarray['created_date'] = date('d M Y',strtotime($issues->created_at));
                    $issueslistarray[] = $issuesarray;
                }

                $response['status'] = 200;
                $response['message'] = 'Issues Report';
                $response['responselist'] = $issueslistarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'list-issues',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function issueProductsList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $medium = $request->medium ?: 'api';
                $unique_id = $request->unique_id ?: '';
                $issue_id = $request->issue_id ?: '';
                $responsearray = array();
                $issueslist = AppProductIssue::join('stores','product_issue.project_id','=','stores.project_id')->select('product_issue.*')->where('product_issue.project_id',$projectid);
                $startdate = str_replace('/','-',$request->start_date);
                $fromdate =  !empty($startdate) ? date('Y-m-d 00:00:01',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $todate = !empty($enddate) ? date('Y-m-d 23:59:59',strtotime($enddate)) : '';
                if(!empty($unique_id)){
                    $issueslist = $issueslist->where('product_issue.unique_id',$unique_id);
                }
                if(!empty($issue_id)){
                    $issueslist = $issueslist->where('product_issue.id',$issue_id);
                }
                if(!empty($fromdate)){
                    $issueslist = $issueslist->where('product_issue.created_at','>=',$fromdate);
                }
                if(!empty($todate)){
                    $issueslist = $issueslist->where('product_issue.created_at','<=',$todate);
                }
                $issueslist = $issueslist->get();
                $issueslistarray = array();
                foreach ($issueslist as $issues){
                    $returnproducts = ProductIssueReturn::where('issue_id',$issues->id)->sum('qty');
                    $issuesarray = array();
                    $issuesarray['id'] = $issues->id;
                    $issuesarray['unique_id'] = $issues->unique_id;
                    $issuesarray['product_id'] = $issues->product_id;
                    $issuesarray['product_name'] = get_local_product_name($issues->product_id);
                    $issuesarray['unit_id'] =  $issues->unit_id;
                    $issuesarray['unit_name'] =  get_unit_name($issues->unit_id);
                    $issuesarray['quantity'] =  $issues->quantity;
                    $issuesarray['return_quantity'] =  !empty($returnproducts) ? $returnproducts : 0;
                    $issuesarray['issued_by'] = $issues->issued_by;
                    $issuesarray['issued_name'] = get_user_name($issues->issued_by);
                    $issuesarray['remark'] = $issues->remark;
                    $issuesarray['date'] = date('d M Y',strtotime($issues->date));
                    $issuesarray['created_date'] = date('d M Y',strtotime($issues->created_at));
                    $issuesarray['images'] = !empty($issues->images) ? $issues->images : array();
                    $issueslistarray[] = $issuesarray;
                }
                $response['status'] = 200;
                $response['message'] = 'Issues Report';
                $response['responselist'] = $issueslistarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'manpower-report',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $productissueid = $request->issue_id;
                $uniqueid = $request->unique_id;
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(!empty($uniqueid)){

                    $issuesarray = AppProductIssue::where('project_id',$projectId->id)->where('unique_id',$uniqueid)->pluck('id')->toArray();

                    $materialreceive = ProductIssueReturn::whereIn('issue_id',$issuesarray)->first();
                    if(!empty($materialreceive)){
                        $response['status'] = 301;
                        $response['message'] = 'Please remove material issue return';
                        return $response;
                    }
                    $issuesarray = AppProductIssue::where('project_id',$projectId->id)->where('unique_id',$uniqueid)->get();
                    if(empty($issuesarray)){
                        $response['status'] = 301;
                        $response['message'] = 'Issued product not found';
                        return $response;
                    }

                    if(count($issuesarray)>0){
                        foreach($issuesarray as $issues){
                            $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                                ->where('module_id', $issues->id)
                                ->where('product_id', $issues->product_id)
                                ->where('module_name','product_issue')->delete();
                            ProductIssueFiles::where('issue_id',$issues->id)->delete();
                            AppProductIssueReply::where('issue_id',$issues->id)->delete();
                            $issues->delete();
                        }
                    }
                }else{
                    $issues = AppProductIssue::find($productissueid);
                    if(empty($issues)){
                        $response['status'] = 301;
                        $response['message'] = 'Issued product not found';
                        return $response;
                    }
                    $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                        ->where('module_id', $issues->id)
                        ->where('product_id', $issues->product_id)
                        ->where('module_name','product_issue')->delete();

                    ProductIssueFiles::where('issue_id',$issues->id)->delete();
                    AppProductIssueReply::where('issue_id',$issues->id)->delete();
                    $issues->delete();

                }
                $response['status'] = 200;
                $response['message'] = 'Material Issue Deleted';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-product-issue',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $issueid = $request->issue_id ?: 0;
                $replyid = $request->reply_id ?: 0;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $projectId->company_id;
                    $fileData =  $request->file('file');
                    $file = new ProductIssueFiles();
                    $file->added_by = $user->id;
                    $file->company_id = $companyid;
                    $file->issue_id = $issueid ?: 0;
                    $file->reply_id = $replyid ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/product-issue-files/'.$issueid;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/product-issue-files/'.$issueid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'product-issue-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('product-issue-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $issueid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$issueid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $issueid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('product-issue-files/'.$issueid.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$issueid.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['image_url'] = get_issue_image_link($file->id,$issueid);
                    $response['message'] = 'Material Issue Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-issue-image',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteStoreImage(Request $request){
        $id = $request->image_id;
        $task = ProductIssueFiles::findOrFail($id);
        if(!empty($task)){
            $task->delete();
        }
        $response['status'] = 200;
        $response['message'] = 'Image deleted successfully';
        return $response;
    }

    public function issueItemReply(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                $productissueid = $request->issue_id;
                $productuniqueid = $request->unique_id;
                if(!empty($productissueid)){
                    $issues = AppProductIssue::find($productissueid);
                }else{
                    $issues = AppProductIssue::where('unique_id',$productuniqueid)->first();
                }
                if(empty($issues)){
                    $response['status'] = 301;
                    $response['message'] = 'Issued product not found';
                    return $response;
                }
                $issuereply = new AppProductIssueReply();
                $issuereply->company_id = $projectId->company_id;
                $issuereply->added_by = $user->id;
                $issuereply->unique_id = $issues->unique_id;
                $issuereply->issue_id = $issues->id;
                $issuereply->comment = $request->comment ?: '';
                $issuereply->save();

                $response['status'] = 200;
                $response['issue_id'] = $issuereply->issue_id;
                $response['reply_id'] = $issuereply->id;
                $response['message'] = 'Material Issue Reply Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'issue-item-reply',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function issueReplyList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $productissueid = $request->issue_id;
                $productuniqueid = $request->unique_id;
                if(!empty($productissueid)){
                    $issues = AppProductIssue::find($productissueid);
                }else{
                    $issues = AppProductIssue::where('unique_id',$productuniqueid)->first();
                }
                if(empty($issues)){
                    $response['status'] = 301;
                    $response['message'] = 'Issued product not found';
                    return $response;
                }
                $issuereplyarray =  AppProductIssueReply::where('unique_id',$issues->unique_id)->get();
                $issuereplylist = array();
                if(count($issuereplyarray)>0){
                    foreach ($issuereplyarray as $issuereply){

                        $issuearray = array();
                        $issuearray['id'] = $issuereply->id;
                        $issuearray['unique_id'] = $issuereply->unique_id;
                        $issuearray['issue_id'] = $issuereply->issue_id;
                        $issuearray['comment'] = $issuereply->comment;
                        $issuearray['alignment'] = $issuereply->alignment;
                        $issuearray['added_by'] = $issuereply->added_by;
                        $issuearray['userdetails'] = $issuereply->userdetails;
                        $issuearray['created_date'] = $issuereply->created_at;
                        $issuearray['datetime'] = $issuereply->datetime;
                        $issuearray['images'] = !empty($issuereply->images) ? $issuereply->images : array();
                        $issuereplylist[] = $issuearray;
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Material Issue Reply Updated';
                $response['responselist'] = $issuereplylist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'issue-reply-list',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}
