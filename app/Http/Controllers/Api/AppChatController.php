<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\Company;
use App\Conversation;
use App\Employee;
use App\GroupChat;
use App\GroupChatMessage;
use App\GroupChatUsers;
use App\Http\Controllers\Controller;
use App\ProjectMember;
use App\ProjectsLogFiles;
use App\ProjectsLogs;
use App\Traits\Notifications;
use App\User;
use App\UserChat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;

class AppChatController extends Controller
{
    use Notifications;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }
    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }
    public function pusherAppKeys(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = $responselist = array();
                $responselist['app_key'] = '56d658761051d805686d';
                $responselist['app_secret'] = '4ca6e2827e2c196895a0';
                $responselist['app_id'] = '1318234';
                $responselist['cluster'] = 'ap2';
                $responselist['chat_channel'] = 'conversations';
                $responselist['chat_channel_event'] = 'conversation.';
                $response['status'] = 200;
                $response['message'] = 'Message sent successfully';
                $response['response'] = $responselist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'send-message',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function conversationList(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = $projectsarray = array();
                $conversationchat = array();
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                $conversationarray = Conversation::whereRaw('FIND_IN_SET(?,users)', [$user->id])->orderBy('lastupdated','desc')->offset($skip)->take($count)->get();
                foreach ($conversationarray as $conversation){
                    $conarray = array();
                    switch ($conversation->module){
                        case 'chat':
                            $usersarray = array_unique(array_filter(explode(',',$conversation->users)));
                            if (($key = array_search($user->id, $usersarray)) !== false) {
                                unset($usersarray[$key]);
                            }
                            $employee = Employee::where('company_id',$user->company_id)->whereIn('user_id',$usersarray)->first();
                            if(!empty($employee)){
                                $lastmessage = UserChat::lastMessage($employee->user_id,$user->id);
                                $conarray['id'] = $conversation->id;
                                $conarray['userid'] = $user->id;
                                $conarray['moduleid'] = $conversation->moduleid;
                                $conarray['module'] = $conversation->module;
                                $conarray['chatid'] = $employee->user_id;
                                $conarray['name'] = $employee->name;
                                $conarray['image'] = get_employee_image_link($employee->id);
                                $conarray['lastmessage'] = !empty($lastmessage) ? $lastmessage->message : '';
                                $conarray['lastmessagedate'] = !empty($lastmessage) ? get_last_date($lastmessage->created_at) : '';
                            }else{
                                $chatusers = User::withoutGlobalScope('company')->withoutGlobalScope('active')->whereIn('id',$usersarray)->first();
                                if(!empty($chatusers)){
                                    $conarray['id'] = $conversation->id;
                                    $conarray['userid'] = $user->id;
                                    $conarray['moduleid'] = $conversation->moduleid;
                                    $conarray['module'] = $conversation->module;
                                    $lastmessage = !empty($chatusers) ? UserChat::lastMessage($chatusers->id,$user->id) : '';
                                    $conarray['chatid'] = $chatusers->id;
                                    $conarray['name'] = $chatusers->name;
                                    $conarray['image'] = get_users_image_link($chatusers->id);
                                    $conarray['lastmessage'] = !empty($lastmessage) ? $lastmessage->message : '';
                                    $conarray['lastmessagedate'] = !empty($lastmessage) ? get_last_date($lastmessage->created_at) : '';
                                }
                            }
                            break;
                        case 'project':
                            $projectinfo =  AppProject::find($conversation->moduleid);
                            if(!empty($projectinfo)){
                                $conarray['id'] = $conversation->id;
                                $conarray['userid'] = $user->id;
                                $conarray['moduleid'] = $conversation->moduleid;
                                $conarray['module'] = $conversation->module;
                                $groupmsg = ProjectsLogs::where('project_id',$projectinfo->id)->where('module_id',$projectinfo->id)->where('module','chat')->orderBy('id','desc')->first();
                                $conarray['projectid'] = $projectinfo->id;
                                $conarray['name'] = $projectinfo->project_name;
                                $conarray['image'] = $projectinfo->imageurl;
                                $conarray['lastmessage'] = !empty($groupmsg) ? $groupmsg->description : '';
                                $conarray['lastmessagedate'] = !empty($groupmsg) ? get_last_date($groupmsg->created_at) : '';
                            }
                            break;
                        case 'group':
                            $groupinfo =  GroupChat::find($conversation->moduleid);
                            if(!empty($groupinfo)) {
                                $conarray['id'] = $conversation->id;
                                $conarray['userid'] = $user->id;
                                $conarray['moduleid'] = $conversation->moduleid;
                                $conarray['module'] = $conversation->module;
                                $groupmsg = GroupChatMessage::where('group_id', $groupinfo->id)->where('type', 'text')->orderBy('id', 'desc')->first();
                                $conarray['groupid'] = $groupinfo->id;
                                $conarray['name'] = $groupinfo->name;
                                $conarray['image'] = $groupinfo->image_url;
                                $conarray['lastmessage'] = !empty($groupmsg) ? $groupmsg->message : '';
                                $conarray['lastmessagedate'] = !empty($groupmsg) ? get_last_date($groupmsg->created_at) : '';
                            }
                            break;
                    }
                    if(!empty($conarray)){
                        $conversationchat[] = $conarray;
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Conversation logs List Fetched';
                $response['response'] = $conversationchat;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'timeline-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function timeLineLogs(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectselectarray = explode(',',$request->project_id);
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                if(!empty($projectselectarray)){
                    $prarray = $projectselectarray;
                }else{
                    $project = AppProject::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                    $pm = ProjectMember::where('user_id',$user->id)->where('share_project','1')->get()->pluck('project_id')->toArray();
                    $prarray = array_filter(array_merge($project,$pm));
                }

                $projectsarray = ProjectsLogs::whereIn('project_id',$prarray);
                if (!empty($subprojectid)) {
                    $projectsarray = $projectsarray->where('subproject_id', $subprojectid);
                }
                if (!empty($request->fromdate) && !empty($request->todate)) {
                    $fromdate = Carbon::parse($request->fromdate)->format('Y-m-d');
                    $todate = Carbon::parse($request->todate)->format('Y-m-d');
                    $projectsarray = $projectsarray->whereBetween('created_at', [$fromdate." 00:00:00", $todate." 23:59:59"]);
                }
                if (!empty($segmentid)) {
                    $projectsarray = $projectsarray->where('segment_id', $segmentid);
                }
                $dontsohw = array('projects','project_category','chat');
                $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
                $response['status'] = 200;
                $response['message'] = 'Timeline logs List Fetched';
                $response['response'] = $projectsarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'timeline-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function mentionLogs(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                $projectsarray = ProjectsLogs::whereNotNull('mentionusers')->whereRaw('FIND_IN_SET(?,mentionusers)', [$user->ID]);
                if (!empty($subprojectid)) {
                    $projectsarray = $projectsarray->where('subproject_id', $subprojectid);
                }
                if (!empty($segmentid)) {
                    $projectsarray = $projectsarray->where('segment_id', $segmentid);
                }
                $dontsohw = array('projects','project_category');
                $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
                $response['status'] = 200;
                $response['message'] = 'Timeline logs List Fetched';
                $response['response'] = $projectsarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'timeline-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function membersList(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                $empusers = Employee::where('company_id',$user->company_id)->get();
                $userarray = array();
                if(count($empusers)>0){
                    foreach ($empusers as $userschat){
                        $users = array();
                        $users['id'] = $userschat->id;
                        $users['userid'] = $userschat->user_id;
                        $users['name'] = ucwords($userschat->name);
                        $users['mobile'] = $userschat->mobile;
                        $users['email'] = $userschat->email;
                        $users['user_type'] = $userschat->user_type;
                        $users['image_url'] = get_employee_image_link( $userschat->id);
                        $userarray[] = $users;
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Chat members List';
                $response['response'] = $userarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'chat-members',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createConversation(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $userid = $request->userid;
                $conversation = UserChat::conversionDetail($user->id,$userid);
                $response['status'] = 200;
                $response['message'] = 'Chat members List';
                $response['conversationid'] = $conversation;
                $response['chatid'] = $userid;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-conversation',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function chatMessages(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                $messages = array();
                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);
                $chatDetailsarray = UserChat::chatDetail($conversationdetails->user_one, $conversationdetails->user_two,$skip);
                if(count($chatDetailsarray)>0){
                    foreach ($chatDetailsarray as $chatDetails){
                        $message =$img = array();
                        $message['id'] = $chatDetails->id;
                       if($chatDetails->from == $user->id){
                           $message['align'] = 'right';
                           $message['name'] =  'you';
                           $message['image'] =  get_users_image_link($chatDetails->from);
                       }else{
                           $message['align'] = 'left';
                           $message['name'] =  $chatDetails->fromUser->name;
                           $message['image'] =  get_users_image_link($chatDetails->to);
                       }
                        $message['from'] = $chatDetails->from;
                        $message['to'] = $chatDetails->to;
                        $message['type'] = $chatDetails->type;
                        $message['message'] = $chatDetails->message;
                        if(!empty(!empty($chatDetails->image))){
                            $img['name'] = '';
                            $img['image'] =   uploads_url().'chat-image/'.$chatDetails->image ;
                            $img['created_at'] = date('d M Y',strtotime($chatDetails->created_at));
                            $message['images'][] = $img;
                        }else{
                            $message['images'] = $img;
                        }
                         $message['datetime'] = get_last_date($chatDetails->created_at);
                        $messages[] = $message;
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Chat members List';
                $response['response'] = $messages;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'chat-members',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function sendMessage(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);
                $userid = $request->userid;
                $message = $request->message ?: '';
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }

                $conversationdetails->lastupdated = date('Y-m-d H:i:s');
                $conversationdetails->save();

                if(!empty($message)) {
                    $allocatedModel = new UserChat();
                    $allocatedModel->message = $message;
                    $allocatedModel->user_one = $user->id;
                    $allocatedModel->user_id = $userid;
                    $allocatedModel->from = $user->id;
                    $allocatedModel->to = $userid;
                    $allocatedModel->type = 'text';
                    $allocatedModel->save();
                }
                if($request->hasFile('image')){
                    foreach ($request->image as $fileData){
                        $image = $fileData->hashName();
                        $destinationPath = 'uploads/chat-image/';
                        if (!file_exists($destinationPath)) {
                            mkdir($destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $image);
                        $allocatedModel = new UserChat();
                        $allocatedModel->image = $image;
                        $allocatedModel->type = 'image';
                        $allocatedModel->message         = $message;
                        $allocatedModel->user_one = $user->id;
                        $allocatedModel->user_id = $userid;
                        $allocatedModel->from = $user->id;
                        $allocatedModel->to = $userid;
                        $allocatedModel->save();
                    }
                }

                $notifyUser = User::withoutGlobalScope('active')->where('id',$allocatedModel->user_id)->first();
                if($notifyUser){
                    $this->conversations($notifyUser, 'chat', $conversationdetails->id, $message);
                }
                $response['status'] = 200;
                $response['message'] = 'Message sent successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'send-message',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function  projectPulseDetails(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $conversationid = $request->conversationid;
                $conversation = Conversation::where('id',$conversationid)->first();
                if(empty($conversation)){
                    $response['message'] = "Conversation Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $projectdetails = AppProject::where('id',$conversation->moduleid)->first();
                if(empty($projectdetails)){
                    $response['message'] = "Project Not Found";
                    $response['status'] = '301';
                    return $response;
                }
                $responsearray = array();
                $responsearray['id'] = $projectdetails->id;
                $responsearray['company_id'] = $projectdetails->company_id;
                $responsearray['name'] = ucwords($projectdetails->project_name);
                $responsearray['image'] = $projectdetails->image;
                $responsearray['image_url'] = $projectdetails->imageurl;
                $usersarray = array();
                $projectmembers = ProjectMember::join('employee','employee.id','=','project_members.employee_id')
                                    ->select('employee.*')->where('project_members.project_id',$projectdetails->id)->where('project_members.share_project','1')->get();
                 if(count($projectmembers)>0){
                    foreach ($projectmembers as $projectmem){
                        $users = array();
                        $users['id'] = $projectmem->id;
                        $users['company_id'] = $projectmem->company_id;
                        $users['user_id'] = $projectmem->user_id;
                        $users['name'] = $projectmem->name;
                        $users['image'] = get_employee_image_link($projectmem->id);
                        $usersarray[] = $users;
                    }
                }
                $responsearray['usersarray'] = $usersarray;
                $imagesarray = array();
                $projectlogsimagesarray = ProjectsLogs::where('project_id',$projectdetails->id)
                                    ->where('module_id',$projectdetails->id)
                                    ->where('module','chat')->get();
                if(count($projectlogsimagesarray)>0){
                    foreach ($projectlogsimagesarray as $projectlogsimage){
                        if(!empty($projectlogsimage->images)){
                            $imagesarray[] =$projectlogsimage->images;
                        }
                    }
                }
                $imagesarray = call_user_func_array('array_merge',$imagesarray);
                $responsearray['imagesarray'] = $imagesarray;

                $response['status'] = 200;
                $response['message'] = 'Project chat Details';
                $response['response'] = $responsearray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'project-pulse-details',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function projectPulseMessages(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);

                $projectid = $conversationdetails->moduleid;
                $projectId = Project::find($projectid);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }

                $projectsarray = ProjectsLogs::where('project_id',$projectId->id);
                if (!empty($subprojectid)) {
                    $projectsarray = $projectsarray->where('subproject_id', $subprojectid);
                }
                if (!empty($request->fromdate) && !empty($request->todate)) {
                    $fromdate = Carbon::parse($request->fromdate)->format('Y-m-d');
                    $todate = Carbon::parse($request->todate)->format('Y-m-d');
                    $projectsarray = $projectsarray->whereBetween('created_at', [$fromdate." 00:00:00", $todate." 23:59:59"]);
                }
                if (!empty($segmentid)) {
                    $projectsarray = $projectsarray->where('segment_id', $segmentid);
                }
                $dontsohw = array('projects','project_category');
                $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get()->toArray();
                $projectsarray =  array_reverse($projectsarray);
                $msgarray = array();
                if(count($projectsarray)>0){
                    foreach ($projectsarray as $projectmsg){
                        $aligntment = 'left';
                        if($projectmsg['added_id']==$user->id){
                            $aligntment = 'right';
                        }
                        $msgarray[] = array(
                            'id' => $projectmsg['id'],
                            'company_id' => $projectmsg['company_id'],
                            'added_id' => $projectmsg['added_id'],
                            'module_id' => $projectmsg['module_id'],
                            'module' => $projectmsg['module'],
                            "modulename" => $projectmsg["modulename"],
                            "category_id" => $projectmsg["category_id"],
                            "category_module" =>  $projectmsg["category_module"],
                            "project_id" => $projectmsg["project_id"],
                            "subproject_id" =>  $projectmsg["subproject_id"],
                            "segment_id" =>  $projectmsg["segment_id"],
                            "heading" =>  $projectmsg["heading"],
                            "description" =>  $projectmsg["description"],
                            "medium" =>  $projectmsg["medium"],
                            "mentionusers" =>  $projectmsg["mentionusers"],
                            "created_at" => $projectmsg["created_at"],
                            "updated_at" =>  $projectmsg["updated_at"],
                            "alignment" =>  $aligntment,
                            "issueid" =>  $projectmsg["issueid"],
                            "taskid" =>  $projectmsg["taskid"],
                            "boqid" =>  $projectmsg["boqid"],
                            "manpoweruniqueid" =>  $projectmsg["manpoweruniqueid"],
                            "manpowerid" =>  $projectmsg["manpowerid"],
                            "indentsid" =>  $projectmsg["indentsid"],
                            "status" =>  $projectmsg["status"],
                            "username" =>  $projectmsg["username"],
                            "userimage" =>  $projectmsg["userimage"],
                            "project_name" =>  $projectmsg["project_name"],
                            "subproject_name" =>  $projectmsg["subproject_name"],
                            "taskpercentcomment" =>  $projectmsg["taskpercentcomment"],
                            "images" =>  $projectmsg["images"],
                            "datetime" => get_last_date($projectmsg["created_at"]),
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Timeline logs List Fetched';
                $response['response'] = $msgarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'project-pulse-messages',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function projectPulseSendMessages(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);
                $conversationdetails->lastupdated = date('Y-m-d H:i:s');
                $conversationdetails->save();

                $projectid = $conversationdetails->moduleid;
                $projectId = Project::find($projectid);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $message = $request->message ?: '';
                $medium = $request->medium ?: 'web';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $projectId->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $projectId->id;
                $createlog->module = 'chat';
                $createlog->project_id = $projectId->id ?: 0;
                $createlog->subproject_id =   0;
                $createlog->segment_id =   0;
                $createlog->heading =   'Chat';
                $createlog->modulename = 'chat';
                $createlog->description = $message;
                $createlog->medium = $medium;
                $createlog->save();
                if(!empty($request->image)){
                    foreach ($request->image as $fileData){
                        $image = $fileData->hashName();
                        $destinationPath = 'uploads/project-logs-files/';
                        if (!file_exists($destinationPath)) {
                            mkdir($destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $image);
                        $createlogfile = new ProjectsLogFiles();
                        $createlogfile->company_id = $user->company_id;
                        $createlogfile->user_id = $user->id;
                        $createlogfile->projectlog_id = $createlog->id;
                        $createlogfile->filename = $fileData->getClientOriginalName();
                        $createlogfile->hashname = $fileData->hashName();
                        $createlogfile->size = $fileData->getSize();
                        $createlogfile->save();
                    }
                }


                $pm = ProjectMember::where('project_id',$projectid)->where('share_project','1')->pluck('user_id')->toArray();
                $pm[] = $user->id;
                $pm[] = $projectId->added_by;
                // Notify User
                $notifyUserarray = User::withoutGlobalScope('company')->withoutGlobalScope('active')->whereIn('id',$pm)->get();
                foreach($notifyUserarray as $notifyUser){
                    if($notifyUser){
                        $this->conversations($notifyUser, 'projectpulse', $projectid, $message);
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Message sent successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'project-pulse-send-messages',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createGroupChat(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $name = $request->name;
                if(empty($name)){
                    $response['message'] = "Group name is empty";
                    $response['status'] = '301';
                    return $response;
                }
                $conversationid = $request->conversationid;
                if(!empty($conversationid)){
                    $conversation = Conversation::where('id',$conversationid)->where('module','group')->first();
                    $groupchatid = $conversation->moduleid;
                }
                if(!empty($groupchatid)){
                    $groupchat = GroupChat::where('company_id',$companyid)->where('id',$groupchatid)->first();
                }else{
                    $groupchat = new GroupChat();
                    $groupchat->company_id = $companyid;
                    $groupchat->added_by = $user->id;
                }
                $groupchat->name = $name;
                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image->hashName();
                    switch($storage) {
                        case 'local':
                            $request->image->storeAs('uploads/groupchats', $image);
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('groupchats/', $request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('groupchats/'.'/', $request->image, $request->image->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }
                    $groupchat->image= $request->image->hashName();
                }
                $groupchat->save();


                if(empty($conversation)){
                    $conversation = new Conversation();
                    $conversation->user_one = $user->id;
                    $conversation->moduleid = $groupchat->id;
                    $conversation->module = 'group';
                    $conversation->lastupdated = date('Y-m-d H:i:s');
                    $conversation->save();
                }
                $page = 0;
                if(!empty($request->usersid)){
                    $selectedusers = explode(',',$request->usersid);
                    $groupusers = explode(',',$groupchat->joined_users);
                    $groupusers[] = $user->id;
                    $groupusers  = array_merge($selectedusers,$groupusers);
                    $groupusers = array_filter(array_unique($groupusers));
                    $joined_users = implode(',',$groupusers);
                    $groupchat->joined_users = $joined_users;
                    $groupchat->save();

                    if(count($groupusers)>0){
                        foreach ($groupusers as $groupus){
                            $groupchatmessage = GroupChatUsers::where('group_id',$groupchat->id)->where('user_id',$groupus)->first();
                            if(empty($groupchatmessage)){
                                $groupchatmessage = new GroupChatUsers();
                                $groupchatmessage->company_id = $groupchat->company_id;
                                $groupchatmessage->group_id = $groupchat->id;
                                $groupchatmessage->user_id = $groupus;
                                $groupchatmessage->status = '1';
                                $groupchatmessage->save();
                            }
                        }
                    }

                    $conversation->users =  $joined_users;
                    $conversation->save();

                }

                $response['status'] = 200;
                $response['message'] = 'Group Chat Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-group-chat',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function  GroupChatDetails(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $conversationid = $request->conversationid;
                $conversation = Conversation::where('id',$conversationid)->first();
                if(empty($conversation)){
                    $response['message'] = "Conversation Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $groupchat = GroupChat::where('id',$conversation->moduleid)->first();
                if(empty($groupchat)){
                    $response['message'] = "Group Not Found";
                    $response['status'] = '301';
                    return $response;
                }
                $responsearray = array();
                $responsearray['id'] = $groupchat->id;
                $responsearray['company_id'] = $groupchat->company_id;
                $responsearray['user_id'] = $user->id;
                $responsearray['added_by'] = $groupchat->added_by;
                $responsearray['name'] = ucwords($groupchat->name);
                $responsearray['image'] = $groupchat->image;
                $responsearray['image_url'] = $groupchat->image_url;
                $responsearray['datetime'] = $groupchat->datetime;
                $usersarray = array();
                $groupusers = GroupChatUsers::where('group_id',$groupchat->id)->get();
                if(count($groupusers)>0){
                    foreach ($groupusers as $groupus){
                            $users = array();
                            $users['id'] = $groupus->id;
                            $users['company_id'] = $groupus->company_id;
                            $users['user_id'] = $groupus->user_id;
                            $users['username'] = $groupus->username;
                            $users['userimage'] = $groupus->userimage;
                            $users['status'] = $groupus->status;
                            $users['status_text'] = 'Pending';
                            if($groupus->status=='1'){
                                $users['status_text'] = 'Active';
                            }
                            if($groupus->status=='2'){
                                $users['status_text'] = 'Exit';
                            }
                            $usersarray[] = $users;
                    }
                }
                $responsearray['usersarray'] = $usersarray;
                $imagesarray = array();
                $groupusersimages = GroupChatMessage::where('group_id',$groupchat->id)->whereNotNull('image')->orderBy('id','asc')->get();
                if(count($groupusersimages)>0){
                    foreach ($groupusersimages as $groupus){
                            $users = array();
                            $users['id'] = $groupus->id;
                            $users['image'] = $groupus->image;
                            $users['image_url'] = !empty($groupus->image) ? uploads_url().'groupchats/'.$groupus->image : '';
                            $users['datetime'] = date('d M Y',strtotime($groupus->created_at));
                            $imagesarray[] = $users;
                        }
                }
                $responsearray['imagesarray'] = $imagesarray;

                $response['status'] = 200;
                $response['message'] = 'Group Chat Updated Successfully';
                $response['response'] = $responsearray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-group-chat',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function GroupChatAddMember(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);
                $userid = $request->userid;
                $message = $request->message;
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }

                $groupid = $conversationdetails->moduleid;
                $page = 0;
                $selectedusers = array_filter(explode(',',$request->usersid));
                $groupdetails = GroupChat::find($groupid);
                $groupusers = explode(',',$groupdetails->joined_users);
                $groupusers  = array_merge($selectedusers,$groupusers);
                $groupusers[] = $user->id;
                $groupusers = array_filter(array_unique($groupusers));
                $joined_users = implode(',',$groupusers);
                $groupdetails->joined_users = $joined_users;
                $groupdetails->save();
                if(count($groupusers)>0){
                    foreach ($groupusers as $groupus){
                        $groupchatmessage = GroupChatUsers::where('group_id',$groupdetails->id)->where('user_id',$groupus)->first();
                        if(empty($groupchatmessage)){
                            $groupchatmessage = new GroupChatUsers();
                            $groupchatmessage->company_id = $groupdetails->company_id;
                            $groupchatmessage->group_id = $groupdetails->id;
                            $groupchatmessage->user_id = $groupus;
                            $groupchatmessage->status = '1';
                            $groupchatmessage->save();
                        }
                    }
                }
                $conversationdetails->users =  $joined_users;
                $conversationdetails->save();

                $response['status'] = 200;
                $response['message'] = 'Member Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'send-message',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function GroupChatMemberExit(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $conversationid = $request->conversationid;
                $conversation = Conversation::where('id',$conversationid)->first();
                if(empty($conversation)){
                    $response['message'] = "Conversation Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $groupid = $conversation->moduleid;
                $userid = $request->userid;
                $groupchatmessage = GroupChatUsers::where('group_id',$groupid)->where('user_id',$userid)->first();
                if(empty($groupchatmessage)){
                    $response['status'] = 301;
                    $response['message'] = 'Group Chat User Not Found';
                    return $response;
                }
                $groupchatmessage->status = '2';
                $groupchatmessage->save();
                $response['status'] = 200;
                $response['message'] = 'Status Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'group-chat-exit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function GroupChatMemberDelete(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $conversationid = $request->conversationid;
                $conversation = Conversation::where('id',$conversationid)->where('module','group')->first();
                if(empty($conversation)){
                    $response['message'] = "Conversation Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $groupid = $conversation->moduleid;
                $groupdetails = GroupChat::find($groupid);
                if(empty($groupdetails)){
                    $response['message'] = "Group Not Found";
                    $response['status'] = 301;
                    return $response;
                }
                $userid = $request->userid;
                 GroupChatUsers::where('group_id',$groupid)->where('user_id',$userid)->delete();
                $joinedusers = explode(',',$groupdetails->joined_users);
                if (($key = array_search($userid, $joinedusers)) !== FALSE) {
                    unset($joinedusers[$key]);
                }
                $joined_users = implode(',',$joinedusers);
                $groupdetails->joined_users = !empty($joined_users) ? $joined_users : '';
                $groupdetails->save();


                $conversation->users =  !empty($joined_users) ? $joined_users : '';
                $conversation->save();

                if(empty($joined_users)){
                    GroupChatUsers::where('group_id',$groupdetails->id)->delete();
                    GroupChatMessage::where('group_id',$groupdetails->id)->delete();

                    $groupdetails->delete();
                    $conversation->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Member Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'group-chat-delete',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function GroupChatMessages(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);

                $groupid = $conversationdetails->moduleid;

                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }

                $projectsarray = GroupChatMessage::where('group_id', $groupid);

                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->pluck('id')->toArray();
                $projectsarray = array_reverse($projectsarray);
                $msgarray = GroupChatMessage::whereIn('id',$projectsarray)->get();
                $replyarray = array();
                foreach ($msgarray as $punchitemreply){
                    $punchite = $img = array();
                    $punchite['id'] = $punchitemreply->id;
                    $punchite['company_id'] = $punchitemreply->company_id;
                    $punchite['user_id'] = $punchitemreply->user_id;
                    $punchite['group_id'] = $punchitemreply->group_id;
                    $punchite['type'] = $punchitemreply->type;
                    $punchite['message'] = $punchitemreply->message;
                    if(!empty(!empty($punchitemreply->image))) {
                        $img['name'] = '';
                        $img['image'] =  uploads_url() . 'group-chat-image/' . $punchitemreply->image;
                        $img['created_at'] = date('d M Y', strtotime($punchitemreply->created_at));
                        $punchite['images'][] = $img;
                    }else{
                        $punchite['images'] = $img;
                    }
                    $punchite['username'] = $punchitemreply->username;
                    $punchite['userimage'] = $punchitemreply->userimage;
                    $punchite['alignment'] = 'left';
                    if($punchitemreply->user_id==$user->id){
                        $punchite['alignment'] = 'right';
                    }
                    $punchite['created_at'] =  $punchitemreply->created_at;
                    $punchite['datetime'] =  get_last_date($punchitemreply->created_at);
                    $replyarray[] = $punchite;
                }
                $response['status'] = 200;
                $response['message'] = 'Group Chat List Fetched';
                $response['response'] = $replyarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'group-chat-messages',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function GroupChatSendMessages(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();

                $converstion = $request->conversationid;
                $conversationdetails = Conversation::find($converstion);
                $conversationdetails->lastupdated = date('Y-m-d H:i:s');
                $conversationdetails->save();

                $groupid = $conversationdetails->moduleid;

                $message = $request->message ?: '';
                $groupchatinfo = GroupChat::find($groupid);

                if(!empty($message)) {
                    $createlog = new GroupChatMessage();
                    $createlog->company_id = $user->company_id;
                    $createlog->user_id = $user->id;
                    $createlog->group_id = $groupchatinfo->id;
                    $createlog->message = $message;
                    $createlog->type = 'text';
                    $createlog->save();
                }
                if($request->hasFile('image')){
                    foreach ($request->image as $fileData){
                        $image = $fileData->hashName();
                        $destinationPath = 'uploads/group-chat-image/';
                        if (!file_exists($destinationPath)) {
                            mkdir($destinationPath, 0777, true);
                        }
                        $fileData->storeAs($destinationPath, $image);
                        $createlog = new GroupChatMessage();
                        $createlog->company_id = $user->company_id;
                        $createlog->user_id = $user->id;
                        $createlog->group_id = $groupchatinfo->id;
                        $createlog->message = $message;
                        $createlog->image = $image;
                        $createlog->type = 'image';
                        $createlog->save();
                    }
                }

                $pm = explode(',',$conversationdetails->users);
                // Notify User
                $notifyUserarray = User::withoutGlobalScope('active')->whereIn('id',$pm)->get();
                foreach($notifyUserarray as $notifyUser){
                    if($notifyUser){
                        $this->conversations($notifyUser, 'groupchat', $groupchatinfo->id, $message);
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Message sent successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'group-chat-send-message',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}