<?php

namespace App\Http\Controllers\Api;

use App\AppLogs;
use App\AppProject;
use App\AppVersion;
use App\Company;
use App\Currency;
use App\Employee;
use App\GlobalCurrency;
use App\Http\Controllers\Controller;
use App\Indent;
use App\IntroSliders;
use App\InvitedUser;
use App\Module;
use App\ModuleSetting;
use App\Package;
use App\Permission;
use App\PermissionRole;
use App\ProjectMember;
use App\ProjectPermission;
use App\Promotional_fcm;
use App\PunchItem;
use App\Role;
use App\RoleUser;
use App\TempUser;
use App\User;
use Dompdf\Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Project;
use Illuminate\Support\Facades\Auth;
use DB;

class AppApiController extends Controller
{
    use AuthenticatesUsers;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    public function appLogin(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $response = array();
                $email = trim($request->email);
                $password = trim($request->password);
                $requestarray  = array();

                //dd($request->email);

                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $requestarray['email']  = $email;
                    $requestarray['password']  = $password;
                    $requestarray['login']  = 'enable';
                    $user = User::where('email', $email)->first();
                }else{
                    $requestarray['mobile']  = $email;
                    $requestarray['password']  = $password;
                    $requestarray['login']  = 'enable';
                    $user = User::where('mobile', $email)->first();
                }

                if(empty($user)){
                    $response['message'] = 'User does not exists.';
                    $response['status'] = 300;
                    return $response;
                }
                $result = Auth::attempt($requestarray, $request->filled('remember'));
                if ($result) {
                    $appid = md5(microtime());
                    $platform = strtolower($request->medium);
                    $fcm = $request->fcm;
                    if($platform=='ios'){
                        $user->iosfcm = $fcm;
                        Promotional_fcm::where('iosfcm_id',$fcm)->delete();
                    }else{
                        $user->fcm = $fcm;
                        Promotional_fcm::where('fcm_id',$fcm)->delete();
                    }
                    $user->appid = $appid;
                    $user->save();
                    $companydetails = Company::where('id',$user->company_id)->first();
                    $uData = array();
                    $uData['id'] = $user->id;
                    $uData['company_id'] = !empty($companydetails) ? $companydetails->id : '';
                    $uData['company_name'] = !empty($companydetails) ? $companydetails->company_name : '';
                    $uData['company_email'] = !empty($companydetails) ? $companydetails->company_email : '';
                    $uData['company_phone'] = !empty($companydetails) ? $companydetails->company_phone : '';
                    $uData['company_logo'] = !empty($companydetails&&$companydetails->logo) ? asset_url('uploads/app-logo/'.$companydetails->logo) : '';
                    $uData['name'] = $user->name;
                    $uData['email'] = $user->email;
                    $uData['image'] = get_users_image_link($user->id);
                    $uData['image_url'] = $user->image;
                    $uData['mobile'] = $user->mobile;
                    $uData['gender'] = $user->gender;
                    $uData['status'] = $user->status;
                    $uData['fcm'] = $user->fcm;
                    $uData['token'] = $appid;
                    $response['message'] = 'success';
                    $response['status'] = '200';
                    $response['response'] = $uData;
                    return $response;
                }
                $this->incrementLoginAttempts($request);
                $response['message'] = 'Invalid User name and Password please try again.';
                $response['status'] = 300;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-login',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function appLoginOtp(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $response = array();
                $mobile = trim($request->mobile);
                if(!checkMobile($mobile)){
                    $response['message'] = 'Mobile number is invalid.';
                    $response['status'] = 300;
                    return $response;
                    //return response()->json(['error' => 'mobile', 'message'=>'Mobile number is invalid.'], 401);
                }
                $requestarray  = array();
                $user = User::withoutGlobalScope('active')->where('mobile', $mobile)->first();
                if(empty($user)){
                    $user = new User();
                    $user->name = 'Guest';
                    $user->company_id = '0';
                    $user->email = '';
                    $user->login = 'disable';
                    $user->status = 'inactive';
                    $user->mobile = $mobile;
                }
                $otp = rand(1111,9999);
                /* 9dFWwwv%2FfIh    V+wRpavOIwp */
                $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
                $user->otp = $otp;
                $user->save();
                send_sms($user->mobile,$message);
                $uData = array();
                $uData['id'] = $user->id;
                $uData['mobile'] =  $user->mobile;
                $uData['name'] = !empty('') ? $user->name : '';
                $response['message'] = 'success';
                $response['status'] = '200';
                $response['response'] = $uData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-login',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }

    public function appLoginOtpVerify(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $response = array();
                $mobile = trim($request->mobile);
                $userid = trim($request->userid);
                $otp = trim($request->otp);
                $user = User::withoutGlobalScope('active')->where('mobile', $mobile)->where('id', $userid)->first();
                if(empty($user->id)){
                    $response['message'] = 'User Does not exists';
                    $response['status'] = 300;
                    return $response;
                }
                if($user->otp!=$otp){
                    $response['message'] = 'Invalid OTP. Please enter a valid otp';
                    $response['status'] = 300;
                    return $response;
                }
                if ($user) {
                    $appid = md5(microtime());
                    $platform = strtolower($request->medium);
                    $fcm = $request->fcm;
                    if($platform=='ios'){
                        $user->iosfcm = $fcm;
                        Promotional_fcm::where('iosfcm_id',$fcm)->delete();
                    }else{
                        $user->fcm = $fcm;
                        Promotional_fcm::where('fcm_id',$fcm)->delete();
                    }
                    $user->appid = $appid;
                    $user->login = 'enable';
                    $user->status = 'active';
                    $user->save();
                    if($user->login=='enable'){

                        if($user->company_id<='0'){
                            $companydetails = Company::where('company_phone',$user->mobile)->first();
                            if(empty($companydetails->id)){
                                $companydetails = new Company();
                            }
                            $companydetails->company_name = $user->name;
                            $companydetails->company_email = $user->email ?: '';
                            $companydetails->company_phone = $user->mobile;
                            $companydetails->address = 'NA';
                            $companydetails->package_type = 'annual';
                            $companydetails->package_id = 1;
                            $companydetails->save();

                            $user->company_id = $companydetails->id;
                            $user->save();

                        }else{
                            $companydetails = Company::where('id',$user->company_id)->first();
                        }

                        $uData = array();
                        $uData['id'] = $user->id;
                        $uData['company_id'] = !empty($companydetails->id) ? $companydetails->id : '';
                        $uData['company_name'] = !empty($companydetails->company_name) ? $companydetails->company_name : '';
                        $uData['company_email'] = !empty($companydetails->company_email) ? $companydetails->company_email : '';
                        $uData['company_phone'] = !empty($companydetails->company_phone) ? $companydetails->company_phone : '';
                        $uData['company_logo'] = !empty($companydetails->logo) ? public_path('uploads/app-logo/'.$companydetails->logo) : '';
                        $uData['name'] = $user->name;
                        $uData['usertype'] = 'old';
                        $uData['email'] = $user->email;
                        $uData['image'] = get_users_image_link($user->id);
                        $uData['image_url'] = $user->image;
                        $uData['mobile'] = $user->mobile;
                        $uData['gender'] = $user->gender;
                        $uData['status'] = $user->status;
                        $uData['fcm'] = $user->fcm;
                        $uData['token'] = $appid;
                        $response['message'] = 'success';
                        $response['status'] = '200';
                        $response['response'] = $uData;
                        return $response;

                    }else{
                        $uData = array();
                        $uData['id'] = $user->id;
                        $uData['mobile'] = $user->mobile;
                        $uData['usertype'] = 'new';
                        $response['message'] = 'success';
                        $response['status'] = '200';
                        $response['response'] = $uData;
                        return $response;

                    }
                }
                $this->incrementLoginAttempts($request);
                $response['message'] = 'Invalid User name and Password please try again.';
                $response['status'] = 300;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-login',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function appRegisterUpdate(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{

                $userid = strtolower(trim($request->userid));
                $mobile = strtolower(trim($request->mobile));
                $medium = strtolower(trim($request->medium));
                $users = User::withoutGlobalScope('active')->where('mobile', $mobile)->where('id', $userid)->first();
                if(empty($users->id)){
                    $response['message'] = 'User Does not exists';
                    $response['status'] = 300;
                    return $response;
                }
                $password = trim($request->password);
                $name = strtolower(trim($request->name));
                $email = !empty($request->email) ? strtolower(trim($request->email)) : '';
                $gender = $request->gender;
                $fcm = $request->fcm;
                //Email Validation
                if(!empty($email)&&!checkEmail($email)){
                    $response['message'] = 'Email Id is invalid.';
                    $response['status'] = 300;
                    return $response;
                }
                $appid = md5(microtime());

               /* $com = Company::where(function ($query) use ($mobile,$email) {
                    $query->where('company_phone', '=', $mobile)
                        ->orWhere('company_email', '=', $email);
                })->first();*/
                $com = Company::where('company_phone',$users->mobile)->first();
                if(empty($com->id)){
                    $com = new Company();
                }
                $com->company_name = $name;
                $com->company_email = $email ?: '';
                $com->company_phone = $mobile;
                $com->address = 'NA';
                $com->package_type = 'annual';
                $com->package_id = 1;
                $com->save();

                $globalCurrency = GlobalCurrency::find(4);
                $currency = Currency::where('currency_code', $globalCurrency->currency_code)
                    ->where('company_id', $com->id)->first();

                if(is_null($currency)) {
                    $currency = new Currency();
                    $currency->currency_name = $globalCurrency->currency_name;
                    $currency->currency_symbol = $globalCurrency->currency_symbol;
                    $currency->currency_code = $globalCurrency->currency_code;
                    $currency->is_cryptocurrency = $globalCurrency->is_cryptocurrency;
                    $currency->usd_price = $globalCurrency->usd_price;
                    $currency->company_id = $com->id;
                    $currency->save();
                }

                $com->currency_id = $currency->id;
                $com->save();
                $uData = array();
                if($com) {

                    $users->company_id = $com->id;
                    $users->name = $name;
                    $users->email = $email ?: '';
                    $users->password = !empty($password) ? bcrypt($password) : '';
                    $users->gender = $gender ?: 'male';
                    $users->appid = $appid;
                    if($medium=='ios'){
                        $users->iosfcm = $fcm;
                    }else{
                        $users->fcm = $fcm;
                    }
                    $users->status = 'active';
                    $users->login = 'enable';
                    $users->save();
                    /*$tmparray = TempUser::where(function ($query) use ($mobile,$email) {
                        $query->where('mobile', '=', $mobile)
                            ->orWhere('mobile', '=', $email);
                    })->get();*/

                    $adminRole = Role::where('name', 'admin')->where('company_id', $com->id)->first();
                    $tmparray = TempUser::where('mobile', '=', $mobile)->get();
                    if ($users) {
                        $users->roles()->attach($adminRole->id);
                        if($tmparray){
                            foreach ($tmparray as $tmp){
                                if($tmp) {
                                    $previnvite = InvitedUser::where('company_id',$tmp->company_id)->where('user_id',$users->id)->first();
                                    if(empty($previnvite->id)){
                                        $iu = new InvitedUser();
                                        $iu->company_id = $tmp->company_id;
                                        $iu->user_id = $users->id;
                                        $iu->invited_by = $users->id;
                                        $iu->mobile = $email;
                                        $iu->save();
                                    }
                                    $prevproject = ProjectMember::where('user_id',$users->id)->where('company_id',$tmp->company_id)->where('project_id',$tmp->project_id)->first();
                                    if(empty($prevproject->id)){

                                        $pm = new ProjectMember();
                                        $pm->company_id = $tmp->company_id;
                                        $pm->project_id = $tmp->project_id;
                                        $pm->user_id = $users->id;
                                        $pm->user_type = $tmp->user_type;
                                        $pm->assigned_by = $tmp->invited_by;
                                        $pm->share_project = '1';
                                        $pm->save();
                                        $urole = Role::where('name', $tmp->user_type)->where('company_id', $tmp->company_id)->first();
                                        if($urole === null){
                                            $response['message'] = 'The company invited you do not have user role type '.$tmp->user_type;
                                            $response['status'] = '401';
                                            return $response;
                                        }
                                        $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                                        foreach ($perms as $perm) {
                                            $pprm = new ProjectPermission();
                                            $pprm->project_id = $tmp->project_id;
                                            $pprm->user_id = $users->id;
                                            $pprm->permission_id = $perm;
                                            $pprm->save();
                                        }
                                    }
                                }
                            }
                            TempUser::where('mobile',$mobile)->orWhere('mobile',$email)->delete();
                        }
                        $uData['id'] = $users->id;
                        $uData['company_id'] = $com->id;
                        $uData['company_name'] = $com->company_name;
                        $uData['company_mobile'] = $com->company_phone;
                        $uData['company_email'] = $com->company_email;
                        $uData['name'] = $users->name;
                        $uData['email'] = $users->email;
                        $uData['image'] = $users->image;
                        $uData['image_url'] = get_users_image_link($users->id);
                        $uData['mobile'] = $users->mobile;
                        $uData['gender'] = $users->gender;
                        $uData['fcm'] = $users->fcm;
                        $uData['token'] = $appid;

                        //Set default roles permissions in PermissionRole table
                        $rsx = Role::where('company_id', 1)->where( function($query){
                            $query->where('name', 'admin');
                            $query->orWhere('name', 'executive');
                            $query->orWhere('name', 'administrative');
                            $query->orWhere('name', 'standard');
                        })->get();
                        foreach($rsx as $rs){
                            $permissions = PermissionRole::where('role_id', $rs->id)->get();
                            foreach ($permissions as $permission){
                                $newRole = Role::where('company_id', $users->company_id)->where('name', $rs->name)->first();
                                if($newRole !== null) {
                                    $perm = new PermissionRole();
                                    $perm->role_id = $newRole->id;
                                    $perm->permission_id = $permission->permission_id;
                                    $perm->save();
                                }
                            }
                        }
                        $payload['message'] = 'success';
                        $payload['status'] = 200;
                        $payload['response'] = $uData;
                    }
                }
            } catch (\Exception $e) {
                $userid = 0;
                app_log($e,'app-register-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $payload['message'] = 'Unauthorised';
            $payload['status'] = '401';
        }
        return $payload;
    }
    public function appRegister(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $name = strtolower(trim($request->name));
                $email = !empty($request->email) ? strtolower(trim($request->email)) : '';
                $mobile = strtolower(trim($request->mobile));
                $password = trim($request->password);
                $gender = $request->gender;
                $fcm = $request->fcm;
                //Email Validation
                if(!empty($email)&&!checkEmail($email)){
                    $response['message'] = 'Email Id is invalid.';
                    $response['status'] = 300;
                    return $response;
                }
                //Mobile number validation
                if(!checkMobile($mobile)){
                    $response['message'] = 'Mobile number is invalid.';
                    $response['status'] = 300;
                    return $response;
                    //return response()->json(['error' => 'mobile', 'message'=>'Mobile number is invalid.'], 401);
                }

                $users = User::withoutGlobalScope('active')->where('mobile', '=', $mobile)->first();
                if(!empty($users->id)&&!empty($users->status)&&$users->status=='active'){
                    $response['message'] = 'Account already exists. Please login with provided details';
                    $response['status'] = 300;
                    return $response;
                }
                $prevmobile = User::withoutGlobalScope('active')->where('mobile',$mobile)->where('login','enable')->first();
                if(empty($prevmobile->id)){

                        $appid = md5(microtime());

                       /* $com = Company::where(function ($query) use ($mobile,$email) {
                            $query->where('company_phone', '=', $mobile)
                                ->orWhere('company_email', '=', $email);
                        })->first();*/
                        $com = Company::where('company_phone',$mobile)->first();
                        if(empty($com->id)){
                            $com = new Company();
                        }
                        $com->company_name = $name;
                        $com->company_email = $email ?: '';
                        $com->company_phone = $mobile;
                        $com->address = 'NA';
                        $com->package_type = 'annual';
                        $com->package_id = 1;
                        $com->save();

                        $globalCurrency = GlobalCurrency::find(4);
                        $currency = Currency::where('currency_code', $globalCurrency->currency_code)
                            ->where('company_id', $com->id)->first();

                        if(is_null($currency)) {
                            $currency = new Currency();
                            $currency->currency_name = $globalCurrency->currency_name;
                            $currency->currency_symbol = $globalCurrency->currency_symbol;
                            $currency->currency_code = $globalCurrency->currency_code;
                            $currency->is_cryptocurrency = $globalCurrency->is_cryptocurrency;
                            $currency->usd_price = $globalCurrency->usd_price;
                            $currency->company_id = $com->id;
                            $currency->save();
                        }

                        $com->currency_id = $currency->id;
                        $com->save();
                        $uData = array();
                        if($com) {

                            //                        withoutGlobalScope('active')->
                            $adminRole = Role::where('name', 'admin')->where('company_id', $com->id)->first();
                            if(!empty($mobile)&&!empty($email)){
                                $users = User::withoutGlobalScope('active')->where(function ($query) use ($mobile,$email) {
                                    $query->where('mobile', '=', $mobile)
                                        ->orWhere('email', '=', $email);
                                })->first();
                            }elseif(!empty($mobile)){
                                $users = User::withoutGlobalScope('active')->where('mobile', '=', $mobile)->first();
                            }elseif(!empty($email)){
                                $users = User::withoutGlobalScope('active')->where('email', '=', $email)->first();
                            }
                            if(empty($users->id)){
                                $users = new User();
                            }
                            $users->company_id = $com->id;
                            $users->name = $name;
                            $users->email = $email ?: '';
                            $users->mobile = $mobile;
                            $users->password = bcrypt($password);
                            $users->gender = $gender ?: 'male';
                            $users->appid = $appid;
                            $users->fcm = $fcm;
                            $users->status = 'inactive';
                            $users->login = 'disable';
                            $users->save();
                            /*$tmparray = TempUser::where(function ($query) use ($mobile,$email) {
                                $query->where('mobile', '=', $mobile)
                                    ->orWhere('mobile', '=', $email);
                            })->get();*/

                            $tmparray = TempUser::where('mobile', '=', $mobile)->get();
                            if ($users) {
                                $users->roles()->attach($adminRole->id);
                                if($tmparray){
                                    foreach ($tmparray as $tmp){
                                        if($tmp) {
                                            $previnvite = InvitedUser::where('company_id',$tmp->company_id)->where('user_id',$users->id)->first();
                                            if(empty($previnvite->id)){
                                                $iu = new InvitedUser();
                                                $iu->company_id = $tmp->company_id;
                                                $iu->user_id = $users->id;
                                                $iu->invited_by = $users->id;
                                                $iu->mobile = $email;
                                                $iu->save();
                                            }
                                            $prevproject = ProjectMember::where('user_id',$users->id)->where('company_id',$tmp->company_id)->where('project_id',$tmp->project_id)->first();
                                            if(empty($prevproject->id)){

                                                $pm = new ProjectMember();
                                                $pm->company_id = $tmp->company_id;
                                                $pm->project_id = $tmp->project_id;
                                                $pm->user_id = $users->id;
                                                $pm->user_type = $tmp->user_type;
                                                $pm->assigned_by = $tmp->invited_by;
                                                $pm->share_project = '1';
                                                $pm->save();
                                                $urole = Role::where('name', $tmp->user_type)->where('company_id', $tmp->company_id)->first();
                                                if($urole === null){
                                                    $response['message'] = 'The company invited you do not have user role type '.$tmp->user_type;
                                                    $response['status'] = '401';
                                                    return $response;
                                                }
                                                $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                                                foreach ($perms as $perm) {
                                                    $pprm = new ProjectPermission();
                                                    $pprm->project_id = $tmp->project_id;
                                                    $pprm->user_id = $users->id;
                                                    $pprm->permission_id = $perm;
                                                    $pprm->save();
                                                }
                                            }
                                        }
                                    }
                                    TempUser::where('mobile',$mobile)->orWhere('mobile',$email)->delete();
                                }
                                //Get User data to return
                                $u = User::withoutGlobalScope('active')->where('id',$users->id)->where('status','inactive')->first();

                                $uData['id'] = $u->id;
                                $uData['company_id'] = $com->id;
                                $uData['company_name'] = $com->name;
                                $uData['company_mobile'] = $com->mobile;
                                $uData['company_email'] = $com->email;
                                $uData['name'] = $u->name;
                                $uData['email'] = $u->email;
                                $uData['image'] = get_users_image_link($u->id);
                                $uData['image_url'] = $u->image;
                                $uData['mobile'] = $u->mobile;
                                $uData['gender'] = $u->gender;
                                $uData['fcm'] = $u->fcm;
                                $uData['token'] = $appid;

                                //Set default roles permissions in PermissionRole table
                                $rsx = Role::where('company_id', 1)->where( function($query){
                                    $query->where('name', 'admin');
                                    $query->orWhere('name', 'executive');
                                    $query->orWhere('name', 'administrative');
                                    $query->orWhere('name', 'standard');
                                })->get();
                                foreach($rsx as $rs){
                                    $permissions = PermissionRole::where('role_id', $rs->id)->get();
                                    foreach ($permissions as $permission){
                                        $newRole = Role::where('company_id', $u->company_id)->where('name', $rs->name)->first();
                                        if($newRole !== null) {
                                            $perm = new PermissionRole();
                                            $perm->role_id = $newRole->id;
                                            $perm->permission_id = $permission->permission_id;
                                            $perm->save();
                                        }
                                    }
                                }
                                $otp = rand(1111,9999);
                                $u->otp = $otp;
                                $u->save();
                                $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
                                if(!empty($mobile)){
                                    send_sms($mobile,$message);
                                }
                            }
                        }
                        $payload['message'] = 'success';
                        $payload['status'] = 200;
                        $payload['response'] = $uData;
                }else{
                    $response['message'] = 'Mobile number is already registered.';
                    $response['status'] = 300;
                    return $response;
                    //return response()->json(['error' => 'mobile', 'message'=>'Mobile number is already registered'], 401);
                }
            } catch (\Exception $e) {
                $userid = 0;
                app_log($e,'app-register',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $payload['message'] = 'Unauthorised';
            $payload['status'] = '401';
        }
        return $payload;
    }
    public function forgotPassword(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if(empty($request->username)){
                    $response['message'] = 'Invalid account. Try again with different email id or mobile number.';
                    $response['status'] = 300;
                    return $response;
                }
                $response = array();
                $username = $request->username;
                if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                    $user = User::where('email', $username)->first();
                }else{
                    $user = User::where('mobile', $username)->first();
                }
                if(!empty($user)){
                    $otp = rand(1111,9999);
                    $user->otp = $otp;
                    $user->save();
                    $mailarray = array();
                    $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
                    $mailarray['email'] = $user->email;
                    $mailarray['subject'] = "OTP for Aakar360 Forgot password";
                    $mailarray['message'] = $message;
                    $user->sendEmail($mailarray);
                    if(!empty($user->mobile)){
                        send_sms($user->mobile,$message);
                    }
                    $response['userid'] = $user->id;
                    $response['message'] = 'Otp Sent Successfully';
                    $response['status'] = 200;
                    return $response;
                }else{
                    $response['message'] = 'Invalid account. Try again with different email id or mobile number.';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'forgot-password',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function resendOTP(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if(empty($request->userid)){
                    $response['message'] = 'Invalid Username';
                    $response['status'] = 401;
                    return $response;
                }
                $response = array();
                $userid = $request->userid;
                $user = User::where('id', $userid)->first();
                if(!empty($user)){
                    $otp = rand(1111,9999);
                    $user->otp = $otp;
                    $user->save();
                    $mailarray = array();
                    $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
                    $mailarray['email'] = $user->email;
                    $mailarray['subject'] = "OTP for Aakar360 Forgot password";
                    $mailarray['message'] = $message;
                    $user->sendEmail($mailarray);
                    if(!empty($user->mobile)){
                        send_sms($user->mobile,$message);
                    }
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    $response['userid'] = $user->id;
                    return $response;
                }else{
                    $response['message'] = 'Login Not found';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'resend-otp',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function resendRegOTP(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if(empty($request->mobile)){
                    $response['message'] = 'Invalid Mobile Number';
                    $response['status'] = 401;
                    return $response;
                }
                $response = array();
                $userid = $request->mobile;
                $user = User::withoutGlobalScope('active')->where('mobile',$userid)->where('status','inactive')->first();
                if(!empty($user)){
                    $otp = rand(1111,9999);
                    $user->otp = $otp;
                    $user->save();
                    $message = "<#> Your Aakar360 App OTP is ".$otp." +BFLcf2yUiQ";
                    if(!empty($user->mobile)){
                        send_sms($user->mobile,$message);
                    }
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    $response['token'] = $user->appid;
                    return $response;
                }else{
                    $response['message'] = 'Login Not found';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'resend-otp',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function verifyUser(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $response = array();
                $userid = $request->userid;
                $otp = (int)$request->otp;
                $user = User::where('id', $userid)->first();
                if(!empty($user)){
                    if($otp===(int)$user->otp){
                        $appid = md5(microtime());
                        $user->appid = $appid;
                        $user->save();
                        $response['message'] = 'success';
                        $response['status'] = 200;
                        $response['token'] = $appid;
                        return $response;
                    }
                    $response['message'] = 'Invalid OTP';
                    $response['status'] = 300;
                    $response['userid'] = $user->id;
                    return $response;
                }else{
                    $response['message'] = 'Account not found';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'verify-user',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function verifyOtp(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                $response = array();
                $userid = $request->userid;
                $otp = (int)$request->otp;
                $user = User::withoutGlobalScope('active')->where('id',$userid)->where('status','inactive')->first();
                if(!empty($user)){
                    if($otp===(int)$user->otp){
                        $appid = md5(microtime());
                        $user->appid = $appid;
                        $user->login = 'enable';
                        $user->status = 'active';
                        $user->save();
                        $response['message'] = 'success';
                        $response['status'] = 200;
                        $response['token'] = $appid;
                        return $response;
                    }
                    $response['message'] = 'Invalid OTP';
                    $response['status'] = 300;
                    $response['userid'] = $user->id;
                    return $response;
                }else{
                    $response['message'] = 'Account not found';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'verify-user',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function resetPassword(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $token= $request->token;
            $newpassword =  trim($request->new_password);
            $oldpassword =  trim($request->old_password);
            $user = User::where('appid', $token)->first();
            try{
                $response = array();
                if(!empty($user->id)){
                    if(empty($oldpassword)) {
                        $appid = $user->appid;
                        $user->password = bcrypt($newpassword);
                        $user->save();
                        $response['message'] = 'Password Updated Successfully.';
                        $response['status'] = 200;
                        $response['token'] = $appid;
                        return $response;
                    }else{
                        if (Auth::attempt(array('id' => $user->id, 'password' => $oldpassword))){
                            $appid = $user->appid;
                            $user->password = bcrypt($newpassword);
                            $user->save();
                            $response['message'] = 'Password Updated Successfully.';
                            $response['status'] = 200;
                            $response['token'] = $appid;
                            return $response;
                        }else{
                            $response['message'] = 'Invalid Previous Password!';
                            $response['status'] = 300;
                            return $response;
                        }

                    }
                }else{
                    $response['message'] = 'Invalid User data!';
                    $response['status'] = 401;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'reset-password',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }

    public function checkPermission(Request $request){
        if (isset($request->API_KEY)) {
            $response = array();
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (!empty($request->token)) {
                    $user = User::where('appid', $request->token)->first();
                    $pem = Permission::where('name',$request->permission_name)->first();
                    if(!empty($user->id)){
                        if(!empty($pem->id)){
                            if($request->project_id == 0){
                                $ucheck = Role::where('company_id',$user->company_id)->where('name','admin')->first();
                                $urole = RoleUser::where('user_id', $user->id)->where('role_id', $ucheck->id)->first();
                                if($urole !== null){
                                    $company = Company::find($user->company_id);
                                    $package = Package::find($company->package_id);
                                    $mods = $package->module_in_package;
                                    $packageModules = Module::whereIn('module_name', (array)json_decode($mods))->get()->pluck('id')->toArray();
                                    if(in_array($pem->module_id, $packageModules)) {
                                        $response['message'] = 'true';
                                        $response['status'] = 200;
                                        return $response;
                                    }else{
                                        $response['message'] = 'You don’t have permission to perform this action.';
                                        $response['status'] = 200;
                                        return $response;
                                    }
                                }
                                $uccheck = PermissionRole::where('permission_id',$pem->id)->where('role_id',$ucheck->id)->first();
                                if($uccheck === null) {
                                    $response['message'] = 'You don’t have permission to perform this action.';
                                    $response['status'] = 200;
                                    return $response;
                                }else{
                                    $response['message'] = 'true';
                                    $response['status'] = 200;
                                    return $response;
                                }
                            }else {
                                $projectdetails = AppProject::where('id',$request->project_id)->first();
                                if(!empty($projectdetails->added_by)&&$projectdetails->added_by==$user->id){
                                    $response['message'] = 'true';
                                    $response['status'] = 200;
                                    return $response;
                                }else{
                                    $pro = ProjectPermission::where('user_id',$user->id)->where('project_id',$request->project_id)->where('permission_id',$pem->id)->first();
                                    $perm = hasPermission($request->permission_name, $user->id);
                                    if($pro === null) {
                                        $response['message'] = 'You don’t have permission to perform this action.';
                                        $response['status'] = 200;
                                        return $response;
                                    }else{
                                        $response['message'] = 'true';
                                        $response['status'] = 200;
                                        return $response;
                                    }
                                }
                            }
                        }else{
                            $response['message'] = 'You don’t have permission to perform this action.';
                            $response['status'] = 200;
                            return $response;
                        }
                    }else{
                        $response['message'] = 'Invalid User.';
                        $response['status'] = 401;
                        return $response;
                    }
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'check-permission',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Invalid Token';
            $response['status'] = 401;
            return $response;
        }else{
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function getAllPermissions(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (isset($request->token)) {
                    if (!empty($request->token)) {
                        $user = User::where('appid', $request->token)->first();
                        if($user === null){
                            $response['message'] = 'Unauthorised';
                            $response['status'] = 401;
                            return $response;
                        }
                        $userid = $request->user_id;
                        $employeeid = $request->employee_id;
                        $projectid = $request->project_id;
                        $showall = false;
                        if(!empty($projectid)){
                            $projects = AppProject::where('id',$projectid)->first();
                            if($projects->added_by==$user->id){
                                $showall = true;
                            }else{
                                $projectpermissions = ProjectPermission::where('user_id',$user->id)->where('project_id',$projectid)->pluck('permission_id')->toArray();
                                 if(!empty($projectpermissions)){

                                $modules = Module::join('permissions','permissions.module_id','=','modules.id')
                                    ->select('modules.*')->whereIn('permissions.id',$projectpermissions)->groupBy('permissions.module_id')->get();
                                $mdArray = array();
                                    if(count($modules)>0){
                                        foreach ($modules as $module){
                                            $permissions = ProjectPermission::join('permissions','permissions.id','=','project_member_permissions.permission_id')
                                                ->select('permissions.*')->where('permissions.module_id',$module->id)
                                                ->where('project_member_permissions.project_id',$projects->id)
                                                ->where('project_member_permissions.user_id',$user->id)
                                                ->whereIn('project_member_permissions.permission_id',$projectpermissions)
                                                ->whereIn('permissions.id',$projectpermissions)->get();
                                            $mdArray[] = array(
                                                'id' => $module->id,
                                                'name' => $module->module_name,
                                                'display_name' => $module->display_name,
                                                'permissions' => $permissions
                                            );
                                        }
                                    }else{
                                        $response['message'] = 'You don’t have permission to perform this action';
                                        $response['status'] = 300;
                                        return $response;
                                    }
                                }else{
                                    $response['message'] = 'You don’t have permission to perform this action';
                                    $response['status'] = 300;
                                    return $response;
                                }
                            }
                        }else{
                            $showall = true;
                        }
                        if($showall){
                            $company = Company::find($user->company_id);
                            $package = Package::find($company->package_id);
                            $mods = $package->module_in_package;
                            $notin = array("add_projects","approve_indent");
                            $arraymods = array_diff($notin,(array)json_decode($mods));
                           /* $packageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->groupBy('module_name')->pluck('module_name')->toArray();
                            whereIn('module_name', $packageModules);*/
                            $notinarray = array('clients','employees','contractors','work_category','tasks','punch_items','attendance','drawing','photo','document','products','material_request','suppliers','accounts','material_in_out');
                            $modulesData = Module::whereIn('module_name', $notinarray)->orderBy('inc','asc')->get();

                            $mdArray = array();
                            $role = Role::first();
                             foreach($modulesData as $md) {
                                $mdArray[] = array(
                                    'id' => $md->id,
                                    'name' => $md->module_name,
                                    'display_name' => $md->display_name,
                                    'permissions' => $md->permissions()->whereNotIn('name',$notin)->get()->toArray()
                                );
                            }
                        }
                        $roles = Role::where('company_id', $user->company_id)->get()->toArray();
                        $response['message'] = 'success';
                        $response['data'] = $mdArray;
                        $response['roles'] = $roles;
                        $response['status'] = 200;
                        $response['pagecount'] = pagecount();
                        return $response;
                    }
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-all-permissions',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getAllUserPermissions(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (isset($request->token)) {
                    if (!empty($request->token)) {
                        $Appuser = User::where('appid', $request->token)->first();
                        if($Appuser === null){
                            $response['message'] = 'Unauthorised';
                            $response['status'] = 401;
                            return $response;
                        }
                        $user = User::find($request->user_id);
                        if($user === null){
                            $response['message'] = 'Invalid User data!';
                            $response['status'] = 300;
                            return $response;
                        }
                        $firstAdmin = User::firstAdmin($user->company_id);
                        if($user->hasRole('admin') && $firstAdmin->id == $user->id){
                            $response['message'] = 'Role of this user cannot be changed.';
                            $response['status'] = 300;
                            return $response;
                        }
                        $roles = $user->roles()->pluck('role_id')->toArray();
                        $custom = 0;
                        $permissions = array();
                        if(in_array(0, $roles)){
                            $custom = 1;
                            $permissions = PermissionRole::where('role_id', '0_'.$user->id)->pluck('permission_id')->toArray();
                        }else{
                            foreach($roles as $role){
                                $x = array();
                                $x = PermissionRole::where('role_id', $role)->pluck('permission_id')->toArray();
                                $permissions = array_merge($permissions, $x);
                            }
                            $permissions = array_unique($permissions, SORT_NUMERIC);
                        }
                        $response['message'] = 'success';
                        $response['custom'] = $custom;
                        $response['permissions'] = $permissions;
                        $response['status'] = 200;
                        return $response;
                    }
                    $response['message'] = 'Unauthorised';
                    $response['status'] = 401;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-all-user-permissions',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getPermissionByUser(Request $request)
    {
        $response = array();
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (isset($request->token)) {
                    if (!empty($request->token)) {
                        $tokenuser = User::withoutGlobalScope('active')->where('appid', $request->token)->first();
                        if($tokenuser === null){
                            $response['message'] = 'Unauthorised';
                            $response['status'] = 401;
                            return $response;
                        }
                        $employee = Employee::find($request->employee_id);
                        if($employee === null){
                            $response['message'] = 'Unauthorised';
                            $response['status'] = 401;
                            return $response;
                        }
                        $user = User::withoutGlobalScope('active')->where('id', $employee->user_id)->first();
                        if($user === null){
                            $response['message'] = 'User Not Found';
                            $response['status'] = 300;
                            return $response;
                        }

                        $project = Project::find($request->project_id);

                        if(!empty($project->id)){
                            if(!empty($user->id)&&$project->added_by == $user->id){
                                $response['message'] = 'Role of this user cannot be changed.';
                                $response['status'] = 300;
                                return $response;
                            }
                            $projectmember = ProjectMember::where('project_id',$project->id)->where('user_id',$employee->user_id)->where('employee_id',$employee->id)->where('company_id',$employee->company_id)->first();
                            $pm = ProjectPermission::where('project_id', $project->id)->where('user_id', $employee->user_id)->get()->pluck('permission_id');
                            $response['message'] = 'success';
                            $response['custom'] = 0;
                            $response['sharetoproject'] = !empty($projectmember->share_project) ? 1 : 0;
                            $response['checkpermission'] = !empty($projectmember->check_permission) ? 1 : 0;
                            $response['level'] = !empty($projectmember->level) ? $projectmember->level : '';
                            $response['permissions'] = $pm;
                            $response['status'] = 200;
                            return $response;
                        }else{
                            $response['message'] = 'Project not Found';
                            $response['status'] = 300;
                            return $response;
                        }
                    }
                    $response['message'] = 'Unauthorised';
                    $response['status'] = 401;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-permission-by-user',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getProjectCheckPermission(Request $request)
    {
        $response = array();
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (isset($request->token)) {
                    if (!empty($request->token)) {
                        $tokenuser = User::where('appid', $request->token)->first();
                        if($tokenuser === null){
                            $response['message'] = 'Unauthorised';
                            $response['status'] = 401;
                            return $response;
                        }
                        $project = Project::find($request->project_id);
                        if(!empty($project->id)){
                            if($tokenuser->id==$project->added_by){
                                $response['checkpermission'] ='true';
                            }else{
                            $projectmember = ProjectMember::where('project_id',$project->id)->where('user_id',$tokenuser->id)->first();
                                if(!empty($projectmember)&&$projectmember->check_permission=='1'){
                                    $response['checkpermission'] ='true';
                                }else{
                                    $response['checkpermission'] ='false';
                                }
                            }
                            $response['status'] = 200;
                            return $response;
                        }else{
                            $response['message'] = 'Project not Found';
                            $response['status'] = 300;
                            return $response;
                        }
                    }
                    $response['message'] = 'Unauthorised';
                    $response['status'] = 401;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-permission-by-user',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function homePage(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (!empty($request->token)) {
                    $token = $request->token;
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $homepagearray = array();
                    $homepagearray['pagecount'] = pagecount();
                    $homepagearray['mode'] = 'true';
                    $homepagearray['errormsg'] = 'App is in development mode till 6:00 PM to 9:00 PM';
                    $banners = array();
                    $banners[]['image'] = asset_url('construction-4.jpg');
                    $banners[]['image'] = asset_url('construction-5.jpg');
                    $banners[]['image'] = asset_url('construction-6.jpg');
                    $homepagearray['images'] = $banners;
                    $proId = explode(',',$request->project_id);
                    $startdate = date('Y-m-d 00:00:00');
                    $enddate = date('Y-m-d 23:59:59');
                    $prarray = array_unique(array_filter($proId));
                    //array_push($cids, $user->company_id);
                    $punchitems = PunchItem::whereIn('projectid',$proId);
                    $indentsi = Indent::whereIn('project_id',$proId);
                    $homepagearray['pendingissuestoday'] = $punchitems->where('status','Open')->count();
                    $homepagearray['totalissuestoday'] = $punchitems->where('created_at','>=',$startdate)->where('created_at','<=',$enddate)->count();
                    $homepagearray['pendingindentstoday'] =  $indentsi->where('status','4')->count();
                    $homepagearray['totalindentstoday'] =  $indentsi->where('created_at','>=',$startdate)->where('created_at','<=',$enddate)->count();
                    $appver = AppVersion::where('medium','app')->where('platform','android')->orderBy('id','ASC')->first();
                    $homepagearray['version_code'] = !empty($appver) ? $appver->latest_version : '';
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $homepagearray['youtubelinks'] = $youtubelinks;
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $homepagearray;
                    $response['message'] = 'Status list Successfully';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'homepage',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function howToUse(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            try{
                if (!empty($request->token)) {
                    $token = $request->token;
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }

                    $youtubelinks = array();
                    $youtubelinks[] = array(
                        "title"=>"Complete Tutorial",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"m24CjND2Tgs",
                                "thumbnail"=>"https://i.ytimg.com/vi/m24CjND2Tgs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLA6aWjlOVG407KN3ruF86F-5invsQ",
                                "title"=>"Aakar360 project complete tutorial",
                                "link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs"
                            ],
                            [
                                "youtubeid"=>"IR-QrykhJ5A",
                                "thumbnail"=>"https://i.ytimg.com/vi/IR-QrykhJ5A/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLBm59PYpUC2IaAWS6OBH-EZft_lbw",
                                "title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल",
                                "link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A"
                            ]
                        ),
                    );
                    $youtubelinks[] = array(
                        "title"=>"Projects",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"MgHv7sw6IHM",
                                "thumbnail"=>"https://i.ytimg.com/vi/MgHv7sw6IHM/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAIE7OzWadyV2o8sGg9HrU_0lzihA",
                                "title"=>"How to create project?",
                                "link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM"
                            ],
                            [
                                "youtubeid"=>"qtzHGj14UVs",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"प्रोजेक्ट कैसे बनाएं?",
                                "link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs"
                            ]
                        )
                    );
                    $youtubelinks[] = array(
                        "title"=>"Task and Activity",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"IoeKsvTlHPY",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"How to make activity and task?",
                                "link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY"
                            ],
                            [
                                "youtubeid"=>"kfZcZgHpR2w",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"एक्टिविटी और टास्क कैसे बनाये?",
                                "link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w"
                            ]
                        )
                    );
                    $youtubelinks[] = array(
                        "title"=>"Issues",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"WfEInJJzeJM",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"How to raise an issue?",
                                "link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM"
                            ],
                            [
                                "youtubeid"=>"03jIjsgP7_M",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"इशू कैसे रेज करें?",
                                "link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M"
                            ]
                        )
                    );
                    $youtubelinks[] = array(
                        "title"=>"Manpower Log",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"wjb_fl7xB_I",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"How to enter manpower log?",
                                "link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I"
                            ],
                            [
                                "youtubeid"=>"3L4MhmTYz04",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"मैनपावर लॉग कैसे दर्ज करें?",
                                "link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s"
                            ]
                        )
                    );
                    $youtubelinks[] = array(
                        "title"=>"Indent",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"t0MrVrnCGpY",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"How to raise indent?",
                                "link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY"
                            ],
                            [
                                "youtubeid"=>"kVsH5lOR0Ao",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"इंडेंट कैसे बनाये?",
                                "link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao"
                            ]
                        )
                    );
                    $youtubelinks[] = array(
                        "title"=>"Invite Member",
                        "listitem"=>array(
                            [
                                "youtubeid"=>"bBI2ZLK83gg",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"How to invite team member?",
                                "link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg"
                            ],
                            [
                                "youtubeid"=>"bBI2ZLK83gg",
                                "thumbnail"=>"https://i.ytimg.com/vi/qtzHGj14UVs/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB88oPcFpuEjbuhSHrl2WPRJuguwA",
                                "title"=>"टीम मेंबर को कैसे जोड़ें?",
                                "link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM"
                            ]
                        )
                    );
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $youtubelinks;
                    $response['message'] = 'Status list Successfully';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'homepage',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function youtubeLinks(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $token = $request->token;
            $user = User::where('appid', $token)->first();
            try{
                if (!empty($request->token)) {
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $homepagearray = array();
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $homepagearray['projects'] = $youtubelinks;
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $homepagearray['issues'] = $youtubelinks;
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $homepagearray['manpowerlogs'] = $youtubelinks;
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $homepagearray['indents'] = $youtubelinks;
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"How to invite team member?","link"=>"https://www.youtube.com/watch?v=bBI2ZLK83gg");
                    $youtubelinks[] = array("youtubeid"=>"bBI2ZLK83gg","title"=>"टीम मेंबर को कैसे जोड़ें?","link"=>"https://www.youtube.com/watch?v=5Losnf2jjZM");
                    $youtubelinks[] = array("youtubeid"=>"t0MrVrnCGpY","title"=>"How to raise indent?","link"=>"https://www.youtube.com/watch?v=t0MrVrnCGpY");
                    $youtubelinks[] = array("youtubeid"=>"kVsH5lOR0Ao","title"=>"इंडेंट कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kVsH5lOR0Ao");
                    $youtubelinks[] = array("youtubeid"=>"MgHv7sw6IHM","title"=>"How to create project?","link"=>"https://www.youtube.com/watch?v=MgHv7sw6IHM");
                    $youtubelinks[] = array("youtubeid"=>"qtzHGj14UVs","title"=>"प्रोजेक्ट कैसे बनाएं?","link"=>"https://www.youtube.com/watch?v=qtzHGj14UVs");
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $youtubelinks[] = array("youtubeid"=>"WfEInJJzeJM","title"=>"How to raise an issue?","link"=>"https://www.youtube.com/watch?v=WfEInJJzeJM");
                    $youtubelinks[] = array("youtubeid"=>"03jIjsgP7_M","title"=>"इशू कैसे रेज करें?","link"=>"https://www.youtube.com/watch?v=03jIjsgP7_M");
                    $youtubelinks[] = array("youtubeid"=>"wjb_fl7xB_I","title"=>"How to enter manpower log?","link"=>"https://www.youtube.com/watch?v=wjb_fl7xB_I");
                    $youtubelinks[] = array("youtubeid"=>"3L4MhmTYz04","title"=>"मैनपावर लॉग कैसे दर्ज करें?","link"=>"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s");
                    $youtubelinks[] = array("youtubeid"=>"m24CjND2Tgs","title"=>"Aakar360 project complete tutorial","link"=>"https://www.youtube.com/watch?v=m24CjND2Tgs");
                    $youtubelinks[] = array("youtubeid"=>"IR-QrykhJ5A","title"=>"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link"=>"https://www.youtube.com/watch?v=IR-QrykhJ5A");
                    $homepagearray['memberpage'] = $youtubelinks;
                    $youtubelinks = array();
                    $youtubelinks[] = array("youtubeid"=>"IoeKsvTlHPY","title"=>"How to make activity and task?","link"=>"https://www.youtube.com/watch?v=IoeKsvTlHPY");
                    $youtubelinks[] = array("youtubeid"=>"kfZcZgHpR2w","title"=>"एक्टिविटी और टास्क कैसे बनाये?","link"=>"https://www.youtube.com/watch?v=kfZcZgHpR2w");
                    $homepagearray['boqlist'] = $youtubelinks;
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $homepagearray;
                    $response['message'] = 'Status list Successfully';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'youtube-links',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function appLogs(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $token = $request->token;
            $user = User::where('appid', $token)->first();
            try{
                $appLogs = new AppLogs();
                $appLogs->user_id = $user->id;
                $appLogs->api_name = $request->api_name;
                $appLogs->status = $request->status;
                $appLogs->message = $request->message;
                $appLogs->line = $request->line;
                $appLogs->file = $request->file;
                $appLogs->medium = $request->medium ?: 'android';
                $appLogs->save();
                $response = array();
                $response['status'] = 200;
                $response['message'] = 'Log Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
    public function introSliders(Request $request){
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $token = $request->token;
            $user = User::where('appid', $token)->first();
            try{
                $introlist = array();
                $introslidersarray = IntroSliders::where('status','1')->get();
                if(count($introslidersarray)>0){
                    foreach ($introslidersarray as $introsliders){
                        $introsli = array();
                        $introsli['id'] = $introsliders->id;
                        $introsli['title'] = $introsliders->title;
                        $introsli['image'] = !empty($introsliders->image) ? uploads_url().'introslides/'.$introsliders->image : '';
                        $introlist[] = $introsli;
                    }
                }
                $appver = AppVersion::where('medium','app')->where('platform','android')->orderBy('id','ASC')->first();

                $response = array();
                $response['status'] = 200;
                $response['message'] = 'Intro Slides';
                $response['responselist'] = $introlist;
                $response['version_code'] = !empty($appver) ? $appver->latest_version : '';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $response['message'] = 'Unauthorised';
            $response['status'] = 401;
            return $response;
        }
    }
}