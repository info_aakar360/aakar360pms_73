<?php

namespace App\Http\Controllers\Api;

use App\AppBom;
use App\AppIndent;
use App\AppLogs;
use App\AppProductIssue;
use App\AppProductLog;
use App\AppProject;
use App\AppPurchaseInvoice;
use App\AppStock;
use App\AppStore;
use App\Company;
use App\Http\Controllers\Controller;
use App\Indent;
use App\IndentProducts;
use App\IndentsFiles;
use App\IndentsReply;
use App\PiProducts;
use App\Product;
use App\ProductCategory;
use App\ProductCategoryFiles;
use App\ProductFiles;
use App\ProductIssueFilesReturn;
use App\ProductIssueReturn;
use App\ProductLog;
use App\ProductReturns;
use App\ProjectsLogs;
use App\PurchaseInvoice;
use App\Stock;
use App\Units;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;

class AppStoreController extends Controller
{
    use AuthenticatesUsers;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function unitsList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $projects = AppProject::find($projectid);
                if(empty($projects)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $page = $request->page;
                $units = Units::where('company_id',$projects->company_id);
                $response = array();
                if($page=='all'){
                    $units = $units->get()->toArray();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $units = $units->offset($skip)->take($count)->get()->toArray();
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'units-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function indentsList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $project = explode(',',$request->project_id);
                $response = array();
                $status = $request->status;
                $page = $request->page;
                $indents = AppIndent::whereIn('project_id', $project);
                if(!empty($status)){
                    $indents = $indents->where('status',$status);
                }
                if(!empty($request->fromdate)){
                    $fromdate = date('Y-m-d',strtotime($request->fromdate));
                    $indents = $indents->where('created_at','>=',$fromdate.' 00:00:00');
                }
                if(!empty($request->todate)){
                    $todate =  date('Y-m-d',strtotime($request->todate));
                    $indents = $indents->where('created_at','<=',$todate.' 23:59:59');
                }
                if($page=='all'){
                    $indents = $indents->orderBy('id','DESC')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $indents = $indents->offset($skip)->take($count)->orderBy('id','DESC')->get();
                }
                if(count($indents)>0){
                    foreach ($indents as $inden){
                        $grn = AppPurchaseInvoice::where('indent_id', $inden->id)->pluck('id')->toArray();
                        $totalquant = IndentProducts::where('indent_id', $inden->id)->sum('quantity');
                        $indentproducts = IndentProducts::where('indent_id', $inden->id)->pluck('cid')->toArray();
                        $productsname =  Product::whereIn('id', $indentproducts)->pluck('name')->toArray();
                        $receviedqt = PiProducts::whereIn('pi_id',$grn)->whereIn('product_id',$indentproducts)->sum('quantity');
                        $indstatus = 'Pending';
                        $inden->status = '4';
                        if(!empty($totalquant)&&!empty($receviedqt)&&$receviedqt>=$totalquant){
                            $indstatus = 'Received';
                            $inden->status = '5';
                            $inden->save();
                        }
                        $proData[] = array(
                            'id' => $inden->id,
                            'indent_no' => $inden->indent_no,
                            'project_id' => $inden->project_id,
                            'project_name' => get_project_name($inden->project_id),
                            'product_name' => !empty($productsname) ? implode(', ',$productsname) : '',
                            'added_by' => get_user_name($inden->added_by),
                            'remark' => !empty($inden->remark) ? $inden->remark : '',
                            'status' => $indstatus,
                            'grn_converted' => !empty($grn) ? '1' : '0',
                            'created_at' => Carbon::parse($inden->created_at)->format('d M Y'),
                        );
                    }

                }else{
                    $proData = array();
                }
                $response['status'] = 200;
                $response['message'] = 'Indents List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function indentProducts(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $page = $request->page;
                $productid = $request->product_id;
                $indent = AppIndent::where('id', $request->indent_id)->first();
                $purInvoices = AppPurchaseInvoice::where('indent_id', $request->indent_id)->pluck('id')->toArray();
                $indents = IndentProducts::where('indent_id', $request->indent_id);
                if(!empty($productid)){
                    $indents = $indents->where('cid',$productid);
                }
                if($page=='all'){
                    $indents = $indents->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $indents = $indents->offset($skip)->take($count)->get();
                }

                foreach ($indents as $project){
                    if(!empty($purInvoices)) {
                        $piProduct = PiProducts::whereIn('pi_id', $purInvoices)->where('product_id', $project->cid)->sum('quantity');
                    }
                    $proData[] = array(
                        'id' => $project->id,
                        'product_id' => $project->cid,
                        'cid' => get_local_product_name($project->cid),
                        'bid' => get_pbrand_name($project->bid),
                        'quantity' => $project->quantity,
                        'already_received_quantity' => (!empty($piProduct)) ? (string)$piProduct : '0',
                        'unit_id' => $project->unit,
                        'unit' => get_unit_name($project->unit),
                        'expected_date' => !empty($project->expected_date) ? Carbon::parse($project->expected_date)->format('d M Y') : '',
                        'created_at' => Carbon::parse($project->created_at)->format('d-m-Y'),
                        'remarks' => $project->remarks,
                    );
                }
                $status_name = array();
                /*$status_name[] = array(  'id' => '0', 'name' => 'Pending RFQ');
                $status_name[] = array(  'id' => '1', 'name' => 'Pending PO');
                $status_name[] = array(  'id' => '2', 'name' => 'Pending Purchase');
                $status_name[] = array(  'id' => '3', 'name' => 'Purchase Done');*/
                $status_name[] = array(  'id' => '4', 'name' => 'Pending');
                $status_name[] = array(  'id' => '5', 'name' => 'Received');

                $grn = AppPurchaseInvoice::where('indent_id', $indent->id)->pluck('id')->toArray();
                $totalquant = IndentProducts::where('indent_id', $indent->id)->sum('quantity');
                $indentproducts = IndentProducts::where('indent_id', $indent->id)->pluck('cid')->toArray();
                $productsname =  Product::whereIn('id', $indentproducts)->pluck('name')->toArray();
                $receviedqt = PiProducts::whereIn('pi_id',$grn)->whereIn('product_id',$indentproducts)->sum('quantity');
                $indstatus = 'Pending';
                $indent->status = '4';
                if(!empty($totalquant)&&!empty($receviedqt)&&$receviedqt>=$totalquant){
                    $indstatus = 'Received';
                    $indent->status = '5';
                    $indent->save();
                }
                $indentData = array(
                    'id' => $indent->id,
                    'indent_no' => $indent->indent_no,
                    'project_id' => $indent->project_id,
                    'project_name' => get_project_name($indent->project_id),
                    'product_name' => !empty($productsname) ? implode(', ',$productsname) : '',
                    'added_by' => get_user_name($indent->added_by),
                    'remark' => !empty($indent->remark) ? $indent->remark : '',
                    'status' => $indstatus,
                    'grn_converted' => !empty($grn) ? '1' : '0',
                    'created_at' => Carbon::parse($indent->created_at)->format('d M Y'),
                );

                $response['status'] = 200;
                $response['message'] = 'Indent Products List Fetched';
                $response['indents'] = $indentData;
                $response['response'] = $proData;
                $response['status_name'] = $status_name;
                $response['indent_status'] = $indent->status;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indent-products',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }


    public function appProductCategory(Request $request)
    {
        Log::info($request->all());
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $companyid = $user->company_id;
                if(!empty($request->project_id)){
                    $projectid = $request->project_id;
                    $projects = AppProject::find($projectid);
                    if(empty($projects)){
                        $response['status'] = 301;
                        $response['message'] = 'Project Not found';
                        return $response;
                    }
                    $companyid = $projects->company_id;
                }

                $response = array();
                $products = ProductCategory::where('company_id', $companyid)->orderBy('id','asc')->get();
                $response['status'] = 200;
                $response['message'] = 'Material List Fetched';
                $response['response'] = $products;
                return $response;
            } catch (\Exception $e) {
                $medium = $request->medium ?: 'api';
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'product-category-list';
                $logs->medium = $medium;
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function productDetailsCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $companyid = $user->company_id;
                if(!empty($request->project_id)){
                    $projectid = $request->project_id;
                    $projects = AppProject::find($projectid);
                    if(empty($projects)){
                        $response['status'] = 301;
                        $response['message'] = 'Project Not found';
                        return $response;
                    }
                    $companyid = $projects->company_id;
                }
                $response = array();
                $productscat = ProductCategory::where('company_id', $companyid)->where('id', $request->categoryid)->first();
                $productscat->image_url = get_product_category_image_link($productscat->id);
                if(empty($productscat)){
                    $response['status'] = 301;
                    $response['message'] = 'Category Not found';
                    return $response;
                }
                $response['status'] = 200;
                $response['message'] = 'Material Category Fetched';
                $response['response'] = $productscat;
                return $response;
            } catch (\Exception $e) {
                $medium = $request->medium ?: 'api';
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'product-category-details';
                $logs->medium = $medium;
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createProductCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $categoryid = $request->categoryid;
                $companyid = $user->company_id;
                if(!empty($request->project_id)){
                    $projectid = $request->project_id;
                    $projects = AppProject::find($projectid);
                    if(empty($projects)){
                        $response['status'] = 301;
                        $response['message'] = 'Project Not found';
                        return $response;
                    }
                    $companyid = $projects->company_id;
                }
                $response = array();
                $name = $request->name;
                if (!empty($categoryid)) {
                    $productcategory = ProductCategory::where('company_id', $companyid)->where('id', $categoryid)->first();

                } else {
                    $productcategory = ProductCategory::where('company_id', $companyid)->where('name', $name)->first();
                    if (!empty($productcategory->id)) {
                        $response['status'] = 301;
                        $response['message'] = 'Category already exists';
                        return $response;
                    }
                    $productcategory = new ProductCategory();
                    $productcategory->company_id = $companyid;
                    $productcategory->added_by = $user->id;
                }
                $medium = !empty($request->medium) ? $request->medium : 'android';
                $productcategory->name = $name;
                $productcategory->save();

                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/product-category-files/'.$productcategory->id.'/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $filename= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('product-category-files/'.$productcategory->id.'/', $request->image, $image->hashName(), 'public');
                            $filename= $image->hashName();
                            break;
                    }
                    $productcategory->image = $filename;
                    $productcategory->save();
                }

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  ProductCategoryFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->productcategory_id =  $productcategory->id;
                            $productimage->save();
                            $sourcepath = 'uploads/product-category-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/product-category-files/'.$productcategory->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/product-category-files/'.$productcategory->id)) {
                                    mkdir('uploads/product-category-files/'.$productcategory->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }

                $createlog = new ProjectsLogs();
                $createlog->company_id = $companyid;
                $createlog->added_id = $user->id;
                $createlog->module_id = $productcategory->id;
                $createlog->module = 'product_category';
                $createlog->medium = $medium;
                $createlog->heading =  'Material Category Created';
                $createlog->description = 'Material Category log created by ' . $user->name;
                $createlog->save();

                $response['status'] = 200;
                $response['category_id'] = $productcategory->id;
                if (!empty($categoryid)) {
                    $response['message'] = 'Material Category Updated Successfully';
                }else{
                    $response['message'] = 'Material Category Created Successfully';
                }
                return $response;

            } catch (\Exception $e) {
                $medium = !empty($request->medium) ? $request->medium : 'android';
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-product-ccategory',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeProductCategoryImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $punchitemid = $request->categoryid;
                $punchitem = ProductCategory::find($punchitemid);
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                    $storage = storage();
                    $companyid = $user->company_id;
                    $file = new ProductCategoryFiles();
                    $file->company_id = $companyid;
                    $file->user_id = $user->id;
                    $file->productcategory_id = $punchitemid ?: 0;
                    switch($storage) {
                        case 'local':

                            $destinationPath = 'uploads/product-category-files/'.$punchitemid;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            $filename = $fileData->hashName();
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/product-category-files/'.$punchitemid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'product-category-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('product-category-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $punchitemid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$punchitemid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $punchitemid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('product-category-files/'.$punchitemid.'/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/product-category-files/'.$punchitemid.'/'.$fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Issue Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-product-category-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteProductCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $manpowerarray = array_filter(explode(',', $request->categoryid));
                foreach ($manpowerarray as $manpower) {
                    $task = ProductCategory::findOrFail($manpower);
                    ProjectsLogs::where('module_id',$task->id)->where('module_id','product_category')->delete();
                    $task->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Material Category Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $medium = !empty($request->medium) ? $request->medium : 'android';
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-product-category',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function productList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $issueId = $request->issue_id;
                $projectId = AppProject::find($request->project_id);
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                $page = $request->page;
                if($page=='all'){
                    $productsarray = Product::where('company_id',$projectId->company_id)->where('project_id',$projectId->id)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $productsarray = Product::where('company_id',$projectId->company_id)->where('project_id',$projectId->id)->offset($skip)->take($count)->get();
                }
                $proData = array();
                foreach ($productsarray as $project){
                     update_product_stock($projectId->id,$storeId->id,$project->id);
                    $stockId = AppStock::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                        ->where('cid', $project->id)
                        ->orderBy('id', 'DESC')->first();
                    $check = AppBom::where('project_id', $projectId->id)
                        ->where('store_id', $storeId->id)
                        ->where('product_id', $project->id)
                        ->where('unit_id', $project->unit_id)
                        ->orderBy('id','DESC')
                        ->first();
                    $proData[] = array(
                        'id' => $project->id,
                        'name' => $project->name,
                        'unit_id' => $project->unit_id,
                        'unit_name' => get_unit_name($project->unit_id),
                        'category_id' => $project->category_id,
                        'category_name' => get_product_category_name($project->category_id),
                        'category_image' => get_product_category_image_link($project->category_id),
                        'opening_stock' => (!empty($check->opening_stock)) ? $check->opening_stock : 0,
                        'est_qty' => (!empty($check->est_qty)) ? $check->est_qty : 0,
                        'est_rate' => (!empty($check->est_rate)) ? $check->est_rate : 0,
                        'closing_stock' => (!empty($stockId->stock)) ? $stockId->stock : 0,
                        'available_qty' => (!empty($stockId->stock)) ? $stockId->stock : 0,
                        'total_issue_return_qty' =>  0,
                        'image_url' => get_product_image_link($project->id),
                    );
                }
                $response = array();
                $response['status'] = 200;
                $response['message'] = 'Material List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'product-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function productDetail(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $html = array();
                $product = Product::where('id',$request->id)->first();
                if(empty($product)){
                    $response['message'] = "Material Not found.";
                    $response['status'] = 300;
                    return $response;
                }
                $html['product'] = array(
                    'id' => $product->id,
                    'name' => $product->name,
                    'unit_id' => $product->unit_id,
                    'unit_name' => get_unit_name($product->unit_id),
                    'unit_symbol' => get_unit_symbol($product->unit_id),
                    'category_id' => $product->category_id,
                    'category_name' => get_product_category_name($product->category_id),
                    'opening_stock' => account_numberformat($product->opening_stock),
                );
                $store = AppStore::where('project_id', $request->project_id)->first();

                $totalIssued = 0;
                $totalReceived = 0;
                $totalBalance = 0;
                $tableData = '';
                $iData = array();

                $stockId = AppStock::where('project_id',$request->project_id)->where('store_id',$store->id)
                    ->where('cid', $product->id)
                    ->orderBy('id', 'DESC')->first();
                $openingstock = AppProductLog::where('project_id', $request->project_id)->where('store_id', $store->id)
                    ->where('product_id', $product->id)->where('module_name','opening_stock')
                    ->first();
                if(!empty($openingstock)) {
                    $iData[] = array(
                        'id'=> $openingstock->id,
                        'date'=>Carbon::parse($openingstock->created_at)->format('d-M-Y'),
                        'module_id'=> $openingstock->module_id,
                        'module_name'=> $openingstock->module_name,
                        'supplier_id'=>0,
                        'supplier_name'=>'',
                        'amount'=> 0,
                        'transaction_type'=> 'Opening Stock Updated ',
                        'created_by' => get_user_name($openingstock->created_by),
                        'remark' => (!empty($openingstock) && !empty($openingstock->remark)) ? $openingstock->remark : '',
                        'issue'=>($openingstock->transaction_type == 'minus') ? (string)$openingstock->quantity : '-',
                        'receive' => (!empty($openingstock) && $openingstock->transaction_type == 'plus') ? (string)$openingstock->quantity : '-',
                        'balance' => (!empty($openingstock) && $openingstock->transaction_type == 'plus') ? (string)$openingstock->quantity : '-',
                    );
                    $totalReceived += (float)$openingstock->quantity;
                }

                $logs = AppProductLog::where('project_id', $request->project_id)->where('store_id', $store->id)
                    ->where('product_id', $product->id)->where('module_name','<>','opening_stock')
                    ->orderBy('date', 'ASC')->get();

                $bal = !empty($openingstock->quantity) ? (float)$openingstock->quantity : 0;


                foreach ($logs as $key=>$log) {

                    if ($log->transaction_type == 'plus') {
                        $totalReceived += (float)$log->quantity;
                        $bal = $bal+(float)$log->quantity;
                    } elseif ($log->transaction_type == 'minus') {
                        $totalIssued += (float)$log->quantity;
                        $bal = $bal-(float)$log->quantity;
                    }
                    $log->balance_quantity  = $bal;
                    $log->save();

                    $supplierid =  $amount = 0;
                        $ttype = '';
                    if($log->module_name=='update-stock'&&$log->transaction_type=='minus'){
                        $ttype = 'Shortage';
                    }elseif($log->module_name=='update-stock'&&$log->transaction_type=='plus'){
                        $ttype = 'Excess';
                    }elseif($log->module_name=='product_issue'&&$log->transaction_type=='minus'){
                        $ttype = 'Material Issue';
                    }elseif($log->module_name=='product_issue_return'&&$log->transaction_type=='plus'){
                        $ttype = 'Material Issue Return';
                    }elseif($log->module_name=='grn'&&$log->transaction_type=='plus'){
                        $purhasesupplier = PurchaseInvoice::where('id',$log->module_id)->first();
                        $supplierid =  !empty($purhasesupplier) ? $purhasesupplier->supplier_id : 0;
                        $purhasereturn = PiProducts::where('pi_id',$log->module_id)->where('product_id',$log->product_id)->first();
                        $amount =  !empty($purhasereturn) ? $purhasereturn->price : 0;
                        $ttype = 'Material Received';
                    }elseif($log->module_name=='grn_return'&&$log->transaction_type=='minus'){
                        $purhasereturn = ProductReturns::where('id',$log->module_id)->first();
                        $supplierid =  !empty($purhasereturn) ? $purhasereturn->supplier_id : 0;
                        $amount =  !empty($purhasereturn) ? $purhasereturn->price : 0;
                        $ttype = 'Purchase Return';
                    }
                    $iData[] = array(
                        'id'=> $log->id,
                        'date'=> !empty($log->date) ? Carbon::parse($log->date)->format('d-M-Y') : '',
                        'transaction_type'=> $ttype,
                        'supplier_id'=>$supplierid,
                        'supplier_name'=>get_employee_name($supplierid),
                        'amount'=> $amount,
                        'module_id'=> $log->module_id,
                        'module_name'=> $log->module_name,
                        'created_by'=>get_user_name($log->created_by),
                        'remark'=>(!empty($log->remark)) ? $log->remark : '',
                        'issue'=>($log->transaction_type == 'minus') ? (string)$log->quantity : '-',
                        'receive'=>($log->transaction_type == 'plus') ? (string)$log->quantity : '-',
                        'balance'=> account_numberformat($bal),
                        'created_date'=>Carbon::parse($log->created_at)->format('d-M-Y'),
                    );
                }
                $cstock = array();
                if($stockId) {

                    /* Update Closing stock here*/
                    $stockId->stock = $bal;
                    $stockId->save();

                    /*$cstock[] = array(
                        'date' => '-',
                        'transaction_type' => '-',
                        'module_id'=> $stockId->id,
                        'module_name'=> 'stock',
                        'supplier_id'=>0,
                        'supplier_name'=>'',
                        'amount'=> 0,
                        'created_by' => '-',
                        'remark' => 'Closing Stock',
                        'issue' => '-',
                        'receive' => (!empty($stockId)) ? (string)$stockId->stock : '0',
                        'balance' => '-',
                    );*/
                }
                $totalBalance = $totalReceived-$totalIssued;
                /*$html['data'] = array_merge($ostock,$iData,$cstock);*/
                $html['data'] = array_reverse($iData);
                $html['openingStock'] = (!empty($openingstock->quantity)) ? account_numberformat($openingstock->quantity) : '0';
                $html['closingBalance'] = (!empty($stockId->stock)) ? account_numberformat($stockId->stock) : '0';
                $html['stock_id'] = (!empty($stockId->id)) ? $stockId->id : '';
                $html['totalIssued'] = account_numberformat($totalIssued);
                $html['totalReceived'] = account_numberformat($totalReceived);
                $html['totalBalance'] = account_numberformat($totalBalance);
                $response = array();
                $response['status'] = 200;
                $response['message'] = 'Material List Fetched';
                $response['response'] = $html;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'product-detail',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function issueProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectId = Project::find($request->project_id);
                $storeId = AppStore::where('project_id',$request->project_id)->orderBy('id','ASC')->first();
                $productissueuni = AppProductIssue::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
                $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                $unique_id = 'MI-'.date("d-m-Y").'-'.$newid;
                $response = array();
                if ($request->quantity !== '') {
                    $scheck = AppStock::where('cid', $request->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                    $bal = 0;
                    if (empty($scheck)) {
                        $response['status'] = 300;
                        $response['message'] = 'Material Stock Not Available';
                        return $response;
                    }
                    if($scheck->stock<$request->quantity){
                        $response['status'] = 300;
                        $response['message'] = 'Available Material stock is '.$scheck->stock;
                        return $response;
                    }
                    if ($request->quantity > $scheck->stock) {
                        $response['status'] = 200;
                        $response['message'] = 'Issued Quantity greater then available stock';
                        return $response;
                    } else {
                        $bal = $scheck->stock - $request->quantity;
                        $scheck->stock = $bal;
                        $scheck->save();
                    }
                    $pro = new AppProductIssue();
                    $pro->unique_id = $unique_id;
                    $pro->project_id = $request->project_id;
                    $pro->company_id = $projectId->company_id;
                    $pro->store_id = $storeId->id;
                    $pro->product_id = $request->product_id;
                    $pro->quantity = $request->quantity;
                    $pro->unit_id = $request->unit_id;
                    $pro->date = Carbon::parse($request->date)->format('Y-m-d');;
                    $pro->remark = $request->remark;
                    $pro->issued_by = $user->id;
                    $pro->save();

                    if ($pro) {
                        $pl = new AppProductLog();
                        $pl->store_id = $storeId->id;
                        $pl->project_id = $request->project_id;
                        $pl->company_id = $projectId->company_id;
                        $pl->created_by = $user->id;
                        $pl->module_id = $pro->id;
                        $pl->module_name = 'product_issue';
                        $pl->product_id = $request->product_id;
                        $pl->quantity = $request->quantity;
                        $pl->balance_quantity = $bal;
                        $pl->transaction_type = 'minus';
                        $pl->remark = $request->remark;
                        $pl->save();
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Material issued Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'product-issue',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function issueProductReturn(Request $request){
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }

                $product = Product::where('id',$request->product_id)->orderBy('id','DESC')->first();
                if(empty($product)){
                    $response['status'] = 301;
                    $response['message'] = 'Material Not found';
                    return $response;
                }
                $qty = $request->quantity;

                $issueuniqueid = '';
                $issueqty = AppProductIssue::where('store_id', $storeId->id)->where('project_id', $projectId->id)->where('product_id',$product->id)->sum('quantity');

                if(!empty($issueqty)){
                    $returedquanity = ProductIssueReturn::where('store_id', $storeId->id)->where('project_id', $projectId->id)->where('product_id',$product->id)->sum('qty');
                    $returedquanity = $returedquanity+$qty;
                    if($issueqty<$returedquanity){
                        $response['status'] = 301;
                        $response['message'] = $product->name.' Material return quantity is greater than issued';
                        return $response;
                    }
                }
                $newqty = $qty;
                $issuequeryarray = array();
                $x=1;
                while ($x==1){
                    $returnqtyvalue = 0;
                    $prissue = AppProductIssue::where('store_id', $storeId->id)->where('project_id', $projectId->id)->where('product_id',$product->id)->where('returned','0')->first();
                   if(!empty($prissue)){
                        $returedquanity = ProductIssueReturn::where('store_id', $storeId->id)->where('project_id', $projectId->id)->where('issue_id',$prissue->id)->sum('qty');
                         $returnqtyvalue +=$returedquanity;
                          $returnqtyvalue +=$newqty;
                          $oldqty = $prissue->quantity;
                        if($returnqtyvalue>=$oldqty){
                            $balquant = $oldqty-$returedquanity;
                            $issuequeryarray[$prissue->id] = $balquant;
                            $newqty = $newqty-$balquant;
                            if($newqty<=0){
                                if($newqty==0){
                                    $prissue->returned = '1';
                                    $prissue->save();
                                    $x=0;
                                }
                                $x=0;
                            }
                           $prissue->returned = '1';
                            $prissue->save();
                        }else{
                            $balqty = $oldqty-$newqty;
                            if($balqty>0){
                                $issuequeryarray[$prissue->id] = $newqty;
                                $newqty = 0;
                                if($newqty<=0){
                                    $x=0;
                                }
                            }elseif($balqty==0){
                                $prissue->returned = '1';
                                $prissue->save();
                                $newqty = 0;
                                if($newqty<=0){
                                    $x=0;
                                }
                            }
                        }
                    }else{
                        $x = 0;
                    }
                }
                if(empty($prissue)){
                    $response['status'] = 301;
                    $response['message'] = 'Material Issue Not Found';
                    return $response;
                }
                foreach ($issuequeryarray as $issueid => $qty){

                $productissueuni = ProductIssueReturn::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
                $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                $unique_id = 'Prod-MIR-'.date("d-m-Y").'-'.$newid;
                $response = array();
                    if (!empty($qty)) {
                        $scheck = AppStock::where('cid', $product->id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                        $bal = 0;
                        if (!empty($scheck)) {
                            $bal = $scheck->stock + $qty;
                            $scheck->stock = $bal;
                            $scheck->save();
                            $pro = new ProductIssueReturn();
                            $pro->unique_id = $unique_id;
                            $pro->project_id = $projectId->id;
                            $pro->company_id = $projectId->company_id;
                            $pro->store_id = $storeId->id;
                            $pro->issue_id = $issueid;
                            $pro->unit_id = !empty($prissue->unit_id) ? $prissue->unit_id : '0';
                            $pro->product_id = $product->id;
                            $pro->qty = $qty;
                            $pro->remark = $request->remark;
                            $pro->date = !empty($request->date) ? Carbon::parse($request->date)->format('Y-m-d') : '';
                            $pro->added_by = $user->id;
                            $pro->inc = $newid;
                            $pro->save();

                            if(!empty($request->images)){
                                $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                                foreach($productimagsarry as $productimags){
                                    $productimage =  ProductIssueFilesReturn::find($productimags);
                                    if(!empty($productimage)){
                                        $productimage->return_id =  $pro->id;
                                        $productimage->save();
                                        $sourcepath = 'uploads/product-issue-return-files/0/'.$productimage->hashname;
                                        $destinationPath = 'uploads/product-issue-return-files/'.$pro->id.'/'.$productimage->hashname;
                                        if(file_exists($sourcepath)){
                                            if (!file_exists('uploads/product-issue-return-files/'.$pro->id)) {
                                                mkdir('uploads/product-issue-return-files/'.$pro->id, 0777, true);
                                            }
                                            rename($sourcepath,$destinationPath);
                                        }
                                    }
                                }
                            }
                            if ($pro) {
                                $pl = new AppProductLog();
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $projectId->id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pro->id;
                                $pl->module_name = 'product_issue_return';
                                $pl->product_id = $product->id;
                                $pl->quantity = $qty;
                                $pl->balance_quantity = $bal;
                                $pl->transaction_type = 'plus';
                                $pl->remark = $request->remark;
                                $pl->save();
                            }
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Material returned Successfully';
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'add-product-issue-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }

        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projects = AppProject::find($projectid);
                if(empty($projects)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $name = $request->name;
                $prevfile = Units::where('company_id',$projects->company_id)->where('name',$name)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'Unit name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                $unit = new Units();
                $unit->company_id = $projects->company_id;
                $unit->name = $request->name;
                $unit->symbol = $request->symbol;
                $unit->added_by = $user->id;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Unit Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createIndent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $sr = 1;
                $ind = AppIndent::orderBy('id', 'DESC')->first();
                if($ind !== null){
                    $in = explode('/', $ind->indent_no);
                    $sr = $in[2];
                    $sr++;
                }
                if($request->store_id == 0) {
                    $store = AppStore::where('project_id', $request->project_id)->first();
                    if($store == null){
                        $response['status'] = 200;
                        $response['message'] = 'No store assigned for this project.';
                        return $response;
                    }else {
                        $indent_no = 'IND/' . date("Y") . '/' . $sr;
                        $indent = new AppIndent();
                        $indent->company_id = $user->company_id;
                        $indent->indent_no = $indent_no;
                        $indent->store_id = $store->id;
                        $indent->project_id = $request->project_id;
                        $indent->status = '4';
                        $indent->added_by = $user->id;
                        $indent->save();
                    }
                }else{
                    $store = AppStore::where('project_id', $request->project_id)->first();
                    $indent_no = 'IND/' . date("Y") . '/' . $sr;
                    $indent = new AppIndent();
                    $indent->company_id = $user->company_id;
                    $indent->indent_no = $indent_no;
                    $indent->store_id = $request->store_id;
                    $indent->project_id = $request->project_id;
                    $indent->status = '4';
                    $indent->added_by = $user->id;
                    $indent->save();
                }

                $ex = json_decode($request->indent);
                foreach ($ex as $data){
                    $indPro = new IndentProducts();
                    $indPro->indent_id = $indent->id;
                    $indPro->cid = $data->product_id;
                    $indPro->unit = $data->unit_id;
                    $indPro->quantity = $data->qty;
                    $indPro->remarks = $data->remark;
                    $indPro->expected_date = $data->date;
                    $indPro->save();
                }
                $imagesarray = array_filter(explode(',',$request->imagesids));
                if(!empty($imagesarray)){
                    foreach ($imagesarray as $images){
                        $punchfile = IndentsFiles::find($images);
                        if(!empty($punchfile->id)){
                            $punchfile->indents_id = $indent->id;
                            $punchfile->reply_id = 0;
                            $punchfile->save();
                        }
                    }
                }
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $indent->id;
                $createlog->module = 'indents';
                $createlog->modulename = 'indents_created';
                $createlog->project_id = $request->project_id;
                $createlog->heading =  $indent->indent_no.' has created';
                $createlog->description = 'Material request with number '.$indent->indent_no.' created by ' . $user->name;
                $createlog->save();

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $indent->project_id)->where('project_members.share_project', '1')
                    ->where('project_members.user_id', '<>',$user->id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Material request';
                    $notifmessage['body'] = 'Material request with number '.$indent->indent_no.' created by ' . $user->name;
                    $notifmessage['activity'] = 'indents';
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Material request Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-indent',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function replyPost(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->indentsid)){
                    $id = $request->indentsid;
                    $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                    $indent = AppIndent::find($id);
                    if($indent){
                        $pi = new IndentsReply();
                        $pi->company_id = $user->company_id;
                        $pi->comment = $request->comment;
                        $pi->indents_id = $indent->id;
                        $pi->added_by = $user->id;
                        $pi->mentionusers = $mentionusers;
                        $pi->save();

                        $imagesarray = array_filter(explode(',',$request->imagesids));
                        if(!empty($imagesarray)){
                            foreach ($imagesarray as $images){
                                $punchfile = IndentsFiles::find($images);
                                if(!empty($punchfile->id)){
                                    $punchfile->indents_id = $indent->id;
                                    $punchfile->reply_id = $pi->id;
                                    $punchfile->save();
                                }
                            }
                        }
                        $medium = $request->medium ?: 'android';
                        $createlog = new ProjectsLogs();
                        $createlog->company_id = $user->company_id;
                        $createlog->added_id = $user->id;
                        $createlog->module_id = $pi->id;
                        $createlog->module = 'indents_reply';
                        $createlog->modulename = 'indents_comment';
                        $createlog->project_id = $indent->project_id ?: 0;
                        $createlog->subproject_id = $indent->subproject_id ?: 0;
                        $createlog->segment_id = $indent->segment_id ?: 0;
                        $createlog->heading =  $pi->comment;
                        $createlog->description = 'Material request Commented by '.$user->name.' with no '.$indent->indent_no.' for the project '.get_project_name($indent->project_id);
                        $createlog->medium = $medium;
                        $createlog->mentionusers = $mentionusers;
                        $createlog->save();

                        if(!empty($indent)){
                            $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                                ->select('users.*')->where('project_members.project_id', $indent->project_id)->where('project_members.share_project', '1')
                                ->where('project_members.user_id', '<>',$user->id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                            foreach($project_membersarray as $project_members){
                                $notifmessage = array();
                                $notifmessage['title'] = 'Material request';
                                $notifmessage['body'] = 'New Comment in material request with number '.$indent->indent_no.' created by ' . $user->name;
                                $notifmessage['activity'] = 'indents';
                                sendFcmNotification($project_members->fcm, $notifmessage);
                            }
                        }

                        $response = array();
                        $response['status'] = 200;
                        $response['indentsid'] = $indent->id;
                        $response['replyid'] = $pi->id;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Material request not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-reply',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->indentsid)){
                    $id = $request->indentsid;
                    $punchitem = Indent::find($id);
                    if(!empty($punchitem)){
                        $page = $request->page;
                        $response = array();
                        if($page=='all'){
                            $pi = IndentsReply::where('indents_id',$punchitem->id)->get();
                        }else{
                            $count = pagecount();
                            $skip = 0;
                            if($page){
                                $skip = $page*$count;
                            }
                            $pi = IndentsReply::where('indents_id',$punchitem->id)->offset($skip)->take($count)->get();
                        }
                        $response = array();
                        $response['status'] = 200;
                        $response['responselist'] = $pi;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Material request not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-reply-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId->id)){
                    $response['message'] = 'Project Details not found';
                    $response['status'] = 300;
                    return $response;
                }
                $name = $request->name;
                $prevfile = Product::where('company_id',$projectId->company_id)->where('project_id',$projectId->id)->where('name',$name)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'Material name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                $response = array();
                $pro = new Product();
                $pro->company_id = $projectId->company_id;
                $pro->category_id = $request->category_id ?: 0;
                $pro->project_id = $projectId->id;
                $pro->name = $name;
                $pro->unit_id = $request->unit_id;
                $pro->save();

                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/product-files/'.$pro->id.'/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $filename= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('product-files/'.$pro->id.'/', $request->image, $image->hashName(), 'public');
                            $filename= $image->hashName();
                            break;
                    }
                    $pro->image = $filename;
                    $pro->save();
                }

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  ProductFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->product_id =  $pro->id;
                            $productimage->save();
                            $sourcepath = 'uploads/product-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/product-files/'.$pro->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/product-files/'.$pro->id)) {
                                    mkdir('uploads/product-files/'.$pro->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }
                $openingstock = account_numberformat($request->opening_stock);
                $storeId = AppStore::where('project_id',$request->project_id)->orderBy('id','DESC')->first();
                if($pro){
                    $check = AppBom::where('project_id', $request->project_id)
                        ->where('store_id', $storeId->id)
                        ->where('product_id', $pro->id)
                        ->where('unit_id', $request->unit_id)
                        ->orderBy('id','DESC')
                        ->first();
                    $sku = time();
                    if($check === null){
                        $bom = new AppBom();
                        $bom->project_id = $request->project_id;
                        $bom->store_id = $storeId->id;
                        $bom->product_id = $pro->id;
                        $bom->unit_id = $request->unit_id;
                        $bom->est_qty = $request->est_qty;
                        $bom->company_id = $projectId->company_id;
                        $bom->est_rate = $request->est_rate;
                        $bom->opening_stock = $openingstock;
                        $bom->save();

                        if($bom) {
                            if(!empty($openingstock)) {
                                $stockCheck = AppStock::where('cid', $pro->id)->where('store_id', $storeId->store_id)->where('project_id', $request->project_id)->first();
                                if (!empty($stockCheck)) {
                                    $stockCheck->stock = (float)$stockCheck->stock + (float)$openingstock;
                                    $stockCheck->save();
                                    if ($stockCheck) {
                                        $pl = new AppProductLog();
                                        $pl->store_id = $storeId->store_id;
                                        $pl->project_id = $request->project_id;
                                        $pl->company_id = $projectId->company_id;
                                        $pl->created_by = $user->id;
                                        $pl->module_id = $bom->id;
                                        $pl->module_name = 'opening_stock';
                                        $pl->product_id = $pro->id;
                                        $pl->quantity = $openingstock;
                                        $pl->balance_quantity = $stockCheck->stock;
                                        $pl->transaction_type = 'plus';
                                        $pl->remark = 'Opening Stock added ';
                                        $pl->save();
                                    }
                                } else {
                                    $stock = new AppStock();
                                    $stock->sku = $sku;
                                    $stock->cid = $pro->id;
                                    $stock->company_id = $projectId->company_id;
                                    $stock->quantity = $openingstock;
                                    $stock->stock = $openingstock;
                                    $stock->unit = $request->unit_id;
                                    $stock->store_id = $storeId->id;
                                    $stock->project_id = $request->project_id;
                                    $stock->save();

                                    if ($stock) {
                                        $pl = new AppProductLog();
                                        $pl->store_id = $storeId->id;
                                        $pl->project_id = $request->project_id;
                                        $pl->company_id = $projectId->company_id;
                                        $pl->created_by = $user->id;
                                        $pl->module_id = $bom->id;
                                        $pl->module_name = 'opening_stock';
                                        $pl->product_id = $pro->id;
                                        $pl->quantity = $openingstock;
                                        $pl->balance_quantity = $openingstock;
                                        $pl->transaction_type = 'plus';
                                        $pl->remark = 'Opening Stock added ';
                                        $pl->save();
                                    }
                                }
                            }
                        }
                    }else{
                        $check->est_qty = $request->est_qty;
                        $check->est_rate = $request->est_rate;
                        $check->opening_stock = $openingstock;
                        $check->save();
                        if($check) {
                            if (!empty($openingstock)) {
                                $stockCheck = AppStock::where('cid', $pro->id)->where('store_id', $storeId->store_id)->where('project_id', $request->project_id)->first();
                                if (!empty($stockCheck)) {
                                    $stockCheck->stock = (float)$stockCheck->stock + (float)$openingstock;
                                    $stockCheck->save();
                                    if ($stockCheck) {
                                        $pl = new AppProductLog();
                                        $pl->store_id = $storeId->id;
                                        $pl->project_id = $request->project_id;
                                        $pl->company_id = $projectId->company_id;
                                        $pl->created_by = $user->id;
                                        $pl->module_id = $check->id;
                                        $pl->module_name = 'opening_stock';
                                        $pl->product_id = $pro->id;
                                        $pl->quantity = $openingstock;
                                        $pl->balance_quantity = $stockCheck->stock;
                                        $pl->transaction_type = 'plus';
                                        $pl->remark = 'Opening Stock added ';
                                        $pl->save();
                                    }
                                } else {
                                    $stock = new AppStock();
                                    $stock->sku = $sku;
                                    $stock->cid = $pro->id;
                                    $stock->company_id = $projectId->company_id;
                                    $stock->quantity = $openingstock;
                                    $stock->stock = $openingstock;
                                    $stock->unit = $request->unit_id;
                                    $stock->store_id = $storeId->id;
                                    $stock->project_id = $request->project_id;
                                    $stock->save();

                                    if ($stock) {
                                        $pl = new AppProductLog();
                                        $pl->store_id = $storeId->id;
                                        $pl->project_id = $request->project_id;
                                        $pl->company_id = $projectId->company_id;
                                        $pl->created_by = $user->id;
                                        $pl->module_id = $check->id;
                                        $pl->module_name = 'opening_stock';
                                        $pl->product_id = $pro->id;
                                        $pl->quantity = $openingstock;
                                        $pl->balance_quantity = $openingstock;
                                        $pl->transaction_type = 'plus';
                                        $pl->remark = 'Opening Stock added ';
                                        $pl->save();
                                    }
                                }
                            }
                        }
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Material Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeProductImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $punchitemid = $request->productid;
                $punchitem = Product::find($punchitemid);
                $replyid = $request->replyid;
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                    $storage = storage();
                    $companyid = $user->company_id;
                    $file = new ProductFiles();
                    $file->company_id = $companyid;
                    $file->user_id = $user->id;
                    $file->product_id = $punchitemid ?: 0;
                    switch($storage) {
                        case 'local':

                            $destinationPath = 'uploads/product-files/'.$punchitemid;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            $filename = $fileData->hashName();
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/product-files/'.$punchitemid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'product-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('product-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $punchitemid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$punchitemid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $punchitemid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('product-files/'.$punchitemid.'/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/product-files/'.$punchitemid.'/'.$fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Issue Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-product-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function updateUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projects = AppProject::find($projectid);
                if(empty($projects)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $unit = Units::find($request->id);
                if(empty($unit)){
                    $response['message'] = "Unit Not Found";
                    $response['status'] = 300;
                    return $response;
                }
                $name  = trim($request->name);
                $pro = Units::where('id','<>',$unit->id)->where('company_id',$projects->company_id)->where('name',$name)->first();
                if(!empty($pro)){
                    $response['message'] = "Unit Already exits";
                    $response['status'] = 300;
                    return $response;
                }
                $unit->name = $request->name;
                $unit->symbol = $request->symbol;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Unit Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateIndentStatus(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = AppIndent::find($request->id);
                $unit->status = $request->status;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Material request Status Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-indent-status',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Units::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectId = AppProject::where('id',$request->project_id)->orderBy('id','DESC')->first();
                if(empty($projectId)){
                    $response['message'] = "Project Not Found";
                    $response['status'] = 300;
                    return $response;
                }
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['message'] = "Store Not Found";
                    $response['status'] = 300;
                    return $response;
                }

                $pro = Product::where('id',$request->id)->first();
                if(empty($pro)){
                    $response['message'] = "Material Not Found";
                    $response['status'] = 300;
                    return $response;
                }
                $name  = trim($request->name);
                $oldpro = Product::where('id','<>',$pro->id)->where('company_id',$projectId->company_id)->where('project_id',$projectId->id)->where('name',$name)->first();
                if(!empty($oldpro)){
                    $response['message'] = "Material Already exits";
                    $response['status'] = 300;
                    return $response;
                }
                $pro->company_id = $projectId->company_id;
                $pro->project_id = $projectId->id;
                $pro->category_id = $request->category_id;
                $pro->name = $name;
                $pro->unit_id = $request->unit_id;
                $pro->save();

                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/product-files/'.$pro->id.'/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $filename= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('product-files/'.$pro->id.'/', $request->image, $image->hashName(), 'public');
                            $filename= $image->hashName();
                            break;
                    }
                    $pro->image = $filename;
                    $pro->save();
                }

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  ProductFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->product_id =  $pro->id;
                            $productimage->save();
                            $sourcepath = 'uploads/product-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/product-files/'.$pro->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/product-files/'.$pro->id)) {
                                    mkdir('uploads/product-files/'.$pro->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }
                if($pro){
                    $bom = AppBom::where('project_id', $projectId->id)
                        ->where('store_id', $storeId->id)
                        ->where('product_id', $pro->id)
                        ->where('unit_id', $request->unit_id)
                        ->orderBy('id','DESC')
                        ->first();
                    $sku = time();
                    if(empty($bom)) {
                        $bom = new AppBom();
                        $bom->project_id = $projectId->id;
                        $bom->store_id = $storeId->id;
                        $bom->product_id = $pro->id;
                        $bom->company_id = $projectId->company_id;
                        $bom->unit_id = $request->unit_id;
                    }
                    $bom->est_qty = $request->est_qty;
                    $bom->est_rate = $request->est_rate;
                    $bom->opening_stock = $request->opening_stock;
                    $bom->save();
                    if($bom) {
                        if(!empty($request->opening_stock)) {
                            $stockCheck = AppStock::where('cid', $pro->id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                            if (!empty($stockCheck)) {
                                    $pl = AppProductLog::where('store_id',$storeId->id)->where('project_id',$projectId->id)->where('module_id',$bom->id)->where('module_name','opening_stock')->where('product_id',$pro->id)->first();
                                    if(empty($pl)){
                                        $pl = new AppProductLog();
                                    }
                                    $pl->store_id = $storeId->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $user->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $bom->id;
                                    $pl->module_name = 'opening_stock';
                                    $pl->product_id = $pro->id;
                                    $pl->quantity = $request->opening_stock;
                                    $pl->balance_quantity = $request->opening_stock;
                                    $pl->transaction_type = 'plus';
                                    $pl->remark = 'Opening Stock added ';
                                    $pl->save();
                            } else {
                                $stock = new AppStock();
                                $stock->sku = $sku;
                                $stock->cid = $pro->id;
                                $stock->quantity = $request->opening_stock;
                                $stock->stock = $request->opening_stock;
                                $stock->unit = $request->unit_id;
                                $stock->store_id = $storeId->id;
                                $stock->company_id = $projectId->company_id;
                                $stock->project_id = $projectId->id;
                                $stock->save();

                                if ($stock) {
                                    $pl = new AppProductLog();
                                    $pl->store_id = $storeId->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $user->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $bom->id;
                                    $pl->module_name = 'opening_stock';
                                    $pl->product_id = $pro->id;
                                    $pl->quantity = $request->opening_stock;
                                    $pl->balance_quantity = $request->opening_stock;
                                    $pl->transaction_type = 'plus';
                                    $pl->remark = 'Opening Stock added ';
                                    $pl->save();
                                }
                            }
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Material Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteproduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $productid = $request->id;
                $products  = Product::where('id',$productid)->first();
                if(empty($products)){
                    $response['status'] = 301;
                    $response['message'] = 'Material not found';
                    return $response;
                }
                $piproducts = PiProducts::where('product_id',$productid)->first();
                if(!empty($piproducts)){
                    $response['status'] = 301;
                    $response['message'] = 'Please remove Material Receive Products';
                    return $response;
                }
                $indeproducts = IndentProducts::where('cid',$productid)->first();
                if(!empty($indeproducts)){
                    $response['status'] = 301;
                    $response['message'] = 'Please remove Material request Products';
                    return $response;
                }
                $indeproducts = AppProductIssue::where('product_id',$productid)->first();
                if(!empty($indeproducts)){
                    $response['status'] = 301;
                    $response['message'] = 'Please remove Products Issues';
                    return $response;
                }
                $unit = Product::where('id',$productid)->where('project_id',$projectid)->delete();
                ProductFiles::where('product_id',$productid)->delete();
                $response['status'] = 200;
                $response['message'] = 'Material Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteIndent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $indents =  AppIndent::where('id',$request->id)->first();
                if(empty($indents)){
                    $response['status'] = 301;
                    $response['message'] = 'Material request Not found';
                    return $response;
                }
                $materialreceive = PurchaseInvoice::where('indent_id',$request->id)->first();
                if(!empty($materialreceive)){
                    $response['status'] = 301;
                    $response['message'] = 'Please remove Material Receive';
                    return $response;
                }
                $indents->delete();
                IndentsFiles::where('indents_id',$request->id)->delete();
                IndentProducts::where('indent_id',$request->id)->delete();

                $indentsreplyarray = IndentsReply::where('indents_id',$request->id)->get();
                foreach($indentsreplyarray as $indentsreply){
                    ProjectsLogs::where('module_id',$indentsreply->id)->where('module','indents_reply')->delete();
                    $indentsreply->delete();
                }

                IndentsFiles::where('indents_id',$request->id)->delete();
                ProjectsLogs::where('module_id',$request->id)->where('module','indents')->delete();
                $response['status'] = 200;
                $response['message'] = 'Material request Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-indent',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteIndentProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $ip = IndentProducts::where('id',$request->id)->first();

                $indents =  AppIndent::where('id',$ip->indent_id)->first();
                if(empty($indents)){
                    $response['status'] = 301;
                    $response['message'] = 'Material request Not found';
                    return $response;
                }
                $materialreceive = PurchaseInvoice::where('indent_id',$ip->indent_id)->pluck('id')->toArray();
                $promaterialreceive = PiProducts::whereIn('pi_id',$materialreceive)->where('product_id',$ip->cid)->pluck('id')->toArray();
                if(!empty($promaterialreceive)){
                    $response['status'] = 301;
                    $response['message'] = 'Please remove Material Receive';
                    return $response;
                }

                $ipc = IndentProducts::where('indent_id',$ip->indent_id)->get();
                if(count($ipc) < 2){
                    Indent::where('id',$ip->indent_id)->delete();
                    IndentProducts::where('id',$request->id)->delete();
                }else {
                    IndentProducts::where('id', $request->id)->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Material request Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-indent-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateIndentProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{

                $indPro = IndentProducts::find($request->id);
                if(empty($indPro)){
                    $response['status'] = 301;
                    $response['message'] = 'Material request Not found';
                    return $response;
                }
                $grn = AppPurchaseInvoice::where('indent_id', $indPro->indent_id)->pluck('id')->toArray();
                $piproductsqty = PiProducts::whereIn('pi_id',$grn)->where('product_id',$indPro->cid)->sum('quantity');

                if((float)$request->qty<(float)$piproductsqty){
                    $response['status'] = 301;
                    $response['message'] = 'Updated Quantity is lesser than received quantity';
                    return $response;
                }
                $indPro->unit = $request->unitid;
                $indPro->quantity = (float)$request->qty;
                $indPro->remarks = $request->remark;
                $indPro->expected_date = !empty($request->date) ? date('Y-m-d',strtotime($request->date)) : '';
                $indPro->save();

                $indent = Indent::find($indPro->indent_id);
                if(!empty($indent)){
                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $indent->project_id)->where('project_members.share_project', '1')
                        ->where('project_members.user_id', '<>',$user->id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Material request';
                        $notifmessage['body'] = 'Material request has been updated with number '.$indent->indent_no.' created by ' . $user->name;
                        $notifmessage['activity'] = 'indents';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'indent updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-indent-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storesList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $units = AppStore::where('added_by',$user->id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'stores-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = new AppStore();
                $pro->company_name = $request->company_name;
                $pro->project_id = $request->project_id;
                $pro->added_by = $user->id;
                $pro->company_id = $user->company_id;
                $pro->address = $request->address;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Store Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = AppStore::find($request->id);
                $pro->company_name = $request->company_name;
                $pro->project_id = $request->project_id;
                $pro->company_id = $user->company_id;
                $pro->address = $request->address;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Store Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = AppStore::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getStore(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $units = AppStore::where('project_id',$request->project_id)->where('added_by',$user->id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $indentsid = $request->indentsid;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                    $file = new IndentsFiles();
                    $file->added_by = $user->id;
                    $file->company_id = $companyid;
                    $file->indents_id = $request->indentsid ?: 0;
                    $file->reply_id = $request->replyid ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/indents-files/'.$indentsid;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/indents-files/'.$indentsid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'indents-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('indents-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $indentsid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$indentsid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $indentsid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('indents-files/'.$request->manpowerid.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->manpowerid.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['message'] = 'Material request Image Uploaded';
                    return $response;

                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function indentReports(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;

                $startdate = str_replace('/','-',$request->start_date);
                $startdate =  !empty($startdate) ? date('Y-m-d',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $enddate = !empty($enddate) ? date('Y-m-d',strtotime($enddate)) : '';

                $response = array();
                $indentsarray = AppIndent::where('project_id',$projectid);
                if(!empty($startdate)){
                    $startdate = date('Y-m-d',strtotime($startdate)).' 00:00:00';
                    $indentsarray = $indentsarray->where('created_at','>=',$startdate);
                }
                if(!empty($enddate)){
                    $enddate = date('Y-m-d',strtotime($enddate)).' 23:59:59';
                    $indentsarray = $indentsarray->where('created_at','<=',$enddate);
                }

                $indentsarray = $indentsarray->get();
                $indentslist = array();
                foreach ($indentsarray as $indents){
                    $indentsrow = array();
                    $indentsrow['id'] = $indents->id;
                    $indentsrow['indent_no'] = $indents->indent_no;
                    $indentproductsarray = IndentProducts::where('indent_id',$indents->id)->get();
                    if(count($indentproductsarray)>0){
                        foreach ($indentproductsarray as $indentproduct){
                            $indenproducts = array();
                            $indenproducts['indentid'] = $indentproduct->indent_id;
                            $indenproducts['product'] = get_ind_pro_name($indentproduct->cid);
                            $indenproducts['quantity'] = $indentproduct->quantity;
                            $indenproducts['unit'] = get_unit_name($indentproduct->unit);
                            $indenproducts['expected_date'] = date('d M Y',strtotime($indentproduct->expected_date));
                            $indenproducts['created_date'] = date('d M Y',strtotime($indentproduct->created_at));
                            $indentsrow['indentslist'][] = $indenproducts;
                        }
                    }else{
                        $indentsrow['indentslist'] = array();
                    }
                    $indentslist[] = $indentsrow;
                }
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = $indentslist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indent-report',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function exportIndent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $data = array();
                $data['project_id'] = $request->project_id;
                $data['indent'] = Indent::find($request->indent_id);
                $data['all_data'] = IndentProducts::where('indent_id', $request->indent_id)->get();
                $filename = '';
                $pdf = PDF::loadView('admin.indent.indentPDF',$data);
                $filename = uniqid();
                $filename .= '.pdf';
                if (!file_exists('indentreportfiles/')) {
                    mkdir('indentreportfiles/', 0755);
                }
                $pdf->save('indentreportfiles/'.$filename);
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = url('indentreportfiles/' . $filename);
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indent-report',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;

    }

    public function updateStock(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $logid = $request->log_id;
                $type = '';
                $stock = AppStock::where('id',$request->stock_id)->first();
                if(empty($stock)){
                    $response['message'] = 'Stock Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                if($request->type == 'shortage'){
                    $type = 'minus';
                    $stock->stock = (float)$stock->stock - (float)$request->quantity;
                }
                if($request->type == 'excess'){
                    $type = 'plus';
                    $stock->stock = (float)$stock->stock + (float)$request->quantity;
                }
                $stock->save();
                if($stock) {
                    if(!empty($logid)){
                        $pl = AppProductLog::where('id',$logid)->first();
                    }else{
                        $pl = new AppProductLog();
                        $pl->store_id = $stock->store_id;
                        $pl->project_id = $stock->project_id;
                        $pl->company_id = $user->company_id;
                        $pl->created_by = $user->id;
                        $pl->module_id = $stock->id;
                        $pl->module_name = 'update-stock';
                        $pl->product_id = $stock->cid;
                    }
                    $pl->quantity = $request->quantity;
                    $pl->balance_quantity = $stock->stock;
                    $pl->transaction_type = $type;
                    $pl->remark = 'Opening Stock updated ';
                    $pl->save();
                }
                $response['status'] = 200;
                $response['message'] = 'Stock Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-stock',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteStock(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $logid = $request->log_id;
                $type = '';
                $stock = AppStock::where('id',$request->stock_id)->first();
                if(empty($stock)){
                    $response['message'] = 'Stock Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                $pl = AppProductLog::where('id',$logid)->first();
                if(empty($stock)){
                    $response['message'] = 'Product log Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                if($pl->type == 'shortage'){
                    $stock->stock = (float)$stock->stock + (float)$request->quantity;
                }
                if($pl->type == 'excess'){
                    $stock->stock = (float)$stock->stock - (float)$request->quantity;
                }
                $stock->save();
                if($stock) {
                    $pl->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Stock Deleted Sucessfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-stock',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function updateOpeningStock(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $logid = $request->log_id;
                $type = '';
                $stock = AppStock::where('id',$request->stock_id)->first();
                if(empty($stock)){
                    $response['message'] = 'Stock Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                if($stock) {
                    $quant = $request->quantity;
                    $pl = AppProductLog::where('id',$logid)->first();

                    $bom = AppBom::find($pl->module_id);

                    if(!empty($bom)){
                        $bom->opening_stock = $quant;
                        $bom->save();
                    }
                    $prevstock = $bom->opening_stock;

                    if($prevstock>=$quant){
                        $stock->stock = (float)$stock->stock - (float)$quant;
                    }
                    if($prevstock<$quant){
                        $stock->stock = (float)$stock->stock + (float)$quant;
                    }
                    $stock->save();


                    $pl->quantity = $quant;
                    $pl->balance_quantity = $stock->stock;
                    $pl->transaction_type = 'plus';
                    $pl->remark = 'Opening Stock updated ';
                    $pl->save();
                }
                $response['status'] = 200;
                $response['message'] = 'Stock Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-opening-stock',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteOpeningStock(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $logid = $request->log_id;
                $type = '';
                $stock = AppStock::where('id',$request->stock_id)->first();
                if(empty($stock)){
                    $response['message'] = 'Stock Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                $pl = AppProductLog::where('id',$logid)->first();
                if(empty($stock)){
                    $response['message'] = 'Product log Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                $bom = AppBom::find($pl->module_id);
                $bom->opening_stock = 0;
                $bom->save();

                $prevstock = $bom->opening_stock;
                $quant = $stock->stock;

                if($prevstock>=$quant){
                    $stock->stock = (float)$stock->stock - (float)$prevstock;
                }
                if($prevstock<$quant){
                    $stock->stock = (float)$stock->stock + (float)$prevstock;
                }
                $stock->save();

                if($stock) {
                    $pl->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Stock Deleted Sucessfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-opening-stock',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function stockReportPDF(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $data = array();
                if(!empty($request->product_id)){
                    $products =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project_id)->where('product_id',$request->product_id)->where('company_id',$user->company_id)
                        ->orderBy('id', 'ASC')->get();
                    $data['productLogs'] = $products;
                    $pdf = PDF::loadView('admin.inventory.smr-report',$this->data);
                    return $pdf->download('material-report.pdf');
                }if(!empty($request->prod_cat_id)){
                    $data['category'] = $request->prod_cat_id;
                    $productarray = Product::where('category_id',$request->prod_cat_id)->pluck('id');
                    $productgroup =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project_id)->whereIn('product_id',$productarray)->where('company_id',$user->company_id)
                        ->orderBy('id', 'ASC')->groupBy('product_id')->pluck('product_id');
                    $products =  ProductLog::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project_id)->whereIn('product_id',$productarray)->where('company_id',$user->company_id)
                        ->orderBy('id', 'ASC')->get();
                    $data['productLogs'] = $products;
                    $data['productgroup'] = $productgroup;
                    $pdf = PDF::loadView('admin.inventory.mcr-report',$this->data);
                    return $pdf->download('material-category-report.pdf');
                }else{
                    $data['products'] = Stock::where(DB::raw('DATE(`created_at`)'),$request->date)->where('project_id',$request->project_id)->where('company_id',$user->company_id)->get();
                    $filename = '';
                    $pdf = PDF::loadView('admin.inventory.stockPDF',$data);
                    return $pdf->download('inventory-report.pdf');
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'stock-report',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}