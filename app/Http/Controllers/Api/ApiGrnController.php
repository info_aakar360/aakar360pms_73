<?php

namespace App\Http\Controllers\Api;

use App\AppIndent;
use App\AppProductLog;
use App\AppProject;
use App\AppPurchaseInvoice;
use App\AppStock;
use App\AppStore;
use App\Company;
use App\Http\Controllers\Controller;
use App\IndentProducts;
use App\PiFile;
use App\PiProducts;
use App\ProductLog;
use App\ProductReturns;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Dropbox\Client;


class ApiGrnController extends Controller
{
    // Validate API_KEY
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    // Validate Token for validating user login
    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 300;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 300;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 300;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request) {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            // checking permission if user has access or not
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function grnList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $projects = AppProject::find($projectid);
                if(empty($projects)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $page = $request->page;
                $store = AppStore::where('project_id', $projects->id)->first();
                $grns = AppPurchaseInvoice::where('project_id',$projects->id)->where('store_id', $store->id);
                if(!empty($request->fromdate)){
                    $fromdate = date('Y-m-d 00:00:01',strtotime($request->fromdate));
                    $grns = $grns->where('created_at','>=',$fromdate);
                }
                if(!empty($request->todate)){
                    $todate = date('Y-m-d 23:59:59',strtotime($request->todate));
                    $grns = $grns->where('created_at','<=',$todate);
                }
                if($page=='all'){
                    $grns = $grns->orderBy('id','desc')->get();
                }else {
                    $count = pagecount();
                    $skip = 0;
                    if ($page) {
                        $skip = $page * $count;
                    }
                    $grns = $grns->orderBy('id', 'desc')
                        ->offset($skip)
                        ->take($count)
                        ->get();
                }
                $grnData = array();
                foreach ($grns as $grn){
                    $productsname = PiProducts::join('products','products.id','=','pi_products.product_id')->where('pi_products.pi_id',$grn->id)->pluck('products.name')->toArray();
                    $grnData[] = array(
                        'id' => (!empty($grn->id) ? $grn->id : ''),
                        'invoice_no' => (!empty($grn->invoice_no) ? $grn->invoice_no : ''),
                        'indent_id' => (!empty($grn->indent_id) ? $grn->indent_id : ''),
                        'po_id' => (!empty($grn->po_id) ? $grn->po_id : ''),
                        'store_id' => (!empty($grn->store_id) ? $grn->store_id : ''),
                        'project_id' => (!empty($grn->project_id) ? $grn->project_id : ''),
                        'project_name' => (!empty($grn->project_id) ? get_project_name($grn->project_id) : ''),
                        'product_name' => !empty($productsname) ? implode(', ',$productsname) : '',
                        'dated' => (!empty($grn->dated) ? Carbon::parse($grn->dated)->format('d M Y' ) : ''),
                        'freight' => (!empty($grn->freight) ? $grn->freight : ''),
                        'gt' => (!empty($grn->gt) ? $grn->gt : ''),
                        'supplier_id' => (!empty($grn->supplier_id) ? $grn->supplier_id : ''),
                        'supplier_name' => get_supplier_name($grn->supplier_id),
                        'remark' => (!empty($grn->remark) ? $grn->remark : ''),
                        'payment_terms' => (!empty($grn->payment_terms) ? $grn->payment_terms : ''),
                        'status' => (!empty($grn->status) ? $grn->status : ''),
                        'created_at' => (!empty($grn->created_at) ? Carbon::parse($grn->created_at)->format('d M Y' ) : ''),
                        'updated_at' => (!empty($grn->updated_at) ? Carbon::parse($grn->updated_at)->format('d M Y' ) : ''),
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = $grnData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function grnDetail(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $grn = AppPurchaseInvoice::where('id', $request->grn_id)->first();
                if(empty($grn)){
                    $response['message'] = 'Material Receive Not found';
                    $response['status'] = '301';
                    return $response;
                }
                $grn->supplier_name = get_supplier_name($grn->supplier_id);
                $grn->dated = Carbon::parse($grn->dated)->format('d-m-Y');
                $grn->project_name = get_project_name($grn->project_id);
                $data['grn'] = $grn;
                $grnProducts = PiProducts::where('pi_id',$request->grn_id)->get();
                $grnData = array();
                foreach ($grnProducts as $grnProduct){
                    $alreadyRec = 0;
                    $indented_qty = 0;
                    $balance_qty = 0;
                    if(!empty($grn->indent_id)){
                        $indPro = IndentProducts::where('indent_id',$grn->indent_id)->where('cid',$grnProduct->product_id)->first();
                        $pgrns = AppPurchaseInvoice::where('purchase_invoice.indent_id',$grn->indent_id)
                            ->join('pi_products','pi_products.pi_id', '=','purchase_invoice.id')
                            ->where('pi_products.product_id',$grnProduct->product_id)
                            ->sum('pi_products.quantity');
                        $alreadyRec = $pgrns;
                        $indented_qty = (!empty($indPro)) ? $indPro->quantity : 0;
                        $balance_qty = (!empty($indPro)) ? $indPro->quantity - $pgrns : 0;
                    }
                    $grnData[] = array(
                        'id' => $grnProduct->id,
                        'product_id' => $grnProduct->product_id,
                        'product_name' => get_local_product_name($grnProduct->product_id),
                        'unit_id' => $grnProduct->unit_id,
                        'unit_name' => get_unit_name($grnProduct->unit_id),
                        'quantity' => $grnProduct->quantity,
                        'date' => Carbon::parse($grnProduct->date)->format('d-m-Y'),
                        'price' => $grnProduct->price,
                        'tax' => $grnProduct->tax,
                        'amount' => $grnProduct->amount,
                        'remark' => $grnProduct->remark,
                        'already_rec_qty' => $alreadyRec,
                        'indented_qty' => $indented_qty,
                        'balance_qty' => $balance_qty,
                    );
                }
                $grnFileData = array();
                $grnFiles = PiFile::where('pi_id',$request->grn_id)->get();
                foreach ($grnFiles as $grnFile){
                    $grnFileData[] = array(
                        'id' => $grnFile->id,
                        'added_by' => get_user_name($grnFile->added_by),
                        'file_name' => $grnFile->filename,
                        'image' => uploads_url().'pi-files/'.$request->grn_id.'/'.$grnFile->hash_name,
                        'created_at' => Carbon::parse($grnFile->created_at)->format('d-m-Y'),
                    );
                }
                $data['grnProducts'] = $grnData;
                $data['grnFiles'] = $grnFileData;
                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-detail',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createGrn(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $projectId = AppProject::find($request->project_id);
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $store = AppStore::where('project_id', $request->project_id)->first();
                $ind = AppPurchaseInvoice::orderBy('id', 'DESC')->first();
                if($ind == null){
                    $grnNo = 'MR-'.date("d-m-Y").'-1';
                }else {
                    $grnNo = 'MR-' . date("d-m-Y") . '-' . ($ind->id+1);
                }
                $pi = new AppPurchaseInvoice();
                $pi->invoice_no = $grnNo;
                $pi->store_id = (!empty($store->id) ? $store->id : '');
                $pi->project_id = $request->project_id;
                $pi->supplier_id = $request->supplier_id;
                $pi->dated = $date;
                $pi->company_id = $projectId->company_id;
                $pi->remark = $request->remark;
                $pi->save();
                $sku = time();
                $storeId = AppStore::where('project_id',$request->project_id)->orderBy('id','DESC')->first();
                if($pi){
                    $indPro = new PiProducts();
                    $indPro->pi_id = $pi->id;
                    $indPro->product_id = $request->product_id;
                    $indPro->quantity = $request->quantity;
                    $indPro->unit_id = $request->unit_id;
                    $indPro->date = $date;
                    $indPro->remark = $request->remark;
                    $indPro->price = $request->price;
                    $indPro->save();
                    if($indPro){
                        $stockCheck = AppStock::where('cid',$request->product_id)->where('store_id',$storeId->id)->where('project_id',$request->project_id)->first();

                        if(!empty($stockCheck)){
                            $stockCheck->stock = (float)$stockCheck->stock+(float)$request->quantity;
                            $stockCheck->save();
                            if($stockCheck){
                                $pl = new AppProductLog();
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $projectId->id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pi->id;
                                $pl->module_name = 'grn';
                                $pl->product_id = $request->product_id;
                                $pl->quantity = $request->quantity;
                                $pl->balance_quantity = $stockCheck->stock;
                                $pl->transaction_type = 'plus';
                                $pl->remark = $request->remark;
                                if(!empty($indPro->date)){
                                    $pl->date =  Carbon::parse($indPro->date)->format('Y-m-d');
                                }
                                $pl->save();
                            }
                        }else{
                            $stock = new AppStock();
                            $stock->sku = $sku;
                            $stock->po = $request->po;
                            $stock->company_id = $projectId->company_id;
                            $stock->inv_id = $pi->id;
                            $stock->cid = $request->product_id;
                            $stock->supplier_id = $request->supplier_id;
                            $stock->quantity = $request->quantity;
                            $stock->stock = $request->quantity;
                            $stock->unit = $request->unit_id;
                            $stock->store_id = $storeId->id;
                            $stock->project_id = $request->project_id;
                            $stock->save();

                            if($stock){
                                $pl = new AppProductLog();
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $projectId->id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pi->id;
                                $pl->module_name = 'grn';
                                $pl->product_id = $request->product_id;
                                $pl->quantity = $request->quantity;
                                $pl->balance_quantity = $request->quantity;
                                $pl->transaction_type = 'plus';
                                $pl->remark = $request->remark;
                                if(!empty($indPro->date)){
                                    $pl->date =  Carbon::parse($indPro->date)->format('Y-m-d');
                                }
                                $pl->save();
                            }
                        }
                    }

                    $response['status'] = 200;
                    $response['message'] = 'Success';
                    $response['grn_id'] = $pi->id;
                    $response['response'] = 'Material Receive Created Successfully!';
                }else{
                    $response['status'] = 200;
                    $response['message'] = 'Success';
                    $response['grn_id'] = $pi->id;
                    $response['response'] = 'Something went wrong';
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'create-grn', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }


    public function editGrn(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $sku = time();
                $storeId = AppStore::where('project_id',$request->project_id)->orderBy('id','DESC')->first();
                $projectId = AppProject::find($request->project_id);
                $ind = AppPurchaseInvoice::where('id', $request->grn_id)->first();
                if(isset($request->project_id)) {
                    $ind->project_id = $request->project_id;
                }
                if(isset($request->dated)) {
                    $date = Carbon::parse($request->dated)->format('Y-m-d');
                    $ind->dated = $date;
                }
                if(isset($request->supplier_id)) {
                    $ind->supplier_id = $request->supplier_id;
                }
                if(isset($request->remark)) {
                    $ind->remark = $request->remark;
                }
                $ind->save();

                $indPro = PiProducts::where('pi_id',$request->grn_id)->where('product_id',$request->product_id)->first();

                $prevqaunt = $indPro->quantity ?: 0;
                if(isset($request->product_id)) {
                    $indPro->product_id = $request->product_id;
                }
                if(!empty($request->quantity)) {
                    $indPro->quantity = $request->quantity;
                }
                if(isset($request->unit_id)) {
                    $indPro->unit_id = $request->unit_id;
                }
                if(isset($request->dated)) {
                    $date = Carbon::parse($request->dated)->format('Y-m-d');
                    $indPro->date = $date;
                }
                if(isset($request->remark)) {
                    $indPro->remark = $request->remark;
                }
                if(isset($request->price)) {
                    $indPro->price = $request->price;
                }
                $indPro->save();

                if($indPro){
                    $stockCheck = AppStock::where('cid',$request->product_id)->where('store_id',$storeId->id)->where('project_id',$request->project_id)->first();
                    if(!empty($stockCheck)){
                        $stockdata = 0;
                        $newstock = (float)$stockCheck->stock - (float)$prevqaunt;
                        $stockdata = (float)$newstock+(float)$request->quantity;

                        $updatestockdata = AppProductLog::where('store_id',$storeId->id)->where('project_id',$projectId->id)->where('product_id',$indPro->product_id)->get();
                        if(!empty($updatestockdata)){
                            foreach ($updatestockdata as $updatestock){
                                if($updatestock->module_name!='opening_stock'){
                                    if($updatestock->transaction_type=='plus'){

                                        $balquantity = $stockdata+$updatestock->quantity;
                                        $stockdata += $updatestock->quantity;
                                    }elseif($updatestock->transaction_type=='minus'){
                                        $balquantity = $stockdata-$updatestock->quantity;
                                        $stockdata -= $updatestock->quantity;
                                    }
                                    $updatestock->balance_quantity = $balquantity;
                                    $updatestock->save();
                                }
                            }
                        }
                        $stockCheck->stock = (float)$stockdata;
                        $stockCheck->save();
                        if($stockCheck){
                            $pl = AppProductLog::where('product_id',$indPro->product_id)->where('module_id',$indPro->pi_id)->where('module_name','grn')->first();
                            if(empty($pl)){
                                $pl = new AppProductLog();
                            }
                            $pl->store_id = $storeId->id;
                            $pl->project_id = $projectId->id;
                            $pl->company_id = $projectId->company_id;
                            $pl->created_by = $user->id;
                            $pl->module_id = $ind->id;
                            $pl->module_name = 'grn';
                            $pl->product_id = $request->product_id;
                            $pl->quantity = $request->quantity;
                            $pl->balance_quantity = $stockCheck->stock;
                            $pl->transaction_type = 'plus';
                            $pl->remark = $request->remark;
                            if(isset($request->dated)) {
                                $date = Carbon::parse($request->dated)->format('Y-m-d');
                                $pl->date = $date;
                            }
                            $pl->save();
                        }
                    }else{
                        $stock = new AppStock();
                        $stock->sku = $sku;
                        $stock->po = $request->po;
                        $stock->company_id = $projectId->company_id;
                        $stock->inv_id = $ind->id;
                        $stock->cid = $request->product_id;
                        $stock->supplier_id = $request->supplier_id;
                        $stock->quantity = $request->quantity;
                        $stock->stock = $request->quantity;
                        $stock->unit = $request->unit_id;
                        $stock->store_id = $storeId->id;
                        $stock->project_id = $projectId->id;
                        $stock->save();

                        if($stock){
                            $pl = new AppProductLog();
                            $pl->store_id = $storeId->id;
                            $pl->project_id = $projectId->id;
                            $pl->company_id = $projectId->company_id;
                            $pl->created_by = $user->id;
                            $pl->module_id = $ind->id;
                            $pl->module_name = 'grn';
                            $pl->product_id = $request->product_id;
                            $pl->quantity = $request->quantity;
                            $pl->balance_quantity = $request->quantity;
                            $pl->transaction_type = 'plus';
                            $pl->remark = $request->remark;
                            if(isset($request->dated)) {
                                $date = Carbon::parse($request->dated)->format('Y-m-d');
                                $pl->date = $date;
                            }
                            $pl->save();
                        }
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['grn_id'] = $ind->id;
                $response['response'] = 'Material Receive Updated Successfully!';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'create-grn', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }

    public function deleteGrn(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                 $materialrecevie = AppPurchaseInvoice::where('id', $request->grn_id)->first();
                 if(!empty($materialrecevie->id)){
                   $materialproductsarray = PiProducts::where('pi_id',$materialrecevie->id)->get();
                   foreach ($materialproductsarray as $materialproducts){
                       $stock =  \App\Stock::where('project_id', $materialrecevie->project_id)->where('store_id',$materialrecevie->store_id)->where('cid', $materialproducts->product_id)->first();
                      $newstock = $stock->stock;
                       if($newstock<$materialproducts->quantity){
                           $response = array();
                           $response['status'] = 300;
                           $response['message'] = 'Stock update not found';
                           return $response;
                       }
                   }
                 }
                 if(!empty($materialrecevie->id)){
                     PiProducts::where('pi_id',$materialrecevie->id)->delete();
                     PiFile::where('pi_id',$materialrecevie->id)->delete();
                     ProductLog::where('module_id',$materialrecevie->id)->where('module_name','grn')->delete();
                     $materialrecevie->delete();
                 }

                $response['status'] = 200;
                $response['message'] = 'Material Receive Deleted Successfully!';
                $response['response'] = '';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-delete',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function piStoreImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                    $file = new PiFile();
                    $file->added_by = $user->id;
                    $file->company_id = $user->company_id;
                    $file->pi_id = $request->grn_id ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/pi-files/'.$request->grn_id;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/pi-files/'.$request->grn_id, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'pi-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('pi-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->grn_id)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$request->grn_id);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $request->grn_id)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('pi-files/'.$request->grn_id.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/pi-files/'.$request->grn_id.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hash_name = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Material Receive Image Uploaded';
                    return $response;

                }
                $response['status'] = 300;
                $response['message'] = 'Uploading failed. Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-grn-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteImageGrn(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $id = $request->image_id;
                $task = PiFile::findOrFail($id);
                if(!empty($task)){
                    $task->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Image deleted successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-image-delete',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function postConvertGrn(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $projectId = AppProject::find($request->project_id);
                $store = AppStore::where('project_id', $request->project_id)->first();
                $indent = AppIndent::find($request->indent_id);
                $ind = AppPurchaseInvoice::orderBy('id', 'DESC')->first();
                $rfq = new AppPurchaseInvoice();

                if ($ind == null) {
                    $grnNo = 'MR-' . date("d-m-Y") . '-1';
                } else {
                    $grnNo = 'MR-' . date("d-m-Y") . '-' . $ind->id;
                }
                $rfq_no = $grnNo;
                $rfq->invoice_no = $rfq_no;
                $rfq->company_id = $projectId->company_id;
                $rfq->store_id = $store->id;
                $rfq->indent_id = $request->indent_id;
                $rfq->project_id = $request->project_id;
                $rfq->supplier_id = $request->supplier_id;
                $rfq->dated = Carbon::parse($request->date)->format('Y-m-d');
                $rfq->remark = $request->remark;
                $rfq->save();
                $updaterecevied = array();
                $all_data = json_decode($request->arrays);
                foreach ($all_data as $key => $data) {
                    if (!empty($data->rec_qty)) {
                        $indPro = new PiProducts();
                        $indPro->pi_id = $rfq->id;
                        $indPro->product_id = $data->product_id;
                        $indPro->quantity = (float)$data->rec_qty;
                        $indPro->unit_id = $data->unit_id;
                        $indPro->save();

                        $sku = time();
                        if ($indPro) {
                            $stockCheck = AppStock::where('cid', $data->product_id)->where('store_id', $store->id)->where('project_id', $projectId->id)->first();
                            if (!empty($stockCheck)) {
                                $stockCheck->stock = (float)$stockCheck->stock + (float)$data->rec_qty;
                                $stockCheck->save();
                                if ($stockCheck) {
                                    $pl = new AppProductLog();
                                    $pl->store_id = $store->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $projectId->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $rfq->id;
                                    $pl->module_name = 'grn';
                                    $pl->product_id = $data->product_id;
                                    $pl->quantity = $data->rec_qty;
                                    $pl->balance_quantity = (float)$stockCheck->stock;
                                    $pl->transaction_type = 'plus';
                                    $pl->remark = $request->remark;
                                    if(isset($rfq->dated)) {
                                        $date = Carbon::parse($rfq->dated)->format('Y-m-d');
                                        $pl->date = $date;
                                    }
                                    $pl->save();
                                }
                            } else {
                                $stock = new AppStock();
                                $stock->sku = $sku;
                                $stock->po = $request->po;
                                $stock->company_id = $projectId->company_id;
                                $stock->inv_id = $rfq->id;
                                $stock->cid = $data->product_id;
                                $stock->quantity = $data->rec_qty;
                                $stock->supplier_id = $request->supplier_id;
                                $stock->stock = $data->rec_qty;
                                $stock->unit = $data->unit_id;
                                $stock->store_id = $store->id;
                                $stock->project_id = $projectId->id;
                                $stock->save();
                                if ($stock) {
                                    $pl = new AppProductLog();
                                    $pl->store_id = $store->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $projectId->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $rfq->id;
                                    $pl->module_name = 'grn';
                                    $pl->product_id = $data->product_id;
                                    $pl->quantity = $data->rec_qty;
                                    $pl->balance_quantity = $data->rec_qty;
                                    $pl->transaction_type = 'plus';
                                    $pl->remark = $request->remark;
                                    if(isset($rfq->dated)) {
                                        $date = Carbon::parse($rfq->dated)->format('Y-m-d');
                                        $pl->date = $date;
                                    }
                                    $pl->save();
                                }
                            }
                        }
                    }
                }
                $indent->status = 1;
                $indent->save();
                $updaterecevied = array();
                $indentproarray  = IndentProducts::where('indent_id',$request->indent_id)->get();
                foreach ($indentproarray as $indentpro){
                    $indgrnarray = AppPurchaseInvoice::where('indent_id',$indentpro->indent_id)->pluck('id')->toArray();
                    $piproducts = PiProducts::whereIn('pi_id',$indgrnarray)->where('product_id',$indentpro->cid)->sum('quantity');
                    if($piproducts==$indentpro->quantity){
                        $updaterecevied[] = 'true';
                    }else{
                        $updaterecevied[] = 'false';
                    }
                }

                if(!empty($updaterecevied)){
                    if(!in_array('false',$updaterecevied)){
                        $indent->status = 5;
                    }
                }
                $indent->save();
                if($rfq){
                    $response['status'] = 200;
                    $response['message'] = 'Success';
                    $response['grn_id'] = $rfq->id;
                    $response['response'] = 'Material Receive Created Successfully!';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'convert-grn',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function editConvertGrn(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $projectId = AppProject::find($request->project_id);
                $store = AppStore::where('project_id', $projectId->id)->first();
                $indent = AppIndent::find($request->indent_id);
                $ind = AppPurchaseInvoice::where('id', $request->grn_id)->first();
                if(!empty($ind)){
                    if(isset($request->supplier_id)) {
                        $ind->supplier_id = $request->supplier_id;
                        $ind->save();
                    }
                }
                $all_data = json_decode($request->arrays);
                foreach ($all_data as $key => $data) {
                    if ($request->rec_qty !== '') {
                        $indPro = PiProducts::find($data->id);
                        $prevqaunt = $indPro->quantity ?: 0;
                        $indPro->quantity = $data->rec_qty;
                        $indPro->save();
                        $sku = time();
                        if ($indPro) {
                            $stockCheck = AppStock::where('cid', $indPro->product_id)->where('store_id', $store->id)->where('project_id', $request->project_id)->first();
                            if (!empty($stockCheck)) {
                                $newstock = (float)$stockCheck->stock - (float)$prevqaunt;
                                $stockCheck->stock = (float)$newstock + (float)$data->rec_qty;
                                $stockCheck->save();
                                if ($stockCheck) {
                                    $pl = AppProductLog::where('product_id',$indPro->product_id)->where('module_id',$indPro->pi_id)->where('module_name','grn')->first();
                                    if(empty($pl)){
                                        $pl = new AppProductLog();
                                    }
                                    $pl->store_id = $store->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $projectId->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $indPro->pi_id;
                                    $pl->module_name = 'grn';
                                    $pl->product_id = $indPro->product_id;
                                    $pl->quantity = $data->rec_qty;
                                    $pl->balance_quantity = (float)$stockCheck->stock;
                                    $pl->transaction_type = 'plus';
                                    $pl->remark = $request->remark;
                                    if(isset($indPro->dated)) {
                                        $date = Carbon::parse($indPro->dated)->format('Y-m-d');
                                        $pl->date = $date;
                                    }
                                    $pl->save();
                                }
                            } else {
                                $stock = new AppStock();
                                $stock->sku = $sku;
                                $stock->po = $request->po;
                                $stock->company_id = $projectId->company_id;
                                $stock->inv_id = $indPro->pi_id;
                                $stock->cid = $indPro->product_id;
                                $stock->supplier_id = $request->supplier_id;
                                $stock->quantity = $data->rec_qty;
                                $stock->stock = $data->rec_qty;
                                $stock->unit = $indPro->unit_id;
                                $stock->store_id = $store->id;
                                $stock->project_id = $projectId->id;
                                $stock->save();

                                if ($stock) {
                                    $pl = AppProductLog::where('product_id',$indPro->product_id)->where('module_id',$indPro->pi_id)->where('module_name','grn')->first();
                                    if(empty($pl)){
                                        $pl = new AppProductLog();
                                    }
                                    $pl->store_id = $store->id;
                                    $pl->project_id = $projectId->id;
                                    $pl->company_id = $projectId->company_id;
                                    $pl->created_by = $user->id;
                                    $pl->module_id = $indPro->pi_id;
                                    $pl->module_name = 'grn';
                                    $pl->product_id = $indPro->product_id;
                                    $pl->quantity = $data->rec_qty;
                                    $pl->balance_quantity = $data->rec_qty;
                                    $pl->transaction_type = 'plus';
                                    if(isset($indPro->dated)) {
                                        $date = Carbon::parse($indPro->dated)->format('Y-m-d');
                                        $pl->date = $date;
                                    }
                                    $pl->save();
                                }
                            }
                        }
                    }
                }
                $indent->status = 1;
                $indent->save();

                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['grn_id'] = $request->grn_id;
                $response['response'] = 'Material Receive Updated Successfully!';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'convert-grn',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function grnReturn(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $projectId = AppProject::find($request->project_id);
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $store = AppStore::where('project_id', $request->project_id)->first();
                $ind = AppPurchaseInvoice::where('store_id',$store->id)->where('project_id',$projectId->id)->orderBy('id', 'ASC')->first();
                $stock = AppStock::where('store_id',$store->id)->where('project_id',$projectId->id)->where('cid',$request->product_id)->first();
                $qty = $request->quantity;
                $prretunedarray = array();
                $newqty = $qty;
                $x=1;
                $alreturnedquantity = 0;
                while($x==1){
                    $returnqtyvalue = 0;
                    $piinvoice = AppPurchaseInvoice::where('store_id',$store->id)->where('project_id',$projectId->id)->where('returned','0')->orderBy('id','asc')->first();
                    if(!empty($piinvoice)){
                        $piproduct = PiProducts::where('pi_id',$piinvoice->id)->where('product_id',$stock->cid)->first();
                        if(!empty($piproduct)){
                            $prquanity = $piproduct->quantity;
                            $returedquanity = ProductReturns::where('invoice_id',$piinvoice->id)->sum('quantity');
                            $returnqtyvalue +=$returedquanity;
                            $returnqtyvalue +=$newqty;
                            if($returnqtyvalue>=$prquanity){
                                $balquant = $prquanity-$returedquanity;
                                $prretunedarray[$piinvoice->id] = $balquant;
                                $newqty = $newqty-$balquant;
                                if($newqty<=0){
                                    if($newqty==0){
                                        $piinvoice->returned = '1';
                                        $piinvoice->save();
                                        $x=0;
                                    }
                                    $x=0;
                                }
                                $piinvoice->returned = '1';
                                $piinvoice->save();
                            }else{
                                $balqty = $prquanity-$newqty;
                                if($balqty>0){
                                    $prretunedarray[$piinvoice->id] = $newqty;
                                    $newqty = 0;
                                    if($newqty<=0){
                                        $x = 0;
                                    }
                                }elseif($balqty==0){
                                    $piinvoice->returned = '1';
                                    $piinvoice->save();
                                    $newqty = 0;
                                    if($newqty<=0){
                                        $x=0;
                                    }
                                }
                            }
                        }else{
                            $x = 0;
                        }
                    }else{
                        $x = 0;
                    }
                }
                if(empty($prretunedarray)){
                    $response['message'] = 'Material Received Not Found Please try again';
                    $response['status'] = '301';
                    return $response;
                }
                foreach ($prretunedarray as $inv =>$qty){
                    $ret = new ProductReturns();
                    $ret->invoice_id = $inv;
                    $ret->cid = $stock->cid;
                    $ret->bid = $stock->bid ?: 0;
                    $ret->quantity = $qty;
                    $ret->unit = $request->unit_id;
                    if(!empty($request->price)){
                        $ret->price = $request->price;
                    }else{
                        $ret->price = $stock->price;
                    }
                    $ret->project_id = $projectId->id;
                    $ret->company_id = $projectId->company_id;
                    $ret->store_id = $store->id;
                    $ret->supplier_id = $request->supplier_id ?: 0;
                    $ret->remark = $request->remark;
                    if(isset($request->date)) {
                        $date = Carbon::parse($request->date)->format('Y-m-d');
                        $ret->date = $date;
                    }
                    $ret->save();

                    $stock->stock = $stock->stock - $qty;
                    $stock->save();

                    $pl = new AppProductLog();
                    $pl->store_id = $store->id;
                    $pl->project_id = $projectId->id;
                    $pl->company_id = $projectId->company_id;
                    $pl->created_by = $user->id;
                    $pl->module_id = $ret->id;
                    $pl->module_name = 'grn_return';
                    $pl->product_id = $request->product_id;
                    $pl->quantity = $qty;
                    $pl->balance_quantity = $qty;
                    $pl->transaction_type = 'minus';
                    $pl->remark = $request->remark;
                    if(isset($request->date)) {
                        $date = Carbon::parse($request->date)->format('Y-m-d');
                        $pl->date = $date;
                    }
                    $pl->save();
                }
                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = 'Purchase Return Updated Successfully!';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'create-grn', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }
    public function grnReturnEdit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $projectId = AppProject::find($request->project_id);
                $date = Carbon::parse($request->date)->format('Y-m-d');
                $store = AppStore::where('project_id', $request->project_id)->first();
                $ind = AppPurchaseInvoice::where('store_id',$store->id)->where('project_id',$projectId->id)->orderBy('id', 'ASC')->first();
                $stock = AppStock::where('store_id',$store->id)->where('project_id',$projectId->id)->where('cid',$request->product_id)->first();
                $qty = $request->quantity;
                $productreturn = $request->return_id;
                $ret = ProductReturns::find($productreturn);
                if(empty($ret)){
                    $response['message'] = 'Material Return not found';
                    $response['status'] = '301';
                    return $response;
                }
                $ret->cid = $stock->cid;
                $ret->bid = $stock->bid ?: 0;
                $ret->quantity = $qty;
                $ret->unit = $request->unit_id;
                if(!empty($request->price)){
                    $ret->price = $request->price;
                }else{
                    $ret->price = $stock->price;
                }
                $ret->project_id = $projectId->id;
                $ret->company_id = $projectId->company_id;
                $ret->store_id = $store->id;
                $ret->supplier_id = $request->supplier_id;
                $ret->remark = $request->remark;
                if(isset($request->date)) {
                    $date = Carbon::parse($request->date)->format('Y-m-d');
                    $ret->date = $date;
                }
                $ret->save();

                $pl = AppProductLog::where('project_id',$ret->project_id)->where('module_id',$ret->id)->where('module_name','grn_return')->first();
                $pl->store_id = $store->id;
                $pl->project_id = $projectId->id;
                $pl->company_id = $projectId->company_id;
                $pl->created_by = $user->id;
                $pl->module_id = $ret->id;
                $pl->module_name = 'grn_return';
                $pl->product_id = $request->product_id;
                $pl->quantity = $qty;
                $pl->balance_quantity = $qty;
                $pl->transaction_type = 'minus';
                $pl->remark = $request->remark;
                if(isset($request->date)) {
                    $date = Carbon::parse($request->date)->format('Y-m-d');
                    $pl->date = $date;
                }
                $pl->save();

                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = 'Purchase Return Updated Successfully!';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e, 'edit-grn-return', $userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
    }
    public function grnReturnDetails(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $data = array();
                $returnid = $request->return_id;
                $returndetails = ProductReturns::where('id',$returnid)->first();
                if(empty($returndetails)){
                    $response['message'] = "Product return not found";
                    $response['status'] = 301;
                    return $response;
                }
                $data['id'] = $returndetails->id;
                $data['company_id'] = $returndetails->company_id;
                $data['supplier_id'] = $returndetails->supplier_id;
                $data['project_id'] = $returndetails->project_id;
                $data['invoice_id'] = $returndetails->invoice_id;
                $data['store_id'] = $returndetails->store_id;
                $data['product_id'] = $returndetails->cid;
                $data['product_name'] = get_local_product_name($returndetails->cid);
                $data['product_image'] = get_product_image_link($returndetails->cid);
                $data['bid'] = $returndetails->bid;
                $data['quantity'] = $returndetails->quantity;
                $data['unit'] = $returndetails->unit;
                $data['price'] = $returndetails->price;
                $data['tax'] = $returndetails->tax;
                $data['amount'] = $returndetails->price;
                $data['remark'] = $returndetails->remark;
                $data['datetime'] = date('d M Y',strtotime($returndetails->created_at));
                $response['status'] = 200;
                $response['message'] = 'Success';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-return-detail',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteReturnGrn(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $materialreturn = ProductReturns::where('id', $request->return_id)->first();
                if(empty($materialreturn)){
                    $response['message'] = "Product return not found";
                    $response['status'] = 301;
                    return $response;
                }
                if(!empty($materialreturn->id)){
                    ProductLog::where('module_id',$materialreturn->id)->where('module_name','grn_return')->delete();
                    $materialreturn->delete();
                }

                $response['status'] = 200;
                $response['message'] = 'Material Return Deleted Successfully!';
                $response['response'] = '';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'grn-return-delete',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}
