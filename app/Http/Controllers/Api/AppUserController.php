<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Http\Controllers\Controller;
use App\ModuleSetting;
use App\Package;
use App\ProjectMember;
use App\Promotional_fcm;
use App\User;
use Illuminate\Http\Request;
use DB;

class AppUserController extends Controller
{ 
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 300;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 300;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 300;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function usersList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $pm = ProjectMember::where('project_id',$request->project_id)->where('user_type','employee')->get()->pluck('user_id')->toArray();
//                $users = User::whereIn('id', $pm)->where('company_id',$user->company_id)->get();
                $users = User::whereIn('id', $pm)->get();
                foreach ($users as $user) {
                    $proData[] = array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'image' => $user->image,
                        'image_path' => get_users_image_link($user->id),
                        'status' => $user->status,
                        'mobile' => $user->mobile,
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Users List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'users-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function modulesList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $users = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->select('users.*', 'roles.name as rolename')->where('users.company_id',$user->company_id)->first();
                $modules = ModuleSetting::where('company_id',$user->company_id)->where('type',$users->rolename)->get();
                $response['status'] = 200;
                $response['message'] = 'Module Settings List Fetched';
                $response['response'] = $modules;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'modules-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function modulesByName(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $name = $request->module_name;
                $response = array();
                $users = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->select('users.*', 'roles.name as rolename')->where('users.company_id',$user->company_id)->first();
                $modules = ModuleSetting::where('company_id',$user->company_id)->where('type',$users->rolename)->where('module_name',$name)->first();
                $response['status'] = 200;
                $response['message'] = 'Module Settings List Fetched';
                $response['response'] = $modules;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'modules-by-name',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function modulesListByPackage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                if(!empty($user->company_id)){
                    $companydetails = Company::find($user->company_id);
                    if(!empty($companydetails->id)) {
                        $packagedetails = Package::find($companydetails->package_id);
                        if($packagedetails->id) {
                            $modules = json_decode($packagedetails->module_in_package);
                            $response['status'] = 200;
                            $response['message'] = 'Module Settings List Fetched';
                            $response['response'] = $modules;
                            return $response;
                         }
                        $response['status'] = 300;
                        $response['message'] = 'Package Not found';
                        return $response;
                     }
                }
                $response['status'] = 300;
                $response['message'] = 'Company Not found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'package-modules-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function validateFcm(Request $request){
        if (!empty($request->API_KEY)) {
            try{
                $response = array();
                $platform = $request->medium;
                $fcm = $request->fcm;
                if(!empty($platform)&&!empty($fcm)){
                    if($platform=='Android'){
                        $existingfcm = Promotional_fcm::where('fcm_id',$request->fcm)->first();
                    }elseif($platform=='Ios'){
                        $existingfcm = Promotional_fcm::where('iosfcm_id',$request->fcm)->first();
                    }

                    if(empty($existingfcm)) {
                        $insertfcm = new Promotional_fcm();
                        if($platform=='Android'){
                            $insertfcm->fcm_id = $request->fcm;
                        }elseif($platform=='Ios'){
                            $insertfcm->iosfcm_id = $request->fcm;
                        }

                        $insertfcm->save();
                        $response['status'] = 200;
                        $response['message'] = 'FCM Saved Successfully!!';
                        return $response;
                    }else{
                        $response['status'] = 200;
                        $response['message'] = 'FCM Already Exists';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'FCM Not found';
                return $response;
            } catch (\Exception $e) {
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}