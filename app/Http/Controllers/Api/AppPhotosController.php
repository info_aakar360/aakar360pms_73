<?php

namespace App\Http\Controllers\Api;

use App\AppFileManager;
use App\AppProject;
use App\Company;
use App\FileManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFileLink;
use App\ProjectCategory;
use App\Project;
use App\TaskFile;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;

class AppPhotosController extends Controller
{
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-deo',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    use AuthenticatesUsers;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
//        return response()->json(['message' => 'Unauthorised'], 300);
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function folderLists(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $filesprojects = array();
                $pathtype = $request->pathtype;
                $projectfilesarray = explode(',',$request->project_id);
                $projectarray = AppProject::whereIn('id',$projectfilesarray)->get();
                foreach ($projectarray as $project){
                    $allprojects = AppFileManager::where('project_id',$project->id)->where('type','folder')->where('parent','0')->where('pathtype',$pathtype)->first();
                    if(empty($allprojects->id)){
                        $newfolder = new AppFileManager();
                        $newfolder->project_id = $project->id;
                        $newfolder->user_id = $user->id;
                        $newfolder->company_id = $user->company_id;
                        $newfolder->filename =  $project->project_name;
                        $newfolder->type = 'folder';
                        $newfolder->parent = '0';
                        $newfolder->revisionfileid = '0';
                        $newfolder->foldertype = 'public';
                        $newfolder->pathtype = $pathtype;
                        $newfolder->save();
                    }
                    $filesprojects[] = $project->id;
                }
                $filemanager = array();
                $storage = storage();
                $url = awsurl();
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    $filemanagerlist =  AppFileManager::whereIn('project_id',$filesprojects)->where('type','folder')->where('parent','0')->where('revisionfileid','0')->where('pathtype',$pathtype)->orderBy('filename', 'ASC')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $filemanagerlist =  AppFileManager::whereIn('project_id',$filesprojects)->where('type','folder')->where('parent','0')->where('revisionfileid','0')->where('pathtype',$pathtype)->orderBy('filename', 'ASC')->offset($skip)->take($count)->get();
                }
               $documentslist = array();
                foreach ($filemanagerlist as $filemanager){
                    $showlist = true;
                    if($filemanager->foldertype=='private'&&$user->id!=$filemanager->user_id){
                        $showlist = false;
                    }
                    $projectdetails = Project::where('id',$filemanager->project_id)->first();
                    $shared = 1;
                    if(!empty($projectdetails)){
                        if($projectdetails->project_admin==$user->id){
                            $shared = 0;
                        }
                    }
                    if($showlist){
                    $documents = array();
                    $documents['id'] = $filemanager->id;
                    $documents['company_id'] = $filemanager->company_id;
                    $documents['project_id'] = $filemanager->project_id;
                    $documents['user_id'] = $filemanager->user_id;
                    $documents['username'] = get_user_name($filemanager->user_id);
                    $documents['userimage'] = get_users_image_link($filemanager->user_id);
                    $documents['pathtype'] = $filemanager->pathtype;
                    $documents['foldertype'] = $filemanager->foldertype ?: 'public';
                    $documents['name'] = $filemanager->name;
                    $documents['filename'] = $filemanager->filename;
                    $documents['hashname'] = $filemanager->hashname;
                    $documents['type'] = $filemanager->type;
                    $documents['parent'] = $filemanager->parent;
                    $documents['description'] = $filemanager->description;
                    $documents['locked'] = $filemanager->locked;
                    $documents['shared'] = $shared;
                    $documents['created_at'] = Carbon::parse($filemanager->created_at)->format('d M Y');
                    $documents['createdby'] = '0';
                    if($user->id==$filemanager->user_id){
                        $documents['createdby'] = '1';
                    }
                    $documentslist[] = $documents;
                    }
                }
                $response['documentslist'] = $documentslist;
                $response['categories'] = ProjectCategory::where('company_id',$user->company_id)->get();
                $response['status'] = 200;
                $response['message'] = 'Document list Fetched';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'folder-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function photosLists(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $pathtype = $request->pathtype;
                $projectid = $request->project_id;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 200;
                    return $response;
                }

                $folderpath = $request->folderid ?: 0;
                $revisionid = $request->revisionid ?: 0;
                $filemanager = array();
                $storage = storage();
                $url = awsurl();
                if(!empty($folderpath)&&$folderpath>0){
                    $folderid = $folderpath;
                }else{
                    $parentfolder = AppFileManager::where('parent',0)->where('pathtype',$pathtype)->where('project_id',$projectid)->where('type','folder')->first();
                    if(empty($parentfolder->id)){
                        $newfolder = new AppFileManager();
                        $newfolder->project_id = $projectdetails->id;
                        $newfolder->user_id = $user->id;
                        $newfolder->company_id = $user->company_id;
                        $newfolder->filename =  $projectdetails->project_name;
                        $newfolder->type = 'folder';
                        $newfolder->parent = '0';
                        $newfolder->revisionfileid = '0';
                        $newfolder->pathtype = $pathtype;
                        $newfolder->save();
                        $folderid = $newfolder->id;
                    }else{
                        $folderid = $parentfolder->id;
                    }
                }
                $foldermanagerlist = AppFileManager::where('removed','0')->where('parent',$folderid)->where('pathtype',$pathtype);
                $filemanagerlist = AppFileManager::where('removed','0')->where('parent',$folderid)->where('pathtype',$pathtype);
                if(!empty($projectid)){
                    $foldermanagerlist = $foldermanagerlist->where('project_id',$projectid);
                    $filemanagerlist = $filemanagerlist->where('project_id',$projectid);
                }
                if(!empty($revisionid)){
                    $foldermanagerlist = $foldermanagerlist->where('revisionfileid',$revisionid);
                    $filemanagerlist = $filemanagerlist->where('revisionfileid',$revisionid);
                }else{
                    $foldermanagerlist = $foldermanagerlist->where('revisionfileid','0');
                    $filemanagerlist = $filemanagerlist->where('revisionfileid','0');
                }

                $allfolder = $foldermanagerlist->where('type','folder')->orderBy('id', 'desc')->get();
                $allfiles = $filemanagerlist->where('type','file')->orderBy('id', 'desc')->get();
                if(!empty($revisionid)){
                    $revisionfileid = AppFileManager::where('id',$revisionid)->where('pathtype',$pathtype)->get();
                    $allfiles = $allfiles->merge($revisionfileid);
                }
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    if(!empty($request->doc_type)&&$request->doc_type=='folder'){
                        $filemanagerlist = $allfolder;
                    }else{
                        $filemanagerlist = $allfolder->merge($allfiles);
                    }
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    if(!empty($request->doc_type)&&$request->doc_type=='folder'){
                        $filemanagerlist = $allfolder->forPage($page,$count);
                    }else{
                        $filemanagerlist = $allfolder->merge($allfiles)->forPage($page,$count);
                    }
                }
               $documentslist = array();

                foreach ($filemanagerlist as $filemanager){
                    $showlist = true;
                    if($filemanager->foldertype=='private'&&$user->id!=$filemanager->user_id){
                        $showlist = false;
                    }
                    if($showlist){
                        $documents =   $this->fileinfo($filemanager,$user,$revisionid);
                    $documentslist[] = $documents;
                    }
                }

                $response['documentslist'] = $documentslist;
                $response['categories'] = ProjectCategory::where('company_id',$user->company_id)->get();
                $response['status'] = 200;
                $response['message'] = 'Document list Fetched';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'photos-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function imagesLists(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $pathtype = $request->pathtype;
                $projectid = $request->project_id;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 200;
                    return $response;
                }

                $folderpath = $request->folderid ?: 0;
                $revisionid = $request->revisionid ?: 0;
                $filemanager = array();
                $storage = storage();
                $url = awsurl();
                if(!empty($folderpath)&&$folderpath>0){
                    $folderid = $folderpath;
                }else{
                    $parentfolder = AppFileManager::where('parent',0)->where('pathtype',$pathtype)->where('project_id',$projectid)->where('type','folder')->first();
                    if(empty($parentfolder->id)){
                        $newfolder = new AppFileManager();
                        $newfolder->project_id = $projectdetails->id;
                        $newfolder->user_id = $user->id;
                        $newfolder->company_id = $user->company_id;
                        $newfolder->filename =  $projectdetails->project_name;
                        $newfolder->type = 'folder';
                        $newfolder->parent = '0';
                        $newfolder->revisionfileid = '0';
                        $newfolder->pathtype = $pathtype;
                        $newfolder->save();
                        $folderid = $newfolder->id;
                    }else{
                        $folderid = $parentfolder->id;
                    }
                }
                $response =  $documentsdata =  $documentsarray =  $documentslist = array();
                 $filemanagerlist = AppFileManager::where('type','file')->where('pathtype','photos')
                        ->where(function ($query) use ($request) {
                            $query->where('hashname', "like", "%.jpeg%");
                            $query->orWhere('hashname', "like", "%.jpg%");
                            $query->orWhere('hashname', "like", "%.png%");
                        });
                    if(!empty($projectid)){
                        $filemanagerlist = $filemanagerlist->where('project_id',$projectid);
                    }
                    $currenfilemanagerlist = $filemanagerlist->orderBy('id','desc')->get()->groupBy(function ($item, $key) {
                        return $item->created_at->format('d M Y');
                    });
                    foreach ($currenfilemanagerlist as $curdate => $filemanagerarray){
                        $documentslist = array();
                        foreach ($filemanagerarray as  $filemanager){
                            $showlist = true;
                            if($filemanager->foldertype=='private'&&$user->id!=$filemanager->user_id){
                                $showlist = false;
                            }
                            if($showlist){
                                $documents =   $this->fileinfo($filemanager,$user,'0');
                                $documentslist[] = $documents;
                            }
                        }
                        if(count($documentslist)>0){
                            $documentsarray['date'] = $curdate;
                            $documentsarray['documents'] = $documentslist;
                            $documentsdata[] = $documentsarray;
                        }
                    }
                $response['documentslist'] = $documentsdata;
                $response['categories'] = ProjectCategory::where('company_id',$user->company_id)->get();
                $response['status'] = 200;
                $response['message'] = 'Document list Fetched';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'photos-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function photosStore(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $folderpath = $request->folderid ?: 0;
                $pathtype = $request->pathtype;
                $projectid = $request->project_id ?: 0;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 401;
                    return $response;
                }
                $prevfile = AppFileManager::where('company_id',$user->company_id)->where('project_id',$projectdetails->id)->where('name',$request->name)->where('type','file')->where('pathtype',$pathtype)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'File name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                if(!empty($folderpath)&&$folderpath>0){
                    $folderid = $folderpath;
                }else{
                    $parentfolder = AppFileManager::where('parent',0)->where('pathtype',$pathtype)->where('project_id',$projectid)->where('type','folder')->first();
                    if(empty($parentfolder->id)){
                        $newfolder = new AppFileManager();
                        $newfolder->project_id = $projectdetails->id;
                        $newfolder->user_id = $user->id;
                        $newfolder->company_id = $user->company_id;
                        $newfolder->filename =  $projectdetails->project_name;
                        $newfolder->type = 'folder';
                        $newfolder->parent = '0';
                        $newfolder->revisionfileid = '0';
                        $newfolder->pathtype = $pathtype;
                        $newfolder->save();
                        $folderid = $newfolder->id;
                    }else{
                        $folderid = $parentfolder->id;
                    }
                }
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $revusionfile = AppFileManager::find($request->revisionid);
                    $file = new AppFileManager();
                    $file->company_id = $user->company_id;
                    $file->user_id = $user->id;
                    $file->name = $request->name;
                    $file->description = $request->description;
                     $file->project_id = $projectid ?: 0;
                    $npid = 0;
                    switch($storage) {
                        case 'local':
                            if(!empty($request->project_id)&&!empty($folderid)) {
                                $x = 1;
                                $parentfolder = '';
                                $pid = $folderid;
                                if($pid){
                                    $pn = AppFileManager::where('project_id', $request->project_id)->where('id', $pid)->where('type', 'folder')->first();
                                    $npid = 0;
                                    if($pn === null){
                                        while($pid != 0){
                                            $pnx = AppFileManager::where('id', $pid)->where('type', 'folder')->first();
                                            $pid =$pnx->parent;
                                        }
                                        $npid = 0;
                                        while($x==1) {
                                            $pn = AppFileManager::where('id', $pnx->id)->where('type', 'folder')->first();
                                            if($pn === null){
                                                $x=0;
                                            }else{
                                                $pnn = $pn->replicate();
                                                $pnn->project_id = $request->project_id;
                                                $pnn->parent = $npid;
                                                $pnn->save();
                                                $pnx = AppFileManager::where('project_id', $pnx->project_id)->where('type', 'folder')->where('parent', $pnx->id)->first();
                                                $npid = $pnn->id;
                                                if($pnx === null){
                                                    $x=0;
                                                }
                                            }
                                        }
                                    }
                                    $x=1;
                                    while($x==1) {
                                        $parentname = AppFileManager::where('id', $pid)->first();
                                        if($parentname !== null){
                                            if($parentfolder != '') {
                                                $parentfolder = $parentname->id . '/' . $parentfolder;
                                            }else{
                                                $parentfolder= $parentname->id;
                                            }
                                            $pid = $parentname->parent;
                                        }else{
                                            $x = 0;
                                        }
                                    }
                                }
                                $image =  $request->file('file');
                                $extension = $image->getClientOriginalExtension();
                                if(!empty($request->extension)){
                                    $extension = $request->extension;
                                }
                                $ext = explode('.',$image->hashName());
                                if(!empty($ext[1])&&$ext[1]=='bin'){
                                    $hashname = $ext[0].'.'.$extension;
                                }else{
                                    $hashname = $image->hashName();
                                }
                                $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentfolder;
                                if (!file_exists($destinationPath)) {
                                    mkdir($destinationPath, 0777, true);
                                }
                                $image->storeAs($destinationPath, $hashname);

                                if($extension=='png'||$extension=='jpg'||$extension=='jpeg'){
                                    $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentfolder.'/thumbnail';
                                    if (!file_exists($destinationPath)) {
                                        mkdir($destinationPath, 0777, true);
                                    }

                                    $img1 = Image::make($image->getRealPath());
                                    $img1->resize(400, 400, function ($constraint) {
                                        $constraint->aspectRatio();
                                    })->save($destinationPath.'/'.$hashname);
                                 }
                            }else {
                                $parentname = AppFileManager::where('id', $folderid)->first();
                                $destinationPath = 'uploads/project-files/'.$projectid.'/'.$parentname->id;
                                $request->file->move($destinationPath, $request->file->hashName());
                            }
                            break;
                        case 's3':
                            if(!empty($request->project_id)&&!empty($folderid)) {
                                $x = 1;
                                $parentfolder = '';
                                $pid = $folderid;
                                if($pid){
                                $pn = AppFileManager::where('project_id', $request->project_id)->where('id', $pid)->where('type', 'folder')->first();
                                $npid = 0;
                                if($pn === null){
                                    while($pid != 0){
                                        $pnx = AppFileManager::where('id', $pid)->where('type', 'folder')->first();
                                        $pid =$pnx->parent;
                                    }
                                    $npid = 0;
                                    while($x==1) {
                                        $pn = AppFileManager::where('id', $pnx->id)->where('type', 'folder')->first();
                                        if($pn === null){
                                            $x=0;
                                        }else{
                                            $pnn = $pn->replicate();
                                            $pnn->project_id = $request->project_id;
                                            $pnn->parent = $npid;
                                            $pnn->save();
                                            $pnx = AppFileManager::where('project_id', $pnx->project_id)->where('type', 'folder')->where('parent', $pnx->id)->first();
                                            $npid = $pnn->id;
                                            if($pnx === null){
                                                $x=0;
                                            }
                                        }
                                    }
                                }
                                $x=1;
                                    while($x==1) {
                                        $parentname = AppFileManager::where('id', $pid)->first();
                                        if($parentname !== null){
                                            if($parentfolder != '') {
                                                $parentfolder = $parentname->id . '/' . $parentfolder;
                                            }else{
                                                $parentfolder= $parentname->id;
                                            }
                                            $pid = $parentname->parent;
                                        }else{
                                            $x = 0;
                                        }
                                    }
                                }
                                $image =  $request->file;
                                $extension = $image->getClientOriginalExtension();
                                if(!empty($request->extension)){
                                    $extension = $request->extension;
                                }
                                Storage::disk('s3')->putFileAs('project-files/'.$projectid.'/'.$parentfolder, $image, $image->hashName(), 'public');
                                if($extension=='png'||$extension=='jpg'||$extension=='jpeg'){
                                    $img = Image::make($image->getRealPath());
                                    $img->resize(400, 400, function ($constraint) {
                                        $constraint->aspectRatio();
                                    });
                                    Storage::disk('s3')->put('project-files/'.$projectid.'/'.$parentfolder.'/thumbnail/'.$image->hashName(), $img->stream()->detach());
                                }
                            }else {
                                $parentname = AppFileManager::where('id', $folderid)->first();
                                Storage::disk('s3')->putFileAs('project-files/'.$projectid.'/'. $parentname->id, $request->file, $request->file->hashName(), 'public');
                            }
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'project-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('project-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->parent)
                                ->first();
                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $request->parent)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $request->file, $request->file->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$request->file->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }
                    $file->filename = $request->file->getClientOriginalName();
                    $file->hashname = $hashname;
                    $file->size = $request->file->getSize();
                    $file->revisionfileid = !empty($revusionfile->id) ? $revusionfile->id : 0;
                    $file->task_file_id = !empty($revusionfile->task_file_id) ? $revusionfile->task_file_id : 0;
                    $file->type = 'file';
                    $file->foldertype =  'public';
                    $file->pathtype = $pathtype;
                    $file->parent = $folderid;
                    $file->save();


                    if($pathtype=='outline'){
                        $response['message'] = 'Drawing Added Successfully';
                        $pathname = 'Drawing File';
                    }
                    if($pathtype=='photos'){
                        $response['message'] = 'Photo Added Successfully';
                        $pathname = 'Photo Image';
                    }
                    if($pathtype=='documents'){
                        $response['message'] = 'Documents Added Successfully';
                        $pathname = 'Documents';
                    }

                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $projectid)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = $pathname;
                        $notifmessage['body'] = $pathname.' has uploaded in '.get_project_name($projectid).' project by '.$user->name;
                        $notifmessage['activity'] = $pathtype;
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }
                    $response['status'] = 200;
                    return $response;
                }else{
                    $response['message'] = 'Documents Not Found';
                    $response['status'] = 300;
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'photos-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeMultiple(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $folderid = $request->folderid ?: 0;
                $pathtype = $request->pathtype;
                $projectid = $request->project_id;
                if ($request->hasFile('file')) {
                    foreach ($request->file as $fileData){
                        $storage = storage();
                        $file = new AppFileManager();
                        $file->company_id = $user->company_id;
                        $file->user_id = $user->id;
                        if(isset($request->project_id)) {
                            $file->project_id = $request->project_id;
                        }
                        switch($storage) {
                            case 'local':
                                if(isset($request->project_id)) {
                                    $proname = AppFileManager::where('project_id',$request->project_id)->where('parent','0')->first();
                                    $parentname = AppFileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                                    $parentfolder = $parentname->id;

                                    $fileData->storeAs('uploads/project-files/'.$proname->id.'/'.$parentfolder, $fileData->hashName());
                                }else {
                                    $parentname = AppFileManager::where('project_id',$request->project_id)->where('id',$request->parent)->first();
                                    $fileData->storeAs('uploads/project-files/' . $parentname->id, $fileData->hashName());
                                }
                                break;
                            case 's3':
                                Storage::disk('s3')->putFileAs('project-files/'.$request->parent, $fileData, $fileData->getClientOriginalName(), 'public');
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', 'project-files')
                                    ->first();
                                if(!$dir) {
                                    Storage::cloud()->makeDirectory('project-files');
                                }
                                $directory = $dir['path'];
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $request->parent)
                                    ->first();
                                if ( ! $directory) {
                                    Storage::cloud()->makeDirectory($dir['path'].'/'.$request->parent);
                                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                    $directory = $contents->where('type', '=', 'dir')
                                        ->where('filename', '=', $request->parent)
                                        ->first();
                                }
                                Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());
                                $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());
                                break;
                            case 'dropbox':
                                Storage::disk('dropbox')->putFileAs('project-files/'.$request->parent.'/', $fileData, $fileData->getClientOriginalName());
                                $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                                $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                    [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/'.$request->parent.'/'.$fileData->getClientOriginalName()]]
                                );
                                $dropboxResult = $res->getBody();
                                $dropboxResult = json_decode($dropboxResult, true);
                                $file->dropbox_link = $dropboxResult['url'];
                                break;
                        }
                        $file->filename = $fileData->getClientOriginalName();
                        $file->hashname = $fileData->hashName();
                        $file->size = $fileData->getSize();
                        $file->revisionfileid = $request->revisionid ?: 0;
                        $file->foldertype =  'public';
                        $file->type = 'file';
                        $file->pathtype = $pathtype;
                        $file->parent = $folderid;
                        $file->save();
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Photo Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'multiple-upload',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                 $filename = AppFileManager::where('id',$request->id)->first();
                 if(!empty($filename)){
                     AppFileManager::where('parent',$request->id)->delete();
                     AppFileManager::where('revisionfileid',$request->id)->delete();
                     TaskFile::where('filemanager_id',$filename->id)->delete();
                     $filename->delete();
                 }
                $response['status'] = 200;
                $response['message'] = 'File Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-photo',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function download(Request $request) {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $storage = storage();
                $file = AppFileManager::findOrFail($request->id);
                switch($storage) {
                    case 'local':
                        $parent = AppFileManager::where('id',$file->parent)->first();
                        if($parent->parent !== '0'){
                            $second = AppFileManager::where('id',$parent->parent)->first();
                            return response()->download('uploads/project-files/'.$second->id.'/'.$parent->id.'/'.$file->hashname, $file->filename);
                        }
                        break;
                    case 's3':
                        $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                        $fs = Storage::getDriver();
                        $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                        return Response::stream(function() use($stream) {
                            fpassthru($stream);
                        }, 200, [
                            "Content-Type" => $ext,
                            "Content-Length" => $file->size,
                            "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                        ]);
                        break;
                    case 'google':
                        $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                        $dir = '/';
                        $recursive = false; // Get subdirectories also?
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'project-files')
                            ->first();

                        $direct = $directory['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($direct, $recursive));
                        $directo = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $file->parent)
                            ->first();

                        $readStream = Storage::cloud()->getDriver()->readStream($directo['path']);
                        return response()->stream(function () use ($readStream) {
                            fpassthru($readStream);
                        }, 200, [
                            'Content-Type' => $ext,
                            'Content-disposition' => 'attachment; filename="'.$file->filename.'"',
                        ]);
                        break;
                    case 'dropbox':
                        $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
                        $fs = Storage::getDriver();
                        $stream = $fs->readStream('project-files/'.$file->parent.'/'.$file->filename);
                        return Response::stream(function() use($stream) {
                            fpassthru($stream);
                        }, 200, [
                            "Content-Type" => $ext,
                            "Content-Length" => $file->size,
                            "Content-disposition" => "attachment; filename=\"" .basename($file->filename) . "\"",
                        ]);
                        break;
                }
                $response['status'] = 200;
                $response['message'] = 'Photo Download Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'download-photo',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeLink(StoreFileLink $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $file = new FileManager();
                $file->user_id = $user->id;
                $file->company_id = $user->company_id;
                if(isset($request->project_id)) {
                    $file->project_id = $request->project_id;
                }
                $file->parent = $request->parent;
                $file->external_link = $request->external_link;
                $file->filename = $request->filename;
                $file->revisionfileid = $request->revisionid;
                $file->save();
                if(isset($request->project_id)) {
                    $this->logProjectActivity($request->project_id, __('messages.newFileUploadedToTheProject'));
                }
                $response['status'] = 200;
                $response['message'] = 'Photo Link Stored Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-link',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createFolder(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id ?: 0;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 401;
                    return $response;
                }
                $folderpath = $request->folderid;
                $name = trim($request->foldername);
                $pathtype = trim($request->pathtype);
                $prevfile = AppFileManager::where('company_id',$projectdetails->company_id)->where('project_id',$projectdetails->id)->where('filename',$name)->where('pathtype',$pathtype)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'Folder name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                if(!empty($folderpath)&&$folderpath>0){
                    $folderid = $folderpath;
                }else{
                    $parentfolder = AppFileManager::where('parent',0)->where('pathtype',$pathtype)->where('project_id',$projectdetails->id)->where('type','folder')->first();
                    $folderid = $parentfolder->id;
                }
                $folder = new AppFileManager();
                $folder->project_id = $projectdetails->id;
                $folder->filename = $request->foldername;
                $folder->user_id = $user->id;
                $folder->company_id = $projectdetails->company_id;
                $folder->name = $request->name;
                $folder->description = $request->description;
                $folder->parent = $folderid;
                $folder->type = 'folder';
                $folder->pathtype = $request->pathtype ?: 'photos';
                $folder->foldertype = $request->folder_type ?: 'public';
                $folder->save();

                if($pathtype=='outline'){
                    $pathname = 'Drawings';
                }
                if($pathtype=='photos'){
                    $pathname = 'Photos';
                }
                if($pathtype=='documents'){
                    $pathname = 'Documents';
                }

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $projectdetails->id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = $pathname;
                    $notifmessage['body'] = 'Folder is created in '.$pathname.' on '.$projectdetails->project_name.' project by '.$user->name;
                    $notifmessage['activity'] = $pathtype;
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }


                $response['status'] = 200;
                $response['message'] = 'Folder Created Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-folder',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function editFolder(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id ?: 0;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 401;
                    return $response;
                }
                $id = $request->id;
                $name = trim($request->foldername);
                $pathtype = trim($request->pathtype);
                $prevfile = AppFileManager::where('company_id',$projectdetails->company_id)->where('project_id',$projectdetails->id)->where('filename',$name)->where('pathtype',$pathtype)->where('id','<>',$id)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'Folder name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                $folder = AppFileManager::find($request->id);
                $folder->filename = $name;
                $folder->foldertype = $request->folder_type;
                $folder->save();

                if($pathtype=='outline'){
                    $pathname = 'Drawings';
                }
                if($pathtype=='photos'){
                    $pathname = 'Photos';
                }
                if($pathtype=='documents'){
                    $pathname = 'Documents';
                }

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $folder->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = $pathname;
                    $notifmessage['body'] = 'Folder name is updated in '.$pathname.' on '.get_project_name($folder->project_id).' project by '.$user->name;
                    $notifmessage['activity'] = $pathtype;
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Folder Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'edit-folder',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function updateFileManagerName(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $id = $request->folderid;
                $name = trim($request->name);
                $prevfile = AppFileManager::where('name',$name)->where('id','<>',$id)->first();
                if(!empty($prevfile->id)){
                    $response['message'] = 'File name already exits';
                    $response['status'] = 300;
                    return $response;
                }
                $folder = AppFileManager::find($id);
                $pathtype = $folder->pathtype;
                $folder->name = $name;
                $folder->description = $request->description;
                $folder->save();

                if($pathtype=='outline'){
                    $pathname = 'Drawings';
                }
                if($pathtype=='photos'){
                    $pathname = 'Photos';
                }
                if($pathtype=='documents'){
                    $pathname = 'Documents';
                }

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $folder->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = $pathname;
                    $notifmessage['body'] = 'File name is updated in '.$pathname.' on '.get_project_name($folder->project_id).' project by '.$user->name;
                    $notifmessage['activity'] = $pathtype;
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Name Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-filemanagername',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteFolder(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $id = $request->folder_id;
                $folpr = AppFileManager::where('parent',$id)->first();
                if(empty($folpr->id)){
                    $file = AppFileManager::where('id',$id)->first();
                    $file->delete();
                    $response['status'] = 200;
                    $response['message'] = ucfirst($file->type).' Removed Successfully';
                    return $response;
                }
                $response['status'] = 300;
                $response['message'] = 'Please delete dependent files';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-folder',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function removeFile(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $id = explode(',',$request->id);
                 AppFileManager::whereIn('id',$id)->update(['removed'=>'1']);
                $response['status'] = 200;
                $response['message'] = 'File Removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'remove-file',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function movePathType(Request $request){
        $pathtype = $request->pathtype;
        $folder = AppFileManager::find($request->fileid);
        $folder->pathtype = $pathtype;
        $folder->save();
    }

    public function moveFolder(Request $request){

        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id ?: 0;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 401;
                    return $response;
                }
                $folderid = $request->folderid;
                $destfolderid = $request->moving_folderid;
                $parent_id = $request->parent_id;
                $revisionid = $request->revisionid;
                $project_id = $request->project_id;
                $medium = $request->medium; //android
                $files = $request->file; // example 1,2,3,4,5
                $pathtype = trim($request->pathtype);
               /* if(!empty($files)){
                    $rivisionids = AppFileManager::whereIn('revisionfileid',$arrayfile)->where('project_id',$project_id)->pluck('id')->toArray();
                    $newarrayfile = array_merge($arrayfile,$rivisionids);
                    $prevfiles = AppFileManager::whereIn('id',$newarrayfile)->where('project_id',$project_id)->get();
                }else{
                    $prevfiles = AppFileManager::where('id',$folderid)->where('project_id',$project_id)->get();
                }*/
                $arrayfile = explode(',',$files);
                $filesarray = AppFileManager::whereIn('id',$arrayfile)->get();
                foreach($filesarray as $files){
                    if(!empty($files)){
                        if($files->type=='folder'){
                            $folder = '';
                            $x=1;
                            $p = $files->parent;
                            while($x==1){
                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $folder = $fn->id.'/'.$folder;
                                    $p = $fn->parent;
                                }else{
                                    $x=0;
                                }
                            }
                            $source = $folder.$files->id.'/';

                            $descfolder = '';
                            $x=1;
                            $p = $destfolderid;
                            while($x==1){
                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $descfolder = $fn->id.'/'.$descfolder;
                                    $p = $fn->parent;
                                }else{
                                    $x=0;
                                }
                            }
                            $desti = $descfolder.$files->id.'/';
                            $parentfolder = '';
                            $p = $files->id;
                            $x=1;
                            while($x==1){
                                $fn = \App\AppFileManager::where('parent', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $parentfolder = $parentfolder.'/'.$fn->id;
                                    $p = $fn->id;
                                }else{
                                    $x=0;
                                }
                            }
                           $sourcepath = $source.$parentfolder;
                            $destpath = $desti.$parentfolder;
                            $folderarray = array_reverse(array_filter(explode('/',$files->id.$parentfolder)));
                            if(!empty($folderarray)){
                                foreach($folderarray as $folder){
                                    $getfilesarray = AppFileManager::where('parent', $folder)->where('type', 'file')->get();
                                    foreach ($getfilesarray as $getfiles){
                                        $filesarray = array();
                                        $filesarray['file'] = $getfiles;
                                        $filesarray['destfolderid'] = $folder;
                                        $filesarray['source'] = $sourcepath;
                                        $filesarray['destination'] = $destpath;
                                         $this->filemovepath($filesarray);
                                    }
                                    $sourcearray =  array_filter(explode('/',$sourcepath));
                                    array_pop($sourcearray);
                                    $sourcepath = implode('/',$sourcearray);

                                    $destarray =  array_filter(explode('/',$destpath));
                                    array_pop($destarray);
                                    $destpath = implode('/',$destarray);
                                }
                            }else{
                                $getfilesarray = AppFileManager::where('parent', $files->id)->where('type', 'file')->get();
                                if(count($getfilesarray)>0){
                                    foreach ($getfilesarray as $getfiles){
                                        $filesarray = array();
                                        $filesarray['file'] = $getfiles;
                                        $filesarray['destfolderid'] = $files->id;
                                        $filesarray['source'] = $sourcepath;
                                        $filesarray['destination'] = $destpath;
                                        $this->filemovepath($filesarray);
                                    }
                                }
                            }

                            $files->parent = $destfolderid;
                            $files->save();
                        }elseif($files->type=='file'){
                            if($destfolderid>0){
                                $filesarray = array();
                                $filesarray['file'] = $files;
                                $filesarray['destfolderid'] = $destfolderid;
                                $this->filemove($filesarray);
                            }else{
                                $dstfolter = AppFileManager::where('pathtype',$pathtype)->where('type','folder')->where('parent','0')->where('project_id',$project_id)->first();
                                $filesarray = array();
                                $filesarray['file'] = $files;
                                $filesarray['destfolderid'] = !empty($dstfolter->id) ? $dstfolter->id : 0;
                                $this->filemove($filesarray);
                            }
                        }
                    }
                }
                if($pathtype=='outline'){
                    $pathname = 'Drawings';
                }
                if($pathtype=='photos'){
                    $pathname = 'Photos';
                }
                if($pathtype=='documents'){
                    $pathname = 'Documents';
                }
                if(!empty($files)){
                    $response['status'] = 200;
                    $response['message'] = 'File Moved Successfully';
                    return $response;
                }else{
                    $response['status'] = 200;
                    $response['message'] = 'Folder Moved Successfully';
                    return $response;
                }

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'move-folder',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function copyFile(Request $request){

        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id ?: 0;
                $projectdetails = AppProject::where('id',$projectid)->first();
                if(empty($projectdetails)){
                    $response['message'] = 'Project Not Found';
                    $response['status'] = 401;
                    return $response;
                }
                $folderid = $request->folderid;
                $destfolderid = $request->moving_folderid;
                $parent_id = $request->parent_id;
                $revisionid = $request->revisionid;
                $project_id = $request->project_id;
                $medium = $request->medium; //android
                $files = $request->file; // example 1,2,3,4,5
                $pathtype = trim($request->pathtype);
               /* if(!empty($files)){
                    $rivisionids = AppFileManager::whereIn('revisionfileid',$arrayfile)->where('project_id',$project_id)->pluck('id')->toArray();
                    $newarrayfile = array_merge($arrayfile,$rivisionids);
                    $prevfiles = AppFileManager::whereIn('id',$newarrayfile)->where('project_id',$project_id)->get();
                }else{
                    $prevfiles = AppFileManager::where('id',$folderid)->where('project_id',$project_id)->get();
                }*/
                $arrayfile = explode(',',$files);
                $filesarray = AppFileManager::whereIn('id',$arrayfile)->get();
                foreach($filesarray as $files){
                    if(!empty($files)){
                        if($files->type=='folder'){
                            $folder = '';
                            $x=1;
                            $p = $files->parent;
                            while($x==1){
                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $folder = $fn->id.'/'.$folder;
                                    $p = $fn->parent;
                                }else{
                                    $x=0;
                                }
                            }
                            $source = $folder.$files->id.'/';

                            $descfolder = '';
                            $x=1;
                            $p = $destfolderid;
                            while($x==1){
                                $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $descfolder = $fn->id.'/'.$descfolder;
                                    $p = $fn->parent;
                                }else{
                                    $x=0;
                                }
                            }
                            $desti = $descfolder.$files->id.'/';
                            $parentfolder = '';
                            $p = $files->id;
                            $x=1;
                            while($x==1){
                                $fn = \App\AppFileManager::where('parent', $p)->where('type', 'folder')->first();
                                if(!empty($fn)){
                                    $parentfolder = $parentfolder.'/'.$fn->id;
                                    $p = $fn->id;
                                }else{
                                    $x=0;
                                }
                            }
                           $sourcepath = $source.$parentfolder;
                            $destpath = $desti.$parentfolder;
                            $folderarray = array_reverse(array_filter(explode('/',$files->id.$parentfolder)));
                            if(!empty($folderarray)){
                                foreach($folderarray as $folder){
                                    $getfilesarray = AppFileManager::where('parent', $folder)->where('type', 'file')->get();
                                    foreach ($getfilesarray as $getfiles){
                                        $filesarray = array();
                                        $filesarray['file'] = $getfiles;
                                        $filesarray['destfolderid'] = $folder;
                                        $filesarray['source'] = $sourcepath;
                                        $filesarray['destination'] = $destpath;
                                         $this->filemovepath($filesarray);
                                    }
                                    $sourcearray =  array_filter(explode('/',$sourcepath));
                                    array_pop($sourcearray);
                                    $sourcepath = implode('/',$sourcearray);

                                    $destarray =  array_filter(explode('/',$destpath));
                                    array_pop($destarray);
                                    $destpath = implode('/',$destarray);
                                }
                            }else{
                                $getfilesarray = AppFileManager::where('parent', $files->id)->where('type', 'file')->get();
                                if(count($getfilesarray)>0){
                                    foreach ($getfilesarray as $getfiles){
                                        $filesarray = array();
                                        $filesarray['file'] = $getfiles;
                                        $filesarray['destfolderid'] = $files->id;
                                        $filesarray['source'] = $sourcepath;
                                        $filesarray['destination'] = $destpath;
                                        $this->filemovepath($filesarray);
                                    }
                                }
                            }

                            $files->parent = $destfolderid;
                            $files->save();
                        }elseif($files->type=='file'){
                            if($destfolderid>0){
                                $filesarray = array();
                                $filesarray['file'] = $files;
                                $filesarray['destfolderid'] = $destfolderid;
                                $this->filecopy($filesarray);
                            }else{
                                $dstfolter = AppFileManager::where('pathtype',$pathtype)->where('type','folder')->where('parent','0')->where('project_id',$project_id)->first();
                                $filesarray = array();
                                $filesarray['file'] = $files;
                                $filesarray['destfolderid'] = !empty($dstfolter->id) ? $dstfolter->id : 0;
                                $this->filecopy($filesarray);
                            }
                        }
                    }
                }
                if($pathtype=='outline'){
                    $pathname = 'Drawings';
                }
                if($pathtype=='photos'){
                    $pathname = 'Photos';
                }
                if($pathtype=='documents'){
                    $pathname = 'Documents';
                }
                if(!empty($files)){
                    $response['status'] = 200;
                    $response['message'] = 'File Copied Successfully';
                    return $response;
                }else{
                    $response['status'] = 200;
                    $response['message'] = 'Folder Copied Successfully';
                    return $response;
                }

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'copy-file',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    protected  function filemove($filearray){

        $storage = storage();
        $url = uploads_url();
        $awsurl = awsurl();
        $files = $filearray['file'];
        $destfolderid = $filearray['destfolderid'];
        $hashname = $files->hashname;
        $goggleurl = $files->google_url;
        $dropbox_link = $files->dropbox_link;
        switch ($storage){
            case 'local':
                $folder = '';
                $profolder = $files->project_id;
                $x=1;
                $p = $files->parent;
                while($x==1){
                    $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                    if($fn !== null){
                        $folder = $fn->id.'/'.$folder;
                        $p = $fn->parent;
                    }else{
                        $x=0;
                    }
                }
                $sourcepath =  'uploads/project-files/'.$profolder.'/'.$folder.$hashname;
                $sourcethumbnaillink = 'uploads/project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                $folder = '';
                $destfolder = AppFileManager::where('id', $destfolderid)->where('type', 'folder')->first();
                $p = $destfolder->parent;
                $x=1;
                while($x==1){
                    $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                    if($fn !== null){
                        $folder = $fn->id.'/'.$folder;
                        $p = $fn->parent;
                    }else{
                        $x=0;
                    }
                }
                $desinationpath =  'uploads/project-files/'.$profolder.'/'.$folder.$destfolderid.'/';
                if(!file_exists($desinationpath)){
                    mkdir( $desinationpath, 0777, true);
                }
                $desinationpathlink =  $desinationpath.$hashname;
                $desinationthumbnail = 'uploads/project-files/'.$profolder.'/'.$folder.$destfolderid.'/'.'thumbnail/';
                if(!file_exists($desinationthumbnail)){
                    mkdir( $desinationthumbnail, 0777, true);
                }
                $desinationthumbnaillink = $desinationthumbnail.$hashname;
                if($files->type=='file'){
                    if(file_exists($sourcepath)){
                        if(rename($sourcepath,$desinationpathlink)){
                            $fx = explode('.', $hashname);
                            $ext = $fx[(count($fx)-1)];
                            if($ext=='jpeg'||$ext=='jpg'||$ext=='png'){
                                if(file_exists($sourcethumbnaillink)){
                                rename($sourcethumbnaillink,$desinationthumbnaillink);
                                }
                            }
                            $files->parent = $destfolderid;
                            $files->save();
                        }
                    }
                }else{
                    $files->parent = $destfolderid;
                    $files->save();
                }
                break;
        }
    }
    protected  function filecopy($filearray){

        $storage = storage();
        $url = uploads_url();
        $awsurl = awsurl();
        $files = $filearray['file'];
        $destfolderid = $filearray['destfolderid'];
        $newfiles = $files->replicate();
        $newfiles->save();
        $hashname = $newfiles->hashname;
        $goggleurl = $newfiles->google_url;
        $dropbox_link = $newfiles->dropbox_link;
        switch ($storage){
            case 'local':
                $folder = '';
                $profolder = $newfiles->project_id;
                $x=1;
                $p = $newfiles->parent;
                while($x==1){
                    $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                    if($fn !== null){
                        $folder = $fn->id.'/'.$folder;
                        $p = $fn->parent;
                    }else{
                        $x=0;
                    }
                }
                $sourcepath =  'uploads/project-files/'.$profolder.'/'.$folder.$hashname;
                $sourcethumbnaillink = 'uploads/project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                $folder = '';
                $destfolder = AppFileManager::where('id', $destfolderid)->where('type', 'folder')->first();
                $p = $destfolder->parent;
                $x=1;
                while($x==1){
                    $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                    if($fn !== null){
                        $folder = $fn->id.'/'.$folder;
                        $p = $fn->parent;
                    }else{
                        $x=0;
                    }
                }
                $desinationpath =  'uploads/project-files/'.$profolder.'/'.$folder.$destfolderid.'/';
                if(!file_exists($desinationpath)){
                    mkdir($desinationpath, 0777, true);
                }
                $desinationpathlink =  $desinationpath.$hashname;
                $desinationthumbnail = 'uploads/project-files/'.$profolder.'/'.$folder.$destfolderid.'/'.'thumbnail/';
                if(!file_exists($desinationthumbnail)){
                    mkdir( $desinationthumbnail, 0777, true);
                }
                $desinationthumbnaillink = $desinationthumbnail.$hashname;
                if($newfiles->type=='file'){
                    if(file_exists($sourcepath)){
                        if(copy($sourcepath,$desinationpathlink)){
                            $fx = explode('.', $hashname);
                            $ext = $fx[(count($fx)-1)];
                            if($ext=='jpeg'||$ext=='jpg'||$ext=='png'){
                                if(file_exists($sourcethumbnaillink)){
                                    copy($sourcethumbnaillink,$desinationthumbnaillink);
                                }
                            }
                            $newfiles->parent = $destfolderid;
                            $newfiles->save();
                        }
                    }
                }else{
                    $newfiles->parent = $destfolderid;
                    $newfiles->save();
                }
                break;
        }
    }
    protected  function filemovepath($filearray){

        $storage = storage();
        $url = uploads_url();
        $awsurl = awsurl();
        $files = $filearray['file'];
        $destfolderid = $filearray['destfolderid'];
        $source = $filearray['source'];
        $destination = $filearray['destination'];
        $hashname = $files->hashname;
        $goggleurl = $files->google_url;
        $dropbox_link = $files->dropbox_link;
        $profolder = $files->project_id;
        switch ($storage){
            case 'local':
                $sourcepath =  'uploads/project-files/'.$profolder.'/'.$source.'/'.$hashname;
                $sourcethumbnaillink = 'uploads/project-files/'.$profolder.'/'.$source.'/thumbnail/'.$hashname;

                $desinationpath =  'uploads/project-files/'.$profolder.'/'.$destination.'/';
                if(!file_exists($desinationpath)){
                    mkdir( $desinationpath, 0777, true);
                }
                $desinationpathlink =  $desinationpath.$hashname;
                $desinationthumbnail = 'uploads/project-files/'.$profolder.'/'.$destination.'/'.'thumbnail/';
                if(!file_exists($desinationthumbnail)){
                    mkdir( $desinationthumbnail, 0777, true);
                }
                $desinationthumbnaillink = $desinationthumbnail.$hashname;
                if($files->type=='file'){
                    if(file_exists($sourcepath)){
                        if(rename($sourcepath,$desinationpathlink)){
                            $fx = explode('.', $hashname);
                            $ext = $fx[(count($fx)-1)];
                            if($ext=='jpeg'||$ext=='jpg'||$ext=='png'){
                                if(file_exists($sourcethumbnaillink)){
                                    rename($sourcethumbnaillink,$desinationthumbnaillink);
                                }
                            }
                            $files->parent = $destfolderid;
                            $files->save();
                        }
                    }
                }else{
                    $files->parent = $destfolderid;
                    $files->save();
                }
                break;
        }
    }
    protected function fileinfo($filemanager,$user,$revisionid){
        $storage = storage();
        $projectdetails = Project::find($filemanager->project_id);
        $documents = array();
        $documents['id'] = $filemanager->id;
        $documents['company_id'] = $filemanager->company_id;
        $documents['project_id'] = $filemanager->project_id;
        $documents['project_admin'] = !empty($projectdetails) ? $projectdetails->added_by : '0';
        $documents['user_id'] = $filemanager->user_id;
        $documents['username'] = get_user_name($filemanager->user_id);
        $documents['userimage'] = get_users_image_link($filemanager->user_id);
        $documents['pathtype'] = $filemanager->pathtype;
        $documents['foldertype'] = $filemanager->foldertype;
        $documents['name'] = $filemanager->name;
        $documents['filename'] = $filemanager->filename;
        $documents['hashname'] = $filemanager->hashname;
        $link = $thumbnaillink = '';
        if($filemanager->type=='file'){
            if(!empty($filemanager->external_link)){
                $link = $filemanager->external_link;
                $documents['filename'] = $filemanager->external_link_name;
            }else{
                $url = uploads_url();
                $awsurl = awsurl();
                $hashname = $filemanager->hashname;
                $goggleurl = $filemanager->google_url;
                $dropbox_link = $filemanager->dropbox_link;
                if(empty($revisionid)&&$filemanager->revisionfileid==0){
                    $latestrevisionfile = AppFileManager::where('revisionfileid',$filemanager->id)->orderBy('id','desc')->first();
                    if(!empty($latestrevisionfile->id)){
                        $hashname = $latestrevisionfile->hashname;
                        $goggleurl = $latestrevisionfile->google_url;
                        $dropbox_link = $latestrevisionfile->dropbox_link;
                    }
                }
                switch ($storage){
                    case 'local':
                        $folder = '';
                        $profolder = $filemanager->project_id;
                        $x=1;
                        $p = $filemanager->parent;
                        while($x==1){
                            $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                            if($fn !== null){
                                $folder = $fn->id.'/'.$folder;
                                $p = $fn->parent;
                            }else{
                                $x=0;
                            }
                        }
                        $link = $url.'project-files/'.$profolder.'/'.$folder.$hashname;
                        $thumbnaillink = $url.'project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                        break;
                    case 's3':
                        $folder = '';
                        $profolder = $filemanager->project_id;
                        $x=1;
                        $p = $filemanager->parent;
                        while($x==1){
                            $fn = \App\AppFileManager::where('id', $p)->where('type', 'folder')->first();
                            if($fn !== null){
                                $folder = $fn->id.'/'.$folder;
                                $p = $fn->parent;
                            }else{
                                $x=0;
                            }
                        }
                        $link = $awsurl.'project-files/'.$profolder.'/'.$folder.$hashname;
                        $thumbnaillink = $awsurl.'project-files/'.$profolder.'/'.$folder.'thumbnail/'.$hashname;
                        break;
                    case 'google':
                        $link =  $goggleurl;
                        $thumbnaillink =  $goggleurl;
                        break;
                    case 'dropbox':
                        $link =  $dropbox_link;
                        $thumbnaillink =  $dropbox_link;
                        break;
                }
            }
        }
        $documents['link'] = $link;
        $documents['thumbnaillink'] = $thumbnaillink;
        $documents['size'] = $filemanager->size;
        $documents['type'] = $filemanager->type;
        $documents['revisionfileid'] = $filemanager->revisionfileid ?: 0;
        $documents['parent'] = $filemanager->parent;
        $documents['description'] = $filemanager->description;
        $documents['locked'] = $filemanager->locked;
        $documents['external_link_name'] = $filemanager->external_link_name;
        $documents['created_at'] = Carbon::parse($filemanager->created_at)->format('d M Y');
        $documents['createdby'] = '0';
        if($user->id==$filemanager->user_id){
            $documents['createdby'] = '1';
        }
        return $documents;
    }
}
