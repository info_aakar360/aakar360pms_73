<?php

namespace App\Http\Controllers\Api;

use App\AccountPayments;
use App\AccountPaymentsFiles;
use App\AppProject;
use App\Company;
use App\Employee;
use App\Http\Controllers\Controller;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\IncomeExpenseHeadFiles;
use App\IncomeExpenseType;
use App\ProjectMember;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;

class ApiAccountsController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }
    protected function validatetToken($apikey,$token)
    {
        $response = array();
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }
    public function incomeTypeList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $incometypelist = array();
                $dataarray = array('Asset'=>'dr','Liability'=>'cr','Income'=>'cr','Expense'=>'dr');
                foreach ($dataarray as $name => $type){
                    $incomearray = IncomeExpenseType::where('name',$name)->where('company_id',$companyid)->first();
                    if(empty($incomearray)){
                        $incomearray = new IncomeExpenseType();
                        $incomearray->company_id = $companyid;
                        $incomearray->name = $name;
                        $incomearray->type = $type;
                        $incomearray->default = '1';
                        $incomearray->save();
                    }
                }
                $incometypelist =  IncomeExpenseType::where('company_id',$companyid)->get();
                $response['status'] = 200;
                $response['message'] = 'Income types List Fetched';
                $response['response'] = $incometypelist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-type-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeGroupList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $namenotin = array('Current Asset','Current Liability');
                $incometypelist = IncomeExpenseGroup::where('company_id',$companyid)->whereNotIn('name',$namenotin)->get();
                $response['status'] = 200;
                $response['message'] = 'Income types List Fetched';
                $response['response'] = $incometypelist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-type-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeGroupItem(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $groupid = $request->groupid;
                $incometypelist = IncomeExpenseGroup::where('company_id',$companyid)->where('id',$groupid)->first();
                $response['status'] = 200;
                $response['message'] = 'Income types List Fetched';
                $response['response'] = $incometypelist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-type-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeGroupAdd(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $incomegroup = IncomeExpenseGroup::where('name',$request->name)->where('company_id',$companyid)->first();
                if(!empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Group already exists';
                }
                $items = new IncomeExpenseGroup();
                $items->company_id = $companyid;
                $items->name = $request->name;
                $items->income_type = $request->type;
                $items->parent = $request->parent ?: 0;
                $items->created_by = $user->id;
                $items->save();
                $response['status'] = 200;
                $response['message'] = 'Group Created Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-group-add',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeGroupUpdate(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $groupid = $request->groupid;
                $incomegroup = IncomeExpenseGroup::where('name',$request->name)->where('id','<>',$groupid)->where('company_id',$companyid)->first();
                if(!empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Group already exists';
                }
                $items = IncomeExpenseGroup::find($groupid);
                $items->company_id = $companyid;
                $items->name = $request->name;
                $items->income_type = $request->type;
                $items->parent = $request->parent ?: 0;
                $items->created_by = $user->id;
                $items->save();
                $response['status'] = 200;
                $response['message'] = 'Group Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-group-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeGroupDelete(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $groupid = $request->groupid;
                $incomegroup = IncomeExpenseGroup::where('id',$groupid)->where('company_id',$companyid)->first();
                if(empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Group not exists';
                }
                $items = IncomeExpenseGroup::find($groupid);
                $items->delete();
                $response['status'] = 200;
                $response['message'] = 'Group Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-group-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeNameList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
               /* $companiesallemployes = Employee::get();
                if(count($companiesallemployes)){
                    foreach ($companiesallemployes as $companiesall){
                        if($companiesall->user_type!=='admin'){
                            $usercompany = $companiesall->company_id;
                            $usertype = ucwords($companiesall->user_type);
                            $incometype = IncomeExpenseGroup::where('name',$usertype)->where('company_id',$usercompany)->first();
                            if(!empty($incometype)){
                                $incomearray = IncomeExpenseHead::where('company_id',$usercompany)->where('income_expense_group_id',$incometype->id)->where('emp_id',$companiesall->id)->first();
                                if(empty($incomearray)){
                                   $incomearray = new IncomeExpenseHead();
                                    $incomearray->company_id = $usercompany;
                                    $incomearray->income_expense_group_id = $incometype->id;
                                    $incomearray->income_expense_type_id = $incometype->income_type;
                                    $incomearray->created_by = $companiesall->user_id;
                                    $incomearray->emp_id = $companiesall->id;
                                    $incomearray->save();
                                }
                                $projectmember = ProjectMember::where('company_id',$usercompany)->where('employee_id',$companiesall->id)->first();

                                $incomearray->name = $companiesall->name;
                                $incomearray->project_id = !empty($projectmember->id) ? $projectmember->project_id : 0;
                                $incomearray->save();
                            }
                        }
                    }
                }*/
                $default = $request->default ?: 0;
                $allincometypelist = array();
                if(!empty($default)&&$default=='all'){
                    $allincometypelist = IncomeExpenseHead::where('company_id',$companyid)->where('project_id','0')->pluck('id')->toArray();
                 }
                $projectincometypelist = IncomeExpenseHead::where('company_id',$companyid)->where('project_id',$projectdetails->id)->pluck('id')->toArray();
                $incomearray = array_merge($allincometypelist,$projectincometypelist);
                $incomearray = array_unique(array_filter($incomearray));
                $incometypelist = IncomeExpenseHead::where('company_id',$companyid)->whereIn('id',$incomearray)->orderBy('name','asc')->get();
                $response['status'] = 200;
                $response['message'] = 'Income name List Fetched';
                $response['response'] = $incometypelist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-name-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeNameItem(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $nameid = $request->nameid;
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $incometypelist = IncomeExpenseHead::where('company_id',$companyid)->where('id',$nameid)->first();
                if(!empty($incometypelist)){
                    $response['status'] = 200;
                    $response['message'] = 'Income name List Fetched';
                    $response['response'] = $incometypelist;
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Income name Not Found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-name-item',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeNameAdd(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $incomegroup = IncomeExpenseHead::where('name',$request->name)->where('company_id',$companyid)->first();
                if(!empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Name already exists';
                }
                $incomegroup = IncomeExpenseGroup::where('id',$request->groupid)->first();
                $items = new IncomeExpenseHead();
                $items->company_id = $companyid;
                $items->project_id = !empty($projectdetails->id) ? $projectdetails->id : 0;
                $items->name = $request->name;
                $items->income_expense_type_id = $incomegroup->income_type;
                $items->income_expense_group_id = $incomegroup->id;
                $items->created_by = $user->id;
                $items->amount = !empty($request->amount) ? $request->amount : 0;
                $items->voucherdate = !empty($request->voucher_date) ? date('Y-m-d',strtotime($request->voucher_date)) : '';
                $items->particulars = $request->particulars;
                $items->emp_id =  0;
                $items->save();
                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  IncomeExpenseHeadFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->ledger_id =  $items->id;
                            $productimage->save();
                            $sourcepath = 'uploads/ledger-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/ledger-files/'.$items->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/ledger-files/'.$items->id)) {
                                    mkdir('uploads/ledger-files/'.$items->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Ledger Name Created Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-name-add',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeNameUpdate(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $nameid = $request->nameid;
                $groupid = $request->groupid;
                $incomegroup = IncomeExpenseHead::where('name',$request->name)->where('id','<>',$nameid)->where('company_id',$companyid)->first();
                if(!empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Name already exists';
                }
                $incomegroup = IncomeExpenseGroup::where('id',$groupid)->first();
                $items = IncomeExpenseHead::find($nameid);
                $items->company_id = $companyid;
                $items->project_id = !empty($projectdetails->id) ? $projectdetails->id : 0;
                $items->name = $request->name;
                $items->income_expense_type_id = $incomegroup->income_type;
                $items->income_expense_group_id = $incomegroup->id;
                $items->created_by = $user->id;
                $items->amount = !empty($request->amount) ? $request->amount : 0;
                $items->voucherdate = !empty($request->voucher_date) ? date('Y-m-d',strtotime($request->voucher_date)) : '';
                $items->particulars = $request->particulars;
                $items->emp_id = !empty($request->emp_id) ? 1 : 0;
                $items->save();

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  IncomeExpenseHeadFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->ledger_id =  $items->id;
                            $productimage->save();
                            $sourcepath = 'uploads/ledger-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/ledger-files/'.$items->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/ledger-files/'.$items->id)) {
                                    mkdir('uploads/ledger-files/'.$items->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Ledger Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-name-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function incomeNameDelete(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $nameid = $request->nameid;
                $incomegroup = IncomeExpenseHead::where('id',$nameid)->where('company_id',$companyid)->first();
                if(empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Name not exists';
                }
                $items = IncomeExpenseHead::find($nameid);
                $items->delete();
                $response['status'] = 200;
                $response['message'] = 'Name Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-name-delete',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $punchitemid = $request->nameid;
                $punchitem = IncomeExpenseHead::find($punchitemid);
                $replyid = $request->replyid;
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                    $storage = storage();
                    $file = new IncomeExpenseHeadFiles();
                    $file->company_id = $punchitem->company_id;
                    $file->added_by = $user->id;
                    $file->ledger_id = $punchitem->id ?: 0;
                    switch($storage) {
                        case 'local':

                            $destinationPath = 'uploads/ledger-files/'.$punchitemid;
                            if (!file_exists(''.$destinationPath)) {
                                mkdir(''.$destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            $filename = $fileData->hashName();
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/ledger-files/'.$punchitemid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'ledger-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('ledger-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $punchitemid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$punchitemid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $punchitemid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('ledger-files/'.$punchitemid.'/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/ledger-files/'.$punchitemid.'/'.$fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Ledger name Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'income-head-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function paymentVoucherList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $partyto = $request->party_to;
                $partyto = array_filter(array_unique(explode(',',$partyto)));
                $ledgerfromarray = IncomeExpenseHead::where('company_id',$companyid)->where('default','1')->pluck('id')->toArray();
                $startdate = !empty($request->start_date) ? date('Y-m-d',strtotime($request->start_date)) : '';
                $enddate = !empty($request->end_date) ? date('Y-m-d',strtotime($request->end_date)) : '';
                $response = array();
                $page = $request->page;
                $incometypelist = AccountPayments::where('company_id',$companyid)->where('project_id',$projectdetails->id)->whereIn('ledger_from',$ledgerfromarray);
                if(!empty($startdate)){
                    $incometypelist = $incometypelist->where('voucher_date','>=',$startdate);
                }
                if(!empty($enddate)){
                    $incometypelist = $incometypelist->where('voucher_date','<=',$enddate);
                }
                if(!empty($partyto)){
                  $incometypelist = $incometypelist->whereIn('ledger_to',$partyto);
                }
                if(empty($page)||$page=='all'){
                    $incometypelist = $incometypelist->orderBy('voucher_date','asc')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $incometypelist = $incometypelist->offset($skip)->take($count)->orderBy('voucher_date','asc')->get();
                }
                $totatdebit = $totalcredit = $totalbalance = 0;
                $totatdebit = IncomeExpenseHead::whereIn('id',$ledgerfromarray)->sum('amount');
                $accountspaymentlist = array();
                if(count($incometypelist)>0){
                    foreach ($incometypelist as $incometype){
                        if($incometype->type=='dr'){
                            $totatdebit += $incometype->amount;
                        }else if($incometype->type=='cr'){
                            $totalcredit += $incometype->amount;
                        }
                        $paymentarray = array();
                        $paymentarray['id'] = $incometype->id;
                        $paymentarray['company_id'] = $incometype->company_id;
                        $paymentarray['project_id'] = $incometype->project_id;
                        $paymentarray['project_name'] = get_project_name($incometype->project_id);
                        $paymentarray['ledger_from'] =  $incometype->ledger_from ?: '' ;
                        $paymentarray['ledgerfromname'] = !empty($incometype->ledgerfrom->name) ? $incometype->ledgerfrom->name : '' ;
                        $paymentarray['ledger_to'] =  $incometype->ledger_to ?: '' ;
                        $paymentarray['ledgertoname'] = !empty($incometype->ledgerto->name) ? $incometype->ledgerto->name : '' ;
                        $paymentarray['voucher_date'] = !empty($incometype->voucher_date) ? date('d M Y',strtotime($incometype->voucher_date)) : '';
                        $paymentarray['type'] = $incometype->type;
                        $paymentarray['particulars'] = $incometype->particulars;
                        $paymentarray['prevbalance'] = $incometype->prevbalance;
                        $paymentarray['amount'] = $incometype->amount;
                        $paymentarray['balance'] = $incometype->balance;
                        $paymentarray['images'] = !empty($incometype->images) ? $incometype->images : array();
                        $accountspaymentlist[] = $paymentarray;
                    }
                }
                $totalbalance = $totatdebit-$totalcredit;
                $response['status'] = 200;
                $response['totatdebit'] = account_numberformat($totatdebit);
                $response['totalcredit'] = account_numberformat($totalcredit);
                $response['totalbalance'] = account_numberformat($totalbalance);
                $response['message'] = 'Payment List Fetched';
                $response['response'] = $accountspaymentlist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'payment-voucher-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function paymentVoucherAdd(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $date = new \DateTime($request->voucher_date);
                $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
                $response = array();

               $accountspayments = new AccountPayments();
                $accountspayments->company_id = $companyid;
                $accountspayments->project_id = !empty($projectdetails->id) ? $projectdetails->id : 0;
                $accountspayments->created_by = $user->id;
                if(!empty($request->ledger_from)){
                    $accountspayments->ledger_from = $request->ledger_from;
                }else{
                    if(!empty($request->ledger_from_name)){
                        $ledgerfromname = $request->ledger_from_name;
                        $ledgerfrom = IncomeExpenseHead::where('company_id',$companyid)->where('name',$ledgerfromname)->first();
                        $accountspayments->ledger_from = $ledgerfrom->id;
                    }
                }
                $accountspayments->ledger_to = $request->ledger_to;
                $accountspayments->amount = account_numberformat($request->amount);
                $accountspayments->type = $request->type;
                $accountspayments->voucher_date = $voucher_date;
                $accountspayments->particulars = $request->particulars;
                $accountspayments->save();

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  AccountPaymentsFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->payment_id =  $accountspayments->id;
                            $productimage->save();
                            $sourcepath = 'uploads/payment-voucher-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/payment-voucher-files/'.$accountspayments->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/payment-voucher-files/'.$accountspayments->id)) {
                                    mkdir('uploads/payment-voucher-files/'.$accountspayments->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }

                $response['status'] = 200;
                $response['paymentid'] = $accountspayments->id;
                $response['message'] = 'Payment Created';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'payment-voucher-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function paymentVoucherUpdate(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }
                $date = new \DateTime($request->voucher_date);
                $voucher_date = $date->format('Y-m-d'); // 31-07-2012 '2008-11-11'
                $id = $request->paymentid;
                $accountspayments = AccountPayments::find($id);
                $prevamount = $accountspayments->amount;

                $accountspayments->project_id = !empty($projectdetails->id) ? $projectdetails->id : 0;
                $accountspayments->company_id = $companyid;
                if(!empty($request->ledger_from)){
                    $accountspayments->ledger_from = $request->ledger_from;
                }else{
                    if(!empty($request->ledger_from_name)){
                        $ledgerfromname = $request->ledger_from_name;
                        $ledgerfrom = IncomeExpenseHead::where('company_id',$companyid)->where('name',$ledgerfromname)->first();
                        $accountspayments->ledger_from = $ledgerfrom->id;
                    }
                }
                $accountspayments->ledger_to = $request->ledger_to;
                $accountspayments->amount = account_numberformat($request->amount);
                $accountspayments->type = $request->type;
                $accountspayments->voucher_date = $voucher_date;
                $accountspayments->particulars = $request->particulars;
                $accountspayments->save();

                if(!empty($request->images)){
                    $productimagsarry = array_filter(array_unique(explode(',',$request->images)));
                    foreach($productimagsarry as $productimags){
                        $productimage =  AccountPaymentsFiles::find($productimags);
                        if(!empty($productimage)){
                            $productimage->payment_id =  $accountspayments->id;
                            $productimage->save();
                            $sourcepath = 'uploads/payment-voucher-files/0/'.$productimage->hashname;
                            $destinationPath = 'uploads/payment-voucher-files/'.$accountspayments->id.'/'.$productimage->hashname;
                            if(file_exists($sourcepath)){
                                if (!file_exists('uploads/payment-voucher-files/'.$accountspayments->id)) {
                                    mkdir('uploads/payment-voucher-files/'.$accountspayments->id, 0777, true);
                                }
                                rename($sourcepath,$destinationPath);
                            }
                        }
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Payment Voucher Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'payment-voucher-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function paymentVoucherDelete(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }

                $groupid = $request->paymentid;
                $incomegroup = AccountPayments::where('id',$groupid)->where('project_id',$projectdetails->id)->first();
                if(empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Payment not Found';
                }
                $items = AccountPayments::find($groupid);
                 AccountPaymentsFiles::where('payment_id',$items->id)->delete();
                $items->delete();
                $response['status'] = 200;
                $response['message'] = 'Payment Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'payment-voucher-delete',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function paymentVoucherItem(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $projectdetails = AppProject::find($projectid);
                if(empty($projectdetails)){
                    $response['message'] = "Project Not found";
                    $response['status'] = '301';
                    return $response;
                }
                $companyid = $user->company_id;
                if(!empty($projectdetails->company_id)){
                    $companyid = $projectdetails->company_id;
                }

                $groupid = $request->paymentid;
                $incomegroup = AccountPayments::where('id',$groupid)->where('project_id',$projectdetails->id)->first();
                if(empty($incomegroup)){
                    $response['status'] = 200;
                    $response['message'] = 'Payment not Found';
                    return $response;
                }
                $response['status'] = 200;
                $response['message'] = 'Payment Info';
                $response['response'] = $incomegroup;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'payment-voucher-item',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storePaymentImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $punchitemid = $request->paymentid;
                $punchitem = AccountPayments::find($punchitemid);
                $replyid = $request->replyid;
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                    $storage = storage();
                    $file = new AccountPaymentsFiles();
                    $file->company_id = $punchitem->company_id;
                    $file->added_by = $user->id;
                    $file->payment_id = $punchitem->id ?: 0;
                    switch($storage) {
                        case 'local':

                            $destinationPath = 'uploads/payment-voucher-files/'.$punchitemid;
                            if (!file_exists(''.$destinationPath)) {
                                mkdir(''.$destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            $filename = $fileData->hashName();
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/payment-voucher-files/'.$punchitemid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'payment-voucher-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('payment-voucher-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $punchitemid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$punchitemid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $punchitemid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('payment-voucher-files/'.$punchitemid.'/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/payment-voucher-files/'.$punchitemid.'/'.$fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Ledger name Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-payment-voucher-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deletePaymentImage(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{

                $response = array();
                $unit = AccountPaymentsFiles::where('id',$request->fileid)->delete();
                $response['status'] = 200;
                $response['message'] = 'File Removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}