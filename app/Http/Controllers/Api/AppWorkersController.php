<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\Company;
use App\Employee;
use App\Http\Controllers\Controller;
use App\ManpowerAttendance;
use App\ManpowerLog;
use App\Traits\EmployeeRegister;
use App\Workers;
use App\User;
use Google\Service\Tasks\Task;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use DB;

class AppWorkersController extends Controller
{
    use EmployeeRegister;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function appWorkers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $empid = $request->emp_id;
                $contractorid = $request->contractor_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                   $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $channels = Workers::where('company_id',$companyid);
                if(!empty($projectid)){
                    $channels = $channels->where('project_id',$projectid);
                }
                if(!empty($empid)){
                    $channels = $channels->where('emp_id',$empid);
                }
                $page = $request->page;
                if($page=='all'){
                    $workersdata = $channels->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $workersdata = $channels->offset($skip)->take($count)->get();
                }
                $workersarray = array();
                if(count($workersdata)>0){
                    foreach ($workersdata as $item) {
                        $labattendance = array();
                        $workersarray[] = array(
                            'id' => $item->id,
                            'company_id' => $item->company_id,
                            'project_id' => $item->project_id,
                            'project_name' => $item->project_name,
                            'category_id' => $item->category,
                            'category_name' => $item->category_name,
                            'added_by' => $item->added_by,
                            'added_name' => get_user_name($item->added_by),
                            'emp_id' => $item->emp_id,
                            'contractor' => $item->contractor,
                            'contractor_name' => get_employee_name($item->emp_id),
                            'labourtype' =>  $item->labourtype,
                            'name' => !empty($item->name) ? ucwords($item->name) : '',
                            'type' => $item->type,
                            'image' => $item->image,
                            'image_url' => !empty($item->image) ? uploads_url().'workers/'.$item->image : '',
                            'salary' => $item->salary,
                            'salarytype' => $item->salarytype,
                            'workinghours' => $item->workinghours,
                            'datetime' => date('d M Y',strtotime($item->created_at)),
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Worker List Fetched';
                $response['response'] = $workersarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function workerDetails(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                   $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $workerid = $request->worker_id;
                $item = Workers::where('id',$workerid)->first();
                $workersarray = array();
                if(!empty($item)){
                        $workersarray = array(
                            'id' => $item->id,
                            'company_id' => $item->company_id,
                            'project_id' => $item->project_id,
                            'project_name' => $item->project_name,
                            'category_id' => $item->category,
                            'category_name' => $item->category_name,
                            'added_by' => $item->added_by,
                            'added_name' => get_user_name($item->added_by),
                            'emp_id' => $item->emp_id,
                            'contractor' => $item->contractor,
                            'contractor_name' => get_employee_name($item->emp_id),
                            'name' => ucwords($item->name),
                            'workertype' =>  $item->type,
                            'labourtype' =>  $item->labourtype,
                            'image' => $item->image,
                            'image_url' => !empty($item->image) ? uploads_url().'workers/'.$item->image : '',
                            'salary' => $item->salary,
                            'salarytype' => $item->salarytype,
                            'workinghours' => $item->workinghours,
                            'datetime' => date('d M Y',strtotime($item->created_at)),
                        );
                }
                $response['status'] = 200;
                $response['message'] = 'Worker List Fetched';
                $response['response'] = $workersarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-details',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createWorkers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $labourtype = $request->labourtype;
                $empid = $request->emp_id ?: 0;
                $contractorid = $request->contractor_id ?: 0;
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $name = $request->name;
                if(empty($empid)&&empty($contractorid)){
                    $empoyeename = '';
                    $usertype = 'employee';
                    if($labourtype=='contractor') {
                        $usertype = 'contractor';
                        $empoyeename = $request->contractor_name;
                    }
                    if($labourtype=='company') {
                        $usertype = 'employee';
                        $empoyeename = $name;
                    }

                    $dataarray = array();
                    $dataarray['user'] = $user;
                    $dataarray['company_id'] = $companyid;
                    $dataarray['name'] = $empoyeename;
                    $dataarray['email'] =  '';
                    $dataarray['mobile'] =  '';
                    $dataarray['user_type'] = $usertype;
                    $dataarray['password'] = $request->password ? : '';
                    $dataarray['gender'] = $request->gender ? : '';
                    $dataarray['gender'] = $request->gender ? : '';
                    $dataarray['address'] = $request->address ? : '';
                    $dataarray['fcm'] = $request->fcm ? : '';
                    $dataarray['projectid'] = $projectid;
                    $dataarray['sharetoproject'] = 0;
                    $payload =  $this->createemployee($dataarray);
                    $employee = $payload['response'];
                    $empid = $employee['id'] ?: 0;
                    $contractorid = $employee['user_id'] ?: 0;
                }

                $channel = new Workers();
                $channel->company_id = $companyid;
                $channel->added_by = $user->id;
                $channel->project_id = $projectid;
                if($labourtype=='contractor'){
                    if(!empty($name)){
                        $channel->name = $name;
                        $channel->type = 'worker';
                    }else{
                        $channel->type = 'category';
                    }
                }
                if($labourtype=='company'){
                    $channel->name = $name;
                    $channel->type = 'worker';
                }
                $channel->emp_id = $empid;
                $channel->contractor = $contractorid;
                $channel->labourtype = $labourtype;
                $channel->category = $request->category ?: 0;
                $channel->salary = $request->salary ?: 0;
                $channel->salarytype = !empty($request->salarytype) ? strtolower($request->salarytype) : '';
                $channel->workinghours = $request->workinghours ?: 0;
                $channel->save();
                 if ($request->hasFile('image')) {
                    $storage = storage();
                    $url = url('/');
                    $awsurl = awsurl();
                     $url = uploads_url();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/workers/' . $channel->id;
                            if (!file_exists('workers/'.$destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('workers/'.$channel->id, $request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('workers/'.'/', $request->image, $request->image->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                     $channel->image= $request->image->hashName();
                }
                $channel->save();

                $response['status'] = 200;
                $response['message'] = 'Worker Created Successfully';
                $response['workerid'] = $channel->id;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-workers',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function updateWorkers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $empid = $request->emp_id;
                $contractorid = $request->contractor_id;
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                $labourtype = $request->labourtype ?: 'company';
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $name = $request->name;
                $workerid = $request->worker_id;
                $channel = Workers::find($workerid);
                if(empty($channel)){
                    $response['status'] = 301;
                    $response['message'] = 'Worker not found';
                    return $response;
                }
                $channel->company_id = $companyid;
                $channel->added_by = $user->id;
                $channel->project_id = $projectid;
                $channel->emp_id = $empid;
                $channel->labourtype = $labourtype;
                $channel->contractor = $contractorid;
                if(!empty($name)){
                    $channel->name = $name;
                    $channel->type = 'worker';
                }else{
                    $channel->type = 'category';
                }
                $channel->category = $request->category;
                $channel->salary = $request->salary;
                $channel->salarytype = $request->salarytype;
                $channel->workinghours = $request->workinghours ?: 0;
                $channel->save();
                if ($request->hasFile('image')) {
                    $storage = storage();
                    $url = url('/');
                    $awsurl = awsurl();
                    $url = uploads_url();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/workers/' . $channel->id;
                            if (!file_exists('workers/'.$destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('workers/'.$channel->id, $request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('workers/'.'/', $request->image, $request->image->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $channel->image= $request->image->hashName();
                }
                $channel->save();
                $manpowerlogsarray = ManpowerLog::where('worker_id',$channel->id)->get();
                if(count($manpowerlogsarray)>0){
                    foreach ($manpowerlogsarray as $manpowerlogs){
                        $manpowerlogs->hourspershift =  $channel->workinghours ?: 0;
                        $manpowerlogs->salaryperday =  $channel->salary ?: 0;
                        $manpowerlogs->salarytype =   strtolower($channel->salarytype) ?: '';
                        $totalprice = labour_totalprice($manpowerlogs);
                        $manpowerlogs->totalprice = $totalprice;
                        $manpowerlogs->save();
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Worker Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-workers',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteWorkers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->worker_id;
                $channel =  Workers::where('id',$id)->first();
                if(!empty($channel->id)){
                    if($channel->added_by==$user->id){
                        ManpowerLog::where('worker_id',$channel->id)->delete();
                        $channel->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Worker deleted Successfully';
                        return $response;
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'You do not permission to delete worker';
                        return $response;
                    }
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Worker Not found';
                    return $response;
                }

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-workers',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function workersAttendanceList(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $date = !empty($request->date) ? date('Y-m-d',strtotime($request->date)) : date('Y-m-d');
                $empid = $request->emp_id;
                $contractor = $request->contractor_id;
                $taskid = $request->task_id ?: "";
                $activityid = $request->activity_id ?: "";
                    $contractorarray = Employee::join('project_members','project_members.employee_id','=','employee.id')
                                       ->select('employee.*')->where('project_members.project_id',$projectdetails->id)->where('employee.company_id',$companyid)->where('employee.user_type','contractor');
                    if(!empty($empid)){
                        $contractorarray = $contractorarray->where('employee.id',$empid);
                    }
                    $contractorarray = $contractorarray->get();
                $workerslistarray = array();
                if(count($contractorarray)>0){
                    foreach ($contractorarray as $contractors){
                        $contractorprice = '0';
                        $contrinfo =  $workersarray = array();
                        $workerslist = \App\Workers::where('project_id',$projectdetails->id)->where('emp_id',$contractors->id)->where('labourtype','contractor')->get();
                        if(count($workerslist)>0){
                        $contrinfo['id'] = $contractors->id;
                        $contrinfo['user_id'] = $contractors->user_id;
                        $contrinfo['name'] = $contractors->name;
                        $contrinfo['image'] = $contractors->image;
                        $contrinfo['image_url'] = get_employee_image_link($contractors->id);
                        foreach ($workerslist as $workers){
                                $work =$manpowerattendance =  array();
                                $manpowerattendance  = \App\ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('contractor',$contractors->user_id)->where('work_date',$date)->where('worker_id',$workers->id)->first();
                                $work['id'] = $workers->id;
                                 $work['manpowerid'] = !empty($manpowerattendance->id) ? $manpowerattendance->id : 0;
                                $work['workertype'] = $workers->type;
                                $work['labourtype'] = $workers->labourtype;
                                $work['name'] = $workers->name ?: '';
                                $manpowercategory = $workers->category ?: '0';
                                 $salary =  $workers->salary ?: '0';
                                $salarytype =  $workers->salarytype ?: '';
                                $hourspershift =  $workers->workinghours ?: '0';
                                $workinghours =  $workers->workinghours ?: '0';
                                $shift =  '';
                                $taskid =  '';
                                $description =  '';
                                $activityid =  '';
                                $workingminutes =  '0';
                                $noofworkers =  '0';
                                $totalprice =  '0';
                                if(!empty($manpowerattendance->id)){
                                    $manpowercategory =  $manpowerattendance->manpower_category ?: '0';
                                    $taskid =  $manpowerattendance->task_id ?: '';
                                    $activityid =  $manpowerattendance->activity_id ?: '';
                                    $salary =  $manpowerattendance->salaryperday ?: '0';
                                    $salarytype =  $manpowerattendance->salarytype ?: '';
                                    $hourspershift =  $manpowerattendance->hourspershift ?: '0';
                                    $workinghours =  $manpowerattendance->workinghours ?: '0';
                                    $workingminutes =  $manpowerattendance->workingminutes ?: '0';
                                    $shift =  $manpowerattendance->shift ?: '';
                                    $noofworkers =  $manpowerattendance->manpower ?: 0;
                                    $totalprice =  $manpowerattendance->totalprice ?: 0;
                                    $description =  $manpowerattendance->description ?: '';
                                }
                                $work['category'] = $manpowercategory;
                                $work['category_name'] = get_manpower_category($manpowercategory);
                                $work['task_id'] =  $taskid ?: '';
                                $work['activity_id'] =  $activityid ?: '';
                                $work['salary'] =  $salary ?: 0;
                                $work['salarytype'] =  $salarytype ?: '';
                                $work['hourspershift'] =  $hourspershift ?: 0;
                                $work['workinghours'] =  $workinghours ?: 0;
                                $work['workingminutes'] =  $workingminutes ?: 0;
                                $work['shift'] =  $shift;
                                $work['manpower'] =  $noofworkers;
                                $work['overtimelatefine'] =  '0';
                                $work['allowancesdeductions'] =  '0';
                                 if((!empty($manpowerattendance->overtimehours)&&!empty($manpowerattendance->overtimeamount)&&!empty($manpowerattendance->overtimetotal))||(!empty($manpowerattendance->latefinehours)&&!empty($manpowerattendance->latefineamount)&&!empty($manpowerattendance->latefinetotal))){
                                 $work['overtimelatefine'] =  '1';
                                }
                                if(!empty($manpowerattendance->allowances)||!empty($manpowerattendance->deductions)){
                                    $work['allowancesdeductions'] =  '1';
                                }
                                $work['description'] =  $description ?: '';
                                 $totalprice =  account_numberformat($totalprice);
                                $work['totalprice'] =  $totalprice;
                                $workersarray[] = $work;
                                $contractorprice += $totalprice;
                            }
                        }
                        $labourslist =  \App\ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('contractor',$contractors->user_id)->where('work_date',$date)->where('worker_id','0')->get();
                        if(count($labourslist)>0){
                                foreach ($labourslist as $manpowerattendance){
                                    $empname = Workers::where('id',$manpowerattendance->worker_id)->first();
                                    $work = array();
                                    $work['id'] = 0;
                                    $work['manpowerid'] = !empty($manpowerattendance->id) ? $manpowerattendance->id : 0;
                                    $work['workertype'] = 'worker';
                                    $work['labourtype'] = !empty($empname) ? $empname->labourtype : 'company';
                                    $work['name'] = !empty($empname) ? $empname->name : '';
                                    $work['category'] = $manpowerattendance->manpower_category ?: 0;
                                    $work['category_name'] = get_manpower_category($manpowerattendance->manpower_category);
                                    $work['salary'] =  $manpowerattendance->salaryperday ?: '0';
                                    $work['salarytype'] =  $manpowerattendance->salarytype ?: '';
                                    $work['hourspershift'] =  $manpowerattendance->hourspershift ?: '0';
                                    $work['workinghours'] =  $manpowerattendance->workinghours ?: '0';
                                    $work['workingminutes'] =  $manpowerattendance->workingminutes ?: '0';
                                    $work['shift'] =  $manpowerattendance->shift ?: '';
                                    $work['manpower'] =  $manpowerattendance->manpower ?: '';
                                    $work['task_id'] =  $manpowerattendance->task_id ?: '';
                                    $work['activity_id'] =  $manpowerattendance->activity_id ?: '';
                                    $work['overtimelatefine'] =  '0';
                                    $work['allowancesdeductions'] =  '0';
                                    if((!empty($manpowerattendance->overtimehours)&&!empty($manpowerattendance->overtimeamount)&&!empty($manpowerattendance->overtimetotal))||(!empty($manpowerattendance->latefinehours)&&!empty($manpowerattendance->latefineamount)&&!empty($manpowerattendance->latefinetotal))){
                                        $work['overtimelatefine'] =  '1';
                                    }
                                    if(!empty($manpowerattendance->allowances)||!empty($manpowerattendance->deductions)){
                                        $work['allowancesdeductions'] =  '1';
                                    }
                                    $work['description'] =  $manpowerattendance->description ?: '';
                                    $totalprice  = $manpowerattendance->totalprice ? account_numberformat($manpowerattendance->totalprice) : '';
                                    $work['totalprice'] =  $totalprice;
                                    $workersarray[] = $work;
                                    $contractorprice += $totalprice;
                                }
                            }
                            if(count($workerslist)>0||count($labourslist)>0){
                                $contrinfo['workersarray'] = $workersarray;
                                $contrinfo['totalprice'] = account_numberformat($contractorprice);
                                $workerslistarray[] = $contrinfo;
                            }
                        }
                }
                $workersarray = array();
                $contractorprice = 0;
                $workerslist = \App\Workers::where('company_id',$projectdetails->company_id)->where('project_id',$projectdetails->id)->where('contractor','0')->where('labourtype','company')->get();
                if(count($workerslist)>0){
                $contrinfo = array();
                $contrinfo['id'] = 0;
                $contrinfo['user_id'] = 0;
                $contrinfo['name'] = 'Company';
                $contrinfo['image'] = !empty($projectdetails->image) ? $projectdetails->image : '';
                $contrinfo['image_url'] = get_project_image_link($projectdetails->id);
                    foreach ($workerslist as $workers){
                        $work = array();
                        $manpowerattendance = \App\ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('contractor',0)->where('work_date',$date)->where('worker_id',$workers->id)->first();
                        $work['id'] = $workers->id;
                        $work['manpowerid'] = !empty($manpowerattendance->id) ? $manpowerattendance->id : 0;
                         $work['workertype'] = $workers->type;
                         $work['labourtype'] = 'company';
                        $work['name'] = $workers->name;
                        $manpowercategory = $workers->category;
                        $salary =  $workers->salary ?: '0';
                        $salarytype =  $workers->salarytype ?: '';
                        $hourspershift =  $workers->workinghours ?: '0';
                        $workinghours =  $workers->workinghours;
                        $workingminutes =   $manpower =   $totalprice =  '0';
                        $task_id =   $activity_id =    $description =    $shift =  '';
                        if(!empty($manpowerattendance->id)){
                            $manpowercategory =  $manpowerattendance->manpower_category;
                            $salary =  $manpowerattendance->salaryperday ?: '0';
                            $salarytype =  $manpowerattendance->salarytype ?: '';
                            $hourspershift =  $manpowerattendance->hourspershift ?: '0';
                            $workinghours =  $manpowerattendance->workinghours ?: '0';
                            $workingminutes =  $manpowerattendance->workingminutes ?: '0';
                            $shift =  $manpowerattendance->shift ?: '';
                            $manpower =  $manpowerattendance->manpower ?: '0';
                            $totalprice =  $manpowerattendance->totalprice ?: '0';
                            $task_id =  $manpowerattendance->task_id ?: '';
                            $activity_id =  $manpowerattendance->activity_id ?: '';
                            $description =  $manpowerattendance->description ?: '';
                        }
                        $work['category'] = $manpowercategory;
                        $work['category_name'] = get_manpower_category($manpowercategory);
                        $work['salary'] =  $salary;
                        $work['salarytype'] =  $salarytype;
                        $work['hourspershift'] =  $hourspershift;
                        $work['workinghours'] =  $workinghours;
                        $work['workingminutes'] =  $workingminutes;
                        $work['shift'] =  $shift;
                        $work['manpower'] =  $manpower;
                        $work['task_id'] =  $task_id;
                        $work['activity_id'] =  $activity_id;
                        $work['overtimelatefine'] =  '0';
                        $work['allowancesdeductions'] =  '0';
                        if((!empty($manpowerattendance->overtimehours)&&!empty($manpowerattendance->overtimeamount)&&!empty($manpowerattendance->overtimetotal))||(!empty($manpowerattendance->latefinehours)&&!empty($manpowerattendance->latefineamount)&&!empty($manpowerattendance->latefinetotal))){
                            $work['overtimelatefine'] =  '1';
                        }
                        if(!empty($manpowerattendance->allowances)||!empty($manpowerattendance->deductions)){
                            $work['allowancesdeductions'] =  '1';
                        }
                        $work['description'] =  $description ?: '';
                        $totalprice =  account_numberformat($totalprice);
                        $work['totalprice'] =  $totalprice;
                        $workersarray[] = $work;
                        $contractorprice += $totalprice;
                    }
                    $contrinfo['workersarray'] = $workersarray;
                    $contrinfo['totalprice'] = account_numberformat($contractorprice);
                    $workerslistarray[] = $contrinfo;
                }
                $response['status'] = 200;
                $response['message'] = 'Attendance List';
                $response['response'] = $workerslistarray;
                 $totalworkers = \App\ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('work_date',$date)->sum('manpower');
                $totalamount = \App\ManpowerLog::where('company_id',$companyid)->where('project_id',$projectdetails->id)->where('work_date',$date)->sum('totalprice');
                $response['totalworkers'] = $totalworkers;
                $response['totalamount'] = account_numberformat($totalamount);
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-attendance-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function workersDetailsList(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $startdate = !empty($request->start_date) ? date('Y-m-d',strtotime($request->start_date)) : '';
                $enddate = !empty($request->end_date) ? date('Y-m-d',strtotime($request->end_date)) : '';
                $workerid = $request->worker_id ?: 0;
                $category = $request->category_id;

                $manpowerarray  = ManpowerLog::where('manpower_logs.company_id',$companyid)->where('manpower_logs.project_id',$projectdetails->id)
                                    ->where('manpower_logs.work_date','>=',$startdate)->where('manpower_logs.work_date','<=',$enddate)
                                    ->where('manpower_logs.worker_id',$workerid);
                if(!empty($category)){
                    $manpowerarray = $manpowerarray->where('manpower_logs.manpower_category',$category);
                }
                $manpowerworkersarray = $manpowerarray->orderBy('manpower_logs.work_date','desc')->get();
                $totalworkers = $totalamount= 0;
                $workerslistarray = array();
                if(count($manpowerworkersarray)>0){
                    if(!empty($workerid)){
                        $workers = Workers::find($workerid);
                    }
                    foreach ($manpowerworkersarray as $manpowerattendance){
                        $work = array();
                        $work['id'] = !empty($workers->id) ? $workers->id : 0;
                        $work['manpowerid'] = !empty($manpowerattendance->id) ? $manpowerattendance->id : 0;
                        $work['workertype'] = !empty($workers->type) ? $workers->type : 'category';
                        $work['labourtype'] = !empty($manpowerattendance->contractor) ? 'contractor' : 'company';
                        $work['name'] =  !empty($workers->name) ? $workers->name : '';
                        $work['workdate'] =  !empty($manpowerattendance->work_date) ? date('d M Y',strtotime($manpowerattendance->work_date)) : '';
                         $manpowercategory = !empty($workers->category) ? $workers->category : '0';
                        $salary = !empty($workers->salary) ? $workers->salary : '0';
                        $salarytype = !empty($workers->salarytype) ? $workers->salarytype : '';
                        $hourspershift = !empty($workers->workinghours) ? $workers->workinghours : '';
                        $workinghours = !empty($workers->workinghours) ? $workers->workinghours : '';
                        $remark =  '';
                        $shift =  '';
                        $task_id =  '';
                        $activty_id =  '';
                        $workingminutes =  '0';
                        $manpower =  '0';
                        $overtimehours =  '0';
                        $overtimeamount =  '0';
                        $overtimetotal =  '0';
                        $latefineamount =  '0';
                        $latefinehours =  '0';
                        $latefinetotal =  '0';
                        $totalprice =  '0';
                        $allowances =  array();
                        $deductions =  array();
                        if(!empty($manpowerattendance->id)){
                            $manpowercategory =  $manpowerattendance->manpower_category ?: 0;
                            $task_id =  $manpowerattendance->task_id ?: 0;
                            $activty_id =  $manpowerattendance->activty_id ?: 0;
                            $salary =  $manpowerattendance->salaryperday ?: 0;
                            $salarytype =  $manpowerattendance->salarytype ?: '';
                            $hourspershift =  $manpowerattendance->hourspershift ?: 0;
                            $workinghours =  $manpowerattendance->workinghours ?: 0;
                            $workingminutes =  $manpowerattendance->workingminutes ?: 0;
                            $shift =  $manpowerattendance->shift ?: '';
                            $manpower =  $manpowerattendance->manpower ?: 0;
                            $overtimehours =  $manpowerattendance->overtimehours ?: '';
                            $overtimeamount =  $manpowerattendance->overtimeamount ?: '';
                            $overtimetotal =  $manpowerattendance->overtimetotal ?: '';
                            $latefinehours =  $manpowerattendance->latefinehours ?: '';
                            $latefineamount =  $manpowerattendance->latefineamount ?: '';
                            $latefinetotal =  $manpowerattendance->latefinetotal ?: '';
                            $allowances =  !empty($manpowerattendance->allowances) ? json_decode($manpowerattendance->allowances) : array();
                            $deductions =  !empty($manpowerattendance->deductions) ? json_decode($manpowerattendance->deductions) : array();
                            $remark =  $manpowerattendance->description ?: '';
                            $totalprice =  $manpowerattendance->totalprice ?: 0;
                        }
                        $work['category'] = $manpowercategory;
                        $work['category_name'] = get_manpower_category($manpowercategory);
                        $work['salary'] =  $salary;
                        $work['task_id'] =  $task_id;
                        $work['activty_id'] =  $activty_id;
                        $work['salarytype'] =  $salarytype;
                        $work['hourspershift'] =  $hourspershift;
                        $work['workinghours'] =  $workinghours;
                        $work['workingminutes'] =  $workingminutes;
                        $work['shift'] =  $shift;
                        $work['manpower'] =  $manpower;
                        $work['overtimehours'] =  $overtimehours;
                        $work['overtimeamount'] =  $overtimeamount;
                        $work['overtimetotal'] =  $overtimetotal;
                        $work['latefinehours'] =  $latefinehours;
                        $work['latefineamount'] =  $latefineamount;
                        $work['latefinetotal'] =  $latefinetotal;
                        $work['allowances'] =  $allowances;
                        $work['deductions'] =  $deductions;
                        $work['description'] =  $remark;
                        $work['overtimelatefine'] =  '0';
                        $work['allowancesdeductions'] =  '0';
                        if((!empty($manpowerattendance->overtimehours)&&!empty($manpowerattendance->overtimeamount)&&!empty($manpowerattendance->overtimetotal))||(!empty($manpowerattendance->latefinehours)&&!empty($manpowerattendance->latefineamount)&&!empty($manpowerattendance->latefinetotal))){
                            $work['overtimelatefine'] =  '1';
                        }
                        if(!empty($manpowerattendance->allowances)||!empty($manpowerattendance->deductions)){
                            $work['allowancesdeductions'] =  '1';
                        }
                        $work['totalprice'] =  $totalprice;
                        $workerslistarray[] = $work;
                        $totalworkers += $manpower;
                        $totalamount += $totalprice;
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Attendance List';
                $response['response'] = $workerslistarray;
                $response['totalworkers'] = $totalworkers;
                $response['totalamount'] = account_numberformat($totalamount);
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-attendance-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function workerAttendance(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $workerid = $request->worker_id ?: 0;
                $attendanceid = $request->manpower_id ?: 0;
                $date = !empty($request->date) ? date('Y-m-d',strtotime($request->date)) : date('Y-m-d');
                $timein = !empty($request->timein) ? date('H:i:s',strtotime($request->timein)) : date('H:i:s');
                $timeout = !empty($request->timeout) ? date('H:i:s',strtotime($request->timeout)) : date('H:i:s');
                $workers = Workers::where('id',$workerid)->first();
                $manpowerattendance = ManpowerLog::where('id',$attendanceid)->where('worker_id',$workerid)->first();
                $work = array();
                $work['id'] = $workers->id;
                $work['manpowerid'] = !empty($manpowerattendance->id) ? $manpowerattendance->id : 0;
                $work['workertype'] = $workers->type;
                $work['name'] = $workers->name;
                $manpowercategory = $workers->category;
                $salary =  $workers->salary;
                $salarytype =  $workers->salarytype;
                $hourspershift =  $workers->workinghours;
                $workinghours =  $workers->workinghours;
                $remark =  '';
                $shift =  '';
                $workingminutes =  '0';
                $manpower =  '0';
                $overtimehours =  '0';
                $overtimeamount =  '0';
                $overtimetotal =  '0';
                $latefineamount =  '0';
                $latefinehours =  '0';
                $latefinetotal =  '0';
                $totalprice =  '0';
                $allowancestotalprice =  '0';
                $deductionstotalprice =  '0';
                $hourrate = '0';
                $allowances =  array();
                $deductions =  array();
                if(!empty($manpowerattendance->id)){
                    $manpowercategory =  $manpowerattendance->manpower_category;
                    $salary =  $manpowerattendance->salaryperday ?: '';
                    $salarytype =  $manpowerattendance->salarytype ?: '';
                    $hourspershift =  $manpowerattendance->hourspershift ?: '0';
                    $workinghours =  $manpowerattendance->workinghours ?: '0';
                    $workingminutes =  $manpowerattendance->workingminutes ?: '0';
                    $shift =  $manpowerattendance->shift ?: '';
                    $manpower =  $manpowerattendance->manpower ?: '0';
                    $overtimehours =  $manpowerattendance->overtimehours ?: '0';
                    $overtimeamount =  $manpowerattendance->overtimeamount ?: '0';
                    $overtimetotal =  $manpowerattendance->overtimetotal ?: '0';
                    $latefinehours =  $manpowerattendance->latefinehours ?: '0';
                    $latefineamount =  $manpowerattendance->latefineamount ?: '0';
                    $latefinetotal =  $manpowerattendance->latefinetotal ?: '0';
                    $allowances =  !empty($manpowerattendance->allowances) ? json_decode($manpowerattendance->allowances) : array();
                    $deductions =  !empty($manpowerattendance->deductions) ? json_decode($manpowerattendance->deductions) : array();
                    $remark =  $manpowerattendance->description ?: '';
                    $totalprice =  $manpowerattendance->totalprice ?: '0';
                    $allowancestotalprice = json_amount($allowances);
                    $deductionstotalprice = json_amount($deductions);

                    $hourrate = !empty($manpowerattendance->id) ? get_labour_hour_rate($manpowerattendance) : 0;
                }
                $work['category'] = $manpowercategory ?: '0';
                $work['category_name'] = get_manpower_category($manpowercategory);
                $work['task_id'] =  !empty($manpowerattendance->task_id) ? $manpowerattendance->task_id : '';
                $work['activity_id'] =  !empty($manpowerattendance->activity_id) ? $manpowerattendance->activity_id : '';
                $work['salary'] =  $salary;
                $work['salarytype'] =  $salarytype;
                if($salarytype=='monthly'){
                    $work['perday'] =  account_numberformat($hourrate*$hourspershift);
                }else{
                $work['perday'] =  account_numberformat($salary);
                }
                $work['hourspershift'] =  $hourspershift;
                $work['hourrate'] =  $hourrate;
                $work['workinghours'] =  $workinghours;
                $work['workingminutes'] =  $workingminutes;
                $work['shift'] =  $shift;
                $work['manpower'] =  $manpower;
                $work['overtimehours'] =  $overtimehours;
                $work['overtimeamount'] =  $overtimeamount;
                $work['overtimetotal'] =  $overtimetotal;
                $work['latefinehours'] =  $latefinehours;
                $work['latefineamount'] =  $latefineamount;
                $work['latefinetotal'] =  $latefinetotal;
                $work['allowances'] =  $allowances;
                $work['allowancesamount'] =  $allowancestotalprice;
                $work['deductionsamount'] =  $deductionstotalprice;
                $work['deductions'] =  $deductions;
                $work['description'] =  $remark;
                $work['totalprice'] =  account_numberformat($totalprice);
                $work['imagesarray'] = !empty($manpowerattendance->images) ?  $manpowerattendance->images : array();
                $response['status'] = 200;
                $response['message'] = 'Attendance Updated Successfully';
                $response['response'] = $work;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-attendance',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function workerAttendancePost(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                if(!empty($projectid)){
                    $projectdetails = AppProject::find($projectid);
                    $companyid = $projectdetails->company_id;
                }
                $workerid = $request->worker_id ?: 0;
                $workers = Workers::where('id',$workerid)->first();
                if(empty($workers)){
                    $response['message'] = 'Worker Not Found';
                    $response['status'] = 301;
                    return $response;
                }
                $attendanceid = $request->manpower_id ?: 0;
                $empid = $request->emp_id ?: 0;
                $contractoruser = $request->contractor_id ?: 0;
                $noofworkers = $request->noofworkers ?: 1;
                $taskid = $request->task_id ?: "";
                $taskdetails = \App\Task::where('id',$taskid)->first();
                $activityid = $request->activity_id ?: "";
                $startdate = !empty($request->date) ? date('Y-m-d',strtotime($request->date)) : date('Y-m-d');
                 $manpowerattendance = ManpowerLog::where('id',$attendanceid)->first();
                if(empty($manpowerattendance)){
                    $productissueuni = ManpowerLog::where('company_id',$companyid)->where('work_date',$startdate)->orderBy('inc','desc')->max('inc');
                    $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                    $unique_id = 'LA-'.$newid;

                    $manpowerattendance = new ManpowerLog();
                    $manpowerattendance->unique_id = $unique_id;
                    $manpowerattendance->company_id = $companyid;
                    $manpowerattendance->added_by = $user->id;
                    $manpowerattendance->emp_id = $empid;
                    $manpowerattendance->contractor = $contractoruser ?: 0;
                    $manpowerattendance->work_date =$startdate;
                    $manpowerattendance->project_id = $projectdetails->id;
                    $manpowerattendance->worker_id = $workerid;
                    $manpowerattendance->title_id =  0;
                    $manpowerattendance->segment_id =  0;
                }
                $manpowerattendance->manpower_category =  $workers->category;
                $manpowerattendance->task_id =  !empty($taskdetails) ? $taskdetails->id : '0';
                $manpowerattendance->costitem_id =  !empty($taskdetails) ? $taskdetails->cost_item_id : '0';
                $manpowerattendance->activity_id =  $activityid;
                $manpowerattendance->manpower = $noofworkers ?: 0;
                $manpowerattendance->shift = !empty($request->shift) ? strtolower($request->shift) : 'day';
                $hourspershift = $workers->workinghours ?: 0;
                $manpowerattendance->hourspershift =  $hourspershift;
                $manpowerattendance->workinghours = !empty($request->workinghours) ? $request->workinghours : $hourspershift;
                $manpowerattendance->salaryperday =  $workers->salary ?: 0;
                $manpowerattendance->salarytype =   strtolower($workers->salarytype) ?: '';
                $manpowerattendance->overtimehours = $request->overtimehours;
                $manpowerattendance->overtimeamount = $request->overtimeamount;
                $manpowerattendance->overtimetotal = $request->overtimetotal;
                $manpowerattendance->latefinehours = $request->latefinehours;
                $manpowerattendance->latefineamount = $request->latefineamount;
                $manpowerattendance->latefinetotal = $request->latefinetotal;
                $manpowerattendance->description = $request->description;
                $manpowerattendance->allowances = !empty(json_decode($request->allowances)) ?  $request->allowances : '';
                $manpowerattendance->deductions = !empty(json_decode($request->deductions)) ?  $request->deductions : '';
                $manpowerattendance->save();
                $totalprice = labour_totalprice($manpowerattendance);
                $manpowerattendance->totalprice = $totalprice;
                $manpowerattendance->save();
                $response['status'] = 200;
                $response['message'] = 'Attendance Updated Successfully';
                $response['response'] = $manpowerattendance;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'workers-attendance-post',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteWorkersAttendance(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->manpower_id;
                $channel =  ManpowerLog::where('id',$id)->first();
                if(!empty($channel->id)){
                    if($channel->added_by==$user->id){
                        $channel->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Worker attendance Successfully';
                        return $response;
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'You do not permission to delete attendance';
                        return $response;
                    }
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Worker attendance Not found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-workers-attendance',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function testsecApi(Request $request){
        echo 'hi';
    }
}