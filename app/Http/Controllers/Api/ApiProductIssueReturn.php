<?php

namespace App\Http\Controllers\Api;

use App\AppProductIssue;
use App\AppProductLog;
use App\AppStock;
use App\AppStore;
use App\Company;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductIssue;
use App\ProductIssueReturn;
use App\ProductIssueFilesReturn;
use App\ProductIssueReturnReply;
use App\ProductReturns;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiProductIssueReturn extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
         if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        $response = array();
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $products = json_decode(trim($request->product));
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }

                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }
                $issueuniqueid = $request->issue_unique_id;
                $productissues = AppProductIssue::where('unique_id',$issueuniqueid)->first();
                if(empty($productissues)){
                    $response['status'] = 301;
                    $response['message'] = 'Material Issue Not found';
                    return $response;
                }
                $productissueuni = ProductIssueReturn::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
                $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                $unique_id = 'Prod-MIR-'.date("d-m-Y").'-'.$newid;
                $response = array();
                if(!empty($products)) {
                    foreach ($products as $product) {
                        if (!empty($product->qty)) {
                            $productissues = AppProductIssue::where('store_id', $storeId->id)->where('project_id', $projectId->id)->where('unique_id',$issueuniqueid)->where('product_id',$product->product_id)->first();
                            if(!empty($productissues)){
                                $issueqty = $productissues->quantity ?: 0;
                                if($issueqty<$product->qty){
                                    $response['status'] = 300;
                                    $response['message'] = get_local_product_name($product->product_id).' Material return quantity is greater than issued';
                                    return $response;
                                }
                            }
                        }
                    }
                    foreach ($products as $product) {
                        if (!empty($product->qty)) {
                            $scheck = AppStock::where('cid', $product->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                            $bal = 0;
                            if (!empty($scheck)) {
                                $bal = $scheck->stock + $product->qty;
                                $scheck->stock = $bal;
                                $scheck->save();
                                $productissues = AppProductIssue::where('unique_id',$issueuniqueid)->where('product_id',$product->product_id)->first();
                                if(!empty($productissues)){
                                    $issueqty = $productissues->quantity ?: 0;
                                    if($issueqty>=$product->qty){
                                        $pro = new ProductIssueReturn();
                                        $pro->unique_id = $unique_id;
                                        $pro->project_id = $projectId->id;
                                        $pro->company_id = $projectId->company_id;
                                        $pro->store_id = $storeId->id;
                                        $pro->issue_id = $productissues->id;
                                        $pro->unit_id = $productissues->unit_id;
                                        $pro->product_id = $product->product_id;
                                        $pro->qty = $product->qty;
                                        $pro->remark = $product->remark;
                                        $pro->date = Carbon::parse($product->date)->format('Y-m-d');
                                        $pro->added_by = $user->id;
                                        $pro->inc = $newid;
                                        $pro->save();

                                        if(!empty($product->images)){
                                            $productimagsarry = array_filter(array_unique(explode(',',$product->images)));
                                            foreach($productimagsarry as $productimags){
                                                $productimage =  ProductIssueFilesReturn::find($productimags);
                                                if(!empty($productimage)){
                                                    $productimage->return_id =  $pro->id;
                                                    $productimage->save();
                                                    $sourcepath = 'uploads/product-issue-return-files/0/'.$productimage->hashname;
                                                    $destinationPath = 'uploads/product-issue-return-files/'.$pro->id.'/'.$productimage->hashname;
                                                    if(file_exists($sourcepath)){
                                                        if (!file_exists('uploads/product-issue-return-files/'.$pro->id)) {
                                                            mkdir('uploads/product-issue-return-files/'.$pro->id, 0777, true);
                                                        }
                                                        rename($sourcepath,$destinationPath);
                                                    }
                                                }
                                            }
                                        }

                                        if ($pro) {
                                            $pl = new AppProductLog();
                                            $pl->store_id = $storeId->id;
                                            $pl->project_id = $projectId->id;
                                            $pl->company_id = $projectId->company_id;
                                            $pl->created_by = $user->id;
                                            $pl->module_id = $pro->id;
                                            $pl->module_name = 'product_issue_return';
                                            $pl->product_id = $product->product_id;
                                            $pl->quantity = $product->qty;
                                            $pl->balance_quantity = $bal;
                                            $pl->transaction_type = 'plus';
                                            $pl->remark = $product->remark;
                                            if(!empty($pro->date)){
                                                $pl->date =  Carbon::parse($pro->date)->format('Y-m-d');
                                            }
                                            $pl->save();
                                        }

                                    }
                                }
                            }
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Material returned Successfully';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Materials Not Found';
                    return $response;
                }

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'add-product-issue-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }

        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if (!empty($request->API_KEY)&&!empty($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $products = json_decode(trim($request->product));
            try{
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }

                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(empty($storeId)){
                    $response['status'] = 301;
                    $response['message'] = 'Store Not found';
                    return $response;
                }
                $issueuniqueid = $request->return_unique_id;
                $productissueuni = ProductIssueReturn::where('company_id',$projectId->company_id)->orderBy('id','desc')->max('inc');
                $newid = !empty($productissueuni) ?  $productissueuni+1 : 1;
                $unique_id = 'Prod-MIR-'.date("d-m-Y").'-'.$newid;
                $response = array();
                if(!empty($products)) {
                    foreach ($products as $product) {
                        if ($product->qty !== '') {
                            $scheck = AppStock::where('cid', $product->product_id)->where('store_id', $storeId->id)->where('project_id', $projectId->id)->first();
                            $bal = 0;
                            if (empty($scheck)) {
                                $response['status'] = 300;
                                $response['message'] = 'Material Stock Not Available';
                                return $response;
                            }
                            if ($scheck->stock < $product->qty) {
                                $response['status'] = 300;
                                $response['message'] = 'Available Material stock is ' . $scheck->stock;
                                return $response;
                            }
                            if ($product->qty > $scheck->stock) {
                                $response['status'] = 200;
                                $response['message'] = 'Issued Quantity greater then available stock';
                                return $response;
                            } else {
                                $bal = $scheck->stock - $product->qty;
                                $scheck->stock = $bal;
                                $scheck->save();
                            }
                            if(!empty($product->id)){
                                $pro =  ProductIssueReturn::find($product->id);
                                $prissue = ProductIssue::where('id',$pro->issue_id)->first();
                                if($product->qty>$prissue->quantity){
                                    $response['status'] = 300;
                                    $response['message'] = 'Return Quantity greater then Issued';
                                    return $response;
                                }elseif($product->qty<$prissue->quantity){
                                    $prissue->returned = '0';
                                    $prissue->save();
                                }
                                $prevquant = $pro->qty ?: 0;
                            }else{
                                $pro = new ProductIssueReturn();
                                $pro->unique_id = $unique_id;
                                $pro->added_by = $user->id;
                            }
                            $pro->project_id = $projectId->id;
                            $pro->store_id = $storeId->id;
                            $pro->product_id = $product->product_id;
                            $pro->qty = $product->qty;
                            $pro->remark = $product->remark;
                            if(!empty($product->date)){
                                $pro->date =  Carbon::parse($product->date)->format('Y-m-d');
                            }
                            $pro->save();

                            $newuanti = $product->qty-$prevquant;
                            $bal = $scheck->stock + $newuanti;
                            $scheck->stock = $bal;
                            $scheck->save();

                            if(!empty($product->images)){
                                $productimagsarry = array_filter(array_unique(explode(',',$product->images)));
                                foreach($productimagsarry as $productimags){
                                    $productimage =  ProductIssueFilesReturn::find($productimags);
                                    if(!empty($productimage)){
                                        $productimage->return_id =  $pro->id;
                                        $productimage->save();
                                        $sourcepath = 'uploads/product-issue-return-files/0/'.$productimage->hashname;
                                        $destinationPath = 'uploads/product-issue-return-files/'.$pro->id.'/'.$productimage->hashname;
                                        if(file_exists($sourcepath)){
                                            if (!file_exists('uploads/product-issue-return-files/'.$pro->id)) {
                                                mkdir('uploads/product-issue-return-files/'.$pro->id, 0777, true);
                                            }
                                            rename($sourcepath,$destinationPath);
                                        }
                                    }
                                }
                            }

                            if (!empty($pro)) {
                                $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                                    ->where('module_id', $pro->id)
                                    ->where('product_id', $product->product_id)
                                    ->where('module_name','product_issue_return')
                                    ->orderBy('id', 'DESC')->first();
                                if(empty($pl)){
                                    $pl = new AppProductLog();
                                }
                                $pl->store_id = $storeId->id;
                                $pl->project_id = $request->project_id;
                                $pl->company_id = $projectId->company_id;
                                $pl->created_by = $user->id;
                                $pl->module_id = $pro->id;
                                $pl->module_name = 'product_issue_return';
                                $pl->product_id = $product->product_id;
                                $pl->quantity = $product->qty;
                                $pl->balance_quantity = $bal;
                                $pl->transaction_type = 'plus';
                                $pl->remark = $product->remark;
                                if(!empty($product->date)){
                                    $pl->date =  Carbon::parse($product->date)->format('Y-m-d');
                                }
                                $pl->save();
                            }
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Material returned Updated Successfully';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Materials Not Found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                $medium = $request->medium ?: 'api';
                app_log($e,'add-product-issue',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $medium = $request->medium ?: 'api';
                $responsearray = array();
                $returnid = $request->return_id;
                $issues = ProductIssueReturn::where('id',$returnid)->first();
                if(empty($issues)){
                    $response['status'] = 301;
                    $response['message'] = 'Material Issue Return Not found';
                    return $response;
                }
                $productsname = Product::join('product_issue_return','product_issue_return.product_id','=','products.id')->where('product_issue_return.id',$issues->id)->pluck('products.name')->toArray();
                $issuesarray = array();
                $issuesarray['id'] = $issues->id;
                $issuesarray['unique_id'] = $issues->unique_id;
                $issuesarray['project_id'] = $issues->project_id;
                $issuesarray['project_name'] = get_project_name($issues->project_id);
                $issuesarray['product_name'] = !empty($productsname) ? implode(', ',$productsname) : '';
                $issuesarray['added_by'] = $issues->added_by;
                $issuesarray['issued_name'] = get_user_name($issues->added_by);
                $issuesarray['returned_date'] = date('d M Y',strtotime($issues->date));
                $issuesarray['images'] = $issues->images;
                $issuesarray['created_date'] = date('d M Y',strtotime($issues->created_at));
                $response['status'] = 200;
                $response['message'] = 'Issues Report';
                $response['responselist'] = $issuesarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'product-issue-return-detail',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function issueList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $permission_project_id = $request->permission_project_id;
                $permission_name = $request->permission_name;
                $medium = $request->medium ?: 'api';
                $responsearray = array();
                $issueuniqueid = $request->issue_unique_id;
                $productissues = AppProductIssue::where('unique_id',$issueuniqueid)->pluck('id')->toArray();
                if(count($productissues)<=0){
                    $response['status'] = 301;
                    $response['message'] = 'Material Issue Not found';
                    return $response;
                }

                $issueslist = ProductIssueReturn::join('stores','product_issue_return.project_id','=','stores.project_id')->select('product_issue_return.*')->where('product_issue_return.project_id',$projectid);
                $startdate = str_replace('/','-',$request->start_date);
                $fromdate =  !empty($startdate) ? date('Y-m-d 00:00:01',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $todate = !empty($enddate) ? date('Y-m-d 23:59:59',strtotime($enddate)) : '';
                $issueslist = $issueslist->whereIn('issue_id',$productissues);
                if(!empty($fromdate)){
                    $issueslist = $issueslist->where('product_issue_return.created_at','>=',$fromdate);
                }
                if(!empty($todate)){
                    $issueslist = $issueslist->where('product_issue_return.created_at','<=',$todate);
                }

                $issueslist = $issueslist->groupBy('product_issue_return.unique_id');
                $page = $request->page;
                if($page=='all'){
                    $issueslist = $issueslist->orderBy('product_issue_return.id','desc')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $issueslist = $issueslist->offset($skip)->take($count)->orderBy('product_issue_return.id','desc')->get();
                }

                $issueslistarray = array();
                foreach ($issueslist as $issues){
                    $productsname = Product::join('product_issue_return','product_issue_return.product_id','=','products.id')->where('product_issue_return.unique_id',$issues->unique_id)->pluck('products.name')->toArray();
                    $issuesarray = array();
                    $issuesarray['unique_id'] = $issues->unique_id;
                    $issuesarray['project_id'] = $issues->project_id;
                    $issuesarray['project_name'] = get_project_name($issues->project_id);
                    $issuesarray['product_name'] = !empty($productsname) ? implode(', ',$productsname) : '';
                    $issuesarray['added_by'] = $issues->added_by;
                    $issuesarray['issued_name'] = get_user_name($issues->added_by);
                    $issuesarray['returned_date'] = date('d M Y',strtotime($issues->date));
                    $issuesarray['created_date'] = date('d M Y',strtotime($issues->created_at));
                    $issueslistarray[] = $issuesarray;
                }
                $response['status'] = 200;
                $response['message'] = 'Issues Report';
                $response['responselist'] = $issueslistarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'list-issues-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function issueProductsList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $medium = $request->medium ?: 'api';
                $unique_id = $request->unique_id ?: '';
                $responsearray = array();
                $issueslist = ProductIssueReturn::join('stores','product_issue_return.project_id','=','stores.project_id')->select('product_issue_return.*')->where('product_issue_return.project_id',$projectid);
                $startdate = str_replace('/','-',$request->start_date);
                $fromdate =  !empty($startdate) ? date('Y-m-d 00:00:01',strtotime($startdate)) : '';
                $enddate = str_replace('/','-',$request->end_date);
                $todate = !empty($enddate) ? date('Y-m-d 23:59:59',strtotime($enddate)) : '';
                if(!empty($unique_id)){
                    $issueslist = $issueslist->where('product_issue_return.unique_id',$unique_id);
                }
                if(!empty($fromdate)){
                    $issueslist = $issueslist->where('product_issue_return.created_at','>=',$fromdate);
                }
                if(!empty($todate)){
                    $issueslist = $issueslist->where('product_issue_return.created_at','<=',$todate);
                }
                $issueslist = $issueslist->get();
                $issueslistarray = array();
                foreach ($issueslist as $return){
                    $productissue = ProductIssue::where('id',$return->issue_id)->first();
                    $returnproducts = ProductIssueReturn::where('issue_id',$return->issue_id)->sum('qty');
                    $issuesarray = array();
                    $issuesarray['id'] = $return->id;
                    $issuesarray['issue_id'] =  !empty($productissue->id) ? $productissue->id : 0;
                    $issuesarray['issue_unique_id'] =  !empty($productissue->unique_id) ? $productissue->unique_id : 0;
                    $issuesarray['unique_id'] = $return->unique_id;
                    $issuesarray['product_id'] = $return->product_id;
                    $issuesarray['product_name'] = get_local_product_name($return->product_id);
                    $issuesarray['unit_id'] =  $return->unit_id;
                    $issuesarray['unit_name'] =  get_unit_name($return->unit_id);
                    $issuesarray['quantity'] =  !empty($productissue->quantity) ? $productissue->quantity : 0;
                    $issuesarray['return_quantity'] =  $return->qty;
                    $issuesarray['received_quantity'] =  !empty($returnproducts) ? $returnproducts : 0;
                    $issuesarray['issued_by'] = $return->added_by;
                    $issuesarray['issued_name'] = get_user_name($return->added_by);
                    $issuesarray['remark'] = $return->remark;
                    $issuesarray['returned_date'] = date('d M Y',strtotime($return->date));
                    $issuesarray['created_date'] = date('d M Y',strtotime($return->created_at));
                    $issuesarray['images'] = !empty($return->images) ? $return->images : array();
                    $issueslistarray[] = $issuesarray;
                }
                $response['status'] = 200;
                $response['message'] = 'Issues Report';
                $response['responselist'] = $issueslistarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'list-product-issue-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $productissueid = $request->return_id;
                $uniqueid = $request->unique_id;
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(!empty($uniqueid)){
                    $issuesarray = ProductIssueReturn::where('project_id',$projectId->id)->where('unique_id',$uniqueid)->get();
                    if(empty($issuesarray)){
                        $response['status'] = 301;
                        $response['message'] = 'Issued product not found';
                        return $response;
                    }
                    if(count($issuesarray)>0){
                        foreach($issuesarray as $issues){
                            $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                                ->where('module_id', $issues->id)
                                ->where('product_id', $issues->product_id)
                                ->where('module_name','product_issue_return')->delete();
                            ProductIssueFilesReturn::where('return_id',$issues->id)->delete();
                            ProductIssueReturnReply::where('return_id',$issues->id)->delete();

                            $issues->delete();
                        }
                    }
                }else{
                    $issues = ProductIssueReturn::find($productissueid);
                    if(empty($issues)){
                        $response['status'] = 301;
                        $response['message'] = 'Issued product not found';
                        return $response;
                    }
                    $pl = AppProductLog::where('project_id', $projectId->id)->where('store_id', $storeId->id)
                        ->where('module_id', $issues->id)
                        ->where('product_id', $issues->product_id)
                        ->where('module_name','product_issue_return')->delete();
                    ProductIssueFilesReturn::where('return_id',$issues->id)->delete();
                    ProductIssueReturnReply::where('return_id',$issues->id)->delete();
                    $issues->delete();

                }
                $response['status'] = 200;
                $response['message'] = 'Material Issue Deleted';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-product-issue-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $issueid = $request->return_id ?: 0;
                $replyid = $request->reply_id ?: 0;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $projectId->company_id;
                    $fileData =  $request->file('file');
                    $file = new ProductIssueFilesReturn();
                    $file->added_by = $user->id;
                    $file->company_id = $companyid;
                    $file->return_id = $issueid ?: 0;
                    $file->reply_id = $replyid ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/product-issue-return-files/'.$issueid;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/product-issue-return-files/'.$issueid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'product-issue-return-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('product-issue-return-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $issueid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$issueid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $issueid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('product-issue-return-files/'.$issueid.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$issueid.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();

                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['image_url'] = get_issue_return_image_link($file->id,$issueid);
                    $response['message'] = 'Material Issue Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-issue-image-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteStoreImage(Request $request){
        $id = $request->image_id;
        $task = ProductIssueFilesReturn::findOrFail($id);
        if(!empty($task)){
            $task->delete();
        }
        $response['status'] = 200;
        $response['message'] = 'Image deleted successfully';
        return $response;
    }

    public function issueItemReply(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $projectId = Project::find($request->project_id);
                if(empty($projectId)){
                    $response['status'] = 301;
                    $response['message'] = 'Project Not found';
                    return $response;
                }
                $productissueid = $request->return_id;
                $productuniqueid = $request->unique_id;
                $storeId = AppStore::where('project_id',$projectId->id)->orderBy('id','DESC')->first();
                if(!empty($productissueid)){
                    $issues = ProductIssueReturn::find($productissueid);
                }else{
                    $issues = ProductIssueReturn::where('unique_id',$productuniqueid)->first();
                }
                if(empty($issues)){
                    $response['status'] = 301;
                    $response['message'] = 'Return product not found';
                    return $response;
                }
                $issuereply = new ProductIssueReturnReply();
                $issuereply->company_id = $projectId->company_id;
                $issuereply->added_by = $user->id;
                $issuereply->unique_id = $issues->unique_id;
                $issuereply->return_id = $issues->id;
                $issuereply->comment = $request->comment ?: '';
                $issuereply->save();

                $response['status'] = 200;
                $response['return_id'] = $issuereply->return_id;
                $response['reply_id'] = $issuereply->id;
                $response['message'] = 'Material Issue Return Reply Updated';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'issue-item-reply-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function issueReplyList(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $medium = $request->medium ?: 'api';
            try{
                $response = array();
                $productissueid = $request->return_id;
                $productuniqueid = $request->unique_id;
                if(!empty($productissueid)){
                    $issues = ProductIssueReturn::find($productissueid);
                }else{
                    $issues = ProductIssueReturn::where('unique_id',$productuniqueid)->first();
                }
                if(empty($issues)){
                    $response['status'] = 301;
                    $response['message'] = 'Issued product not found';
                    return $response;
                }
                $issuereplyarray =  ProductIssueReturnReply::where('return_id',$issues->id)->get();
                $issuereplylist = array();
                if(count($issuereplyarray)>0){
                    foreach ($issuereplyarray as $issuereply){

                        $issuearray = array();
                        $issuearray['id'] = $issuereply->id;
                        $issuearray['unique_id'] = $issuereply->unique_id;
                        $issuearray['return_id'] = $issuereply->return_id;
                        $issuearray['comment'] = $issuereply->comment;
                        $issuearray['alignment'] = $issuereply->alignment;
                        $issuearray['added_by'] = $issuereply->added_by;
                        $issuearray['userdetails'] = $issuereply->userdetails;
                        $issuearray['created_date'] = $issuereply->created_at;
                        $issuearray['datetime'] = $issuereply->datetime;
                        $issuearray['images'] = !empty($issuereply->images) ? $issuereply->images : array();
                        $issuereplylist[] = $issuearray;
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Material Issue Reply Updated';
                $response['responselist'] = $issuereplylist;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'issue-item-reply-return',$userid,$medium);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

}
