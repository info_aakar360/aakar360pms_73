<?php

namespace App\Http\Controllers\Api;

use App\AppLogs;
use App\ClientDetails;
use App\Company;
use App\Contractors;
use App\Conversation;
use App\EmailNotificationSetting;
use App\Employee;
use App\EmployeeDetails;
use App\Http\Controllers\Controller;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\InvitedUser;
use App\Permission;
use App\PermissionRole;
use App\ProjectMember;
use App\ProjectPermission;
use App\ProjectsLogs;
use App\Role;
use App\TempUser;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Notifications\NewUser;
use Illuminate\Support\Facades\Hash;

class AppMemberController extends Controller
{
    use AuthenticatesUsers;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
//        return response()->json(['message' => 'Unauthorised'], 300);
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function addClient(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $curuser = $validate['user'];
            try{
                $existing_user = User::withoutGlobalScope('company')->select('id', 'email')->where('email', $request->input('email'))->where('mobile', $request->input('mobile'))->first();
                if (!$existing_user) {
                    $password = str_random(8);
                    // create new user
                    $user = new User();
                    $user->name = $request->input('name');
                    $user->email = $request->input('email');
                    $user->password = Hash::make($password);
                    $user->mobile = $request->input('mobile');
                    $user->save();

                    // attach role
                    $role = Role::where('name', 'client')->first();
                    $user->attachRole($role->id);
                }

                $existing_client_count = ClientDetails::select('id', 'email', 'company_id')
                    ->where(
                        [
                            'email' => $request->input('email')
                        ]
                    )->count();

                if ($existing_client_count === 0) {
                    $client = new ClientDetails();
                    $client->user_id = $existing_user ? $existing_user->id : $validate['user']->id;
                    $client->company_id = $validate['user']->company_id;
                    $client->name = $request->input('name');
                    $client->email = $request->input('email');
                    $client->mobile = $request->input('mobile');
                    $client->address = $request->address;
                    $client->save();

                    // attach role
                    if ($existing_user) {
                        $role = Role::where('name', 'client')->where('company_id', $client->company_id)->first();
                        $existing_user->attachRole($role->id);
                    }
                } else {
                    $response['message'] = 'Provided email is already registered. Try with different email.';
                    $response['status'] = 300;
                    return $response;
    //                return Reply::message('Provided email is already registered. Try with different email.');
                }

                $emailSetting = EmailNotificationSetting::all();

                if (!$existing_user && $emailSetting[0]->send_email == 'yes' && $request->sendMail == 'yes') {
                    //send welcome email notification
                    $user->notify(new NewUser($password));
                }

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $validate['user']->company_id;
                $createlog->added_id = $curuser->id;
                $createlog->module_id = $validate['user']->id;
                $createlog->module = 'clients';
                $createlog->project_id = $request->project_id ?: 0;
                $createlog->subproject_id = $request->title_id ?: 0;
                $createlog->segment_id = $request->segment_id ?: 0;
                $createlog->medium = $medium;
                $createlog->heading =  'Client Added';
                $createlog->description = 'Client created by ' . $curuser->name . ' for the project ' . get_project_name($request->project_id);
                $createlog->save();

                $response['status'] = 200;
                $response['message'] = 'Client Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'add-client',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function addContractor(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $users = new User();
                $users->company_id = $user->company_id;
                $users->name = $request->name;
                $users->email = $request->email;
                $users->mobile = $request->mobile;
                $users->password = bcrypt(trim($request->password));
                $users->save();

                $contractors = new Contractors();
                $contractors->user_id = $users->id;
                $contractors->firmname = $request->firmname;
                $contractors->name = $request->name;
                $contractors->email = $request->email;
                $contractors->mobile = $request->mobile;
                $contractors->landline = $request->landline;
                $contractors->address = $request->address;
                $contractors->gst_no = $request->gst_number;
                $contractors->added_by = $user->id;
                $contractors->company_id = $user->company_id;
                $contractors->status = 'active';
                $contractors->save();

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $users->id;
                $createlog->module = 'contractors';
                $createlog->project_id = $request->project_id ?: 0;
                $createlog->subproject_id = $request->title_id ?: 0;
                $createlog->segment_id = $request->segment_id ?: 0;
                $createlog->medium = $medium;
                $createlog->heading =  'Contractor Added';
                $createlog->description = 'Contractor created by ' . $user->name . ' for the project ' . get_project_name($request->project_id);
                $createlog->save();

                $response['status'] = 200;
                $response['contractorid'] = $users->id;
                $response['message'] = 'Contractor Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'add-contractor',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function addEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $company = company();

    //            if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
    //                $response['message'] = 'You need to upgrade your package to create more employees because your employees length is '.company()->employees->count().' and package max employees length is  '.$company->package->max_employees;
    //                $response['status'] = 300;
    //                return $response;
    //            }
    //
    //            if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
    //                $response['message'] = 'You can\'t downgrade package because your employees length is '.company()->employees->count().' and package max employees length is '.$company->package->max_employees;
    //                $response['status'] = 300;
    //                return $response;
    //            }

                $u = new User();
                $u->name = $request->name;
                $u->email = $request->email;
                $u->mobile = $request->mobile;
                $u->gender = $request->gender;
                $u->company_id = $user->company_id;
                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/avatar/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $u->image= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('avatar/', $image, $image->hashName(), 'public');
                            $u->image= $image->hashName();
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());
                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());
                            $u->image= $user->google_url;
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            $u->image= $user->dropbox_link;
                            break;
                    }

                    $u->save();
                    $role = Role::where('name', 'employee')->where('company_id',$validate['user']->company_id)->first();
                    $u->attachRole($role->id);

                    $employeedetail = new EmployeeDetails();
                    $employeedetail->user_id = $u->id;
                    $employeedetail->address = $request->address;
                    $employeedetail->company_id = $validate['user']->company_id;
                    $employeedetail->save();

                    $medium = $request->medium ?: 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $validate['user']->company_id;
                    $createlog->added_id = $validate['user']->id;
                    $createlog->module_id = $employeedetail->id;
                    $createlog->module = 'employee_details';
                    $createlog->project_id = $request->project_id ?: 0;
                    $createlog->subproject_id = $request->title_id ?: 0;
                    $createlog->segment_id = $request->segment_id ?: 0;
                    $createlog->heading =  'Account created';
                    $createlog->description = 'Employee created by ' . $validate['user']->name;
                    $createlog->medium = $medium;
                    $createlog->save();

                }
                $response['status'] = 200;
                $response['message'] = 'Employee Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'add-employee',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateClient(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            try{
                $client = ClientDetails::find($request->id);
                $client->name = $request->name;
                $client->email = $request->email;
                $client->mobile = $request->mobile;
                $client->address = $request->address;
                $client->save();

                $user = User::find($client->user_id);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->mobile = $request->mobile;
                $user->save();

                $response['status'] = 200;
                $response['message'] = 'Client Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-client',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateContractor(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $response = array();
                if($request->contractorid){
                    $contractors = Contractors::find($request->contractorid);
                    $contractors->firmname = $request->firmname;
                    $contractors->name = $request->name;
                    $contractors->email = $request->email;
                    $contractors->mobile = $request->mobile;
                    $contractors->landline = $request->landline;
                    $contractors->address = $request->address;
                    $contractors->gst_no = $request->gst_number;
                    if(!empty($contractors->user_id)){
                        $users = User::find($contractors->user_id);
                    }else{
                        $users = new User();
                    }
                    $users->company_id = $user->company_id;
                    $users->name = $request->name;
                    $users->email = $request->email;
                    $users->mobile = $request->mobile;
                    if(!empty($request->password)){
                        $users->password = bcrypt(trim($request->password));
                    }
                    $users->save();

                    $contractors->user_id = $users->id;
                    $contractors->save();

                    $response['status'] = 200;
                    $response['message'] = 'Contractor Updated Successfully';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-contractor',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Contractor not found';
        $response['status'] = 401;
        return $response;
    }

    public function updateEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $awsurl = awsurl();
                $u = User::find($request->id);
                $u->name = $request->name;
                $u->email = $request->email;
                $u->mobile = $request->mobile;
                $u->gender = $request->gender;
                if ($request->hasFile('image')) {

                    $storage = storage();

                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/avatar/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $u->image= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('avatar/', $image, $image->hashName(), 'public');
                            $u->image= $image->hashName();
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());
                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());
                            $u->image= $user->google_url;
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            $u->image= $user->dropbox_link;
                            break;
                    }

                    $u->image= $image->hashName();
                    $u->save();
                    $employeedetail = EmployeeDetails::where('user_id',$u->id);
                    $employeedetail->address = $request->address;
                    $employeedetail->save();
                }
                $response['status'] = 200;
                $response['message'] = 'Employee Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'update-employee';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteClient(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                Client::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Client Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'delete-client';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                User::where('id',$request->id)->delete();
                EmployeeDetails::where('user_id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'delete-employee';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteContractor(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                Contractors::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Contractor Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'delete-contractor';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getMembers(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                if($request->project_id == 0){
                    $pros = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id');
                    $pids = array();
                    $perm = 'view_employees';
                    if($request->user_type == 'client'){
                        $perm = 'view_clients';
                    }
                    if($request->user_type == 'contractor'){
                        $perm = 'view_contractors';
                    }
                    foreach ($pros as $pro){
                        $pem = Permission::where('name',$perm)->first();
                        $havePerm = ProjectPermission::where('project_id', $pro)->where('user_id', $user->id)->where('permission_id', $pem->id)->first();
                        if($havePerm !== null){
                            array_push($pids, $pro);
                        }
                    }
                    $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                    $pms = ProjectMember::whereIn('project_id',$pids)->orWhereIn('project_id',$project)->where('user_type', $request->user_type)->where('user_id', '<>', $user->id);
                    $uids = array();
                    $uids = $pms->get()->pluck('user_id');
                    $projects = User::whereIn('id', $uids)->get();
                    foreach ($projects as $us) {
                        $proData[] = array(
                            'id' => $us->id,
                            'name' => $us->name,
                            'email' => $us->email,
                            'image' => $us->image,
                            'image_path' => get_users_image_link($us->id),
                            'status' => $us->status,
                            'mobile' => $us->mobile,
                        );
                    }
                }else {
                    $projectid = explode(',',$request->project_id);
                    $pms = ProjectMember::where('user_type', $request->user_type)->where('user_id', '<>', $user->id);
                    $uids = array();
                    if ($request->project_id) {
                        $pms->whereIn('project_id', $projectid);
                    }
                    $uids = $pms->get()->pluck('user_id');
                    $projects = User::whereIn('id', $uids)->get();
                    foreach ($projects as $us) {
                        $proData[] = array(
                            'id' => $us->id,
                            'name' => $us->name,
                            'email' => $us->email,
                            'image' => $us->image,
                            'image_path' => get_users_image_link($us->id),
                            'status' => $us->status,
                            'mobile' => $us->mobile,
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Members List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'get-members';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
//        return response()->json(['error' => 'Unauthorised'], 401);
    }

    public function employeesList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
    //            $projects = Contractors::where('company_id',$user->company_id)->get()->toArray();
                $pms = ProjectMember::where('company_id',$user->company_id)->where('user_type', 'employee');
                $uids = array();
                if($request->project_id) {
                    $pms->where('project_id', $request->project_id);
                }
                $uids = $pms->get()->pluck('user_id');
                $projects = User::whereIn('id', $uids)->get();
                $proData = array();
                foreach ($projects as $us) {
                    $proData[] = array(
                        'id' => $us->id,
                        'name' => $us->name,
                        'email' => $us->email,
                        'image' => $us->image,
                        'image_path' => get_users_image_link($us->id),
                        'status' => $us->status,
                        'mobile' => $us->mobile,
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Contractors List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'employee-list';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getClient(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = array();
                $us = User::findOrFail($request->id);
                $projects['client_detail'] = ClientDetails::where('user_id',$us->id)->first();
                if(!empty($us->id)){
                $projects['clients'] = array(
                        'id' => $us->id,
                        'name' => $us->name,
                        'email' => $us->email,
                        'image' => $us->image,
                        'image_path' => get_users_image_link($us->id),
                        'status' => $us->status,
                        'mobile' => $us->mobile,
                        'fcm' => $us->fcm,
                        'appid' => $us->appid,
                        'gender' => $us->gender,
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Clients List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'get-client';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getContractor(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $uData = array();
                $user = User::where('id',$request->id)->first();
                $uData['id'] = $user->id;
                $uData['company_id'] = $user->company_id;
                $uData['name'] = $user->name;
                $uData['email'] = $user->email;
                $uData['image_url'] = get_users_image_link($user->id);
                $uData['image'] = $user->image;
                $uData['mobile'] = $user->mobile;
                $uData['gender'] = $user->gender;
                $uData['status'] = $user->status;
                $response['status'] = 200;
                $response['message'] = 'Contractors List Fetched';
                $response['response'] = $uData;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'get-contractor';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getEmployee(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $uData = array();
                $user = User::where('id',$request->id)->first();
                $userAddress = EmployeeDetails::where('user_id',$request->id)->first();
                $uData['id'] = $user->id;
                $uData['company_id'] = $user->company_id;
                $uData['name'] = $user->name;
                $uData['email'] = $user->email;
                $uData['image_url'] = get_users_image_link($user->id);
                $uData['image'] = $user->image;
                $uData['mobile'] = $user->mobile;
                $uData['gender'] = $user->gender;
                $uData['status'] = $user->status;
                if($userAddress !== null) {
                    $uData['address'] = $userAddress->address;
                }else{
                    $uData['address'] = '';
                }
                $response['status'] = 200;
                $response['message'] = 'Employees List Fetched';
                $response['response'] = $uData;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'get-employee';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function employeeSearch(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $users = User::where('mobile','like','%'.$request->mobile.'%')->orWhere('name','like','%'.$request->mobile.'%')->orWhere('email','like','%'.$request->mobile.'%')->get();
                foreach($users as $searuser){
                    $added = 0;
                    if(isset($request->project_id)){
                        $data = ProjectMember::where('user_id', $searuser->id)->where('project_id', $request->project_id)->where('user_type', 'employee')->first();
                        if($data !== null){
                            $added = 1;
                        }
                    }
                    if($user->mobile!=$searuser->mobile||$user->email!=$searuser->email){
                        $proData[] = array(
                            'id' => $searuser->id,
                            'name' => $searuser->name,
                            'email' => $searuser->email,
                            'image' => $searuser->image,
                            'image_path' => get_users_image_link($searuser->id),
                            'status' => $searuser->status,
                            'mobile' => $searuser->mobile,
                            'added' => $added
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Employees List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'employee-search',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function clientSearch(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $users = User::where('mobile','like','%'.$request->mobile.'%')->orWhere('name','like','%'.$request->mobile.'%')->orWhere('email','like','%'.$request->mobile.'%')->get();
                foreach($users as $searuser){
                    $added = 0;
                    if(isset($request->project_id)){
                        $data = ProjectMember::where('user_id', $searuser->id)->where('project_id', $request->project_id)->where('user_type', 'client')->first();
                        if($data !== null){
                            $added = 1;
                        }
                    }
                    if($user->mobile!=$searuser->mobile||$user->email!=$searuser->email){
                        $proData[] = array(
                            'id' => $searuser->id,
                            'name' => $searuser->name,
                            'email' => $searuser->email,
                            'image' => $searuser->image,
                            'image_path' => get_users_image_link($searuser->id),
                            'status' => $searuser->status,
                            'mobile' => $searuser->mobile,
                            'added' => $added
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Clients List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {

                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'client-search',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function contractorSearch(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $users = User::where('mobile','like','%'.$request->mobile.'%')->orWhere('name','like','%'.$request->mobile.'%')->orWhere('email','like','%'.$request->mobile.'%')->get();
                foreach($users as $searuser){
                    $added = 0;
                    if(isset($request->project_id)){
                        $data = ProjectMember::where('user_id', $searuser->id)->where('project_id', $request->project_id)->where('user_type', 'contractor')->first();
                        if($data !== null){
                            $added = 1;
                        }
                    }
                    if($user->mobile!=$searuser->mobile||$user->email!=$searuser->email){
                        $proData[] = array(
                            'id' => $searuser->id,
                            'name' => $searuser->name,
                            'email' => $searuser->email,
                            'image' => $searuser->image,
                            'image_path' => get_users_image_link($searuser->id),
                            'status' => $searuser->status,
                            'mobile' => $searuser->mobile,
                            'added' => $added
                        );
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Employees List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'contractor-search',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function inviteMember(Request $request){
        if (!defined('API_ACCESS_KEY')) define('API_ACCESS_KEY','AAAAZs3kkrA:APA91bH1TsyEUhjgSzNs8-CBs6LCnoow0sZSxi6j7rv3Lvn6M7BKnh21sQ1M1Igp1oKjsxvAwHE3Sfk2AWsmS0Na3OtHOVnbKlCVgTxn64FnJJR4TMCT33pHpEXVL0J46qfXyKmkG24d');
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $usr = User::find($request->user_id);
                $iu = InvitedUser::where('user_id',$request->user_id)->first();
                $mobile = $request->mobile;
                if(!checkEmail($mobile)){
                    $mobile = str_replace(" ", "", $mobile);
                    $len = strlen($mobile);
                    $c = $len - 10;
                    if($c){
                        $mobile = substr($mobile, $c, 10);
                    }
                }
                if($mobile==$user->mobile||$mobile==$user->email){
                    $response['status'] = 300;
                    $response['message'] = 'Self invitation is not applicable!';
                    $response['response'] = '';
                    return $response;
                }
                if($usr !== null && $iu !== null){
                    $pn = Project::find($request->project_id);
                    if(!empty($pn->id)){

                        $pm = ProjectMember::where('user_id',$usr->id)->where('company_id',$user->company_id)->where('project_id',$pn->id)->first();
                        if(empty($pm->id)) {
                            $pm = new ProjectMember();
                            $pm->company_id = $user->company_id;
                            $pm->user_id = $usr->id;
                            $pm->project_id = $pn->id;
                        }
                        $pm->user_type = $request->user_type;
                        $pm->assigned_by = $user->id;
                        $pm->share_project = '1';
                        $pm->save();

                        $urole = Role::where('name', $request->user_type)->where('company_id', $user->company_id)->first();
                        if ($urole === null) {
                            $response['message'] = 'You do not have any role with name ' . $request->user_type;
                            $response['status'] = 300;
                            return $response;
                        }
                        $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                        //dd($perms);
                        ProjectPermission::where('project_id', $request->project_id)->where('user_id', $usr->id)->delete();
                        foreach ($perms as $perm) {
                            $pprm = new ProjectPermission();
                            $pprm->project_id = $request->project_id;
                            $pprm->user_id = $usr->id;
                            $pprm->permission_id = $perm;
                            $pprm->save();
                        }
                        $notifmessage = array();
                        $notifmessage['title'] = 'Project Assignment';
                        $notifmessage['body'] = 'You have been added to '.$pn->project_name.' project by '.$user->name;
                        $notifmessage['activity'] = 'projects';
                        sendFcmNotification($usr->id, $notifmessage);
                    }
                }elseif($usr !== null && $iu == null){
                    $inu = new InvitedUser();
                    $inu->company_id = $user->company_id;
                    $inu->invited_by = $user->id;
                    $inu->user_id = $usr->id;
                    $inu->mobile = $mobile;
                    $inu->save();

                    $pm = new ProjectMember();
                    $pm->company_id = $user->company_id;
                    $pm->user_id = $usr->id;
                    $pm->user_type = $request->user_type;
                    $pm->project_id = $request->project_id;
                    $pm->assigned_by = $user->id;
                    $pm->share_project = '0';
                    $pm->save();

                    $pn = Project::find($request->project_id);

                    /*$fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                    $token =$usr->fcm;
                    $note = [
                        'title' =>'Project Invitation',
                        'body' => 'You have been invited to '.$pn->project_name.' project by '.$user->name,
                        'activity' => 'projects'
                    ];
                    $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                    $fcmNotification = [
                        'to'        => $token, //single token
                        'notification' => $note
                    ];
                    $headers = [
                        'Authorization: key=' . API_ACCESS_KEY,
                        'Content-Type: application/json'
                    ];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                    curl_exec($ch);
                    curl_close($ch);*/
                    $notifmessage = array();
                    $notifmessage['title'] = 'Project Assignment';
                    $notifmessage['body'] = 'You have been added to '.$pn->project_name.' project by '.$user->name;
                        $notifmessage['activity'] = 'projects';
                        sendFcmNotification($usr->id, $notifmessage);
                }else{
                    $tc = new TempUser();
                    $tc->company_id = $user->company_id;
                    $tc->mobile = $mobile;
                    $tc->project_id = $request->project_id;
                    $tc->user_type = $request->user_type;
                    $tc->invited_by = $user->id;
                    $tc->save();

    //                $pn = Project::find($request->project_id);
    //                $username = $request->mobile;
    //                if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
    //                    $qty = \App\User::where('email', $username)->first();
    //                    $data = 'Your have been invited to '.$pn->project_name.' project by '.$user->name.'. Please download the application using below given link to join the project.';
    //                    if(isset($qty->id)){
    //                        $mailarray = array();
    //                        $mailarray['email'] = $qty->email;
    //                        $mailarray['subject'] = 'Invitation for the project - '.$pn->project_name;
    //                        $mailarray['message'] = $data;
    //                        $qty->sendEmail($mailarray);
    //                    }
    //                }else{
    //                    $msg = $data = 'Your have been invited to '.$pn->project_name.' project by '.$user->name.'. Please download the application using below given link to join the project.';
    //                    $sms = urlencode($msg);
    //                    $no = urlencode($username);
    //                    $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
    //                    $ch = curl_init();
    //                    curl_setopt($ch, CURLOPT_URL, $url);
    //                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //                    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    //                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //                    curl_exec($ch);
    //                    curl_error($ch);
    //                    curl_close($ch);
    //                }
                }
                $response['status'] = 200;
                $response['message'] = 'Member invitation sent successfully!';
                $response['response'] = '';
                return $response;
            } catch (\Exception $e) {

                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'invite-member',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteMember(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                ProjectMember::where('project_id',$request->project_id)->where('user_id',$request->user_id)->where('company_id',$user->company_id)->delete();
                ProjectPermission::where('project_id',$request->project_id)->where('user_id',$request->user_id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Member Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-member',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getRoles(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            $response = array();
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $roles = Role::where('company_id', $user->company_id)->where('name', '<>', 'admin')->get()->toArray();
                $response['status'] = 200;
                $response['response'] = $roles;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-roles',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getPermissionsByRole(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            $response = array();
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $role = Role::where('company_id', $user->company_id)->where('name', $request->role_name)->first();
                if($role === null){
                    $response['status'] = 300;
                    $response['response'] = 'Role not defined.';
                    return $response;
                }
                $permissions = PermissionRole::where('role_id', $role->id)->get();
                $permissionsx = array();
                if(count($permissions) == 0){
                    $rolex = Role::where('company_id', 1)->where('name', $request->role_name)->first();
                    if($rolex === null){
                        $response['status'] = 300;
                        $response['response'] = 'Role not defined.';
                        return $response;
                    }
                    $permissionsx = PermissionRole::where('role_id', $rolex->id)->get();
                    foreach ($permissionsx as $permission){
                        $prevpermission = PermissionRole::where('role_id',$rolex->id)->where('permission_id',$permission->permission_id)->first();
                        if(empty($prevpermission->id)){
                            $pr = new PermissionRole();
                            $pr->permission_id = $permission->permission_id;
                            $pr->role_id = $rolex->id;
                            $pr->save();
                        }
                    }
                    $response['status'] = 200;
                    $response['response'] = $permissionsx;
                    return $response;
                }
                $response['status'] = 200;
                $response['response'] = $permissions;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-permissions-by-role',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getUserProfile(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $uData = array();
                $user = User::where('id',$request->user_id)->first();
                $com = Company::where('id',$user->company_id)->first();

                $uData['id'] = $user->id;
                $uData['company_id'] = $user->company_id;
                $uData['name'] = $user->name;
                $uData['email'] = $user->email;
                $uData['image'] = $user->image;
                $uData['image_url'] = get_users_image_link($user->id);
                $uData['mobile'] = $user->mobile;
                $uData['gender'] = $user->gender;
                $uData['status'] = $user->status;
                $uData['address'] = $user->address;
                $uData['company_name'] = $com->company_name;
                $uData['company_email'] = $com->company_email;
                $uData['company_phone'] = $com->company_phone;
                $uData['logo'] = asset('uploads/app-logo/'.$com->logo);
                $uData['company_address'] = $com->address;
                $uData['website'] = $com->website;
                $response['status'] = 200;
                $response['message'] = 'Employees List Fetched';
                $response['response'] = $uData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-user-profile',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateUserProfile(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/avatar/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $user->image= $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('avatar/', $request->image, $image->hashName(), 'public');
                            $user->image= $image->hashName();
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());
                            $user->image= Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->image= $dropboxResult['url'];
                            break;
                    }
    //                $u->image= $request->image->getClientOriginalName();
                }
                $user->name = $request->name;
                $user->gender = $request->gender;
                $user->address = $request->address;
                $user->save();

                $com = Company::find($user->company_id);
                if($com === null){
                    $com = new Company();
                    $com->package_id = '1';
                }
                if ($request->hasFile('logo')) {
                    $storage = storage();
                    $image = $request->logo;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/avatar/';
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            $com->logo = $image->hashName();
                            break;
                        case 's3':
                            $st = Storage::disk('s3')->putFileAs('avatar/', $image, $image->hashName(), 'public');
                            $com->logo = $image->hashName();
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('logo', '=', 'avatar')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->logo)
                                ->first();
                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->logo)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $request->logo, $request->logo->getClientOriginalName());
                            $com->logo= Storage::cloud()->url($directory['path'].'/'.$request->logo->getClientOriginalName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->logo, $request->logo->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->logo->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $com->logo= $dropboxResult['url'];
                            break;
                    }
                }
                $com->company_name = isset($request->company_name) ? ($request->company_name != null ? $request->company_name : $user->name) : $user->name;
                $com->company_email = isset($request->company_email) ? ($request->company_email != null ? $request->company_email : $user->email) : $user->email;
                $com->company_phone = isset($request->company_phone) ? ($request->company_phone != null ? $request->company_phone : $user->mobile) : $user->mobile;
                $com->address = isset($request->company_address) ? ($request->company_address != null ? $request->company_address : $user->address) : $user->address;
                $com->website = isset($request->website) ? ($request->website != null ? $request->website : null) : null;
                $com->save();
                $user->company_id = $com->id;
                $user->save();
                $response['status'] = 200;
                $response['message'] = 'User Profile Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-user-profile',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function savePermission(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $employee = Employee::find($request->employee_id);
                $user = User::withoutGlobalScope('active')->where('id', $employee->user_id)->first();
                if($user === null){
                    $response['message'] = 'User Not Found';
                    $response['status'] = 300;
                    return $response;
                }
                $projectid = $request->project_id;
                $projectsdetails = Project::find($projectid);
                if(!empty($projectsdetails->id)){
                    if(!empty($employee->id)){
                        $sharetoproject = $request->sharetoproject;
                        $checkpermission = $request->checkpermission;
                        $sendnotification = $request->sendnotification;
                        $user_type = $employee->user_type;
                      /*  $level = 0;
                        if($projectsdetails->added_by==$user->id){
                            $level = 1;
                        }else{
                            $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectsdetails->id)->first();
                            $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                            $level = $prolevel+1;
                        }*/
                        $projectmember = ProjectMember::where('project_id',$projectsdetails->id)->where('employee_id',$employee->id)->where('user_id',$employee->user_id)->where('company_id',$employee->company_id)->first();
                        if(empty($projectmember)){
                            $projectmember = new ProjectMember();
                            $projectmember->project_id = $projectid;
                            $projectmember->user_id = $employee->user_id;
                            $projectmember->employee_id = $employee->id;
                            $projectmember->company_id = $employee->company_id;
                            $projectmember->assigned_by = $user->id;
                            $projectmember->user_type = $employee->user_type;
                        }
                        $projectmember->share_project = 0;
                        if($sharetoproject=='1'){
                            $projectmember->share_project = '1';
                        }
                        $projectmember->check_permission = 0;
                        if($checkpermission=='1'){
                            $projectmember->check_permission = '1';
                        }
                        if($employee->added_by==$user->id){
                            $level = $projectmember->level;
                            $prevproject = ProjectMember::where('user_id',$employee->added_by)->where('project_id',$projectsdetails->id)->first();
                            $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                            $level = $prolevel+1;
                            $projectmember->level = $level;
                        }
                        $projectmember->save();
                        if($sendnotification=='1'){
                            $usersdata = User::where('id',$employee->user_id)->first();
                            if($sharetoproject=='1'&&!empty($usersdata->fcm)) {
                                $notifmessage = array();
                                $notifmessage['title'] = 'Project Shared';
                                $notifmessage['body'] = 'You have been added to ' . $projectsdetails->project_name . ' project by ' . $user->name;
                                $notifmessage['activity'] = 'projects';
                                sendFcmNotification($usersdata->id, $notifmessage);
                            }
                        }
                    }
                    /*
                    * Unchecked roles from permission list
                    * uncheck permission comment on guidance for vikram sir 15-5-2021
                 * */

                   /* if(!empty($request->unroles)){
                        $permissions = explode(',',str_replace(']', '', str_replace('[', '', $request->unroles)));
                        $permissions = array_filter(array_unique($permissions));
                        $userprojectmember = ProjectMember::where('project_id',$projectsdetails->id)->where('user_id',$user->id)->first();
                        $level = !empty($userprojectmember) ? $userprojectmember->level : 0;
                        $x=1; $start = 0;
                        $levelusers = array();
                        while($x==1){
                            $level = $level+1;
                            if($start==1){
                                $levelmembersarray = ProjectMember::where('project_id',$projectsdetails->id)->where('level',$level)->whereIn('assigned_by',$levelusers)->pluck('user_id')->toArray();
                            }else{
                                $levelmembersarray = ProjectMember::where('project_id',$projectsdetails->id)->where('level',$level)->where('assigned_by',$user->id)->where('user_id',$member->id)->pluck('user_id')->toArray();
                            }
                            if(!empty($levelmembersarray)){
                                foreach ($levelmembersarray as $levelmembers){
                                    ProjectPermission::where('user_id', $levelmembers)->where('project_id', $projectsdetails->id)->whereIn('permission_id', $permissions)->delete();
                                    $levelusers[] = $levelmembers;
                                }
                                $start = 1;
                            }else{
                                $x = 0;
                            }
                        }
                    }*/
                    $member = User::withoutGlobalScope('active')->find($request->user_id);
                    if(!empty($member)){
                        ProjectPermission::where('user_id', $member->id)->where('project_id', $projectsdetails->id)->delete();
                    }
                    if(!empty($request->role_id)){
                        $selectedroles = !empty($request->role_id) ? explode(',',$request->role_id) : array();
                        if(!empty($selectedroles)){
                            foreach ($selectedroles as $selectedrole){
                                $permissions = explode(',',str_replace(']', '', str_replace('[', '', $request->roles)));

                                if(!empty($member)){
                                    $permissions = array_filter(array_unique($permissions));
                                    foreach ($permissions as $key=>$permission){
                                        $p = new ProjectPermission();
                                        $p->company_id = $projectsdetails->company_id;
                                        $p->project_id = $projectsdetails->id;
                                        $p->user_id = $member->id;
                                        $p->permission_id = $permission;
                                        $p->role_id = $selectedrole;
                                        $p->save();
                                    }
                                }
                            }
                        }
                    }else{
                        $permissions = explode(',',str_replace(']', '', str_replace('[', '', $request->roles)));

                        if(!empty($member)){
                            $permissions = array_filter(array_unique($permissions));
                                foreach ($permissions as $key=>$permission){
                                    $p = new ProjectPermission();
                                    $p->company_id = $projectsdetails->company_id;
                                    $p->project_id = $projectsdetails->id;
                                    $p->user_id = $member->id;
                                    $p->permission_id = $permission;
                                    $p->save();
                                }
                        }
                    }

                    $conversation = Conversation::where('moduleid',$projectsdetails->id)->where('module','project')->first();
                    if(empty($conversation)){
                        $conversation = new Conversation();
                        $conversation->moduleid = $projectsdetails->id;
                        $conversation->module = 'project';
                        $conversation->lastupdated = date('Y-m-d H:i:s');
                    }
                    $projectmembers = ProjectMember::where('project_id',$projectsdetails->id)->where('share_project','1')->pluck('user_id')->toArray();
                    $projectmembers[] = $user->id;
                    $projectmembers = array_filter(array_unique($projectmembers));
                    $conversation->users = !empty($projectmembers) ? implode(',',$projectmembers) : '';
                    $conversation->save();

                    $user_type = $employee->user_type;
                    if($user_type!=='admin'){
                        $usertype = ucwords($user_type);
                        $incometype = IncomeExpenseGroup::where('name',$usertype)->where('company_id',$employee->company_id)->first();
                        $incomearray = IncomeExpenseHead::where('company_id',$projectsdetails->company_id)->where('income_expense_group_id',$incometype->id)->where('emp_id',$employee->id)->first();
                        if(empty($incomearray)){
                            $incomearray = new IncomeExpenseHead();
                            $incomearray->company_id = $projectsdetails->company_id;
                            $incomearray->created_by = $user->id;
                            $incomearray->emp_id = $employee->id;
                        }
                            $incomearray->name = $employee->name;
                            $incomearray->project_id = !empty($projectsdetails->id) ? $projectsdetails->id : 0;
                            $incomearray->income_expense_group_id = $incometype->id;
                            $incomearray->income_expense_type_id = $incometype->income_type;
                            $incomearray->save();
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Permissions updated successfully.';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'save-permission',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function savePermissionsByRole(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $rolearray = !empty($request->role_id) ? explode(',',$request->role_id) : array();
                if(!empty($rolearray)){
                    foreach ($rolearray as $role_id){
                        $permissions = explode(',',str_replace(']', '', str_replace('[', '', $request->permissions_id)));
                        PermissionRole::where('role_id', $role_id)->delete();
                        foreach ($permissions as $key=>$permission){
                            $perm = new PermissionRole();
                            $perm->permission_id = $permission;
                            $perm->role_id = $role_id;
                            $perm->save();
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'success';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'save-permissions-by-role',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getProjectsPermissionsRoles(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $userid = $request->user_id;
                $projectid = $request->project_id;
                $projects = Project::find($projectid);
                if(!empty($projects->id)){
                    $projectpermissions = ProjectPermission::where('user_id',$userid)->where('project_id',$projects->id)->where('role_id','<>','0')->pluck('role_id')->toArray();
                    $response['status'] = 200;
                    $response['message'] = 'success';
                    $response['roles'] = $projectpermissions;
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Project not found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'save-permissions-by-role',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function removeMember(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                if($request->project_id == '0' || $request->project_id == 0){
                    $pros = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
                    $pids = array();
                    $perm = 'delete_employees';
                    if($request->user_type == 'client'){
                        $perm = 'delete_clients';
                    }
                    if($request->user_type == 'contractor'){
                        $perm = 'delete_contractors';
                    }
                    foreach ($pros as $pro){
                        $pem = Permission::where('name',$perm)->first();
                        $havePerm = ProjectPermission::where('project_id', $pro)->where('user_id', $user->id)->where('permission_id', $pem->id)->first();
                        if($havePerm !== null){
                            array_push($pids, $pro);
                        }
                    }
                    if(count($pids)) {
                        ProjectMember::whereIn('project_id', $pids)->where('user_type', $request->user_type)->where('user_id', $request->user_id)->delete();
                        ProjectPermission::whereIn('project_id', $pids)->where('user_id', $request->user_id)->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Member Removed Successfully';
                        return $response;
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'Permission denied!';
                        return $response;
                    }
                }else{
                    $perm = 'delete_employees';
                    if($request->user_type == 'client'){
                        $perm = 'delete_clients';
                    }
                    if($request->user_type == 'contractor'){
                        $perm = 'delete_contractors';
                    }
                    $pem = Permission::where('name',$perm)->first();
                    $havePerm = ProjectPermission::where('project_id', $request->project_id)->where('user_id', $user->id)->where('permission_id', $pem->id)->first();
                    if($havePerm !== null){
                        ProjectMember::where('project_id',$request->project_id)->where('user_type', $request->user_type)->where('user_id', $request->user_id)->delete();
                        ProjectPermission::where('project_id',$request->project_id)->where('user_id', $request->user_id)->delete();
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'You don’t have permission to perform this action.';
                        return $response;
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Member Removed Successfully';
                    return $response;
                }
                $response['status'] = 300;
                $response['message'] = 'Something went wrong. Please try again!';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'remove-member';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function sendNotification(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pn = Project::find($request->project_id);
                $notifmessage = array();
                $notifmessage['title'] = 'Project Assignment';
                $notifmessage['body'] = 'You have been added to '.$pn->project_name.' project by '.$user->name;
                $notifmessage['activity'] = 'projects';
                sendFcmNotification($request->fcm, $notifmessage);
                $response['status'] = 200;
                $response['message'] = 'Notification Sent!';
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'remove-member';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function addNoninvitedMember(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $usr = User::find($request->user_id);
                $iu = InvitedUser::where('user_id',$request->user_id)->first();
                $mobile = $request->mobile;
                if($mobile==$user->mobile||$mobile==$user->email){
                    $response['status'] = 300;
                    $response['message'] = 'Self invitation is not applicable!';
                    $response['response'] = '';
                    return $response;
                }
                if(!checkEmail($mobile)){
                    $mobile = str_replace(" ", "", $mobile);
                    $len = strlen($mobile);
                    $c = $len - 10;
                    if($c){
                        $mobile = substr($mobile, $c, 10);
                    }
                }
                if($usr !== null && $iu !== null){
                    $pm = new ProjectMember();
                    $pm->company_id = $user->company_id;
                    $pm->user_id = $usr->id;
                    $pm->project_id = $request->project_id;
                    $pm->user_type = $request->user_type;
                    $pm->assigned_by = $user->id;
                    $pm->save();
                }elseif($usr !== null && $iu == null){
                    $inu = new InvitedUser();
                    $inu->company_id = $user->company_id;
                    $inu->invited_by = $user->id;
                    $inu->user_id = $usr->id;
                    $inu->mobile = $mobile;
                    $inu->save();

                    $pm = new ProjectMember();
                    $pm->company_id = $user->company_id;
                    $pm->user_id = $usr->id;
                    $pm->user_type = $request->user_type;
                    $pm->project_id = $request->project_id;
                    $pm->assigned_by = $user->id;
                    $pm->save();
                }else{
                    $tc = new TempUser();
                    $tc->company_id = $user->company_id;
                    $tc->mobile = $mobile;
                    $tc->project_id = $request->project_id;
                    $tc->user_type = $request->user_type;
                    $tc->invited_by = $user->id;
                    $tc->save();
                }
                $response['status'] = 200;
                $response['message'] = 'Member invitation sent successfully!';
                $response['response'] = '';
                return $response;
            } catch (\Exception $e) {

                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'invite-member',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}