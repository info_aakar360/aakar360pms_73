<?php

namespace App\Http\Controllers\Api;

use App\Channels;
use App\ChannelsMembers;
use App\Company;
use App\Employee;
use App\Http\Controllers\Controller;
use App\ProjectMember;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use DB;

class AppChannelController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            if(!empty($request->permission_name)){
                $permissionname = $request->permission_name;
                $projectid = $request->permission_project_id;
                $checkpermission = checkPermission($user->id,$projectid,$permissionname);
                if(!empty($checkpermission['message'])&&$checkpermission['message']!='true'){
                    echo json_encode($checkpermission);
                    exit();
                }
            }
        }
    }

    public function appChannels(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $companyid = $user->company_id;
                $channels = Channels::where('company_id',$companyid);
                if(!empty($projectid)){
                    $channels = $channels->where('project_id',$projectid);
                }
                $page = $request->page;
                if($page=='all'){
                    $channels = $channels->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $channels = $channels->offset($skip)->take($count)->get();
                }
                $response['status'] = 200;
                $response['message'] = 'Channels List Fetched';
                $response['response'] = $channels;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'channel-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createChannel(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $name = $request->name;
                $channel = new Channels();
                $channel->company_id = $user->company_id;
                $channel->added_by = $user->id;
                $channel->project_id = $projectid;
                $channel->name = $name;
                $channel->save();
                 if ($request->hasFile('image')) {
                    $storage = storage();
                    $url = url('/');
                    $awsurl = awsurl();
                     $url = uploads_url();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/channels/' . $channel->id;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('channels/'.$channel->id, $request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('channels/'.'/', $request->image, $request->image->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                     $channel->image= $request->image->hashName();
                }
                $channel->save();
                $response['status'] = 200;
                $response['message'] = 'Channel Added Successfully';
                $response['channelid'] = $channel->id;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-channel',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function updateChannel(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $channelid = $request->channel_id;
                $projectid = $request->project_id;
                $name = $request->name;
                $channel =  Channels::find($channelid);
                $channel->company_id = $user->company_id;
                $channel->added_by = $user->id;
                $channel->project_id = $projectid;
                $channel->name = $name;
                 if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image->hashName();
                    switch($storage) {
                        case 'local':
                            $extension = $image->getClientOriginalExtension();
                            $destinationPath = 'uploads/channels/' . $channel->id;
                            if (!file_exists($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                            }
                            if($extension=='png'||$extension=='jpg'||$extension=='jpeg') {
                                $img = Image::make($image->getRealPath());
                                $img->save($destinationPath . '/' . $image->hashName());
                            }else{
                                $image->storeAs($destinationPath, $image->hashName());
                            }
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('channels/'.$channel->id,$request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->hashName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('channels/'.'/', $request->image, $request->image->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                     $channel->image= $request->image->hashName();
                }
                $channel->save();
                $response['status'] = 200;
                $response['message'] = 'Channel Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-channel',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteChannel(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->channel_id;
                $channel =  Channels::where('id',$id)->first();
                if(empty($channel->id)){
                    if($channel->added_by==$user->id){
                        $channel->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Channel deleted Successfully';
                        return $response;
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'You do not permission to delete channel';
                        return $response;
                    }
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Channel Not found';
                    return $response;
                }

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-channel',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function channelUsersList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData =  $usersdata =  $backofficedata = array();
                $channelid = $request->channel_id;
                $page = $request->page;

                $channels = Channels::find($channelid);
                if(!empty($channels->id)){
                    $pm = ProjectMember::where('project_id',$channels->project_id)->where('share_project','1')->get()->pluck('user_id')->toArray();
    //                $users = User::whereIn('id', $pm)->where('company_id',$user->company_id)->get();
                    if($page=='all'){
                        $users = User::whereIn('id', $pm)->get();
                    }else{
                        $count = pagecount();
                        $skip = 0;
                        if($page){
                            $skip = $page*$count;
                        }
                        $users = User::whereIn('id', $pm)->offset($skip)->take($count)->get();
                    }
                    foreach ($users as $user) {
                        $usersdata[] = array(
                            'id' => $user->id,
                            'name' => $user->name,
                            'email' => $user->email,
                            'image' => $user->image,
                            'image_path' => get_users_image_link($user->id),
                            'status' => $user->status,
                            'mobile' => $user->mobile,
                        );
                    }
                }
                $employeelist = Employee::where('company_id', $channels->company_id)->get()->pluck('user_id')->toArray();
                if($page=='all'){
                    $usersarray = User::whereIn('id', $employeelist)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $usersarray = User::whereIn('id', $employeelist)->offset($skip)->take($count)->get();
                }
                foreach ($usersarray as $user) {
                    $backofficedata[] = array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'image' => $user->image,
                        'image_path' => get_employee_image_link($user->id),
                        'status' => $user->status,
                        'mobile' => $user->mobile,
                    );
                }
                $proData = array('userslist'=>$usersdata,'backofficelist'=>$backofficedata);
                $response['status'] = 200;
                $response['message'] = 'Users List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'channel-users-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function addChannelMembers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $channelid = $request->channel_id;
                $role = $request->role ?: 'user';
                $memberarray = $request->member_id;
                if(!empty($memberarray)){
                    foreach ($memberarray as $memberid){
                        $channelmember = ChannelsMembers::where('channel_id',$channelid)->where('member_id',$memberid)->first();
                        if(empty($channelmember->id)){
                            $channelmem = new ChannelsMembers();
                            $channelmem->company_id = $user->company_id;
                            $channelmem->added_by = $user->id;
                            $channelmem->channel_id = $channelid;
                            $channelmem->member_id = $memberid;
                            $channelmem->role = $role;
                            $channelmem->save();
                        }
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Members added to Channels';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'add-channel-members',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function removeChannelMembers(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $channelid = $request->channel_id;
                $role = $request->role ?: 'user';
                $memberid = $request->member_id;
                $channelmember = ChannelsMembers::where('channel_id',$channelid)->where('member_id',$memberid)->first();
                if(!empty($channelmember->id)){
                    $channelmember->delete();
                    $response['status'] = 200;
                    $response['message'] = 'Member added to Channels';
                    return $response;
                }else{
                $response['status'] = 300;
                $response['message'] = 'Member not found';
                return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'remove-channel-members',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function setChannelAdmin(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $channelid = $request->channel_id;
                $role = $request->role ?: 'user';
                $memberid = $request->member_id;
                $channelmember = ChannelsMembers::where('channel_id',$channelid)->where('member_id',$memberid)->first();
                if(!empty($channelmember->id)){
                    $channelmember->role = $role;
                    $channelmember->save();
                    $response['status'] = 200;
                    $response['message'] = 'Member Updated';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Member not found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'set-channel-admin',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}