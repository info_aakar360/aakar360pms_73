<?php

namespace App\Http\Controllers;

use App\Promotional_fcm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PromotionalFcmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promotional_fcm  $promotional_fcm
     * @return \Illuminate\Http\Response
     */
    public function show(Promotional_fcm $promotional_fcm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promotional_fcm  $promotional_fcm
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotional_fcm $promotional_fcm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promotional_fcm  $promotional_fcm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotional_fcm $promotional_fcm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promotional_fcm  $promotional_fcm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotional_fcm $promotional_fcm)
    {
        //
    }
}
