<?php

namespace App\Http\Controllers\Front;

use App\Company;
use App\CreditNotes;
use App\Feature;
use App\FooterMenu;
use App\GlobalSetting;
use App\Helper\Reply;
use App\Http\Requests\Front\ContactUs\ContactUsRequest;
use App\Invoice;
use App\InvoiceItems;
use App\Notifications\ContactUsMail;
use App\Package;
use App\PoProducts;
use App\ProductBrand;
use App\ProductCategory;
use App\Project;
use App\PurchaseOrder;
use App\QuoteProducts;
use App\Quotes;
use App\Rfq;
use App\RfqProducts;
use App\Setting;
use App\Store;
use App\Supplier;
use App\Task;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersContracts;
use App\TendersFiles;
use App\Units;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Notification;
use App\Module;
use App\InvoiceSetting;
use App\OfflinePaymentMethod;
use App\PaymentGatewayCredentials;
use View;


class HomeController extends FrontBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {

        $this->pageTitle = 'Home';
        $this->packages = Package::where('default', 'no')->get();

        $features = Feature::all();
        $this->featureWithImages = $features->filter(function ($value, $key) {
            return $value->type == 'image';
        });

        $this->featureWithIcons = $features->filter(function ($value, $key) {
            return $value->type == 'icon';
        });
        $this->packageFeatures = Module::get()->pluck('module_name')->toArray();

        if($slug){
            $this->slugData = FooterMenu::where('slug', $slug)->first();
            return view('front.footer-page', $this->data);
        }

        return view('front.home', $this->data);
    }

    public function page($slug = null)
    {
        $this->slugData = FooterMenu::where('slug', $slug)->first();
        $this->pageTitle = 'Page-'.$this->slugData->name;

        return view('front.footer-page', $this->data);
    }

    public function contactUs(ContactUsRequest $request) {

        $this->pageTitle = 'Contact Us';
        $superAdmins = User::where('super_admin', '1')->get();

        $this->table = '<table><tbody style="color:#0000009c;">
        <tr>
            <td><p>Name : </p></td>
            <td><p>'.ucwords($request->name).'</p></td>
        </tr>
        <tr>
            <td style="font-family: Avenir, Helvetica, sans-serif;box-sizing: border-box;min-width: 98px;vertical-align: super;"><p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Message : </p></td>
            <td><p>'.$request->message.'</p></td>
        </tr>
</tbody>
        
</table><br>';

        Notification::route('mail', $this->detail->email)
            ->notify(new ContactUsMail($this->data, $request->email));


        return Reply::success('Thanks for contacting us. We will catch you soon.');
    }

    public function invoice($id)
    {
        $this->pageTitle = __('app.menu.clients');
        $this->pageIcon = 'icon-people';

        $this->invoice = Invoice::whereRaw('md5(id) = ?', $id)->with('payment')->firstOrFail();
//        dd($this->invoice);
        $this->paidAmount = $this->invoice->getPaidAmount();

        if($this->invoice->discount > 0){
            if($this->invoice->discount_type == 'percent'){
                $this->discount = (($this->invoice->discount/100)*$this->invoice->sub_total);
            }
            else{
                $this->discount = $this->invoice->discount;
            }
        }
        else{
            $this->discount = 0;
        }

        $taxList = array();

        $items = InvoiceItems::whereNotNull('taxes')
            ->where('invoice_id', $this->invoice->id)
            ->get();

        foreach ($items as $item) {
            foreach (json_decode($item->taxes) as $tax){
                $this->tax = InvoiceItems::taxbyid($tax)->first();
                if ($this->tax){
                    if (!isset($taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'])) {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = ($this->tax->rate_percent / 100) * $item->amount;
                    } else {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] + (($this->tax->rate_percent / 100) * $item->amount);
                    }
                }
            }
        }

        $this->taxes = $taxList;

        $this->settings = Company::findOrFail($this->invoice->company_id);
        $this->credentials = PaymentGatewayCredentials::first();
        $this->methods = OfflinePaymentMethod::activeMethod();
        $this->invoiceSetting = InvoiceSetting::first();

        return view('invoice', [
            'companyName' => $this->settings->company_name,
            'pageTitle' => $this->pageTitle,
            'pageIcon' => $this->pageIcon,
            'global' => $this->settings,
            'setting' => $this->settings,
            'settings' => $this->settings,
            'invoice' => $this->invoice,
            'paidAmount' => $this->paidAmount,
            'discount' => $this->discount,
            'credentials' => $this->credentials,
            'taxes' => $this->taxes,
            'methods' => $this->methods,
            'invoiceSetting' => $this->invoiceSetting,
        ]);
    }

    public function domPdfObjectForDownload($id)
    {
        $this->invoice = Invoice::whereRaw('md5(id) = ?', $id)->firstOrFail();
        $this->paidAmount = $this->invoice->getPaidAmount();
        $this->creditNote = 0;
        if ($this->invoice->credit_note) {
            $this->creditNote = CreditNotes::where('invoice_id', $id)
                ->select('cn_number')
                ->first();
        }

        if ($this->invoice->discount > 0) {
            if ($this->invoice->discount_type == 'percent') {
                $this->discount = (($this->invoice->discount / 100) * $this->invoice->sub_total);
            } else {
                $this->discount = $this->invoice->discount;
            }
        } else {
            $this->discount = 0;
        }

        $taxList = array();

        $items = InvoiceItems::whereNotNull('taxes')
            ->where('invoice_id', $this->invoice->id)
            ->get();

        foreach ($items as $item) {
            foreach (json_decode($item->taxes) as $tax){
                $this->tax = InvoiceItems::taxbyid($tax)->first();
                if ($this->tax){
                    if (!isset($taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'])) {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = ($this->tax->rate_percent / 100) * $item->amount;
                    } else {
                        $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] = $taxList[$this->tax->tax_name . ': ' . $this->tax->rate_percent . '%'] + (($this->tax->rate_percent / 100) * $item->amount);
                    }
                }
            }
        }

        $this->taxes = $taxList;

        $this->settings = $this->global;

        $this->invoiceSetting = InvoiceSetting::first();
        //        return view('invoices.'.$this->invoiceSetting->template, $this->data);

        $pdf = app('dompdf.wrapper');
        $pdf->loadView('invoices.' . $this->invoiceSetting->template, $this->data);
        $filename = $this->invoice->invoice_number;

        return [
            'pdf' => $pdf,
            'fileName' => $filename
        ];
    }

    public function downloadInvoice($id)
    {

        $this->invoice = Invoice::whereRaw('md5(id) = ?', $id)->firstOrFail();

        // Download file uploaded
        if ($this->invoice->file != null) {
            return response()->download(storage_path('app/invoice-files') . '/' . $this->invoice->file);
        }

        $pdfOption = $this->domPdfObjectForDownload($id);
        $pdf = $pdfOption['pdf'];
        $filename = $pdfOption['fileName'];

        return $pdf->download($filename . '.pdf');
    }

    public function app(){
        return ['data' => GlobalSetting::select('id','company_name')->first()];
    }

    public function gantt($ganttProjectId)
    {
        $this->project = Project::whereRaw('md5(id) = ?', $ganttProjectId)->firstOrFail();
        $this->settings = Setting::findOrFail($this->project->company_id);
        $this->ganttProjectId = $ganttProjectId;

        return view('gantt', [
            'ganttProjectId' => $this->ganttProjectId,
            'global' => $this->settings,
            'project' => $this->project
        ]);
    }

    public function ganttData($ganttProjectId)
    {

        $data = array();
        $links = array();

        $projects = Project::select('id', 'project_name', 'start_date', 'deadline', 'completion_percent')
            ->whereRaw('md5(id) = ?', $ganttProjectId)
            ->get();

        $id = 0; //count for gantt ids
        foreach ($projects as $project) {
            $id = $id + 1;
            $projectId = $id;

            // TODO::ProjectDeadline to do
            $projectDuration = 0;
            if ($project->deadline) {
                $projectDuration = $project->deadline->diffInDays($project->start_date);
            }

            $data[] = [
                'id' => $projectId,
                'text' => ucwords($project->project_name),
                'start_date' => $project->start_date->format('Y-m-d H:i:s'),
                'duration' => $projectDuration,
                'progress' => $project->completion_percent/100,
                'project_id' => $project->id
            ];

            $tasks = Task::projectOpenTasks($project->id);

            foreach ($tasks as $key => $task) {
                $id = $id + 1;

                $taskDuration = $task->due_date->diffInDays($task->start_date);
                $taskDuration = $taskDuration +1;

                $data[] = [
                    'id' => $task->id,
                    'text' => ucfirst($task->heading),
                    'start_date' => (!is_null($task->start_date)) ? $task->start_date->format('Y-m-d'): $task->due_date->format('Y-m-d'),
                    'duration' => $taskDuration,
                    'parent' => $projectId,
                    'users' => [
                        ucwords($task->user->name)
                    ],
                    'taskid' => $task->id
                ];

                $links[] = [
                    'id' => $id,
                    'source' => $task->dependent_task_id != '' ? $task->dependent_task_id : $projectId,
                    'target' => $task->id,
                    'type' => $task->dependent_task_id != '' ? 0 : 1
                ];
            }
        }

        $ganttData = [
            'data' => $data,
            'links' => $links
        ];

        return response()->json($ganttData);
    }

    public function changeLanguage($lang)
    {
        $cookie = Cookie::forever('language', $lang);
        return redirect()->back()->withCookie($cookie);
    }

    public function submitQuotation($rfqId, $supplierId){
        $this->stores = Store::all();
        $this->products = ProductCategory::all();
        $this->brands = ProductBrand::all();
        $this->units = Units::all();
        $this->pageTitle = 'Submit Quotation';
        $this->pageIcon = 'icon-people';
        $all_data = RfqProducts::where('rfq_id', $rfqId)->get();
        $this->tmpData = $all_data;
        $this->rfq = Rfq::where('id', $rfqId)->first();
        $this->supplier = Supplier::find($supplierId);
        $this->submitted = false;
        $quote = Quotes::where('rfq_id', $rfqId)->where('supplier_id', $supplierId)->first();
        if($quote !== null){
            $this->submitted = true;
        }
        return view('member.rfq.submit-quote', $this->data);
    }
    public function viewPO($poId, $supplierId = 0){
        $this->pageTitle = 'View Purchase Order';
        $this->pageIcon = 'icon-people';
        $all_data = PoProducts::where('po_id', $poId)->get();
        $this->tmpData = $all_data;
        $this->po = PurchaseOrder::where('id', $poId)->first();
        $this->supplier = false;
        if($supplierId) {
            $this->supplier = Supplier::find($supplierId);
        }
        $rfq = Rfq::find($this->po->rfq_id);
        $this->store = Store::find($rfq->store_id);
        $this->submitted = false;

        return view('member.po.view-po', $this->data);
    }

    public function postQuotation($rfqId, $supplierId, Request $request){

        $rfCom = Rfq::where('id',$rfqId)->first();
        $quote = new Quotes();
        $quote->rfq_id = $rfqId;
        $quote->supplier_id = $supplierId;
        $quote->remark = $request->remark;
        $quote->project_id = $request->project_id;
        $quote->indent_id = $request->indent_id;
        $quote->payment_terms = $request->payment_terms;
        $quote->company_id = $rfCom->company_id;
        $quote->save();

        foreach($request->cid as $key=>$cid) {
            $qp = new QuoteProducts();
            $qp->quote_id = $quote->id;
            $qp->indent_id = $quote->indent_id;
            $qp->cid = $cid;
            $qp->bid = (!empty($request->bid[$key])) ? $request->bid[$key] : '';
            $qp->quantity = (!empty($request->quantity[$key])) ? $request->quantity[$key] : '';
            $qp->unit = (!empty($request->unit[$key])) ? $request->unit[$key] : '';
            $qp->expected_date = (!empty($request->dated[$key])) ? $request->dated[$key] : '';
            $qp->remarks = (!empty($request->remarks[$key])) ? $request->remarks[$key] : '';
            $qp->price = (!empty($request->price[$key])) ? $request->price[$key] : '';
            $qp->tax = (!empty($request->tax[$key])) ? $request->tax[$key] : '';
            $qp->save();
        }
        return Reply::redirect(route('front.rfq.success'));
    }

    public function rfqSuccess(){
        $this->pageTitle = 'Success';
        $this->pageIcon = 'icon-tick';
        return view('member.rfq.success', $this->data);
    }

    public function privacyPolicy(){
        return view('front.privacy-policy');
    }
    public function tenderContract($id,$uniqueid){

        $this->pageTitle = 'Bidding Contract';
        $this->pageIcon = 'icon-people';
        $tendercontract = TendersContracts::where('id',$id)->where('unique_id',$uniqueid)->first();
        $tenders = Tenders::find($tendercontract->tender_id);
        $tenderbidding = TenderBidding::where('user_id',$tendercontract->user_id)->where("tender_id",$tenders->id)->first();
        $this->tendercontract = $tendercontract;
        $this->tenderbidding = $tenderbidding;
        $this->tenders = $tenders;
        $this->storage = storage();
        $this->files = TendersFiles::where('tender_id',$tenders->id)->get();
        return view('front.tenders.bidding-list', $this->data);
    }
    public function tenderBiddingLoop(Request $request)
    {
        $id = $request->tendercontract;
        $tendercontract = TendersContracts::where('id',$id)->first();
        $tenders = Tenders::find($tendercontract->tender_id);
        $this->tenders = $tenders;
        $this->contractor  = !empty($tendercontract->user_id) ? $tendercontract->user_id : 0;
        $this->files = TendersFiles::where('tender_id',$tenders->id)->get();
        $this->storage = storage();
        $messageview = View::make('front.tenders.biddingloop',$this->data);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function contractorBiddingPrice(Request $request)
    {
        $id = $request->tendercontract;
        $tendercontract = TendersContracts::where('id',$id)->first();
        $tenderproductid = $request->tenderproduct;
        $price = $request->price;
        $userid = $tendercontract->user_id;
        $companyid = $tendercontract->company_id ?: 1;
        if(!empty($tenderproductid)&&!empty($price)){
            $tenderproduct = \App\TendersProduct::where('id',$tenderproductid)->first();
            if(!empty($tenderproduct->id)){
                $tender = Tenders::find($tenderproduct->tender_id);
                if(!empty($tender->id)){
                    $tenderbidding = TenderBidding::where('user_id',$userid)->where("tender_id",$tender->id)->first();
                    if(empty($tenderbidding->id)){
                        $tenderbidding = new TenderBidding();
                        $tenderbidding->project_id = $tender->project_id;
                        $tenderbidding->tender_id = $tender->id;
                        $tenderbidding->user_id = $userid;
                        $tenderbidding->company_id = $companyid;
                        $tenderbidding->sourcing_id  = '' ;
                        $tenderbidding->save();
                    }
                    $tenderbiddingproduct = TenderBiddingProduct::where('tender_id',$tender->id)->where('tender_product',$tenderproduct->id)->where('bidding_id',$tenderbidding->id)->first();
                    if(empty($tenderbiddingproduct->id)){
                        $tenderbiddingproduct = new TenderBiddingProduct();
                        $tenderbiddingproduct->tender_id = $tender->id;
                        $tenderbiddingproduct->bidding_id = $tenderbidding->id;
                        $tenderbiddingproduct->tender_product = $tenderproduct->id;
                        $tenderbiddingproduct->products = $tenderproduct->cost_items;
                        $tenderbiddingproduct->category = $tenderproduct->category;
                    }
                    $qty = $tenderproduct->qty ?: 0;
                    $finalamount = $price*$qty;
                    $tenderbiddingproduct->qty = $qty;
                    $tenderbiddingproduct->price = numberformat($price);
                    $tenderbiddingproduct->finalprice = numberformat($finalamount);
                    $tenderbiddingproduct->save();
                    return Reply::dataOnly(['amount' => $finalamount]);
                }
            }
        }
    }

}

