<?php

namespace App\Http\Requests\Todo;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreTodo extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'status' => 'required',
            'project_id' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            //'category_id' => 'required'
        ];
    }
}
