<?php

namespace App\Http\Requests\CostItems;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreCostItems extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cost_item_name' => 'required',
            'unit' => 'required'
            //'cost_item_description' => 'required',
            //'unit' => 'required'
        ];
    }
}
