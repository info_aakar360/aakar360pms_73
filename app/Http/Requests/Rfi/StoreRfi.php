<?php

namespace App\Http\Requests\Rfi;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreRfi extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'subject' => 'required',
            'due_date' => 'required',
            'drawing_no' => 'required',
            'project_id' => 'required',
            'activity' => 'required',
            'costitem' => 'required',
            'location' => 'required'
        ];
    }
}
