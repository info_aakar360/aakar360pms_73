<?php

namespace App\Http\Requests\Admin\Contractors;

use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContractorsRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name" => "required',
            'email' => 'required|email|unique:contractors',
            'mobile' => 'required|numeric|digits:10',
        ];
    }
}
