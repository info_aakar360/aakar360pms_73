<?php

namespace App\Http\Requests\Admin\Employee;

use App\EmployeeDetails;
use App\Http\Requests\CoreRequest;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends CoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $detailID = EmployeeDetails::where('user_id', $this->route('employee'))->first();
        return [
            'name'  => 'required',
            "email" => "email|nullable",
            'mobile' => 'required|numeric|digits:10',

        ];
    }
}
