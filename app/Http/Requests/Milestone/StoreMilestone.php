<?php

namespace App\Http\Requests\Milestone;

use Illuminate\Foundation\Http\FormRequest;

class StoreMilestone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required',
            'title' => 'required',
            'start_date' => 'required',
            'due_date' => 'required',
            'status' => 'required',
            'activity' => 'required',
            'tasks' => 'required',
        ];

        return $rules;
    }
}
