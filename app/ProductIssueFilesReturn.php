<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductIssueFilesReturn extends Model
{
    protected $table = 'product_issue_return_files';

    protected static function boot()
    {
        parent::boot();
    }
}
