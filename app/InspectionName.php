<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InspectionName extends Model
{
    protected $table = 'inspection_name';

    protected static function boot()
    {
        parent::boot();

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('inspection_name.company_id', '=', $company->id);
            }
        });
    }
}
