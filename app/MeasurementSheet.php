<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasurementSheet extends Model
{
    protected $table = 'measurement_sheet';

}
