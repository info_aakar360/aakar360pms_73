<?php

namespace App;

use App\Observers\SupplierObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Stripe\Invoice as StripeInvoice;
use Laravel\Cashier\Invoice;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(SupplierObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('suppliers.company_id', '=', $company->id);
            }
        });
    }

    public function getAdminUser($companyid=false)
    {
        if(empty($companyid)){
            $companyid =  company()->id;
        }
        $roles = RoleUser::join('roles','roles.id','=','role_user.role_id')->select('role_user.*')->where('roles.company_id',$companyid)->where('roles.name','admin')->first();
        if(!empty($roles)){
            return $roles->user_id;
        }
    }

    public function sendEmail($emailarray)
    {

        $smtp = SmtpSettingsUser::where('user_id',$this->id)->where('company_id',$this->company_id)->first();
        if(isset($smtp->status)){
            $status = $smtp->status;
            if($status=='0'){
                $adminuser = $this->getAdminUser($this->company_id);
                if($adminuser){
                    $smtp = SmtpSettingsUser::where('user_id',$adminuser)->first();
                }
            }
        }else{
            $adminuser = $this->getAdminUser($this->company_id);
            if($adminuser){
                $smtp = SmtpSettingsUser::where('user_id',$adminuser)->first();
            }
        }
        $mailarray = array();
        if(!empty($smtp)){
            if($smtp->mail_driver == 'mail'){
                switch ($smtp->mail_sender){
                    case 'google':
                        $mailarray['host'] = 'smtp.gmail.com';
                        $mailarray['port'] = '587';
                        $mailarray['encryption'] = 'tls';
                        break;
                    case 'zoho':
                        $mailarray['host'] = 'smtp.zoho.com';
                        $mailarray['port'] = '587';
                        $mailarray['encryption'] = 'tls';
                        break;
                    case 'yahoo':
                        $mailarray['host'] = 'smtp.mail.yahoo.com';
                        $mailarray['port'] = '465';
                        $mailarray['encryption'] = 'ssl';
                        break;
                }
            }else if ($smtp->mail_driver == 'smtp') {
                $mailarray['host'] = $smtp->mail_host;
                $mailarray['port'] = $smtp->mail_port;
                $mailarray['encryption'] = $smtp->mail_encryption;
            }
        }
        if($mailarray){
            try {
                $transport = new \Swift_SmtpTransport($mailarray['host'], $mailarray['port'], $mailarray['encryption']);
                $transport->setUsername($smtp->mail_username);
                $transport->setPassword($smtp->mail_password);
                $mailer = new \Swift_Mailer($transport);
                $subject = $emailarray['subject'];
                $body = $emailarray['message'];
                $message   = (new \Swift_Message($subject))
                    ->setFrom($smtp->mail_username, $smtp->mail_from_name)
                    ->setTo($emailarray['email'])
                    ->setBody($body);
                $mailer->send($message);
                if($smtp->verified==0){
                    $smtp->verified = 1;
                    $smtp->save();
                }
                return [
                    'success' => true,
                    'message' => __('messages.smtpSuccess')
                ];
            } catch (\Swift_TransportException $e) {
                return [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            } catch (\Exception $e) {
                return [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            }
        }
    }
}
