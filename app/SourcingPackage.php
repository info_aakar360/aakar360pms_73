<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SourcingPackage extends Model
{
    protected $table = 'sourcing_packages';

    protected static function boot()
    {
        parent::boot();
    }
}
