<?php

namespace App;

use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;

class AppProject extends Model
{
    protected $table = 'projects';
    protected $appends = ['imageurl','statusname','startdatetime','deadlinedatetime'];
    //define accessor
    public function getImageUrlattribute()
    {
        $filename = uploads_url().'building-construction.jpg';
        $imagename  = $this->image;
        if($imagename){
            $storage = storage();
            $url = uploads_url();
            $awsurl = awsurl();
            $companyid = $this->company_id;
            $id = $this->id;
            if(file_exists('uploads/project-files/'.$id.'/'.$imagename)){
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'project-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/project-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $imagename;
                            break;
                        case 'dropbox':
                            $filename = $imagename;
                            break;
                    }
                }
            }
        }
        return $filename;
    }
    public function getStatusnameattribute()
    {
        $status = 'Not Started';
        $statusname  = $this->status;
        if($statusname){
                switch($statusname){
                    case 'not started':
                        $status = 'Not Started';
                        break;
                    case 'in complete':
                        $status = 'In Complete';
                        break;
                    case 'completed':
                        $status = 'Completed';
                        break;
                    case 'in progress':
                        $status = 'In Progress';
                        break;
        }
        }
        return $status;
    }
    public function getstartdatetimeattribute()
    {
        if(!empty($this->start_date)){
            return date('d M Y',strtotime($this->start_date));
        }
    }
    public function getdeadlinedatetimeattribute()
    {
        if(!empty($this->deadline)){
            return date('d M Y',strtotime($this->deadline));
        }
    }
}