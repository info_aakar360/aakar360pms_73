<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectCostItemsPosition extends Model
{
    protected $table = 'project_cost_item_position';

    protected static function boot()
    {
        parent::boot();

    }
    public static function categoryList($projectid,$subprojectid)
    {
        $subprojectid = $subprojectid ?: 0;
         $categoryarray = array();
        $maxlevel = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->max('level');
        $category  = '';
        $projectcostitemarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->get();
        $parent = 0;
         foreach ($projectcostitemarray as $projectcostitem){
            if($projectcostitem->parent>0){
              $categoryarray[] = $projectcostitem->parent.','.$projectcostitem->itemid;
          }else{
              $categoryarray[] = $projectcostitem->itemid;
          }
        }
        return \GuzzleHttp\json_encode(array_filter($categoryarray));
    }

}
