<?php

namespace App;

use App\Observers\ManpowerLogObserver;
use App\Observers\ProjectTimeLogObserver;
use App\Observers\WeatherLogObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class WeatherLog extends Model
{
    use Notifiable;

    protected $table = 'weather_logs';

    protected static function boot()
    {
        parent::boot();

        static::observe(WeatherLogObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('weather_logs.company_id', '=', $company->id);
            }
        });
    }
    public function project() {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

}
