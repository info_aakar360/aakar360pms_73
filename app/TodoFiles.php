<?php

namespace App;

use App\Observers\TaskObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TodoFiles extends Model
{
    protected $table = 'todo_files';

    protected static function boot()
    {
        parent::boot();

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('todo_files.company_id', '=', $company->id);
            }
        });
    }
}
