<?php

namespace App;

use App\IncomeExpenseHead;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AccountPayments extends Model
{
    protected $table = 'account_payments';

    protected $appends = ['images','ledgerfromname','ledgertoname'];

    public function ledgerfrom()
    {
        return $this->belongsTo('App\IncomeExpenseHead', 'ledger_from','id');
    }
    public function ledgerto()
    {
        return $this->belongsTo('App\IncomeExpenseHead', 'ledger_to','id');
    }
    public function getledgerfromnameAttribute()
    {
        $newimages = '';
        if(!empty($this->ledger_from)){
            $newimages = income_head($this->ledger_from);
        }
        return $newimages;
    }
    public function getledgertonameAttribute()
    {
        $newimages = '';
        if(!empty($this->ledger_to)){
            $newimages = income_head($this->ledger_to);
        }
        return $newimages;
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = AccountPaymentsFiles::where('payment_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            $filename = '';
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'payment-voucher-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/payment-voucher-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
