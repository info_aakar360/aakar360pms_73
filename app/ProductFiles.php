<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductFiles extends Model
{
    protected $table = 'product_files';

    protected static function boot()
    {
        parent::boot();
    }
}
