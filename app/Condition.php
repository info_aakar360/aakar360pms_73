<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'condition';

    protected static function boot()
    {
        parent::boot();
    }
}
