<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingAssignedUser extends Model
{
    protected $table = 'meeting_assigned_user';

    protected static function boot()
    {
        parent::boot();
    }
}
