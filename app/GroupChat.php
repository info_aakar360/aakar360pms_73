<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupChat extends Model
{

    protected $table = 'group_chat';

    protected $appends = ['image_url','datetime'];
    public function getImageUrlAttribute()
    {
        $allign = uploads_url().'multiple-user.png';
        if(!empty($this->image)){
            $allign = uploads_url().'groupchats/'.$this->image;
        }
        return $allign;
    }
    public function getDatetimeAttribute()
    {
        return date('d M Y h:i A',strtotime($this->created_at));
    }
}
