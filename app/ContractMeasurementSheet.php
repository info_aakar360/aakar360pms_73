<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractMeasurementSheet extends Model
{
    protected $table = 'contract_measurement_sheet';

}
