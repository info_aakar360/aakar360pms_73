<?php

namespace App;

use App\Observers\DocumentTypeObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DocumentType extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::observe(DocumentTypeObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('document_types.company_id', '=', $company->id);
            }
        });
    }
}
