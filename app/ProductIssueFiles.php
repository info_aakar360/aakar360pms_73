<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductIssueFiles extends Model
{
    protected $table = 'product_issue_files';

    protected static function boot()
    {
        parent::boot();
    }
}
