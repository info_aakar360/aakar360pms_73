<?php
namespace App\Traits;

use App\AppProject;
use App\Conversation;
use App\Employee;
use App\IncomeExpenseGroup;
use App\IncomeExpenseHead;
use App\ProjectMember;
use App\User;
use Google\Service\Compute\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

trait EmployeeRegister
{
    public function createemployee($userarray)
    {
        try{

            $user = $userarray['user'];
            $usercompany = $userarray['company_id'];
            $projectid = $userarray['projectid'];
            $sharetoproject = $userarray['sharetoproject'];
            $name = $userarray['name'];
            $email = $userarray['email'];
            $mobile = $userarray['mobile'];
            $user_type = $userarray['user_type'];
            $password = $userarray['password'];
            $gender = $userarray['gender'];
            $address = $userarray['address'];
            $fcm = $userarray['fcm'];
            $projectdetails  = AppProject::find($projectid);
            if(!empty($mobile)){

                $users = Employee::where('user_type',$user_type)->where('mobile',$mobile)->where('company_id',$usercompany)->first();
                if(empty($users->id)){
                    $alruser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('mobile',$mobile)->first();
                    if(empty($alruser->id)){
                        $alruser = new User();
                        $alruser->company_id = '0';
                        $alruser->name = $name;
                        $alruser->email = $email ? : '';
                        $alruser->mobile = $mobile;
                        $alruser->login = 'disable';
                        $alruser->status = 'inactive';
                        $alruser->save();
                    }
                    $users = new Employee();
                    $users->company_id = $usercompany;
                    $users->user_id = $alruser->id;
                    $prevmem = Employee::where('mobile',$mobile)->where('company_id',$usercompany)->first();
                    if(!empty($prevmem->id)){
                        $users->name = $prevmem->name;
                    }else{
                        $users->name = $name;
                    }
                    $users->email = $email ? : '';
                    $users->mobile = $mobile;
                    if($password){
                        $users->password = bcrypt($password);
                    }
                    $users->gender = $gender ?: '';
                    $users->status = 'active';
                    $users->user_type = $user_type;
                    $users->added_by = $user->id;
                    $users->save();

                }

            }else{
                $users = new Employee();
                $users->company_id = $usercompany;
                $users->user_id = 0;
                $users->name = $name;
                $users->email = $email ? : '';
                $users->mobile = $mobile ? : '';
                $users->gender = $gender ?: '';
                $users->status = 'active';
                $users->user_type = $user_type;
                $users->added_by = $user->id;
                $users->save();
            }

            if($user_type!=='admin'){
                $usertype = ucwords($user_type);
                $incometype = IncomeExpenseGroup::where('name',$usertype)->where('company_id',$usercompany)->first();
                $incomearray = IncomeExpenseHead::where('company_id',$usercompany)->where('income_expense_group_id',$incometype->id)->where('emp_id',$users->id)->first();
                if(empty($incomearray)){
                    $incomearray = new IncomeExpenseHead();
                    $incomearray->company_id = $usercompany;
                    $incomearray->name = $name;
                    $incomearray->project_id = !empty($projectdetails->id) ? $projectdetails->id : 0;
                    $incomearray->income_expense_group_id = $incometype->id;
                    $incomearray->income_expense_type_id = $incometype->income_type;
                    $incomearray->created_by = $user->id;
                    $incomearray->emp_id = $users->id;
                    $incomearray->save();
                }
            }


            if(!empty($projectid)&&!empty($users)){
                $prevproject = ProjectMember::where('employee_id',$users->id)->where('project_id',$projectid)->first();
                if(empty($prevproject->id)){
                    $level = 0;
                    if($projectdetails->added_by==$user->id){
                        $level = 1;
                    }else{
                        $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectdetails->id)->first();
                        $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                        $level = $prolevel+1;
                    }
                    $pm = new ProjectMember();
                    $pm->company_id = $projectdetails->company_id;
                    $pm->project_id = $projectdetails->id;
                    $pm->user_id = $users->user_id;
                    $pm->employee_id = $users->id;
                    $pm->user_type = $user_type;
                    $pm->assigned_by = $user->id;
                    $pm->share_project = '0';
                    $pm->level = $level;
                    if($sharetoproject=='1'){
                        $pm->share_project = '1';
                        if(!empty($users->user_id)){
                            $alreuser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->find($users->user_id);
                            if(!empty($alreuser)&&!empty($alreuser->fcm)){
                                $notifmessage['title'] = 'Project Shared';
                                $notifmessage['body'] = 'You have been added to ' . ucwords(get_project_name($projectid)) . ' project by ' . $user->name;
                                $notifmessage['activity'] = 'projects';
                                sendFcmNotification($alreuser->id, $notifmessage);
                            }
                        }
                    }
                    $pm->save();

                    $conversation = Conversation::where('moduleid',$projectdetails->id)->where('module','project')->first();
                    if(empty($conversation)){
                        $conversation = new Conversation();
                        $conversation->moduleid = $projectdetails->id;
                        $conversation->module = 'project';
                        $conversation->lastupdated = date('Y-m-d H:i:s');
                    }
                    $projectmembers = ProjectMember::where('project_id',$projectdetails->id)->where('share_project','1')->pluck('user_id')->toArray();
                    $projectmembers[] = $user->id;
                    $projectmembers = array_filter(array_unique($projectmembers));
                    $conversation->users = !empty($projectmembers) ? implode(',',$projectmembers) : '';
                    $conversation->save();

                    $response['status'] = 200;
                    $response['response'] = $users->toArray();
                    $response['message'] = 'User Assigned To Project';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['response'] = $users->toArray();
                    $response['message'] = 'User already assigned to project';
                    return $response;
                }
            }

            $response['status'] = 200;
            $response['response'] = $users;
            $response['message'] = 'User Assigned To Project';
            return $response;
        }catch(\Exception $ex){
            return new Response('Something went wrong. Try again!', 500);
        }
    }
}
