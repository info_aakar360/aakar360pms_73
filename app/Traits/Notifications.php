<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

trait Notifications
{
    public function notify($user, $count=false, $type="new-notification", $message="New notification received.")
    {
        try{
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER', 'ap2'),
                'encrypted' => true,
                'useTLS' => true
            );
            $pusher = new Pusher(
                env('PUSHER_APP_KEY', "56d658761051d805686d"),
                env('PUSHER_APP_SECRET', "4ca6e2827e2c196895a0"),
                env('PUSHER_APP_ID', "1318234"),
                $options
            );
            $pusher->trigger('notifications', $type.'.'.$user->id, ["message"=>$message, "count"=>$count]);
            return new Response('Success', 200);
        }catch(\Exception $ex){
            return new Response('Something went wrong. Try again!', 500);
        }
    }

    public function conversations($user, $module,$moduleid,$message="New notification received.")
    {
        try{
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER', 'ap2'),
                'encrypted' => true,
                'useTLS' => true
            );
            $pusher = new Pusher(
                env('PUSHER_APP_KEY', "56d658761051d805686d"),
                env('PUSHER_APP_SECRET', "4ca6e2827e2c196895a0"),
                env('PUSHER_APP_ID', "1318234"),
                $options
            );
            $pusher->trigger('conversations',  'conversation.'.$user->id, ["message"=>$message,"module"=>$module,"moduleid"=>$moduleid]);
            return new Response('Success', 200);
        }catch(\Exception $ex){
            return new Response('Something went wrong. Try again!', 500);
        }
    }
}
