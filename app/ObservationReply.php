<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ObservationReply extends Model
{
    protected $table = 'observations_reply';

    protected static function boot()
    {
        parent::boot();

    }
}
