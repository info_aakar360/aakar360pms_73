<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class GrnFiles extends Model
{
    protected $table = 'grn_files';

    protected static function boot()
    {
        parent::boot();
    }
}
