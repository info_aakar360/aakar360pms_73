<?php

namespace App;

use App\Observers\ManpowerCategoryObserver;
use App\Observers\ManpowerLogObserver;
use App\Observers\ProjectTimeLogObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ManpowerCategory extends Model
{
    use Notifiable;

    protected $table = 'manpower_category';

    /*protected static function boot()
    {
        parent::boot();

        static::observe(ManpowerCategoryObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('manpower_category.company_id', '=', $company->id);
            }
        });
    }*/

}
