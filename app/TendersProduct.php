<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TendersProduct extends Model
{
    protected $table = 'tenders_product';

    protected static function boot()
    {
        parent::boot();
    }
}
