<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavesRules extends Model
{
    protected $fillable = ['leave_rule_name'];
}
