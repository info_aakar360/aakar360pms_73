<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractInvoiceboq extends Model
{
    protected $guarded = ['id'];
    protected $table = 'contract_invoices_boq';

}
