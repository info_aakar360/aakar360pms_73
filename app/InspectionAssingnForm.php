<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InspectionAssingnForm extends Model
{
    protected $table = 'inspection_assign_form';

    protected static function boot()
    {
        parent::boot();
    }
}
