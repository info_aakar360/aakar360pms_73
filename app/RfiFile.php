<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RfiFile extends Model
{
    protected $table = 'rfi_files';

    protected static function boot()
    {
        parent::boot();
    }
}
