<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoiceboq extends Model
{
    protected $guarded = ['id'];
    protected $table = 'invoices_boq';

}
