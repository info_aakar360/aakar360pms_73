<?php
namespace App;

use App\Observers\HolidayObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Holiday
 * @package App\Models
 */
class ProjectsHoliday extends Model
{
    // Don't forget to fill this array
    protected $table = 'projects_holidays';

    protected $fillable = [];

    protected $guarded = ['id'];

    public static function getHolidayByDates($startDate, $endDate){

        return ProjectsHoliday::select(DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as holiday_date'), 'occassion')->where('date', '>=', $startDate)->where('date', '<=', $endDate)->get();
    }

    public static function checkHolidayByDate($date){
        return ProjectsHoliday::Where('date', $date)->first();
    }
}