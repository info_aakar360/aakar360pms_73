<?php

namespace App;

use App\Observers\SupplierObserver;
use App\Observers\TaskObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Stripe\Invoice as StripeInvoice;
use Laravel\Cashier\Invoice;

class Contractors extends Model
{
    protected $table = 'contractors';
    protected $dates = ['created_at', 'updated_at'];

    protected static function boot()
    {
        parent::boot();
        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('contractors.company_id', '=', $company->id);
            }
        });
    }

}
