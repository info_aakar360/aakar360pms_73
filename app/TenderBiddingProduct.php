<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TenderBiddingProduct extends Model
{
    protected $table = 'tender_bidding_product';

    protected static function boot()
    {
        parent::boot();
    }
}
