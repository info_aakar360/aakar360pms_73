<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ManpowerAttendance extends Model
{
    protected $table = 'manpower_attendance';
    protected $appends = ['images','datetime'];
    protected static function boot()
    {
        parent::boot();
    }
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y');
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $manpowerid = $this->manpower_id;
        $companyid = $this->company_id;
        $punchitemarray = ManpowerLogFiles::where('manpower_id',$manpowerid)->where('attendance_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            $filename = '';
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'manpower-log-files/'.$manpowerid.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/manpower-log-files/'.$manpowerid.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
