<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveRulesSettings extends Model
{
    protected $fillable  = ['name','description','field_id','field_class'];
}
