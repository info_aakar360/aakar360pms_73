<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class IndentsFiles extends Model
{
    protected $table = 'indents_files';

    protected static function boot()
    {
        parent::boot();
    }
}
