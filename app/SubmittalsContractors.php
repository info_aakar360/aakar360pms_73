<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SubmittalsContractors extends Model
{
    protected $table = 'submittals_contractors';

    protected static function boot()
    {
        parent::boot();
    }
}
