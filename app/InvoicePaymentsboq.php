<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePaymentsboq extends Model
{
    protected $guarded = ['id'];
    protected $table = 'invoices_payments_boq';

}
