<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RfiReply extends Model
{
    protected $table = 'rfi_reply';

    protected static function boot()
    {
        parent::boot();
    }
}
