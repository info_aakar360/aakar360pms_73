<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class PunchItemReply extends Model
{
    protected $table = 'punch_item_reply';

    protected $appends = ['userdetails','images'];
    protected static function boot()
    {
        parent::boot();

    }
    //define accessor
    public function getUserdetailsAttribute()
    {
        $allign = array();
        if(!empty($this->added_by)){
            $userid = $this->added_by;
            $allign['name'] = get_user_name($userid);
            $allign['image'] = get_users_image_link($userid);
        }
        return $allign;
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $punchitemid = $this->punch_item_id;
        $companyid = $this->company_id;
        $punchitemarray = PunchItemFiles::where('task_id',$punchitemid)->where('reply_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $awsurl = awsurl();
                $url = uploads_url();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'punch-files/'.$punchitemid.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/punch-files/'.$punchitemid.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
