<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AppBom extends Model
{
    protected $table = 'bom';

    protected static function boot()
    {
        parent::boot();
    }
}
