<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Stripe\Invoice as StripeInvoice;
use Laravel\Cashier\Invoice;

class AppPurchaseInvoice extends Model
{
    protected $table = 'purchase_invoice';
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
    }

    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = PiFile::where('pi_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'pi-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/pi-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
