<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Meetings extends Model
{
    protected $table = 'meetings';

    protected static function boot()
    {
        parent::boot();
    }
}
