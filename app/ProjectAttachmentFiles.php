<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectAttachmentFiles extends Model
{
    protected $table = 'project_attachment_files';

    protected static function boot()
    {
        parent::boot();

//        static::observe(ProjectCategoryObserver::class);
//
//        $company = company();
//
//        static::addGlobalScope('company', function (Builder $builder) use($company) {
//            if ($company) {
//                $builder->where('project_category.company_id', '=', $company->id);
//            }
//        });
    }
}
