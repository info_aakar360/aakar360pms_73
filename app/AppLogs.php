<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppLogs extends Model
{
    protected $table = 'app_logs';
}