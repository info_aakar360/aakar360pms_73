<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DailyLogFiles extends Model
{
    protected $table = 'daily_log_files';

    protected static function boot()
    {
        parent::boot();
    }
}
