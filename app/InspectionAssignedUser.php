<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InspectionAssignedUser extends Model
{
    protected $table = 'inspection_assigned_users';

    protected static function boot()
    {
        parent::boot();
    }
}
