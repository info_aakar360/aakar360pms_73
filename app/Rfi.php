<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Rfi extends Model
{
    protected $table = 'rfi';

    protected static function boot()
    {
        parent::boot();
    }
}
