<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tenders extends Model
{
    protected $table = 'tenders';

    protected static function boot()
    {
        parent::boot();
    }
}
