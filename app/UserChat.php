<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Entrust;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class UserChat extends Authenticatable
{

    protected $table = 'users_chat';

    public $timestamps = true;

    protected $guarded = ["id"];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $dates = ['created_at', 'updated_at'];

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from')->withoutGlobalScope('active');
    }

    public function toUser()
    {
        return $this->belongsTo(User::class, 'to')->withoutGlobalScope('active');
    }

    public static function chatDetail($id,$userID,$skip)
    {
        $count = pagecount();
        $msigs = UserChat::where(function($q) use ($id,$userID) {
            $q->Where('user_id', $id)->Where('user_one', $userID)
                ->orwhere(function($q) use ($id,$userID) {
                    $q->Where('user_one', $id)
                        ->Where('user_id', $userID);
                });
        })->offset($skip)->take($count)->orderBy('id', 'desc')->pluck('id')->toArray();
        $msigs = array_reverse($msigs);
        return UserChat::whereIn('id',$msigs)->get();
    }

    public static function lastMessage($id,$userID)
    {
        $msigs = UserChat::where(function($q) use ($id,$userID) {
            $q->Where('user_id', $id)->Where('user_one', $userID)
                ->orwhere(function($q) use ($id,$userID) {
                    $q->Where('user_one', $id)
                        ->Where('user_id', $userID);
                });
        })->orderBy('id', 'desc')->first();
        return !empty($msigs) ? $msigs : '';
    }
    public static function conversionDetail($id,$userID)
    {
        $converstion = Conversation::where(function($q) use ($id,$userID) {
            $q->Where('user_one', $id)->Where('user_two', $userID)
                ->orwhere(function($q) use ($id,$userID) {
                    $q->Where('user_two', $id)
                        ->Where('user_one', $userID);
                });
        })->orderBy('created_at', 'asc')->first();
        if(empty($converstion)){
            $converstion = new Conversation();
            $converstion->user_one = $id;
            $converstion->user_two = $userID;
        }
        $converstion->users = $id.','.$userID;
        $converstion->moduleid = '0';
        $converstion->module = 'chat';
        $converstion->lastupdated = date('Y-m-d H:i:s');
        $converstion->save();
        return $converstion->id;
    }
    public static function messageSeenUpdate($loginUser,$toUser,$updateData)
    {
        return UserChat::where('from', $toUser)->where('to', $loginUser)->update($updateData);
    }

}
