<?php

namespace App;

use App\Observers\ProjectTimeLogObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProjectWorkWeek extends Model
{
    use Notifiable;
    protected  $table = 'projects_workweek';

}
