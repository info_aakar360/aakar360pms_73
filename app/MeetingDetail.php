<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingDetail extends Model
{
    protected $table = 'meeting_details';

    protected static function boot()
    {
        parent::boot();
    }
}
