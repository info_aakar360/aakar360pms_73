<?php

namespace App;

use App\Observers\ManpowerLogObserver;
use App\Observers\ProjectTimeLogObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ManpowerLog extends Model
{
    use Notifiable;

    protected $table = 'manpower_logs';
    protected $appends = ['category_name','contractor_name','project_name','images','task_name','datetime'];
   /* protected static function boot()
    {
        parent::boot();

        static::observe(ManpowerLogObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('manpower_logs.company_id', '=', $company->id);
            }
        });
    }*/
    public function project() {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y');
    }

    public function getTaskNameAttribute()
    {
        $costitemname = '';
        $taskid = $this->task_id;
        if($taskid){
            $task = Task::find($taskid);
            if(!empty($task->heading)){
                $costitemname = $task->heading;
            }
        }
        return $costitemname;
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = ManpowerLogFiles::where('manpower_id',$id)->where('reply_id','0')->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'manpower-log-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/manpower-log-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
    //define accessor
    public function getProjectNameAttribute()
    {
        return get_project_name($this->project_id);
    }
    //define accessor
    public function getContractorNameAttribute()
    {
        return get_users_contractor_name($this->contractor);
    }
    //define accessor
    public function getCategoryNameAttribute()
    {
        return get_manpower_category($this->manpower_category);
    }
}
