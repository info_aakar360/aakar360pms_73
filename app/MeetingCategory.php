<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingCategory extends Model
{
    protected $table = 'meeting_category';

    protected static function boot()
    {
        parent::boot();
    }
}
