<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_type';

    protected static function boot()
    {
        parent::boot();
        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('product_type.company_id', '=', $company->id);
            }
        });
    }
}
