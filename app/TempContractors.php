<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TempContractors extends Model
{
    protected $table = 'temp_contractors';

    protected static function boot()
    {
        parent::boot();
    }
}
