<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeasurementType extends Model
{
    protected $table = 'measurement_type';


    public function unitdetails(){

        return $this->hasOne('App\Units','id','unit');

    }
}
