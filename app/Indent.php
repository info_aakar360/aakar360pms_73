<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Stripe\Invoice as StripeInvoice;
use Laravel\Cashier\Invoice;

class Indent extends Model
{
    protected $table = 'indents';
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('indents.company_id', '=', $company->id);
            }
        });
    }
}
