<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupChatUsers extends Model
{
    protected $table = 'group_chat_users';

    protected $appends = ['alignment','username','userimage','datetime'];
    public function getAlignmentAttribute()
    {
        $allign = 'right';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $allign = 'left';
        }
        return $allign;
    }
    public function getDatetimeAttribute()
    {
        return date('d M Y h:i A',strtotime($this->created_at));
    }

    public function getUsernameAttribute()
    {
        $allign = '';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $companyid = $this->company_id;
            $allign = get_users_employee_name($userid,$companyid);
        }
        return $allign;
    }
    public function getUserimageAttribute()
    {
        $allign = '';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $allign = get_users_image_link($userid);
        }
        return $allign;
    }
}
