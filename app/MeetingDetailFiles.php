<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingDetailFiles extends Model
{
    protected $table = 'meeting_detail_files';

    protected static function boot()
    {
        parent::boot();
    }
}
