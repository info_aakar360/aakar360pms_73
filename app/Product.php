<?php

namespace App;

use App\Observers\ProductObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'price', 'tax_id'];
    protected $appends = ['total_amount','images','imageurl'];

    protected static function boot()
    {
        parent::boot();

        static::observe(ProductObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('products.company_id', '=', $company->id);
            }
        });
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }

    public static function taxbyid($id) {
        return Tax::where('id', $id);
    }

    public function getTotalAmountAttribute(){

        if(!is_null($this->price) && !is_null($this->tax)){
            return $this->price + ($this->price * ($this->tax->rate_percent/100));
        }

        return "";
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = ProductFiles::where('product_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            $filename = '';
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'product-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/product-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
    public function getImageUrlattribute()
    {
        $filename = uploads_url().'building-construction.jpg';
        $url = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";
        $companyid = $this->company_id;
        $id = $this->id;
        $imagename  = $this->image;
        if($imagename){
            if(file_exists('uploads/product-files/'.$id.'/'.$imagename)){
                $storage = storage();
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = uploads_url().'product-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $url.$companyid.'/product-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $imagename;
                            break;
                        case 'dropbox':
                            $filename = $imagename;
                            break;
                    }
                }
            }
        }
        return $filename;
    }
}
