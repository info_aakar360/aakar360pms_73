<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SubmittalsWorkflow extends Model
{
    protected $table = 'submittals_workflow';

    protected static function boot()
    {
        parent::boot();
    }
}
