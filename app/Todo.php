<?php

namespace App;

use App\Observers\TaskObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Todo extends Model
{
    protected $table = 'todo';
    use Notifiable;

    protected static function boot()
    {
        parent::boot();

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('todo.company_id', '=', $company->id);
            }
        });
    }

    protected $dates = ['due_date', 'completed_on', 'start_date'];
    protected $appends = ['due_on','create_on'];

    /**
     * @return string
     */
    public function getDueOnAttribute(){
        if(!is_null($this->due_date)){
            return $this->due_date->format('d M, y');
        }
        return "";
    }
    public function getCreateOnAttribute(){
        if(!is_null($this->start_date)){
            return $this->start_date->format('d M, y');
        }
        return "";
    }

}
