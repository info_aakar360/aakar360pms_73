<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AppProductIssue extends Model
{
    protected $table = 'product_issue';
    protected $appends = ['images','datetime'];
    protected static function boot()
    {
        parent::boot();
    }
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y');
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = ProductIssueFiles::where('issue_id',$id)->where('reply_id','0')->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $awsurl = awsurl();
                $url = uploads_url();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'product-issue-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/product-issue-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d-m-Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
