<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TendersContracts extends Model
{
    protected $table = 'tenders_contracts';

    protected static function boot()
    {
        parent::boot();
    }
}
