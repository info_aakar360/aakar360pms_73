<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignStructure extends Model
{
    protected $fillable  = ['user_id','rules_id','effective_date'];
}
