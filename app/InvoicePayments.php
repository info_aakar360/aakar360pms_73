<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePayments extends Model
{
    protected $guarded = ['id'];
    protected $table = 'invoices_payments';

}
