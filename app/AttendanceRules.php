<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceRules extends Model
{
    protected $fillable = ['name'];
}
