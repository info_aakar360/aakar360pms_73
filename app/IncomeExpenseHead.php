<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
class IncomeExpenseHead extends Model
{
    use Notifiable;
   /* protected $dates = ['deleted_at'];*/
    protected $appends = ['images','user_type','income_group_name'];

  /*  public function IncomeExpenseType()
    {
        return $this->belongsTo('App\IncomeExpenseType','income_expense_type_id');
    }

    public function IncomeExpenseGroup()
    {
        return $this->belongsTo('App\IncomeExpenseGroup','income_expense_group_id');
    }


    public function Transaction()
    {
        return $this->hasMany('App\Transaction','income_expense_head_id');
    }*/
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = IncomeExpenseHeadFiles::where('ledger_id',$id)->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            $filename = '';
            if($imagename){
                $storage = storage();
                $url = uploads_url();
                $awsurl = awsurl();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'ledger-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/ledger-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['id'] = $punchitemimage->id;
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d/m/Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
    public function getUserTypeAttribute()
    {
        $newimages = '';
        if(!empty($this->emp_id)){
            $emploee = Employee::find($this->emp_id);
            if(!empty($emploee)){
                $newimages = ucwords($emploee->user_type);
            }
        }
        return $newimages;
    }
    public function getIncomeGroupNameAttribute()
    {
        $newimages = '';
        if(!empty($this->income_expense_group_id)){
            $emploee = IncomeExpenseGroup::find($this->income_expense_group_id);
            if(!empty($emploee)){
                $newimages = ucwords($emploee->name);
            }
        }
        return $newimages;
    }

}
