<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channels extends Model
{
    protected $table = 'channels';
    protected $appends = array('channelimage');

    public function getChannelimageAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $image = $this->image;
        $filename = '';
        $storage = storage();
        $url = uploads_url();
        $id = $this->id;
        if($storage){
            switch($storage){
                case 'local':
                    $filename = $url.'channels/'.$id.'/'.$image;
                    break;
                case 's3':
                    $filename = $awsurl.'channels/'.$image;
                    break;
                case 'google':
                    $filename = $image;
                    break;
                case 'dropbox':
                    $filename = $image;
                    break;
            }
        }
        return $filename;
    }

}