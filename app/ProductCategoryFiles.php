<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductCategoryFiles extends Model
{
    protected $table = 'product_category_files';

    protected static function boot()
    {
        parent::boot();
    }
}
