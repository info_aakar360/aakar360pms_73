<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductTrade extends Model
{
    protected $table = 'product_trade';

    protected static function boot()
    {
        parent::boot();
        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('product_trade.company_id', '=', $company->id);
            }
        });
    }
}
