<?php

namespace App;

use App\Observers\EmployeeDetailObserver;
use App\Traits\CustomFieldsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = 'employee';


    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'emp_id');
    }
    public static function getAllEmployees($user,$projectid = null,$exceptId = null)
    {
        if(!empty($projectid)){
            $projectlist = explode(',',$projectid);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->where('user_type','employee');
            if (!is_null($exceptId)) {
                $users->where('id', '<>', $exceptId);
            }
        return $users->groupBy('user_id')->get();
    }
    public static function getAllEmployeesButNotManagers($user,$id,$projectid = null,$exceptId = null)
    {

        $employeeTeam = EmployeeTeam::where('employee_id',$id)->pluck('team_id')->toArray();
        $employeeDirect = DirectReportTeam::where('employee_id',$id)->pluck('team_id')->toArray();
        $excludeemplyee = array_unique (array_merge ($employeeTeam, $employeeDirect));
        array_push($excludeemplyee,$id);
        if(!empty($projectid)){
            $projectlist = explode(',',$projectid);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->where('user_type','employee');
            if (!empty($excludeemplyee)) {
                $users->whereNotIn('id',$excludeemplyee);
            }
        return $users->get();
    }
    public static function getAllClients($user,$projectid = null,$exceptId = null)
    {
        if(!empty($projectid)){
            $projectlist = explode(',',$projectid);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->where('user_type','client');
        if (!is_null($exceptId)) {
            $users->where('id', '<>', $exceptId);
        }
        return $users->groupBy('user_id')->get();
    }
    public static function getAllContractors($user,$projectid = null,$exceptId = null)
    {
        if(!empty($projectid)){
            $projectlist = explode(',',$projectid);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->where('user_type','contractor');
        if (!is_null($exceptId)) {
            $users->where('id', '<>', $exceptId);
        }
        return $users->groupBy('user_id')->get();
    }
    public static function getAllEmployeeClients($user,$exceptId = null)
    {
        if(!empty($projectid)){
            $projectlist = explode(',',$projectid);
        }else{
            $projectlist = explode(',',$user->projectlist);
        }
        $companylist = Project::whereIn('id',$projectlist)->pluck('company_id')->toArray();
        $companylist[] = $user->company_id;
        $companylist = array_unique(array_filter($companylist));
        $users = Employee::whereIn('company_id',$companylist)
            ->whereOr('user_type','client')
            ->whereOr('user_type','employee');
        if (!is_null($exceptId)) {
            $users->where('id', '<>', $exceptId);
        }
        return $users->groupBy('user_id')->get();
    }
}
