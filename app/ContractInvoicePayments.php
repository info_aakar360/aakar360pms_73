<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractInvoicePayments extends Model
{
    protected $guarded = ['id'];
    protected $table = 'contract_invoices_payments';

}
