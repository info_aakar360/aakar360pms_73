<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;


class QuoteProducts extends Model
{
    protected $table = 'quote_products';
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
    }
}
