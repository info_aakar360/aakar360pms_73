<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SourcingPackageProduct extends Model
{
    protected $table = 'sourcing_packages_product';

    protected static function boot()
    {
        parent::boot();
    }
}
