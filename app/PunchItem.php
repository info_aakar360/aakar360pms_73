<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PunchItem extends Model
{
    protected $table = 'punch_item';
    protected $appends = ['assign_name','category_name','project_name','subproject_name','distribution_name','task_name','images','datetime'];
    protected static function boot()
    {
        parent::boot();

    }

    //define accessor
    public function getStatusTextAttribute()
    {
        $costitemname = '';
        $taskid = $this->category_id;
        if($taskid){
            $task = BoqCategory::find($taskid);
            if(!empty($task->title)){
                $costitemname = $task->title;
            }
        }
        return $costitemname;
    }
    //define accessor
    public function getCategoryNameAttribute()
    {
        $costitemname = '';
        $taskid = $this->category_id;
        if($taskid){
            $task = BoqCategory::find($taskid);
            if(!empty($task->title)){
                $costitemname = $task->title;
            }
        }
        return $costitemname;
    }
    //define accessor
    public function getTaskNameAttribute()
    {
        $costitemname = '';
        $taskid = $this->task_id;
        if($taskid){
            $task = Task::find($taskid);
            if(!empty($task->heading)){
                $costitemname = $task->heading;
            }
        }
        return $costitemname;
    }
    //define accessor
    public function getAssignNameAttribute()
    {
        return get_user_name($this->assign_to);
    }
    //define accessor
    public function getProjectNameAttribute()
    {
        return get_project_name($this->projectid);
    }
    //define accessor
    public function getSubProjectNameAttribute()
    {
        return get_title($this->titleid);
    }
    //define accessor
    public function getDistributionNameAttribute()
    {
        return get_user_name($this->distribution);
    }
    public function getDatetimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('d M Y');
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $id = $this->id;
        $companyid = $this->company_id;
        $punchitemarray = PunchItemFiles::where('task_id',$id)->where('reply_id','0')->get();
        foreach ($punchitemarray as $punchitemimage){
            $imagename  = $punchitemimage->hashname;
            if($imagename){
                $storage = storage();
                $awsurl = awsurl();
                $url = uploads_url();
                $id = $this->id;
                if($storage){
                    switch($storage){
                        case 'local':
                            $filename = $url.'punch-files/'.$id.'/'.$imagename;
                            break;
                        case 's3':
                            $filename = $awsurl.'/punch-files/'.$id.'/'.$imagename;
                            break;
                        case 'google':
                            $filename = $punchitemimage->google_url;
                            break;
                        case 'dropbox':
                            $filename = $punchitemimage->dropbox_link;
                            break;
                    }
                }
            }
            $imagesarray['name'] = $punchitemimage->filename;
            $imagesarray['image'] = $filename;
            $imagesarray['created_at'] = Carbon::parse($punchitemimage->created_at)->format('d-m-Y');
            $newimages[] = $imagesarray;
        }
        return $newimages;
    }
}
