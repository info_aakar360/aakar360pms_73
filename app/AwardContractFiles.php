<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AwardContractFiles extends Model
{
    protected $table = 'awarded_contracts_files';

    protected static function boot()
    {
        parent::boot();
    }
}
