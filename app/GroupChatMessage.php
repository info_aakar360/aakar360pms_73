<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupChatMessage extends Model
{
    protected $table = 'group_chat_message';

    protected $appends = ['alignment','username','userimage','image_url','datetime'];
    public function getAlignmentAttribute()
    {
        $allign = 'right';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $allign = 'left';
        }
        return $allign;
    }
    public function getDatetimeAttribute()
    {
        return date('d M Y h:i A',strtotime($this->created_at));
    }
    public function getUsernameAttribute()
    {
        $allign = '';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $allign = get_user_name($userid);
        }
        return $allign;
    }
    public function getUserimageAttribute()
    {
        $allign = '';
        if(!empty($this->user_id)){
            $userid = $this->user_id;
            $allign = get_users_image_link($userid);
        }
        return $allign;
    }
    public function getImageUrlAttribute()
    {
        $allign = '';
        if(!empty($this->image)){
            $allign = uploads_url().'group-chat-image/'.$this->image;
        }
        return $allign;
    }
}
