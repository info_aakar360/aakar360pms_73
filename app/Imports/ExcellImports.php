<?php

namespace App\Imports;

use App\BoqCategory;
use App\CostItems;
use App\Project;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\Units;
use Google\Service\Tasks\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExcellImports implements ToCollection, WithMultipleSheets
{
    public function __construct($importarray)
    {
        $this->projectid = $importarray['projectid'];
        $this->subprojectid = $importarray['subprojectid'];
        $this->user = $importarray['user'];
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }


    public function collection(Collection $renderarray)
    {
        $projectid = $this->projectid;
        $projectdetails = Project::find($projectid);
        $subprojectid = $this->subprojectid ?: 0;
        $user = $this->user;
        if(!empty($renderarray)){
            unset($renderarray[0]);
            foreach ($renderarray as $key => $row) {
                $positioncat = $actaarray = array();
                $arraycount = count(array_filter($row));
                $activitycount = $arraycount-6;
                for($a=0;$a<$activitycount;$a++){
                    $actaarray[] = trim($row[$a]);
                }
                $parent =  $positionid = $level = 0;
                $actaarray = array_values(array_filter($actaarray));
                if(!empty($actaarray)){
                foreach($actaarray as $key => $activty){
                    $boqcategory = BoqCategory::where('company_id',$projectdetails->company_id)->where('title',$activty)->first();
                    if(empty($boqcategory)){
                        $boqcategory = new BoqCategory();
                        $boqcategory->title = $activty;
                        $boqcategory->company_id = $projectdetails->company_id;
                        $boqcategory->parent = $parent ?: 0;
                        $boqcategory->save();
                    }
                    $procostposition = ProjectCostItemsPosition::where("project_id",$projectid)->where("title",$subprojectid)->where('position','row')->where('level',$level)->where('itemid',$boqcategory->id)->first();
                    if(empty($procostposition)){
                        $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('title',$subprojectid)->where('position','row')->where('level',$level)->orderBy('inc','desc')->first();
                        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
                        $procostposition = new ProjectCostItemsPosition();
                        $procostposition->project_id = $projectid;
                        $procostposition->title = $subprojectid;
                        $procostposition->position = 'row';
                        $procostposition->itemid = $boqcategory->id;
                        $procostposition->itemname = $boqcategory->title;
                        $procostposition->collock = '0';
                        $procostposition->catlevel = !empty($positioncat) ? implode(',',$positioncat) : '';
                        $procostposition->level = $level;
                        $procostposition->parent = $parent ?: 0;
                        $procostposition->inc = $newid;
                        $procostposition->save();
                    }
                    $level++;
                    $positionid =   $procostposition->id;
                    $parent =   $procostposition->id;
                    $positioncat[] = $procostposition->itemid;
                }
                $itemkey = $activitycount;
                $taskkey = $activitycount+1;
                $desckey = $activitycount+2;
                $unitkey = $activitycount+3;
                $ratekey = $activitycount+4;
                $quankey = $activitycount+5;
                $unitname = trim($row[$unitkey]);
                $units = Units::where('company_id',$projectdetails->company_id)->where('name',$unitname)->first();
                if(empty($units)){
                    $units = new Units();
                    $units->name = $unitname;
                    $units->company_id = $projectdetails->company_id;
                    $units->symbol = '';
                    $units->save();
                }
                $taskname = trim($row[$taskkey]);
                $cost_item = CostItems::where('company_id',$projectdetails->company_id)->where('cost_item_name',$taskname)->first();
                if(empty($cost_item)){
                    $cost_item = new CostItems();
                    $cost_item->cost_item_name = $taskname;
                    $cost_item->company_id = $projectdetails->company_id;
                    $cost_item->unit = $units->id;
                    $cost_item->save();
                }
                $projecttask = new ProjectCostItemsProduct();
                $projecttask->project_id = $projectid;
                $projecttask->title = $subprojectid;
                $projecttask->category = !empty($positioncat) ? implode(',',$positioncat) : 0;
                $projecttask->itemno = !empty($row[$itemkey]) ? $row[$itemkey] : '';
                $projecttask->cost_items_id = $cost_item->id;
                $projecttask->position_id = $positionid ?: 0;
                $projecttask->description = trim($row[$desckey]);
                $projecttask->unit = $units->id;
                $projecttask->qty = $row[$quankey];
                $projecttask->rate = $row[$ratekey];
                $projecttask->start_date  = date('Y-m-d');
                $projecttask->deadline   = date('Y-m-d');
                $projecttask->save();

                $taskcreate = new \App\Task();
                $taskcreate->project_id = $projectid;
                $taskcreate->title = $subprojectid;
                $taskcreate->company_id = $projectdetails->company_id;
                $taskcreate->heading = $row[$taskkey];
                $taskcreate->description = $row[$desckey];
                $taskcreate->user_id = $user->id;
                $taskcreate->cost_item_id = $projecttask->id;
                $taskcreate->boqinclude = 1;
                $taskcreate->task_category_id = !empty($procostposition->itemid) ? $procostposition->itemid : '';
                $taskcreate->save();

                }
            }
        }
    }
}
