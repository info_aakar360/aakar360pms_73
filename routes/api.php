<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Api for RFI
Route::post('projects/get-rfi', ['uses' => '\App\Http\Controllers\Api\ApiController@getRfi']);

//Api for App
Route::post('homepage', ['uses' => '\App\Http\Controllers\Api\AppApiController@homePage'])->middleware('guest');
Route::post('how-to-use', ['uses' => '\App\Http\Controllers\Api\AppApiController@howToUse'])->middleware('guest');
Route::post('app-login', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLogin'])->middleware('guest');
Route::post('app-login-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLoginOtp'])->middleware('guest');
Route::post('app-login-otp-verify', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLoginOtpVerify'])->middleware('guest');
Route::post('app-register', ['uses' => '\App\Http\Controllers\Api\AppApiController@appRegister'])->middleware('guest');
Route::post('app-register-update', ['uses' => '\App\Http\Controllers\Api\AppApiController@appRegisterUpdate'])->middleware('guest');
Route::post('forgot-password', ['uses' => '\App\Http\Controllers\Api\AppApiController@forgotPassword'])->middleware('guest');
Route::post('verify-user', ['uses' => '\App\Http\Controllers\Api\AppApiController@verifyUser'])->middleware('guest');
Route::post('verify-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@verifyOtp'])->middleware('guest');
Route::post('resend-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@resendOTP'])->middleware('guest');
Route::post('resend-reg-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@resendRegOTP'])->middleware('guest');
Route::post('reset-password', ['uses' => '\App\Http\Controllers\Api\AppApiController@resetPassword'])->middleware('guest');
Route::post('youtube-links', ['uses' => '\App\Http\Controllers\Api\AppApiController@youtubeLinks'])->middleware('guest');
Route::post('app-logs', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLogs'])->middleware('guest');
Route::post('intro-slider', ['uses' => '\App\Http\Controllers\Api\AppApiController@introSliders'])->middleware('guest');

Route::post('users-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@usersList'])->middleware('guest');
Route::post('modules-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesList'])->middleware('guest');
Route::post('modules-by-name', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesByName'])->middleware('guest');
Route::post('package-modules-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesListByPackage']);


Route::post('employee-register', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeRegister']);
Route::post('employee-list', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeList']);
Route::post('employee-type-list', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeTypeList']);
Route::post('project-employee-list', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@projectEmployeeList']);
Route::post('add-employee-to-project', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@addEmployeetoProject']);
Route::post('share-project', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@shareProject']);
Route::post('employee-details', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeDetails']);
Route::post('employee-update', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeUpdate']);
Route::post('employee-job-update', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeJobUpdate']);
Route::post('delete-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@deleteEmployee']);
Route::post('remove-project-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@removeProjectEmployee']);
Route::post('remove-projects-by-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@removeProjectsByEmployee']);

Route::post('app-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjectCategory']);
Route::post('projects-category-details', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectDetailsCategory']);
Route::post('create-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createProjectCategory']);
Route::post('delete-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteProjectCategory']);
Route::post('get-project', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProject']);
Route::post('get-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProjects']);


Route::post('sub-projects-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@subProjectsList']);
Route::post('create-sub-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createSubProjects']);
Route::post('delete-sub-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteSubProjects']);


Route::post('segments-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@segmentsList']);
Route::post('create-segments', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createSegments']);
Route::post('delete-segments', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteSegments']);
Route::post('project-status-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectStatusList']);


Route::post('app-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjects']);
Route::post('app-projects-companies', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjectsCompanies']);
Route::post('projects-details', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectDetails']);
Route::post('create-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createProject']);
Route::post('delete-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteProject']);
Route::post('leave-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@leaveProjects']);
Route::post('launch-projects-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@launchProjectLists']);
Route::post('get-employee-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getEmployeeProject']);
Route::post('get-projects-by-employee', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProjectsEmployee']);


Route::post('app-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@appWorks']);
Route::post('create-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@createWork']);
Route::post('edit-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@editWork']);
Route::post('delete-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@deleteWork']); 
Route::post('delete-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@deleteWork']); 
Route::post('delete-activity', ['uses' => '\App\Http\Controllers\Api\AppWorkController@deleteActivity']);
Route::post('select-activity', ['uses' => '\App\Http\Controllers\Api\AppWorkController@selectActivity']);

Route::post('app-costitems', ['uses' => '\App\Http\Controllers\Api\AppWorkController@appCostitems']);

//Api for Task
Route::post('create-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@createTask']);
Route::post('create-multiple-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@createMultipleTask']);
Route::post('tasks-list', ['uses' => '\App\Http\Controllers\Api\AppTaskController@appTasks']);
Route::post('edit-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@editTask']);
Route::post('delete-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@deleteTask']);
Route::post('tasks-percentage', ['uses' => '\App\Http\Controllers\Api\AppTaskController@updatePercentage']);
Route::post('tasks-comment', ['uses' => '\App\Http\Controllers\Api\AppTaskController@updateComment']);
Route::post('tasks-timelines', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskTimelines']);
Route::post('store-task-image', ['uses' => '\App\Http\Controllers\Api\AppTaskController@storeImage']);
Route::post('task-attachments', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskAttachments']);

Route::post('activitylist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@activityList']);
Route::post('boqlist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boqList']);
Route::post('boqtasklist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boqTaskList']);
Route::post('boq', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boq']);
Route::post('boqupdate', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boqUpdate']);
Route::post('costitemlist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@costItemList']);
Route::post('task-status-list', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskStatusList']);
Route::post('progress-report', ['uses' => '\App\Http\Controllers\Api\AppTaskController@progressReport']);
Route::post('progress-report-export', ['uses' => '\App\Http\Controllers\Api\AppTaskController@progressReportExport']);
Route::post('task-by-activity', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskbyActivity']);
Route::post('store-task-attachment', ['uses' => '\App\Http\Controllers\Api\AppTaskController@storeTaskAttachments']);
Route::post('delete-task-attachment', ['uses' => '\App\Http\Controllers\Api\AppTaskController@deleteTaskAttachments']);


//Api for punch item
Route::post('punch-item-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@appPunchitem']);
Route::post('create-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@createPunchitem']);
Route::post('delete-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@deletePunchitem']);
Route::post('get-type', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@getType']);
Route::post('store-punch-item-image', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@storeImage']);
Route::post('punch-item-reply', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@replyPost']);
Route::post('punch-item-reply-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@replyList']);
Route::post('punch-status-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@punchStatusList']);
Route::post('get-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@getPunchItem']);
Route::post('punch-item-status', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@punchItemStatus']);
Route::post('issues-report', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@issuesReport']);
Route::post('issues-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@issuesList']);


//Api for manpower
Route::post('man-power-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@appManpowerLog']);
Route::post('man-power-list-details', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@appManpowerLogDetails']);
Route::post('man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@manpowerLog']);
Route::post('create-man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@createManpowerLog']);
Route::post('delete-man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@deleteManpowerLog']);
Route::post('store-man-power-image', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@storeImage']);
Route::post('manpower-reply', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@replyPost']);
Route::post('manpower-reply-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@replyList']);
Route::post('manpower-report', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@reportList']);
Route::post('delete-man-power-image', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@deleteManpowerLogImage']);

//Api for category
Route::post('man-power-category-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@appManpowerCat']);
Route::post('create-man-power-category', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@createManpowerCat']);
Route::post('delete-man-power-category', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@deleteManpowerCat']);


//Api for manpower
Route::post('labour-attendance-list', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@appManpowerLog']);
Route::post('labour-attendance-list-details', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@appManpowerLogDetails']);
Route::post('labour-attendance', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@manpowerLog']);
Route::post('create-labour-attendance', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@createManpowerLog']);
Route::post('delete-labour-attendance', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@deleteManpowerLog']);
Route::post('store-labour-attendance-image', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@storeImage']);
Route::post('labour-attendance-reply', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@replyPost']);
Route::post('labour-attendance-reply-list', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@replyList']);
Route::post('labour-attendance-report', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@reportList']);
Route::post('delete-labour-attendance-image', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@deleteManpowerLogImage']);

//Api for category
Route::post('labour-attendance-category-list', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@appManpowerCat']);
Route::post('create-labour-attendance-category', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@createManpowerCat']);
Route::post('delete-labour-attendance-category', ['uses' => '\App\Http\Controllers\Api\AppLabourAttendanceController@deleteManpowerCat']);


//Api for Workers
Route::post('workers-list', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@appWorkers']);
Route::post('workers-details', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@workerDetails']);
Route::post('create-workers', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@createWorkers']);
Route::post('update-workers', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@updateWorkers']);
Route::post('delete-workers', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@deleteWorkers']);
Route::post('workers-attendance-list', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@workersAttendanceList']);
Route::post('workers-attendance', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@workerAttendance']);
Route::post('workers-attendance-post', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@workerAttendancePost']);
Route::post('delete-workers-attendance', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@deleteWorkersAttendance']);
Route::post('workers-details-list', ['uses' => '\App\Http\Controllers\Api\AppWorkersController@workersDetailsList']);
/*Route::post('test-api', function (){
    return 'hi';
});*/

//store apis
Route::post('units-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@unitsList']);
Route::post('indents-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@indentsList']);
Route::post('store-indents-image', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storeImage']);
Route::post('indents-reply', ['uses' => '\App\Http\Controllers\Api\AppStoreController@replyPost']);
Route::post('indents-reply-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@replyList']);


Route::post('product-category-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@appProductCategory']);
Route::post('product-category-details', ['uses' => '\App\Http\Controllers\Api\AppStoreController@productDetailsCategory']);
Route::post('create-product-category', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createProductCategory']);
Route::post('delete-product-category', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteProductCategory']);
Route::post('product-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@productList']);
Route::post('create-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createUnit']);
Route::post('create-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createProduct']);
Route::post('create-indent', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createIndent']);
Route::post('indent-products', ['uses' => '\App\Http\Controllers\Api\AppStoreController@indentProducts']);
Route::post('update-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateUnit']);
Route::post('delete-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteUnit']);
Route::post('update-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateProduct']);
Route::post('delete-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteproduct']);
Route::post('delete-indent', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteIndent']);
Route::post('delete-indent-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteIndentProduct']);
Route::post('update-indent-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateIndentProduct']);
Route::post('update-indent-status', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateIndentStatus']);
Route::post('stores-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storesList']);
Route::post('create-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createStore']);
Route::post('update-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateStore']);
Route::post('delete-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteStore']);
Route::post('get-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@getStore']);
Route::post('indent-report', ['uses' => '\App\Http\Controllers\Api\AppStoreController@indentReports']);
Route::post('product-detail', ['uses' => '\App\Http\Controllers\Api\AppStoreController@productDetail']);
Route::post('update-stock', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateStock']);
Route::post('delete-stock', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteStock']);
Route::post('update-opening-stock', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateOpeningStock']);
Route::post('delete-opening-stock', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteOpeningStock']);

Route::post('store-product-image', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storeProductImage']);
Route::post('store-product-category-image', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storeProductCategoryImage']);

Route::post('product-issue', ['uses' => '\App\Http\Controllers\Api\AppStoreController@issueProduct']);
Route::post('material-issue-return', ['uses' => '\App\Http\Controllers\Api\AppStoreController@issueProductReturn']);

//Indent Export
Route::post('indent-export',['uses'=>'\App\Http\Controllers\Api\AppStoreController@exportIndent']);
Route::post('stock-export',['uses'=>'\App\Http\Controllers\Api\AppStoreController@stockReportPDF']);
//End Export Indent

//members apis
Route::post('add-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addClient']);
Route::post('update-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateClient']);
Route::post('delete-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteClient']);
Route::post('add-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addContractor']);
Route::post('update-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateContractor']);
Route::post('delete-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteContractor']);
Route::post('add-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addEmployee']);
Route::post('update-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateEmployee']);
Route::post('delete-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteEmployee']);
Route::post('get-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getClient']);
Route::post('get-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getContractor']);
Route::post('get-employee', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getEmployee']);
Route::post('employee-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@employeeSearch']);
Route::post('client-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@clientSearch']);
Route::post('contractor-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@contractorSearch']);
Route::post('delete-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteMember']);
Route::post('update-user-profile', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateUserProfile']);
Route::post('get-user-profile', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getUserProfile']);
Route::post('get-members', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getMembers']);
Route::post('save-permission', ['uses' => '\App\Http\Controllers\Api\AppMemberController@savePermission']);
Route::post('remove-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@removeMember']);
Route::post('send-notification', ['uses' => '\App\Http\Controllers\Api\AppMemberController@sendNotification']);

Route::post('add-product-issue', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@store']);
Route::post('edit-product-issue', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@edit']);
Route::post('edit-product-issue-single', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@editSingle']);
Route::post('list-issues', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@issueList']);
Route::post('list-product-issue', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@issueProductsList']);
Route::post('delete-product-issue', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@destroy']);
Route::post('store-issue-image', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@storeImage']);
Route::post('delete-issue-image', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@deleteStoreImage']);
Route::post('issue-item-reply', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@issueItemReply']);
Route::post('issue-reply-list', ['uses' => '\App\Http\Controllers\Api\ApiProductIssue@issueReplyList']);

Route::post('add-product-issue-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@store']);
Route::post('edit-product-issue-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@edit']);
Route::post('list-issues-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@issueList']);
Route::post('list-product-issue-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@issueProductsList']);
Route::post('delete-product-issue-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@destroy']);
Route::post('product-issue-return-detail', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@show']);
Route::post('store-issue-image-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@storeImage']);
Route::post('delete-issue-image-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@deleteStoreImage']);
Route::post('issue-item-reply-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@issueItemReply']);
Route::post('issue-reply-list-return', ['uses' => '\App\Http\Controllers\Api\ApiProductIssueReturn@issueReplyList']);

Route::post('invite-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@inviteMember']);
Route::post('add-noninvited-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addNoninvitedMember']);

//Permission Role APIs
Route::post('check-permission', ['uses' => '\App\Http\Controllers\Api\AppApiController@checkPermission']);
Route::post('get-all-permissions', ['uses' => '\App\Http\Controllers\Api\AppApiController@getAllPermissions']);
Route::post('get-permissions-by-module', ['uses' => '\App\Http\Controllers\Api\AppApiController@getPermissionsByModule']);
Route::post('get-all-user-permissions', ['uses' => '\App\Http\Controllers\Api\AppApiController@getAllUserPermissions']);
Route::post('get-permission-by-user', ['uses' => '\App\Http\Controllers\Api\AppApiController@getPermissionByUser']);
Route::post('get-projects-check-permission', ['uses' => '\App\Http\Controllers\Api\AppApiController@getProjectCheckPermission']);
Route::post('get-roles', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getRoles']);
Route::post('get-permissions-by-role', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getPermissionsByRole']);
Route::post('save-permissions-by-role', ['uses' => '\App\Http\Controllers\Api\AppMemberController@savePermissionsByRole']);
Route::post('get-projects-permissions-roles', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getProjectsPermissionsRoles']);

Route::post('folder-list', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@folderLists']);
Route::post('photos-list', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@photosLists']);
Route::post('images-list', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@imagesLists']);
Route::post('copy-file', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@copyFile']);
Route::post('move-pathtype', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@movePathType']);
Route::post('store-link', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@storeLink']);
Route::post('create-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@createFolder']);
Route::post('edit-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@editFolder']);
Route::post('download-photo', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@download']);
Route::post('multiple-upload', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@storeMultiple']);
Route::post('photos-store', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@photosStore']);
Route::post('update-filemanagername', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@updateFileManagerName']);
Route::post('delete-photo', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@deletePhoto']);
Route::post('delete-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@deleteFolder']);
Route::post('move-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@moveFolder']);
Route::post('remove-file', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@removeFile']);

Route::post('pusher-app-keys', ['uses' => '\App\Http\Controllers\Api\AppChatController@pusherAppKeys']);
Route::post('conversations-list', ['uses' => '\App\Http\Controllers\Api\AppChatController@conversationList']);
Route::post('timeline-logs', ['uses' => '\App\Http\Controllers\Api\AppChatController@timeLineLogs']);
Route::post('mention-logs', ['uses' => '\App\Http\Controllers\Api\AppChatController@mentionLogs']);
Route::post('members-list', ['uses' => '\App\Http\Controllers\Api\AppChatController@membersList']);
Route::post('create-conversation', ['uses' => '\App\Http\Controllers\Api\AppChatController@createConversation']);
Route::post('chat-messages', ['uses' => '\App\Http\Controllers\Api\AppChatController@chatMessages']);
Route::post('send-message', ['uses' => '\App\Http\Controllers\Api\AppChatController@sendMessage']);
Route::post('project-pulse-details', ['uses' => '\App\Http\Controllers\Api\AppChatController@projectPulseDetails']);
Route::post('project-pulse-messages', ['uses' => '\App\Http\Controllers\Api\AppChatController@projectPulseMessages']);
Route::post('project-pulse-send-message', ['uses' => '\App\Http\Controllers\Api\AppChatController@projectPulseSendMessages']);
Route::post('create-group-chat', ['uses' => '\App\Http\Controllers\Api\AppChatController@createGroupChat']); 
Route::post('group-chat-details', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatDetails']); 
Route::post('group-chat-messages', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatMessages']);
Route::post('group-chat-send-message', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatSendMessages']);
Route::post('group-chat-add-member', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatAddMember']);
Route::post('group-chat-member-list', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatMemberList']); 
Route::post('group-chat-exit', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatMemberExit']);
Route::post('group-chat-delete', ['uses' => '\App\Http\Controllers\Api\AppChatController@GroupChatMemberDelete']); 
Route::post('channel-list', ['uses' => '\App\Http\Controllers\Api\AppChannelController@appChannels']);
Route::post('create-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@createChannel']);
Route::post('update-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@updateChannel']);
Route::post('delete-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@deleteChannel']);
Route::post('channel-users-list', ['uses' => '\App\Http\Controllers\Api\AppChannelController@channelUsersList']);
Route::post('add-channel-members', ['uses' => '\App\Http\Controllers\Api\AppChannelController@addChannelMembers']);
Route::post('remove-channel-members', ['uses' => '\App\Http\Controllers\Api\AppChannelController@removeChannelMembers']);
Route::post('set-channel-admin', ['uses' => '\App\Http\Controllers\Api\AppChannelController@setChannelAdmin']);
//attendance api
Route::post('attendance', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@create']);
Route::post('employee-attendance-list', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@index']);
Route::post('post-attendance', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@store']);
Route::post('group-attendance-post', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@groupAttendance']);
Route::post('groupattendance-employee-list', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@groupAttendanceList']);
Route::post('attendance-post-absent', ['uses' => '\App\Http\Controllers\Api\ApiAttendanceController@AttendancePostAbsent']);
//GRN api
Route::post('create-grn', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@createGrn']);
Route::post('convert-grn', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@postConvertGrn']);
Route::post('edit-convert-grn', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@editConvertGrn']);
Route::post('edit-grn', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@editGrn']);
Route::post('delete-grn', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@deleteGrn']);
Route::post('store-grn-files', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@piStoreImage']);
Route::post('grn-list', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@grnList']);
Route::post('grn-detail', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@grnDetail']);
Route::post('grn-return', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@grnReturn']);
Route::post('grn-return-detail', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@grnReturnDetails']);
Route::post('edit-grn-return', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@grnReturnEdit']);
Route::post('grn-return-delete', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@deleteReturnGrn']);
Route::post('grn-image-delete', ['uses' => '\App\Http\Controllers\Api\ApiGrnController@deleteImageGrn']);
//Promotional FCM
Route::post('promotional-fcm', ['uses' => '\App\Http\Controllers\Api\AppUserController@validateFcm']);
//supplier
Route::post('create-supplier','\App\Http\Controllers\Api\ApiSupplierController@store');
Route::post('update-supplier','\App\Http\Controllers\Api\ApiSupplierController@update');
Route::post('supplier-list','\App\Http\Controllers\Api\ApiSupplierController@index');
Route::post('delete-supplier','\App\Http\Controllers\Api\ApiSupplierController@destroy');

Route::post('income-type-list', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeTypeList']);

Route::post('income-group-list', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeGroupList']);
Route::post('income-group-item', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeGroupItem']);
Route::post('income-group-add', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeGroupAdd']);
Route::post('income-group-update', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeGroupUpdate']);
Route::post('income-group-delete', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeGroupDelete']);

Route::post('income-name-list', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeNameList']);
Route::post('income-name-item', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeNameItem']);
Route::post('income-name-add', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeNameAdd']);
Route::post('income-name-update', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeNameUpdate']);
Route::post('income-name-delete', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@incomeNameDelete']);
Route::post('store-income-name-image', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@storeImage']);


Route::post('payment-voucher-list', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@paymentVoucherList']);
Route::post('payment-voucher-add', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@paymentVoucherAdd']);
Route::post('payment-voucher-update', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@paymentVoucherUpdate']);
Route::post('payment-voucher-delete', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@paymentVoucherDelete']);
Route::post('payment-voucher-item', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@paymentVoucherItem']);
Route::post('store-payment-voucher-image', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@storePaymentImage']);
Route::post('delete-payment-voucher-image', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@deletePaymentImage']);

Route::post('ledger-payments', ['uses' => '\App\Http\Controllers\Api\ApiAccountsController@ledgerPayments']);