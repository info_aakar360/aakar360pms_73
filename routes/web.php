<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteSertviceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', ['uses' => 'Auth\LoginController@logout']);
Route::post('/consent/remove-lead-request', ['uses' => 'PublicLeadGdprController@removeLeadRequest'])->name('front.gdpr.remove-lead-request');
Route::post('/consent/l/update/{lead}', ['uses' => 'PublicLeadGdprController@updateConsent'])->name('front.gdpr.consent.update');
Route::post('/consent/l/update/{lead}', ['uses' => 'PublicLeadGdprController@updateConsent'])->name('front.gdpr.consent.update');
Route::get('/consent/l/{lead}', ['uses' => 'PublicLeadGdprController@consent'])->name('front.gdpr.consent');
Route::post('/forms/l/update/{lead}', ['uses' => 'PublicLeadGdprController@updateLead'])->name('front.gdpr.lead.update');
Route::get('/forms/l/{lead}', ['uses' => 'PublicLeadGdprController@lead'])->name('front.gdpr.lead');
Route::get('/contract/{id}', ['uses' => 'Front\PublicUrlController@contractView'])->name('front.contract.show');
Route::get('/contract/download/{id}', ['uses' => 'Front\PublicUrlController@contractDownload'])->name('front.contract.download');
Route::get('contract/sign-modal/{id}', ['uses' => 'Front\PublicUrlController@contractSignModal'])->name('front.contract.sign-modal');
Route::post('contract/sign/{id}', ['uses' => 'Front\PublicUrlController@contractSign'])->name('front.contract.sign');
Route::get('/estimate/{id}', ['uses' => 'Front\PublicUrlController@estimateView'])->name('front.estimate.show');
Route::post('/estimate/decline/{id}', ['uses' => 'Front\PublicUrlController@decline'])->name('front.estimate.decline');
Route::get('/estimate/accept/{id}', ['uses' => 'Front\PublicUrlController@acceptModal'])->name('front.estimate.accept');
Route::post('/estimate/accept/{id}', ['uses' => 'Front\PublicUrlController@accept'])->name('front.accept-estimate');
Route::get('/estimate/download/{id}', ['uses' => 'Front\PublicUrlController@estimateDownload'])->name('front.estimateDownload');
Route::get('/invoice/download/{id}', ['uses' => 'Front\HomeController@downloadInvoice'])->name('front.invoiceDownload');
Route::get('/invoice/{id}', ['uses' => '\App\Http\Controllers\Front\HomeController@invoice'])->name('front.invoice');
Route::get('page/{slug?}', ['uses' => '\App\Http\Controllers\Front\HomeController@page'])->name('front.page');
Route::get('/gantt-chart-data/{id}', ['uses' => 'Front\HomeController@ganttData'])->name('front.gantt-data');
Route::get('/gantt-chart/{id}', ['uses' => 'Front\HomeController@gantt'])->name('front.gantt');
Route::get('/privacy-policy/', ['uses' => 'Front\HomeController@privacyPolicy'])->name('front.privacyPolicy');
Route::post('public/pay-with-razorpay', array('as' => 'public.pay-with-razorpay','uses' => 'Client\RazorPayController@payWithRazorPay',));

Route::match(['get','post'],'/',['uses' => 'Auth\LoginController@showLoginForm'])->name('auth.login') ;

Route::get('/signup', ['uses' => '\App\Http\Controllers\Auth\RegisterController@index'])->name('signup');
Route::post('/sendotp', ['uses' => '\App\Http\Controllers\Auth\RegisterController@sendotp'])->name('sendotp');
Route::post('/postregister', ['uses' => '\App\Http\Controllers\Auth\RegisterController@store'])->name('postregister');
Route::get('/forgot-password', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@forgotPassword'])->name('forgot-password');
Route::post('/post-forgot-password', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@postForgotPassword'])->name('post-forgot-password');

Route::get('/validate-otp', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@otpPage'])->name('validate-otp');
Route::post('/post-validate-otp', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@postOtpPassword'])->name('post-validate-otp');

Route::get('/reset-password', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@resetPassword'])->name('reset-password');
Route::post('/post-reset-password', ['uses' => '\App\Http\Controllers\Auth\ForgotPasswordController@postResetPassword'])->name('post-reset-password');

Route::group(
    ['namespace' => 'Front', 'as' => 'front.'], function () {
    Route::post('/contact-us', 'HomeController@contactUs')->name('contact-us');
   /* Route::resource('/signup', 'RegisterController', ['only' => ['index', 'store']]);*/
    Route::get('/email-verification/{code}', 'RegisterController@getEmailVerification')->name('get-email-verification');
    Route::get('api/v1/app', ['uses' => 'HomeController@app'])->name('api.app');
    Route::get('language/{lang}', ['as' => 'language.lang', 'uses' => 'HomeController@changeLanguage']);
    Route::get('submit-quotation/{rfqId}/{supplierId}', ['uses' => 'HomeController@submitQuotation'])->name('submitQuotation');
    Route::get('view-po/{poId}/{supplierId?}', ['uses' => 'HomeController@viewPO'])->name('viewPO');
    Route::patch('submit-quotation/{rfqId}/{supplierId}', ['uses' => 'HomeController@postQuotation'])->name('postQuotation');
    Route::get('rfq/success', ['uses' => 'HomeController@rfqSuccess'])->name('rfq.success');
    Route::get('tender-contract/{id}/{uniqueid}', ['uses' => 'HomeController@tenderContract'])->name('tenderContract');
    Route::post('contractor_bidding_product', ['uses' => 'HomeController@contractorBiddingPrice'])->name('contractor_bidding_product');
    Route::post('biddingloop', ['uses' => 'HomeController@tenderBiddingLoop'])->name('biddingloop');

});

Route::group(
    ['namespace' => 'Client', 'prefix' => 'client', 'as' => 'client.'], function () {

    Route::post('stripe/{invoiceId}', array('as' => 'stripe', 'uses' => 'StripeController@paymentWithStripe',));
    Route::post('stripe-public/{invoiceId}', array('as' => 'stripe-public', 'uses' => 'StripeController@paymentWithStripePublic',));
    // route for post request
    Route::get('paypal-public/{invoiceId}', array('as' => 'paypal-public', 'uses' => 'PaypalController@paymentWithpaypalPublic',));
    Route::get('paypal/{invoiceId}', array('as' => 'paypal', 'uses' => 'PaypalController@paymentWithpaypal',));
    // route for check status responce
    Route::get('paypal', array('as' => 'status', 'uses' => 'PaypalController@getPaymentStatus',));
    Route::get('paypal-recurring', array('as' => 'paypal-recurring','uses' => 'PaypalController@payWithPaypalRecurrring',));
});

 //Paypal IPN
Route::post('verify-ipn', array('as' => 'verify-ipn','uses' => 'PaypalIPNController@verifyIPN'));
Route::post('verify-billing-ipn', array('as' => 'verify-billing-ipn','uses' => 'PaypalIPNController@verifyBillingIPN'));
Route::post('/verify-webhook', ['as' => 'verify-webhook', 'uses' => 'StripeWebhookController@verifyStripeWebhook']);
Route::post('/save-invoices', ['as' => 'save_webhook', 'uses' => 'StripeWebhookController@saveInvoices']);
Route::post('/save-razorpay-invoices', ['as' => 'save_razorpay-webhook', 'uses' => 'RazorpayWebhookController@saveInvoices']);
Route::get('/check-razorpay-invoices', ['as' => 'check_razorpay-webhook', 'uses' => 'RazorpayWebhookController@checkInvoices']);

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    // Super admin routes
    Route::group(
        ['namespace' => 'SuperAdmin', 'prefix' => 'super-admin', 'as' => 'super-admin.', 'middleware' => ['super-admin']], function () {

        Route::get('/dashboard', 'SuperAdminDashboardController@index')->name('dashboard');
        Route::post('profile/updateOneSignalId', ['uses' => 'SuperAdminProfileController@updateOneSignalId'])->name('profile.updateOneSignalId');
        Route::resource('/profile', 'SuperAdminProfileController', ['only' => ['index', 'update']]);

        // Faq routes
        Route::resource('/faq-category/{category}/faq', 'SuperAdminFaqController')->except(['index', 'show']);

        // Faq Category routes
        Route::get('faq-category/data', ['uses' => 'SuperAdminFaqCategoryController@data'])->name('faq-category.data');
        Route::resource('/faq-category', 'SuperAdminFaqCategoryController');

        Route::post('intro-slider/storeImage', ['uses' => 'SuperAdminIntroSliderController@storeImage'])->name('intro-slider.storeImage');

        Route::resource('intro-slider', 'SuperAdminIntroSliderController');

        // Packages routes
        Route::get('packages/data', ['uses' => 'SuperAdminPackageController@data'])->name('packages.data');
        Route::resource('/packages', 'SuperAdminPackageController');

        // Companies routes
        Route::get('companies/data', ['uses' => 'SuperAdminCompanyController@data'])->name('companies.data');
        Route::get('companies/usage', ['uses' => 'SuperAdminCompanyController@usage'])->name('companies.usage');
        Route::post('companies/weblogin', ['uses' => 'SuperAdminCompanyController@weblogin'])->name('companies.weblogin');
        Route::get('companies/usagedata', ['uses' => 'SuperAdminCompanyController@usagedata'])->name('companies.usagedata');

        Route::get('companies/un-registered-users', ['uses' => 'SuperAdminCompanyController@unRegUsers'])->name('companies.un-reg-users');
        Route::get('companies/un-registered-users-data', ['uses' => 'SuperAdminCompanyController@unRegUsersData'])->name('companies.un-reg-users-data');

        Route::get('companies/editPackage/{companyId}', ['uses' => 'SuperAdminCompanyController@editPackage'])->name('companies.edit-package.get');
        Route::put('companies/editPackage/{companyId}', ['uses' => 'SuperAdminCompanyController@updatePackage'])->name('companies.edit-package.post');
        Route::post('/companies', ['uses' => 'SuperAdminCompanyController@store']);

        Route::resource('/companies', 'SuperAdminCompanyController');
        Route::get('invoices/data', ['uses' => 'SuperAdminInvoiceController@data'])->name('invoices.data');
        Route::resource('/invoices', 'SuperAdminInvoiceController', ['only' => ['index']]);
        Route::get('paypal-invoice-download/{id}', array('as' => 'paypal.invoice-download','uses' => 'SuperAdminInvoiceController@paypalInvoiceDownload',));
        Route::get('billing/invoice-download/{invoice}', 'SuperAdminInvoiceController@download')->name('stripe.invoice-download');
        Route::get('billing/razorpay-download/{invoice}', 'SuperAdminInvoiceController@razorpayInvoiceDownload')->name('razorpay.invoice-download');
        Route::get('billing/offline-download/{invoice}', 'SuperAdminInvoiceController@offlineInvoiceDownload')->name('offline.invoice-download');

        Route::resource('/settings', 'SuperAdminSettingsController', ['only' => ['index', 'update']]);

        Route::get('super-admin/data', ['uses' => 'SuperAdminController@data'])->name('super-admin.data');
        Route::resource('/super-admin', 'SuperAdminController');

        Route::get('offline-plan/data', ['uses' => 'OfflinePlanChangeController@data'])->name('offline-plan.data');
        Route::post('offline-plan/verify', ['uses' => 'OfflinePlanChangeController@verify'])->name('offline-plan.verify');
        Route::post('offline-plan/reject', ['uses' => 'OfflinePlanChangeController@reject'])->name('offline-plan.reject');
        Route::resource('/offline-plan', 'OfflinePlanChangeController', ['only' => ['index', 'update']]);
        Route::group(
            ['prefix' => 'settings'],
            function () {
                Route::get('email-settings/sent-test-email', ['uses' => 'SuperAdminEmailSettingsController@sendTestEmail'])->name('email-settings.sendTestEmail');
                Route::resource('/email-settings', 'SuperAdminEmailSettingsController', ['only' => ['index', 'update']]);
                Route::post('/stripe-method-change', 'SuperAdminStripeSettingsController@changePaymentMethod')->name('stripe.method-change');
                Route::get('offline-payment-setting/createModal', ['uses' => 'OfflinePaymentSettingController@createModal'])->name('offline-payment-setting.createModal');
                Route::get('offline-payment/method', ['uses' => 'OfflinePaymentSettingController@offlinePaymentMethod'])->name('offline-payment-method.create');
                Route::resource('offline-payment-setting', 'OfflinePaymentSettingController');
                Route::resource('/stripe-settings', 'SuperAdminStripeSettingsController', ['only' => ['index', 'update']]);

                Route::get('push-notification-settings/sent-test-notification', ['uses' => 'SuperAdminPushSettingsController@sendTestEmail'])->name('push-notification-settings.sendTestEmail');
                Route::get('push-notification-settings/sendTestNotification', ['uses' => 'SuperAdminPushSettingsController@sendTestNotification'])->name('push-notification-settings.sendTestNotification');
                Route::resource('/push-notification-settings', 'SuperAdminPushSettingsController', ['only' => ['index', 'update']]);

                Route::get('currency/exchange-key', ['uses' => 'SuperAdminCurrencySettingController@currencyExchangeKey'])->name('currency.exchange-key');
                Route::post('currency/exchange-key-store', ['uses' => 'SuperAdminCurrencySettingController@currencyExchangeKeyStore'])->name('currency.exchange-key-store');
                Route::resource('currency', 'SuperAdminCurrencySettingController');
                Route::get('currency/exchange-rate/{currency}', ['uses' => 'SuperAdminCurrencySettingController@exchangeRate'])->name('currency.exchange-rate');
                Route::get('currency/update/exchange-rates', ['uses' => 'SuperAdminCurrencySettingController@updateExchangeRate'])->name('currency.update-exchange-rates');
                Route::resource('currency', 'SuperAdminCurrencySettingController');

                Route::post('update-settings/deleteFile', ['uses' => 'UpdateDatabaseController@deleteFile'])->name('update-settings.deleteFile');
                Route::get('update-settings/install', ['uses' => 'UpdateDatabaseController@install'])->name('update-settings.install');
                Route::get('update-settings/manual-update', ['uses' => 'UpdateDatabaseController@manual'])->name('update-settings.manual');
                Route::resource('update-settings', 'UpdateDatabaseController');

                // Language Settings
                Route::post('language-settings/update-data/{id?}', ['uses' => 'SuperAdminLanguageSettingsController@updateData'])->name('language-settings.update-data');
                Route::resource('language-settings', 'SuperAdminLanguageSettingsController');

                Route::resource('front-settings', 'SuperAdminFrontSettingController', ['only' => ['index', 'update']]);
                Route::resource('package-settings', 'SuperAdminPackageSettingController', ['only' => ['index', 'update']]);

                Route::resource('feature-settings', 'SuperAdminFeatureSettingController');
                Route::resource('footer-settings', 'SuperAdminFooterSettingController');

            }
        );

        Route::post('activity/store-cat', ['uses' => 'BoqCategoryController@storeCat'])->name('activity.store-cat');
        Route::get('activity/create-cat', ['uses' => 'BoqCategoryController@createCat'])->name('activity.create-cat');
        Route::get('activity/edit-cat/{id}', ['uses' => 'BoqCategoryController@editCat'])->name('activity.edit-cat');
        Route::post('activity/update-cat/{id}', ['uses' => 'BoqCategoryController@updateCat'])->name('activity.update-cat');
        Route::resource('activity', 'BoqCategoryController');

        Route::post('task/store-cat', ['uses' => 'ManageCostItemsController@storeCat'])->name('task.store-cat');
        Route::get('task/create-cat', ['uses' => 'ManageCostItemsController@createCat'])->name('task.create-cat');
        Route::get('task/edit-cat/{id}', ['uses' => 'ManageCostItemsController@editCat'])->name('task.edit-cat');
        Route::post('task/update-cat/{id}', ['uses' => 'ManageCostItemsController@updateCat'])->name('task.update-cat');
        Route::post('task/get-cost', ['uses' => 'ManageCostItemsController@getCost'])->name('task.getCost');
        Route::match(['get','post'],'task/product-add/{id}', ['uses' => 'ManageCostItemsController@productAdd'])->name('task.product-add');
        Route::match(['get','post'],'task/product-edit/{id}', ['uses' => 'ManageCostItemsController@productEdit'])->name('task.product-edit');
        Route::post('task/update-product/{id}', ['uses' => 'ManageCostItemsController@productUpdate'])->name('task.update-product');
        Route::delete('task/destroy-product/{id}', ['uses' => 'ManageCostItemsController@destroyProduct'])->name('task.destroy-product');
        //Route::get('task/data', ['uses' => 'CostItemsController@data'])->name('task.data');
        //Route::get('task/data', ['as' => 'task.data', 'uses' => 'CostItemsController@data']);

       /* Route::post('task/store-product/{id}', ['uses' => 'ManageCostItemsController@storeProduct'])->name('task.store-product');
        Route::post('task/name-store', ['uses' => 'CostItemsController@nameStore'])->name('task.namestore');*/
        Route::resource('task', 'ManageCostItemsController');

        Route::get('product-category/index', ['uses' => 'ManageProductCategoryController@index'])->name('productCategory.index');
        Route::resource('product-category', 'ManageProductCategoryController');

        Route::get('product-brand/index', ['uses' => 'ManageProductBrandController@index'])->name('productBrand.index');
        Route::resource('product-brand', 'ManageProductBrandController');

        Route::get('manage-modules/assign-permissions', ['uses' => 'SuperAdminManageModuleController@assignPermissions'])->name('manageModules.assignPermissions');
        Route::post('manage-modules/store-assign-permissions', ['uses' => 'SuperAdminManageModuleController@storeAssignPermissions'])->name('manageModules.storeAssignPermissions');
        Route::delete('manage-modules/remove-permissions/{id}', ['uses'=> 'SuperAdminManageModuleController@removePermissions'])->name('manageModules.removePermissions');
        Route::match(['get','post'],'manage-modules/get-permissions', ['uses' => 'SuperAdminManageModuleController@getPermissions'])->name('manageModules.getPermissions');
        Route::resource('manageModules', 'SuperAdminManageModuleController');

        Route::get('app-notification', ['uses' => 'SuperAdminController@appNotification'])->name('appNotification');
        Route::post('app-notification/send-notification', ['uses' => 'SuperAdminController@sendNotification'])->name('appNotification.sendNotification');
        //Promotional Notification
        Route::get('promotional-notification', ['uses' => 'SuperAdminController@promotionalNotification'])->name('promotionalNotification');
        Route::post('promotional-notification/send-notification', ['uses' => 'SuperAdminController@promotionalSendNotification'])->name('promotionalNotification.sendNotification');
    });

    // Admin routes
    Route::group(
        ['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['admin']],
        function () {

            Route::get('workers/workerdData', ['uses' => 'ManageWorkersController@workerdData'])->name('workers.workerData');

            Route::group(['middleware' => ['account-setup', 'license-expire']], function () {

                Route::match(['get','post'],'/dashboard', 'AdminDashboardController@index')->name('dashboard');
                Route::post('/dashboard/widget', 'AdminDashboardController@widget')->name('dashboard.widget');

                Route::get('designations/quick-create', ['uses' => 'ManageDesignationController@quickCreate'])->name('designations.quick-create');
                Route::post('designations/quick-store', ['uses' => 'ManageDesignationController@quickStore'])->name('designations.quick-store');
                Route::get('designations/data', ['uses' => 'ManageDesignationController@getDesignationData'])->name('designations.getDesignationData');
                Route::resource('designations', 'ManageDesignationController');
                // Location
                Route::post('location/store-cat', ['uses' => 'ManageLocationController@storeCat'])->name('location.store-cat');
                Route::get('location/create-cat', ['uses' => 'ManageLocationController@createCat'])->name('location.create-cat');
                Route::get('location/edit-cat/{id}', ['uses' => 'ManageLocationController@editCat'])->name('location.edit-cat');
                Route::post('location/update-cat/{id}', ['uses' => 'ManageLocationController@updateCat'])->name('location.update-cat');
                Route::post('location/get-city/{id}', ['uses' => 'ManageLocationController@getCity'])->name('location.get-city');
                Route::resource('location', 'ManageLocationController');
                Route::post('location/store-sub-cat', ['uses' => 'ManageLocationController@storesubCat'])->name('location.store-sub-cat');
                Route::get('location/sub-edit/{id}', ['uses' => 'ManageLocationController@editsubCat'])->name('location.sub-edit');
                Route::post('location/sub-update/{id}', ['uses' => 'ManageLocationController@updatesubCat'])->name('location.sub-update');
                Route::post('location/delete-sub-cat/{id}', ['uses' => 'ManageLocationController@deletesubCat'])->name('location.delete-sub-cat');
                Route::get('location/location-edit/{id}', ['uses' => 'ManageLocationController@editLocation'])->name('location.location-edit');
                Route::post('location/location-update/{id}', ['uses' => 'ManageLocationController@updateLocation'])->name('location.location-update');
                Route::post('location/delete-location/{id}', ['uses' => 'ManageLocationController@deleteLocation'])->name('location.delete-location');

                //Asset
                Route::post('asset-category/create-asset-cat', ['uses' => 'AssetCategoryController@createassetCat'])->name('asset-category.create-asset-cat');
                Route::get('asset-category/create-asset-edit/{id}', ['uses' => 'AssetCategoryController@editassetCat'])->name('asset-category.edit-asset-cat');
                Route::post('asset-category/update-asset-edit/{id}', ['uses' => 'AssetCategoryController@updateassetCat'])->name('asset-category.update-asset-cat');
                Route::get('asset-category/assets', ['uses' => 'AssetCategoryController@assets'])->name('asset-category.assets');
                Route::post('asset-category/create-asset', ['uses' => 'AssetCategoryController@createAsset'])->name('asset-category.create-asset');
                Route::get('asset-category/edit-asset/{id}', ['uses' => 'AssetCategoryController@editAsset'])->name('asset-category.edit-asset');
                Route::post('asset-category/update-asset/{id}', ['uses' => 'AssetCategoryController@updateAsset'])->name('asset-category.update-asset');
                Route::post('location/delete-asset/{id}', ['uses' => 'AssetCategoryController@deleteAsset'])->name('asset-category.delete-asset');
                Route::get('asset-category/sub-asset', ['uses' => 'AssetCategoryController@subAsset'])->name('asset-category.sub-asset');
                Route::post('asset-category/create-sub-asset', ['uses' => 'AssetCategoryController@createSubAsset'])->name('asset-category.create-sub-asset');
                Route::get('asset-category/edit-sub-asset/{id}', ['uses' => 'AssetCategoryController@editSubAsset'])->name('asset-category.edit-sub-asset');
                Route::post('asset-category/update-sub-asset/{id}', ['uses' => 'AssetCategoryController@updateSubAsset'])->name('asset-category.update-sub-asset');
                Route::post('location/delete-sub-asset/{id}', ['uses' => 'AssetCategoryController@deleteSubAsset'])->name('asset-category.delete-sub-asset');

                Route::resource('asset-category', 'AssetCategoryController');

                // Work Request and Work Order
                Route::get('work-order', ['uses' => 'WorkRequestController@index'])->name('work-order');
                Route::get('work-order-recurring', ['uses' => 'WorkRequestController@recurring'])->name('work-order.recurring');
                Route::get('work-order/all', ['uses' => 'WorkRequestController@allWorkOrder'])->name('work-order.allworkorder');

                Route::get('work-order/editworkorder/{id}', ['uses' => 'WorkRequestController@editOrder'])->name('work-order.editworkorder');


                Route::get('work-order/timer/{status}/{value}', ['uses' => 'WorkRequestController@timer'])->name('work-order.timer');


                Route::get('work-order/wostore/{id}', ['uses' => 'WorkRequestController@woStore'])->name('work-order.wostore');
                Route::post('work-order/wostoreupdate', ['uses' => 'WorkRequestController@woStoreUpdate'])->name('work-order.wostore.update');

                Route::post('work-order/woupdate/{id}', ['uses' => 'WorkRequestController@woUpdate'])->name('work-order.woupdate');
                Route::match(['get','post'],'work-order/changestatus/{id}/{status}', ['uses' => 'WorkRequestController@changeStatus'])->name('work-order.changestatus');

                Route::get('work-order/storeImage', ['uses' => 'WorkRequestController@storeImage'])->name('work-order.storeImage');

                Route::get('create-work-request', ['uses' => 'WorkRequestController@createWR'])->name('create-work-request');
                Route::post('work-request/add', ['uses' => 'WorkRequestController@addWorkRequest'])->name('work-request.add');
                Route::get('work-order/edit/{id}', ['uses' => 'WorkRequestController@editWorkOrder'])->name('work-order.edit');
                Route::post('work-order/update/{id}', ['uses' => 'WorkRequestController@updateWorkOrder'])->name('work-order.update');
                Route::post('work-order/delete/{id}', ['uses' => 'WorkRequestController@deleteWorkOrder'])->name('work-order.delete');
                Route::get('work-order/status/{id}', ['uses' => 'WorkRequestController@statusWorkOrder'])->name('work-order.status');
                Route::get('work-order/assign/{id}', ['uses' => 'WorkRequestController@assignWorkOrder'])->name('work-order.assign');
                Route::post('work-order/status-update/{id}', ['uses' => 'WorkRequestController@statusUpdateWorkOrder'])->name('work-order.status-update');
                Route::post('work-order/assign-update/{id}', ['uses' => 'WorkRequestController@assignUpdateWorkOrder'])->name('work-order.assign-update');
                Route::post('work-order/get-sub-location/{id}', ['uses' => 'WorkRequestController@getSubLocation'])->name('work-order.get-sub-location');
                Route::resource('asset-category', 'AssetCategoryController');

                // FAQ
                Route::get('faqs/{id}', ['uses' => 'FaqController@details'])->name('faqs.details');
                Route::get('faqs', ['uses' => 'FaqController@index'])->name('faqs.index');


                Route::get('clients/export/{status?}/{client?}', ['uses' => 'ManageClientsController@export'])->name('clients.export');
                Route::get('clients/data', ['uses' => 'ManageClientsController@data'])->name('clients.data');
                Route::resource('clients', 'ManageClientsController', ['expect' => ['create']]);

                Route::resource('workers', 'ManageWorkersController');
                Route::post('workers/get-contractors', 'ManageWorkersController@getContractors')->name('workers.get-contractors');
                Route::post('workers/workerlist', 'ManageWorkersController@workerList')->name('workers.workerlist');


                Route::get('suppliers/export/{status?}/{supplier?}', ['uses' => 'ManageSuppliersController@export'])->name('suppliers.export');
                Route::get('suppliers/data', ['uses' => 'ManageSuppliersController@data'])->name('suppliers.data');
                Route::resource('suppliers', 'ManageSuppliersController', ['expect' => ['create']]);

                Route::get('contractors/export/{status?}/{contractor?}', ['uses' => 'ManageContractorsController@export'])->name('contractors.export');
                Route::get('all-time-logscontractors/data', ['uses' => 'ManageContractorsController@data'])->name('contractors.data');
                Route::resource('contractors', 'ManageContractorsController', ['expect' => ['create']]);

                Route::get('stores/export/{status?}/{store?}', ['uses' => 'ManageStoresController@export'])->name('stores.export');
                Route::get('stores/data/{id?}', ['uses' => 'ManageStoresController@data'])->name('stores.data');
                Route::resource('stores','ManageStoresController', ['expect' => ['create']]);


                Route::get('leads/gdpr/{leadID}', ['uses' => 'LeadController@gdpr'])->name('leads.gdpr');
                Route::get('leads/export/{followUp?}/{client?}', ['uses' => 'LeadController@export'])->name('leads.export');
                Route::get('leads/data', ['uses' => 'LeadController@data'])->name('leads.data');
                Route::post('leads/change-status', ['uses' => 'LeadController@changeStatus'])->name('leads.change-status');
                Route::get('leads/follow-up/{leadID}', ['uses' => 'LeadController@followUpCreate'])->name('leads.follow-up');
                Route::get('leads/followup/{leadID}', ['uses' => 'LeadController@followUpShow'])->name('leads.followup');
                Route::post('leads/follow-up-store', ['uses' => 'LeadController@followUpStore'])->name('leads.follow-up-store');
                Route::get('leads/follow-up-edit/{id?}', ['uses' => 'LeadController@editFollow'])->name('leads.follow-up-edit');
                Route::post('leads/follow-up-update', ['uses' => 'LeadController@UpdateFollow'])->name('leads.follow-up-update');
                Route::get('leads/follow-up-sort', ['uses' => 'LeadController@followUpSort'])->name('leads.follow-up-sort');
                Route::post('leads/save-consent-purpose-data/{lead}', ['uses' => 'LeadController@saveConsentLeadData'])->name('leads.save-consent-purpose-data');
                Route::get('leads/consent-purpose-data/{lead}', ['uses' => 'LeadController@consentPurposeData'])->name('leads.consent-purpose-data');
                Route::resource('leads', 'LeadController');

                // Lead Files
                Route::get('lead-files/download/{id}', ['uses' => 'LeadFilesController@download'])->name('lead-files.download');
                Route::get('lead-files/thumbnail', ['uses' => 'LeadFilesController@thumbnailShow'])->name('lead-files.thumbnail');
                Route::resource('lead-files', 'LeadFilesController');

                // Proposal routes
                Route::get('proposals/data/{id?}', ['uses' => 'ProposalController@data'])->name('proposals.data');
                Route::get('proposals/download/{id}', ['uses' => 'ProposalController@download'])->name('proposals.download');
                Route::get('proposals/convert-proposal/{id?}', ['uses' => 'ProposalController@convertProposal'])->name('proposals.convert-proposal');
                Route::resource('proposals', 'ProposalController' , ['expect' => ['create']]);

                // Holidays
                Route::get('holidays/calendar-month', 'HolidaysController@getCalendarMonth')->name('holidays.calendar-month');
                Route::get('holidays/view-holiday/{year?}', 'HolidaysController@viewHoliday')->name('holidays.view-holiday');
                Route::get('holidays/mark_sunday', 'HolidaysController@Sunday')->name('holidays.mark-sunday');
                Route::get('holidays/calendar/{year?}', 'HolidaysController@holidayCalendar')->name('holidays.calendar');
                Route::get('holidays/mark-holiday', 'HolidaysController@markHoliday')->name('holidays.mark-holiday');
                Route::post('holidays/mark-holiday-store', 'HolidaysController@markDayHoliday')->name('holidays.mark-holiday-store');
                Route::resource('holidays', 'HolidaysController');

                Route::group(
                    ['prefix' => 'employees'],
                    function () {
                        Route::post('employees/store-jobprofile', ['uses' => 'ManageEmployeesController@storeJobProfile'])->name('employees.storeJobProfile');
                        Route::get('employees/free-employees', ['uses' => 'ManageEmployeesController@freeEmployees'])->name('employees.freeEmployees');
                        Route::get('employees/docs-create/{id}', ['uses' => 'ManageEmployeesController@docsCreate'])->name('employees.docs-create');
                        Route::get('employees/tasks/{userId}/{hideCompleted}', ['uses' => 'ManageEmployeesController@tasks'])->name('employees.tasks');
                        Route::get('employees/time-logs/{userId}', ['uses' => 'ManageEmployeesController@timeLogs'])->name('employees.time-logs');
                        Route::get('employees/data', ['uses' => 'ManageEmployeesController@data'])->name('employees.data');
                        Route::get('employees/salarydata', ['uses' => 'ManageEmployeesController@salarydata'])->name('employees.salarydata');
                        Route::get('employees/export/{status?}/{employee?}/{role?}', ['uses' => 'ManageEmployeesController@export'])->name('employees.export');
                        Route::post('employees/assignRole', ['uses' => 'ManageEmployeesController@assignRole'])->name('employees.assignRole');
                        Route::post('employees/assignProjectAdmin', ['uses' => 'ManageEmployeesController@assignProjectAdmin'])->name('employees.assignProjectAdmin');
                        Route::get('employees/employee-job-profile/{id}', ['uses' => 'ManageEmployeesController@addJobProfile'])->name('employees.employee_job_profile');
                        Route::get('employees/employee-profile-document/{id}', ['uses' => 'ManageEmployeesController@addProfileDocuments'])->name('employees.employee_profile_documents');
                        Route::get('employees/addId/{id}', ['uses' => 'ManageEmployeesController@addId'])->name('employees.addId');
                        Route::get('employees/addCertifiations', ['uses' => 'ManageEmployeesController@addCertifiations'])->name('employees.addCertifiations');
                        Route::get('employees/addWorks', ['uses' => 'ManageEmployeesController@addWorks'])->name('employees.addWorks');
                        Route::get('employees/allowance', ['uses'=> 'ManageEmployeesController@allowance'])->name('employees.allowance');
                        Route::get('employees/addAllowace', ['uses'=> 'ManageEmployeesController@addAllowace'])->name('employees.addAllowace');
                        Route::get('employees/allowance-data', ['uses'=> 'ManageEmployeesController@allowaceData'])->name('employees.allowance_data');
                        Route::delete('employees/delete-allowance/{id}', ['uses'=> 'ManageEmployeesController@destroyAllowance'])->name('employees.destroyAllowance');
                        Route::get('employees/edit-allowance/{id}', ['uses'=> 'ManageEmployeesController@editAllowance'])->name('employees.edit_allowance');
                        Route::post('employees/post-allowance', ['uses'=> 'ManageEmployeesController@postAllowance'])->name('employees.post_allowance');
                        Route::post('employees/update-allowance', ['uses'=> 'ManageEmployeesController@updateAllowance'])->name('employees.update_allowance');
                        Route::get('employees/overtime', ['uses'=> 'ManageEmployeesController@overtime'])->name('employees.overtime');
                        Route::get('employees/overtime-data', ['uses'=> 'ManageEmployeesController@overtimeData'])->name('employees.overtime_data');
                        Route::get('employees/overtime/{id}', ['uses'=> 'ManageEmployeesController@overtimeEdit'])->name('employees.edit_overtime');
                        Route::get('employees/addOvertime', ['uses'=> 'ManageEmployeesController@addOvertime'])->name('employees.addOvertime');
                        Route::post('employees/postOvertime', ['uses'=> 'ManageEmployeesController@postOvertime'])->name('employees.postOvertime');
                        Route::post('employees/update-overtime', ['uses'=> 'ManageEmployeesController@updateOvertime'])->name('employees.updateOvertime');
                        Route::delete('employees/delete-overtime/{id}', ['uses'=> 'ManageEmployeesController@deleteOvertime'])->name('employees.overtime_destroy');
                        Route::get('employees/salary-slip', ['uses'=> 'ManageEmployeesController@salaryslipDetail'])->name('employees.salary_slip');
                        Route::get('employees/salary-slip-show', ['uses'=> 'ManageEmployeesController@showSalarySlip'])->name('employees.show_salary_slip');
                        Route::get('employees/team/{id}', ['uses' => 'ManageEmployeesController@reportTeam'])->name('employees.team');
                        Route::post('employees/storeReportManager', ['uses' => 'ManageEmployeesController@storeReportManager'])->name('employees.storeReportManager');
                        Route::post('employees/storeReportTeam', ['uses' => 'ManageEmployeesController@storeReportTeam'])->name('employees.storeReportTeam');
                        Route::post('employees/storeDirectTeam', ['uses' => 'ManageEmployeesController@storeDirectTeam'])->name('employees.storeDirectTeam');
                        Route::get('employees/education/{id}', ['uses' => 'ManageEmployeesController@educationDetail'])->name('employees.education');
                        Route::get('employees/family/{id}', ['uses' => 'ManageEmployeesController@familyDetail'])->name('employees.family');
                        Route::match(['get','post'],'employees/uploadId/{id}', ['uses' => 'ManageEmployeesController@uploadId'])->name('employees.uploadId');
                        Route::delete('employees/deleteId/{id}', ['uses' => 'ManageEmployeesController@deleteId'])->name('employees.deleteId');
                        Route::match(['get','post'],'employees/editId/{id}', ['uses' => 'ManageEmployeesController@editId'])->name('employees.editId');
                        Route::match(['get','post'],'employees/updateId/{id}', ['uses' => 'ManageEmployeesController@updateId'])->name('employees.updateId');
                        Route::post('employees/store-family-detail', ['uses' => 'ManageEmployeesController@storeFamilyDetail'])->name('employees.storeFamilyDetail');
                        Route::post('employees/store-education-detail', ['uses' => 'ManageEmployeesController@storeEducationDetail'])->name('employees.storeEducationDetail');
                        Route::post('employees/saly-overview-data', ['uses' => 'ManageEmployeesController@getSalaryOvervies'])->name('employees.getSalaryOvervies');
                        Route::post('employees/sarave-role-permissions/{id}', ['uses' => 'ManageEmployeesController@saveRolePermissions'])->name('employees.saveRole');
                        Route::resource('employees', 'ManageEmployeesController');
                        //Route::resource('overtime', 'OvertimeController');
                        Route::get('department/quick-create', ['uses' => 'ManageTeamsController@quickCreate'])->name('teams.quick-create');
                        Route::get('department/data', ['uses' => 'ManageTeamsController@getData'])->name('teams.getData');
                        Route::post('department/quick-store', ['uses' => 'ManageTeamsController@quickStore'])->name('teams.quick-store');
                        Route::resource('teams', 'ManageTeamsController');
                        Route::resource('employee-teams', 'ManageEmployeeTeamsController');

                        Route::get('employee-docs/download/{id}', ['uses' => 'EmployeeDocsController@download'])->name('employee-docs.download');
                        Route::resource('employee-docs', 'EmployeeDocsController');
                    });
                Route::post('employee/store-jobprofile', ['uses' => 'ManageEmployeeController@storeJobProfile'])->name('employee.storeJobProfile');
                Route::get('employee/free-employees', ['uses' => 'ManageEmployeeController@freeEmployees'])->name('employee.freeEmployees');
                Route::get('employee/docs-create/{id}', ['uses' => 'ManageEmployeeController@docsCreate'])->name('employee.docs-create');
                Route::get('employee/tasks/{userId}/{hideCompleted}', ['uses' => 'ManageEmployeeController@tasks'])->name('employee.tasks');
                Route::get('employee/time-logs/{userId}', ['uses' => 'ManageEmployeeController@timeLogs'])->name('employee.time-logs');
                Route::get('employee/data', ['uses' => 'ManageEmployeeController@data'])->name('employee.data');
                Route::get('employee/salarydata', ['uses' => 'ManageEmployeeController@salarydata'])->name('employee.salarydata');
                Route::get('employee/export/{status?}/{employee?}/{role?}', ['uses' => 'ManageEmployeeController@export'])->name('employee.export');
                Route::post('employee/assignRole', ['uses' => 'ManageEmployeeController@assignRole'])->name('employee.assignRole');
                Route::post('employee/assignProjectAdmin', ['uses' => 'ManageEmployeeController@assignProjectAdmin'])->name('employee.assignProjectAdmin');
                Route::get('employee/employee-job-profile/{id}', ['uses' => 'ManageEmployeeController@addJobProfile'])->name('employee.employee_job_profile');
                Route::get('employee/employee-profile-document/{id}', ['uses' => 'ManageEmployeeController@addProfileDocuments'])->name('employee.employee_profile_documents');
                Route::get('employee/addId/{id}', ['uses' => 'ManageEmployeeController@addId'])->name('employee.addId');
                Route::get('employee/addCertifiations', ['uses' => 'ManageEmployeeController@addCertifiations'])->name('employee.addCertifiations');
                Route::get('employee/addWorks', ['uses' => 'ManageEmployeeController@addWorks'])->name('employee.addWorks');
                Route::get('employee/allowance', ['uses'=> 'ManageEmployeeController@allowance'])->name('employee.allowance');
                Route::get('employee/addAllowace', ['uses'=> 'ManageEmployeeController@addAllowace'])->name('employee.addAllowace');
                Route::get('employee/allowance-data', ['uses'=> 'ManageEmployeeController@allowaceData'])->name('employee.allowance_data');
                Route::delete('employee/delete-allowance/{id}', ['uses'=> 'ManageEmployeeController@destroyAllowance'])->name('employee.destroyAllowance');
                Route::get('employee/edit-allowance/{id}', ['uses'=> 'ManageEmployeeController@editAllowance'])->name('employee.edit_allowance');
                Route::post('employee/post-allowance', ['uses'=> 'ManageEmployeeController@postAllowance'])->name('employee.post_allowance');
                Route::post('employee/update-allowance', ['uses'=> 'ManageEmployeeController@updateAllowance'])->name('employee.update_allowance');
                Route::get('employee/overtime', ['uses'=> 'ManageEmployeeController@overtime'])->name('employee.overtime');
                Route::get('employee/overtime-data', ['uses'=> 'ManageEmployeeController@overtimeData'])->name('employee.overtime_data');
                Route::get('employee/overtime/{id}', ['uses'=> 'ManageEmployeeController@overtimeEdit'])->name('employee.edit_overtime');
                Route::get('employee/addOvertime', ['uses'=> 'ManageEmployeeController@addOvertime'])->name('employee.addOvertime');
                Route::post('employee/postOvertime', ['uses'=> 'ManageEmployeeController@postOvertime'])->name('employee.postOvertime');
                Route::post('employee/update-overtime', ['uses'=> 'ManageEmployeeController@updateOvertime'])->name('employee.updateOvertime');
                Route::delete('employee/delete-overtime/{id}', ['uses'=> 'ManageEmployeeController@deleteOvertime'])->name('employee.overtime_destroy');
                Route::get('employee/salary-slip', ['uses'=> 'ManageEmployeeController@salaryslipDetail'])->name('employee.salary_slip');
                Route::get('employee/salary-slip-show', ['uses'=> 'ManageEmployeeController@showSalarySlip'])->name('employee.show_salary_slip');
                Route::get('employee/team/{id}', ['uses' => 'ManageEmployeeController@reportTeam'])->name('employee.team');
                Route::get('employee/editreporting/{id}', ['uses' => 'ManageEmployeeController@editReporting'])->name('employee.editReportingManager');
                Route::get('employee/editEducation/{id}', ['uses' => 'ManageEmployeeController@editEducation'])->name('employee.editEducation');
                Route::get('employee/editdirectreporting/{id}', ['uses' => 'ManageEmployeeController@editDirectReportingManager'])->name('employee.editDirectReportingManager');
                Route::post('employee/storeReportManager', ['uses' => 'ManageEmployeeController@storeReportManager'])->name('employee.storeReportManager');
                Route::post('employee/storeReportTeam', ['uses' => 'ManageEmployeeController@storeReportTeam'])->name('employee.storeReportTeam');
                Route::post('employee/updateReportTeam', ['uses' => 'ManageEmployeeController@updateReportTeam'])->name('employee.updateReportTeam');
                Route::post('employee/updateEducation', ['uses' => 'ManageEmployeeController@updateEducation'])->name('employee.updateEducation');
                Route::post('employee/updateDirectReportTeam', ['uses' => 'ManageEmployeeController@updateDirectReportTeam'])->name('employee.updateDirectReportTeam');
                Route::delete('employee/deleteReportTeam/{id}', ['uses' => 'ManageEmployeeController@deleteReportTeam'])->name('employee.deleteReportTeam');
                Route::delete('employee/deleteEducation/{id}', ['uses' => 'ManageEmployeeController@deleteEducation'])->name('employee.deleteEducation');
                Route::delete('employee/deleteDirectReportTeam/{id}', ['uses' => 'ManageEmployeeController@deleteDirectReportTeam'])->name('employee.deleteDirectReportTeam');
                Route::post('employee/storeDirectTeam', ['uses' => 'ManageEmployeeController@storeDirectTeam'])->name('employee.storeDirectTeam');
                Route::get('employee/education/{id}', ['uses' => 'ManageEmployeeController@educationDetail'])->name('employee.education');
                Route::get('employee/family/{id}', ['uses' => 'ManageEmployeeController@familyDetail'])->name('employee.family');
                Route::match(['get','post'],'employee/uploadId/{id}', ['uses' => 'ManageEmployeeController@uploadId'])->name('employee.uploadId');
                Route::delete('employee/deleteId/{id}', ['uses' => 'ManageEmployeeController@deleteId'])->name('employee.deleteId');
                Route::match(['get','post'],'employee/editId/{id}', ['uses' => 'ManageEmployeeController@editId'])->name('employee.editId');
                Route::match(['get','post'],'employee/updateId/{id}', ['uses' => 'ManageEmployeeController@updateId'])->name('employee.updateId');
                Route::post('employee/store-family-detail', ['uses' => 'ManageEmployeeController@storeFamilyDetail'])->name('employee.storeFamilyDetail');
                Route::post('employee/store-education-detail', ['uses' => 'ManageEmployeeController@storeEducationDetail'])->name('employee.storeEducationDetail');
                Route::post('employee/saly-overview-data', ['uses' => 'ManageEmployeeController@getSalaryOvervies'])->name('employee.getSalaryOvervies');
                Route::post('employee/sarave-role-permissions/{id}', ['uses' => 'ManageEmployeeController@saveRolePermissions'])->name('employee.saveRole');
                Route::get('employee/data', ['uses' => 'ManageEmployeeController@data'])->name('employee.data');
                Route::post('employee/storeImage', ['uses' => 'ManageEmployeeController@storeImage'])->name('employee.storeImage');
                Route::resource('employee', 'ManageEmployeeController');
                //payroll

                Route::group(
                    ['prefix' => 'payroll'],
                    function () {
                        Route::resource('payroll', 'ManagePayrollController');
                        Route::get('pf-esi-settings', ['uses'=>'ManagePayrollController@pfEsiSettings'])->name('payroll.pfEsiSettings');
                        Route::post('store-pf-esi-settings', ['uses'=>'ManagePayrollController@storePfEsiSettings'])->name('payroll.storePfEsiSettings');
                        Route::post('store-esi-settings', ['uses'=>'ManagePayrollController@storeEsiSettings'])->name('payroll.storeEsiSettings');
                        Route::get('edit-pf-esi-settings/{id}', ['uses'=>'ManagePayrollController@editPfEsiSettings'])->name('payroll.editPfEsiSettings');
                        Route::post('update-pf-esi-settings', ['uses'=>'ManagePayrollController@updatePfEsiSettings'])->name('payroll.updatePfEsiSettings');
                        Route::post('update-pf-status', ['uses'=>'ManagePayrollController@updatePfStatus'])->name('payroll.updatePfStatus');
                        Route::post('update-esi-status', ['uses'=>'ManagePayrollController@updateEsiStatus'])->name('payroll.updateEsiStatus');
                        Route::get('pf-esi-overview', ['uses'=>'ManagePayrollController@pfEsiOverview'])->name('payroll.pfEsiOverview');
                        Route::get('pf-esi-data', ['uses'=>'ManagePayrollController@pfEsiData'])->name('payroll.data');

                        Route::get('pt-settings', ['uses'=>'ManagePayrollController@ptSettings'])->name('payroll.ptSettings');
                        Route::post('store-pt-settings', ['uses'=>'ManagePayrollController@storePtSettings'])->name('payroll.storePtSettings');
                        Route::post('update-pt-settings', ['uses'=>'ManagePayrollController@updatePtSettings'])->name('payroll.updatePtSettings');
                        Route::post('edit-pt-settings/{id}', ['uses'=>'ManagePayrollController@editptSettings'])->name('payroll.editptSettings');

                        Route::get('pt-overview', ['uses'=>'ManagePayrollController@ptOverview'])->name('payroll.ptOverview');
                        Route::get('pt-data', ['uses'=>'ManagePayrollController@ptData'])->name('payroll.ptData');

                        Route::get('desclaration-settings', ['uses'=>'ManagePayrollController@declarationSettings'])->name('payroll.declarationSettings');
                        Route::post('store-dec-settings', ['uses'=>'ManagePayrollController@storeDecSettings'])->name('payroll.storeDecSettings');

                        Route::get('pay-slip-settings', ['uses'=>'ManagePayrollController@paySlipSettings'])->name('payroll.paySlipSettings');
                        Route::get('pay-slip-format', ['uses'=>'ManagePayrollController@paySlipformat'])->name('payroll.paySlipformat');
                        Route::get('pay-slip-overview/{id}', ['uses'=>'ManagePayrollController@formatOverview'])->name('payroll.formatOverview');
                        Route::post('store-payslip-format', ['uses'=>'ManagePayrollController@storePaySlipFormat'])->name('payroll.storePaySlipFormat');

                        Route::post('generate-payslip', ['uses'=>'ManagePayrollController@generatePayslip'])->name('payroll.generatePayslip');
                        Route::get('pay-slips/{id}', ['uses'=>'ManagePayrollController@paySlips'])->name('payroll.paySlips');
                        Route::get('components', ['uses'=>'ManagePayrollController@Components'])->name('payroll.components');

                    });

                Route::group(
                    ['prefix' => 'salarystructure'],
                    function () {
                        Route::resource('salarystructure', 'ManageSalaryStructureController');
                        Route::get('component/recurring', ['uses' => 'ManageSalaryStructureController@recurring'])->name('component.recurring');
                        Route::get('component/variable', ['uses' => 'ManageSalaryStructureController@variable'])->name('component.variable');
                        Route::get('component/base-component', ['uses' => 'ManageSalaryStructureController@baseComponent'])->name('component.baseComponent');
                        Route::post('component/store-base-component', ['uses' => 'ManageSalaryStructureController@storeBaseComponent'])->name('component.storeBaseComponent');
                        Route::get('component/recurring-data', ['uses' => 'ManageSalaryStructureController@data'])->name('component.data');
                        Route::get('component/recurring-addcomponent', ['uses' => 'ManageSalaryStructureController@addComponent'])->name('component.addComponent');
                        Route::get('component/add-variable', ['uses' => 'ManageSalaryStructureController@addVariable'])->name('component.addVariable');
                        Route::get('component/add-adhoc-earning', ['uses' => 'ManageSalaryStructureController@addhocEarning'])->name('component.addhocEarning');
                        Route::get('component/variable-data', ['uses' => 'ManageSalaryStructureController@variableData'])->name('component.variableData');
                        Route::get('component/add-adhoc-earning-data', ['uses' => 'ManageSalaryStructureController@adhocEarningData'])->name('component.adhocEarningData');
                        Route::delete('component/delete-adhoc-earning/{id}', ['uses' => 'ManageSalaryStructureController@deleteAdhocEarning'])->name('component.deleteAdhocEarning');
                        Route::get('component/add-adhoc-diduction-data', ['uses' => 'ManageSalaryStructureController@adhocDiductionData'])->name('component.adhocDiductionData');
                        Route::delete('component/delete-adhoc-diduction/{id}', ['uses' => 'ManageSalaryStructureController@deleteAdhocDeduction'])->name('component.deleteAdhocDeduction');
                        Route::get('component/add-new-adhoc-earning', ['uses' => 'ManageSalaryStructureController@addNewAdhocEarning'])->name('component.addNewAdhocEarning');
                        Route::post('component/store-adhoc-earning', ['uses' => 'ManageSalaryStructureController@postAdhocEarning'])->name('component.postAdhocEarning');
                        Route::post('component/store-adhoc-diduction', ['uses' => 'ManageSalaryStructureController@postAdhocDiduction'])->name('component.postAdhocDiduction');
                        Route::get('component/add-adhoc-deduction', ['uses' => 'ManageSalaryStructureController@addhocDiduction'])->name('component.addhocDiduction');
                        Route::get('component/add-new-adhoc-deduction', ['uses' => 'ManageSalaryStructureController@addNewAdhocDeduction'])->name('component.addNewAdhocDeduction');
                        Route::post('component/recurring-storerecurringcomponent', ['uses' => 'ManageSalaryStructureController@storeRecurringcomponent'])->name('component.storeRecurringcomponent');
                        Route::post('component/store-variable-component', ['uses' => 'ManageSalaryStructureController@storeVariablegComponent'])->name('component.storeVariablegComponent');
                        Route::get('structure/salary-structure', ['uses' => 'ManageSalaryStructureController@salaryStructure'])->name('component.salaryStructure');
                        Route::get('structure/structure-rules-data', ['uses' => 'ManageSalaryStructureController@structureRulesData'])->name('structure.structureRulesData');
                        Route::get('structure/add-salary-structure', ['uses' => 'ManageSalaryStructureController@addSalaryStructure'])->name('component.addSalaryStructure');
                        Route::post('structure/store-structure', ['uses' => 'ManageSalaryStructureController@storeStructure'])->name('structure.storeStructure');
                        Route::get('structure/structure-data', ['uses' => 'ManageSalaryStructureController@structureData'])->name('structure.structureData');
                        Route::get('structure/edit-structure/{id}', ['uses' => 'ManageSalaryStructureController@editStructure'])->name('structure.editStructure');
                        Route::post('structure/update-structure', ['uses' => 'ManageSalaryStructureController@updateStructure'])->name('structure.updateStructure');
                        Route::get('structure/assign-structure', ['uses' => 'ManageSalaryStructureController@assignStructure'])->name('structure.assignStructure');
                        Route::post('structure/post-assign-structure', ['uses' => 'ManageSalaryStructureController@postAssignStructure'])->name('structure.postAssignStructure');
                        Route::get('structure/contractor-salary-structure', ['uses' => 'ManageSalaryStructureController@contratorSalaryStructure'])->name('component.contratorSalaryStructure');
                        Route::get('structure/contractor-add-salary-structure', ['uses' => 'ManageSalaryStructureController@addContractorSalaryStructure'])->name('component.addContractorSalaryStructure');
                        Route::post('structure/contractor-store-structure', ['uses' => 'ManageSalaryStructureController@storeContractorStructure'])->name('structure.storeContractorStructure');
                        Route::get('structure/contractor-structure-data', ['uses' => 'ManageSalaryStructureController@contractorStructureData'])->name('structure.contractorStructureData');
                        Route::delete('structure/delete-contractor-structure/{id}', ['uses' => 'ManageSalaryStructureController@deleteContactSalary'])->name('structure.deleteContactSalary');                        
                        Route::get('structure/contractor-assign-structure/{id}', ['uses' => 'ManageSalaryStructureController@assignContractorStructure'])->name('structure.assignContractorStructure');
                        Route::get('structure/salary-structure-overview', ['uses' => 'ManageSalaryStructureController@salaryStructureOverview'])->name('structure.salaryStructureOverview');
                        Route::get('structure/salary-structure-overview-data', ['uses' => 'ManageSalaryStructureController@salaryStructureOverviewData'])->name('structure.salOverviewData');
                        Route::get('structure/edit-structure-overview/{id}', ['uses' => 'ManageSalaryStructureController@editStructureOverview'])->name('structure.editStructureOverview');
                        Route::post('structure/post-structure-overview', ['uses' => 'ManageSalaryStructureController@postStructureOverview'])->name('structure.postStructureOverview');
                        Route::post('structure/post-structure-update', ['uses' => 'ManageSalaryStructureController@updateStatus'])->name('structure.updateStatus');
                        Route::post('structure/get-structure-amount', ['uses' => 'ManageSalaryStructureController@getStructureAmount'])->name('structure.getStructureAmount');

                    });

                Route::group(
                    ['prefix' => 'declaration'],
                    function () {
                        Route::resource('/', 'ManageDeclarationController');
                        Route::get('/view', ['uses' => 'ManageDeclarationController@view'])->name('declaration');
                        Route::get('/tax-data',['uses' => 'ManageDeclarationController@taxData'])->name('declaration.taxSchemeData');
                        Route::get('/hra-data',['uses' => 'ManageDeclarationController@hraData'])->name('declaration.hraData');
                        Route::get('/declare-data',['uses' => 'ManageDeclarationController@declareData'])->name('declaration.declareData');
                        Route::get('/adhoc-data',['uses' => 'ManageDeclarationController@getAdhocData'])->name('declaration.getAdhocData');
                        Route::get('/edit-adhoc',['uses' => 'ManageDeclarationController@editAdhocAction'])->name('declaration.editAdhocAction');
                        Route::post('/update-adhoc',['uses' => 'ManageDeclarationController@updateAdhoc'])->name('declaration.updateAdhoc');
                        Route::get('/income-loss-data',['uses' => 'ManageDeclarationController@incomelossData'])->name('declaration.incomelossData');
                        Route::get('/lta',['uses' => 'ManageDeclarationController@ltaData'])->name('declaration.ltaData');
                        Route::get('/previous-income',['uses' => 'ManageDeclarationController@incomePreviousData'])->name('declaration.incomePreviousData');
                        Route::get('/audit-history',['uses' => 'ManageDeclarationController@auditHistory'])->name('audit.auditHistory');
                        Route::get('/attendance-data', ['uses' => 'ManageDeclarationController@getAttendanceData'])->name('declaration.getAttendanceOverallData');
                        Route::get('/add-lop', ['uses' => 'ManageDeclarationController@addLop'])->name('declaration.addLop');
                        Route::post('/post-lop', ['uses' => 'ManageDeclarationController@postLop'])->name('declaration.postLop');
                        Route::get('/leave-data', ['uses' => 'ManageDeclarationController@getLeaveData'])->name('declaration.getLeaveData');
                        Route::get('/salary-revision-data', ['uses' => 'ManageDeclarationController@getSalaryRevision'])->name('declaration.getSalaryRevision');
                        Route::get('/vriable-adhoc-data', ['uses' => 'ManageDeclarationController@getVariableAdhoc'])->name('declaration.getVariableAdhoc');
                        Route::get('/salary-onhold-data', ['uses' => 'ManageDeclarationController@getSalaryOnHold'])->name('declaration.geSalaryOnHold');
                        Route::get('/edit-salary-onhold', ['uses' => 'ManageDeclarationController@editSalaryOnHold'])->name('declaration.editSalaryOnHold');
                        Route::post('/update-salary-onhold', ['uses' => 'ManageDeclarationController@updateSalaryOnHold'])->name('declaration.updateSalaryOnHold');
                        Route::get('/override-data', ['uses' => 'ManageDeclarationController@geTaxOverride'])->name('declaration.geTaxOverride');
                        Route::get('/edit-override', ['uses' => 'ManageDeclarationController@editTaxOverride'])->name('declaration.editTaxOverride');
                        Route::post('/update-override', ['uses' => 'ManageDeclarationController@updateTaxDetail'])->name('declaration.updateTaxDetail');
                        Route::get('/preview-data', ['uses' => 'ManageDeclarationController@getPreview'])->name('declaration.getPreview');
                        Route::get('/employee-tax-detail', ['uses' => 'ManageDeclarationController@employeeTaxDetail'])->name('declaration.employeeTaxDetail');
                        Route::post('/post-tax-detail', ['uses' => 'ManageDeclarationController@postTaxDetail'])->name('declaration.postTaxDetail');
                        Route::get('/salary-on-hold', ['uses' => 'ManageDeclarationController@salaryOnHold'])->name('declaration.salaryOnHold');
                        Route::get('/adhoc', ['uses' => 'ManageDeclarationController@adhocAction'])->name('declaration.adhocAction');
                        Route::post('/post-adhoc', ['uses' => 'ManageDeclarationController@postAdhoc'])->name('declaration.postAdhoc');
                        Route::post('/post-salry-on-hold', ['uses' => 'ManageDeclarationController@postSalaryOnHold'])->name('declaration.postSalaryOnHold');
                        Route::get('/preview-slip', ['uses' => 'ManageDeclarationController@previewSlip'])->name('declaration.previewSlip');
                        Route::post('/varify', ['uses' => 'ManageDeclarationController@payrollVerification'])->name('declaration.verify');
                        Route::post('/unvarify', ['uses' => 'ManageDeclarationController@payrollUnverify'])->name('declaration.unverify');
                        Route::post('/pay-register', ['uses' => 'ManageDeclarationController@payRegister'])->name('declaration.payregister');
                        Route::post('/revert-payroll-new', ['uses' => 'ManageDeclarationController@revertPayrollNew'])->name('declaration.revertPayrollNew');
                        Route::match(['get', 'post'],'/get-component', ['uses' => 'ManageDeclarationController@getComponentName'])->name('declaration.getComponentName');

                    });

                Route::match(['get','post'],'projects/get-subprojects-options', ['uses' => 'ManageProjectsController@subProjectOptions'])->name('projects.subprojectoptions');
                Route::post('projects/edit-gnatt-task', ['uses' => 'ManageProjectsController@editGnattTask'])->name('projects.editGnattTask');
                Route::post('projects/gantt-task-update/{id}', ['uses' => 'ManageProjectsController@updateTaskDuration'])->name('projects.gantt-task-update');
                Route::get('projects/ajaxCreate/{columnId}', ['uses' => 'ManageProjectsController@ajaxCreate'])->name('projects.ajaxCreate');
                Route::get('projects/archive-data', ['uses' => 'ManageProjectsController@archiveData'])->name('projects.archive-data');
                Route::get('projects/archive', ['uses' => 'ManageProjectsController@archive'])->name('projects.archive');
                Route::get('projects/archive-restore/{id?}', ['uses' => 'ManageProjectsController@archiveRestore'])->name('projects.archive-restore');
                Route::get('projects/archive-delete/{id?}', ['uses' => 'ManageProjectsController@archiveDestroy'])->name('projects.archive-delete');
                Route::get('projects/export/{status?}/{clientID?}', ['uses' => 'ManageProjectsController@export'])->name('projects.export');
                Route::get('projects/data', ['uses' => 'ManageProjectsController@data'])->name('projects.data');
                Route::get('projects/ganttData/{projectId?}', ['uses' => 'ManageProjectsController@ganttData'])->name('projects.ganttData');
                Route::get('projects/gantt/{projectId?}', ['uses' => 'ManageProjectsController@gantt'])->name('projects.gantt');
                Route::get('projects/burndown/{projectId?}', ['uses' => 'ManageProjectsController@burndownChart'])->name('projects.burndown-chart');
                Route::get('projects/boq/{projectId?}', ['uses' => 'ManageProjectsController@Boq'])->name('projects.boq');
                Route::get('projects/boq/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@BoqTitle'])->name('projects.boqtitle');
                Route::post('projects/boqtitleloop', ['uses' => 'ManageProjectsController@BoqTitleloop'])->name('projects.boqtitleloop');
                Route::get('projects/showitemno/{projectid?}/{subprojectid?}', ['uses' => 'ManageProjectsController@showItemno'])->name('projects.showitemno');

                Route::get('projects/boqbudget/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@BoqBudget'])->name('projects.boqbudget');
                Route::post('projects/boqbudgetloop', ['uses' => 'ManageProjectsController@BoqBudgetloop'])->name('projects.boqbudgetloop');
                Route::get('projects/boqinvoices/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@BoqInvoices'])->name('projects.boqinvoices');
                Route::post('projects/boqinvoicesloop', ['uses' => 'ManageProjectsController@BoqInvoicesloop'])->name('projects.boqinvoicesloop');

                Route::post('projects/boqexcellimport', ['uses' => 'ManageProjectsController@boqExcellImport'])->name('projects.boqExcellImport');
                Route::post('projects/templatesubmit', ['uses' => 'ManageProjectsController@templateSubmit'])->name('projects.templateSubmit');

                Route::post('projects/addcostitemrow', ['uses' => 'ManageProjectsController@addBoqCostItemRow'])->name('projects.addcostitemrow');
                Route::post('projects/addcostitemcategory', ['uses' => 'ManageProjectsController@addBoqCostItemCategory'])->name('projects.addcostitemcategory');
                Route::post('projects/boqcatchangeposition', ['uses' => 'ManageProjectsController@boqChangeCatPosition'])->name('projects.boqcatchangeposition');
                Route::post('projects/boqcostitemchangeposition', ['uses' => 'ManageProjectsController@boqChangeCostitemPosition'])->name('projects.boqcostitemchangeposition');
                Route::post('projects/boqcolchangeposition', ['uses' => 'ManageProjectsController@boqChangeColPosition'])->name('projects.boqcolchangeposition');
                Route::post('projects/projectBoqLock', ['uses' => 'ManageProjectsController@projectBoqLock'])->name('projects.projectBoqLock');
                Route::post('projects/updateboqcostitem', ['uses' => 'ManageProjectsController@updateCostItem'])->name('projects.updateboqcostitem');
                Route::delete('projects/project-destroy-cost-item/{id}', ['uses' => 'ManageProjectsController@costitemDestroy'])->name('projects.costitem-destroy');
                Route::delete('projects/project-destroy-cat-cost-item/{id}', ['uses' => 'ManageProjectsController@costitemCatDestroy'])->name('projects.costitem-cat-destroy');
                Route::delete('projects/project-title-destroy/{id}', ['uses' => 'ManageProjectsController@titleDestroy'])->name('projects.title-destroy');
                Route::match(['get','post'],'projects/boq-rate-sheet/{id}', ['uses' => 'ManageProjectsController@boqRateSheet'])->name('projects.rate-sheet');
                Route::match(['get','post'],'projects/add-rate-to-costproduct', ['uses' => 'ManageProjectsController@addRateToProductCost'])->name('projects.addratetocostitem');
                Route::match(['get','post'],'projects/ratesheetstore', ['uses' => 'ManageProjectsController@addRateSheetStore'])->name('projects.ratesheetstore');
                Route::match(['get','post'],'projects/removeratesheetid', ['uses' => 'ManageProjectsController@removeRateSheetRes'])->name('projects.removeratesheetid');
                Route::match(['get','post'],'projects/get-title-by-projects', ['uses' => 'ManageProjectsController@getTitlesByProjects'])->name('projects.projecttitles');
                Route::match(['get','post'],'projects/get-segments-by-projects', ['uses' => 'ManageProjectsController@getSegmentsByProjects'])->name('projects.projectsegments');
                Route::match(['get','post'],'projects/get-costiem-by-titles', ['uses' => 'ManageProjectsController@getCostitemByTitle'])->name('projects.projectcostitems');
                Route::match(['get','post'],'projects/progress-report', ['uses' => 'ManageProjectsController@getProgressReport'])->name('projects.progress-report');
                Route::match(['get','post'],'projects/progress-report-html', ['uses' => 'ManageProjectsController@getProgressReportHtml'])->name('projects.progress-report-html');
                Route::match(['get','post'],'projects/progress-report-export', ['uses' => 'ManageProjectsController@exportProgressReportHtml'])->name('projects.exportprogressreport');
                Route::match(['get','post'],'projects/progress-report-pdf', ['uses' => 'ManageProjectsController@progressReportPdf'])->name('projects.progressReportPdf');
                Route::match(['get','post'],'projects/progress-report-pdf-html/{id}', ['uses' => 'ManageProjectsController@progressReportPdfhtml'])->name('projects.progressReportPdfHtml');
                Route::match(['get','post'],'projects/viewTask', ['uses' => 'ManageProjectsController@viewTask'])->name('projects.viewTask');
                Route::match(['get','post'],'projects/projectMembers/{projectid?}', ['uses' => 'ManageProjectsController@projectMembers'])->name('projects.members');

                Route::match(['get','post'],'projects/project-boq-create/{projectId?}', ['uses' => 'ManageProjectsController@projectBoqCreate'])->name('projects.projectBoqCreate');
                Route::match(['get','post'],'projects/edit-file/{projectId}/{fileId}', ['uses' => 'ManageProjectsController@projectEditFile'])->name('projects.projectEditFile');
                Route::match(['get','post'],'projects/titleCreate/{projectId?}', ['uses' => 'ManageProjectsController@titleCreate'])->name('projects.titleCreate');
                Route::match(['get','post'],'projects/schedulingData/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@schedulingData'])->name('projects.schedulingData');
                Route::match(['get','post'],'projects/resourseData/{projectId?}', ['uses' => 'ManageProjectsController@resourseData'])->name('projects.resourseData');
                Route::match(['get','post'],'projects/baselineupdate', ['uses' => 'ManageProjectsController@baselineUpdate'])->name('projects.baselineupdate');
                Route::match(['get','post'],'projects/createbaseline', ['uses' => 'ManageProjectsController@createBaseline'])->name('projects.createbaseline');
                Route::match(['get','post'],'projects/removeBaseline', ['uses' => 'ManageProjectsController@removeBaseline'])->name('projects.removeBaseline');
                Route::match(['get','post'],'projects/activeBaseline', ['uses' => 'ManageProjectsController@activeBaseline'])->name('projects.activeBaseline');

                Route::match(['get','post'],'projects/costData/{Id?}', ['uses' => 'ManageProjectsController@costData'])->name('projects.costData');
                Route::match(['get','post'],'projects/costDataUpdate', ['uses' => 'ManageProjectsController@costDataUpdate'])->name('projects.costDataUpdate');
                Route::match(['get','post'],'projects/scheduling/{projectId?}', ['uses' => 'ManageProjectsController@scheduling'])->name('projects.scheduling');
                Route::match(['get','post'],'projects/scheduling/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@schedulingChart'])->name('projects.schedulingChart');
                Route::match(['get','post'],'projects/setresource/{projectId?}/{title?}', ['uses' => 'ManageProjectsController@setResource'])->name('projects.setresource');


                Route::match(['get','post'],'projects/costDataCreate', ['uses' => 'ManageProjectsController@costDataCreate'])->name('projects.costDataCreate');
                Route::match(['get','post'],'projects/activityCreate', ['uses' => 'ManageProjectsController@activityCreate'])->name('projects.activityCreate');


                Route::match(['get','post'],'projects/getItemLavel', ['uses' => 'ManageProjectsController@getItemLavel'])->name('projects.getItemLavel');
                Route::match(['get','post'],'projects/getChild', ['uses' => 'ManageProjectsController@getChild'])->name('projects.getChild');
                Route::match(['get','post'],'projects/getProduct', ['uses' => 'ManageProjectsController@getProduct'])->name('projects.getProduct');
                Route::match(['get','post'],'projects/getCostItem', ['uses' => 'ManageProjectsController@getCostItem'])->name('projects.getCostItem');
                Route::match(['get','post'],'projects/updateStatus/{id}', ['uses' => 'ManageProjectsController@updateStatus'])->name('projects.updateStatus');
                Route::match(['get','post'],'projects/viewBoq/{id}/{title}', ['uses' => 'ManageProjectsController@product-type-workforce'])->name('projects.viewBoq');
                Route::match(['get','post'],'projects/editBoq/{id}/{title}', ['uses' => 'ManageProjectsController@editBoq'])->name('projects.editBoq');
                Route::match(['get','post'],'projects/updateBoq/{id}/{title}', ['uses' => 'ManageProjectsController@updateBoq'])->name('projects.updateBoq');
                Route::match(['get','post'],'projects/addCostItems', ['uses' => 'ManageProjectsController@addCostItems'])->name('projects.addCostItems');
                Route::match(['get','post'],'projects/ProjectFinalQty/{id}', ['uses' => 'ManageProjectsController@ProjectFinalQty'])->name('projects.ProjectFinalQty');
                Route::match(['get','post'],'projects/schedulingDue/{columnId}', ['uses' => 'ManageProjectsController@schedulingDue'])->name('projects.schedulingDue');
                Route::match(['get','post'],'projects/getDesignFiles', ['uses' => 'ManageProjectsController@getDesignFiles'])->name('projects.getDesignFiles');
                Route::match(['get','post'],'projects/openDesignFolder/{id}', ['uses' => 'ManageProjectsController@openDesignFolder'])->name('projects.openDesignFolder');
                Route::match(['get','post'],'projects/selectDesignFile/{id}', ['uses' => 'ManageProjectsController@selectDesignFile'])->name('projects.selectDesignFile');
                Route::delete('projects/deleteDesignFile/{id}', ['uses' => 'ManageProjectsController@deleteDesignFile'])->name('projects.deleteDesignFile');

               /* Route::match(['get','post'],'projects/showDesignFiles/{id}', ['uses' => 'ManageDesignController@showDesignFiles'])->name('projects.showDesignFiles');
                Route::post('projects/store-design', ['uses' => 'ManageDesignController@storeDesign'])->name('projects.storeDesign');
                Route::post('projects/store-link', ['uses' => 'ManageDesignController@storeLink'])->name('projects.storeDesignLink');
                Route::post('projects/create-folder', ['uses' => 'ManageDesignController@createFolder'])->name('projects.createDesignFolder');
                Route::post('projects/edit-folder', ['uses' => 'ManageDesignController@editFolder'])->name('projects.editDesignFolder');
                Route::get('projects/download/{id}', ['uses' => 'ManageDesignController@download'])->name('projects.Designdownload');
                Route::get('projects/thumbnail', ['uses' => 'ManageDesignController@thumbnailShow'])->name('projects.Designthumbnail');
                Route::post('projects/multiple-upload', ['uses' => 'ManageDesignController@storeMultiple'])->name('projects.Designmultiple-upload');*/


                Route::get('projects/segments/{projectId?}', ['uses' => 'ManageSegmentsController@Segments'])->name('projects.segments');
                Route::get('projects/segments/{projectId?}/{title?}/{segment?}', ['uses' => 'ManageSegmentsController@segmentsTitle'])->name('projects.segmentstitle');
                Route::delete('projects/segment-project-destroy-cost-item/{id}', ['uses' => 'ManageSegmentsController@costitemDestroy'])->name('projects.segment-costitem-destroy');
                Route::delete('projects/segment-project-destroy-cat-cost-item/{id}', ['uses' => 'ManageSegmentsController@costitemCatDestroy'])->name('projects.segment-costitem-cat-destroy');
                Route::post('projects/segment-addcostitemrow', ['uses' => 'ManageSegmentsController@addBoqCostItemRow'])->name('projects.segment-addcostitemrow');
                Route::post('projects/segment-addcostitemcategory', ['uses' => 'ManageSegmentsController@addBoqCostItemCategory'])->name('projects.segment-addcostitemcategory');
                Route::post('projects/segment-boqcatchangeposition', ['uses' => 'ManageSegmentsController@boqChangeCatPosition'])->name('projects.segment-boqcatchangeposition');
                Route::post('projects/segment-boqcostitemchangeposition', ['uses' => 'ManageSegmentsController@boqChangeCostitemPosition'])->name('projects.segment-boqcostitemchangeposition');
                Route::post('projects/segment-boqcolchangeposition', ['uses' => 'ManageSegmentsController@boqChangeColPosition'])->name('projects.segment-boqcolchangeposition');
                Route::post('projects/segment-projectBoqLock', ['uses' => 'ManageSegmentsController@projectBoqLock'])->name('projects.segment-projectBoqLock');
                Route::post('projects/segment-updateboqcostitem', ['uses' => 'ManageSegmentsController@updateCostItem'])->name('projects.segment-updateboqcostitem');
                Route::post('projects/segment-boqtitleloop', ['uses' => 'ManageSegmentsController@boqtitleLoop'])->name('projects.segment-boqtitleloop');


                //sourcing packages
                Route::match(['get','post'],'projects/sourcingPackages/{id}', ['uses' => 'ManageProjectsController@sourcingPackages'])->name('projects.sourcingPackages');
                Route::match(['get','post'],'projects/addSourcingPackages/{id}', ['uses' => 'ManageProjectsController@addSourcingPackages'])->name('projects.addSourcingPackages');
                Route::match(['get','post'],'projects/storeSourcingPackages/{id}', ['uses' => 'ManageProjectsController@storeSourcingPackages'])->name('projects.storeSourcingPackages');
                Route::match(['get','post'],'projects/editSourcingPackages/{pid}/{id}', ['uses' => 'ManageProjectsController@editSourcingPackages'])->name('projects.editSourcingPackages');
                Route::match(['get','post'],'projects/updateSourcingPackages/{id}', ['uses' => 'ManageProjectsController@updateSourcingPackages'])->name('projects.updateSourcingPackages');
                Route::match(['get','post'],'projects/getProducts/', ['uses' => 'ManageProjectsController@getProducts'])->name('projects.getProducts');
                Route::match(['get','post'],'projects/filterData/', ['uses' => 'ManageProjectsController@filterData'])->name('projects.filterData');
                Route::delete('projects/deleteSourcingPackages/{id}', ['uses' => 'ManageProjectsController@deleteSourcingPackages'])->name('projects.deleteSourcingPackages');
                Route::match(['get','post'],'projects/createTender/{pid}/{id}', ['uses' => 'ManageProjectsController@createTender'])->name('projects.createTender');
                Route::match(['get','post'],'projects/editTender/{pid}/{sid}/{id}', ['uses' => 'ManageProjectsController@editTender'])->name('projects.editTender');
                Route::match(['get','post'],'projects/assignTender/{pid}/{sid}/{id}', ['uses' => 'ManageProjectsController@assignTender'])->name('projects.assignTender');
                Route::match(['get','post'],'projects/storeAssignTender/', ['uses' => 'ManageProjectsController@storeAssignTender'])->name('projects.storeAssignTender');
                Route::match(['get','post'],'projects/tendersList/{pid}/{id}', ['uses' => 'ManageProjectsController@tendersList'])->name('projects.tendersList');
                Route::match(['get','post'],'projects/getBrandsTag', ['uses' => 'ManageProjectsController@getBrandsTag'])->name('projects.getBrandsTag');
                Route::match(['get','post'],'projects/saveTenders', ['uses' => 'ManageProjectsController@saveTenders'])->name('projects.saveTenders');
                Route::match(['get','post'],'projects/storeTenderFiles', ['uses' => 'ManageProjectsController@storeTenderFiles'])->name('projects.storeTenderFiles');
                Route::match(['get','post'],'projects/tenderBiddings', ['uses' => 'ManageProjectsController@tenderBiddings'])->name('projects.tenderBiddings');
                Route::match(['get','post'],'projects/tenderBiddingDetails/{id}', ['uses' => 'ManageProjectsController@tenderBiddingDetails'])->name('projects.tenderBiddingDetails');
                Route::match(['get','post'],'projects/productIssue/{id}', ['uses' => 'ManageProjectsController@productIssue'])->name('projects.productIssue');
                Route::match(['get','post'],'projects/storeIssuedProduct/', ['uses' => 'ManageProjectsController@storeIssuedProduct'])->name('projects.storeIssuedProduct');
                Route::match(['get','post'],'projects/createIssuedProduct/{id}', ['uses' => 'ManageProjectsController@createIssuedProduct'])->name('projects.createIssuedProduct');
                Route::match(['get','post'],'projects/issuedProductDetail/{pid}/{id}', ['uses' => 'ManageProjectsController@issuedProductDetail'])->name('projects.issuedProductDetail');
                Route::match(['get','post'],'projects/returnIssuedProduct/{pid}/{id}', ['uses' => 'ManageProjectsController@returnIssuedProduct'])->name('projects.returnIssuedProduct');
                Route::get('projects/data-simple', ['uses' => 'ManageProjectsController@dataSimple'])->name('projects.dataSimple');
                Route::post('projects/store-simple', ['uses' => 'ManageProjectsController@storeSimple'])->name('projects.storeSimple');
                Route::put('projects/update-simple/{id}', ['uses' => 'ManageProjectsController@updateSimple'])->name('projects.updateSimple');
                Route::match(['get','post'],'projects/showStoresProducts/{id}/{sid}', ['uses' => 'ManageProjectsController@showStoresProducts'])->name('projects.showStoresProducts');
                Route::match(['get','post'],'projects/showStoresStock/{id}/{sid}', ['uses' => 'ManageProjectsController@showStoresStock'])->name('projects.showStoresStock');
                Route::get('projects/stockProjectData/{id}/{sid}', ['uses' => 'ManageProjectsController@stockProjectData'])->name('projects.stockProjectData');
                Route::match(['get','post'],'projects/bom/{pid}/{sid}', ['uses' => 'ManageProjectsController@bom'])->name('projects.bom');
                Route::match(['get','post'],'projects/storeBom/', ['uses' => 'ManageProjectsController@storeBom'])->name('projects.storeBom');
                Route::match(['get','post'],'projects/updateBom/', ['uses' => 'ManageProjectsController@updateBom'])->name('projects.updateBom');
                Route::post('projects/projectstore', ['uses' => 'ManageProjectsController@projectstore'])->name('projects.projectstore');
                Route::post('projects/replyComment', ['uses' => 'ManageProjectsController@replyComment'])->name('projects.replyComment');
                Route::get('projects/select-projects', ['uses' => 'ManageProjectsController@selectProjects'])->name('projects.selectProjects');
                Route::post('projects/update-select-projects', ['uses' => 'ManageProjectsController@updateSelectedProjects'])->name('projects.update-selected-projects');
                Route::post('projects/store-projects-image', ['uses' => 'ManageProjectsController@storeImage'])->name('projects.storeImage');
                Route::resource('projects', 'ManageProjectsController');



                Route::delete('changeorders/project-destroy-cost-item/{id}', ['uses' => 'ManageChangeOrdersController@costitemDestroy'])->name('changeorders.costitem-destroy');
                Route::delete('changeorders/project-destroy-cat-cost-item/{id}', ['uses' => 'ManageChangeOrdersController@costitemCatDestroy'])->name('changeorders.costitem-cat-destroy');
                Route::match(['get','post'],'changeorders/getItemLavel', ['uses' => 'ManageChangeOrdersController@getItemLavel'])->name('changeorders.getItemLavel');
                Route::get('changeorders/boq/{projectId?}', ['uses' => 'ManageChangeOrdersController@Boq'])->name('changeorders.boq');
                Route::get('changeorders/boq/{projectId?}/{title?}', ['uses' => 'ManageChangeOrdersController@BoqTitle'])->name('changeorders.boqtitle');
                Route::post('changeorders/boqtitleloop', ['uses' => 'ManageChangeOrdersController@BoqTitleloop'])->name('changeorders.boqtitleloop');
                Route::post('changeorders/boqexcellimport', ['uses' => 'ManageChangeOrdersController@boqExcellImport'])->name('changeorders.boqExcellImport');
                Route::post('changeorders/templatesubmit', ['uses' => 'ManageChangeOrdersController@templateSubmit'])->name('changeorders.templateSubmit');
                Route::post('changeorders/addcostitemrow', ['uses' => 'ManageChangeOrdersController@addBoqCostItemRow'])->name('changeorders.addcostitemrow');
                Route::post('changeorders/addcostitemcategory', ['uses' => 'ManageChangeOrdersController@addBoqCostItemCategory'])->name('changeorders.addcostitemcategory');
                Route::post('changeorders/boqcatchangeposition', ['uses' => 'ManageChangeOrdersController@boqChangeCatPosition'])->name('changeorders.boqcatchangeposition');
                Route::post('changeorders/boqcostitemchangeposition', ['uses' => 'ManageChangeOrdersController@boqChangeCostitemPosition'])->name('changeorders.boqcostitemchangeposition');
                Route::post('changeorders/boqcolchangeposition', ['uses' => 'ManageChangeOrdersController@boqChangeColPosition'])->name('changeorders.boqcolchangeposition');
                Route::post('changeorders/projectBoqLock', ['uses' => 'ManageChangeOrdersController@projectBoqLock'])->name('changeorders.projectBoqLock');
                Route::post('changeorders/updateboqcostitem', ['uses' => 'ManageChangeOrdersController@updateCostItem'])->name('changeorders.updateboqcostitem');
                Route::resource('changeorders', 'ManageChangeOrdersController');

                Route::get('projectleaves/create_work_week/{id}', ['uses' => 'ManageProjectsLeavesController@createWorkWeek'])->name('projectleaves.createworkweek');
                Route::get('projectleaves/selectproject', ['uses' => 'ManageProjectsLeavesController@selectWorkweekProjects'])->name('projectleaves.workweek');
                Route::post('projectleaves/store-work-week', ['uses' => 'ManageProjectsLeavesController@storeWorkWeek'])->name('projectleaves.storeworkweek');
               /* Route::resource('projectleaves', 'ManageProjectsLeavesController');*/
                // Holidays
                Route::get('projectholidays/calendar-month', 'ManageProjectsHolidaysController@getCalendarMonth')->name('projectholidays.calendar-month');
                Route::get('projectholidays/view-holiday/{year?}', 'ManageProjectsHolidaysController@viewHoliday')->name('projectholidays.view-holiday');
                Route::get('projectholidays/mark_sunday', 'ManageProjectsHolidaysController@Sunday')->name('projectholidays.mark-sunday');
                Route::get('projectholidays/calendar/{year?}/{id?}', 'ManageProjectsHolidaysController@holidayCalendar')->name('projectholidays.calendar');
                Route::get('projectholidays/mark-holiday', 'ManageProjectsHolidaysController@markHoliday')->name('projectholidays.mark-holiday');
                Route::post('projectholidays/mark-holiday-store', 'ManageProjectsHolidaysController@markDayHoliday')->name('projectholidays.mark-holiday-store');
                Route::get('projectholidays/select-project-holidays', ['uses' => 'ManageProjectsHolidaysController@selectHolidaysProjects'])->name('projectholidays.holidays');
                Route::get('projectholidays/{id}', ['uses' => 'ManageProjectsHolidaysController@index'])->name('projectholidays.holidayindex');
                Route::get('projectholidays/create/{id}', ['uses' => 'ManageProjectsHolidaysController@create'])->name('projectholidays.holidaycreate');
                Route::post('projectholidays/store/{id}', ['uses' => 'ManageProjectsHolidaysController@store'])->name('projectholidays.holidaystore');
                Route::resource('projectholidays', 'ManageProjectsHolidaysController');

                Route::post('projects/indent/approve/{id}/{val}', ['uses' => 'ManageProjectsController@indentApprove'])->name('projects.indent.approve');
                Route::get('projects/indent/create/{pid}/{sid}', ['uses' => 'ManageProjectsController@indentCreate'])->name('projects.indent.create');
                Route::get('projects/indent/create-new/{pid}/{sid}', ['uses' => 'ManageProjectsController@indentCreateNew'])->name('projects.indent.createNew');
                Route::get('projects/indent/export', ['uses' => 'ManageProjectsController@indentExport'])->name('projects.indent.export');
                Route::get('projects/indent/{id}/convert', ['uses' => 'ManageProjectsController@indentConvert'])->name('projects.indent.gconvert');
                Route::patch('projects/indent/{id}/convert', ['uses' => 'ManageProjectsController@indentPostConvert'])->name('projects.indent.pconvert');
                Route::post('projects/indent/get-brands', ['uses' => 'ManageProjectsController@indentGetBrands'])->name('projects.indent.getBrands');
                Route::post('projects/indent/store-tmp', ['uses' => 'ManageProjectsController@indentStoreTmp'])->name('projects.indent.storeTmp');
                Route::post('projects/indent/delete-tmp', ['uses' => 'ManageProjectsController@indentDeleteTmp'])->name('projects.indent.deleteTmp');
                Route::post('projects/indent/store-indent', ['uses' => 'ManageProjectsController@indentStore'])->name('projects.indent.store');
                Route::match(['get','post'],'projects/indent/edit-indent/{pid}/{sid}/{id}', ['uses' => 'ManageProjectsController@indentEdit'])->name('projects.indent.edit');
                Route::post('projects/indent/update-indent/{id}', ['uses' => 'ManageProjectsController@indentUpdate'])->name('projects.indent.update');
                Route::delete('projects/indent/destroy-indent/{id}', ['uses' => 'ManageProjectsController@indentDestroy'])->name('projects.indent.destroy');
                Route::get('indent/data/{pid}/{sid?}', ['uses' => 'ManageProjectsController@indentData'])->name('projects.indent.data');
                Route::post('indent/brand', ['uses' => 'ManageProjectsController@indentBrand'])->name('projects.indent.brand');

                Route::get('projects/{id}/stores', 'ManageProjectsController@stores')->name('projects.stores');
                Route::get('projects/indents/{pid}/{sid}', 'ManageProjectsController@indents')->name('projects.indents');
                Route::get('projects/rfqs/{pid}/{sid}', 'ManageProjectsController@rfqs')->name('projects.rfqs');
                Route::get('projects/quotations/{pid}/{sid}', 'ManageProjectsController@quotations')->name('projects.quotations');
                Route::get('projects/{id}/purchase-orders', 'ManageProjectsController@purchaseOrders')->name('projects.purchaseOrders');

                //Store Projects
                Route::get('stores-projects/index', ['uses' => 'ManageStoresProjectsController@storesIndex'])->name('stores.projects.storesIndex');
                Route::get('stores-projects/data-simple', ['uses' => 'ManageStoresProjectsController@dataSimple'])->name('stores.projects.dataSimple');
                Route::post('stores-projects/store-simple', ['uses' => 'ManageStoresProjectsController@storeSimple'])->name('stores.projects.storeSimple');
                Route::put('stores-projects/update-simple/{id}', ['uses' => 'ManageStoresProjectsController@updateSimple'])->name('stores.projects.updateSimple');
                Route::match(['get','post'],'stores-projects/showStoresProducts/{id}/{sid}', ['uses' => 'ManageStoresProjectsController@showStoresProducts'])->name('stores.projects.showStoresProducts');
                Route::match(['get','post'],'stores-projects/showStoresStock/{id}/{sid}', ['uses' => 'ManageStoresProjectsController@showStoresStock'])->name('stores.projects.showStoresStock');
                Route::get('stores-projects/stockProjectData/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@stockProjectData'])->name('stores.projects.stockProjectData');
                Route::match(['get','post'],'stores-projects/bom/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@bom'])->name('stores.projects.bom');
                Route::match(['get','post'],'stores-projects/storeBom/', ['uses' => 'ManageStoresProjectsController@storeBom'])->name('stores.projects.storeBom');
                Route::match(['get','post'],'stores-projects/updateBom/', ['uses' => 'ManageStoresProjectsController@updateBom'])->name('stores.projects.updateBom');
                Route::match(['get','post'],'stores-projects/deleteBom/{id}', ['uses' => 'ManageStoresProjectsController@deleteBom'])->name('stores.projects.deleteBom');
                Route::resource('stores-projects', 'ManageStoresProjectsController');
                Route::post('stores-projects/indent/approve/{id}/{val}', ['uses' => 'ManageStoresProjectsController@indentApprove'])->name('stores.projects.indent.approve');
                Route::get('stores-projects/indent/create/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@indentCreate'])->name('stores.projects.indent.create');
                Route::get('stores-projects/indent/create-new/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@indentCreateNew'])->name('stores.projects.indent.createNew');
                Route::get('stores-projects/indent/export', ['uses' => 'ManageStoresProjectsController@indentExport'])->name('stores.projects.indent.export');
                Route::get('stores-projects/indent/{id}/convert', ['uses' => 'ManageStoresProjectsController@indentConvert'])->name('stores.projects.indent.gconvert');
                Route::patch('stores-projects/indent/{id}/convert', ['uses' => 'ManageStoresProjectsController@indentPostConvert'])->name('stores.projects.indent.pconvert');
                Route::post('stores-projects/indent/get-brands', ['uses' => 'ManageStoresProjectsController@indentGetBrands'])->name('stores.projects.indent.getBrands');
                Route::post('stores-projects/indent/store-tmp', ['uses' => 'ManageStoresProjectsController@indentStoreTmp'])->name('stores.projects.indent.storeTmp');
                Route::post('stores-projects/indent/delete-tmp', ['uses' => 'ManageStoresProjectsController@indentDeleteTmp'])->name('stores.projects.indent.deleteTmp');
                Route::post('stores-projects/indent/store-indent', ['uses' => 'ManageStoresProjectsController@indentStore'])->name('stores.projects.indent.store');
                Route::match(['get','post'],'stores-projects/indent/edit-indent/{pid}/{sid}/{id}', ['uses' => 'ManageStoresProjectsController@indentEdit'])->name('stores.projects.indent.edit');
                Route::post('stores-projects/indent/update-indent/{id}', ['uses' => 'ManageStoresProjectsController@indentUpdate'])->name('stores.projects.indent.update');
                Route::delete('stores-projects/indent/destroy-indent/{id}', ['uses' => 'ManageStoresProjectsController@indentDestroy'])->name('stores.projects.indent.destroy');
                Route::get('stores-indent/data/{pid}/{sid?}', ['uses' => 'ManageStoresProjectsController@indentData'])->name('stores.projects.indent.data');
                Route::post('stores-indent/brand', ['uses' => 'ManageStoresProjectsController@indentBrand'])->name('stores.projects.indent.brand');

                Route::get('stores-projects/stores/{id}', 'ManageStoresProjectsController@stores')->name('stores.projects.stores');
                Route::get('stores-projects/indents/{pid}/{sid}', 'ManageStoresProjectsController@indents')->name('stores.projects.indents');
                Route::get('stores-projects/rfqs/{pid}/{sid}', 'ManageStoresProjectsController@rfqs')->name('stores.projects.rfqs');
                Route::get('stores-projects/quotations/{pid}/{sid}', 'ManageStoresProjectsController@quotations')->name('stores.projects.quotations');
                Route::get('stores-projects/{id}/purchase-orders', 'ManageStoresProjectsController@purchaseOrders')->name('stores.projects.purchaseOrders');

                Route::match(['get','post'],'stores-projects/product-issue/{pid}/{sid}', 'ManageStoresProjectsController@productIssue')->name('stores.projects.productIssue');
                Route::match(['get','post'],'stores-projects/product-log/{pid}/{sid}', 'ManageStoresProjectsController@productLog')->name('stores.projects.productLog');
                Route::match(['get','post'],'stores-projects/product-wise-log/{pid}/{sid}/{id}', 'ManageStoresProjectsController@productWiseLog')->name('stores.projects.productWiseLog');
                Route::match(['get','post'],'stores-projects/update-stock/{pid}/{sid}/{cid}/{id}', 'ManageStoresProjectsController@updateStock')->name('stores.projects.updateStock');
                Route::match(['get','post'],'stores-projects/store-update-stock/{pid}/{sid}/{id}', 'ManageStoresProjectsController@storeUpdateStock')->name('stores.projects.storeUpdateStock');
                Route::match(['get','post'],'stores-projects/product-log-data/{pid}/{sid}', 'ManageStoresProjectsController@productLogData')->name('stores.projects.productLogData');
                Route::match(['get','post'],'stores-projects/storeIssuedProduct/', ['uses' => 'ManageStoresProjectsController@storeIssuedProduct'])->name('stores.projects.storeIssuedProduct');
                Route::match(['get','post'],'stores-projects/createIssuedProduct/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@createIssuedProduct'])->name('stores.projects.createIssuedProduct');
                Route::match(['get','post'],'stores-projects/issuedProductDetail/{pid}/{id}', ['uses' => 'ManageStoresProjectsController@issuedProductDetail'])->name('stores.projects.issuedProductDetail');
                Route::match(['get','post'],'stores-projects/returnIssuedProduct/{pid}/{sid}/{id}', ['uses' => 'ManageStoresProjectsController@returnIssuedProduct'])->name('stores.projects.returnIssuedProduct');
                Route::match(['get','post'],'stores-projects/getItemLavel', ['uses' => 'ManageStoresProjectsController@getItemLavel'])->name('stores.projects.getItemLavel');
                Route::match(['get','post'],'stores-projects/applyIssuedFilter/{pid}/{sid}', ['uses' => 'ManageStoresProjectsController@applyIssuedFilter'])->name('stores.projects.applyIssuedFilter');
                Route::match(['get','post'],'stores-projects/getIndentData', ['uses' => 'ManageStoresProjectsController@getIndentData'])->name('stores.projects.getIndentData');
                Route::match(['get','post'],'stores-projects/productData', ['uses' => 'ManageStoresProjectsController@productData'])->name('stores.projects.productData');
                Route::match(['get','post'],'stores-projects/productPopup', ['uses' => 'ManageStoresProjectsController@productPopup'])->name('stores.projects.productPopup');
                Route::match(['get','post'],'stores-projects/convert-product-issue/{pid}/{sid}/{id}', 'ManageStoresProjectsController@convertProductIssue')->name('stores.projects.convertProductIssue');
                Route::match(['get','post'],'stores-projects/storeReturnedProduct/', ['uses' => 'ManageStoresProjectsController@storeReturnedProduct'])->name('stores.projects.storeReturnedProduct');

                Route::get('rfq/data/{id?}', ['uses' => 'MemberRfqController@data'])->name('rfq.data');
                Route::get('rfq/link-data/{id}', ['uses' => 'MemberRfqController@linkData'])->name('rfq.linkData');
            /*    Route::get('rfq/create/{clientID?}', ['uses' => 'MemberRfqController@create'])->name('rfq.create');*/
                Route::get('rfq/create-new/{id?}', ['uses' => 'MemberRfqController@createNew'])->name('rfq.createNew');
                Route::get('rfq/export', ['uses' => 'MemberRfqController@export'])->name('rfq.export');
                Route::post('rfq/get-brands', ['uses' => 'MemberRfqController@getBrands'])->name('rfq.getBrands');
                Route::post('rfq/store-tmp', ['uses' => 'MemberRfqController@storeTmp'])->name('rfq.storeTmp');
                Route::post('rfq/delete-tmp', ['uses' => 'MemberRfqController@deleteTmp'])->name('rfq.deleteTmp');
                Route::get('rfq/rfq-link/{id}', ['uses' => 'MemberRfqController@rfqLink'])->name('rfq.rfqLink');

                //Store-Rfq
                Route::get('stores-rfq/data/{id?}', ['uses' => 'MemberStoreRfqController@data'])->name('stores-rfq.data');
                Route::get('stores-rfq/link-data/{id}', ['uses' => 'MemberStoreRfqController@linkData'])->name('stores-rfq.linkData');
               /* Route::get('stores-rfq/create/{clientID?}', ['uses' => 'MemberStoreRfqController@create'])->name('stores-rfq.create');*/
                Route::get('stores-rfq/create-new/{id?}', ['uses' => 'MemberStoreRfqController@createNew'])->name('stores-rfq.createNew');
                Route::get('stores-rfq/export', ['uses' => 'MemberStoreRfqController@export'])->name('stores-rfq.export');
                Route::post('stores-rfq/get-brands', ['uses' => 'MemberStoreRfqController@getBrands'])->name('stores-rfq.getBrands');
                Route::post('stores-rfq/store-tmp', ['uses' => 'MemberStoreRfqController@storeTmp'])->name('stores-rfq.storeTmp');
                Route::post('stores-rfq/delete-tmp', ['uses' => 'MemberStoreRfqController@deleteTmp'])->name('stores-rfq.deleteTmp');
                Route::get('stores-rfq/rfq-link/{id}', ['uses' => 'MemberStoreRfqController@rfqLink'])->name('stores-rfq.rfqLink');
                Route::get('stores-rfq/view-rfq/{id}', ['uses' => 'MemberStoreRfqController@viewRfq'])->name('stores-rfq.viewRfq');
                Route::resource('stores-rfq', 'MemberStoreRfqController');

                Route::get('project-template/data', ['uses' => 'ProjectTemplateController@data'])->name('project-template.data');
                Route::get('project-template/detail/{id?}', ['uses' => 'ProjectTemplateController@taskDetail'])->name('project-template.detail');
                Route::resource('project-template', 'ProjectTemplateController');

                Route::post('project-template-members/save-group', ['uses' => 'ProjectMemberTemplateController@storeGroup'])->name('project-template-members.storeGroup');
                Route::resource('project-templatrfqe-member', 'ProjectMemberTemplateController');

                Route::get('project-template-task/data/{templateId?}', ['uses' => 'ProjectTemplateTaskController@data'])->name('project-template-task.data');
                Route::get('project-template-task/detail/{id?}', ['uses' => 'ProjectTemplateTaskController@taskDetail'])->name('project-template-task.detail');
                Route::resource('project-template-task', 'ProjectTemplateTaskController');

                Route::post('projectCategory/store-cat', ['uses' => 'ManageProjectCategoryController@storeCat'])->name('projectCategory.store-cat');
                Route::get('projectCategory/create-cat', ['uses' => 'ManageProjectCategoryController@createCat'])->name('projectCategory.create-cat');
                Route::resource('projectCategory', 'ManageProjectCategoryController');

                Route::get('notices/data', ['uses' => 'ManageNoticesController@data'])->name('notices.data');
                Route::get('notices/export/{startDate}/{endDate}', ['uses' => 'ManageNoticesController@export'])->name('notices.export');
                Route::resource('notices', 'ManageNoticesController');

                Route::get('settings/change-language', ['uses' => 'OrganisationSettingsController@changeLanguage'])->name('settings.change-language');
                Route::resource('settings', 'OrganisationSettingsController', ['only' => ['edit', 'update', 'index', 'change-language']]);

                Route::group(
                    ['prefix' => 'settings'],
                    function () {
                        Route::post('email-settings/sent-test-email', ['uses' => 'EmailNotificationSettingController@sendTestEmail'])->name('email-settings.sendTestEmail');
                        Route::post('email-settings/updateMailConfig', ['uses' => 'EmailNotificationSettingController@updateMailConfig'])->name('email-settings.updateMailConfig');
                         Route::get('email-settings/mail-config', ['uses' => 'EmailNotificationSettingController@mailConfig'])->name('email-settings.mail-config');
                        Route::resource('email-settings', 'EmailNotificationSettingController');
                        Route::resource('profile-settings', 'AdminProfileSettingsController');

                        Route::get('currency/exchange-key', ['uses' => 'CurrencySettingController@currencyExchangeKey'])->name('currency.exchange-key');
                        Route::post('currency/exchange-key-store', ['uses' => 'CurrencySettingController@currencyExchangeKeyStore'])->name('currency.exchange-key-store');
                        Route::resource('currency', 'CurrencySettingController');
                        Route::get('currency/exchange-rate/{currency}', ['uses' => 'CurrencySettingController@exchangeRate'])->name('currency.exchange-rate');
                        Route::get('currency/update/exchange-rates', ['uses' => 'CurrencySettingController@updateExchangeRate'])->name('currency.update-exchange-rates');
                        Route::resource('currency', 'CurrencySettingController');

                        Route::post('theme-settings/activeTheme', ['uses' => 'ThemeSettingsController@activeTheme'])->name('theme-settings.activeTheme');
                        Route::post('theme-settings/roundedTheme', ['uses' => 'ThemeSettingsController@roundedTheme'])->name('theme-settings.roundedTheme');
                        Route::resource('theme-settings', 'ThemeSettingsController');
                        Route::resource('project-settings', 'ProjectSettingsController');

                        // Log time
                        Route::resource('log-time-settings', 'LogTimeSettingsController');
                        Route::resource('task-settings', 'TaskSettingsController',  ['only' => ['index', 'store']]);

                        Route::resource('payment-gateway-credential', 'PaymentGatewayCredentialController');
                        Route::resource('invoice-settings', 'InvoiceSettingController');

                        Route::get('slack-settings/sendTestNotification', ['uses' => 'SlackSettingController@sendTestNotification'])->name('slack-settings.sendTestNotification');
                        Route::post('slack-settings/updateSlackNotification/{id}', ['uses' => 'SlackSettingController@updateSlackNotification'])->name('slack-settings.updateSlackNotification');
                        Route::resource('slack-settings', 'SlackSettingController');

                        Route::get('push-notification-settings/sendTestNotification', ['uses' => 'PushNotificationController@sendTestNotification'])->name('push-notification-settings.sendTestNotification');
                        Route::post('push-notification-settings/updatePushNotification/{id}', ['uses' => 'PushNotificationController@updatePushNotification'])->name('push-notification-settings.updatePushNotification');
                        Route::resource('push-notification-settings', 'PushNotificationController');

                        Route::post('ticket-agents/update-group/{id}', ['uses' => 'TicketAgentsController@updateGroup'])->name('ticket-agents.update-group');
                        Route::resource('ticket-agents', 'TicketAgentsController');
                        Route::resource('ticket-groups', 'TicketGroupsController');

                        Route::get('ticketTypes/createModal', ['uses' => 'TicketTypesController@createModal'])->name('ticketTypes.createModal');
                        Route::resource('ticketTypes', 'TicketTypesController');

                        Route::get('lead-source-settings/createModal', ['uses' => 'LeadSourceSettingController@createModal'])->name('leadSetting.createModal');
                        Route::resource('lead-source-settings', 'LeadSourceSettingController');

//                        Route::get('lead-status-settings/createModal', ['uses' => 'LeadStatusSettingController@createModal'])->name('leadSetting.createModal');
//                        Route::resource('lead-status-settings', 'LeadStatusSettingController');

                        Route::post('lead-agent-settings/create-agent', ['uses' => 'LeadAgentSettingController@storeAgent'])->name('lead-agent-settings.create-agent');
                        Route::resource('lead-agent-settings', 'LeadAgentSettingController');

                        Route::get('offline-payment-setting/createModal', ['uses' => 'OfflinePaymentSettingController@createModal'])->name('offline-payment-setting.createModal');
                        Route::resource('offline-payment-setting', 'OfflinePaymentSettingController');

                        Route::get('ticketChannels/createModal', ['uses' => 'TicketChannelsController@createModal'])->name('ticketChannels.createModal');
                        Route::resource('ticketChannels', 'TicketChannelsController');

                        Route::post('replyTemplates/fetch-template', ['uses' => 'TicketReplyTemplatesController@fetchTemplate'])->name('replyTemplates.fetchTemplate');
                        Route::resource('replyTemplates', 'TicketReplyTemplatesController');

                        Route::resource('attendance-settings', 'AttendanceSettingController');

                        Route::resource('leaves-settings', 'LeavesSettingController');

                        Route::get('data', ['uses' => 'AdminCustomFieldsController@getFields'])->name('custom-fields.data');
                        Route::resource('custom-fields', 'AdminCustomFieldsController');

                        // Message settings
                        Route::resource('message-settings', 'MessageSettingsController');

                        // Storage settings
                        Route::resource('storage-settings', 'StorageSettingsController');

                        // Module settings
                        Route::resource('module-settings', 'ModuleSettingsController');

                        Route::get('gdpr/lead/approve-reject/{id}/{type}', ['uses' => 'GdprSettingsController@approveRejectLead'])->name('gdpr.lead.approve-reject');
                        Route::get('gdpr/approve-reject/{id}/{type}', ['uses' => 'GdprSettingsController@approveReject'])->name('gdpr.approve-reject');
                        Route::get('gdpr/lead/removal-data', ['uses' => 'GdprSettingsController@removalLeadData'])->name('gdpr.lead.removal-data');
                        Route::get('gdpr/removal-data', ['uses' => 'GdprSettingsController@removalData'])->name('gdpr.removal-data');
                        Route::put('gdpr/update-consent/{id}', ['uses' => 'GdprSettingsController@updateConsent'])->name('gdpr.update-consent');
                        Route::get('gdpr/edit-consent/{id}', ['uses' => 'GdprSettingsController@editConsent'])->name('gdpr.edit-consent');
                        Route::delete('gdpr/purpose-delete/{id}', ['uses' => 'GdprSettingsController@purposeDelete'])->name('gdpr.purpose-delete');
                        Route::get('gdpr/consent-data', ['uses' => 'GdprSettingsController@data'])->name('gdpr.purpose-data');
                        Route::post('gdpr/store-consent', ['uses' => 'GdprSettingsController@storeConsent'])->name('gdpr.store-consent');
                        Route::get('gdpr/add-consent', ['uses' => 'GdprSettingsController@AddConsent'])->name('gdpr.add-consent');
                        Route::get('gdpr/consent', ['uses' => 'GdprSettingsController@consent'])->name('gdpr.consent');
                        Route::get('gdpr/right-of-access', ['uses' => 'GdprSettingsController@rightOfAccess'])->name('gdpr.right-of-access');
                        Route::get('gdpr/right-to-informed', ['uses' => 'GdprSettingsController@rightToInformed'])->name('gdpr.right-to-informed');
                        Route::get('gdpr/right-to-data-portability', ['uses' => 'GdprSettingsController@rightToDataPortability'])->name('gdpr.right-to-data-portability');
                        Route::get('gdpr/right-to-erasure', ['uses' => 'GdprSettingsController@rightToErasure'])->name('gdpr.right-to-erasure');
                        Route::resource('gdpr', 'GdprSettingsController',['only' => ['index', 'store']]);
                    });

                        Route::resource('project-members', 'ManageProjectMembersController');
                        Route::post('project-members/invitemember', ['uses' => 'ManageProjectMembersController@inviteMember'])->name('project-members.invitemember');
                        Route::post('project-members/save-group', ['uses' => 'ManageProjectMembersController@storeGroup'])->name('project-members.storeGroup');
                        Route::delete('project-members/invitedestroy/{id}', ['uses' => 'ManageProjectMembersController@inviteDestroy'])->name('project-members.invitedestroy');

                        Route::post('tasks/sort', ['uses' => 'ManageTasksController@sort'])->name('tasks.sort');
                        Route::post('tasks/change-status', ['uses' => 'ManageTasksController@changeStatus'])->name('tasks.changeStatus');
                        Route::get('tasks/check-task/{taskID}', ['uses' => 'ManageTasksController@checkTask'])->name('tasks.checkTask');
                        Route::post('tasks/data/{projectId?}', 'ManageTasksController@data')->name('tasks.data');
                        Route::get('tasks/export/{projectId?}', 'ManageTasksController@export')->name('tasks.export');
                        Route::post('tasks/projecttitles', ['uses' => 'ManageTasksController@projectTitles'])->name('tasks.projecttitles');
                        Route::post('tasks/costitembytitle', ['uses' => 'ManageTasksController@costitemBytitle'])->name('tasks.costitembytitle');
                        Route::resource('tasks', 'ManageTasksController');

                        Route::post('files/store-link', ['uses' => 'ManageProjectFilesController@storeLink'])->name('files.storeLink');
                        Route::get('files/download/{id}', ['uses' => 'ManageProjectFilesController@download'])->name('files.download');
                        Route::get('files/thumbnail', ['uses' => 'ManageProjectFilesController@thumbnailShow'])->name('files.thumbnail');
                        Route::post('files/multiple-upload', ['uses' => 'ManageProjectFilesController@storeMultiple'])->name('files.multiple-upload');
                        Route::resource('files', 'ManageProjectFilesController');

                        Route::get('invoices/download/{id}', ['uses' => 'ManageInvoicesController@download'])->name('invoices.download');
                        Route::get('invoices/create-invoice/{id}', ['uses' => 'ManageInvoicesController@createInvoice'])->name('invoices.createInvoice');
                        Route::resource('invoices', 'ManageInvoicesController');

                        Route::resource('issues', 'ManageIssuesController');

                        Route::post('time-logs/stop-timer/{id}', ['uses' => 'ManageTimeLogsController@stopTimer'])->name('time-logs.stopTimer');
                        Route::get('time-logs/data/{id}', ['uses' => 'ManageTimeLogsController@data'])->name('time-logs.data');
                        Route::post('time-logs/storeImage', ['uses' => 'ManageTimeLogsController@storeImage'])->name('time-logs.storeImage');
                        Route::get('time-logs/comment/{id}', ['uses' => 'ManageTimeLogsController@comment'])->name('time-logs.comment');
                        Route::post('time-logs/commentPost/{id}', ['uses' => 'ManageTimeLogsController@commentPost'])->name('time-logs.commentPost');
                        Route::resource('time-logs', 'ManageTimeLogsController');

                        Route::post('milestone/projectinfo', ['uses' => 'ManageProjectMilestoneController@getProjectsInfo'])->name('milestone.projectinfo');
                        Route::get('milestonelist/{projectId?}/{title?}', ['uses' => 'ManageProjectMilestoneController@milestoneslist'])->name('milestone.milestoneslist');
                        Route::get('milestone/detail/{id}', ['uses' => 'ManageProjectMilestoneController@detail'])->name('milestone.detail');
                        Route::get('milestone/data', ['uses' => 'ManageProjectMilestoneController@data'])->name('milestone.data');
                        Route::get('milestone/editdata/{id}', ['uses' => 'ManageProjectMilestoneController@editdata'])->name('milestone.editdata');
                        Route::resource('milestone', 'ManageProjectMilestoneController');


                Route::post('todo/sort', ['uses' => 'ManageTodoController@sort'])->name('todo.sort');
                Route::post('todo/change-status', ['uses' => 'ManageTodoController@changeStatus'])->name('todo.changeStatus');
                Route::get('todo/members/{projectId}', ['uses' => 'ManageTodoController@membersList'])->name('todo.members');
                Route::get('todo/dependent-todo/{projectId}/{taskId?}', ['uses' => 'ManageTodoController@dependentTaskLists'])->name('todo.dependent-todo');
                Route::get('todo/check-task/{taskID}', ['uses' => 'ManageTodoController@checkTask'])->name('todo.checkTask');
                Route::post('todo/data/{projectId?}', 'ManageTodoController@data')->name('todo.data');
                Route::get('todo/export/{projectId?}', 'ManageTodoController@export')->name('todo.export');
                Route::post('todo/projecttitles', ['uses' => 'ManageTodoController@projectTitles'])->name('todo.projecttitles');
                Route::post('todo/costitembytitle', ['uses' => 'ManageTodoController@costitemBytitle'])->name('todo.costitembytitle');
                Route::post('todo/storeImage', ['uses' => 'ManageTodoController@storeImage'])->name('todo.storeImage');
                Route::delete('todo/removeFile/{id}', ['uses' => 'ManageTodoController@removeFile'])->name('todo.removeFile');
                Route::resource('todo', 'ManageTodoController');
                Route::group(
                    ['prefix' => 'manage-files'],
                    function() {
                        Route::post('manage-files/move-pathtype', ['uses' => 'ManageFilesController@movePathType'])->name('manage-files.movepathtype');
                        Route::post('manage-files/store-link', ['uses' => 'ManageFilesController@storeLink'])->name('manage-files.storeLink');
                        Route::post('manage-files/create-folder', ['uses' => 'ManageFilesController@createFolder'])->name('manage-files.createFolder');
                        Route::post('manage-files/edit-folder', ['uses' => 'ManageFilesController@editFolder'])->name('manage-files.editFolder');
                        Route::get('manage-files/download/{id}', ['uses' => 'ManageFilesController@download'])->name('manage-files.download');
                        Route::get('manage-files/thumbnail', ['uses' => 'ManageFilesController@thumbnailShow'])->name('manage-files.thumbnail');
                        Route::post('manage-files/multiple-upload', ['uses' => 'ManageFilesController@storeMultiple'])->name('manage-files.multiple-upload');
                        Route::post('manage-files/show-name', ['uses' => 'ManageFilesController@showName'])->name('manage-files.showName');
                        Route::post('manage-files/edit-name', ['uses' => 'ManageFilesController@editName'])->name('manage-files.editName');
                        Route::post('manage-files/storeImage', ['uses' => 'ManageFilesController@storeImage'])->name('manage-files.storeImage');
                        Route::resource('manage-files', 'ManageFilesController');
                    }
                );
                Route::group(
                    ['prefix' => 'manage-drawings'],
                    function() {
                        Route::get('manage-drawings/revisionFiles/{id}/{fileid}', ['uses' => 'ManageDrawingsController@revisionFiles'])->name('manage-drawings.revisionFiles');
                        Route::post('manage-drawings/move-pathtype', ['uses' => 'ManageDrawingsController@movePathType'])->name('manage-drawings.movepathtype');
                        Route::post('manage-drawings/store-link', ['uses' => 'ManageDrawingsController@storeLink'])->name('manage-drawings.storeLink');
                        Route::post('manage-drawings/create-folder', ['uses' => 'ManageDrawingsController@createFolder'])->name('manage-drawings.createFolder');
                        Route::post('manage-drawings/edit-folder', ['uses' => 'ManageDrawingsController@editFolder'])->name('manage-drawings.editFolder');
                        Route::get('manage-drawings/download/{id}', ['uses' => 'ManageDrawingsController@download'])->name('manage-drawings.download');
                        Route::get('manage-drawings/thumbnail', ['uses' => 'ManageDrawingsController@thumbnailShow'])->name('manage-drawings.thumbnail');
                        Route::post('manage-drawings/multiple-upload', ['uses' => 'ManageDrawingsController@storeMultiple'])->name('manage-drawings.multiple-upload');
                        Route::post('manage-drawings/show-name', ['uses' => 'ManageDrawingsController@showName'])->name('manage-drawings.showName');
                        Route::post('manage-drawings/edit-name', ['uses' => 'ManageDrawingsController@editName'])->name('manage-drawings.editName');
                        Route::post('manage-drawings/storeImage', ['uses' => 'ManageDrawingsController@storeImage'])->name('manage-drawings.storeImage');
                        Route::resource('manage-drawings', 'ManageDrawingsController');
                    }
                );
                Route::group(
                    ['prefix' => 'manage-photos'],
                    function() {
                        Route::post('manage-photos/move-pathtype', ['uses' => 'ManagePhotosController@movePathType'])->name('manage-photos.movepathtype');
                        Route::post('manage-photos/store-link', ['uses' => 'ManagePhotosController@storeLink'])->name('manage-photos.storeLink');
                        Route::post('manage-photos/create-folder', ['uses' => 'ManagePhotosController@createFolder'])->name('manage-photos.createFolder');
                        Route::post('manage-photos/edit-folder', ['uses' => 'ManagePhotosController@editFolder'])->name('manage-photos.editFolder');
                        Route::get('manage-photos/download/{id}', ['uses' => 'ManagePhotosController@download'])->name('manage-photos.download');
                        Route::get('manage-photos/thumbnail', ['uses' => 'ManagePhotosController@thumbnailShow'])->name('manage-photos.thumbnail');
                        Route::post('manage-photos/multiple-upload', ['uses' => 'ManagePhotosController@storeMultiple'])->name('manage-photos.multiple-upload');
                        Route::post('manage-photos/show-name', ['uses' => 'ManagePhotosController@showName'])->name('manage-photos.showName');
                        Route::post('manage-photos/edit-name', ['uses' => 'ManagePhotosController@editName'])->name('manage-photos.editName');
                        Route::post('manage-photos/storeImage', ['uses' => 'ManagePhotosController@storeImage'])->name('manage-photos.storeImage');
                        Route::resource('manage-photos', 'ManagePhotosController');
                    }
                );

                Route::group(
                    ['prefix' => 'clients'],
                    function() {
                        Route::post('save-consent-purpose-data/{client}', ['uses' => 'ManageClientsController@saveConsentLeadData'])->name('clients.save-consent-purpose-data');
                        Route::get('consent-purpose-data/{client}', ['uses' => 'ManageClientsController@consentPurposeData'])->name('clients.consent-purpose-data');
                        Route::get('gdpr/{id}', ['uses' => 'ManageClientsController@gdpr'])->name('clients.gdpr');
                        Route::get('projects/{id}', ['uses' => 'ManageClientsController@showProjects'])->name('clients.projects');
                        Route::get('invoices/{id}', ['uses' => 'ManageClientsController@showInvoices'])->name('clients.invoices');

                        Route::get('contacts/data/{id}', ['uses' => 'ClientContactController@data'])->name('contacts.data');
                        Route::resource('contacts', 'ClientContactController');
                    });

                Route::get('all-issues/data', ['uses' => 'ManageAllIssuesController@data'])->name('all-issues.data');
                Route::resource('all-issues', 'ManageAllIssuesController');

                Route::get('all-time-logs/members/{projectId}', ['uses' => 'ManageAllTimeLogController@membersList'])->name('all-time-logs.members');
                Route::get('all-time-logs/show-active-timer', ['uses' => 'ManageAllTimeLogController@showActiveTimer'])->name('all-time-logs.show-active-timer');
                Route::get('all-time-logs/export/{startDate?}/{endDate?}/{projectId?}/{employee?}', ['uses' => 'ManageAllTimeLogController@export'])->name('all-time-logs.export');
                Route::post('all-time-logs/data/{startDate?}/{endDate?}/{projectId?}/{employee?}', ['uses' => 'ManageAllTimeLogController@data'])->name('all-time-logs.data');
                Route::post('all-time-logs/stop-timer/{id}', ['uses' => 'ManageAllTimeLogController@stopTimer'])->name('all-time-logs.stopTimer');
                Route::post('all-time-logs/datecal', ['uses' => 'ManageAllTimeLogController@dateCal'])->name('all-time-logs.dateCal');
                Route::resource('all-time-logs', 'ManageAllTimeLogController');



                Route::post('man-power-logs/labourattendancepost', 'ManageManPowerLogController@labourAttendancePost')->name('man-power-logs.labourattendancepost');
                Route::post('man-power-logs/workerattendance', 'ManageManPowerLogController@workerAttendance')->name('man-power-logs.workerattendance');
                Route::post('man-power-logs/workerattendancepost', 'ManageManPowerLogController@workerAttendancePost')->name('man-power-logs.workerattendancepost');
                Route::get('man-power-logs/workercreate/{id}', 'ManageManPowerLogController@workerCreate')->name('man-power-logs.workercreate');
                Route::post('man-power-logs/workercreatepost', 'ManageManPowerLogController@workerCreatePost')->name('man-power-logs.workercreatepost');
                Route::post('man-power-logs/workercreateonetimepost', 'ManageManPowerLogController@workerCreateOneTimePost')->name('man-power-logs.workercreateonetimepost');
                Route::get('man-power-logs/workercreateonetime/{id}', 'ManageManPowerLogController@workerCreateOneTime')->name('man-power-logs.workercreateonetime');
                Route::post('man-power-logs/workerlist', 'ManageManPowerLogController@workerList')->name('man-power-logs.workerlist');
                Route::post('man-power-logs/removeattendance', 'ManageManPowerLogController@removeAttendance')->name('man-power-logs.removeattendance');
                Route::get('man-power-logs/workerallowncess/{attendance}', ['uses' => 'ManageManPowerLogController@workerAllowncess'])->name('man-power-logs.workerallowncess');
                Route::post('man-power-logs/allowanceattendancepost', ['uses' => 'ManageManPowerLogController@workerAllowncessPost'])->name('man-power-logs.allowanceattendancepost');
                Route::get('man-power-logs/members/{projectId}', ['uses' => 'ManageManPowerLogController@membersList'])->name('man-power-logs.members');
                Route::get('man-power-logs/show-active-timer', ['uses' => 'ManageManPowerLogController@showActiveTimer'])->name('man-power-logs.show-active-timer');
                Route::get('man-power-logs/export/{startDate?}/{endDate?}/{projectId?}/{employee?}', ['uses' => 'ManageManPowerLogController@export'])->name('man-power-logs.export');
                Route::get('man-power-logs/exportreport/{projectId?}/{subproject?}/{segment?}/{month?}/{contractor?}', ['uses' => 'ManageManPowerLogController@exportReport'])->name('man-power-logs.exportreport');
                Route::post('man-power-logs/data/{startDate?}/{endDate?}/{projectId?}/{employee?}', ['uses' => 'ManageManPowerLogController@data'])->name('man-power-logs.data');
                Route::post('man-power-logs/stop-timer/{id}', ['uses' => 'ManageManPowerLogController@stopTimer'])->name('man-power-logs.stopTimer');
                Route::post('man-power-logs/storedata', ['uses' => 'ManageManPowerLogController@storedata'])->name('man-power-logs.storedata');
                Route::post('man-power-logs/storeImage', ['uses' => 'ManageManPowerLogController@storeImage'])->name('man-power-logs.storeImage');
                Route::get('man-power-logs/manpower-report', ['uses' => 'ManageManPowerLogController@manpowerReport'])->name('man-power-logs.man-report');
                Route::post('man-power-logs/manpower-report-html', ['uses' => 'ManageManPowerLogController@manpowerReportHtml'])->name('man-power-logs.reporthtml');
                Route::post('man-power-logs/manpower-report-pdf', ['uses' => 'ManageManPowerLogController@manpowerReportPdf'])->name('man-power-logs.manpowerReportPdf');
                Route::get('man-power-logs/reply/{taskid}', ['uses' => 'ManageManPowerLogController@reply'])->name('man-power-logs.reply');
                Route::post('man-power-logs/replyPost/{taskid}', ['uses' => 'ManageManPowerLogController@replyPost'])->name('man-power-logs.replyPost');
                Route::delete('man-power-logs/removeFile/{id}', ['uses' => 'ManageManPowerLogController@removeFile'])->name('man-power-logs.removeFile');
                Route::post('man-power-logs/addrow', ['uses' => 'ManageManPowerLogController@addRow'])->name('man-power-logs.addrow');
                Route::resource('man-power-logs', 'ManageManPowerLogController');

                Route::resource('manpower-category', 'ManageManpowerCategoryController');

                Route::get('weather-logs/members/{projectId}', ['uses' => 'ManageWeatherLogController@membersList'])->name('weather-logs.members');
                Route::get('weather-logs/show-active-timer', ['uses' => 'ManageWeatherLogController@showActiveTimer'])->name('weather-logs.show-active-timer');
                Route::get('weather-logs/export/{startDate?}/{endDate?}/{projectId?}/{employee?}', ['uses' => 'ManageWeatherLogController@export'])->name('weather-logs.export');
                Route::post('weather-logs/data', ['uses' => 'ManageWeatherLogController@data'])->name('weather-logs.data');
                Route::post('weather-logs/stop-timer/{id}', ['uses' => 'ManageWeatherLogController@stopTimer'])->name('weather-logs.stopTimer');
                Route::post('weather-logs/storeImage', ['uses' => 'ManageWeatherLogController@storeImage'])->name('weather-logs.storeImage');
                Route::resource('weather-logs', 'ManageWeatherLogController');

                // task routes
//                Route::resource('tasks', 'ManageAllTasksController', ['only' => ['edit', 'update', 'index']]); // hack to make left admin menu item active


                        Route::post('taskCategory/store-cat', ['uses' => 'ManageTaskCategoryController@storeCat'])->name('taskCategory.store-cat');
                        Route::get('taskCategory/create-cat', ['uses' => 'ManageTaskCategoryController@createCat'])->name('taskCategory.create-cat');
                        Route::resource('taskCategory', 'ManageTaskCategoryController');


                        Route::get('all-tasks/export/{startDate?}/{endDate?}/{projectId?}/{hideCompleted?}', ['uses' => 'ManageAllTasksController@export'])->name('all-tasks.export');
                        Route::post('all-tasks/data/{startDate?}/{endDate?}/{hideCompleted?}/{projectId?}', ['uses' => 'ManageAllTasksController@data'])->name('all-tasks.data');
                        Route::get('all-tasks/dependent-tasks/{projectId}/{taskId?}', ['uses' => 'ManageAllTasksController@dependentTaskLists'])->name('all-tasks.dependent-tasks');
                        Route::get('all-tasks/members/{projectId}', ['uses' => 'ManageAllTasksController@membersList'])->name('all-tasks.members');
                        Route::get('all-tasks/ajaxCreate/{columnId}', ['uses' => 'ManageAllTasksController@ajaxCreate'])->name('all-tasks.ajaxCreate');
                        Route::get('all-tasks/reminder/{taskid}', ['uses' => 'ManageAllTasksController@remindForTask'])->name('all-tasks.reminder');
                        Route::get('all-tasks/files/{taskid}', ['uses' => 'ManageAllTasksController@showFiles'])->name('all-tasks.show-files');
                        Route::post('all-tasks/projecttitles', ['uses' => 'ManageAllTasksController@projectTitles'])->name('all-tasks.projecttitles');
                        Route::post('all-tasks/costitembytitle', ['uses' => 'ManageAllTasksController@costitemBytitle'])->name('all-tasks.costitembytitle');
                        Route::get('all-tasks/update-task/{id}', ['uses' => 'ManageAllTasksController@updateTask'])->name('all-tasks.updateTask');
                        Route::post('all-tasks/update-task-percentage/{id}', ['uses' => 'ManageAllTasksController@updatePercentage'])->name('all-tasks.updatePercentage');
                       Route::resource('all-tasks', 'ManageAllTasksController');

                        // taskboard resource
                        Route::post('taskboard/updateIndex', ['as' => 'taskboard.updateIndex', 'uses' => 'AdminTaskboardController@updateIndex']);
                        Route::resource('taskboard', 'AdminTaskboardController');

                        Route::get('task-files/download/{id}', ['uses' => 'TaskFilesController@download'])->name('task-files.download');
                        Route::resource('task-files', 'TaskFilesController');

                // task calendar routes
                Route::match(['get','post'],'taskscalender', ['uses' => 'AdminCalendarController@calenderIndex'])->name('taskscalender.index');
                Route::resource('task-calendar', 'AdminCalendarController');
                Route::resource('sticky-note', 'ManageStickyNotesController');


                Route::resource('reports', 'TaskReportController', ['only' => ['edit', 'update', 'index']]); // hack to make left admin menu item active
                Route::group(
                    ['prefix' => 'reports'],
                    function () {
                        Route::get('task-report/data/{startDate?}/{endDate?}/{employeeId?}/{projectId?}', ['uses' => 'TaskReportController@data'])->name('task-report.data');
                        Route::get('task-report/export/{startDate?}/{endDate?}/{employeeId?}/{projectId?}', ['uses' => 'TaskReportController@export'])->name('task-report.export');
                        Route::resource('task-report', 'TaskReportController');
                        Route::resource('time-log-report', 'TimeLogReportController');
                        Route::resource('finance-report', 'FinanceReportController');
                        Route::resource('income-expense-report', 'IncomeVsExpenseReportController');
                        //region Leave Report routes
                        Route::get('leave-report/data/{startDate?}/{endDate?}/{employeeId?}', ['uses' => 'LeaveReportController@data'])->name('leave-report.data');
                        Route::get('leave-report/export/{id?}/{startDate?}/{endDate?}', 'LeaveReportController@export')->name('leave-report.export');
                        Route::get('leave-report/pending-leaves/{id?}', 'LeaveReportController@pendingLeaves')->name('leave-report.pending-leaves');
                        Route::get('leave-report/upcoming-leaves/{id?}', 'LeaveReportController@upcomingLeaves')->name('leave-report.upcoming-leaves');
                        Route::resource('leave-report', 'LeaveReportController');

                        Route::get('attendance-report/{startDate}/{endDate}/{employee}', ['uses' => 'AttendanceReportController@report'])->name('attendance-report.report');
                        Route::get('attendance-report/export/{startDate}/{endDate}/{employee}', ['uses' => 'AttendanceReportController@reportExport'])->name('attendance-report.reportExport');
                        Route::resource('attendance-report', 'AttendanceReportController');
                        //endregion
                    });

                Route::resource('search', 'AdminSearchController');

                Route::resource('finance', 'ManageEstimatesController', ['only' => ['edit', 'update', 'index']]); // hack to make left admin menu item active

                Route::group(
                    ['prefix' => 'finance'],
                    function () {
                        // Estimate routes
                        Route::get('estimates/data', ['uses' => 'ManageEstimatesController@data'])->name('estimates.data');
                        Route::get('estimates/download/{id}', ['uses' => 'ManageEstimatesController@download'])->name('estimates.download');
                        Route::get('estimates/export/{startDate}/{endDate}/{status}', ['uses' => 'ManageEstimatesController@export'])->name('estimates.export');
                        Route::resource('estimates', 'ManageEstimatesController');

                        //Expenses routes
                        Route::get('expenses/data', ['uses' => 'ManageExpensesController@data'])->name('expenses.data');
                        Route::get('expenses/export/{startDate}/{endDate}/{status}/{employee}', ['uses' => 'ManageExpensesController@export'])->name('expenses.export');
                        Route::resource('expenses', 'ManageExpensesController');

                        // All invoices list routes
                        Route::post('file/store', ['uses' => 'ManageAllInvoicesController@storeFile'])->name('invoiceFile.store');
                        Route::delete('file/destroy', ['uses' => 'ManageAllInvoicesController@destroyFile'])->name('invoiceFile.destroy');
                        Route::get('all-invoices/data', ['uses' => 'ManageAllInvoicesController@data'])->name('all-invoices.data');
                        Route::get('all-invoices/applied-credits/{id}', ['uses' => 'ManageAllInvoicesController@appliedCredits'])->name('all-invoices.applied-credits');
                        Route::post('all-invoices/delete-applied-credit/{id}', ['uses' => 'ManageAllInvoicesController@deleteAppliedCredit'])->name('all-invoices.delete-applied-credit');
                        Route::get('all-invoices/download/{id}', ['uses' => 'ManageAllInvoicesController@download'])->name('all-invoices.download');
                        Route::get('all-invoices/export/{startDate}/{endDate}/{status}/{projectID}', ['uses' => 'ManageAllInvoicesController@export'])->name('all-invoices.export');
                        Route::get('all-invoices/convert-estimate/{id}', ['uses' => 'ManageAllInvoicesController@convertEstimate'])->name('all-invoices.convert-estimate');
                        Route::get('all-invoices/convert-milestone/{id}', ['uses' => 'ManageAllInvoicesController@convertMilestone'])->name('all-invoices.convert-milestone');
                        Route::get('all-invoices/convert-proposal/{id}', ['uses' => 'ManageAllInvoicesController@convertProposal'])->name('all-invoices.convert-proposal');
                        Route::get('all-invoices/update-item', ['uses' => 'ManageAllInvoicesController@addItems'])->name('all-invoices.update-item');
                        Route::get('all-invoices/payment-detail/{invoiceID}', ['uses' => 'ManageAllInvoicesController@paymentDetail'])->name('all-invoices.payment-detail');
                        Route::get('all-invoices/get-client-company/{projectID?}', ['uses' => 'ManageAllInvoicesController@getClientOrCompanyName'])->name('all-invoices.get-client-company');
                        Route::get('all-invoices/get-client/{projectID}', ['uses' => 'ManageAllInvoicesController@getClient'])->name('all-invoices.get-client');
                        Route::get('all-invoices/payment-reminder/{invoiceID}', ['uses' => 'ManageAllInvoicesController@remindForPayment'])->name('all-invoices.payment-reminder');
                        Route::get('all-invoices/payment-verify/{invoiceID}', ['uses' => 'ManageAllInvoicesController@verifyOfflinePayment'])->name('all-invoices.payment-verify');
                        Route::post('all-invoices/payment-verify-submit/{offlinePaymentId}', ['uses' => 'ManageAllInvoicesController@verifyPayment'])->name('offline-invoice-payment.verify');
                        Route::post('all-invoices/payment-reject-submit/{offlinePaymentId}', ['uses' => 'ManageAllInvoicesController@rejectPayment'])->name('offline-invoice-payment.reject');
                        Route::get('all-invoices/update-status/{invoiceID}', ['uses' => 'ManageAllInvoicesController@cancelStatus'])->name('all-invoices.update-status');
                        Route::post('all-invoices/boqinfo', ['uses' => 'ManageAllInvoicesController@boqInfo'])->name('all-invoices.boqinfo');
                        Route::get('all-invoices/record-payments/{id}', ['uses' => 'ManageAllInvoicesController@recordPayments'])->name('all-invoices.record-payments');
                        Route::get('all-invoices/record-payments-edit/{id}', ['uses' => 'ManageAllInvoicesController@recordPaymentsEdit'])->name('all-invoices.record-payments-edit');
                        Route::post('all-invoices/invoice-task-loop', ['uses' => 'ManageAllInvoicesController@invoiceTaskLoop'])->name('all-invoices.invoice-task-loop');
                       Route::post('all-invoices/invoice-record-payment-post', ['uses' => 'ManageAllInvoicesController@invoiceRecordPaymentPost'])->name('all-invoices.invoice-record-payment-post');
                        Route::delete('all-invoices/paymentsdestroy/{id}', ['uses' => 'ManageAllInvoicesController@PaymentDestory'])->name('all-invoices.paymentsdestroy');

                        Route::get('all-invoices/measurement-sheet/{id}/{invoice?}', ['uses' => 'ManageAllInvoicesController@measurementSheet'])->name('all-invoices.measurement-sheet');
                        Route::post('all-invoices/add-measurement-sheet', ['uses' => 'ManageAllInvoicesController@addQtytoMeasuresheet'])->name('all-invoices.addqtytomeasuresheet');
                        Route::post('all-invoices/remove-measurement-sheet', ['uses' => 'ManageAllInvoicesController@removeQtytoMeasuresheet'])->name('all-invoices.removeqtytomeasuresheet');
                        Route::post('all-invoices/invoice-loop', ['uses' => 'ManageAllInvoicesController@invoiceLoop'])->name('all-invoices.invoice-loop');
                        Route::post('all-invoices/amtcalculation', ['uses' => 'ManageAllInvoicesController@amtCalculation'])->name('all-invoices.amtcalculation');

                        Route::resource('all-invoices', 'ManageAllInvoicesController');
                        Route::get('contract-invoices/update-status/{invoiceID}', ['uses' => 'ManageContractInvoicesController@cancelStatus'])->name('contract-invoices.update-status');
                        Route::post('contract-invoices/boqinfo', ['uses' => 'ManageContractInvoicesController@boqInfo'])->name('contract-invoices.boqinfo');
                        Route::get('contract-invoices/record-payments/{id}', ['uses' => 'ManageContractInvoicesController@recordPayments'])->name('contract-invoices.record-payments');
                        Route::get('contract-invoices/record-payments-edit/{id}', ['uses' => 'ManageContractInvoicesController@recordPaymentsEdit'])->name('contract-invoices.record-payments-edit');
                        Route::post('contract-invoices/invoice-task-loop', ['uses' => 'ManageContractInvoicesController@invoiceTaskLoop'])->name('contract-invoices.invoice-task-loop');
                        Route::post('contract-invoices/invoice-payment-loop', ['uses' => 'ManageContractInvoicesController@invoicePaymentLoop'])->name('contract-invoices.invoice-payment-loop');
                        Route::post('contract-invoices/invoice-record-payment-post', ['uses' => 'ManageContractInvoicesController@invoiceRecordPaymentPost'])->name('contract-invoices.invoice-record-payment-post');
                        Route::delete('contract-invoices/paymentsdestroy/{id}', ['uses' => 'ManageContractInvoicesController@PaymentDestory'])->name('contract-invoices.paymentsdestroy');
                        Route::get('contract-invoices/download/{id}', ['uses' => 'ManageContractInvoicesController@download'])->name('contract-invoices.download');
                        Route::get('contract-invoices/export/{startDate}/{endDate}/{status}/{projectID}', ['uses' => 'ManageContractInvoicesController@export'])->name('contract-invoices.export');
                        Route::get('contract-invoices/data', ['uses' => 'ManageContractInvoicesController@data'])->name('contract-invoices.data');
                        Route::get('contract-invoices/measurement-sheet/{id}/{invoice?}', ['uses' => 'ManageContractInvoicesController@measurementSheet'])->name('contract-invoices.measurement-sheet');
                        Route::post('contract-invoices/add-measurement-sheet', ['uses' => 'ManageContractInvoicesController@addQtytoMeasuresheet'])->name('contract-invoices.addqtytomeasuresheet');
                        Route::post('contract-invoices/remove-measurement-sheet', ['uses' => 'ManageContractInvoicesController@removeQtytoMeasuresheet'])->name('contract-invoices.removeqtytomeasuresheet');
                        Route::post('contract-invoices/get-awarded-contracts', ['uses' => 'ManageContractInvoicesController@getAwardedContracts'])->name('contract-invoices.get-awarded-contracts');
                        Route::post('contract-invoices/amtcalculation', ['uses' => 'ManageContractInvoicesController@amtCalculation'])->name('contract-invoices.amtcalculation');

                        Route::resource('contract-invoices', 'ManageContractInvoicesController');

                        // All Credit Note routes
                        Route::post('credit-file/store', ['uses' => 'ManageAllCreditNotesController@storeFile'])->name('creditNoteFile.store');
                        Route::delete('credit-file/destroy', ['uses' => 'ManageAllCreditNotesController@destroyFile'])->name('creditNoteFile.destroy');
                        Route::get('all-credit-notes/data', ['uses' => 'ManageAllCreditNotesController@data'])->name('all-credit-notes.data');
                        Route::get('all-credit-notes/apply-to-invoice/{id}', ['uses' => 'ManageAllCreditNotesController@applyToInvoiceModal'])->name('all-credit-notes.apply-to-invoice-modal');
                        Route::post('all-credit-notes/apply-to-invoice/{id}', ['uses' => 'ManageAllCreditNotesController@applyToInvoice'])->name('all-credit-notes.apply-to-invoice');
                        Route::get('all-credit-notes/credited-invoices/{id}', ['uses' => 'ManageAllCreditNotesController@creditedInvoices'])->name('all-credit-notes.credited-invoices');
                        Route::post('all-credit-notes/delete-credited-invoice/{id}', ['uses' => 'ManageAllCreditNotesController@deleteCreditedInvoice'])->name('all-credit-notes.delete-credited-invoice');
                        Route::get('all-credit-notes/download/{id}', ['uses' => 'ManageAllCreditNotesController@download'])->name('all-credit-notes.download');
                        Route::get('all-credit-notes/export/{startDate}/{endDate}/{projectID}', ['uses' => 'ManageAllCreditNotesController@export'])->name('all-credit-notes.export');
                        Route::get('all-credit-notes/convert-invoice/{id}', ['uses' => 'ManageAllCreditNotesController@convertInvoice'])->name('all-credit-notes.convert-invoice');
                        // Route::get('all-credit-notes/convert-proposal/{id}', ['uses' => 'ManageAllCreditNotesController@convertProposal'])->name('all-credit-notes.convert-proposal');
                        Route::get('all-credit-notes/update-item', ['uses' => 'ManageAllCreditNotesController@addItems'])->name('all-credit-notes.update-item');
                        Route::get('all-credit-notes/payment-detail/{creditNoteID}', ['uses' => 'ManageAllCreditNotesController@paymentDetail'])->name('all-credit-notes.payment-detail');
                        Route::resource('all-credit-notes', 'ManageAllCreditNotesController');

                        //Payments routes
                        Route::get('payments/export/{startDate}/{endDate}/{status}/{payment}', ['uses' => 'ManagePaymentsController@export'])->name('payments.export');
                        Route::get('payments/data', ['uses' => 'ManagePaymentsController@data'])->name('payments.data');
                        Route::get('payments/pay-invoice/{invoiceId}', ['uses' => 'ManagePaymentsController@payInvoice'])->name('payments.payInvoice');
                        Route::get('payments/download', ['uses' => 'ManagePaymentsController@downloadSample'])->name('payments.downloadSample');
                        Route::post('payments/import', ['uses' => 'ManagePaymentsController@importExcel'])->name('payments.importExcel');
                        Route::resource('payments', 'ManagePaymentsController');
                    });

                    Route::post('tenders/contractor_bidding_product', ['uses' => 'TenderController@contractorBiddingPrice'])->name('tenders.contractor_bidding_product');
                    Route::post('tenders/biddingloop', ['uses' => 'TenderController@tenderBiddingLoop'])->name('tenders.biddingloop');
                    Route::get('tenders/bidding/{id}', ['uses' => 'TenderController@tenderBidding'])->name('tenders.bidding');
                    Route::get('tenders/bidding-list/{id}', ['uses' => 'TenderController@tenderBiddingList'])->name('tenders.bidding-list');
                    Route::get('tenders/bidding-sheet/{id}', ['uses' => 'TenderController@tenderBiddingsheet'])->name('tenders.bidding-sheet');
                    Route::get('tenders/editcostitem/{id}', ['uses' => 'TenderController@editCostItem'])->name('tenders.editcostitem');
                    Route::post('tenders/projecttitles', ['uses' => 'TenderController@projectTitles'])->name('tenders.projecttitles');
                    Route::post('tenders/segmentslist', ['uses' => 'TenderController@segmentList'])->name('tenders.segmentslist');
                    Route::post('tenders/boqcostitems', ['uses' => 'TenderController@boqCostItems'])->name('tenders.boqcostitems');
                    Route::post('tenders/boqrow', ['uses' => 'TenderController@boqItemRow'])->name('tenders.boqrow');
                    Route::post('tenders/boqcatrow', ['uses' => 'TenderController@boqItemCatRow'])->name('tenders.boqcatrow');
                    Route::delete('tenders/boqrowremove/{id}', ['uses' => 'TenderController@boqItemRemoveRow'])->name('tenders.boqrowremove');
                    Route::post('tenders/storeImage', ['uses' => 'TenderController@storeImage'])->name('tenders.storeImage');
                    Route::delete('tenders/removeFile/{id}', ['uses' => 'TenderController@removeFile'])->name('tenders.removeFile');
                    Route::get('tenders/contractorslist/{id}', ['uses' => 'TenderController@contractorsList'])->name('tenders.contractorslist');
                    Route::post('tenders/sendcontractorsmail', ['uses' => 'TenderController@sendContractorsMail'])->name('tenders.sendcontractorsmail');
                    Route::post('tenders/tendersellersubmit', ['uses' => 'TenderController@tenderSellerSubmit'])->name('tenders.tenderSellerSubmit');
                    Route::post('tenders/boqcolchangeposition', ['uses' => 'TenderController@boqChangeColPosition'])->name('tenders.boqcolchangeposition');
                    Route::post('tenders/costitemloop', ['uses' => 'TenderController@costitemLoop'])->name('tenders.costitemloop');
                    Route::delete('tenders/costitemcatremove/{id}', ['uses' => 'TenderController@costitemCatRemove'])->name('tenders.costitem-cat-destroy');
                   Route::resource('tenders', 'TenderController');

                Route::get('awardedcontracts/awarded-contracts', ['uses' => 'AwardedContractorController@awardedContracts'])->name('awardedcontracts.awardedContracts');
                Route::get('awardedcontracts/awarded-contracts-clear', ['uses' => 'AwardedContractorController@awardedContractsclear'])->name('awardedcontracts.awardedContractsclear');
                Route::post('awardedcontracts/storeImage', ['uses' => 'AwardedContractorController@storeImage'])->name('awardedcontracts.storeImage');
                Route::get('awardedcontracts/awarded-data', ['uses' => 'AwardedContractorController@awardedData'])->name('awardedcontracts.awardeddata');
                Route::delete('awardedcontracts/delete-award-contractor/{id}', ['uses' => 'AwardedContractorController@deleteAwardContractor'])->name('awardedcontracts.deleteAwardContractor');
                Route::get('awardedcontracts/awardcreate', ['uses' => 'AwardedContractorController@awardCreate'])->name('awardedcontracts.awardcreate');
                Route::post('awardedcontracts/awardedstore', ['uses' => 'AwardedContractorController@awardedStore'])->name('awardedcontracts.awardedstore');
                Route::get('awardedcontracts/awarded-tender-details/{id}', ['uses' => 'AwardedContractorController@awardedtenderDetails'])->name('awardedcontracts.awardedtenderDetails');
                Route::get('awardedcontracts/awarded-tender-bidding/{id}', ['uses' => 'AwardedContractorController@awardedtenderBidding'])->name('awardedcontracts.awardedtenderBidding');
                Route::get('awardedcontracts/awarded-bidding-products/{id}', ['uses' => 'AwardedContractorController@awardedBiddingProducts'])->name('awardedcontracts.awardedBiddingProducts');
                Route::post('awardedcontracts/projecttitles', ['uses' => 'AwardedContractorController@projectTitles'])->name('awardedcontracts.projecttitles');
                Route::post('awardedcontracts/segmentslist', ['uses' => 'AwardedContractorController@segmentList'])->name('awardedcontracts.segmentslist');
                Route::post('awardedcontracts/boqcostitems', ['uses' => 'AwardedContractorController@boqCostItems'])->name('awardedcontracts.boqcostitems');
                Route::post('awardedcontracts/boqrow', ['uses' => 'AwardedContractorController@boqItemRow'])->name('awardedcontracts.boqrow');
                Route::post('awardedcontracts/boqcatrow', ['uses' => 'AwardedContractorController@boqItemCatRow'])->name('awardedcontracts.boqcatrow');
                Route::delete('awardedcontracts/boqrowremove/{id}', ['uses' => 'AwardedContractorController@boqItemRemoveRow'])->name('awardedcontracts.boqrowremove');
                Route::delete('awardedcontracts/boqrowcatremove/{id}', ['uses' => 'AwardedContractorController@boqItemCatRemoveRow'])->name('awardedcontracts.boqrowcatremove');
                Route::post('awardedcontracts/contractproductsloop', ['uses' => 'AwardedContractorController@contractproductsloop'])->name('awardedcontracts.contractproductsloop');
                Route::post('awardedcontracts/updateboqcostitem', ['uses' => 'AwardedContractorController@updateCostItem'])->name('awardedcontracts.updateboqcostitem');
                Route::resource('awardedcontracts', 'AwardedContractorController');
                //Ticket routes
                Route::get('tickets/export/{startDate?}/{endDate?}/{agentId?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'ManageTicketsController@export'])->name('tickets.export');
                Route::get('tickets/data/{startDate?}/{endDate?}/{agentId?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'ManageTicketsController@data'])->name('tickets.data');
                Route::get('tickets/refresh-count/{startDate?}/{endDate?}/{agentId?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'ManageTicketsController@refreshCount'])->name('tickets.refreshCount');
                Route::get('tickets/reply-delete/{id?}', ['uses' => 'ManageTicketsController@destroyReply'])->name('tickets.reply-delete');

                Route::resource('tickets', 'ManageTicketsController');

                Route::get('ticket-files/download/{id}', ['uses' => 'TicketFilesController@download'])->name('ticket-files.download');
                Route::resource('ticket-files', 'TicketFilesController');

                // User message
                Route::post('chat-menu-list', ['as' => 'user-chat.chat-menu-list', 'uses' => 'AdminChatController@menulist']);
                Route::post('add-member-to-chat', ['as' => 'user-chat.add-member-to-chat', 'uses' => 'AdminChatController@addMemberToChat']);
                Route::post('groupchat-box', ['as' => 'user-chat.groupchat-box', 'uses' => 'AdminChatController@groupchatBox']);
                Route::post('chat-box', ['as' => 'user-chat.chat-box', 'uses' => 'AdminChatController@chatBox']);
                Route::post('chat-messages', ['as' => 'user-chat.chat-messages', 'uses' => 'AdminChatController@chatMessages']);
                Route::post('conversation-chat-messages', ['as' => 'user-chat.conversation-chat-messages', 'uses' => 'AdminChatController@conversationChatMessages']);
                Route::post('message-submit', ['as' => 'user-chat.message-submit', 'uses' => 'AdminChatController@postChatMessage']);
                Route::get('user-search', ['as' => 'user-chat.user-search', 'uses' => 'AdminChatController@getUserSearch']);
                Route::get('group-create', ['as' => 'user-chat.group-create', 'uses' => 'AdminChatController@groupCreate']);
                Route::get('create-group-chat', ['as' => 'user-chat.create-group-chat', 'uses' => 'AdminChatController@createGroupChat']);
                Route::post('group-chat-store', ['as' => 'user-chat.group-chat-store', 'uses' => 'AdminChatController@groupChatStore']);
                Route::post('group-chat-data', ['as' => 'user-chat.group-chat-data', 'uses' => 'AdminChatController@groupChatData']);
                Route::post('group-chat-join-user', ['as' => 'user-chat.group-chat-join-user', 'uses' => 'AdminChatController@groupChatJoinUser']);
                Route::post('group-chat-submit', ['as' => 'user-chat.group-chat-submit', 'uses' => 'AdminChatController@groupChatSubmit']);
                Route::post('chat-storeimage', ['as' => 'user-chat.storeImage', 'uses' => 'AdminChatController@storeImage']);

                Route::post('project-pulse/chat-box', ['uses' => '\App\Http\Controllers\Admin\AdminChatController@projectChatBox'])->name('user-chat.pulse-chat-box');
                Route::post('project-pulse/data', ['uses' => '\App\Http\Controllers\Admin\AdminChatController@projectPulseData'])->name('user-chat.pulse-data');
                Route::post('project-pulse/message-submit', ['uses' => '\App\Http\Controllers\Admin\AdminChatController@projectPulseMessageSubmit'])->name('user-chat.pulse-message-submit');

                Route::resource('user-chat', 'AdminChatController');

                // attendance
                Route::get('attendances/export/{startDate?}/{endDate?}/{employee?}', ['uses' => 'ManageAttendanceController@export'])->name('attendances.export');

                Route::get('attendances/detail', ['uses' => 'ManageAttendanceController@attendanceDetail'])->name('attendances.detail');
                Route::get('attendances/data', ['uses' => 'ManageAttendanceController@data'])->name('attendances.data');
                Route::get('attendances/check-holiday', ['uses' => 'ManageAttendanceController@checkHoliday'])->name('attendances.check-holiday');
                Route::get('attendances/employeeData/{startDate?}/{endDate?}/{userId?}', ['uses' => 'ManageAttendanceController@employeeData'])->name('attendances.employeeData');
                Route::get('attendances/employeeDataByRange/{startDate?}/{endDate?}/{userId?}', ['uses' => 'ManageAttendanceController@employeeDataByRange'])->name('attendances.employeeDataByRange');
                Route::get('attendances/refresh-count/{startDate?}/{endDate?}/{userId?}', ['uses' => 'ManageAttendanceController@refreshCount'])->name('attendances.refreshCount');
                Route::get('attendances/attendance-by-date', ['uses' => 'ManageAttendanceController@attendanceByDate'])->name('attendances.attendanceByDate');
                Route::get('attendances/byDateData', ['uses' => 'ManageAttendanceController@byDateData'])->name('attendances.byDateData');
                Route::post('attendances/dateAttendanceCount', ['uses' => 'ManageAttendanceController@dateAttendanceCount'])->name('attendances.dateAttendanceCount');
                Route::get('attendances/info/{id}', ['uses' => 'ManageAttendanceController@detail'])->name('attendances.info');
                Route::get('attendances/summary', ['uses' => 'ManageAttendanceController@summary'])->name('attendances.summary');
                Route::post('attendances/summaryData', ['uses' => 'ManageAttendanceController@summaryData'])->name('attendances.summaryData');
                Route::post('attendances/storeMark', ['uses' => 'ManageAttendanceController@storeMark'])->name('attendances.storeMark');
                Route::get('attendances/mark/{id}/{day}/{month}/{year}', ['uses' => 'ManageAttendanceController@mark'])->name('attendances.mark');
                Route::get('attendances/attendance-by-camera', ['uses' => 'ManageAttendanceController@byCamera'])->name('attendances.byCamera');
                Route::get('attendances/single-user-attendance', ['uses' => 'ManageAttendanceController@singleAttendance'])->name('attendances.singleAttendance');
                Route::get('attendances/single-data', ['uses' => 'ManageAttendanceController@singleData'])->name('attendances.single-data');
                Route::get('attendances/single-user-date_range_attendance', ['uses' => 'ManageAttendanceController@singleAttendanceDateRange'])->name('attendances.singleAttendanceRange');
                Route::get('attendances/rules', ['uses' => 'ManageAttendanceController@rules'])->name('attendances.rules');
                Route::get('attendances/rulesdata', ['uses' => 'ManageAttendanceController@rulesData'])->name('attendances.rulesdata');
                Route::get('attendances/createrules', ['uses' => 'ManageAttendanceController@createRules'])->name('attendances.createrules');
                Route::get('attendances/editrules/{id}', ['uses' => 'ManageAttendanceController@editRules'])->name('attendances.editrules');
                Route::post('attendances/storeGeneralRules', ['uses' => 'ManageAttendanceController@storeGeneralRules'])->name('attendances.storeGeneralRules');
                Route::post('attendances/updateGeneralRules/{id}', ['uses' => 'ManageAttendanceController@updateGeneralRules'])->name('attendances.updateGeneralRules');
                Route::post('attendances/storeAdvanceRules', ['uses' => 'ManageAttendanceController@storeAdvanceRules'])->name('attendances.storeAdvanceRules');
                Route::post('attendances/updateAdvanceRules/{id}', ['uses' => 'ManageAttendanceController@updateAdvanceRules'])->name('attendances.updateAdvanceRules');
                Route::delete('attendances/deleteRules/{id}', ['uses' => 'ManageAttendanceController@deleteRules'])->name('attendances.deleteRules');
                Route::get('attendances/rules-data', ['uses' => 'ManageAttendanceController@attendaceRulesData'])->name('attendances.attendaceRulesData');
                Route::get('attendances/emp-rules-data', ['uses' => 'ManageAttendanceController@attendaceEmpRulesData'])->name('attendances.attendaceEmpRulesData');
                Route::get('attendances/assignRules', ['uses' => 'ManageAttendanceController@assignRules'])->name('attendances.assignRules');
                Route::post('attendances/postAssignRules', ['uses' => 'ManageAttendanceController@postAssignRules'])->name('attendances.postAssignRules');
                Route::post('/getData', ['uses' => 'ManageAttendanceController@getData'])->name('declaration.getData');
                Route::post('/attendances/updateAttendance/{id}', ['uses' => 'ManageAttendanceController@updateAttendance'])->name('attendances.updateattendance');
                Route::resource('attendances', 'ManageAttendanceController');

                //Event Calendar
                Route::post('events/removeAttendee', ['as' => 'events.removeAttendee', 'uses' => 'AdminEventCalendarController@removeAttendee']);
                Route::resource('events', 'AdminEventCalendarController');

                // Role permission routes
                Route::get('role-permission/clients', ['uses' => 'ManageRolePermissionController@clients'])->name('role-permission.clients');
                Route::get('role-permission/employees', ['uses' => 'ManageRolePermissionController@employees'])->name('role-permission.employees');
                Route::get('role-permission/employeesdata', ['uses' => 'ManageRolePermissionController@employeedata'])->name('role-permission.employeedata');
                Route::get('role-permission/contractors', ['uses' => 'ManageRolePermissionController@contractors'])->name('role-permission.contractors');
                Route::get('role-permission/permission/{id}/{project?}', ['uses' => 'ManageRolePermissionController@permission'])->name('role-permission.permission');
                Route::post('role-permission/save-permission', ['uses' => 'ManageRolePermissionController@savePermission'])->name('role-permission.save-permission');
                Route::post('role-permission/assignAllPermission', ['as' => 'role-permission.assignAllPermission', 'uses' => 'ManageRolePermissionController@assignAllPermission']);
                Route::post('role-permission/removeAllPermission', ['as' => 'role-permission.removeAllPermission', 'uses' => 'ManageRolePermissionController@removeAllPermission']);
                Route::post('role-permission/assignRole', ['as' => 'role-permission.assignRole', 'uses' => 'ManageRolePermissionController@assignRole']);
                Route::post('role-permission/detachRole', ['as' => 'role-permission.detachRole', 'uses' => 'ManageRolePermissionController@detachRole']);
                Route::post('role-permission/storeRole', ['as' => 'role-permission.storeRole', 'uses' => 'ManageRolePermissionController@storeRole']);
                Route::post('role-permission/deleteRole', ['as' => 'role-permission.deleteRole', 'uses' => 'ManageRolePermissionController@deleteRole']);
                Route::get('role-permission/showMembers/{id}', ['as' => 'role-permission.showMembers', 'uses' => 'ManageRolePermissionController@showMembers']);
                Route::resource('role-permission', 'ManageRolePermissionController');

                //Leaves
                Route::post('leaves/leaveAction', ['as' => 'leaves.leaveAction', 'uses' => 'ManageLeavesController@leaveAction']);
                Route::get('leaves/show-reject-modal', ['as' => 'leaves.show-reject-modal', 'uses' => 'ManageLeavesController@rejectModal']);
                Route::post('leave/data/{employeeId?}', ['uses' => 'ManageLeavesController@data'])->name('leave.data');
                Route::get('leave/all-leaves', ['uses' => 'ManageLeavesController@allLeave'])->name('leave.all-leaves');
                Route::get('leaves/pending', ['as' => 'leaves.pending', 'uses' => 'ManageLeavesController@pendingLeaves']);

                Route::get('leave/myleaves', ['uses' => 'ManageLeavesController@myLeaves'])->name('leave.myleaves');
                Route::get('leave/leave-rules', ['uses' => 'ManageLeavesController@leaveRules'])->name('leave.leaverules');
                Route::get('leave/add-leave-rules', ['uses' => 'ManageLeavesController@addLeaveRules'])->name('leave.addLeaveRules');
                Route::get('leave/edit-leave-rules/{id}', ['uses' => 'ManageLeavesController@editLeaveRules'])->name('leave.editLeaveRules');
                Route::post('leave/store-leave-rules', ['uses' => 'ManageLeavesController@storeLeaveRules'])->name('leave.storeLeaveRules');
                Route::post('leave/update-leave-rules', ['uses' => 'ManageLeavesController@updateLeaveRules'])->name('leave.updateLeaveRules');
                Route::delete('leave/delete-leave-rule/{id}', ['uses' => 'ManageLeavesController@deleteLeaveRule'])->name('leave.deleteLeaveRule');
                Route::get('leave/leave-data-ofrules', ['uses' => 'ManageLeavesController@leaveDataOfRules'])->name('leave.leaveDataOfRules');
                Route::get('leave/assign-leave-rules', ['uses' => 'ManageLeavesController@assignLeaveRules'])->name('leave.assignLeaveRules');
                Route::post('leave/post-assign-leave-rules', ['uses' => 'ManageLeavesController@postAssignLeaveRules'])->name('leave.postAssignLeaveRules');
                Route::get('leave/leave-rules-data', ['uses' => 'ManageLeavesController@leavesRulesData'])->name('leave.leavesrulesdata');
                Route::get('leave/leave-rules-settings/{id}', ['uses' => 'ManageLeavesController@leaveRulesSettings'])->name('leave.leaveRulesSettings');
                Route::post('leave/store-leave-rules-settings', ['uses' => 'ManageLeavesController@storeLeaveRulesSettings'])->name('leave.storeLeaveRulesSettings');
                Route::get('leave/create-work-day', ['uses' => 'ManageLeavesController@createWorkDay'])->name('leave.createWorkDay');
                Route::post('leave/store-work-day', ['uses' => 'ManageLeavesController@storeWorkday'])->name('leave.storeWorkday');
                Route::get('leave/work-day-data', ['uses' => 'ManageLeavesController@workWeakData'])->name('leave.workWeakData');
                Route::get('leave/work', ['uses' => 'ManageLeavesController@assignRule'])->name('leave.assignRule');
                Route::post('leave/post-assign-work', ['uses' => 'ManageLeavesController@postAssignRule'])->name('leave.postAssignRule');
                Route::get('leave/leave-balance', ['uses' => 'ManageLeavesController@leaveBalance'])->name('leave.leaveBalance');
                Route::get('leave/leave-balance-data', ['uses' => 'ManageLeavesController@leaveBalanceData'])->name('leave.leaveBalanceData');
                Route::get('leave/leave-rules-settings-list', ['uses' => 'ManageLeavesController@leaveRulesSettingsList'])->name('leave.leaveRulesSettingsList');
                Route::delete('leave/delete-work-day/{id}', ['uses' => 'ManageLeavesController@deleteWorkDay'])->name('leave.deleteWorkDay');
            
                Route::get('leave/leave-rules-settings-data', ['uses' => 'ManageLeavesController@leaveRulesSettingsdata'])->name('leave.leaveRulesSettingsdata');

                Route::get('leave/create-leave-rules', ['uses' => 'ManageLeavesController@createLeaveRules'])->name('leave.createLeaveRules');
                Route::resource('leaves', 'ManageLeavesController');

                Route::resource('leaveType', 'ManageLeaveTypesController');

                //sub task routes
                Route::post('sub-task/changeStatus', ['as' => 'sub-task.changeStatus', 'uses' => 'ManageSubTaskController@changeStatus']);
                Route::resource('sub-task', 'ManageSubTaskController');

                //task comments
                Route::resource('task-comment', 'AdminTaskCommentController');

                //taxes
                Route::resource('taxes', 'TaxSettingsController');

                //region Products Routes
                Route::get('products/data', ['uses' => 'AdminProductController@data'])->name('products.data');
                Route::get('products/export', ['uses' => 'AdminProductController@export'])->name('products.export');
                Route::resource('products', 'AdminProductController');
                //endregion

                //region contracts routes
                Route::get('contracts/data', ['as' => 'contracts.data', 'uses' => 'AdminContractController@data']);
                Route::get('contracts/download/{id}', ['as' => 'contracts.download', 'uses' => 'AdminContractController@download']);
                Route::get('contracts/sign/{id}', ['as' => 'contracts.sign-modal', 'uses' => 'AdminContractController@contractSignModal']);
                Route::post('contracts/sign/{id}', ['as' => 'contracts.sign', 'uses' => 'AdminContractController@contractSign']);
                Route::get('contracts/copy/{id}', ['as' => 'contracts.copy', 'uses' => 'AdminContractController@copy']);
                Route::post('contracts/copy-submit', ['as' => 'contracts.copy-submit', 'uses' => 'AdminContractController@copySubmit']);
                Route::post('contracts/add-discussion/{id}', ['as' => 'contracts.add-discussion', 'uses' => 'AdminContractController@addDiscussion']);
                Route::get('contracts/edit-discussion/{id}', ['as' => 'contracts.edit-discussion', 'uses' => 'AdminContractController@editDiscussion']);
                Route::post('contracts/update-discussion/{id}', ['as' => 'contracts.update-discussion', 'uses' => 'AdminContractController@updateDiscussion']);
                Route::post('contracts/remove-discussion/{id}', ['as' => 'contracts.remove-discussion', 'uses' => 'AdminContractController@removeDiscussion']);
                Route::resource('contracts', 'AdminContractController');
                //endregion

                //region contracts type routes
                Route::get('contract-type/data', ['as' => 'contract-type.data', 'uses' => 'AdminContractTypeController@data']);
                Route::post('contract-type/type-store', ['as' => 'contract-type.store-contract-type', 'uses' => 'AdminContractTypeController@storeContractType']);
                Route::get('contract-type/type-create', ['as' => 'contract-type.create-contract-type', 'uses' => 'AdminContractTypeController@createContractType']);

                Route::resource('contract-type', 'AdminContractTypeController')->parameters([
                    'contract-type' => 'type'
                ]);
                //endregion

                Route::get('document-type/data', ['as' => 'document-type.data', 'uses' => 'AdminDocumentTypeController@data']);
                Route::post('document-type/type-store', ['as' => 'document-type.store-document-type', 'uses' => 'AdminDocumentTypeController@storeDocumentType']);
                Route::get('document-type/type-create', ['as' => 'document-type.create-document-type', 'uses' => 'AdminDocumentTypeController@createDocumentType']);

                Route::resource('document-type', 'AdminDocumentTypeController')->parameters([
                    'document-type' => 'type'
                ]);
                //region contract renew routes
                Route::get('contract-renew/{id}', ['as' => 'contracts.renew', 'uses' => 'AdminContractRenewController@index']);
                Route::post('contract-renew-submit/{id}', ['as' => 'contracts.renew-submit', 'uses' => 'AdminContractRenewController@renew']);
                Route::post('contract-renew-remove/{id}', ['as' => 'contracts.renew-remove', 'uses' => 'AdminContractRenewController@destroy']);
                //endregion
            });

            Route::group(['middleware' => ['account-setup']], function () {
                Route::post('billing/unsubscribe',  'AdminBillingController@cancelSubscription')->name('billing.unsubscribe');
                Route::post('billing/razorpay-payment',  'AdminBillingController@razorpayPayment')->name('billing.razorpay-payment');
                Route::post('billing/razorpay-subscription',  'AdminBillingController@razorpaySubscription')->name('billing.razorpay-subscription');
                Route::get('billing/data',  'AdminBillingController@data')->name('billing.data');
                Route::get('billing/select-package/{packageID}',  'AdminBillingController@selectPackage')->name('billing.select-package');
                Route::get('billing', 'AdminBillingController@index')->name('billing');
                Route::get('billing/packages', 'AdminBillingController@packages')->name('billing.packages');
                Route::post('billing/payment-stripe', 'AdminBillingController@payment')->name('payments.stripe');
                Route::get('billing/invoice-download/{invoice}', 'AdminBillingController@download')->name('stripe.invoice-download');
                Route::get('billing/razorpay-invoice-download/{id}', 'AdminBillingController@razorpayInvoiceDownload')->name('billing.razorpay-invoice-download');
                Route::get('billing/offline-invoice-download/{id}', 'AdminBillingController@offlineInvoiceDownload')->name('billing.offline-invoice-download');
                Route::get('billing/offline-payment', 'AdminBillingController@offlinePayment')->name('billing.offline-payment');
                Route::post('billing/offline-payment-submit', 'AdminBillingController@offlinePaymentSubmit')->name('billing.offline-payment-submit');

                Route::get('paypal-recurring', array('as' => 'paypal-recurring','uses' => 'AdminPaypalController@payWithPaypalRecurrring',));
                Route::get('paypal-invoice-download/{id}', array('as' => 'paypal.invoice-download','uses' => 'AdminPaypalController@paypalInvoiceDownload',));
                Route::get('paypal-invoice', array('as' => 'paypal-invoice','uses' => 'AdminPaypalController@createInvoice'));

                // route for view/blade file
                Route::get('paywithpaypal', array('as' => 'paywithpaypal','uses' => 'AdminPaypalController@payWithPaypal'));
                // route for post request
                Route::get('paypal/{packageId}/{type}', array('as' => 'paypal','uses' => 'AdminPaypalController@paymentWithpaypal'));
                Route::get('paypal/cancel-agreement', array('as' => 'paypal.cancel-agreement','uses' => 'AdminPaypalController@cancelAgreement'));
                // route for check status responce
                Route::get('paypal', array('as' => 'status','uses' => 'AdminPaypalController@getPaymentStatus'));
            });
            Route::resource('account-setup', 'ManageAccountSetupController');
            Route::post('account-setup/update-invoice/{id}', ['uses' => 'ManageAccountSetupController@updateInvoice'])->name('account-setup.update-invoice');

            Route::post('activity/store-cat', ['uses' => 'ManageBoqCategoryController@storeCat'])->name('activity.store-cat');
            Route::get('activity/create-cat', ['uses' => 'ManageBoqCategoryController@createCat'])->name('activity.create-cat');
            Route::get('activity/edit-cat/{id}', ['uses' => 'ManageBoqCategoryController@editCat'])->name('activity.edit-cat');
            Route::post('activity/update-cat/{id}', ['uses' => 'ManageBoqCategoryController@updateCat'])->name('activity.update-cat');
            Route::resource('activity', 'ManageBoqCategoryController');
            //BOQ CATEGORY

            //task
            Route::post('task/store-cat', ['uses' => 'CostItemsController@storeCat'])->name('task.store-cat');
            Route::get('task/create-cat', ['uses' => 'CostItemsController@createCat'])->name('task.create-cat');
            Route::get('task/edit-cat/{id}', ['uses' => 'CostItemsController@editCat'])->name('task.edit-cat');
            Route::post('task/update-cat/{id}', ['uses' => 'CostItemsController@updateCat'])->name('task.update-cat');
            Route::post('task/get-cost', ['uses' => 'CostItemsController@getCost'])->name('task.getCost');
            Route::match(['get','post'],'task/product-add/{id}', ['uses' => 'CostItemsController@productAdd'])->name('task.product-add');
            Route::match(['get','post'],'task/product-edit/{id}', ['uses' => 'CostItemsController@productEdit'])->name('task.product-edit');
            Route::post('task/update-product/{id}', ['uses' => 'CostItemsController@productUpdate'])->name('task.update-product');
            Route::delete('task/destroy-product/{id}', ['uses' => 'CostItemsController@destroyProduct'])->name('task.destroy-product');
            Route::post('task/store-product/{id}', ['uses' => 'CostItemsController@storeProduct'])->name('task.store-product');
            Route::post('task/name-store', ['uses' => 'CostItemsController@nameStore'])->name('task.namestore');
            Route::post('task/cost-item-list', ['uses' => 'CostItemsController@costItemList'])->name('task.cost-item-list');
            Route::resource('task', 'CostItemsController');

            //product-issue
            /*Route::get('product-issue/index', ['uses' => 'ProductIssueController@index'])->name('product-issue.index');*/
            Route::get('product-issue/return/{id}', ['uses' => 'ProductIssueController@returns'])->name('product-issue.return');
            Route::post('product-issue/storeReturn/', ['uses' => 'ProductIssueController@storeReturn'])->name('product-issue.storeReturn');
            Route::get('product-issue/issuedProductDetail/{id}', ['uses' => 'ProductIssueController@issuedProductDetail'])->name('product-issue.issuedProductDetail');

            Route::resource('product-issue', 'ProductIssueController');
            //Type
            Route::resource('units', 'ManageUnitsController');
            Route::resource('measurement-types', 'ManageMeasurementTypesController');
            Route::resource('types', 'ManageTypesController');
            Route::resource('trades', 'ManageTradesController');
            Route::resource('segments', 'ManageSegmentsController');

            Route::get('boq-template/addBoq/{id}', ['uses' => 'ManageBoqTemplateController@addboq'])->name('boq-template.addboq');
            Route::post('boq-template/boqloop', ['uses' => 'ManageBoqTemplateController@boqLoop'])->name('boq-template.boqloop');
            Route::post('boq-template/BoqLock', ['uses' => 'ManageBoqTemplateController@BoqLock'])->name('boq-template.BoqLock');
            Route::delete('boq-template/taskdestroy/{id}', ['uses' => 'ManageBoqTemplateController@taskDestroy'])->name('boq-template.task-destroy');
            Route::delete('boq-template/activitydestroy/{id}', ['uses' => 'ManageBoqTemplateController@activityDestroy'])->name('boq-template.activity-destroy');
            Route::post('boq-template/addBoqCostItemCategory', ['uses' => 'ManageBoqTemplateController@addBoqCostItemCategory'])->name('boq-template.template-addcostitemcategory');
            Route::post('boq-template/updateboqcostitem', ['uses' => 'ManageBoqTemplateController@updateCostItem'])->name('boq-template.template-updateboqcostitem');
            Route::post('boq-template/addcostitemrow', ['uses' => 'ManageBoqTemplateController@addBoqCostItemRow'])->name('boq-template.template-addcostitemrow');
            Route::post('boq-template/boqcatchangeposition', ['uses' => 'ManageBoqTemplateController@boqChangeCatPosition'])->name('boq-template.template-boqcatchangeposition');
            Route::post('boq-template/boqcolchangeposition', ['uses' => 'ManageBoqTemplateController@boqChangeColPosition'])->name('boq-template.template-boqcolchangeposition');
            Route::post('boq-template/boqcostitemchangeposition', ['uses' => 'ManageBoqTemplateController@boqChangeCostitemPosition'])->name('boq-template.template-boqcostitemchangeposition');
            Route::get('boq-template/data', ['uses' => 'ManageBoqTemplateController@data'])->name('boq-template.data');
            Route::resource('boq-template', 'ManageBoqTemplateController');

            Route::get('product-category/index', ['uses' => 'ProductCategoryController@index'])->name('productCategory.index');
            Route::get('product-category/data', ['uses' => 'ProductCategoryController@data'])->name('productCategory.data');
            Route::get('product-category/add', ['uses' => 'ProductCategoryController@addProduct'])->name('productCategory.addProduct');
            Route::post('product-category/post', ['uses' => 'ProductCategoryController@postProduct'])->name('productCategory.postProduct');
            Route::get('product-category/edit/{id}', ['uses' => 'ProductCategoryController@editProduct'])->name('productCategory.editProduct');
            Route::post('product-category/updatecateogy', ['uses' => 'ProductCategoryController@updateCategory'])->name('productCategory.updateCategory');
            Route::delete('product-category/delete/{id}', ['uses' => 'ProductCategoryController@destroyCategory'])->name('productCategory.destroyCategory');
            Route::resource('product-category', 'ProductCategoryController');

            Route::get('product-brand/index', ['uses' => 'ProductBrandController@index'])->name('productBrand.index');
            Route::get('product-brand/add', ['uses' => 'ProductBrandController@addBrand'])->name('productBrand.addBrand');
            Route::get('product-brand/data', ['uses' => 'ProductBrandController@brandData'])->name('productBrand.data');
            Route::get('product-brand/edit/{id}', ['uses' => 'ProductBrandController@editBrand'])->name('productBrand.editBrand');
            Route::post('product-brand/store', ['uses' => 'ProductBrandController@postBrand'])->name('productBrand.postBrand');
            Route::post('product-brand/update', ['uses' => 'ProductBrandController@updateBrand'])->name('productBrand.updateBrand');
            Route::delete('product-brand/destroy/{id}', ['uses' => 'ProductBrandController@destroyBrand'])->name('productBrand.destroyBrand');
            Route::resource('product-brand', 'ProductBrandController');

            Route::get('issue/export', ['uses' => 'ManagePunchItemController@export'])->name('issue.export');
            Route::post('issue/data', ['uses' => 'ManagePunchItemController@data'])->name('issue.data');
            Route::post('issue/storeImage', ['uses' => 'ManagePunchItemController@storeImage'])->name('issue.storeImage');
            Route::get('issue/dependent-tasks/{projectId}/{taskId?}', ['uses' => 'ManagePunchItemController@dependentTaskLists'])->name('issue.dependent-tasks');
            Route::get('issue/members/{projectId}', ['uses' => 'ManagePunchItemController@membersList'])->name('issue.members');
            Route::get('issue/ajaxCreate/{columnId}', ['uses' => 'ManagePunchItemController@ajaxCreate'])->name('issue.ajaxCreate');
            Route::get('issue/reminder/{taskid}', ['uses' => 'ManagePunchItemController@remindForTask'])->name('issue.reminder');
            Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
            Route::delete('issue/removeFile/{id}', ['uses' => 'ManagePunchItemController@removeFile'])->name('issue.removeFile');
            Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
            Route::get('issue/comment/{taskid}', ['uses' => 'ManagePunchItemController@comment'])->name('issue.comment');
            Route::get('issue/reply/{taskid}', ['uses' => 'ManagePunchItemController@reply'])->name('issue.reply');
            Route::post('issue/replyPost/{taskid}', ['uses' => 'ManagePunchItemController@replyPost'])->name('issue.replyPost');
            Route::post('issue/commentPost/{taskid}', ['uses' => 'ManagePunchItemController@commentPost'])->name('issue.commentPost');
            Route::match(['get','post'],'issue/issuedata', ['uses' => 'ManagePunchItemController@issueData'])->name('issue.issueData');
            Route::resource('issue', 'ManagePunchItemController');

            Route::post('inspection-type/store-type', ['uses' => 'ManageInspectionTypeController@storeType'])->name('inspectionType.storeType');
            Route::get('inspection-type/create-type', ['uses' => 'ManageInspectionTypeController@createType'])->name('inspectionType.createType');
            Route::get('inspection-type/edit-type/{id}', ['uses' => 'ManageInspectionTypeController@editType'])->name('inspectionType.editType');
            Route::post('inspection-type/update-type/{id}', ['uses' => 'ManageInspectionTypeController@updateType'])->name('inspectionType.updateType');
            Route::delete('inspection-type/delete-type/{id}', ['uses' => 'ManageInspectionTypeController@destroyType'])->name('inspectionType.destroyType');
            Route::resource('inspection-type', 'ManageInspectionTypeController');

            Route::post('input-fields/store-fields', ['uses' => 'ManageInputFieldsController@storeFields'])->name('inputFields.storeFields');
            Route::get('input-fields/create-fields', ['uses' => 'ManageInputFieldsController@createFields'])->name('inputFields.createFields');
            Route::get('input-fields/edit-fields/{id}', ['uses' => 'ManageInputFieldsController@editFields'])->name('inputFields.editFields');
            Route::post('input-fields/update-fields/{id}', ['uses' => 'ManageInputFieldsController@updateFields'])->name('inputFields.updateFields');
            Route::delete('input-fields/delete-fields/{id}', ['uses' => 'ManageInputFieldsController@destroyFields'])->name('inputFields.destroyFields');
            Route::resource('input-fields', 'ManageInputFieldsController');

            Route::post('inspection-name/store-name', ['uses' => 'ManageInspectionNameController@storeName'])->name('inspectionName.storeName');
            Route::get('inspection-name/create-name', ['uses' => 'ManageInspectionNameController@createName'])->name('inspectionName.createName');
            Route::match(['get','post'],'inspection-name/form-data', ['uses' => 'ManageInspectionNameController@formdata'])->name('inspectionName.formdata');
            Route::get('inspection-name/edit-name/{id}', ['uses' => 'ManageInspectionNameController@editName'])->name('inspectionName.editName');
            Route::post('inspection-name/update-name/{id}', ['uses' => 'ManageInspectionNameController@updateName'])->name('inspectionName.updateName');
            Route::delete('inspection-name/delete-name/{id}', ['uses' => 'ManageInspectionNameController@destroyName'])->name('inspectionName.destroyName');
            Route::post('inspection-name/store-image', ['uses' => 'ManageInspectionNameController@storeImage'])->name('inspectionName.storeImage');
            Route::get('inspection-name/inspection-answer', ['uses' => 'ManageInspectionNameController@inspectionAnswer'])->name('inspectionName.inspectionAnswer');
            Route::get('inspection-name/inspection-answer-form/{id}', ['uses' => 'ManageInspectionNameController@inspectionAnswerForm'])->name('inspectionName.inspectionAnswerForm');
            Route::match(['get','post'],'inspection-name/inspection-question/{id}', ['uses' => 'ManageInspectionNameController@inspectionQuestion'])->name('inspectionName.inspectionQuestion');
            Route::post('inspection-name/get-data', ['uses' => 'ManageInspectionNameController@getData'])->name('inspectionName.getData');
            Route::post('inspection-name/delete-question', ['uses' => 'ManageInspectionNameController@destroyQuestion'])->name('inspectionName.destroyQuestion'); Route::match(['get','post'],'inspection-name/store-question', ['uses' => 'ManageInspectionNameController@storeQuestion'])->name('inspectionName.storeQuestion');
            Route::match(['get','post'],'inspection-name/store-answer', ['uses' => 'ManageInspectionNameController@storeAnswer'])->name('inspectionName.storeAnswer');
            Route::match(['get','post'],'inspection-name/assign-user', ['uses' => 'ManageInspectionNameController@assignUser'])->name('inspectionName.assignUser');
            Route::match(['get','post'],'inspection-name/assign-user-store', ['uses' => 'ManageInspectionNameController@assignUserStore'])->name('inspectionName.assignUserStore');
            Route::match(['get','post'],'inspection-name/inspection-replies', ['uses' => 'ManageInspectionNameController@inspectionReplies'])->name('inspectionName.inspectionReplies');
            Route::match(['get','post'],'inspection-name/inspection-replies-view/{id}/{userId}', ['uses' => 'ManageInspectionNameController@inspectionRepliesView'])->name('inspectionName.inspectionRepliesView');
            Route::match(['get','post'],'inspection-name/inspection-replies-submit', ['uses' => 'ManageInspectionNameController@inspectionRepliesSubmit'])->name('inspectionName.inspectionRepliesSubmit');
            Route::delete('inspection-name/removeFile/{id}', ['uses' => 'ManageInspectionNameController@removeFile'])->name('inspectionName.removeFile');
            Route::match(['get','post'],'inspection-name/inspection-assigned', ['uses' => 'ManageInspectionNameController@inspectionAssigned'])->name('inspectionName.inspectionAssigned');
            Route::match(['get','post'],'inspection-name/inspection-assigned-data', ['uses' => 'ManageInspectionNameController@inspectionAssignedData'])->name('inspectionName.inspectionAssignedData');
            Route::match(['get','post'],'inspection-name/inspection-assign-employes/{id}', ['uses' => 'ManageInspectionNameController@AssignForm'])->name('inspectionName.inspectionAssignForm');
            Route::post('inspection-name/inspection-assign-submitform', ['uses' => 'ManageInspectionNameController@SubmitAssignForm'])->name('inspectionName.SubmitAssignForm');
            Route::post('inspection-name/assign-status-update', ['uses' => 'ManageInspectionNameController@assignStatusUpdate'])->name('inspectionName.assignStatusUpdate');
            Route::get('inspection-name/observationform/{inspection?}/{questionid?}', ['uses' => 'ManageInspectionNameController@GetObservation'])->name('inspectionName.AddObservation');
            Route::get('inspection-name/observation/edit/{obid?}', ['uses' => 'ManageInspectionNameController@observationEdit'])->name('observations.edit');
            Route::get('inspection-name/observation', ['uses' => 'ManageInspectionNameController@observationIndex'])->name('observations.index');
            Route::post('inspection-name/observation-store', ['uses' => 'ManageInspectionNameController@observationStore'])->name('observations.observationstore');
            Route::post('inspection-name/observation-store-image', ['uses' => 'ManageInspectionNameController@observationstoreImage'])->name('observations.observationstoreImage');
            Route::post('inspection-name/observation-remove-image', ['uses' => 'ManageInspectionNameController@observationremoveImage'])->name('observations.removeFile');
            Route::post('inspection-name/observation-destroy', ['uses' => 'ManageInspectionNameController@observationDestroy'])->name('observations.destroy');
            Route::get('inspection-name/observation-reply/{id?}', ['uses' => 'ManageInspectionNameController@observationReply'])->name('observations.reply');
            Route::post('inspection-name/observation-replypost/{id?}', ['uses' => 'ManageInspectionNameController@observationreplyPost'])->name('observations.replyPost');
            Route::match(['get','post'],'inspection-name/observation-data', ['uses' => 'ManageInspectionNameController@observationData'])->name('observations.observationData');
            Route::resource('inspection-name', 'ManageInspectionNameController');

            /* adminrefi*/

            Route::post('rfi/store-rfi', ['uses' => 'ManageRfiController@storeRfi'])->name('rfi.storeRfi');
            Route::get('rfi/create-rfi', ['uses' => 'ManageRfiController@createRfi'])->name('rfi.createRfi');
            Route::get('rfi/edit-rfi/{id}', ['uses' => 'ManageRfiController@editRfi'])->name('rfi.editRfi');
            Route::post('rfi/update-rfi/{id}', ['uses' => 'ManageRfiController@updateRfi'])->name('rfi.updateRfi');
            Route::delete('rfi/delete-rfi/{id}', ['uses' => 'ManageRfiController@destroyRfi'])->name('rfi.destroyRfi');
            Route::post('rfi/storeImage', ['uses' => 'ManageRfiController@storeImage'])->name('rfi.storeImage');
            Route::delete('rfi/removeFile/{id}', ['uses' => 'ManageRfiController@removeFile'])->name('rfi.removeFile');
            Route::post('rfi/officialresponse', ['uses' => 'ManageRfiController@officialResponse'])->name('rfi.officialResponse');
            Route::get('rfi/details/{id}', ['uses' => 'ManageRfiController@detailsRfi'])->name('rfi.details');
            Route::post('rfi/replyPost/{taskid}', ['uses' => 'ManageRfiController@replyPost'])->name('rfi.replyPost');
            Route::post('rfi/projecttitles', ['uses' => 'ManageRfiController@projectTitles'])->name('rfi.projecttitles');
            Route::post('rfi/costitembytitle', ['uses' => 'ManageRfiController@costitemBytitle'])->name('rfi.costitembytitle');
            Route::match(['get','post'],'rfi/rfidata', ['uses' => 'ManageRfiController@rfiData'])->name('rfi.rfidata');
            Route::resource('rfi', 'ManageRfiController');

            /* adminsubmittals */

            Route::post('submittals/submittalsdata', ['uses' => 'ManageSubmittalsController@submittalsData'])->name('submittals.submittalsdata');
            Route::post('submittals/store-submittals', ['uses' => 'ManageSubmittalsController@storeSubmittals'])->name('submittals.storeSubmittals');
            Route::post('submittals/projecttitles', ['uses' => 'ManageSubmittalsController@projectTitles'])->name('submittals.projecttitles');
            Route::post('submittals/costitembytitle', ['uses' => 'ManageSubmittalsController@costitemBytitle'])->name('submittals.costitembytitle');
            Route::get('submittals/create-submittals', ['uses' => 'ManageSubmittalsController@createSubmittals'])->name('submittals.createSubmittals');
            Route::get('submittals/edit-submittals/{id}', ['uses' => 'ManageSubmittalsController@editSubmittals'])->name('submittals.editSubmittals');
            Route::post('submittals/update-submittals/{id}', ['uses' => 'ManageSubmittalsController@updateSubmittals'])->name('submittals.updateSubmittals');
            Route::delete('submittals/delete-submittals/{id}', ['uses' => 'ManageSubmittalsController@destroySubmittals'])->name('submittals.destroySubmittals');
            Route::post('submittals/storeImage', ['uses' => 'ManageSubmittalsController@storeImage'])->name('submittals.storeImage');
            Route::delete('submittals/removeFile/{id}', ['uses' => 'ManageSubmittalsController@removeFile'])->name('submittals.removeFile');
            Route::post('submittals/removeworkflow', ['uses' => 'ManageSubmittalsController@removeWorkflow'])->name('submittals.removeworkflow');
            Route::post('submittals/officialresponse', ['uses' => 'ManageSubmittalsController@officialResponse'])->name('submittals.officialResponse');
            Route::get('submittals/details/{id}', ['uses' => 'ManageSubmittalsController@detailsSubmittals'])->name('submittals.details');
            Route::post('submittals/replyPost/{taskid}', ['uses' => 'ManageSubmittalsController@replyPost'])->name('submittals.replyPost');
            Route::get('submittals/wrkresponse/{taskid}', ['uses' => 'ManageSubmittalsController@wrkResponse'])->name('submittals.wrkresponse');
            Route::post('submittals/workflowpost/{id}', ['uses' => 'ManageSubmittalsController@workflowPost'])->name('submittals.workflowPost');
            Route::post('submittals/forwardworkflowpost/{id}', ['uses' => 'ManageSubmittalsController@forwardWrkResponsePost'])->name('submittals.forwardwrkresponsepost');
            Route::get('submittals/distributeSubmittals/{id}', ['uses' => 'ManageSubmittalsController@distributeSubmittals'])->name('submittals.distributeSubmittals');
            Route::get('submittals/revisesubmittals/{id}', ['uses' => 'ManageSubmittalsController@revisionSubmittals'])->name('submittals.revisionSubmittals');
            Route::get('submittals/revisesubmittalspost/{id}', ['uses' => 'ManageSubmittalsController@reviseSubmittalsPost'])->name('submittals.reviseSubmittalsPost');
            Route::get('submittals/prepareSubmittals/{sid}/{id}', ['uses' => 'ManageSubmittalsController@prepareSubmittals'])->name('submittals.prepareSubmittals');
            Route::post('submittals/storepreparesubmittals', ['uses' => 'ManageSubmittalsController@storePrepareSubmittals'])->name('submittals.storeprepareSubmittals');
           Route::resource('submittals', 'ManageSubmittalsController');

            Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-workforce.addProduct');
            Route::post('product-type-workforce/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-workforce.storeProduct');
            Route::get('product-type-workforce/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-workforce.editProduct');
            Route::post('product-type-workforce/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-workforce.updateProduct');
            Route::post('product-type-workforce/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-workforce.store-type');
            Route::get('product-type-workforce/create-type', ['uses' => 'ManageProductTypeController@createType'])->name('product-type-workforce.create-type');
            Route::get('product-type-workforce/edit-type/{id}', ['uses' => 'ManageProductTypeController@editType'])->name('product-type-workforce.edit-type');
            Route::post('product-type-workforce/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-workforce.update-type');
            Route::get('product-type-workforce/workforce-index', ['uses' => 'ManageProductTypeController@workforceIndex'])->name('product-type-workforce.workforce-index');
            Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-workforce.addProcuct');
            Route::resource('product-type-workforce', 'ManageProductTypeController');
            Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-equipment.addProduct');
            Route::post('product-type-equipment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-equipment.storeProduct');
            Route::get('product-type-equipment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-equipment.editProduct');
            Route::post('product-type-equipment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-equipment.updateProduct');
            Route::post('product-type-equipment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-equipment.store-type');
            Route::get('product-type-equipment/create-type', ['uses' => 'ManageProductTypeController@createEquipmentType'])->name('product-type-equipment.create-equipment-type');
            Route::get('product-type-equipment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editEquipmentType'])->name('product-type-equipment.edit-equipment-type');
            Route::post('product-type-equipment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-equipment.update-type');
            Route::get('product-type-equipment/equipment-index', ['uses' => 'ManageProductTypeController@equipmentIndex'])->name('product-type-equipment.equipment-index');
            Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-equipment.addProcuct');
            Route::resource('product-type-equipment', 'ManageProductTypeController');
            Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-material.addProduct');
            Route::post('product-type-material/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-material.storeProduct');
            Route::get('product-type-material/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-material.editProduct');
            Route::post('product-type-material/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-material.updateProduct');
            Route::post('product-type-material/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-material.store-type');
            Route::get('product-type-material/create-type', ['uses' => 'ManageProductTypeController@createMaterialType'])->name('product-type-material.create-material-type');
            Route::get('product-type-material/edit-type/{id}', ['uses' => 'ManageProductTypeController@editMaterialType'])->name('product-type-material.edit-material-type');
            Route::post('product-type-material/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-material.update-type');
            Route::get('product-type-material/equipment-index', ['uses' => 'ManageProductTypeController@materialIndex'])->name('product-type-material.material-index');
            Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-material.addProcuct');
            Route::resource('product-type-material', 'ManageProductTypeController');

            Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-commitment.addProduct');
            Route::post('product-type-commitment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-commitment.storeProduct');
            Route::get('product-type-commitment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-commitment.editProduct');
            Route::post('product-type-commitment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-commitment.updateProduct');
            Route::post('product-type-commitment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-commitment.store-type');
            Route::get('product-type-commitment/create-type', ['uses' => 'ManageProductTypeController@createCommitmentType'])->name('product-type-commitment.create-commitment-type');
            Route::get('product-type-commitment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editCommitmentType'])->name('product-type-commitment.edit-commitment-type');
            Route::post('product-type-commitment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-commitment.update-type');
            Route::get('product-type-commitment/commitment-index', ['uses' => 'ManageProductTypeController@commitmentIndex'])->name('product-type-commitment.commitment-index');
            Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-commitment.addProcuct');
            Route::resource('product-type-commitment', 'ManageProductTypeController');

            Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-owner-cost.addProduct');
            Route::post('product-type-owner-cost/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-owner-cost.storeProduct');
            Route::get('product-type-owner-cost/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-owner-cost.editProduct');
            Route::post('product-type-owner-cost/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-owner-cost.updateProduct');
            Route::post('product-type-owner-cost/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-owner-cost.store-type');
            Route::get('product-type-owner-cost/create-type', ['uses' => 'ManageProductTypeController@createOwnerCostType'])->name('product-type-owner-cost.create-owner-cost-type');
            Route::get('product-type-owner-cost/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOwnerCostType'])->name('product-type-owner-cost.edit-owner-cost-type');
            Route::post('product-type-owner-cost/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-owner-cost.update-type');
            Route::get('product-type-owner-cost/owner-cost-index', ['uses' => 'ManageProductTypeController@ownerCostIndex'])->name('product-type-owner-cost.owner-cost-index');
            Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-owner-cost.addProcuct');
            Route::resource('product-type-owner-cost', 'ManageProductTypeController');
            Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-professional.addProduct');
            Route::post('product-type-professional/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-professional.storeProduct');
            Route::get('product-type-professional/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-professional.editProduct');
            Route::post('product-type-professional/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-professional.updateProduct');
            Route::post('product-type-professional/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-professional.store-type');
            Route::get('product-type-professional/create-type', ['uses' => 'ManageProductTypeController@createProfessionalServicesType'])->name('product-type-professional.create-professional-type');
            Route::get('product-type-professional/edit-type/{id}', ['uses' => 'ManageProductTypeController@editProfessionalServicesType'])->name('product-type-professional.edit-professional-type');
            Route::post('product-type-professional/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-professional.update-type');
            Route::get('product-type-professional/professional-index', ['uses' => 'ManageProductTypeController@professionalServicesIndex'])->name('product-type-professional.professional-index');
            Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-professional.addProcuct');
            Route::resource('product-type-professional', 'ManageProductTypeController');

            Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-other.addProduct');
            Route::post('product-type-other/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-other.storeProduct');
            Route::get('product-type-other/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-other.editProduct');
            Route::post('product-type-other/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-other.updateProduct');
            Route::post('product-type-other/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-other.store-type');
            Route::get('product-type-other/create-type', ['uses' => 'ManageProductTypeController@createOtherType'])->name('product-type-other.create-other-type');
            Route::get('product-type-other/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOtherType'])->name('product-type-other.edit-other-type');
            Route::post('product-type-other/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-other.update-type');
            Route::get('product-type-other/other-index', ['uses' => 'ManageProductTypeController@otherIndex'])->name('product-type-other.other-index');
            Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-other.addProcuct');
            Route::resource('product-type-other', 'ManageProductTypeController');

            Route::post('condition/store-condition', ['uses' => 'ManageConditionController@storeCondition'])->name('condition.store-condition');
            Route::get('condition/create-condition', ['uses' => 'ManageConditionController@createCondition'])->name('condition.create-condition');
            Route::get('condition/edit-condition/{id}', ['uses' => 'ManageConditionController@editCondition'])->name('condition.edit-condition');
            Route::post('condition/update-condition/{id}', ['uses' => 'ManageConditionController@updateCondition'])->name('condition.update-condition');
            Route::resource('condition', 'ManageConditionController');

            Route::get('stores/create-new/{id?}', ['uses' => 'MemberStoresController@createNew'])->name('stores.createNew');
            Route::resource('stores', 'MemberStoresController');
//            Route::get('stores/export', ['uses' => 'MemberStoresController@export'])->name('stores.export');

            Route::get('indent/data', ['uses' => 'MemberIndentController@data'])->name('indent.data');
            Route::post('indent/approve/{id}/{val}', ['uses' => 'MemberIndentController@approve'])->name('indent.approve');
//            Route::get('indent/create/{clientID?}', ['uses' => 'MemberIndentController@create'])->name('indent.create');
            Route::resource('indent', 'MemberIndentController');
            Route::post('indent/getStores', ['uses' => 'MemberIndentController@getStores'])->name('indent.getStores');
            Route::post('indent/getBomIndent', ['uses' => 'MemberIndentController@getBomIndent'])->name('indent.getBomProducts');
            Route::get('indent/export', ['uses' => 'MemberIndentController@export'])->name('indent.export');
            Route::get('indent/{id}/convert', ['uses' => 'MemberIndentController@convert'])->name('indent.gconvert');
            Route::patch('indent/{id}/convert', ['uses' => 'MemberIndentController@postConvert'])->name('indent.pconvert');
            Route::get('indent/convert-grn/{id}', ['uses' => 'MemberIndentController@convertGrn'])->name('indent.convertGrn');
            Route::post('indent/post-convert-grn/{id}', ['uses' => 'MemberIndentController@postConvertGrn'])->name('indent.postConvertGrn');
            Route::get('indent/{id}/view-indent', ['uses' => 'MemberIndentController@viewIndent'])->name('indent.viewIndent');
            Route::get('indent/{id}/export-indent', ['uses' => 'MemberIndentController@exportPDF'])->name('indent.exportPDF');
            Route::post('indent/get-brands', ['uses' => 'MemberIndentController@getBrands'])->name('indent.getBrands');
            Route::post('indent/store-tmp', ['uses' => 'MemberIndentController@storeTmp'])->name('indent.storeTmp');
            Route::post('indent/delete-tmp', ['uses' => 'MemberIndentController@deleteTmp'])->name('indent.deleteTmp');
            Route::get('indent/reply/{taskid}', ['uses' => 'MemberIndentController@reply'])->name('indent.reply');
            Route::get('get-temp-data', ['uses' => 'MemberIndentController@getTempdata'])->name('indent.getTempdata');
            Route::post('indent/replyPost/{taskid}', ['uses' => 'MemberIndentController@replyPost'])->name('indent.replyPost');
            Route::delete('indent/removeFile/{id}', ['uses' => 'MemberIndentController@removeFile'])->name('indent.removeFile');
            Route::post('indent/storeImage', ['uses' => 'MemberIndentController@storeImage'])->name('indent.storeImage');
            Route::match(['get','post'],'indent/projectMembers/{projectid?}', ['uses' => 'MemberIndentController@projectMembers'])->name('indent.members');

            Route::get('rfq/data/{id?}', ['uses' => 'MemberRfqController@data'])->name('rfq.data');
            Route::get('rfq/link-data/{id}', ['uses' => 'MemberRfqController@linkData'])->name('rfq.linkData');
//            Route::get('rfq/create/{clientID?}', ['uses' => 'MemberRfqController@create'])->name('rfq.create');
//            Route::get('rfq/create/{clientID?}', ['uses' => 'MemberRfqController@create'])->name('rfq.create');
            Route::get('rfq/create-new/{id?}', ['uses' => 'MemberRfqController@createNew'])->name('rfq.createNew');
            Route::resource('rfq', 'MemberRfqController');
            Route::get('rfq/export', ['uses' => 'MemberRfqController@export'])->name('rfq.export');
            Route::post('rfq/get-brands', ['uses' => 'MemberRfqController@getBrands'])->name('rfq.getBrands');
            Route::post('rfq/store-tmp', ['uses' => 'MemberRfqController@storeTmp'])->name('rfq.storeTmp');
            Route::post('rfq/delete-tmp', ['uses' => 'MemberRfqController@deleteTmp'])->name('rfq.deleteTmp');
            Route::get('rfq/rfq-link/{id}', ['uses' => 'MemberRfqController@rfqLink'])->name('rfq.rfqLink');

            Route::get('quotations/data/{id?}', ['uses' => 'MemberQuotationController@data'])->name('quotations.data');
            Route::get('quotations/show-details/{id}', ['uses' => 'MemberQuotationController@showAll'])->name('quotations.showAll');
            Route::get('quotations/link-data/{id}', ['uses' => 'MemberQuotationController@linkData'])->name('quotations.linkData');
//            Route::get('quotations/create/{clientID?}', ['uses' => 'MemberQuotationController@create'])->name('quotations.create');
            Route::delete('quotations/delete/{id}', ['uses' => 'MemberQuotationController@delete'])->name('quotations.delete');
            Route::resource('quotations', 'MemberQuotationController');
            Route::get('quotations/export', ['uses' => 'MemberQuotationController@export'])->name('quotations.export');

            //PO
            Route::get('purchase-order/data', ['uses' => 'MemberPOController@data'])->name('purchase-order.data');
            Route::delete('purchase-order/delete/{id}', ['uses' => 'MemberPOController@delete'])->name('po.delete');
            Route::post('purchase-order/approve/{id}/{val}', ['uses' => 'MemberPOController@approve'])->name('purchase-order.approve');
            Route::get('purchase-order/links/{poId}', ['uses' => 'MemberPOController@poLinks'])->name('purchase-order.links');
            Route::get('purchase-order/link-data/{id}', ['uses' => 'MemberPOController@linkData'])->name('purchase-order.linkData');
            Route::get('purchase-order/detail/{poId}', ['uses' => 'MemberPOController@viewPO'])->name('purchase-order.viewPO');
            Route::get('purchase-order/print/{poId}', ['uses' => 'MemberPOController@printPO'])->name('purchase-order.printPO');
            Route::post('quotation/generate-po', ['uses' => 'MemberQuotationController@generatePO'])->name('quotations.generatePO');
            Route::post('quotation/submit-po', ['uses' => 'MemberQuotationController@postPO'])->name('quotations.postPO');
            Route::match(['get','post'],'purchase-order/send-email/{id}/{poId}', ['uses' => 'MemberPOController@sendEmail'])->name('purchase-order.sendEmail');
            Route::match(['get','post'],'purchase-order/send-rfq-email/{id}/{poId}/{rfqid}', ['uses' => 'MemberPOController@sendRFQEmail'])->name('purchase-order.sendRFQEmail');
            Route::resource('purchase-order', 'MemberPOController');

            //Resources
            Route::post('get-resource-details', 'ManageResourcesController@getResource')->name('resources.getResource');
            Route::post('calculate', 'ManageResourcesController@calculate')->name('resources.calculate');
            Route::post('sheetcalculate', 'ManageResourcesController@sheetcalculate')->name('resources.sheetcalculate');
            Route::post('validate', 'ManageResourcesController@validateFormula')->name('resources.validate');
            Route::post('resources/removeresource', 'ManageResourcesController@removeResource')->name('resources.remove');
            Route::post('resources/resourceloop', 'ManageResourcesController@resourceLoop')->name('resources.resourceloop');
            Route::resource('resources', 'ManageResourcesController');

            //Combo Sheet
            Route::post('resources/get-combo-sheet/{id}', 'ManageResourcesController@getComboSheet')->name('resources.getComboSheet');
            Route::post('resources/combo-sheet-store', 'ManageResourcesController@getComboSheetStore')->name('resources.combosheetstore');
            Route::post('resources/combo-sheet-remove', 'ManageResourcesController@comboSheetRemove')->name('resources.removecombosheet');
            Route::post('resources/comboloop', 'ManageResourcesController@comboSheetloop')->name('resources.comboloop');

            //Inventory
            Route::get('inventory/data', ['uses' => 'MemberInventoryController@stockData'])->name('inventory.data');
            Route::get('inventory/purchase-data', ['uses' => 'MemberInventoryController@purchaseData'])->name('inventory.purchaseData');
            Route::get('inventory/invoice-data', ['uses' => 'MemberInventoryController@invoiceData'])->name('inventory.invoiceData');
            Route::get('inventory/return-data', ['uses' => 'MemberInventoryController@returnData'])->name('inventory.returnData');
            Route::delete('inventory/purchase-delete/{id}', ['uses' => 'MemberInventoryController@purchaseDelete'])->name('inventory.purchasedestroy');
            Route::get('inventory/add/{poId}', ['uses' => 'MemberInventoryController@add'])->name('inventory.add');
            Route::post('inventory/add/{poId}', ['uses' => 'MemberInventoryController@postAdd'])->name('inventory.postAdd');
            Route::get('inventory/add-new', ['uses' => 'MemberInventoryController@addNew'])->name('inventory.addNew');
            Route::post('inventory/add-new', ['uses' => 'MemberInventoryController@postAddNew'])->name('inventory.postAddNew');
            Route::get('inventory/stock', ['uses' => 'MemberInventoryController@stock'])->name('inventory.stock');
            Route::get('inventory/purchase-history/{bid}/{store?}', ['uses' => 'MemberInventoryController@purchaseHistory'])->name('inventory.purchase-history');
            Route::get('inventory/product-inward', ['uses' => 'MemberInventoryController@inward'])->name('inventory.inward');
            Route::get('inventory/purchase-invoices', ['uses' => 'MemberInventoryController@purchaseInvoices'])->name('inventory.invoices');
            Route::get('inventory/view-invoice/{id}', ['uses' => 'MemberInventoryController@viewInvoice'])->name('inventory.viewInvoice');
            Route::get('inventory/view-grn-against-po/{id}', ['uses' => 'MemberInventoryController@viewGrnAgainstPo'])->name('inventory.viewGrnAgainstPo');
            Route::get('purchase-invoices/make-payment/{id}', ['uses' => 'MemberInventoryController@makePayment'])->name('purchase.payment');
            Route::post('purchase-invoices/make-payment/{id}', ['uses' => 'MemberInventoryController@submitPayment'])->name('purchase.submitPayment');
            Route::delete('purchase-invoices/delete-payment/{id}', ['uses' => 'MemberInventoryController@deletePayment'])->name('payment.destroy');
            Route::get('inventory/product-return/{invId}', ['uses' => 'MemberInventoryController@pReturn'])->name('inventory.return');
            Route::get('inventory/view-return/{invId}', ['uses' => 'MemberInventoryController@viewReturn'])->name('inventory.viewReturn');
            Route::get('inventory/product-returns', ['uses' => 'MemberInventoryController@pReturns'])->name('inventory.returns');
            Route::get('inventory/create-return', ['uses' => 'MemberInventoryController@createReturn'])->name('inventory.createReturn');
            Route::get('inventory/edit-return/{id}', ['uses' => 'MemberInventoryController@editReturn'])->name('inventory.editReturn');
            Route::post('inventory/store-return', ['uses' => 'MemberInventoryController@storeReturn'])->name('inventory.storeReturn');
            Route::post('inventory/getPiProducts', ['uses' => 'MemberInventoryController@getPiProducts'])->name('inventory.getPiProducts');
            Route::post('inventory/product-return/{invId}', ['uses' => 'MemberInventoryController@postReturn'])->name('inventory.postReturn');
            Route::post('inventory/{id}/{val}', ['uses' => 'MemberInventoryController@inventoryApprove'])->name('inventory.approve');
            Route::delete('inventory/{id}', ['uses' => 'MemberInventoryController@invoicedestroy'])->name('inventory.invoicedestroy');
            Route::get('inventory/edit/{id}', ['uses' => 'MemberInventoryController@editGrn'])->name('inventory.edit-grn');
            Route::match(['get','post'],'inventory/inventory-report', ['uses' => 'MemberInventoryController@invReport'])->name('inventory.Report');
            Route::match(['get','post'],'inventory/report-html', ['uses' => 'MemberInventoryController@invReport'])->name('inventory.reportHtml');
            Route::match(['get','post'],'inventory/report-pdf', ['uses' => 'MemberInventoryController@invReportPDF'])->name('inventory.inventoryReportPdf');
            Route::resource('inventory', 'MemberInventoryController');
            Route::post('inventory/store-tmp', ['uses' => 'MemberInventoryController@storeTmp'])->name('inventory.storeTmp');
            Route::post('inventory/delete-tmp', ['uses' => 'MemberInventoryController@deleteTmp'])->name('inventory.deleteTmp');

            //boq product issue apis
            Route::match(['get','post'],'boq-product-issue-index/', ['uses' => 'MemberInventoryController@boqProductIssueIndex'])->name('inventory.boqProductIssueIndex');
            Route::match(['get','post'],'boq-product-issue-data/', ['uses' => 'MemberInventoryController@boqProductIssueData'])->name('inventory.boqProductIssueData');
            Route::match(['get','post'],'boq-product-issue/', ['uses' => 'MemberInventoryController@boqProductIssue'])->name('inventory.boqProductIssue');
            Route::match(['get','post'],'store-boq-product-issue/', ['uses' => 'MemberInventoryController@storeBoqProductIssue'])->name('inventory.storeBoqProductIssue');
            Route::match(['get','post'],'get-title-by-projects-for-boq', ['uses' => 'MemberInventoryController@boqGetTitlesByProjects'])->name('inventory.boqGetTitlesByProjects');
            Route::match(['get','post'],'get-costiem-by-titles-for-boq', ['uses' => 'MemberInventoryController@boqGetCostitemByTitle'])->name('inventory.boqGetCostitemByTitle');

            //GRN Against PO
            Route::get('grn/grn-against-po', ['uses' => 'MemberInventoryController@grnAgainstPo'])->name('inventory.grnAgainstPo');
            Route::get('grn/againstPoData', ['uses' => 'MemberInventoryController@againstPoData'])->name('inventory.againstPoData');
            Route::get('grn/add-new-grn', ['uses' => 'MemberInventoryController@addNewGrn'])->name('inventory.addNewGrn');
            Route::post('grn/add-new-grn', ['uses' => 'MemberInventoryController@postAddNewGrn'])->name('inventory.postAddNewGrn');
            Route::post('grn/get-po-data', ['uses' => 'MemberInventoryController@getPoData'])->name('inventory.getPoData');
            Route::post('grn/storeImage', ['uses' => 'MemberInventoryController@storeImage'])->name('inventory.storeImage');

            //Meetings
            Route::match(['get','post'],'meetings/create-meeting/', ['uses' => 'MeetingsController@createMeeting'])->name('meetings.createMeeting');
            Route::match(['get','post'],'meetings/store-meeting/', ['uses' => 'MeetingsController@storeMeeting'])->name('meetings.storeMeeting');
            Route::match(['get','post'],'meetings/edit-meeting/{id}', ['uses' => 'MeetingsController@editMeeting'])->name('meetings.editMeeting');
            Route::match(['get','post'],'meetings/update-meeting/', ['uses' => 'MeetingsController@updateMeeting'])->name('meetings.updateMeeting');
            Route::match(['get','post'],'meetings/store-image/', ['uses' => 'MeetingsController@storeImage'])->name('meetings.storeImage');
            Route::match(['get','post'],'meetings/view-meeting/{id}', ['uses' => 'MeetingsController@viewMeeting'])->name('meetings.viewMeeting');
            Route::match(['get','post'],'meetings/store-meeting-category/', ['uses' => 'MeetingsController@storeMeetingCategory'])->name('meetings.storeMeetingCategory');
            Route::match(['get','post'],'meetings/store-business-items/', ['uses' => 'MeetingsController@storeBusinessItems'])->name('meetings.storeBusinessItems');
            Route::match(['get','post'],'meetings/store-business-image/', ['uses' => 'MeetingsController@storeBusinessImage'])->name('meetings.storeBusinessImage');
            Route::delete('meetings/meetings-cat-delete/{id}', ['uses' => 'MeetingsController@deleteCat'])->name('meetings.deleteCat');
            Route::match(['get','post'],'meetings/meetings-cat-edit/{id}', ['uses' => 'MeetingsController@editCategory'])->name('meetings.editCategory');
            Route::match(['get','post'],'meetings/meetings-cat-update/{id}', ['uses' => 'MeetingsController@updateCategory'])->name('meetings.updateCategory');
            Route::match(['get','post'],'meetings/meetings-business-item-edit/{id}', ['uses' => 'MeetingsController@editBusinessItem'])->name('meetings.editBusinessItem');
            Route::match(['get','post'],'meetings/meetings-business-item-update/', ['uses' => 'MeetingsController@updateBusinessItem'])->name('meetings.updateBusinessItem');
            Route::match(['get','post'],'meetings/meeting-convert-to-minutes/{id}', ['uses' => 'MeetingsController@meetingConvertToMinute'])->name('meetings.meetingConvertToMinute');
            Route::match(['get','post'],'meetings/meeting-users-status/{id}/{status}', ['uses' => 'MeetingsController@updateUserStatus'])->name('meetings.updateUserStatus');
            Route::match(['get','post'],'meetings/meetings-minutes/', ['uses' => 'MeetingsController@meetingMinutes'])->name('meetings.meetingMinutes');
            Route::match(['get','post'],'meetings/meetings-edit-minutes/{id}', ['uses' => 'MeetingsController@editMinutes'])->name('meetings.editMinutes');
            Route::match(['get','post'],'meetings/meetings-update-minutes/{id}', ['uses' => 'MeetingsController@updateMinutes'])->name('meetings.updateMinutes');
            Route::match(['get','post'],'meetings/meetings-update-status/', ['uses' => 'MeetingsController@updateStatus'])->name('meetings.updateStatus');
            Route::match(['get','post'],'meetings/follow-up-meeting/{id}', ['uses' => 'MeetingsController@followUpMeeting'])->name('meetings.followUpMeeting');
            Route::match(['get','post'],'meetings/distribute-agenda/{id}', ['uses' => 'MeetingsController@distributeAgenda'])->name('meetings.distributeAgenda');
            Route::match(['get','post'],'meetings/add-followup-meeting', ['uses' => 'MeetingsController@addFollowupMeeting'])->name('meetings.addFollowupMeeting');
            Route::match(['get','post'],'meetings/convert-meeting-update', ['uses' => 'MeetingsController@convertMeetingUpdate'])->name('meetings.convertMeetingUpdate');
            Route::match(['get','post'],'meetings/add-business-item/{id}/{mid}', ['uses' => 'MeetingsController@createBusinessItem'])->name('meetings.createBusinessItem');
            Route::match(['get','post'],'meetings/distribute-minute/{id}', ['uses' => 'MeetingsController@distributeMinute'])->name('meetings.distributeMinute');
            Route::post('meetings/distribute-minute-update/{id}', ['uses' => 'MeetingsController@distributeMinutesUpdate'])->name('meetings.distributeMinutesUpdate');
            Route::delete('meetings/meetings-business-delete/{id}', ['uses' => 'MeetingsController@deleteBusinessItem'])->name('meetings.deleteBusinessItem');
            Route::delete('meetings/details-destroy/{id}', ['uses' => 'MeetingsController@detailsDestroy'])->name('meetings.details-destroy');
            Route::resource('meetings', 'MeetingsController');

            //product-trade
            Route::post('product-trade/store-trade', ['uses' => 'ManageProductTradeController@storeTrade'])->name('productTrade.store-trade');
            Route::get('product-trade/create-trade', ['uses' => 'ManageProductTradeController@createTrade'])->name('productTrade.create-trade');
            Route::get('product-trade/edit-trade/{id}', ['uses' => 'ManageProductTradeController@editTrade'])->name('productTrade.edit-trade');
            Route::post('product-trade/update-trade/{id}', ['uses' => 'ManageProductTradeController@updateTrade'])->name('productTrade.update-trade');
            Route::resource('product-trade', 'ManageProductTradeController');


            //Accounts Module
            //    Branch Manage
            Route::get('/accounts/branch', ['uses' => 'BranchController@index', 'as' => 'branch']);
            Route::get('/accounts/branch/show/{id}', ['uses' => 'BranchController@show', 'as' => 'branch.show']);
            Route::get('/accounts/branch/create', ['uses' => 'BranchController@create', 'as' => 'branch.create']);
            Route::post('/accounts/branch/store', ['uses' => 'BranchController@store', 'as' => 'branch.store']);
            Route::get('/accounts/branch/edit/{id}', ['uses' => 'BranchController@edit', 'as' => 'branch.edit']);
            Route::post('/accounts/branch/update/{id}', ['uses' => 'BranchController@update', 'as' => 'branch.update' ]);
            Route::get('/accounts/branch/destroy/{id}', ['uses' => 'BranchController@destroy', 'as' => 'branch.destroy']);
            Route::get('/accounts/branch/pdf/{id}', ['uses' => 'BranchController@pdf', 'as' => 'branch.pdf']);
            Route::get('/accounts/branch/trashed', ['uses' => 'BranchController@trashed', 'as' => 'branch.trashed']);
            Route::get('/accounts/branch/restore/{id}', ['uses' => 'BranchController@restore', 'as' => 'branch.restore']);
            Route::get('/accounts/branch/kill/{id}', ['uses' => 'BranchController@kill', 'as' => 'branch.kill']);
            Route::get('/accounts/branch/active/search', ['uses' => 'BranchController@activeSearch', 'as' => 'branch.active.search']);
            Route::get('/accounts/branch/trashed/search', ['uses' => 'BranchController@trashedSearch', 'as' => 'branch.trashed.search']);
            Route::get('/accounts/branch/active/action', ['uses' => 'BranchController@activeAction', 'as' => 'branch.active.action']);
            Route::get('/accounts/branch/trashed/action', ['uses' => 'BranchController@trashedAction', 'as' => 'branch.trashed.action']);
            //    Branch Manage End

            //    Ledger  Start
            //   Type Start
            Route::get('/accounts/ledger/type', ['uses' => 'IncomeExpenseTypeController@index', 'as' => 'income_expense_type']);
            Route::get('/accounts/ledger/type/show/{id}', ['uses' => 'IncomeExpenseTypeController@show', 'as' => 'income_expense_type.show']);
            Route::get('/accounts/ledger/type/create', ['uses' => 'IncomeExpenseTypeController@create', 'as' => 'income_expense_type.create']);
            Route::post('/accounts/ledger/type/store', ['uses' => 'IncomeExpenseTypeController@store', 'as' => 'income_expense_type.store']);
            Route::get('/accounts/ledger/type/edit/{id}', ['uses' => 'IncomeExpenseTypeController@edit', 'as' => 'income_expense_type.edit']);
            Route::post('/accounts/ledger/type/update/{id}', ['uses' => 'IncomeExpenseTypeController@update', 'as' => 'income_expense_type.update']);
            Route::get('/accounts/ledger/type/destroy/{id}', ['uses' => 'IncomeExpenseTypeController@destroy', 'as' => 'income_expense_type.destroy']);
            Route::get('/accounts/ledger/type/pdf/{id}', ['uses' => 'IncomeExpenseTypeController@pdf', 'as' => 'income_expense_type.pdf']);
            Route::get('/accounts/ledger/type/trashed', ['uses' => 'IncomeExpenseTypeController@trashed', 'as' => 'income_expense_type.trashed']);
            Route::get('/accounts/ledger/type/restore/{id}', ['uses' => 'IncomeExpenseTypeController@restore', 'as' => 'income_expense_type.restore']);
            Route::get('/accounts/ledger/type/kill/{id}', ['uses' => 'IncomeExpenseTypeController@kill', 'as' => 'income_expense_type.kill']);
            Route::get('/accounts/ledger/type/active/search', ['uses' => 'IncomeExpenseTypeController@activeSearch', 'as' => 'income_expense_type.active.search']);
            Route::get('/accounts/ledger/type/trashed/search', ['uses' => 'IncomeExpenseTypeController@trashedSearch', 'as' => 'income_expense_type.trashed.search']);
            Route::get('/accounts/ledger/type/active/action', ['uses' => 'IncomeExpenseTypeController@activeAction', 'as' => 'income_expense_type.active.action']);
            Route::get('/accounts/ledger/type/trashed/action', ['uses' => 'IncomeExpenseTypeController@trashedAction', 'as' => 'income_expense_type.trashed.action']);
            // Type End

            //   Group Start
            Route::get('/accounts/ledger/group', ['uses' => 'IncomeExpenseGroupController@index', 'as' => 'income_expense_group']);
            Route::get('/accounts/ledger/group/show/{id}', ['uses' => 'IncomeExpenseGroupController@show', 'as' => 'income_expense_group.show']);
            Route::get('/accounts/ledger/group/create', ['uses' => 'IncomeExpenseGroupController@create', 'as' => 'income_expense_group.create']);
            Route::post('/accounts/ledger/group/store', ['uses' => 'IncomeExpenseGroupController@store', 'as' => 'income_expense_group.store']);
            Route::get('/accounts/ledger/group/edit/{id}', ['uses' => 'IncomeExpenseGroupController@edit', 'as' => 'income_expense_group.edit']);
            Route::post('/accounts/ledger/group/update/{id}', ['uses' => 'IncomeExpenseGroupController@update', 'as' => 'income_expense_group.update']);
            Route::get('/accounts/ledger/group/destroy/{id}', ['uses' => 'IncomeExpenseGroupController@destroy', 'as' => 'income_expense_group.destroy']);
            Route::get('/accounts/ledger/group/pdf/{id}', ['uses' => 'IncomeExpenseGroupController@pdf', 'as' => 'income_expense_group.pdf']);
            Route::get('/accounts/ledger/group/trashed', ['uses' => 'IncomeExpenseGroupController@trashed', 'as' => 'income_expense_group.trashed']);
            Route::get('/accounts/ledger/group/restore/{id}', ['uses' => 'IncomeExpenseGroupController@restore', 'as' => 'income_expense_group.restore']);
            Route::get('/accounts/ledger/group/kill/{id}', ['uses' => 'IncomeExpenseGroupController@kill', 'as' => 'income_expense_group.kill']);
            Route::get('/accounts/ledger/group/active/search', ['uses' => 'IncomeExpenseGroupController@activeSearch', 'as' => 'income_expense_group.active.search']);
            Route::get('/accounts/ledger/group/trashed/search', ['uses' => 'IncomeExpenseGroupController@trashedSearch', 'as' => 'income_expense_group.trashed.search']);
            Route::get('/accounts/ledger/group/active/action', ['uses' => 'IncomeExpenseGroupController@activeAction', 'as' => 'income_expense_group.active.action']);
            Route::get('/accounts/ledger/group/trashed/action', ['uses' => 'IncomeExpenseGroupController@trashedAction', 'as' => 'income_expense_group.trashed.action']);
            // Group End

            //    ledger - name Start
            Route::get('/accounts/ledger/name', ['uses' => 'IncomeExpenseHeadController@index', 'as' => 'income_expense_head']);
            Route::get('/accounts/ledger/name/show/{id}', ['uses' => 'IncomeExpenseHeadController@show', 'as' => 'income_expense_head.show']);
            Route::get('/accounts/ledger/name/create', ['uses' => 'IncomeExpenseHeadController@create', 'as' => 'income_expense_head.create']);
            Route::post('/accounts/ledger/name/store', ['uses' => 'IncomeExpenseHeadController@store', 'as' => 'income_expense_head.store']);
            Route::post('/accounts/ledger/name/storeImage', ['uses' => 'IncomeExpenseHeadController@storeImage', 'as' => 'income_expense_head.storeImage']);
            Route::get('/accounts/ledger/name/edit/{id}', ['uses' => 'IncomeExpenseHeadController@edit', 'as' => 'income_expense_head.edit']);
            Route::post('/accounts/ledger/name/update/{id}', ['uses' => 'IncomeExpenseHeadController@update', 'as' => 'income_expense_head.update']);
            Route::get('/accounts/ledger/name/destroy/{id}', ['uses' => 'IncomeExpenseHeadController@destroy', 'as' => 'income_expense_head.destroy']);
            Route::get('/accounts/ledger/name/pdf/{id}', ['uses' => 'IncomeExpenseHeadController@pdf', 'as' => 'income_expense_head.pdf']);
            Route::get('/accounts/ledger/name/trashed', ['uses' => 'IncomeExpenseHeadController@trashed', 'as' => 'income_expense_head.trashed']);
            Route::get('/accounts/ledger/name/restore/{id}', ['uses' => 'IncomeExpenseHeadController@restore', 'as' => 'income_expense_head.restore']);
            Route::get('/accounts/ledger/name/kill/{id}', ['uses' => 'IncomeExpenseHeadController@kill', 'as' => 'income_expense_head.kill']);
            Route::get('/accounts/ledger/name/active/search', ['uses' => 'IncomeExpenseHeadController@activeSearch', 'as' => 'income_expense_head.active.search']);
            Route::get('/accounts/ledger/name/trashed/search', ['uses' => 'IncomeExpenseHeadController@trashedSearch', 'as' => 'income_expense_head.trashed.search']);
            Route::get('/accounts/ledger/name/active/action', ['uses' => 'IncomeExpenseHeadController@activeAction', 'as' => 'income_expense_head.active.action']);
            Route::get('/accounts/ledger/name/trashed/action', ['uses' => 'IncomeExpenseHeadController@trashedAction', 'as' => 'income_expense_head.trashed.action']);
            // ledger name End
            //    Ledger  End

            //    Bank Cash Start
            Route::get('/accounts/bank-cash', ['uses' => 'BankCashController@index', 'as' => 'bank_cash']);
            Route::get('/accounts/bank-cash/show/{id}', ['uses' => 'BankCashController@show', 'as' => 'bank_cash.show']);
            Route::get('/accounts/bank-cash/create', ['uses' => 'BankCashController@create', 'as' => 'bank_cash.create']);
            Route::post('/accounts/bank-cash/store', ['uses' => 'BankCashController@store', 'as' => 'bank_cash.store']);
            Route::get('/accounts/bank-cash/edit/{id}', ['uses' => 'BankCashController@edit', 'as' => 'bank_cash.edit']);
            Route::post('/accounts/bank-cash/update/{id}', ['uses' => 'BankCashController@update', 'as' => 'bank_cash.update']);
            Route::get('/accounts/bank-cash/destroy/{id}', ['uses' => 'BankCashController@destroy', 'as' => 'bank_cash.destroy']);
            Route::get('/accounts/bank-cash/pdf/{id}', ['uses' => 'BankCashController@pdf', 'as' => 'bank_cash.pdf']);
            Route::get('/accounts/bank-cash/trashed', ['uses' => 'BankCashController@trashed', 'as' => 'bank_cash.trashed']);
            Route::get('/accounts/bank-cash/restore/{id}', ['uses' => 'BankCashController@restore', 'as' => 'bank_cash.restore']);
            Route::get('/accounts/bank-cash/kill/{id}', ['uses' => 'BankCashController@kill', 'as' => 'bank_cash.kill']);
            Route::get('/accounts/bank-cash/active/search', ['uses' => 'BankCashController@activeSearch', 'as' => 'bank_cash.active.search']);
            Route::get('/accounts/bank-cash/trashed/search', ['uses' => 'BankCashController@trashedSearch', 'as' => 'bank_cash.trashed.search']);
            Route::get('/accounts/bank-cash/active/action', ['uses' => 'BankCashController@activeAction', 'as' => 'bank_cash.active.action']);
            Route::get('/accounts/bank-cash/trashed/action', ['uses' => 'BankCashController@trashedAction', 'as' => 'bank_cash.trashed.action']);
            // Bank Cash End

            //    initial_income_expense_head_balance Start
            Route::get('/accounts/initial-ledger-balance', ['uses' => 'InitialIncomeExpenseHeadBalanceController@index', 'as' => 'initial_income_expense_head_balance']);
            Route::get('/accounts/initial-ledger-balance/show/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@show', 'as' => 'initial_income_expense_head_balance.show']);
            Route::get('/accounts/initial-ledger-balance/create', ['uses' => 'InitialIncomeExpenseHeadBalanceController@create', 'as' => 'initial_income_expense_head_balance.create']);
            Route::post('/accounts/initial-income-expense-head-balance/store', ['uses' => 'InitialIncomeExpenseHeadBalanceController@store', 'as' => 'initial_income_expense_head_balance.store']);
            Route::get('/accounts/initial-ledger-balance/edit/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@edit', 'as' => 'initial_income_expense_head_balance.edit']);
            Route::post('/accounts/initial-ledger-balance/update/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@update', 'as' => 'initial_income_expense_head_balance.update']);
            Route::get('/accounts/initial-ledger-balance/destroy/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@destroy', 'as' => 'initial_income_expense_head_balance.destroy']);
            Route::get('/accounts/initial-ledger-balance/pdf/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@pdf', 'as' => 'initial_income_expense_head_balance.pdf']);
            Route::get('/accounts/initial-ledger-balance/trashed', ['uses' => 'InitialIncomeExpenseHeadBalanceController@trashed', 'as' => 'initial_income_expense_head_balance.trashed']);
            Route::get('/accounts/initial-income-expense-head-balance/restore/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@restore', 'as' => 'initial_income_expense_head_balance.restore']);
            Route::get('/accounts/initial-income-expense-head-balance/kill/{id}', ['uses' => 'InitialIncomeExpenseHeadBalanceController@kill', 'as' => 'initial_income_expense_head_balance.kill']);
            Route::get('/accounts/initial-income-expense-head-balance/active/search', ['uses' => 'InitialIncomeExpenseHeadBalanceController@activeSearch', 'as' => 'initial_income_expense_head_balance.active.search']);
            Route::get('/accounts/initial-income-expense-head-balance/trashed/search', ['uses' => 'InitialIncomeExpenseHeadBalanceController@trashedSearch', 'as' => 'initial_income_expense_head_balance.trashed.search']);
            Route::get('/accounts/initial-income-expense-head-balance/active/action', ['uses' => 'InitialIncomeExpenseHeadBalanceController@activeAction', 'as' => 'initial_income_expense_head_balance.active.action']);
            Route::get('/accounts/initial-income-expense-head-balance/trashed/action', ['uses' => 'InitialIncomeExpenseHeadBalanceController@trashedAction', 'as' => 'initial_income_expense_head_balance.trashed.action']);
            // initial_income_expense_head_balance End

            //    initial_bank_cash_balance Start
            Route::get('/accounts/initial-bank-cash-balance', ['uses' => 'InitialBankCashBalanceController@index', 'as' => 'initial_bank_cash_balance']);
            Route::get('/accounts/initial-bank-cash-balance/show/{id}', ['uses' => 'InitialBankCashBalanceController@show', 'as' => 'initial_bank_cash_balance.show']);
            Route::get('/accounts/initial-bank-cash-balance/create', ['uses' => 'InitialBankCashBalanceController@create', 'as' => 'initial_bank_cash_balance.create']);
            Route::post('/accounts/initial-bank-cash-balance/store', ['uses' => 'InitialBankCashBalanceController@store', 'as' => 'initial_bank_cash_balance.store']);
            Route::get('/accounts/initial-bank-cash-balance/edit/{id}', ['uses' => 'InitialBankCashBalanceController@edit', 'as' => 'initial_bank_cash_balance.edit']);
            Route::post('/accounts/initial-bank-cash-balance/update/{id}', ['uses' => 'InitialBankCashBalanceController@update', 'as' => 'initial_bank_cash_balance.update']);
            Route::get('/accounts/initial-bank-cash-balance/destroy/{id}', ['uses' => 'InitialBankCashBalanceController@destroy', 'as' => 'initial_bank_cash_balance.destroy']);
            Route::get('/accounts/initial-bank-cash-balance/pdf/{id}', ['uses' => 'InitialBankCashBalanceController@pdf', 'as' => 'initial_bank_cash_balance.pdf']);
            Route::get('/accounts/initial-bank-cash-balance/trashed', ['uses' => 'InitialBankCashBalanceController@trashed', 'as' => 'initial_bank_cash_balance.trashed']);
            Route::get('/accounts/initial-bank-cash-balance/restore/{id}', ['uses' => 'InitialBankCashBalanceController@restore', 'as' => 'initial_bank_cash_balance.restore']);
            Route::get('/accounts/initial-bank-cash-balance/kill/{id}', ['uses' => 'InitialBankCashBalanceController@kill', 'as' => 'initial_bank_cash_balance.kill']);
            Route::get('/accounts/initial-bank-cash-balance/active/search', ['uses' => 'InitialBankCashBalanceController@activeSearch', 'as' => 'initial_bank_cash_balance.active.search']);
            Route::get('/accounts/initial-bank-cash-balance/trashed/search', ['uses' => 'InitialBankCashBalanceController@trashedSearch', 'as' => 'initial_bank_cash_balance.trashed.search']);
            Route::get('/accounts/initial-bank-cash-balance/active/action', ['uses' => 'InitialBankCashBalanceController@activeAction', 'as' => 'initial_bank_cash_balance.active.action']);
            Route::get('/accounts/initial-bank-cash-balance/trashed/action', ['uses' => 'InitialBankCashBalanceController@trashedAction', 'as' => 'initial_bank_cash_balance.trashed.action']);
            // initial_bank_cash_balance End

            //  jnl_voucher Start
            Route::get('/accounts/payment-voucher', ['uses' => 'PaymentVoucherController@index', 'as' => 'payment_voucher']);
            Route::get('/accounts/payment-voucher/show/{id}', ['uses' => 'PaymentVoucherController@show', 'as' => 'payment_voucher.show']);
            Route::get('/accounts/payment-voucher/create', ['uses' => 'PaymentVoucherController@create', 'as' => 'payment_voucher.create']);
            Route::post('/accounts/payment-voucher/store', ['uses' => 'PaymentVoucherController@store', 'as' => 'payment_voucher.store']);
            Route::get('/accounts/payment-voucher/edit/{id}', ['uses' => 'PaymentVoucherController@edit', 'as' => 'payment_voucher.edit']);
            Route::post('/accounts/payment-voucher/update/{id}', ['uses' => 'PaymentVoucherController@update', 'as' => 'payment_voucher.update']);
            Route::get('/accounts/payment-voucher/destroy/{id}', ['uses' => 'PaymentVoucherController@destroy', 'as' => 'payment_voucher.destroy']);
            Route::get('/accounts/payment-voucher/pdf/{id}', ['uses' => 'PaymentVoucherController@pdf', 'as' => 'payment_voucher.pdf']);
            Route::get('/accounts/payment-voucher/trashed', ['uses' => 'PaymentVoucherController@trashed', 'as' => 'payment_voucher.trashed']);
            Route::get('/accounts/payment-voucher/restore/{id}', ['uses' => 'PaymentVoucherController@restore', 'as' => 'payment_voucher.restore']);
            Route::get('/accounts/payment-voucher/kill/{id}', ['uses' => 'PaymentVoucherController@kill', 'as' => 'payment_voucher.kill']);
            Route::get('/accounts/payment-voucher/active/search', ['uses' => 'PaymentVoucherController@activeSearch', 'as' => 'payment_voucher.active.search']);
            Route::get('/accounts/payment-voucher/trashed/search', ['uses' => 'PaymentVoucherController@trashedSearch', 'as' => 'payment_voucher.trashed.search']);
            Route::get('/accounts/payment-voucher/active/action', ['uses' => 'PaymentVoucherController@activeAction', 'as' => 'payment_voucher.active.action']);
            Route::get('/accounts/payment-voucher/trashed/action', ['uses' => 'PaymentVoucherController@trashedAction', 'as' => 'payment_voucher.trashed.action']);
            Route::match(['get','post'],'/accounts/payment-voucher/ledgerlog', ['uses' => 'PaymentVoucherController@ledgerLog', 'as' => 'payment_voucher.ledgerlog']);
            Route::get('/accounts/payment-voucher/ledgerlog-data', ['uses' => 'PaymentVoucherController@ledgerLogData', 'as' => 'payment_voucher.ledgerlogdata']);
            // payment_voucher End



            //  DrVoucher Start
            Route::get('/accounts/dr-voucher', ['uses' => 'DrVoucherController@index', 'as' => 'dr_voucher']);
            Route::get('/accounts/dr-voucher/show/{id}', ['uses' => 'DrVoucherController@show', 'as' => 'dr_voucher.show']);
            Route::get('/accounts/dr-voucher/create', ['uses' => 'DrVoucherController@create', 'as' => 'dr_voucher.create']);
            Route::post('/accounts/dr-voucher/store', ['uses' => 'DrVoucherController@store', 'as' => 'dr_voucher.store']);
            Route::get('/accounts/dr-voucher/edit/{id}', ['uses' => 'DrVoucherController@edit', 'as' => 'dr_voucher.edit']);
            Route::post('/accounts/dr-voucher/update/{id}', ['uses' => 'DrVoucherController@update', 'as' => 'dr_voucher.update']);
            Route::get('/accounts/dr-voucher/destroy/{id}', ['uses' => 'DrVoucherController@destroy', 'as' => 'dr_voucher.destroy']);
            Route::get('/accounts/dr-voucher/pdf/{id}', ['uses' => 'DrVoucherController@pdf', 'as' => 'dr_voucher.pdf']);
            Route::get('/accounts/dr-voucher/trashed', ['uses' => 'DrVoucherController@trashed', 'as' => 'dr_voucher.trashed']);
            Route::get('/accounts/dr-voucher/restore/{id}', ['uses' => 'DrVoucherController@restore', 'as' => 'dr_voucher.restore']);
            Route::get('/accounts/dr-voucher/kill/{id}', ['uses' => 'DrVoucherController@kill', 'as' => 'dr_voucher.kill']);
            Route::get('/accounts/dr-voucher/active/search', ['uses' => 'DrVoucherController@activeSearch', 'as' => 'dr_voucher.active.search']);
            Route::get('/accounts/dr-voucher/trashed/search', ['uses' => 'DrVoucherController@trashedSearch', 'as' => 'dr_voucher.trashed.search']);
            Route::get('/accounts/dr-voucher/active/action', ['uses' => 'DrVoucherController@activeAction', 'as' => 'dr_voucher.active.action']);
            Route::get('/accounts/dr-voucher/trashed/action', ['uses' => 'DrVoucherController@trashedAction', 'as' => 'dr_voucher.trashed.action']);
            // DrVoucher End

            //  cr_voucher Start
            Route::get('/accounts/cr-voucher', ['uses' => 'CrVoucherController@index', 'as' => 'cr_voucher']);
            Route::get('/accounts/cr-voucher/show/{id}', ['uses' => 'CrVoucherController@show', 'as' => 'cr_voucher.show']);
            Route::get('/accounts/cr-voucher/create', ['uses' => 'CrVoucherController@create', 'as' => 'cr_voucher.create']);
            Route::post('/accounts/cr-voucher/store', ['uses' => 'CrVoucherController@store', 'as' => 'cr_voucher.store']);
            Route::get('/accounts/cr-voucher/edit/{id}', ['uses' => 'CrVoucherController@edit', 'as' => 'cr_voucher.edit']);
            Route::post('/accounts/cr-voucher/update/{id}', ['uses' => 'CrVoucherController@update', 'as' => 'cr_voucher.update']);
            Route::get('/accounts/cr-voucher/destroy/{id}', ['uses' => 'CrVoucherController@destroy', 'as' => 'cr_voucher.destroy']);
            Route::get('/accounts/cr-voucher/pdf/{id}', ['uses' => 'CrVoucherController@pdf', 'as' => 'cr_voucher.pdf']);
            Route::get('/accounts/cr-voucher/trashed', ['uses' => 'CrVoucherController@trashed', 'as' => 'cr_voucher.trashed']);
            Route::get('/accounts/cr-voucher/restore/{id}', ['uses' => 'CrVoucherController@restore', 'as' => 'cr_voucher.restore']);
            Route::get('/accounts/cr-voucher/kill/{id}', ['uses' => 'CrVoucherController@kill', 'as' => 'cr_voucher.kill']);
            Route::get('/accounts/cr-voucher/active/search', ['uses' => 'CrVoucherController@activeSearch', 'as' => 'cr_voucher.active.search']);
            Route::get('/accounts/cr-voucher/trashed/search', ['uses' => 'CrVoucherController@trashedSearch', 'as' => 'cr_voucher.trashed.search']);
            Route::get('/accounts/cr-voucher/active/action', ['uses' => 'CrVoucherController@activeAction', 'as' => 'cr_voucher.active.action']);
            Route::get('/accounts/cr-voucher/trashed/action', ['uses' => 'CrVoucherController@trashedAction', 'as' => 'cr_voucher.trashed.action']);
            // cr_voucher End

            //  jnl_voucher Start
            Route::get('/accounts/journal-voucher', ['uses' => 'JournalVoucherController@index', 'as' => 'jnl_voucher']);
            Route::get('/accounts/journal-voucher/show/{id}', ['uses' => 'JournalVoucherController@show', 'as' => 'jnl_voucher.show']);
            Route::get('/accounts/journal-voucher/create', ['uses' => 'JournalVoucherController@create', 'as' => 'jnl_voucher.create']);
            Route::post('/accounts/journal-voucher/store', ['uses' => 'JournalVoucherController@store', 'as' => 'jnl_voucher.store']);
            Route::get('/accounts/journal-voucher/edit/{id}', ['uses' => 'JournalVoucherController@edit', 'as' => 'jnl_voucher.edit']);
            Route::post('/accounts/journal-voucher/update/{id}', ['uses' => 'JournalVoucherController@update', 'as' => 'jnl_voucher.update']);
            Route::get('/accounts/journal-voucher/destroy/{id}', ['uses' => 'JournalVoucherController@destroy', 'as' => 'jnl_voucher.destroy']);
            Route::get('/accounts/journal-voucher/pdf/{id}', ['uses' => 'JournalVoucherController@pdf', 'as' => 'jnl_voucher.pdf']);
            Route::get('/accounts/journal-voucher/trashed', ['uses' => 'JournalVoucherController@trashed', 'as' => 'jnl_voucher.trashed']);
            Route::get('/accounts/journal-voucher/restore/{id}', ['uses' => 'JournalVoucherController@restore', 'as' => 'jnl_voucher.restore']);
            Route::get('/accounts/journal-voucher/kill/{id}', ['uses' => 'JournalVoucherController@kill', 'as' => 'jnl_voucher.kill']);
            Route::get('/accounts/journal-voucher/active/search', ['uses' => 'JournalVoucherController@activeSearch', 'as' => 'jnl_voucher.active.search']);
            Route::get('/accounts/journal-voucher/trashed/search', ['uses' => 'JournalVoucherController@trashedSearch', 'as' => 'jnl_voucher.trashed.search']);
            Route::get('/accounts/journal-voucher/active/action', ['uses' => 'JournalVoucherController@activeAction', 'as' => 'jnl_voucher.active.action']);
            Route::get('/accounts/journal-voucher/trashed/action', ['uses' => 'JournalVoucherController@trashedAction', 'as' => 'jnl_voucher.trashed.action']);
            // jnl_voucher End

            //  contra_voucher Start
            Route::get('/accounts/contra-voucher', ['uses' => 'ContraVoucherController@index', 'as' => 'contra_voucher']);
            Route::get('/accounts/contra-voucher/show/{id}', ['uses' => 'ContraVoucherController@show', 'as' => 'contra_voucher.show']);
            Route::get('/accounts/contra-voucher/create', ['uses' => 'ContraVoucherController@create', 'as' => 'contra_voucher.create']);
            Route::post('/accounts/contra-voucher/store', ['uses' => 'ContraVoucherController@store', 'as' => 'contra_voucher.store']);
            Route::get('/accounts/contra-voucher/edit/{id}', ['uses' => 'ContraVoucherController@edit', 'as' => 'contra_voucher.edit']);
            Route::post('/accounts/contra-voucher/update/{id}', ['uses' => 'ContraVoucherController@update', 'as' => 'contra_voucher.update']);
            Route::get('/accounts/contra-voucher/destroy/{id}', ['uses' => 'ContraVoucherController@destroy', 'as' => 'contra_voucher.destroy']);
            Route::get('/accounts/contra-voucher/pdf/{id}', ['uses' => 'ContraVoucherController@pdf', 'as' => 'contra_voucher.pdf']);
            Route::get('/accounts/contra-voucher/trashed', ['uses' => 'ContraVoucherController@trashed', 'as' => 'contra_voucher.trashed']);
            Route::get('/accounts/contra-voucher/restore/{id}', ['uses' => 'ContraVoucherController@restore', 'as' => 'contra_voucher.restore']);
            Route::get('/accounts/contra-voucher/kill/{id}', ['uses' => 'ContraVoucherController@kill', 'as' => 'contra_voucher.kill']);
            Route::get('/accounts/contra-voucher/active/search', ['uses' => 'ContraVoucherController@activeSearch', 'as' => 'contra_voucher.active.search']);
            Route::get('/accounts/contra-voucher/trashed/search', ['uses' => 'ContraVoucherController@trashedSearch', 'as' => 'contra_voucher.trashed.search']);
            Route::get('/accounts/contra-voucher/active/action', ['uses' => 'ContraVoucherController@activeAction', 'as' => 'contra_voucher.active.action']);
            Route::get('/accounts/contra-voucher/trashed/action', ['uses' => 'ContraVoucherController@trashedAction', 'as' => 'contra_voucher.trashed.action']);
            // contra_voucher End

            //    Accounts Report Start
            //    ledger
            Route::get('/accounts/reports/accounts/ledger', ['uses' => 'AccountsReportController@ledger_index', 'as' => 'reports.accounts.ledger']);
            Route::post('/accounts/reports/accounts/ledger/branch-wise/report', ['uses' => 'AccountsReportController@ledger_branch_wise_report', 'as' => 'reports_accounts_ledger.branch_wise.report']);
            Route::post('/accounts/reports/accounts/ledger/income-expense-head-wise/report', ['uses' => 'AccountsReportController@ledger_income_expense_head_wise_report', 'as' => 'reports_accounts_ledger.income_expense_head_wise.report']);
            Route::post('/accounts/reports/accounts/ledger/bank-cash-wise/report', ['uses' => 'AccountsReportController@ledger_bank_cash_wise_report', 'as' => 'reports_accounts_ledger.bank_cash_wise.report']);

            //    Trial Balance
            Route::get('/accounts/reports/accounts/trial-balance', ['uses' => 'Reports\Accounts\TrialBalanceController@index', 'as' => 'reports.accounts.trial_balance']);
            Route::post('/accounts/reports/accounts/trial-balance/report', ['uses' => 'Reports\Accounts\TrialBalanceController@branch_wise', 'as' => 'reports.accounts.trial_balance.branch_wise.report']);

            //    Cost Of Revenue Manage
            Route::get('/accounts/reports/accounts/cost-of-revenue', ['uses' => 'Reports\Accounts\CostOfRevenueController@index', 'as' => 'reports.accounts.cost_of_revenue']);
            Route::post('/accounts/reports/accounts/cost-of-revenue/report', ['uses' => 'Reports\Accounts\CostOfRevenueController@branch_wise', 'as' => 'reports.accounts.cost_of_revenue.report']);

            //    Profit & Loss Account
            Route::get('/accounts/reports/accounts/profit-or-loss-account', ['uses' => 'Reports\Accounts\ProfitAndLossAccountController@index', 'as' => 'reports.accounts.profit_or_loss_account']);
            Route::post('/accounts/reports/accounts/profit-or-loss-account/report', ['uses' => 'Reports\Accounts\ProfitAndLossAccountController@branch_wise', 'as' => 'reports.accounts.profit_or_loss_account.report']);

            //    Retained Earnings
            Route::get('/accounts/reports/accounts/retained-earnings', ['uses' => 'Reports\Accounts\RetainedEarningsController@index', 'as' => 'reports.accounts.retained_earnings']);
            Route::post('/accounts/reports/accounts/retained-earnings/report', ['uses' => 'Reports\Accounts\RetainedEarningsController@branch_wise', 'as' => 'reports.accounts.retained_earnings.report']);

            //    Fixed Asset Schedule
            Route::get('/accounts/reports/accounts/fixed-asset-schedule', ['uses' => 'Reports\Accounts\FixedAssetScheduleController@index', 'as' => 'reports.accounts.fixed_asset_schedule']);
            Route::post('/accounts/reports/accounts/fixed-asset-schedule/report', ['uses' => 'Reports\Accounts\FixedAssetScheduleController@branch_wise', 'as' => 'reports.accounts.fixed_asset_schedule.report']);

            //  Balance sheet
            Route::get('/accounts/reports/accounts/balance-sheet', ['uses' => 'Reports\Accounts\BalanceSheetController@index', 'as' => 'reports.accounts.balance_sheet']);
            Route::post('/accounts/reports/accounts/balance-sheet/report', ['uses' => 'Reports\Accounts\BalanceSheetController@branch_wise', 'as' => 'reports.accounts.balance_sheet.report']);

            //  Cash Flow
            Route::get('/accounts/reports/accounts/cash-flow', ['uses' => 'Reports\Accounts\CashFlowController@index', 'as' => 'reports.accounts.cash_flow']);
            Route::post('/accounts/reports/accounts/cash-flow/report', ['uses' => 'Reports\Accounts\CashFlowController@branch_wise', 'as' => 'reports.accounts.cash_flow.report']);

            //  Receive Payment
            Route::get('/accounts/reports/accounts/receive-payment', ['uses' => 'Reports\Accounts\ReceivePaymentController@index', 'as' => 'reports.accounts.receive_payment']);
            Route::post('/accounts/reports/accounts/receive-payment/report', ['uses' => 'Reports\Accounts\ReceivePaymentController@branch_wise', 'as' => 'reports.accounts.receive_payment.report']);

            //  Notes start
            Route::get('/accounts/reports/accounts/notes', ['uses' => 'Reports\Accounts\NotesController@index', 'as' => 'reports.accounts.notes']);
            Route::post('/accounts/reports/accounts/notes/type_wise/report', ['uses' => 'Reports\Accounts\NotesController@type_wise', 'as' => 'reports.accounts.notes.type_wise.report']);
            Route::post('/accounts/reports/accounts/notes/group_wise/report', ['uses' => 'Reports\Accounts\NotesController@group_wise', 'as' => 'reports.accounts.notes.group_wise.report']);
            //    Notes End
            //    Accounts Report End

            //    General Report Start
            //    Branch Start
            Route::get('/accounts/reports/general/branch', ['uses' => 'Reports\General\GeneralReportController@branch', 'as' => 'reports.general.branch']);
            Route::post('/accounts/reports/general/branch/report', ['uses' => 'Reports\General\GeneralReportController@branch_report', 'as' => 'reports.general.branch.report']);
            //    Branch End

            //    Ledger Start
            Route::get('/accounts/reports/general/ledger', ['uses' => 'Reports\General\GeneralReportController@ledger_type', 'as' => 'reports.general.ledger.type']);
            Route::post('/accounts/reports/general/ledger/type/report', [ 'uses' => 'Reports\General\GeneralReportController@ledger_type_report', 'as' => 'reports.general.ledger.type.report']);
            Route::post('/accounts/reports/general/ledger/group/report', ['uses' => 'Reports\General\GeneralReportController@ledger_group_report', 'as' => 'reports.general.ledger.group.report']);
            Route::post('/accounts/reports/general/ledger/name/report', ['uses' => 'Reports\General\GeneralReportController@ledger_name_report', 'as' => 'reports.general.ledger.name.report']);
            //    Ledger End

            //    Bank Cash Start
            Route::get('/accounts/reports/general/bank-cash', ['uses' => 'Reports\General\GeneralReportController@bank_cash', 'as' => 'reports.general.bank_cash']);
            Route::post('/accounts/reports/general/ledger/bank-cash/report', ['uses' => 'Reports\General\GeneralReportController@bank_cash_report', 'as' => 'reports.general.bank_cash.report']);
            //    Bank Cash End

            //    Voucher start
            Route::get('/accounts/reports/general/voucher', ['uses' => 'Reports\General\GeneralReportController@voucher', 'as' => 'reports.general.voucher']);
            Route::post('/accounts/reports/general/voucher/report', ['uses' => 'Reports\General\GeneralReportController@voucher_report', 'as' => 'reports.general.voucher.report']);
            //    Voucher start
    }
    );

    // Employee routes
    Route::group(
        ['namespace' => 'Member', 'prefix' => 'member', 'as' => 'member.', 'middleware' => ['role:employee']], function () {

        Route::get('dashboard', ['uses' => 'MemberDashboardController@index'])->name('dashboard');

        Route::get('email-settings/mail-config', ['uses' => 'MemberEmailConfigController@index'])->name('mailconfig.mailConfig');
        Route::post('email-settings/emailupdate', ['uses' => 'MemberEmailConfigController@emailUpdate'])->name('mailconfig.emailupdate');
        Route::post('email-settings/send-test-mail', ['uses' => 'MemberEmailConfigController@sendTestEmail'])->name('mailconfig.sendTestEmail');

        Route::post('profile/updateOneSignalId', ['uses' => 'MemberProfileController@updateOneSignalId'])->name('profile.updateOneSignalId');
        Route::resource('profile', 'MemberProfileController');

        Route::post('projects/gantt-task-update/{id}', ['uses' => 'MemberProjectsController@updateTaskDuration'])->name('projects.gantt-task-update');
        Route::get('projects/ajaxCreate/{columnId}', ['uses' => 'MemberProjectsController@ajaxCreate'])->name('projects.ajaxCreate');
        Route::get('projects/ganttData/{projectId?}', ['uses' => 'MemberProjectsController@ganttData'])->name('projects.ganttData');
        Route::get('projects/gantt/{projectId?}', ['uses' => 'MemberProjectsController@gantt'])->name('projects.gantt');
        Route::get('projects/data', ['uses' => 'MemberProjectsController@data'])->name('projects.data');
        //KD
        Route::get('projects/data-simple', ['uses' => 'MemberProjectsController@dataSimple'])->name('projects.dataSimple');
        Route::post('projects/store-simple', ['uses' => 'MemberProjectsController@storeSimple'])->name('projects.storeSimple');
        Route::put('projects/update-simple/{id}', ['uses' => 'MemberProjectsController@updateSimple'])->name('projects.updateSimple');
        //KD End
        Route::get('projects/edit-file/{projectId}/{fileId}', ['uses' => 'MemberProjectsController@editFile'])->name('projects.editFile');
        Route::match(['get','post'],'projects/sourcingPackages/{id}', ['uses' => 'MemberProjectsController@sourcingPackages'])->name('projects.sourcingPackages');
        Route::match(['get','post'],'projects/addSourcingPackages/{id}', ['uses' => 'MemberProjectsController@addSourcingPackages'])->name('projects.addSourcingPackages');
        Route::match(['get','post'],'projects/storeSourcingPackages/{id}', ['uses' => 'MemberProjectsController@storeSourcingPackages'])->name('projects.storeSourcingPackages');
        Route::match(['get','post'],'projects/editSourcingPackages/{pid}/{id}', ['uses' => 'MemberProjectsController@editSourcingPackages'])->name('projects.editSourcingPackages');
        Route::match(['get','post'],'projects/updateSourcingPackages/{id}', ['uses' => 'MemberProjectsController@updateSourcingPackages'])->name('projects.updateSourcingPackages');
        Route::match(['get','post'],'projects/getProducts/', ['uses' => 'MemberProjectsController@getProducts'])->name('projects.getProducts');
        Route::match(['get','post'],'projects/filterData/', ['uses' => 'MemberProjectsController@filterData'])->name('projects.filterData');
        Route::delete('projects/deleteSourcingPackages/{id}', ['uses' => 'MemberProjectsController@deleteSourcingPackages'])->name('projects.deleteSourcingPackages');
        Route::match(['get','post'],'projects/createTender/{pid}/{id}', ['uses' => 'MemberProjectsController@createTender'])->name('projects.createTender');
        Route::match(['get','post'],'projects/editTender/{pid}/{sid}/{id}', ['uses' => 'MemberProjectsController@editTender'])->name('projects.editTender');
        Route::match(['get','post'],'projects/assignTender/{pid}/{sid}/{id}', ['uses' => 'MemberProjectsController@assignTender'])->name('projects.assignTender');
        Route::match(['get','post'],'projects/storeAssignTender/', ['uses' => 'MemberProjectsController@storeAssignTender'])->name('projects.storeAssignTender');
        Route::match(['get','post'],'projects/tendersList/{pid}/{id}', ['uses' => 'MemberProjectsController@tendersList'])->name('projects.tendersList');
        Route::match(['get','post'],'projects/getBrandsTag', ['uses' => 'MemberProjectsController@getBrandsTag'])->name('projects.getBrandsTag');
        Route::match(['get','post'],'projects/saveTenders', ['uses' => 'MemberProjectsController@saveTenders'])->name('projects.saveTenders');
        Route::match(['get','post'],'projects/storeTenderFiles', ['uses' => 'MemberProjectsController@storeTenderFiles'])->name('projects.storeTenderFiles');
        Route::match(['get','post'],'projects/tenderBiddings', ['uses' => 'MemberProjectsController@tenderBiddings'])->name('projects.tenderBiddings');
        Route::match(['get','post'],'projects/tenderBiddingDetails/{id}', ['uses' => 'MemberProjectsController@tenderBiddingDetails'])->name('projects.tenderBiddingDetails');

        Route::get('projects/boq/{projectId?}', ['uses' => 'MemberProjectsController@Boq'])->name('projects.boq');
        Route::get('projects/project-boq-create/{projectId?}', ['uses' => 'MemberProjectsController@projectBoqCreate'])->name('projects.projectBoqCreate');

        Route::get('projects/edit-file/{projectId}/{fileId}', ['uses' => 'MemberProjectsController@projectEditFile'])->name('projects.projectEditFile');
        Route::match(['get','post'],'projects/titleCreate/{projectId?}', ['uses' => 'MemberProjectsController@titleCreate'])->name('projects.titleCreate');
        Route::match(['get','post'],'projects/schedulingData/{projectId?}/{title?}', ['uses' => 'MemberProjectsController@schedulingData'])->name('projects.schedulingData');
        Route::match(['get','post'],'projects/scheduling/{projectId?}', ['uses' => 'MemberProjectsController@scheduling'])->name('projects.scheduling');

        Route::match(['get','post'],'projects/getItemLavel', ['uses' => 'MemberProjectsController@getItemLavel'])->name('projects.getItemLavel');
        Route::match(['get','post'],'projects/getChild', ['uses' => 'MemberProjectsController@getChild'])->name('projects.getChild');
        Route::match(['get','post'],'projects/getProduct', ['uses' => 'MemberProjectsController@getProduct'])->name('projects.getProduct');
        Route::match(['get','post'],'projects/getCostItem', ['uses' => 'MemberProjectsController@getCostItem'])->name('projects.getCostItem');
        Route::match(['get','post'],'projects/updateStatus/{id}', ['uses' => 'MemberProjectsController@updateStatus'])->name('projects.updateStatus');
        Route::match(['get','post'],'projects/viewBoq/{id}/{title}', ['uses' => 'MemberProjectsController@viewBoq'])->name('projects.viewBoq');
        Route::match(['get','post'],'projects/editBoq/{id}/{title}', ['uses' => 'MemberProjectsController@editBoq'])->name('projects.editBoq');
        Route::match(['get','post'],'projects/updateBoq/{id}/{title}', ['uses' => 'MemberProjectsController@updateBoq'])->name('projects.updateBoq');
        Route::match(['get','post'],'projects/addCostItems', ['uses' => 'MemberProjectsController@addCostItems'])->name('projects.addCostItems');
        Route::match(['get','post'],'projects/ProjectFinalQty/{id}', ['uses' => 'MemberProjectsController@ProjectFinalQty'])->name('projects.ProjectFinalQty');
        Route::match(['get','post'],'projects/schedulingDue/{columnId}', ['uses' => 'MemberProjectsController@schedulingDue'])->name('projects.schedulingDue');
        Route::match(['get','post'],'projects/getFiles', ['uses' => 'MemberProjectsController@getFiles'])->name('projects.getFiles');
        Route::match(['get','post'],'projects/openFolder/{id}', ['uses' => 'MemberProjectsController@openFolder'])->name('projects.openFolder');
        Route::match(['get','post'],'projects/showFiles/{id}', ['uses' => 'MemberProjectsController@showFiles'])->name('projects.showFiles');
        Route::match(['get','post'],'projects/revisionFiles/{prid}/{id}', ['uses' => 'MemberProjectsController@revisionFiles'])->name('projects.revisionFiles');
        Route::match(['get','post'],'projects/selectFile/{id}', ['uses' => 'MemberProjectsController@selectFile'])->name('projects.selectFile');
        Route::delete('projects/deleteFile/{id}', ['uses' => 'MemberProjectsController@deleteFile'])->name('projects.deleteFile');
        Route::match(['get','post'],'projects/getDesignFiles', ['uses' => 'MemberProjectsController@getDesignFiles'])->name('projects.getDesignFiles');
        Route::match(['get','post'],'projects/openDesignFolder/{id}', ['uses' => 'MemberProjectsController@openDesignFolder'])->name('projects.openDesignFolder');
        Route::match(['get','post'],'projects/selectDesignFile/{id}', ['uses' => 'MemberProjectsController@selectDesignFile'])->name('projects.selectDesignFile');
        Route::delete('projects/deleteDesignFile/{id}', ['uses' => 'MemberProjectsController@deleteDesignFile'])->name('projects.deleteDesignFile');
        Route::match(['get','post'],'projects/sourcingPackages/{id}', ['uses' => 'MemberProjectsController@sourcingPackages'])->name('projects.sourcingPackages');

       /* Route::match(['get','post'],'projects/showDesignFiles/{id}', ['uses' => 'ManageDesignController@showDesignFiles'])->name('projects.showDesignFiles');
        Route::post('projects/store-design', ['uses' => 'ManageDesignController@storeDesign'])->name('projects.storeDesign');
        Route::post('projects/store-link', ['uses' => 'ManageDesignController@storeLink'])->name('projects.storeDesignLink');
        Route::post('projects/create-folder', ['uses' => 'ManageDesignController@createFolder'])->name('projects.createDesignFolder');
        Route::post('projects/edit-folder', ['uses' => 'ManageDesignController@editFolder'])->name('projects.editDesignFolder');
        Route::get('projects/download/{id}', ['uses' => 'ManageDesignController@download'])->name('projects.Designdownload');
        Route::get('projects/thumbnail', ['uses' => 'ManageDesignController@thumbnailShow'])->name('projects.Designthumbnail');
      *
        * /
        Route::post('projects/multiple-upload', ['uses' => 'MemberProjectsController@storeMultiple'])->name('projects.Designmultiple-upload');
        Route::get('projects/data-simple', ['uses' => 'MemberProjectsController@dataSimple'])->name('projects.dataSimple');
        Route::post('projects/store-simple', ['uses' => 'MemberProjectsController@storeSimple'])->name('projects.storeSimple');
        Route::put('projects/update-simple/{id}', ['uses' => 'MemberProjectsController@updateSimple'])->name('projects.updateSimple');
        Route::match(['get','post'],'projects/showStoresProducts/{id}/{sid}', ['uses' => 'MemberProjectsController@showStoresProducts'])->name('projects.showStoresProducts');
        Route::match(['get','post'],'projects/showStoresStock/{id}/{sid}', ['uses' => 'MemberProjectsController@showStoresStock'])->name('projects.showStoresStock');
        Route::get('projects/stockProjectData/{id}/{sid}', ['uses' => 'MemberProjectsController@stockProjectData'])->name('projects.stockProjectData');
        Route::match(['get','post'],'projects/bom/{pid}/{sid}', ['uses' => 'MemberProjectsController@bom'])->name('projects.bom');
        Route::match(['get','post'],'projects/storeBom/', ['uses' => 'MemberProjectsController@storeBom'])->name('projects.storeBom');
        Route::match(['get','post'],'projects/updateBom/', ['uses' => 'MemberProjectsController@updateBom'])->name('projects.updateBom');
        Route::resource('projects', 'MemberProjectsController');

        Route::post('projects/indent/approve/{id}/{val}', ['uses' => 'MemberProjectsController@indentApprove'])->name('projects.indent.approve');
        Route::get('projects/indent/create/{pid}/{sid}', ['uses' => 'MemberProjectsController@indentCreate'])->name('projects.indent.create');
        Route::get('projects/indent/create-new/{pid}/{sid}', ['uses' => 'MemberProjectsController@indentCreateNew'])->name('projects.indent.createNew');
        Route::get('projects/indent/export', ['uses' => 'MemberProjectsController@indentExport'])->name('projects.indent.export');
        Route::get('projects/indent/{id}/convert', ['uses' => 'MemberProjectsController@indentConvert'])->name('projects.indent.gconvert');
        Route::patch('projects/indent/{id}/convert', ['uses' => 'MemberProjectsController@indentPostConvert'])->name('projects.indent.pconvert');
        Route::post('projects/indent/get-brands', ['uses' => 'MemberProjectsController@indentGetBrands'])->name('projects.indent.getBrands');
        Route::post('projects/indent/store-tmp', ['uses' => 'MemberProjectsController@indentStoreTmp'])->name('projects.indent.storeTmp');
        Route::post('projects/indent/delete-tmp', ['uses' => 'MemberProjectsController@indentDeleteTmp'])->name('projects.indent.deleteTmp');
        Route::post('projects/indent/store-indent', ['uses' => 'MemberProjectsController@indentStore'])->name('projects.indent.store');
        Route::match(['get','post'],'projects/indent/edit-indent/{pid}/{sid}/{id}', ['uses' => 'MemberProjectsController@indentEdit'])->name('projects.indent.edit');
        Route::post('projects/indent/update-indent/{id}', ['uses' => 'MemberProjectsController@indentUpdate'])->name('projects.indent.update');
        Route::delete('projects/indent/destroy-indent/{id}', ['uses' => 'MemberProjectsController@indentDestroy'])->name('projects.indent.destroy');
        Route::get('indent/data/{pid}/{sid?}', ['uses' => 'MemberProjectsController@indentData'])->name('projects.indent.data');
        Route::post('indent/brand', ['uses' => 'MemberProjectsController@indentBrand'])->name('projects.indent.brand');

        Route::get('projects/{id}/stores', 'MemberProjectsController@stores')->name('projects.stores');
        Route::get('projects/indents/{pid}/{sid}', 'MemberProjectsController@indents')->name('projects.indents');
        Route::get('projects/rfqs/{pid}/{sid}', 'MemberProjectsController@rfqs')->name('projects.rfqs');
        Route::get('projects/quotations/{pid}/{sid}', 'MemberProjectsController@quotations')->name('projects.quotations');
        Route::get('projects/{id}/purchase-orders', 'MemberProjectsController@purchaseOrders')->name('projects.purchaseOrders');

        Route::get('project-template/data', ['uses' => 'ProjectTemplateController@data'])->name('project-template.data');
        Route::resource('project-template', 'ProjectTemplateController');

        Route::post('project-template-members/save-group', ['uses' => 'ProjectMemberTemplateController@storeGroup'])->name('project-template-members.storeGroup');
        Route::resource('project-template-member', 'ProjectMemberTemplateController');

        Route::resource('project-template-task', 'ProjectTemplateTaskController');

        Route::get('leads/data', ['uses' => 'MemberLeadController@data'])->name('leads.data');
        Route::post('leads/change-status', ['uses' => 'MemberLeadController@changeStatus'])->name('leads.change-status');
        Route::get('leads/follow-up/{leadID}', ['uses' => 'MemberLeadController@followUpCreate'])->name('leads.follow-up');
        Route::get('leads/followup/{leadID}', ['uses' => 'MemberLeadController@followUpShow'])->name('leads.followup');
        Route::post('leads/follow-up-store', ['uses' => 'MemberLeadController@followUpStore'])->name('leads.follow-up-store');
        Route::get('leads/follow-up-edit/{id?}', ['uses' => 'MemberLeadController@editFollow'])->name('leads.follow-up-edit');
        Route::post('leads/follow-up-update', ['uses' => 'MemberLeadController@UpdateFollow'])->name('leads.follow-up-update');
        Route::get('leads/follow-up-sort', ['uses' => 'MemberLeadController@followUpSort'])->name('leads.follow-up-sort');
        Route::resource('leads', 'MemberLeadController');

        // Lead Files
        Route::get('lead-files/download/{id}', ['uses' => 'LeadFilesController@download'])->name('lead-files.download');
        Route::get('lead-files/thumbnail', ['uses' => 'LeadFilesController@thumbnailShow'])->name('lead-files.thumbnail');
        Route::resource('lead-files', 'LeadFilesController');

        // Proposal routes
        Route::get('proposals/data/{id?}', ['uses' => 'MemberProposalController@data'])->name('proposals.data');
        Route::get('proposals/download/{id}', ['uses' => 'MemberProposalController@download'])->name('proposals.download');
//        Route::get('proposals/create/{leadID?}', ['uses' => 'MemberProposalController@create'])->name('proposals.create');
        Route::get('proposals/convert-proposal/{id?}', ['uses' => 'MemberProposalController@convertProposal'])->name('proposals.convert-proposal');
        Route::resource('proposals', 'MemberProposalController' , ['expect' => ['create']]);

        // Location
        Route::post('location/store-cat', ['uses' => 'ManageLocationController@storeCat'])->name('location.store-cat');
        Route::get('location/create-cat', ['uses' => 'ManageLocationController@createCat'])->name('location.create-cat');
        Route::get('location/edit-cat/{id}', ['uses' => 'ManageLocationController@editCat'])->name('location.edit-cat');
        Route::post('location/update-cat/{id}', ['uses' => 'ManageLocationController@updateCat'])->name('location.update-cat');
        Route::post('location/get-city/{id}', ['uses' => 'ManageLocationController@getCity'])->name('location.get-city');
        Route::resource('location', 'ManageLocationController');
        Route::post('location/store-sub-cat', ['uses' => 'ManageLocationController@storesubCat'])->name('location.store-sub-cat');
        Route::get('location/sub-edit/{id}', ['uses' => 'ManageLocationController@editsubCat'])->name('location.sub-edit');
        Route::post('location/sub-update/{id}', ['uses' => 'ManageLocationController@updatesubCat'])->name('location.sub-update');
        Route::post('location/delete-sub-cat/{id}', ['uses' => 'ManageLocationController@deletesubCat'])->name('location.delete-sub-cat');
        Route::get('location/location-edit/{id}', ['uses' => 'ManageLocationController@editLocation'])->name('location.location-edit');
        Route::post('location/location-update/{id}', ['uses' => 'ManageLocationController@updateLocation'])->name('location.location-update');
        Route::post('location/delete-location/{id}', ['uses' => 'ManageLocationController@deleteLocation'])->name('location.delete-location');

        //Asset
        Route::post('asset-category/create-asset-cat', ['uses' => 'AssetCategoryController@createassetCat'])->name('asset-category.create-asset-cat');
        Route::get('asset-category/create-asset-edit/{id}', ['uses' => 'AssetCategoryController@editassetCat'])->name('asset-category.edit-asset-cat');
        Route::post('asset-category/update-asset-edit/{id}', ['uses' => 'AssetCategoryController@updateassetCat'])->name('asset-category.update-asset-cat');
        Route::get('asset-category/assets', ['uses' => 'AssetCategoryController@assets'])->name('asset-category.assets');
        Route::post('asset-category/create-asset', ['uses' => 'AssetCategoryController@createAsset'])->name('asset-category.create-asset');
        Route::get('asset-category/edit-asset/{id}', ['uses' => 'AssetCategoryController@editAsset'])->name('asset-category.edit-asset');
        Route::post('asset-category/update-asset/{id}', ['uses' => 'AssetCategoryController@updateAsset'])->name('asset-category.update-asset');
        Route::post('location/delete-asset/{id}', ['uses' => 'AssetCategoryController@deleteAsset'])->name('asset-category.delete-asset');
        Route::get('asset-category/sub-asset', ['uses' => 'AssetCategoryController@subAsset'])->name('asset-category.sub-asset');
        Route::post('asset-category/create-sub-asset', ['uses' => 'AssetCategoryController@createSubAsset'])->name('asset-category.create-sub-asset');
        Route::get('asset-category/edit-sub-asset/{id}', ['uses' => 'AssetCategoryController@editSubAsset'])->name('asset-category.edit-sub-asset');
        Route::post('asset-category/update-sub-asset/{id}', ['uses' => 'AssetCategoryController@updateSubAsset'])->name('asset-category.update-sub-asset');
        Route::post('location/delete-sub-asset/{id}', ['uses' => 'AssetCategoryController@deleteSubAsset'])->name('asset-category.delete-sub-asset');
        Route::resource('asset-category', 'AssetCategoryController');

        //payslips
            Route::resource('project-members', 'MemberProjectsMemberController');
            Route::post('tasks/sort', ['uses' => 'MemberTasksController@sort'])->name('tasks.sort');
            Route::post('tasks/change-status', ['uses' => 'MemberTasksController@changeStatus'])->name('tasks.changeStatus');
            Route::get('tasks/check-task/{taskID}', ['uses' => 'MemberTasksController@checkTask'])->name('tasks.checkTask');
//            Route::resource('tasks', 'MemberTasksController');

            Route::get('files/download/{id}', ['uses' => 'MemberProjectFilesController@download'])->name('files.download');
            Route::get('files/thumbnail', ['uses' => 'MemberProjectFilesController@thumbnailShow'])->name('files.thumbnail');
            Route::post('files/multiple-upload', ['uses' => 'MemberProjectFilesController@storeMultiple'])->name('files.multiple-upload');
            Route::resource('files', 'MemberProjectFilesController');

            Route::get('time-log/show-log/{id}', ['uses' => 'MemberTimeLogController@showTomeLog'])->name('time-log.show-log');
            Route::get('time-log/data/{id}', ['uses' => 'MemberTimeLogController@data'])->name('time-log.data');
            Route::post('time-log/store-time-log', ['uses' => 'MemberTimeLogController@storeTimeLog'])->name('time-log.store-time-log');
            Route::put('time-log/update-time-log/{id}', ['uses' => 'MemberTimeLogController@updateTimeLog'])->name('time-log.update-time-log');
            Route::resource('time-log', 'MemberTimeLogController');

        //sticky note
        Route::resource('sticky-note', 'MemberStickyNoteController');

        // User message
        Route::post('message-submit', ['as' => 'user-chat.message-submit', 'uses' => 'MemberChatController@postChatMessage']);
        Route::get('user-search', ['as' => 'user-chat.user-search', 'uses' => 'MemberChatController@getUserSearch']);
        Route::resource('user-chat', 'MemberChatController');

        //Notice
        Route::get('notices/data', ['uses' => 'MemberNoticesController@data'])->name('notices.data');
        Route::resource('notices', 'MemberNoticesController');

        // task routes
        Route::resource('tasks', 'MemberAllTasksController', ['only' => ['edit', 'update', 'index']]); // hack to make left admin menu item active
        Route::group(
            ['prefix' => 'tasks'],
            function () {

            Route::get('all-tasks/dependent-tasks/{projectId}/{taskId?}', ['uses' => 'MemberAllTasksController@dependentTaskLists'])->name('all-tasks.dependent-tasks');
            Route::post('all-tasks/data/{hideCompleted?}/{projectId?}', ['uses' => 'MemberAllTasksController@data'])->name('all-tasks.data');
            Route::get('all-tasks/members/{projectId}', ['uses' => 'MemberAllTasksController@membersList'])->name('all-tasks.members');
            Route::get('all-tasks/ajaxCreate/{columnId}', ['uses' => 'MemberAllTasksController@ajaxCreate'])->name('all-tasks.ajaxCreate');
            Route::get('all-tasks/reminder/{taskid}', ['uses' => 'MemberAllTasksController@remindForTask'])->name('all-tasks.reminder');
            Route::get('all-tasks/files/{taskid}', ['uses' => 'MemberAllTasksController@showFiles'])->name('all-tasks.show-files');
            Route::resource('all-tasks', 'MemberAllTasksController');

            // taskboard resource
            Route::post('taskboard/updateIndex', ['as' => 'taskboard.updateIndex', 'uses' => 'MemberTaskboardController@updateIndex']);
            Route::resource('taskboard', 'MemberTaskboardController');

            // task calendar routes
            Route::resource('task-calendar', 'MemberCalendarController');
            Route::get('task-files/download/{id}', ['uses' => 'TaskFilesController@download'])->name('task-files.download');
            Route::resource('task-files', 'TaskFilesController');

        });

        Route::resource('finance', 'MemberEstimatesController', ['only' => ['edit', 'update', 'index']]); // hack to make left admin menu item active
        Route::group(
            ['prefix' => 'finance'],
            function () {

            // Estimate routes
            Route::get('estimates/data', ['uses' => 'MemberEstimatesController@data'])->name('estimates.data');
            Route::get('estimates/download/{id}', ['uses' => 'MemberEstimatesController@download'])->name('estimates.download');
            Route::resource('estimates', 'MemberEstimatesController');

            //Expenses routes
            Route::get('expenses/data', ['uses' => 'MemberExpensesController@data'])->name('expenses.data');
            Route::resource('expenses', 'MemberExpensesController');

            // All invoices list routes
            Route::post('file/store', ['uses' => 'MemberAllInvoicesController@storeFile'])->name('invoiceFile.store');
            Route::delete('file/destroy', ['uses' => 'MemberAllInvoicesController@destroyFile'])->name('invoiceFile.destroy');
            Route::get('all-invoices/data', ['uses' => 'MemberAllInvoicesController@data'])->name('all-invoices.data');
            Route::get('all-invoices/download/{id}', ['uses' => 'MemberAllInvoicesController@download'])->name('all-invoices.download');
            Route::get('all-invoices/convert-estimate/{id}', ['uses' => 'MemberAllInvoicesController@convertEstimate'])->name('all-invoices.convert-estimate');
            Route::get('all-invoices/update-item', ['uses' => 'MemberAllInvoicesController@addItems'])->name('all-invoices.update-item');
            Route::get('all-invoices/payment-detail/{invoiceID}', ['uses' => 'MemberAllInvoicesController@paymentDetail'])->name('all-invoices.payment-detail');
            Route::get('all-invoices/get-client-company/{projectID?}', ['uses' => 'MemberAllInvoicesController@getClientOrCompanyName'])->name('all-invoices.get-client-company');
            Route::get('all-invoices/update-status/{invoiceID}', ['uses' => 'MemberAllInvoicesController@cancelStatus'])->name('all-invoices.update-status');

                Route::resource('all-invoices', 'MemberAllInvoicesController');

            // All Credit Note routes
            Route::post('credit-file/store', ['uses' => 'MemberAllCreditNotesController@storeFile'])->name('creditNoteFile.store');
            Route::delete('credit-file/destroy', ['uses' => 'MemberAllCreditNotesController@destroyFile'])->name('creditNoteFile.destroy');
            Route::get('all-credit-notes/data', ['uses' => 'MemberAllCreditNotesController@data'])->name('all-credit-notes.data');
            Route::get('all-credit-notes/download/{id}', ['uses' => 'MemberAllCreditNotesController@download'])->name('all-credit-notes.download');
            Route::get('all-credit-notes/convert-invoice/{id}', ['uses' => 'MemberAllCreditNotesController@convertInvoice'])->name('all-credit-notes.convert-invoice');
            Route::get('all-credit-notes/update-item', ['uses' => 'MemberAllCreditNotesController@addItems'])->name('all-credit-notes.update-item');
            Route::get('all-credit-notes/payment-detail/{creditNoteID}', ['uses' => 'MemberAllCreditNotesController@paymentDetail'])->name('all-credit-notes.payment-detail');
            Route::resource('all-credit-notes', 'MemberAllCreditNotesController');

            //Payments routes
            Route::get('payments/data', ['uses' => 'MemberPaymentsController@data'])->name('payments.data');
            Route::get('payments/pay-invoice/{invoiceId}', ['uses' => 'MemberPaymentsController@payInvoice'])->name('payments.payInvoice');
            Route::resource('payments', 'MemberPaymentsController');
        });

        // Ticket reply template routes
        Route::post('replyTemplates/fetch-template', ['uses' => 'MemberTicketReplyTemplatesController@fetchTemplate'])->name('replyTemplates.fetchTemplate');

        //Tickets routes
        Route::get('tickets/data', ['uses' => 'MemberTicketsController@data'])->name('tickets.data');
        Route::post('tickets/storeAdmin', ['uses' => 'MemberTicketsController@storeAdmin'])->name('tickets.storeAdmin');
        Route::post('tickets/updateAdmin/{id}', ['uses' => 'MemberTicketsController@updateAdmin'])->name('tickets.updateAdmin');
        Route::post('tickets/close-ticket/{id}', ['uses' => 'MemberTicketsController@closeTicket'])->name('tickets.closeTicket');
        Route::post('tickets/open-ticket/{id}', ['uses' => 'MemberTicketsController@reopenTicket'])->name('tickets.reopenTicket');
        Route::get('tickets/admin-data/{startDate?}/{endDate?}/{agentId?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'MemberTicketsController@adminData'])->name('tickets.adminData');
        Route::get('tickets/refresh-count/{startDate?}/{endDate?}/{agentId?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'MemberTicketsController@refreshCount'])->name('tickets.refreshCount');
        Route::get('tickets/reply-delete/{id?}', ['uses' => 'MemberTicketsController@destroyReply'])->name('tickets.reply-delete');
        Route::resource('tickets', 'MemberTicketsController');

        //Ticket agent routes
        Route::get('ticket-agent/data/{startDate?}/{endDate?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'MemberTicketsAgentController@data'])->name('ticket-agent.data');
        Route::get('ticket-agent/refresh-count/{startDate?}/{endDate?}/{status?}/{priority?}/{channelId?}/{typeId?}', ['uses' => 'MemberTicketsAgentController@refreshCount'])->name('ticket-agent.refreshCount');
        Route::post('ticket-agent/fetch-template', ['uses' => 'MemberTicketsAgentController@fetchTemplate'])->name('ticket-agent.fetchTemplate');
        Route::resource('ticket-agent', 'MemberTicketsAgentController');
        Route::get('ticket-files/download/{id}', ['uses' => 'TicketFilesController@download'])->name('ticket-files.download');
        Route::resource('ticket-files', 'TicketFilesController');

        // attendance
        Route::get('attendances/detail', ['uses' => 'MemberAttendanceController@attendanceDetail'])->name('attendances.detail');
        Route::get('attendances/data', ['uses' => 'MemberAttendanceController@data'])->name('attendances.data');
        Route::get('attendances/check-holiday', ['uses' => 'MemberAttendanceController@checkHoliday'])->name('attendances.check-holiday');
        Route::post('attendances/storeAttendance', ['uses' => 'MemberAttendanceController@storeAttendance'])->name('attendances.storeAttendance');
        Route::get('attendances/employeeData/{startDate?}/{endDate?}/{userId?}', ['uses' => 'MemberAttendanceController@employeeData'])->name('attendances.employeeData');
        Route::get('attendances/refresh-count/{startDate?}/{endDate?}/{userId?}', ['uses' => 'MemberAttendanceController@refreshCount'])->name('attendances.refreshCount');
        Route::post('attendances/storeMark', ['uses' => 'MemberAttendanceController@storeMark'])->name('attendances.storeMark');
        Route::get('attendances/mark/{id}/{day}/{month}/{year}', ['uses' => 'MemberAttendanceController@mark'])->name('attendances.mark');
        Route::get('attendances/summary', ['uses' => 'MemberAttendanceController@summary'])->name('attendances.summary');
        Route::post('attendances/summaryData', ['uses' => 'MemberAttendanceController@summaryData'])->name('attendances.summaryData');
        Route::get('attendances/info/{id}', ['uses' => 'MemberAttendanceController@detail'])->name('attendances.info');
        Route::post('attendances/updateDetails/{id}', ['uses' => 'MemberAttendanceController@updateDetails'])->name('attendances.updateDetails');

        Route::resource('attendances', 'MemberAttendanceController');

        // Holidays
        Route::get('holidays/view-holiday/{year?}', 'MemberHolidaysController@viewHoliday')->name('holidays.view-holiday');
        Route::get('holidays/calendar-month', 'MemberHolidaysController@getCalendarMonth')->name('holidays.calendar-month');
        Route::get('holidays/mark_sunday', 'MemberHolidaysController@Sunday')->name('holidays.mark-sunday');
        Route::get('holidays/calendar/{year?}', 'MemberHolidaysController@holidayCalendar')->name('holidays.calendar');
        Route::get('holidays/mark-holiday', 'MemberHolidaysController@markHoliday')->name('holidays.mark-holiday');
        Route::post('holidays/mark-holiday-store', 'MemberHolidaysController@markDayHoliday')->name('holidays.mark-holiday-store');
        Route::resource('holidays', 'MemberHolidaysController');

        // events
        Route::post('events/removeAttendee', ['as' => 'events.removeAttendee', 'uses' => 'MemberEventController@removeAttendee']);
        Route::resource('events', 'MemberEventController');

        // clients
        Route::group(
            ['prefix' => 'clients'],
            function() {
            Route::get('projects/{id}', ['uses' => 'MemberClientsController@showProjects'])->name('clients.projects');
            Route::get('invoices/{id}', ['uses' => 'MemberClientsController@showInvoices'])->name('clients.invoices');

            Route::get('contacts/data/{id}', ['uses' => 'MemberClientContactController@data'])->name('contacts.data');
            Route::resource('contacts', 'MemberClientContactController');
        });

        Route::get('clients/data', ['uses' => 'MemberClientsController@data'])->name('clients.data');
        /*Route::get('clients/create/{clientID?}', ['uses' => 'MemberClientsController@create'])->name('clients.create');*/
        Route::resource('clients', 'MemberClientsController');

        Route::get('suppliers/data', ['uses' => 'MemberSuppliersController@data'])->name('suppliers.data');
        /*Route::get('suppliers/create/{clientID?}', ['uses' => 'MemberSuppliersController@create'])->name('suppliers.create');*/
        Route::resource('suppliers', 'MemberSuppliersController');
        Route::get('suppliers/export', ['uses' => 'MemberSuppliersController@export'])->name('suppliers.export');


        Route::get('stores/data/{id?}', ['uses' => 'MemberStoresController@data'])->name('stores.data');
       /* Route::get('stores/create/{clientID?}', ['uses' => 'MemberStoresController@create'])->name('stores.create');*/
        Route::get('stores/create-new/{id?}', ['uses' => 'MemberStoresController@createNew'])->name('stores.createNew');
        Route::resource('stores', 'MemberStoresController');
        Route::get('stores/export', ['uses' => 'MemberStoresController@export'])->name('memberstores.export');

        Route::get('indent/data/{id?}', ['uses' => 'MemberIndentController@data'])->name('indent.data');
        Route::post('indent/approve/{id}/{val}', ['uses' => 'MemberIndentController@approve'])->name('indent.approve');
//        Route::get('indent/create/{clientID?}', ['uses' => 'MemberIndentController@create'])->name('indent.create');
        Route::get('indent/create-new/{id?}', ['uses' => 'MemberIndentController@createNew'])->name('indent.createNew');
        Route::resource('indent', 'MemberIndentController');
        Route::get('indent/export', ['uses' => 'MemberIndentController@export'])->name('indent.export');
        Route::get('indent/{id}/convert', ['uses' => 'MemberIndentController@convert'])->name('indent.gconvert');
        Route::patch('indent/{id}/convert', ['uses' => 'MemberIndentController@postConvert'])->name('indent.pconvert');
        Route::get('indent/convert-grn/{id}', ['uses' => 'MemberIndentController@quotations/show-details'])->name('indent.convertGrn');
        Route::patch('indent/post-convert-grn/{id}', ['uses' => 'MemberIndentController@postConvertGrn'])->name('indent.postConvertGrn');
        Route::post('indent/get-brands', ['uses' => 'MemberIndentController@getBrands'])->name('indent.getBrands');
        Route::post('indent/store-tmp', ['uses' => 'MemberIndentController@storeTmp'])->name('indent.storeTmp');
        Route::post('indent/delete-tmp', ['uses' => 'MemberIndentController@deleteTmp'])->name('indent.deleteTmp');

        Route::get('rfq/data/{id?}', ['uses' => 'MemberRfqController@data'])->name('rfq.data');
        Route::get('rfq/link-data/{id}', ['uses' => 'MemberRfqController@linkData'])->name('rfq.linkData');
        /*Route::get('rfq/create/{clientID?}', ['uses' => 'MemberRfqController@create'])->name('rfq.create');*/
        Route::get('rfq/create-new/{id?}', ['uses' => 'MemberRfqController@createNew'])->name('rfq.createNew');
        Route::resource('rfq', 'MemberRfqController');
        Route::get('rfq/export', ['uses' => 'MemberRfqController@export'])->name('rfq.export');
        Route::post('rfq/get-brands', ['uses' => 'MemberRfqController@getBrands'])->name('rfq.getBrands');
        Route::post('rfq/store-tmp', ['uses' => 'MemberRfqController@storeTmp'])->name('rfq.storeTmp');
        Route::post('rfq/delete-tmp', ['uses' => 'MemberRfqController@deleteTmp'])->name('rfq.deleteTmp');
        Route::get('rfq/rfq-link/{id}', ['uses' => 'MemberRfqController@rfqLink'])->name('rfq.rfqLink');

        Route::get('quotations/data/{id?}', ['uses' => 'MemberQuotationController@data'])->name('quotations.data');
        Route::get('quotations/show-details/{id}', ['uses' => 'MemberQuotationController@showAll'])->name('quotations.showAll');
        Route::get('quotations/link-data/{id}', ['uses' => 'MemberQuotationController@linkData'])->name('quotations.linkData');
        /*Route::get('quotations/create/{clientID?}', ['uses' => 'MemberQuotationController@create'])->name('quotations.create');*/
        Route::resource('quotations', 'MemberQuotationController');
        Route::get('quotations/export', ['uses' => 'MemberQuotationController@export'])->name('quotations.export');

        //PO
        Route::get('purchase-order/data', ['uses' => 'MemberPOController@data'])->name('purchase-order.data');
        Route::post('purchase-order/approve/{id}/{val}', ['uses' => 'MemberPOController@approve'])->name('purchase-order.approve');
        Route::get('purchase-order/links/{poId}', ['uses' => 'MemberPOController@poLinks'])->name('purchase-order.links');
        Route::get('purchase-order/link-data/{id}', ['uses' => 'MemberPOController@linkData'])->name('purchase-order.linkData');
        Route::get('purchase-order/detail/{poId}', ['uses' => 'MemberPOController@viewPO'])->name('purchase-order.viewPO');
        Route::post('quotation/generate-po', ['uses' => 'MemberQuotationController@generatePO'])->name('quotations.generatePO');
        Route::post('quotation/submit-po', ['uses' => 'MemberQuotationController@postPO'])->name('quotations.postPO');
        Route::resource('purchase-order', 'MemberPOController');

        //Resources
        Route::resource('resources', 'MemberResourcesController');


        //Inventory
        Route::get('inventory/data', ['uses' => 'MemberInventoryController@stockData'])->name('inventory.data');
        Route::get('inventory/purchase-data', ['uses' => 'MemberInventoryController@purchaseData'])->name('inventory.purchaseData');
        Route::get('inventory/invoice-data', ['uses' => 'MemberInventoryController@invoiceData'])->name('inventory.invoiceData');
        Route::get('inventory/return-data', ['uses' => 'MemberInventoryController@returnData'])->name('inventory.returnData');
        Route::get('inventory/add/{poId}', ['uses' => 'MemberInventoryController@add'])->name('inventory.add');
        Route::post('inventory/add/{poId}', ['uses' => 'MemberInventoryController@postAdd'])->name('inventory.postAdd');
        Route::get('inventory/add-new', ['uses' => 'MemberInventoryController@addNew'])->name('inventory.addNew');
        Route::post('inventory/add-new', ['uses' => 'MemberInventoryController@postAddNew'])->name('inventory.postAddNew');
        Route::get('inventory/stock', ['uses' => 'MemberInventoryController@stock'])->name('inventory.stock');
        Route::get('inventory/purchase-history/{bid}/{store?}', ['uses' => 'MemberInventoryController@purchaseHistory'])->name('inventory.purchase-history');
        Route::get('inventory/product-inward', ['uses' => 'MemberInventoryController@inward'])->name('inventory.inward');
        Route::get('inventory/purchase-invoices', ['uses' => 'MemberInventoryController@purchaseInvoices'])->name('inventory.invoices');
        Route::get('purchase-invoices/make-payment/{id}', ['uses' => 'MemberInventoryController@makePayment'])->name('purchase.payment');
        Route::post('purchase-invoices/make-payment/{id}', ['uses' => 'MemberInventoryController@submitPayment'])->name('purchase.submitPayment');
        Route::delete('purchase-invoices/delete-payment/{id}', ['uses' => 'MemberInventoryController@deletePayment'])->name('payment.destroy');
        Route::get('inventory/product-return/{invId}', ['uses' => 'MemberInventoryController@pReturn'])->name('inventory.return');
        Route::get('inventory/product-returns', ['uses' => 'MemberInventoryController@pReturns'])->name('inventory.returns');
        Route::post('inventory/product-return/{invId}', ['uses' => 'MemberInventoryController@postReturn'])->name('inventory.postReturn');
        Route::resource('inventory', 'MemberInventoryController');
        Route::post('inventory/store-tmp', ['uses' => 'MemberInventoryController@storeTmp'])->name('inventory.storeTmp');
        Route::post('inventory/delete-tmp', ['uses' => 'MemberInventoryController@deleteTmp'])->name('inventory.deleteTmp');

        Route::get('employees/docs-create/{id}', ['uses' => 'MemberEmployeesController@docsCreate'])->name('employees.docs-create');
        Route::get('employees/tasks/{userId}/{hideCompleted}', ['uses' => 'MemberEmployeesController@tasks'])->name('employees.tasks');
        Route::get('employees/time-logs/{userId}', ['uses' => 'MemberEmployeesController@timeLogs'])->name('employees.time-logs');
        Route::get('employees/data', ['uses' => 'MemberEmployeesController@data'])->name('employees.data');
        Route::get('employees/export', ['uses' => 'MemberEmployeesController@export'])->name('employees.export');
        Route::post('employees/assignRole', ['uses' => 'MemberEmployeesController@assignRole'])->name('employees.assignRole');
        Route::post('employees/assignProjectAdmin', ['uses' => 'MemberEmployeesController@assignProjectAdmin'])->name('employees.assignProjectAdmin');
        Route::resource('employees', 'MemberEmployeesController');

        Route::get('employee-docs/download/{id}', ['uses' => 'MemberEmployeeDocsController@download'])->name('employee-docs.download');
        Route::resource('employee-docs', 'MemberEmployeeDocsController');

        Route::get('all-time-logs/show-active-timer', ['uses' => 'MemberAllTimeLogController@showActiveTimer'])->name('all-time-logs.show-active-timer');
        Route::post('all-time-logs/stop-timer/{id}', ['uses' => 'MemberAllTimeLogController@stopTimer'])->name('all-time-logs.stopTimer');
        Route::post('all-time-logs/data/{projectId?}/{employee?}', ['uses' => 'MemberAllTimeLogController@data'])->name('all-time-logs.data');
        Route::get('all-time-logs/members/{projectId}', ['uses' => 'MemberAllTimeLogController@membersList'])->name('all-time-logs.members');
        Route::post('all-time-logs/storeImage', ['uses' => 'MemberAllTimeLogController@storeImage'])->name('all-time-logs.storeImage');
        Route::resource('all-time-logs', 'MemberAllTimeLogController');

        Route::post('leaves/leaveAction', ['as' => 'leaves.leaveAction', 'uses' => 'MemberLeavesController@leaveAction']);
        Route::get('leaves/data', ['as' => 'leaves.data', 'uses' => 'MemberLeavesController@data']);
        Route::resource('leaves', 'MemberLeavesController');
        Route::post('leaves-dashboard/leaveAction', ['as' => 'leaves-dashboard.leaveAction', 'uses' => 'MemberLeaveDashboardController@leaveAction']);
        Route::resource('leaves-dashboard', 'MemberLeaveDashboardController');

        //sub task routes
        Route::post('sub-task/changeStatus', ['as' => 'sub-task.changeStatus', 'uses' => 'MemberSubTaskController@changeStatus']);
        Route::resource('sub-task', 'MemberSubTaskController');

        //task comments
        Route::resource('task-comment', 'MemberTaskCommentController');

        //region Products Routes
        Route::get('products/data', ['uses' => 'MemberProductController@data'])->name('products.data');
        Route::resource('products', 'MemberProductController');
        //endregion

        //boq-category renamed as activity
        Route::post('activity/store-cat', ['uses' => 'ManageBoqCategoryController@storeCat'])->name('activity.store-cat');
        Route::get('activity/create-cat', ['uses' => 'ManageBoqCategoryController@createCat'])->name('activity.create-cat');
        Route::get('activity/edit-cat/{id}', ['uses' => 'ManageBoqCategoryController@editCat'])->name('activity.edit-cat');
        Route::post('activity/update-cat/{id}', ['uses' => 'ManageBoqCategoryController@updateCat'])->name('activity.update-cat');
        Route::resource('activity', 'ManageBoqCategoryController');

        //product-trade
        Route::post('product-trade/store-trade', ['uses' => 'ManageProductTradeController@storeTrade'])->name('productTrade.store-trade');
        Route::get('product-trade/create-trade', ['uses' => 'ManageProductTradeController@createTrade'])->name('productTrade.create-trade');
        Route::get('product-trade/edit-trade/{id}', ['uses' => 'ManageProductTradeController@editTrade'])->name('productTrade.edit-trade');
        Route::post('product-trade/update-trade/{id}', ['uses' => 'ManageProductTradeController@updateTrade'])->name('productTrade.update-trade');
        Route::resource('product-trade', 'ManageProductTradeController');

        //cost-items renamed as task
        Route::post('task/store-cat', ['uses' => 'CostItemsController@storeCat'])->name('task.store-cat');
        Route::get('task/create-cat', ['uses' => 'CostItemsController@createCat'])->name('task.create-cat');
        Route::get('task/edit-cat/{id}', ['uses' => 'CostItemsController@editCat'])->name('task.edit-cat');
        Route::post('task/update-cat/{id}', ['uses' => 'CostItemsController@updateCat'])->name('task.update-cat');
        Route::post('task/get-cost', ['uses' => 'CostItemsController@getCost'])->name('task.getCost');
        Route::match(['get','post'],'task/product-add/{id}', ['uses' => 'CostItemsController@productAdd'])->name('task.product-add');
        Route::match(['get','post'],'task/product-edit/{id}', ['uses' => 'CostItemsController@productEdit'])->name('task.product-edit');
        Route::post('task/update-product/{id}', ['uses' => 'CostItemsController@productUpdate'])->name('task.update-product');
        Route::delete('task/destroy-product/{id}', ['uses' => 'CostItemsController@destroyProduct'])->name('task.destroy-product');
        Route::post('task/store-product/{id}', ['uses' => 'CostItemsController@storeProduct'])->name('task.store-product');
        Route::post('task/name-store', ['uses' => 'CostItemsController@nameStore'])->name('task.namestore');
        Route::resource('task', 'CostItemsController');

        //product-category
        Route::get('product-category/index', ['uses' => 'ProductCategoryController@index'])->name('productCategory.index');
        Route::resource('product-category', 'ProductCategoryController');

        //product-brand
        Route::get('product-brand/index', ['uses' => 'ProductBrandController@index'])->name('productBrand.index');
        Route::resource('product-brand', 'ProductBrandController');

        //issue
        Route::get('issue/export', ['uses' => 'ManagePunchItemController@export'])->name('issue.export');
        Route::post('issue/data', ['uses' => 'ManagePunchItemController@data'])->name('issue.data');
        Route::post('issue/storeImage', ['uses' => 'ManagePunchItemController@storeImage'])->name('issue.storeImage');
        Route::get('issue/dependent-tasks/{projectId}/{taskId?}', ['uses' => 'ManagePunchItemController@dependentTaskLists'])->name('issue.dependent-tasks');
        Route::get('issue/members/{projectId}', ['uses' => 'ManagePunchItemController@membersList'])->name('issue.members');
        Route::get('issue/ajaxCreate/{columnId}', ['uses' => 'ManagePunchItemController@ajaxCreate'])->name('issue.ajaxCreate');
        Route::get('issue/reminder/{taskid}', ['uses' => 'ManagePunchItemController@remindForTask'])->name('issue.reminder');
        Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
        Route::delete('issue/removeFile/{id}', ['uses' => 'ManagePunchItemController@removeFile'])->name('issue.removeFile');
        Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
        Route::get('issue/reply/{taskid}', ['uses' => 'ManagePunchItemController@reply'])->name('issue.reply');
        Route::get('issue/comment/{taskid}', ['uses' => 'ManagePunchItemController@comment'])->name('issue.comment');
        Route::post('issue/replyPost/{taskid}', ['uses' => 'ManagePunchItemController@replyPost'])->name('issue.replyPost');
        Route::post('issue/commentPost/{taskid}', ['uses' => 'ManagePunchItemController@commentPost'])->name('issue.commentPost');

        Route::match(['get','post'],'issue/issuedata', ['uses' => 'ManagePunchItemController@issueData'])->name('issue.issueData');
        Route::resource('issue', 'ManagePunchItemController');

        //inspection-type
        Route::post('inspection-type/store-type', ['uses' => 'ManageInspectionTypeController@storeType'])->name('inspectionType.storeType');
        Route::get('inspection-type/create-type', ['uses' => 'ManageInspectionTypeController@createType'])->name('inspectionType.createType');
        Route::get('inspection-type/edit-type/{id}', ['uses' => 'ManageInspectionTypeController@editType'])->name('inspectionType.editType');
        Route::post('inspection-type/update-type/{id}', ['uses' => 'ManageInspectionTypeController@updateType'])->name('inspectionType.updateType');
        Route::delete('inspection-type/delete-type/{id}', ['uses' => 'ManageInspectionTypeController@destroyType'])->name('inspectionType.destroyType');
        Route::resource('inspection-type', 'ManageInspectionTypeController');

        //input-fields
        Route::post('input-fields/store-fields', ['uses' => 'ManageInputFieldsController@storeFields'])->name('inputFields.storeFields');
        Route::get('input-fields/create-fields', ['uses' => 'ManageInputFieldsController@createFields'])->name('inputFields.createFields');
        Route::get('input-fields/edit-fields/{id}', ['uses' => 'ManageInputFieldsController@editFields'])->name('inputFields.editFields');
        Route::post('input-fields/update-fields/{id}', ['uses' => 'ManageInputFieldsController@updateFields'])->name('inputFields.updateFields');
        Route::delete('input-fields/delete-fields/{id}', ['uses' => 'ManageInputFieldsController@destroyFields'])->name('inputFields.destroyFields');
        Route::resource('input-fields', 'ManageInputFieldsController');

        //inspection-name
        Route::post('inspection-name/store-name', ['uses' => 'ManageInspectionNameController@storeName'])->name('inspectionName.storeName');
        Route::get('inspection-name/create-name', ['uses' => 'ManageInspectionNameController@createName'])->name('inspectionName.createName');
        Route::get('inspection-name/edit-name/{id}', ['uses' => 'ManageInspectionNameController@editName'])->name('inspectionName.editName');
        Route::post('inspection-name/update-name/{id}', ['uses' => 'ManageInspectionNameController@updateName'])->name('inspectionName.updateName');
        Route::delete('inspection-name/delete-name/{id}', ['uses' => 'ManageInspectionNameController@destroyName'])->name('inspectionName.destroyName');
        Route::post('inspection-name/store-image', ['uses' => 'ManageInspectionNameController@storeImage'])->name('inspectionName.storeImage');
        Route::get('inspection-name/inspection-answer', ['uses' => 'ManageInspectionNameController@inspectionAnswer'])->name('inspectionName.inspectionAnswer');
        Route::get('inspection-name/inspection-answer-form/{id}', ['uses' => 'ManageInspectionNameController@inspectionAnswerForm'])->name('inspectionName.inspectionAnswerForm');
        Route::match(['get','post'],'inspection-name/inspection-question/{id}', ['uses' => 'ManageInspectionNameController@inspectionQuestion'])->name('inspectionName.inspectionQuestion');
        Route::post('inspection-name/get-data', ['uses' => 'ManageInspectionNameController@getData'])->name('inspectionName.getData');
        Route::match(['get','post'],'inspection-name/store-question', ['uses' => 'ManageInspectionNameController@storeQuestion'])->name('inspectionName.storeQuestion');
        Route::match(['get','post'],'inspection-name/store-answer', ['uses' => 'ManageInspectionNameController@storeAnswer'])->name('inspectionName.storeAnswer');
        Route::match(['get','post'],'inspection-name/assign-user', ['uses' => 'ManageInspectionNameController@assignUser'])->name('inspectionName.assignUser');
        Route::match(['get','post'],'inspection-name/assign-user-store', ['uses' => 'ManageInspectionNameController@assignUserStore'])->name('inspectionName.assignUserStore');
        Route::match(['get','post'],'inspection-name/inspection-replies', ['uses' => 'ManageInspectionNameController@inspectionReplies'])->name('inspectionName.inspectionReplies');
        Route::match(['get','post'],'inspection-name/inspection-replies-view/{id}', ['uses' => 'ManageInspectionNameController@inspectionRepliesView'])->name('inspectionName.inspectionRepliesView');
        Route::delete('inspection-name/removeFile/{id}', ['uses' => 'ManageInspectionNameController@removeFile'])->name('inspectionName.removeFile');
        Route::resource('inspection-name', 'ManageInspectionNameController');

        //memberrfi

        Route::post('rfi/store-rfi', ['uses' => 'ManageRfiController@storeRfi'])->name('rfi.storeRfi');
        Route::get('rfi/create-rfi', ['uses' => 'ManageRfiController@createRfi'])->name('rfi.createRfi');
        Route::get('rfi/edit-rfi/{id}', ['uses' => 'ManageRfiController@editRfi'])->name('rfi.editRfi');
        Route::post('rfi/update-rfi/{id}', ['uses' => 'ManageRfiController@updateRfi'])->name('rfi.updateRfi');
        Route::delete('rfi/delete-rfi/{id}', ['uses' => 'ManageRfiController@destroyRfi'])->name('rfi.destroyRfi');
        Route::post('rfi/storeImage', ['uses' => 'ManageRfiController@storeImage'])->name('rfi.storeImage');
        Route::post('rfi/officialresponse', ['uses' => 'ManageRfiController@officialResponse'])->name('rfi.officialResponse');
        Route::delete('rfi/removeFile/{id}', ['uses' => 'ManageRfiController@removeFile'])->name('rfi.removeFile');
        Route::get('rfi/details/{id}', ['uses' => 'ManageRfiController@detailsRfi'])->name('rfi.details');
        Route::post('rfi/replyPost/{taskid}', ['uses' => 'ManageRfiController@replyPost'])->name('rfi.replyPost');
        Route::post('rfi/projecttitles', ['uses' => 'ManageRfiController@projectTitles'])->name('rfi.projecttitles');
        Route::post('rfi/costitembytitle', ['uses' => 'ManageRfiController@costitemBytitle'])->name('rfi.costitembytitle');
        Route::resource('rfi', 'ManageRfiController');

        //product-type-workforce
        Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-workforce.addProduct');
        Route::post('product-type-workforce/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-workforce.storeProduct');
        Route::get('product-type-workforce/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-workforce.editProduct');
        Route::post('product-type-workforce/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-workforce.updateProduct');
        Route::post('product-type-workforce/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-workforce.store-type');
        Route::get('product-type-workforce/create-type', ['uses' => 'ManageProductTypeController@createType'])->name('product-type-workforce.create-type');
        Route::get('product-type-workforce/edit-type/{id}', ['uses' => 'ManageProductTypeController@editType'])->name('product-type-workforce.edit-type');
        Route::post('product-type-workforce/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-workforce.update-type');
        Route::get('product-type-workforce/workforce-index', ['uses' => 'ManageProductTypeController@workforceIndex'])->name('product-type-workforce.workforce-index');
        Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-workforce.addProcuct');
        Route::resource('product-type-workforce', 'ManageProductTypeController');

        //product-type-equipment
        Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-equipment.addProduct');
        Route::post('product-type-equipment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-equipment.storeProduct');
        Route::get('product-type-equipment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-equipment.editProduct');
        Route::post('product-type-equipment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-equipment.updateProduct');
        Route::post('product-type-equipment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-equipment.store-type');
        Route::get('product-type-equipment/create-type', ['uses' => 'ManageProductTypeController@createEquipmentType'])->name('product-type-equipment.create-equipment-type');
        Route::get('product-type-equipment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editEquipmentType'])->name('product-type-equipment.edit-equipment-type');
        Route::post('product-type-equipment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-equipment.update-type');
        Route::get('product-type-equipment/equipment-index', ['uses' => 'ManageProductTypeController@equipmentIndex'])->name('product-type-equipment.equipment-index');
        Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-equipment.addProcuct');
        Route::resource('product-type-equipment', 'ManageProductTypeController');

        //product-type-material
        Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-material.addProduct');
        Route::post('product-type-material/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-material.storeProduct');
        Route::get('product-type-material/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-material.editProduct');
        Route::post('product-type-material/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-material.updateProduct');
        Route::post('product-type-material/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-material.store-type');
        Route::get('product-type-material/create-type', ['uses' => 'ManageProductTypeController@createMaterialType'])->name('product-type-material.create-material-type');
        Route::get('product-type-material/edit-type/{id}', ['uses' => 'ManageProductTypeController@editMaterialType'])->name('product-type-material.edit-material-type');
        Route::post('product-type-material/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-material.update-type');
        Route::get('product-type-material/equipment-index', ['uses' => 'ManageProductTypeController@materialIndex'])->name('product-type-material.material-index');
        Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-material.addProcuct');
        Route::resource('product-type-material', 'ManageProductTypeController');

        //product-type-commitment
        Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-commitment.addProduct');
        Route::post('product-type-commitment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-commitment.storeProduct');
        Route::get('product-type-commitment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-commitment.editProduct');
        Route::post('product-type-commitment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-commitment.updateProduct');
        Route::post('product-type-commitment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-commitment.store-type');
        Route::get('product-type-commitment/create-type', ['uses' => 'ManageProductTypeController@createCommitmentType'])->name('product-type-commitment.create-commitment-type');
        Route::get('product-type-commitment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editCommitmentType'])->name('product-type-commitment.edit-commitment-type');
        Route::post('product-type-commitment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-commitment.update-type');
        Route::get('product-type-commitment/commitment-index', ['uses' => 'ManageProductTypeController@commitmentIndex'])->name('product-type-commitment.commitment-index');
        Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-commitment.addProcuct');
        Route::resource('product-type-commitment', 'ManageProductTypeController');

        //product-type-owner-cost
        Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-owner-cost.addProduct');
        Route::post('product-type-owner-cost/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-owner-cost.storeProduct');
        Route::get('product-type-owner-cost/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-owner-cost.editProduct');
        Route::post('product-type-owner-cost/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-owner-cost.updateProduct');
        Route::post('product-type-owner-cost/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-owner-cost.store-type');
        Route::get('product-type-owner-cost/create-type', ['uses' => 'ManageProductTypeController@createOwnerCostType'])->name('product-type-owner-cost.create-owner-cost-type');
        Route::get('product-type-owner-cost/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOwnerCostType'])->name('product-type-owner-cost.edit-owner-cost-type');
        Route::post('product-type-owner-cost/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-owner-cost.update-type');
        Route::get('product-type-owner-cost/owner-cost-index', ['uses' => 'ManageProductTypeController@ownerCostIndex'])->name('product-type-owner-cost.owner-cost-index');
        Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-owner-cost.addProcuct');
        Route::resource('product-type-owner-cost', 'ManageProductTypeController');

        //product-type-professional
        Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-professional.addProduct');
        Route::post('product-type-professional/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-professional.storeProduct');
        Route::get('product-type-professional/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-professional.editProduct');
        Route::post('product-type-professional/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-professional.updateProduct');
        Route::post('product-type-professional/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-professional.store-type');
        Route::get('product-type-professional/create-type', ['uses' => 'ManageProductTypeController@createProfessionalServicesType'])->name('product-type-professional.create-professional-type');
        Route::get('product-type-professional/edit-type/{id}', ['uses' => 'ManageProductTypeController@editProfessionalServicesType'])->name('product-type-professional.edit-professional-type');
        Route::post('product-type-professional/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-professional.update-type');
        Route::get('product-type-professional/professional-index', ['uses' => 'ManageProductTypeController@professionalServicesIndex'])->name('product-type-professional.professional-index');
        Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-professional.addProcuct');
        Route::resource('product-type-professional', 'ManageProductTypeController');

        //product-type-other
        Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-other.addProduct');
        Route::post('product-type-other/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-other.storeProduct');
        Route::get('product-type-other/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-other.editProduct');
        Route::post('product-type-other/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-other.updateProduct');
        Route::post('product-type-other/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-other.store-type');
        Route::get('product-type-other/create-type', ['uses' => 'ManageProductTypeController@createOtherType'])->name('product-type-other.create-other-type');
        Route::get('product-type-other/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOtherType'])->name('product-type-other.edit-other-type');
        Route::post('product-type-other/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-other.update-type');
        Route::get('product-type-other/other-index', ['uses' => 'ManageProductTypeController@otherIndex'])->name('product-type-other.other-index');
        Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-other.addProcuct');
        Route::resource('product-type-other', 'ManageProductTypeController');

        //condition
        Route::post('condition/store-condition', ['uses' => 'ManageConditionController@storeCondition'])->name('condition.store-condition');
        Route::get('condition/create-condition', ['uses' => 'ManageConditionController@createCondition'])->name('condition.create-condition');
        Route::get('condition/edit-condition/{id}', ['uses' => 'ManageConditionController@editCondition'])->name('condition.edit-condition');
        Route::post('condition/update-condition/{id}', ['uses' => 'ManageConditionController@updateCondition'])->name('condition.update-condition');
        Route::resource('condition', 'ManageConditionController');

        //manage-files
        Route::group(
            ['prefix' => 'manage-files'],
            function() {
                Route::post('manage-files/move-pathtype', ['uses' => 'ManageFilesController@movePathType'])->name('manage-files.movepathtype');
                Route::post('manage-files/store-link', ['uses' => 'ManageFilesController@storeLink'])->name('manage-files.storeLink');
                Route::post('manage-files/create-folder', ['uses' => 'ManageFilesController@createFolder'])->name('manage-files.createFolder');
                Route::post('manage-files/edit-folder', ['uses' => 'ManageFilesController@editFolder'])->name('manage-files.editFolder');
                Route::get('manage-files/download/{id}', ['uses' => 'ManageFilesController@download'])->name('manage-files.download');
                Route::get('manage-files/thumbnail', ['uses' => 'ManageFilesController@thumbnailShow'])->name('manage-files.thumbnail');
                Route::post('manage-files/multiple-upload', ['uses' => 'ManageFilesController@storeMultiple'])->name('manage-files.multiple-upload');
                Route::post('manage-files/storeImage', ['uses' => 'ManageFilesController@storeImage'])->name('manage-files.storeImage');
                Route::resource('manage-files', 'ManageFilesController');
            }
        );
    });

    // Client routes
    Route::group(
        ['namespace' => 'Client', 'prefix' => 'client', 'as' => 'client.', 'middleware' => []],
        function () {

        Route::resource('dashboard', 'ClientDashboardController');
        Route::resource('profile', 'ClientProfileController');

        // Project section
        Route::get('projects/data', ['uses' => 'ClientProjectsController@data'])->name('projects.data');

            //added from admin
            Route::get('projects/boq/{projectId?}', ['uses' => 'ClientProjectsController@Boq'])->name('projects.boq');
            Route::get('projects/project-boq-create/{projectId?}', ['uses' => 'ClientProjectsController@projectBoqCreate'])->name('projects.projectBoqCreate');
            Route::get('projects/edit-file/{projectId}/{fileId}', ['uses' => 'ClientProjectsController@projectEditFile'])->name('projects.projectEditFile');
            Route::match(['get','post'],'projects/titleCreate/{projectId?}', ['uses' => 'ClientProjectsController@titleCreate'])->name('projects.titleCreate');
            Route::match(['get','post'],'projects/schedulingData/{projectId?}', ['uses' => 'ClientProjectsController@schedulingData'])->name('projects.schedulingData');
            Route::match(['get','post'],'projects/scheduling/{projectId?}', ['uses' => 'ClientProjectsController@scheduling'])->name('projects.scheduling');
            Route::match(['get','post'],'projects/getItemLavel', ['uses' => 'ClientProjectsController@getItemLavel'])->name('projects.getItemLavel');
            Route::match(['get','post'],'projects/getChild', ['uses' => 'ClientProjectsController@getChild'])->name('projects.getChild');
            Route::match(['get','post'],'projects/getProduct', ['uses' => 'ClientProjectsController@getProduct'])->name('projects.getProduct');
            Route::match(['get','post'],'projects/getCostItem', ['uses' => 'ClientProjectsController@getCostItem'])->name('projects.getCostItem');
            Route::match(['get','post'],'projects/updateStatus/{id}', ['uses' => 'ClientProjectsController@updateStatus'])->name('projects.updateStatus');
            Route::match(['get','post'],'projects/viewBoq/{id}/{title}', ['uses' => 'ClientProjectsController@viewBoq'])->name('projects.viewBoq');
            Route::match(['get','post'],'projects/editBoq/{id}/{title}', ['uses' => 'ClientProjectsController@editBoq'])->name('projects.editBoq');
            Route::match(['get','post'],'projects/updateBoq/{id}/{title}', ['uses' => 'ClientProjectsController@updateBoq'])->name('projects.updateBoq');
            Route::match(['get','post'],'projects/addCostItems', ['uses' => 'ClientProjectsController@addCostItems'])->name('projects.addCostItems');
            Route::match(['get','post'],'projects/ProjectFinalQty/{id}', ['uses' => 'ClientProjectsController@ProjectFinalQty'])->name('projects.ProjectFinalQty');
            Route::match(['get','post'],'projects/schedulingDue/{columnId}', ['uses' => 'ClientProjectsController@schedulingDue'])->name('projects.schedulingDue');
            Route::match(['get','post'],'projects/getFiles', ['uses' => 'ClientProjectsController@getFiles'])->name('projects.getFiles');
            Route::match(['get','post'],'projects/openFolder/{id}', ['uses' => 'ClientProjectsController@openFolder'])->name('projects.openFolder');
            Route::match(['get','post'],'projects/showFiles/{id}', ['uses' => 'ClientProjectsController@showFiles'])->name('projects.showFiles');
            Route::match(['get','post'],'projects/revisionFiles/{prid}/{id}', ['uses' => 'ClientProjectsController@revisionFiles'])->name('projects.revisionFiles');
            Route::match(['get','post'],'projects/selectFile/{id}', ['uses' => 'ClientProjectsController@selectFile'])->name('projects.selectFile');
            Route::delete('projects/deleteFile/{id}', ['uses' => 'ClientProjectsController@deleteFile'])->name('projects.deleteFile');
            Route::match(['get','post'],'projects/getDesignFiles', ['uses' => 'ClientProjectsController@getDesignFiles'])->name('projects.getDesignFiles');
            Route::match(['get','post'],'projects/openDesignFolder/{id}', ['uses' => 'ClientProjectsController@openDesignFolder'])->name('projects.openDesignFolder');
            Route::match(['get','post'],'projects/selectDesignFile/{id}', ['uses' => 'ClientProjectsController@selectDesignFile'])->name('projects.selectDesignFile');
            Route::delete('projects/deleteDesignFile/{id}', ['uses' => 'ClientProjectsController@deleteDesignFile'])->name('projects.deleteDesignFile');
            Route::match(['get','post'],'projects/sourcingPackages/{id}', ['uses' => 'ClientProjectsController@sourcingPackages'])->name('projects.sourcingPackages');

            //sourcing packages
            Route::match(['get','post'],'projects/sourcingPackages/{id}', ['uses' => 'ClientProjectsController@sourcingPackages'])->name('projects.sourcingPackages');
            Route::match(['get','post'],'projects/addSourcingPackages/{id}', ['uses' => 'ClientProjectsController@addSourcingPackages'])->name('projects.addSourcingPackages');
            Route::match(['get','post'],'projects/storeSourcingPackages/{id}', ['uses' => 'ClientProjectsController@storeSourcingPackages'])->name('projects.storeSourcingPackages');
            Route::match(['get','post'],'projects/editSourcingPackages/{pid}/{id}', ['uses' => 'ClientProjectsController@editSourcingPackages'])->name('projects.editSourcingPackages');
            Route::match(['get','post'],'projects/updateSourcingPackages/{id}', ['uses' => 'ClientProjectsController@updateSourcingPackages'])->name('projects.updateSourcingPackages');
            Route::match(['get','post'],'projects/getProducts/', ['uses' => 'ClientProjectsController@getProducts'])->name('projects.getProducts');
            Route::match(['get','post'],'projects/filterData/', ['uses' => 'ClientProjectsController@filterData'])->name('projects.filterData');
            Route::delete('projects/deleteSourcingPackages/{id}', ['uses' => 'ClientProjectsController@deleteSourcingPackages'])->name('projects.deleteSourcingPackages');
            Route::match(['get','post'],'projects/createTender/{pid}/{id}', ['uses' => 'ClientProjectsController@createTender'])->name('projects.createTender');
            Route::match(['get','post'],'projects/editTender/{pid}/{sid}/{id}', ['uses' => 'ClientProjectsController@editTender'])->name('projects.editTender');
            Route::match(['get','post'],'projects/assignTender/{pid}/{sid}/{id}', ['uses' => 'ClientProjectsController@assignTender'])->name('projects.assignTender');
            Route::match(['get','post'],'projects/storeAssignTender/', ['uses' => 'ClientProjectsController@storeAssignTender'])->name('projects.storeAssignTender');
            Route::match(['get','post'],'projects/tendersList/{pid}/{id}', ['uses' => 'ClientProjectsController@tendersList'])->name('projects.tendersList');
            Route::match(['get','post'],'projects/getBrandsTag', ['uses' => 'ClientProjectsController@getBrandsTag'])->name('projects.getBrandsTag');
            Route::match(['get','post'],'projects/saveTenders', ['uses' => 'ClientProjectsController@saveTenders'])->name('projects.saveTenders');
            Route::match(['get','post'],'projects/storeTenderFiles', ['uses' => 'ClientProjectsController@storeTenderFiles'])->name('projects.storeTenderFiles');
            Route::match(['get','post'],'projects/tenderBiddings', ['uses' => 'ClientProjectsController@tenderBiddings'])->name('projects.tenderBiddings');
            Route::match(['get','post'],'projects/tenderBiddingDetails/{id}', ['uses' => 'ClientProjectsController@tenderBiddingDetails'])->name('projects.tenderBiddingDetails');
            Route::resource('projects', 'ClientProjectsController');

        Route::group(
            ['prefix' => 'projects'],
            function () {
            Route::resource('project-members', 'ClientProjectMembersController');
            Route::resource('tasks', 'ClientTasksController');
            Route::get('files/download/{id}', ['uses' => 'ClientFilesController@download'])->name('files.download');
            Route::get('files/thumbnail', ['uses' => 'ClientFilesController@thumbnailShow'])->name('files.thumbnail');
            Route::resource('files', 'ClientFilesController');
            Route::get('time-log/data/{id}', ['uses' => 'ClientTimeLogController@data'])->name('time-log.data');
            Route::resource('time-log', 'ClientTimeLogController');
            Route::get('project-invoice/download/{id}', ['uses' => 'ClientProjectInvoicesController@download'])->name('project-invoice.download');
            Route::resource('project-invoice', 'ClientProjectInvoicesController');
        });

        //region Products Routes
        Route::get('products/data', ['uses' => 'ClientProductController@data'])->name('products.data');
        Route::get('products/update-item', ['uses' => 'ClientProductController@addItems'])->name('products.update-item');
        Route::resource('products', 'ClientProductController');

        //sticky note
        Route::resource('sticky-note', 'ClientStickyNoteController');

        // Invoice Section
        Route::get('invoices/download/{id}', ['uses' => 'ClientInvoicesController@download'])->name('invoices.download');
        Route::get('invoices/offline-payment', 'ClientInvoicesController@offlinePayment')->name('invoices.offline-payment');
        Route::post('invoices/offline-payment-submit', 'ClientInvoicesController@offlinePaymentSubmit')->name('invoices.offline-payment-submit');
        Route::resource('invoices', 'ClientInvoicesController');

        // Estimate Section
        Route::get('estimates/download/{id}', ['uses' => 'ClientEstimateController@download'])->name('estimates.download');
        Route::resource('estimates', 'ClientEstimateController');

        //Payments section
        Route::get('payments/data', ['uses' => 'ClientPaymentsController@data'])->name('payments.data');
        Route::resource('payments', 'ClientPaymentsController');

        // Issues section
        Route::get('my-issues/data', ['uses' => 'ClientMyIssuesController@data'])->name('my-issues.data');
        Route::resource('my-issues', 'ClientMyIssuesController');

        // route for view/blade file
        Route::get('paywithpaypal', array('as' => 'paywithpaypal','uses' => 'PaypalController@payWithPaypal',));

        // change language
        Route::get('language/change-language', ['uses' => 'ClientProfileController@changeLanguage'])->name('language.change-language');
        // change company
        Route::get('company/change-company', ['uses' => 'ClientProfileController@changeCompany'])->name('company.change-company');

        //Tickets routes
        Route::get('tickets/data', ['uses' => 'ClientTicketsController@data'])->name('tickets.data');
        Route::post('tickets/close-ticket/{id}', ['uses' => 'ClientTicketsController@closeTicket'])->name('tickets.closeTicket');
        Route::post('tickets/open-ticket/{id}', ['uses' => 'ClientTicketsController@reopenTicket'])->name('tickets.reopenTicket');
        Route::resource('tickets', 'ClientTicketsController');
        Route::resource('events', 'ClientEventController');

        Route::post('gdpr/update-consent', ['uses' => 'ClientGdprController@updateConsent'])->name('gdpr.update-consent');
        Route::get('gdpr/consent', ['uses' => 'ClientGdprController@consent'])->name('gdpr.consent');
        Route::get('gdpr/download', ['uses' => 'ClientGdprController@downloadJSON'])->name('gdpr.download-json');
        Route::post('gdpr/remove-request', ['uses' => 'ClientGdprController@removeRequest'])->name('gdpr.remove-request');
        Route::get('privacy-policy', ['uses' => 'ClientGdprController@privacy'])->name('gdpr.privacy');
        Route::get('terms-and-condition', ['uses' => 'ClientGdprController@terms'])->name('gdpr.terms');
        Route::resource('gdpr', 'ClientGdprController');

        // User message
        Route::post('message-submit', ['as' => 'user-chat.message-submit', 'uses' => 'ClientChatController@postChatMessage']);
        Route::get('user-search', ['as' => 'user-chat.user-search', 'uses' => 'ClientChatController@getUserSearch']);
        Route::resource('user-chat', 'ClientChatController');

        //task comments
        Route::resource('task-comment', 'ClientTaskCommentController');
        Route::post('pay-with-razorpay', array('as' => 'pay-with-razorpay','uses' => 'RazorPayController@payWithRazorPay',));

        //region contracts routes
        Route::get('contracts/data', ['as' => 'contracts.data', 'uses' => 'ClientContractController@data']);
        Route::get('contracts/download/{id}', ['as' => 'contracts.download', 'uses' => 'ClientContractController@download']);
        Route::get('contracts/sign/{id}', ['as' => 'contracts.sign-modal', 'uses' => 'ClientContractController@signModal']);
        Route::post('contracts/sign/{id}', ['as' => 'contracts.sign', 'uses' => 'ClientContractController@sign']);
        Route::post('contracts/add-discussion/{id}', ['as' => 'contracts.add-discussion', 'uses' => 'ClientContractController@addDiscussion']);
        Route::get('contracts/edit-discussion/{id}', ['as' => 'contracts.edit-discussion', 'uses' => 'ClientContractController@editDiscussion']);
        Route::post('contracts/update-discussion/{id}', ['as' => 'contracts.update-discussion', 'uses' => 'ClientContractController@updateDiscussion']);
        Route::post('contracts/remove-discussion/{id}', ['as' => 'contracts.remove-discussion', 'uses' => 'ClientContractController@removeDiscussion']);
        Route::resource('contracts', 'ClientContractController');
        //endregion

        //Notice
        Route::get('notices/data', ['uses' => 'ClientNoticesController@data'])->name('notices.data');
        Route::resource('notices', 'ClientNoticesController');

            //boq-category
            Route::post('activity/store-cat', ['uses' => 'ManageBoqCategoryController@storeCat'])->name('activity.store-cat');
            Route::get('activity/create-cat', ['uses' => 'ManageBoqCategoryController@createCat'])->name('activity.create-cat');
            Route::get('activity/edit-cat/{id}', ['uses' => 'ManageBoqCategoryController@editCat'])->name('activity.edit-cat');
            Route::post('activity/update-cat/{id}', ['uses' => 'ManageBoqCategoryController@updateCat'])->name('activity.update-cat');
            Route::resource('activity', 'ManageBoqCategoryController');

            //task
           /* Route::post('task/store-cat', ['uses' => 'CostItemsController@storeCat'])->name('task.store-cat');
            Route::get('task/create-cat', ['uses' => 'CostItemsController@createCat'])->name('task.create-cat');
            Route::get('task/edit-cat/{id}', ['uses' => 'CostItemsController@editCat'])->name('task.edit-cat');
            Route::post('task/update-cat/{id}', ['uses' => 'CostItemsController@updateCat'])->name('task.update-cat');
            Route::post('task/get-cost', ['uses' => 'CostItemsController@getCost'])->name('task.getCost');
            Route::match(['get','post'],'task/product-add/{id}', ['uses' => 'CostItemsController@productAdd'])->name('task.product-add');
            Route::match(['get','post'],'task/product-edit/{id}', ['uses' => 'CostItemsController@productEdit'])->name('task.product-edit');
            Route::post('task/update-product/{id}', ['uses' => 'CostItemsController@productUpdate'])->name('task.update-product');
            Route::delete('task/destroy-product/{id}', ['uses' => 'CostItemsController@destroyProduct'])->name('task.destroy-product');
            Route::post('task/store-product/{id}', ['uses' => 'CostItemsController@storeProduct'])->name('task.store-product');
           Route::resource('task', 'CostItemsController');*/

            //product-category
            Route::get('product-category/index', ['uses' => 'ProductCategoryController@index'])->name('productCategory.index');
            Route::resource('product-category', 'ProductCategoryController');

            //product-brand
            Route::get('product-brand/index', ['uses' => 'ProductBrandController@index'])->name('productBrand.index');
            Route::resource('product-brand', 'ProductBrandController');

            //issue
            Route::get('issue/export', ['uses' => 'ManagePunchItemController@export'])->name('issue.export');
            Route::post('issue/data', ['uses' => 'ManagePunchItemController@data'])->name('issue.data');
            Route::post('issue/storeImage', ['uses' => 'ManagePunchItemController@storeImage'])->name('issue.storeImage');
            Route::get('issue/dependent-tasks/{projectId}/{taskId?}', ['uses' => 'ManagePunchItemController@dependentTaskLists'])->name('issue.dependent-tasks');
            Route::get('issue/members/{projectId}', ['uses' => 'ManagePunchItemController@membersList'])->name('issue.members');
            Route::get('issue/ajaxCreate/{columnId}', ['uses' => 'ManagePunchItemController@ajaxCreate'])->name('issue.ajaxCreate');
            Route::get('issue/reminder/{taskid}', ['uses' => 'ManagePunchItemController@remindForTask'])->name('issue.reminder');
            Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
            Route::delete('issue/removeFile/{id}', ['uses' => 'ManagePunchItemController@removeFile'])->name('issue.removeFile');
            Route::get('issue/files/{taskid}', ['uses' => 'ManagePunchItemController@showFiles'])->name('issue.show-files');
            Route::get('issue/reply/{taskid}', ['uses' => 'ManagePunchItemController@reply'])->name('issue.reply');
            Route::get('issue/comment/{taskid}', ['uses' => 'ManagePunchItemController@comment'])->name('issue.comment');
            Route::post('issue/replyPost/{taskid}', ['uses' => 'ManagePunchItemController@replyPost'])->name('issue.replyPost');
            Route::post('issue/commentPost/{taskid}', ['uses' => 'ManagePunchItemController@commentPost'])->name('issue.commentPost');
            Route::match(['get','post'],'issue/issuedata', ['uses' => 'ManagePunchItemController@issueData'])->name('issue.issueData');
            Route::resource('issue', 'ManagePunchItemController');

            //inspection-type
            Route::post('inspection-type/store-type', ['uses' => 'ManageInspectionTypeController@storeType'])->name('inspectionType.storeType');
            Route::get('inspection-type/create-type', ['uses' => 'ManageInspectionTypeController@createType'])->name('inspectionType.createType');
            Route::get('inspection-type/edit-type/{id}', ['uses' => 'ManageInspectionTypeController@editType'])->name('inspectionType.editType');
            Route::post('inspection-type/update-type/{id}', ['uses' => 'ManageInspectionTypeController@updateType'])->name('inspectionType.updateType');
            Route::delete('inspection-type/delete-type/{id}', ['uses' => 'ManageInspectionTypeController@destroyType'])->name('inspectionType.destroyType');
            Route::resource('inspection-type', 'ManageInspectionTypeController');

            //input-fields
            Route::post('input-fields/store-fields', ['uses' => 'ManageInputFieldsController@storeFields'])->name('inputFields.storeFields');
            Route::get('input-fields/create-fields', ['uses' => 'ManageInputFieldsController@createFields'])->name('inputFields.createFields');
            Route::get('input-fields/edit-fields/{id}', ['uses' => 'ManageInputFieldsController@editFields'])->name('inputFields.editFields');
            Route::post('input-fields/update-fields/{id}', ['uses' => 'ManageInputFieldsController@updateFields'])->name('inputFields.updateFields');
            Route::delete('input-fields/delete-fields/{id}', ['uses' => 'ManageInputFieldsController@destroyFields'])->name('inputFields.destroyFields');
            Route::resource('input-fields', 'ManageInputFieldsController');

            //inspection-name
            Route::post('inspection-name/store-name', ['uses' => 'ManageInspectionNameController@storeName'])->name('inspectionName.storeName');
            Route::get('inspection-name/create-name', ['uses' => 'ManageInspectionNameController@createName'])->name('inspectionName.createName');
            Route::get('inspection-name/edit-name/{id}', ['uses' => 'ManageInspectionNameController@editName'])->name('inspectionName.editName');
            Route::post('inspection-name/update-name/{id}', ['uses' => 'ManageInspectionNameController@updateName'])->name('inspectionName.updateName');
            Route::delete('inspection-name/delete-name/{id}', ['uses' => 'ManageInspectionNameController@destroyName'])->name('inspectionName.destroyName');
            Route::post('inspection-name/store-image', ['uses' => 'ManageInspectionNameController@storeImage'])->name('inspectionName.storeImage');
            Route::post('inspection-name/delete-question', ['uses' => 'ManageInspectionNameController@destroyQuestion'])->name('inspectionName.destroyQuestion');
            Route::get('inspection-name/inspection-answer', ['uses' => 'ManageInspectionNameController@inspectionAnswer'])->name('inspectionName.inspectionAnswer');
            Route::get('inspection-name/inspection-answer-form/{id}', ['uses' => 'ManageInspectionNameController@inspectionAnswerForm'])->name('inspectionName.inspectionAnswerForm');
            Route::match(['get','post'],'inspection-name/inspection-question/{id}', ['uses' => 'ManageInspectionNameController@inspectionQuestion'])->name('inspectionName.inspectionQuestion');
            Route::post('inspection-name/get-data', ['uses' => 'ManageInspectionNameController@getData'])->name('inspectionName.getData');
            Route::match(['get','post'],'inspection-name/store-question', ['uses' => 'ManageInspectionNameController@storeQuestion'])->name('inspectionName.storeQuestion');
            Route::match(['get','post'],'inspection-name/store-answer', ['uses' => 'ManageInspectionNameController@storeAnswer'])->name('inspectionName.storeAnswer');
            Route::match(['get','post'],'inspection-name/assign-user', ['uses' => 'ManageInspectionNameController@assignUser'])->name('inspectionName.assignUser');
            Route::match(['get','post'],'inspection-name/assign-user-store', ['uses' => 'ManageInspectionNameController@assignUserStore'])->name('inspectionName.assignUserStore');
            Route::match(['get','post'],'inspection-name/inspection-replies', ['uses' => 'ManageInspectionNameController@inspectionReplies'])->name('inspectionName.inspectionReplies');
            Route::match(['get','post'],'inspection-name/inspection-replies-view/{id}/{userId}', ['uses' => 'ManageInspectionNameController@inspectionRepliesView'])->name('inspectionName.inspectionRepliesView');
            Route::delete('inspection-name/removeFile/{id}', ['uses' => 'ManageInspectionNameController@removeFile'])->name('inspectionName.removeFile');
            Route::resource('inspection-name', 'ManageInspectionNameController');


            //clientrfi

            Route::post('rfi/store-rfi', ['uses' => 'ManageRfiController@storeRfi'])->name('rfi.storeRfi');
            Route::get('rfi/create-rfi', ['uses' => 'ManageRfiController@createRfi'])->name('rfi.createRfi');
            Route::get('rfi/edit-rfi/{id}', ['uses' => 'ManageRfiController@editRfi'])->name('rfi.editRfi');
            Route::post('rfi/update-rfi/{id}', ['uses' => 'ManageRfiController@updateRfi'])->name('rfi.updateRfi');
            Route::delete('rfi/delete-rfi/{id}', ['uses' => 'ManageRfiController@destroyRfi'])->name('rfi.destroyRfi');
            Route::post('rfi/storeImage', ['uses' => 'ManageRfiController@storeImage'])->name('rfi.storeImage');
            Route::post('rfi/officialresponse', ['uses' => 'ManageRfiController@officialResponse'])->name('rfi.officialResponse');
            Route::delete('rfi/removeFile/{id}', ['uses' => 'ManageRfiController@removeFile'])->name('rfi.removeFile');
            Route::get('rfi/details/{id}', ['uses' => 'ManageRfiController@detailsRfi'])->name('rfi.details');
            Route::post('rfi/replyPost/{taskid}', ['uses' => 'ManageRfiController@replyPost'])->name('rfi.replyPost');
            Route::post('rfi/projecttitles', ['uses' => 'ManageRfiController@projectTitles'])->name('rfi.projecttitles');
            Route::post('rfi/costitembytitle', ['uses' => 'ManageRfiController@costitemBytitle'])->name('rfi.costitembytitle');
            Route::resource('rfi', 'ManageRfiController');

            //product-type-workforce
            Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-workforce.addProduct');
            Route::post('product-type-workforce/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-workforce.storeProduct');
            Route::get('product-type-workforce/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-workforce.editProduct');
            Route::post('product-type-workforce/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-workforce.updateProduct');
            Route::post('product-type-workforce/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-workforce.store-type');
            Route::get('product-type-workforce/create-type', ['uses' => 'ManageProductTypeController@createType'])->name('product-type-workforce.create-type');
            Route::get('product-type-workforce/edit-type/{id}', ['uses' => 'ManageProductTypeController@editType'])->name('product-type-workforce.edit-type');
            Route::post('product-type-workforce/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-workforce.update-type');
            Route::get('product-type-workforce/workforce-index', ['uses' => 'ManageProductTypeController@workforceIndex'])->name('product-type-workforce.workforce-index');
            Route::get('product-type-workforce/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-workforce.addProcuct');
            Route::resource('product-type-workforce', 'ManageProductTypeController');

            //product-type-equipment
            Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-equipment.addProduct');
            Route::post('product-type-equipment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-equipment.storeProduct');
            Route::get('product-type-equipment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-equipment.editProduct');
            Route::post('product-type-equipment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-equipment.updateProduct');
            Route::post('product-type-equipment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-equipment.store-type');
            Route::get('product-type-equipment/create-type', ['uses' => 'ManageProductTypeController@createEquipmentType'])->name('product-type-equipment.create-equipment-type');
            Route::get('product-type-equipment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editEquipmentType'])->name('product-type-equipment.edit-equipment-type');
            Route::post('product-type-equipment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-equipment.update-type');
            Route::get('product-type-equipment/equipment-index', ['uses' => 'ManageProductTypeController@equipmentIndex'])->name('product-type-equipment.equipment-index');
            Route::get('product-type-equipment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-equipment.addProcuct');
            Route::resource('product-type-equipment', 'ManageProductTypeController');

            //product-type-material
            Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-material.addProduct');
            Route::post('product-type-material/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-material.storeProduct');
            Route::get('product-type-material/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-material.editProduct');
            Route::post('product-type-material/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-material.updateProduct');
            Route::post('product-type-material/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-material.store-type');
            Route::get('product-type-material/create-type', ['uses' => 'ManageProductTypeController@createMaterialType'])->name('product-type-material.create-material-type');
            Route::get('product-type-material/edit-type/{id}', ['uses' => 'ManageProductTypeController@editMaterialType'])->name('product-type-material.edit-material-type');
            Route::post('product-type-material/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-material.update-type');
            Route::get('product-type-material/equipment-index', ['uses' => 'ManageProductTypeController@materialIndex'])->name('product-type-material.material-index');
            Route::get('product-type-material/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-material.addProcuct');
            Route::resource('product-type-material', 'ManageProductTypeController');

            //product-type-commitment
            Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-commitment.addProduct');
            Route::post('product-type-commitment/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-commitment.storeProduct');
            Route::get('product-type-commitment/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-commitment.editProduct');
            Route::post('product-type-commitment/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-commitment.updateProduct');
            Route::post('product-type-commitment/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-commitment.store-type');
            Route::get('product-type-commitment/create-type', ['uses' => 'ManageProductTypeController@createCommitmentType'])->name('product-type-commitment.create-commitment-type');
            Route::get('product-type-commitment/edit-type/{id}', ['uses' => 'ManageProductTypeController@editCommitmentType'])->name('product-type-commitment.edit-commitment-type');
            Route::post('product-type-commitment/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-commitment.update-type');
            Route::get('product-type-commitment/commitment-index', ['uses' => 'ManageProductTypeController@commitmentIndex'])->name('product-type-commitment.commitment-index');
            Route::get('product-type-commitment/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-commitment.addProcuct');
            Route::resource('product-type-commitment', 'ManageProductTypeController');

            //product-type-owner-cost
            Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-owner-cost.addProduct');
            Route::post('product-type-owner-cost/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-owner-cost.storeProduct');
            Route::get('product-type-owner-cost/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-owner-cost.editProduct');
            Route::post('product-type-owner-cost/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-owner-cost.updateProduct');
            Route::post('product-type-owner-cost/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-owner-cost.store-type');
            Route::get('product-type-owner-cost/create-type', ['uses' => 'ManageProductTypeController@createOwnerCostType'])->name('product-type-owner-cost.create-owner-cost-type');
            Route::get('product-type-owner-cost/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOwnerCostType'])->name('product-type-owner-cost.edit-owner-cost-type');
            Route::post('product-type-owner-cost/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-owner-cost.update-type');
            Route::get('product-type-owner-cost/owner-cost-index', ['uses' => 'ManageProductTypeController@ownerCostIndex'])->name('product-type-owner-cost.owner-cost-index');
            Route::get('product-type-owner-cost/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-owner-cost.addProcuct');
            Route::resource('product-type-owner-cost', 'ManageProductTypeController');

            //product-type-professional
            Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-professional.addProduct');
            Route::post('product-type-professional/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-professional.storeProduct');
            Route::get('product-type-professional/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-professional.editProduct');
            Route::post('product-type-professional/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-professional.updateProduct');
            Route::post('product-type-professional/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-professional.store-type');
            Route::get('product-type-professional/create-type', ['uses' => 'ManageProductTypeController@createProfessionalServicesType'])->name('product-type-professional.create-professional-type');
            Route::get('product-type-professional/edit-type/{id}', ['uses' => 'ManageProductTypeController@editProfessionalServicesType'])->name('product-type-professional.edit-professional-type');
            Route::post('product-type-professional/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-professional.update-type');
            Route::get('product-type-professional/professional-index', ['uses' => 'ManageProductTypeController@professionalServicesIndex'])->name('product-type-professional.professional-index');
            Route::get('product-type-professional/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-professional.addProcuct');
            Route::resource('product-type-professional', 'ManageProductTypeController');

            //product-type-other
            Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProduct'])->name('product-type-other.addProduct');
            Route::post('product-type-other/store-product', ['uses' => 'ManageProductTypeController@storeProduct'])->name('product-type-other.storeProduct');
            Route::get('product-type-other/edit-product/{id}', ['uses' => 'ManageProductTypeController@editProduct'])->name('product-type-other.editProduct');
            Route::post('product-type-other/update-product/{id}', ['uses' => 'ManageProductTypeController@updateProduct'])->name('product-type-other.updateProduct');
            Route::post('product-type-other/store-type', ['uses' => 'ManageProductTypeController@storeType'])->name('product-type-other.store-type');
            Route::get('product-type-other/create-type', ['uses' => 'ManageProductTypeController@createOtherType'])->name('product-type-other.create-other-type');
            Route::get('product-type-other/edit-type/{id}', ['uses' => 'ManageProductTypeController@editOtherType'])->name('product-type-other.edit-other-type');
            Route::post('product-type-other/update-type/{id}', ['uses' => 'ManageProductTypeController@updateType'])->name('product-type-other.update-type');
            Route::get('product-type-other/other-index', ['uses' => 'ManageProductTypeController@otherIndex'])->name('product-type-other.other-index');
            Route::get('product-type-other/add-product', ['uses' => 'ManageProductTypeController@addProcuct'])->name('product-type-other.addProcuct');
            Route::resource('product-type-other', 'ManageProductTypeController');

            //condition
            Route::post('condition/store-condition', ['uses' => 'ManageConditionController@storeCondition'])->name('condition.store-condition');
            Route::get('condition/create-condition', ['uses' => 'ManageConditionController@createCondition'])->name('condition.create-condition');
            Route::get('condition/edit-condition/{id}', ['uses' => 'ManageConditionController@editCondition'])->name('condition.edit-condition');
            Route::post('condition/update-condition/{id}', ['uses' => 'ManageConditionController@updateCondition'])->name('condition.update-condition');
            Route::resource('condition', 'ManageConditionController');

            //manage-files
            Route::group(
                ['prefix' => 'manage-files'],
                function() {
                    Route::post('manage-files/store-link', ['uses' => 'ManageFilesController@storeLink'])->name('manage-files.storeLink');
                    Route::post('manage-files/create-folder', ['uses' => 'ManageFilesController@createFolder'])->name('manage-files.createFolder');
                    Route::post('manage-files/edit-folder', ['uses' => 'ManageFilesController@editFolder'])->name('manage-files.editFolder');
                    Route::get('manage-files/download/{id}', ['uses' => 'ManageFilesController@download'])->name('manage-files.download');
                    Route::get('manage-files/thumbnail', ['uses' => 'ManageFilesController@thumbnailShow'])->name('manage-files.thumbnail');
                    Route::post('manage-files/multiple-upload', ['uses' => 'ManageFilesController@storeMultiple'])->name('manage-files.multiple-upload');
                    Route::post('manage-files/storeImage', ['uses' => 'ManageFilesController@storeImage'])->name('manage-files.storeImage');
                    Route::resource('manage-files', 'ManageFilesController');
                }
            );
    });
    // Mark all notifications as read
    Route::post('mark-notification-read', ['uses' => 'NotificationController@markAllRead'])->name('mark-notification-read');
    Route::get('show-all-member-notifications', ['uses' => 'NotificationController@showAllMemberNotifications'])->name('show-all-member-notifications');
    Route::get('show-all-client-notifications', ['uses' => 'NotificationController@showAllClientNotifications'])->name('show-all-client-notifications');
    Route::get('show-all-admin-notifications', ['uses' => 'NotificationController@showAllAdminNotifications'])->name('show-all-admin-notifications');
    Route::post('mark-superadmin-notification-read', ['uses' => 'SuperAdmin\NotificationController@markAllRead'])->name('mark-superadmin-notification-read');
    Route::get('show-all-super-admin-notifications', ['uses' => 'SuperAdmin\NotificationController@showAllSuperAdminNotifications'])->name('show-all-super-admin-notifications');

});
Route::get('admin/projects/bidding/{company_id}/{project_id}/{tender_id}/{sourcing_id}/{user_id}', ['uses' => '\App\Http\Controllers\Admin\BiddingController@bidding'])->name('projects.bidding');
Route::post('admin/projects/biddingSubmit', ['uses' => '\App\Http\Controllers\Admin\BiddingController@biddingSubmit'])->name('projects.biddingSubmit');
Route::match(['get','post'],'admin/projects/success', ['uses' => '\App\Http\Controllers\Admin\BiddingController@success'])->name('projects.success');
Route::post('pulse/chat-box', ['uses' => '\App\Http\Controllers\Admin\PulseController@chatBox'])->name('pulse.chat-box');
Route::post('pulse/data', ['uses' => '\App\Http\Controllers\Admin\PulseController@data'])->name('pulse.data');
Route::post('mentions/data', ['uses' => '\App\Http\Controllers\Admin\PulseController@mentionsData'])->name('mentions.data');
Route::get('mentions', ['uses' => '\App\Http\Controllers\Admin\PulseController@mentionsindex'])->name('mentions.index');
Route::post('project-pulse/message-submit', ['uses' => '\App\Http\Controllers\Admin\PulseController@projectPulseMessageSubmit'])->name('projectpulse.message-submit');
Route::post('project-pulse/data', ['uses' => '\App\Http\Controllers\Admin\PulseController@projectPulseData'])->name('projectpulse.data');
Route::get('project-pulse', ['uses' => '\App\Http\Controllers\Admin\PulseController@projectPulseindex'])->name('projectpulse.index');
Route::resource('pulse', '\App\Http\Controllers\Admin\PulseController');

Route::post('channels/block', ['uses' => '\App\Http\Controllers\Admin\ChannelsController@channelBlock'])->name('channels.channelBlock');
Route::resource('channels', '\App\Http\Controllers\Admin\ChannelsController');

Route::post('groups-chat/block', ['uses' => '\App\Http\Controllers\Admin\GroupChatController@groupsChat'])->name('groups-chat.GroupChatBlock');
Route::resource('groups-chat', '\App\Http\Controllers\Admin\GroupChatController');
