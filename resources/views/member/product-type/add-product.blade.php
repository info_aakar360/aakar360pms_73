<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Add Product</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" name="name" id="title" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>@lang('app.action')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($products as $key=>$category)
                        <tr id="cat-{{ $category->id }}">
                            <td>{{ $key+1 }}</td>
                            <td>{{ ucwords($category->name) }}</td>
                            <td>
                                <?php if($category->added_by == Auth::user()->company_id){?>
                                <a href="javascript:;" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle editTaskCategory"
                                   data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="javascript:;" data-cat-id="{{ $category->id }}" class="btn btn-sm btn-danger btn-circle delete-category" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">No Product Found</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

        </div>

        {!! Form::close() !!}
    </div>
</div>

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="productCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}

<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('member.product-type-workforce.storeProduct')}}',
            container: '#createBoqCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('.editTaskCategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.product-type-workforce.editProduct',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#productCategoryModal', url);
        $('#taskCategoryModal').close();
    })


    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('member.product-type-workforce.storeProduct')}}',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>