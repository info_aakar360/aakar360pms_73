<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Product</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createProductCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" name="name" id="title" class="form-control" value="{{$product->name}}">
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" id="save-product" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
            </div>



        </div>

        {!! Form::close() !!}
    </div>
</div>

<script>


    $('.editTaskCategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.product-type-workforce.editProduct',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
    })


    $('#save-product').click(function () {
        var id = '{{ $product->id }}';
        var url = '{{ route('member.product-type-workforce.updateProduct',':id')}}';
        url = url.replace(':id', id);
        var formData = $('form#createProductCategory').serialize();
        $.easyAjax({
            url: url,
            container: '#createProductCategory',
            type: "POST",
            data: $('#createProductCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>