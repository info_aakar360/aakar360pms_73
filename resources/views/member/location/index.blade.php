@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">

@endpush

@section('content')


    <div class="row">
        <div class="col-xs-4 col-sm-4 col-lg-4">
            <select class="selectpicker" data-live-search="true"  style="float: right;width:226.08px" >
                <option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
                <option data-tokens="mustard">Burger, Shake and a Smile</option>
                <option data-tokens="frosting">Sugar, Spice and all things nice</option>
            </select>

        </div>
        <div class="col-xs-2 col-sm-2 col-lg-2">


        </div>
        <div class="col-xs-2 col-sm-2 col-lg-2">
            <button type="button" class="btn btn-primary btn-sm"   style="float: right;     width: 132.08px;" ><span class="glyphicon glyphicon-plus"></span> Export</button>

        </div>

        <div class="col-xs-2 col-sm-2 col-lg-2">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addsublocation"   style="float: right;     width: 132.08px;" ><span class="glyphicon glyphicon-plus"></span> Add Sub Location</button>

        </div>

        <div class="col-xs-2 col-sm-2 col-lg-2">

            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addlocation" style="float: right;    width: 132.08px;"><span class="glyphicon glyphicon-plus"></span> Add Location</button>
        </div>


    </div>
    <h2>Projects Location</h2>
    <div class="row">
        @foreach($location as $locations)
            <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-xs-9 col-sm-9 col-lg-9">
                    <h4 class="panel-title">
                        <b> <a>{{$locations->location_name}} ({{$locations->description}})</a></b>
                    </h4>
                        <h6>
                            <a>{{$locations->local_address}},{{get_city_name($locations->city_id)}},{{get_state_name($locations->id) }},{{get_country_name($locations->country_id)}}</a>

                        </h6>
                    </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <a data-toggle="collapse" href="#collapse{{$locations->id}}"></a>
                            <a type="button" class="btn btn-default" d data-toggle="tooltip" data-original-title="Total"> {{ get_count_sub_location($locations->id)  }}</a>

                            <a type="button" class="btn btn-default editLocation" data-cat-id="{{ $locations->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a type="button" class="btn btn-default deleteLocation" data-cat-id="{{ $locations->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-times-circle-o" aria-hidden="true"></i></a>



                        </div>
                    </div>
                </div>
                <div id="collapse{{$locations->id}}" class="panel-collapse collapse">
                    @foreach($sublocation as $sublocations)
                        @if($sublocations->location_id == $locations->id)
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-9 col-sm-9 col-lg-9">
                        <h5>{{$sublocations->name}}</h5>
                        <p>{{$sublocations->description}}</p>

                            </div>
                            <div class="col-xs-3 col-sm-3 col-lg-3">


                                <a type="button" class="btn btn-default editSubLocation" data-cat-id="{{ $sublocations->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>


                                <a type="button" class="btn btn-default deleteSubLocation" data-cat-id="{{ $sublocations->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-times-circle-o" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </div>
                        @endif

                    @endforeach

                </div>
            </div>
        </div>

        </div>
        @endforeach
    </div>


    <!-- Start Modal add location -->
    <div class="modal" id="addlocation">
        {!! Form::open(['id'=>'createLocation','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add  Location</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                        <div class="form-group">
                            <label for="inputName">Project Name</label>
                            <input type="text" class="form-control" id="inputName" name="buildingName" placeholder="Enter  Name"/>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Description	</label>
                            <input type="text" class="form-control" id="inputName" name="description" placeholder="Enter Description"/>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Local Address</label>
                            <input type="text" class="form-control" id="inputName" name="localAddress" placeholder="Enter Local Address"/>
                        </div>
                        <div class="form-group">
                            <label for="inputName">State</label>
                            <select class="form-control selectpicker" data-live-search="true" name="state_id"  onchange="getCity(this.value);"  >
                                <option data-tokens="ketchup mustard">Select State</option>
                                @foreach($state as $states)

                                <option  value="{{$states->id}}">{{$states->name}}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">City</label>
                            <select class="form-control selectpicker" data-live-search="true" name="city_id" id="city"  >
                                <option data-tokens="ketchup mustard">Select City</option>

                            </select>
                        </div>


                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal add location -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="addsublocation">
        {!! Form::open(['id'=>'createsubLocation','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add  Location</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                    <div class="form-group">
                        <label for="inputName">Sub Project Name</label>
                        <input type="text" class="form-control" id="inputName" name="subName" placeholder="Enter  Name"/>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Description	</label>

                        <input type="text" class="form-control" id="inputName" name="description" placeholder="Enter Description"/>
                    </div>

                    <div class="form-group">
                        <label for="inputName">Project Location</label>
                        <select class="form-control selectpicker" data-live-search="true" name="locationId">
                            <option data-tokens="ketchup mustard">Select Project</option>
                            @foreach($location as $locations)

                                <option  value="{{$locations->id}}">{{$locations->location_name}}</option>

                            @endforeach
                        </select>
                    </div>



                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal sub add location -->


    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color: red;">Warning</h4>
                </div>
                <div class="modal-body">
                    <p id="error" style="color: red;"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>

<script>

    $('#createLocation').submit(function () {
        $.easyAjax({
            url: '{{route('member.location.store-cat')}}',
            container: '#createLocation',
            type: "POST",
            data: $('#createLocation').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    });

    $('#createsubLocation').submit(function () {
        $.easyAjax({
            url: '{{route('member.location.store-sub-cat')}}',
            container: '#createsubLocation',
            type: "POST",
            data: $('#createsubLocation').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    });

    $('.editSubLocation').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.location.sub-edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Sub Location");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('.editLocation').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.location.location-edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Project Location");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });



    $('.deleteSubLocation').click(function(){



        var id = $(this).data('cat-id');
        var url = "{{ route('member.location.delete-sub-cat',':id',':state_id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'POST'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        });
    });

    $('.deleteLocation').click(function(){

        var sub_loc_count="{{get_count_sub_location($locations->id)}}";
        if( sub_loc_count!=0){


                    $("#error").html("Please Delete Sub Project Location First !");
                    $('#myModal').modal("show");


        }else {

            var id = $(this).data('cat-id');
            var url = "{{ route('member.location.delete-location',':id',':state_id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'POST'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        window.location.reload();
                    }
                }
            });
        }
    });

    function getCity($state_id) {
        //alert($state_id);
        var url = "{{ route('member.location.get-city',':id') }}";
        url = url.replace(':id', $state_id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token,'state_id':$state_id},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    var options = [];
                    var rData = [];
                    rData = response.city;
                    $.each(rData, function( index, value ) {
                      //  alert(value.id);
                        var selectData = '';
                        selectData = '<option value="'+index+'">'+value+'</option>';
                        options.push(selectData);
                    });

                    $('#city').html(options);
                    $('#city').selectpicker('refresh');
                }
            }
        });


    }



</script>


@endpush