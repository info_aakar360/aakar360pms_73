<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Boq Category</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqCategory','class'=>'ajax-form','method'=>'PUT']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $category->title }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Parent Category</label>
                        <select name="parent" id="parent" class="form-control">
                            <option value="">Please select parent category</option>
                            <?php  ?>
                            @foreach($categories as $cat)
                                <?php
                                $selected = '';
                                if($category->parent !== NULL){
                                if($cat->id == $category->parent){
                                    $selected = 'selected';
                                } }?>
                                <option value="{{ $cat->id }}" <?=$selected; ?>>{{ $cat->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('member.activity.update', [$category->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })



    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('member.activity.update', [$category->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>