@extends('layouts.public-quote')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

@endpush

@section('content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <div class="panel panel-inverse">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body text-center">
                        <h1><i class="fa fa-check-circle" style="color: green; font-size: 120px"></i></h1>
                        <h1>Success</h1>
                        <h4>Quotation submitted, we will get back to you soon.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')

@endpush

