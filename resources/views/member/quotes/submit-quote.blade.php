@extends('layouts.public-quote')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="@if($submitted) col-md-4 col-md-offset-4 @else col-md-12 @endif">

            <div class="panel panel-inverse">
                @if($submitted)
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body text-center">
                            <h1><i class="fa fa-exclamation-circle" style="color: orange; font-size: 120px"></i></h1>
                            <h2>Quotation already submitted.</h2>
                            <h5>You can not submit same quotation multiple times.</h5>
                        </div>
                    </div>
                @else
                <div class="panel-heading"> Quotation Information <span style="float: right">Supplier : {{$supplier->company_name}}</span></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <div class="form-body">

                            <h3 class="box-title">@lang('modules.rfq.productDetail')</h3>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Req. Date</th><th>Remark</th><th>Price</th></thead><tbody>';
                                    if(count($tmpData)){
                                        $i = 1;
                                        foreach($tmpData as $data){
                                            $html .= '<tr><td>'.$i.'</td><td>'.get_pcat_name($data->cid).'<input type="hidden" name="cid[]" value="'.$data->cid.'"/></td><td>'.get_pbrand_name($data->bid).'<input type="hidden" name="bid[]" value="'.$data->bid.'"/></td><td>'.$data->quantity.'</td><input type="hidden" name="quantity[]" value="'.$data->quantity.'"/><td>'.get_unit_name($data->unit).'<input type="hidden" name="unit[]" value="'.$data->unit.'"/></td><td>'.$data->expected_date.'<input type="hidden" name="dated[]" value="'.$data->expected_date.'"/></td><td>'.$data->remarks.'<input type="hidden" name="remarks[]" value="'.$data->remarks.'"/></td><td><input class="form-control" type="text" value="0" name="price[]" style="max-width: 150px;"/></td></tr>';
                                            $i++;
                                        }
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                 ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.rfq.remark')</b></label>
                                        <p>{{ $rfq->remark }}</p>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.rfq.paymentTerms')</b></label>
                                        <p>{{ $rfq->payment_terms }}</p>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('front.postQuotation', [$rfq->id, $supplier->id])}}',
                container: '#createRfq',
                type: "PATCH",
                redirect: true,
                data: $('#createRfq').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('member.rfq.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('member.rfq.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');


            $.ajax({
                url: '{{route('member.rfq.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });
    </script>
@endpush

