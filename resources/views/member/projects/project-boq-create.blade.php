@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('member.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('member.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('member.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('member.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <form method="post" id="productCostItem">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-3" style="float: left;">
                                        <select name="category[]" class="form-control" onchange="getChild(this.value,0)" id="category">
                                            <option value="">Please select category</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3" style="float: left;" id="child">

                                    </div>
                                    <div class="col-lg-6" style="float: left;" id="display">
                                        <select name="cost_item_lavel" class="form-control" id="cost_item_lavel" onchange="getProduct(this.value)">
                                            <option value="">Please select Task</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="white-box">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php
                                            $titlename = \App\Title::get();
                                            ?>
                                                <div class="col-lg-12" style="float: left;">
                                                    <select name="title" class="form-control" id="">
                                                        <option value="">Please select Title</option>
                                                        @foreach($titlename as $category)
                                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" id="productData">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(document).on('click', '#saveProduct', function(){
            $.easyAjax({
                url: '{{ route('member.projects.addCostItems') }}',
                container: '#productCostItem',
                type: "POST",
                data: $('#productCostItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });

        function getChild(val,id) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getChild') }}",
                data: {'_token': token, 'category_id': val, 'id':id},
                success: function(data){
                    if(data !== '') {
                        $("#child").append(data);
                        getLavel(val);
                    }else{
                        getLavel(val);
                    }
                }
            });
        }

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function getProduct(val) {
            var token = "{{ csrf_token() }}";
            var id = "{{ $id }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getProduct') }}",
                data: {'_token': token, 'cost_item_id': val, 'id': id},
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');

        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#rate'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#cost'+id).val(total);
        }

    </script>
@endpush