@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('member.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('member.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('member.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('member.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h4>Boq List
                                        <a href="javascript:;" class="btn btn-outline btn-success btn-sm createTitle" style="float: right">Create New Title<i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <a href="{{ route('member.projects.projectBoqCreate', [$id]) }}" class="btn btn-outline btn-success btn-sm createTaskCategory" style="float: right; margin-right: 10px;">Create New <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php $acc_id = 0; ?>
    @foreach(array_unique($projectproduct) as $propros)
        <?php
            $proproget = \App\ProjectCostItemsProduct::where('title',$propros)->groupBy('category')->get();
            $proprog = \App\ProjectCostItemsProduct::where('title',$propros)->first();

            $tv = DB::table('project_cost_item_final_qty')
            ->select(DB::raw('SUM(total_amount) as total_cost'))
            ->where('title', $proprog->title)
            ->where('project_id', $proprog->project_id)
            ->groupBy('title')
            ->first();


            $tq = DB::table('project_cost_items_product')
            ->select(DB::raw('count(*) as total_count'))
            ->where('title', $proprog->title)
            ->where('project_id', $proprog->project_id)
            ->groupBy('title')
            ->first();


        $tn = \App\Title::where('id',$proprog->title)->first();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="b-b p-b-10">{{ $tn->title }}</h3>
                    <h5 class="b-b p-b-10">Total Boq Value : <?php if(isset($tv) && $tv !== ''){ echo $tv->total_cost; } ?></h5>
                    <h5 class="b-b p-b-10">Total no. of items : <?php if(isset($tq)){ echo $tq->total_count; } ?></h5>
                    <div class="card">
                        <div class="card-header" id="heading">
                            <div class="col-sm-6">
                                Name
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-6">
                                    No. of Items
                                </div>
                                <div class="col-sm-6">
                                    Trade Value
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        //dd($proproget);
                    foreach ($proproget as $key=>$propro){
                    $proproc = \Illuminate\Support\Arr::pluck(\App\ProjectCostItemsProduct::where('category',$propro->category)->groupBy('category')->get(), 'category');
                    $catp = array_unique($proproc);
                    foreach ($catp as $catpa){
                        $catparent = explode(',',$catpa);

                    $sr = '1';
                    ?>
                    <div class="accordion" id="accordionExample{{ $acc_id }}">
                        <div class="card">
                            <div class="card-header" id="heading">
                                <div class="col-sm-6">
                                    <h2 class="mb-0 mt-0" style="margin: 0;">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $acc_id }}" aria-expanded="true" aria-controls="collapse">
                                            <?php if (isset($catparent)){ echo get_cat_name($catparent[0]); } ?>
                                        </button>
                                    </h2>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <span style="line-height: 36px"><?php if(isset($catparent)){ echo (count($catparent)-1); } ?></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <span style="line-height: 36px">Rs. <?php if(isset($tv) && $tv !== ''){ echo $tv->total_cost; } ?></span>
                                    </div>
                                </div>
                            </div>

                            <div id="collapse{{ $acc_id }}" class="collapse" aria-labelledby="heading" data-parent="#accordionExample{{ $acc_id }}">
                                @if(isset($catparent))
                                @for($i=1; $i<count($catparent);$i++)
                                    <div class="card-body">
                                        <div class="col-sm-6">
                                            <span class="mb-0 mt-0" style="margin-left: 30px;font-size: 14px;line-height: 14px;">
                                                {{ get_cat_name($catparent[$i]) }}
                                            </span>
                                        </div>
                                        <?php $catqcount = DB::table('project_cost_items_product')
                                            ->whereRaw("FIND_IN_SET($catparent[$i], category)")
                                            ->where('project_id', $propro->project_id)
                                            ->groupBy('title')
                                            ->get();
                                        //dd($propro->project_id.'/'.$catqcount);
                                        ?>
                                        <div class="col-sm-6">
                                            <div class="col-sm-6">
                                                <span class="mb-0 mt-0" style="font-size: 14px;line-height: 14px;">{{ count($catqcount) }}</span>
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="mb-0 mt-0" style="font-size: 14px;line-height: 14px;">&nbsp;</span>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                                    @endif
                            </div>
                        </div>
                    </div>
                    <?php $acc_id++; $sr++; } } ?>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <a href="{{ route('member.projects.editBoq', [$id, $propro->title]) }}" class="btn btn-outline btn-success btn-sm createTaskCategory" style="float: right;">Edit <i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
                                <a href="{{ route('member.projects.viewBoq', [$id, $propro->title]) }}" class="btn btn-outline btn-success btn-sm createTaskCategory" style="float: right; margin-right: 10px;">View <i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endforeach
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('member.projects.titleCreate', [$id]) }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <table class="table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($titles as $key=>$t)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $t->title }}</td>
                                    <td>{{ $key+1 }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12">
                            @csrf
                            <input type="text" class="form-control" name="title" placeholder="Title *" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');
    </script>
@endpush