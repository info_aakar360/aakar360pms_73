@extends('layouts.member-app')

@section('page-title')

@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<style>
    .rc-handle-container {
        position: relative;
    }
    .rc-handle {
        position: absolute;
        width: 7px;
        cursor: ew-resize;
        margin-left: -3px;
        z-index: 2;
    }
    table.rc-table-resizing {
        cursor: ew-resize;
    }
    table.rc-table-resizing thead,
    table.rc-table-resizing thead > th,
    table.rc-table-resizing thead > th > a {
        cursor: ew-resize;
    }
    .combo-sheet{
        overflow-x: scroll;
        overflow-y: auto;
        max-height: 75vh;
        border: 2px solid #bdbdbd;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
    }
    .combo-sheet .table-wrapper{
        min-width: 1350px;
    }
    .combo-sheet thead th{
        font-size: 12px;
        background-color: #C2DCE8;
        color: #3366CC;
        padding: 3px 5px !important;
        text-align: center;
        position: sticky;
        top: -1px;
        z-index: 1;

        &:first-of-type {
           left: 0;
           z-index: 3;
        }
    }
    .combo-sheet td{
        padding: 0 !important;
        font-size: 12px;
    }
    .combo-sheet td input.cell-inp{
        min-width: 100%;
        max-width: 100%;
        width: 100%;
        border: none !important;
        padding: 3px 5px;
        cursor: default;
        color: #000000;
        font-size: 1.2rem;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
    }
    .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border: 1px solid #bdbdbd;
    }
    .combo-sheet tr:hover td, .combo-sheet tr:hover input{
        background-color: #ffdd99;
    }
    .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
        background-color: #eaf204;
    }
    .RowCategory{
        width: 63px;
    }
    .RowType{
        width: 45px;
    }
    .RowCode{
        width: 145px;
    }
    .RowDescription{
        width: 255px;
    }
    .RowUnit{
        width: 90px;
    }
    .RowBaseRate{
        width: 90px;
    }
    .RowSurcharge{
        width: 117px;
    }
    .RowDiscount{
        width: 108px;
    }
    .RowFactor{
        width: 90px;
    }
    .RowFinalRate{
        width: 90px;
    }
    .RowRemark{
        width: 360px;
    }
    .white-box{
        padding: 5px !important;
    }
    .pdl10{
        padding-left: 10px !important;
    }
    .page-title{
        color: #0f49bd;
        font-weight: 900;
    }
    .page-title i{
        margin-right: 5px;
    }
</style>

@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row pdl10">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <div class="combo-sheet">
                    <div class="table-wrapper">
                        <table class="table table-bordered default footable-loaded footable" id="users-table" data-resizable-columns-id="users-table">
                            <thead>
                                <tr>
                                    <th class="RowCategory" data-resizable-column-id="category">@lang('modules.resources.category')</th>
                                    <th class="RowType" data-resizable-column-id="type">@lang('modules.resources.type')</th>
                                    <th class="RowCode" data-resizable-column-id="code">@lang('modules.resources.code')</th>
                                    <th class="RowDescription" data-resizable-column-id="description">@lang('modules.resources.description')</th>
                                    <th class="RowUnit" data-resizable-column-id="unit">@lang('modules.resources.unit')</th>
                                    <th class="RowBaseRate" data-resizable-column-id="base_rate">@lang('modules.resources.baseRate')</th>
                                    <th class="RowSurcharge" data-resizable-column-id="surcharge">@lang('modules.resources.surcharge')</th>
                                    <th class="RowDiscount" data-resizable-column-id="discount">@lang('modules.resources.discount')</th>
                                    <th class="RowFactor" data-resizable-column-id="factor">@lang('modules.resources.factor')</th>
                                    <th class="RowFinalRate" data-resizable-column-id="final_rate">@lang('modules.resources.finalRate')</th>
                                    <th class="RowRemark" data-resizable-column-id="remark">@lang('modules.resources.remark')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($i=0;$i<35;$i++)
                                <tr>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                    <td><input type="text" class="cell-inp"></td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('member.rfq.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this PO?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this PO?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('member.purchase-order.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                  swal("Updated!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            });

            function loadTable() {
                table = $('#users-table').resizableColumns({
                    responsive: true,
                    processing: true,
                    destroy: true,
                    stateSave: true,
                    language: {
                        "url": "<?php echo __("app.datatable") ?>"
                    },
                    "fnDrawCallback": function (oSettings) {
                        $("body").tooltip({
                            selector: '[data-toggle="tooltip"]'
                        });
                    }
                });
            }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var rfq = $('#rfq').val();
            var status = $('#status').val();

            var url = '{{ route('member.rfq.export', [':status', ':rfq']) }}';
            url = url.replace(':rfq', rfq);
            url = url.replace(':status', status);

            window.location.href = url;
        }
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });

    </script>
@endpush