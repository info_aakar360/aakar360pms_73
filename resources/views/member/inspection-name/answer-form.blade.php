@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

                <div class="row">
                    <div class="col-md-12">
                        <b>Name</b>
                        <br>
                        {{ $inspection->name }}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Type</b>
                        <br>
                        {{ get_ins_type_name($inspection->type) }}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Description</b>
                        <br>
                        {!! $inspection->description !!}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Files</b>
                        <br>
                        <div class="row" id="list">
                            <ul class="list-group" id="files-list">
                                @forelse($files as $file)
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-9">
                                                {{ $file->filename }}
                                            </div>
                                            <div class="col-md-3">
                                                @if($file->external_link != '')
                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 'local')
                                                    <a target="_blank" href="{{ asset_url('task-files/'.$file->inspection_id.'/'.$file->hashname) }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 's3')
                                                    <a target="_blank" href="{{ $url.'task-files/'.$file->inspection_id.'/'.$file->filename }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'google')
                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'dropbox')
                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @endif

                                                {{--@if(is_null($file->external_link))--}}
                                                {{--<a href="{{ route('member.task-files.download', $file->id) }}"--}}
                                                {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                {{--class="btn btn-inverse btn-circle"><i--}}
                                                {{--class="fa fa-download"></i></a>--}}
                                                {{--@endif--}}

                                                {{--<a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="{{ $file->id }}"--}}
                                                   {{--data-pk="list" class="btn btn-danger btn-circle sa-delete"><i class="fa fa-times"></i></a>--}}
                                                <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                    </li>
                                @empty
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-10">
                                                @lang('messages.noFileUploaded')
                                            </div>
                                        </div>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">&nbsp;</div>

                    <div class="col-md-12">&nbsp;</div>
                    <div class="row">
                        <form method="post" id="answerForm">
                            @foreach($questions as $question)
                                <div class="col-md-12" style="padding-bottom: 20px;">
                                    <div class="col-md-12" style="background-color: #c3c1c1; padding: 13px;">{{ $question->category }}</div>
                                    <div class="col-md-12">

                                        <table class="table-striped" style="width: 100%;">
                                            <?php
                                            $q = \App\InspectionQuestion::where('category',$question->category)->get();
                                            ?>
                                            @foreach($q as $p=>$r)
                                                <tr>
                                                    <td style="width: 10%;"><input type="hidden" name="inspection_id" value="{{ $inspection->id }}">{{ $p+1 }}</td>
                                                    <td style="width: 45%;"><input type="hidden" name="question_id[]" value="{{ $r->id }}">{{ $r->question }}</td>
                                                    <td style="width: 45%;">
                                                        <?php
                                                        if($r->input_field_id){
                                                            $field = \App\InputFields::where('id',$r->input_field_id)->first();
                                                            $html = '';

                                                            if($field->type == 'checkbox'){
                                                                $opt = json_decode($field->options);
                                                                foreach($opt as $o ) {
                                                                    $html .= '<input type="checkbox" name="answer[]" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                                                                }
                                                            }

                                                            if($field->type == 'select'){
                                                                $opt = json_decode($field->options);
                                                                $html = '<select name="answer[]" class="form-control">';
                                                                foreach($opt as $o ) {
                                                                    $html .= '<option value="'.$o.'"> '.$o.'</option>';
                                                                }
                                                                $html .='</select>';
                                                            }

                                                            if($field->type == 'select'){
                                                                $opt = json_decode($field->options);
                                                                $html = '<select name="answer[]" class="form-control">';
                                                                foreach($opt as $o ) {
                                                                    $html .= '<option value="'.$o.'"> '.$o.'</option>';
                                                                }
                                                                $html .='</select>';
                                                            }

                                                            if($field->type == 'button'){
                                                                $html .= '<input type="button" name="answer[]" value="'.$field->title.'" class="form-control">';
                                                            }

                                                            if($field->type == 'date'){
                                                                $html .= '<input type="date" name="answer[]" placeholder="'.$field->title.'" class="form-control">';
                                                            }

                                                            if($field->type == 'email'){
                                                                $html .= '<input type="email" name="answer[]" placeholder="'.$field->title.'" class="form-control">';
                                                            }

                                                            if($field->type == 'number'){
                                                                $html .= '<input type="number" name="answer[]" placeholder="'.$field->title.'" class="form-control">';
                                                            }

                                                            if($field->type == 'radio'){
                                                                $opt = json_decode($field->options);
                                                                foreach($opt as $o ) {
                                                                    $html .= '<input type="radio" name="answer[]" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                                                                }
                                                            }

                                                            if($field->type == 'text'){
                                                                $html .= '<input type="text" name="answer[]" placeholder="'.$field->title.'" class="form-control">';
                                                            }
                                                            echo $html;
                                                        }
                                                        ?>
                                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                            <label class="btn btn-primary">
                                                                <input type="checkbox" autocomplete="off" name="answer[]" value="N/A"> N/A
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>

                                    </div>
                                </div>
                            @endforeach
                            @csrf
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success" onclick="submitAnswer()" value="">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- .row -->


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>

    <script>
        $('.createTaskCategory').click(function(){
            var url = '{{ route('member.inspectionName.createName')}}';
            $('#modelHeading').html("Create New");
            $.ajaxModal('#taskCategoryModal', url);
        })

        $('.editTaskCategory').click(function(){
            var id = $(this).data('cat-id');
            var url = '{{ route('member.inspectionName.editName',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Category");
            $.ajaxModal('#taskCategoryModal', url);
        })

        $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );

        $('ul.showProjectTabs .projectTasks').addClass('tab-current');

        $('.delete-category').click(function () {
            var id = $(this).data('cat-id');
            var url = "{{ route('member.inspectionName.destroyName',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                    }
                }
            });
        });

        function showData(val,row) {
            var url = "{{ route('member.inspectionName.getData') }}";
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, 'id':val, 'row_id':row},
                success: function (data) {
                    $('#ansData'+row).html(data.html);
                }
            });
        }

        function submitAnswer() {
            $.easyAjax({
                url: '{{route('member.inspectionName.storeAnswer')}}',
                container: '#answerForm',
                type: "POST",
                data: $('#answerForm').serialize(),
                success: function (data) {

                    var msgs = "@lang('Answer updated successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.reload();

                }
            })
            return false;
        }
    </script>
@endpush