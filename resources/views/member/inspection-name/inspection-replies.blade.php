@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="javascript:;" class="btn btn-outline btn-success btn-sm createTaskCategory">Assign User <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Assigned To</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($answers as $key=>$ans)
                            <?php $names = \App\InspectionName::where('id',$ans->inspection_id)->get(); ?>
                            @forelse($names as $category)
                                <tr id="cat-{{ $category->id }}">
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ ucwords($category->name) }}</td>
                                    <td>{{ get_ins_type_name($category->type) }}</td>
                                    <td>{!!  $category->description !!}</td>
                                    <td>
                                        <?php
                                        $users = \App\User::where('id',$ans->user_id)->first();
                                        ?>
                                        @if($users->image)
                                            <img data-toggle="tooltip" data-original-title="{{ ucwords($users->name) }}" src="{{ asset('user-uploads/avatar/' . $users->image) }}"
                                                 alt="user" class="img-circle" width="30">
                                        @else
                                            <img data-toggle="tooltip" data-original-title="{{ ucwords($users->name) }}" src="{{ asset('default-profile-2.png') }}"
                                                 alt="user" class="img-circle" width="30">
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('member.inspectionName.inspectionRepliesView',[$category->id, $ans->user_id])}}" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle"
                                           data-toggle="tooltip" data-original-title="View Reply"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        &nbsp;

                                        <?php
                                        if($category->assign_to){
                                        $as = explode(',',$category->assign_to);
                                        if (in_array(\Illuminate\Support\Facades\Auth::user()->id, $as)){ ?>
                                        <a href="{{ route('member.inspectionName.inspectionAnswerForm',[$category->id])}}" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle"
                                           data-toggle="tooltip" data-original-title="Submit Answer"><i class="fa fa-check" aria-hidden="true"></i></a>
                                        &nbsp;
                                        <?php } }?>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">No Replies Found</td>
                                </tr>
                            @endforelse
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" id="answerForm">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Inspection
                                    </label>
                                    <select class="selectpicker form-control" name="id" data-style="form-control" required>
                                        <option value="">Please Select Inspection Name</option>
                                        @foreach($namess as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Assign User
                                    </label>
                                    <select class="selectpicker form-control" name="assign_to[]" data-style="form-control" multiple required>
                                        <option value="">Please Select User</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @csrf
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success" onclick="submitAnswer()" value="">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>

<script>
    $('.createTaskCategory').click(function(){
        {{--        var url = '{{ route('member.inspectionName.createName')}}';--}}
        $('#modelHeading').html("Create New");
        $('#taskCategoryModal').show();
//        $.ajaxModal('#taskCategoryModal', url);
    })

    $('.closeButton').click(function(){
        $('#taskCategoryModal').hide();
    })

    $('.editTaskCategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.inspectionName.editName',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $(document).ready(function() {
        $('#example').DataTable( {
            deferRender:    true,
            scrollCollapse: true,
            scroller:       true
        } );
    } );

    $('ul.showProjectTabs .projectTasks').addClass('tab-current');

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('member.inspectionName.destroyName',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    $.each(rData, function( index, value ) {
                        var selectData = '';
                        selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                        options.push(selectData);
                    });

                    $('#category_id').html(options);
                    $('#category_id').selectpicker('refresh');
                }
            }
        });
    });

    function showData(val,row) {
        var url = "{{ route('member.inspectionName.getData') }}";
        var token = "{{ csrf_token() }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, 'id':val, 'row_id':row},
            success: function (data) {
                $('#ansData'+row).html(data.html);
            }
        });
    }

    function submitAnswer() {
        $.easyAjax({
            url: '{{route('member.inspectionName.assignUserStore')}}',
            container: '#answerForm',
            type: "POST",
            data: $('#answerForm').serialize(),
            success: function (data) {

                var msgs = "@lang('Answer updated successfully')";
                $.showToastr(msgs, 'success');
                window.location.reload();

            }
        })
        return false;
    }
</script>
@endpush