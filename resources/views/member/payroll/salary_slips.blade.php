@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .ps-body {
            background: #ffffff;
        }

        .img-fluid {
            max-width: 100%;
            height: auto;
        }

        .ps-table-common {
            width: 100%;
            height: 100%;
            table-layout: fixed;
            border-collapse: collapse;
        }

        .ps-table-common td,
        .ps-table-common th {
            height: 100%;
        }

        .ps-table-common.br {
            border: 1px solid #000000;
        }

        .ps-table-common tr.header {
            font-weight: 600;
            border-bottom: 1px solid #000000;
        }

        .ps-table-common tr.footer {
            font-weight: 600;
            border-top: 1px solid #000000;
        }

        .ps-table-common.br > tbody > tr > td:first-child {
            border-right: 1px solid #000000;
        }

        .half-75 {
            width: 75%;
            padding: 5px 0 5px 10px;
            display: inline-block;
            text-transform: capitalize;
        }
        .half-25 {
            width: 25%;
            padding: 5px 0 5px 10px;
            display: inline-block;
            text-transform: capitalize;
        }

        .half {
            width: 50%;
            padding: 5px 0 5px 10px;
            display: inline-block;
            text-transform: capitalize;
        }

        .half-2 {
            width: 25%;
            padding: 5px 0;
            display: inline-block;
            text-transform: capitalize;
        }

        .half:last-child,
        .half-2:last-child,
        .half-25:last-child,
        .half-75:last-child {
            padding: 5px 10px 5px 0;
        }

        .semibold {
            font-weight: 600;
        }

        .ps-wrapper {
            padding: 20px;
            font-size: 14px;
            position: relative;
            background: #ffffff;
            padding-bottom: 100px;
            border: 10px solid #ddd;
            font-family: "Open Sans";
        }

        .ps-company-logo {
            max-height: 100px;
        }

        .ps-company-title-wrapper {
            width: 100%;
        }

        .ps-main-title {
            font-size: 30px;
            font-weight: 600;
            line-height: 1;
            margin: 10px 0;
        }

        .ps-main-address {
            font-size: 14px;
        }

        .ps-time-period {
            font-weight: 400;
            text-align: center;
            font-size: 20px;
            margin: 30px 0 20px;
        }

        .ps-ul {
            margin: 0;
            height: 100%;
            position: relative;
            list-style-type: none;
            border: 1px solid #000;
            text-transform: capitalize;
        }

        .ps-ul.ps-ul-amount {
            padding-bottom: 35px;
        }

        .ps-ul > li {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .ps-ul.ps-ul-amount > li.header {
            margin-top: 0;
            font-weight: 700;
            margin-bottom: 0;
            padding-top: 5px;
            padding-bottom: 5px;
            border-bottom: 1px solid #000;
        }

        .ps-ul.ps-ul-amount > li.footer {
            bottom: 0;
            width: 100%;
            margin-top: 0;
            font-weight: 700;
            margin-bottom: 0;
            padding-top: 5px;
            position: absolute;
            padding-bottom: 5px;
            border-top: 1px solid #000;
        }

        .ps-ul.ps-ul-amount > li > div:last-child {
            text-align: right;
        }

        .ps-wrapper-footer {
            left: 0;
            bottom: 0;
            width: 100%;
            position: absolute;
            padding-left: 20px;
            padding-right: 20px;
            padding-bottom: 10px;
        }

        .amount-text {
            font-style: italic;
            text-transform: capitalize;
        }

        hr.ps-hr {
            margin: 10px 0;
            border-color: #000000;
        }

        .eq-row {
            margin: 0;
            width: 100%;
            display: table;
        }

        .eq-row > [class*="col-"] {
            float: none;
            padding: 0;
            display: table-cell;
            vertical-align: top;
        }

        .abs {
            position: absolute;
            width: 100%;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .mt {
            margin-top: 2.5rem;
        }

        .m-0 {
            margin: 0;
        }

        .br-0 {
            border-right: none;
        }

        .float-right {
            float: right;
        }

        .bt {
            border-top: 1px solid #000000;
        }

        #payslipTitle {
            font-size: 18px;
            font-weight: 600;
        }

        .payslip-container p.err,
        .payslip-mobile-container p.err {
            margin: 10px 0 0 0;
            padding: 15px;
            text-align: center;
        }

        .payslip-mobile-container p.err {
            margin: 0;
        }

        .alt-loader {
            margin: 10px 0 0 0;
            height: 100px;
            display: flex;
            align-items: center;
        }

        .alt-loader > .loader {
            position: relative;
        }

        .payslip-mobile-container .alt-loader {
            margin: 0;
        }

        @media screen and (max-width: 767px) {
            .main-wrapper {
                margin-top: 215px;
            }
            .salary-slip-navigation-buttons {
                float: none;
                text-align: center;
                margin-top: 20px;
            }
        }

        @media screen and (max-width: 768px) {
            .payslip-wrapper {
                padding: 5px;
            }
        }

        .default-rule-btn-wrapper {
            float: right;
            display: flex;
        }
        .default-rule-btn {
            cursor: pointer;
            min-width: 185px;
            text-transform: none !important;
        }

        .default-rule-btn-wrapper.default .not-default,
        .default-rule-btn-wrapper .is-default {
            display: none;
        }

        .default-rule-btn-wrapper.default .is-default,
        .default-rule-btn-wrapper .not-default {
            display: block;
        }

        .default-rule-btn-wrapper.default .default-text {
            display: flex;
        }

        .default-rule-btn.default {
            min-width: auto;
            font-size: 14px;
            box-shadow: none;
            padding-right: 15px;
            color: #ea4033 !important;
            background-color: #ffe5e3 !important;
        }

        .default-rule-btn.default .is-default span {
            position: relative;
            top: 1px;
        }
        .default-text {
            color: #21c70f;
            display: flex;
            font-size: 14px;
            font-weight: 600;
            margin-right: 20px;
            align-items: center;
        }

        .default-text .material-icons {
            margin-right: 5px;
        }
        #salarySlipDefaultBtn.default{
            background-color: #2818c7 !important;
            color: #fff !important;
        }
        #salarySlipDefaultBtn.default .material-icons{
            color: #fff !important;
        }


    </style>
@endpush

@section('content')

    <div class="attandance-section">
        <div class="row">

            <div class="col-md-9">
                <!-- payslip components -->
                <div class="section-details p-3 mt-0">
                    <div id="payslipContainer">
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <meta http-equiv="x-ua-compatible" content="ie=edge; charset=utf-8">
                        <title>Salary Slip | Format 1</title>
                        <link type="image/png" href="/static/images/favicon-16x16.png" rel="shortcut icon">

                        <!-- Font family -->
                        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
                        <style>
                            .ps-wrapper {
                                margin-top: 10px;
                            }
                        </style>

                        @foreach($attencedata as $employee)

                        <div class="mbl-responsive-table">
                            <div class="ps-wrapper container-fluid">
                                <table class="ps-table-common">
                                    <tbody>
                                    <tr>
                                        <td width="20%" class="for_mobile-res-td-wd-mtop-20 min-wd-320">
                                            <div class="ps-logo-wrapper">
                                                <p><img src="{{ $global->logo() }}" class="admin-logo"  alt="logo" width="150px" /></p>
                                            </div>
                                        </td>
                                        <td width="60%" class="for_mobile-res-td-width min-wd-320">
                                            <div class="col-xs-12">
                                                <div class="ps-company-title-wrapper text-center">
                                                    <h2 class="ps-main-title">Abc</h2>
                                                    <p class="m-0 ps-main-address">

                                                        [ {{$employee->address}} ]

                                                    </p>
                                                    <p class="m-0 ps-main-address">

                                                    </p><div>Registered office address of Aakar360 Mentors Private Limited is D-06, Sector 2, Agroha Society Ring Road No. 01, Raipura Raipur Raipur-492013 Chhattisgarh</div>

                                                    <p></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="20%"></td>
                                    </tr>
                                    </tbody>
                                </table>

                                <h5 class="text-center ps-time-period">Payslip for the Month of {{$month}}, {{$year}}</h5>
                                <table class="ps-table-common br displayBlock">
                                    <tbody>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Name:</div><div class="half">{{$employee->name}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Employee ID:</div><div class="half">{{$employee->id}}</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Designation:</div><div class="half whitespace-normal">{{$employee->designame}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Bank Name:</div><div class="half whitespace-normal">  </div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Department:</div><div class="half">{{$employee->team_name}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Bank Account No:</div><div class="half">  </div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Location:</div><div class="half">{{$employee->address}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">PAN No.:</div><div class="half">ABCDE1234F</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Effective Work Days:</div><div class="half"><?php $id = $employee->id; $name=$employee->name ; $final = $employeeAttendence;$view = view('admin.attendance.working_days', compact('id','name','final'))->render(); ?>{{$view}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">PF No.:</div><div class="half wordBreak">{{$pfesidata->pf_number}}</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mobile-res-wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">LOP:</div><div class="half">{{$lop->lop}}</div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table class="ps-table-common br">
                                    <tbody>
                                    <tr class="header">
                                        <td width="50%" class="min-wd min-420">
                                            <div class="d-flex h-100"><div class="half mobile-res-65">Earnings</div><div class="half mobile-res-35  text-right">22,000</div></div>
                                        </td>
                                        <td width="50%" class="min-wd min-420">
                                            <div class="d-flex h-100"><div class="half mobile-res-65">Deductions</div><div class="half mobile-res-35  text-right">10,000</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">BASIC</div><div class="half text-right">{{$salary->basic}}</div></div>
                                        </td>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">PF</div><div class="half text-right">5,200</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">HRA</div><div class="half text-right">{{$salary->hra}}</div></div>
                                        </td>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">INCOME TAX</div><div class="half text-right">{{$taxdetail->incometax}}</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">@foreach($allowance as $key=>$value){{$value}} @endforeach</div><div class="half text-right">@foreach($amount as $key=>$value){{$value}} @endforeach</div></div>
                                        </td>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">PROF TAX</div><div class="half text-right">200</div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half">SPECIAL ALLOWANCE</div><div class="half text-right">45,617</div></div>
                                        </td>
                                        <td width="50%">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="ps-table-common br">
                                    <tbody>
                                    <tr class="header">
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half mobile-res-65">Total Earnings (Rs)</div><div class="half mobile-res-35 text-right">1,07,083</div></div>
                                        </td>
                                        <td width="50%">
                                            <div class="d-flex h-100"><div class="half mobile-res-65">Total Deductions (Rs)</div><div class="half mobile-res-35 text-right">21,981</div></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="ps-table-common">
                                    <tbody>
                                    <tr>
                                        <td class="mobile-res-wd mtop-10 mbott-10 for_mobile_large_wd" width="50%">
                                            <div class="d-flex h-100"><div class="half">Net Pay for the Month:</div><div class="half text-right"><span class="semibold" id="netpay">85,102</span></div></div>
                                        </td>
                                        <td class="mobile-res-wd" width="50%"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div>
                                    <p class="amount-text m-0 text-capitalize" id="netpayinword">(Rupees Eighty Five Thousand and One Hundred and Two Only)</p>
                                    <hr class="ps-hr">
                                    <p class="text-center m-0">This is a system generated payslip and does not require signature.</p>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- end payslip components -->
            </div>
        </div>
    </div>

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $val = convertNumberToWords($("#netpay").text());
            $("#netpayinword").text('( Repees  ' + $val + ')');
        });
    </script>
    <script>

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.store')}}',
                container: '#createPaySettings',
                type: "POST",
                redirect: true,
                data: $('#createPaySettings').serialize()
            })
        });
        function convertNumberToWords(amount) {
            var words = new Array();
            words[0] = '';
            words[1] = 'One';
            words[2] = 'Two';
            words[3] = 'Three';
            words[4] = 'Four';
            words[5] = 'Five';
            words[6] = 'Six';
            words[7] = 'Seven';
            words[8] = 'Eight';
            words[9] = 'Nine';
            words[10] = 'Ten';
            words[11] = 'Eleven';
            words[12] = 'Twelve';
            words[13] = 'Thirteen';
            words[14] = 'Fourteen';
            words[15] = 'Fifteen';
            words[16] = 'Sixteen';
            words[17] = 'Seventeen';
            words[18] = 'Eighteen';
            words[19] = 'Nineteen';
            words[20] = 'Twenty';
            words[30] = 'Thirty';
            words[40] = 'Forty';
            words[50] = 'Fifty';
            words[60] = 'Sixty';
            words[70] = 'Seventy';
            words[80] = 'Eighty';
            words[90] = 'Ninety';
            amount = amount.toString();
            var atemp = amount.split(".");
            var number = atemp[0].split(",").join("");
            var n_length = number.length;
            var words_string = "";
            if (n_length <= 9) {
                var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
                var received_n_array = new Array();
                for (var i = 0; i < n_length; i++) {
                    received_n_array[i] = number.substr(i, 1);
                }
                for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                    n_array[i] = received_n_array[j];
                }
                for (var i = 0, j = 1; i < 9; i++, j++) {
                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                        if (n_array[i] == 1) {
                            n_array[j] = 10 + parseInt(n_array[j]);
                            n_array[i] = 0;
                        }
                    }
                }
                value = "";
                for (var i = 0; i < 9; i++) {
                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                        value = n_array[i] * 10;
                    } else {
                        value = n_array[i];
                    }
                    if (value != 0) {
                        words_string += words[value] + " ";
                    }
                    if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Crores ";
                    }
                    if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Lakhs ";
                    }
                    if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                        words_string += "Thousand ";
                    }
                    if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                        words_string += "Hundred and ";
                    } else if (i == 6 && value != 0) {
                        words_string += "Hundred ";
                    }
                }
                words_string = words_string.split("  ").join(" ");
            }
            return words_string;
        }
    </script>
@endpush