@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">Invoice No</label>
                        <p><b>{{ $inv->invoice_no }}</b></p>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Store</label>
                        <p><b>{{ get_store_name($inv->store_id) }}</b></p>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Supplier</label>
                        <p><b>{{ get_supplier_name($inv->supplier_id) }}</b></p>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Invoice Date</label>
                        <p><b>{{ $inv->dated }}</b></p>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Amount</label>
                        <p><b>{{ $inv->gt }}</b></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">Amount</label>
                        <p><input type="text" class="form-control" name="amount" placeholder="Enter Amount" required/></p>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Date</label>
                        <p><input type="date" class="form-control" name="dated" placeholder="Enter Date" required/></p>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Status</label>
                        <p><select class="form-control" name="status" required><option value="1">Partially Paid</option><option value="2">Fully Paid</option></select> </p>
                    </div>

                </div>
                <div class="form-actions text-right">
                    <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="col-md-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                            <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.inventory.amount')</th>
                                    <th>@lang('modules.inventory.dated')</th>
                                    <th>@lang('modules.inventory.status')</th>
                                    <th>@lang('modules.inventory.created_date')</th>
                                    <th>@lang('app.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($tmpData))
                                @foreach($tmpData as $tmp)
                                    <tr>
                                        <td>{{ $tmp->id }}</td>
                                        <td>{{ $tmp->amount }}</td>
                                        <td>{{ $tmp->dated }}</td>
                                        <td>@if($tmp->status == 1) Partially Paid @elseif($tmp->status == 2) Fully Paid @endif</td>
                                        <td>{{ $tmp->created_at }}</td>
                                        <td><a href="javascript:;" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-user-id="{{$tmp->id}}" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;</td>
                                    </tr>
                                @endforeach
                            @else
                                    <tr><td colspan="6">No Transactions Found.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('member.payment.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    location.reload();
                                }
                            }
                        });
                    }
                });
            });

            });

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('member.purchase.submitPayment', $inv->id)}}',
                container: '#createRfq',
                type: "POST",
                redirect: true,
                data: $('#createRfq').serialize()
            })
        });
        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var rfq = $('#rfq').val();
            var status = $('#status').val();

            var url = '{{ route('member.rfq.export', [':status', ':rfq']) }}';
            url = url.replace(':rfq', rfq);
            url = url.replace(':status', status);

            window.location.href = url;
        }
        $(document).on('keyup', '.ret_qty', function(){
            var inp = $(this);
            var key = inp.data('key');
            var qty = inp.val();
            var act_qty = inp.attr('max');
            var bal = parseFloat(act_qty)-parseFloat(qty);
            //alert(bal);
            if(parseInt(bal) >= 0){
                //$('#save-form').removeAttr('disabled');
            }else{
                alert('Invalid quantity.');
                inp.val(act_qty);
                //$('#save-form').attr('disabled', true);
            }

        });
    </script>
@endpush