@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.punch-items.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.punch_items.updateTask')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'PUT']) !!}

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.title') </label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" value="{{ $punchitem->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="select2 form-control" name="user_id[]" id="user_id" >
                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                            <?php $as = explode(',',$punchitem->assign_to); ?>
                                            @foreach($employees as $employee)
                                                <?php
                                                //                                                $selected = '';
                                                //                                                if($employee->id == $punchitem->assign_to){
                                                //                                                    $selected = 'selected';
                                                //                                                }
                                                ?>
                                                <option value="{{ $employee->id }}"
                                                <?php

                                                    if (in_array($employee->id, $as)){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <?php if($punchitem->status == 'Open'){ ?>
                                            <option value="Open" selected>Open</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Inprogress">Inprogress</option>
                                            <?php } elseif($punchitem->status == 'Resolved'){?>
                                            <option value="Open">Open</option>
                                            <option value="Resolved" selected>Resolved</option>
                                            <option value="Inprogress">Inprogress</option>
                                            <?php } else { ?>
                                            <option value="Open">Open</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Inprogress" selected>Inprogress</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type</label>
                                        <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                            <option value="">Please Select type</option>
                                            <?php if($punchitem->type == 'Architect'){ ?>
                                            <option value="Architect" selected>Architect</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Owner">Owner</option>
                                            <?php } if($punchitem->type == 'Contractor'){ ?>
                                            <option value="Architect">Architect</option>
                                            <option value="Contractor" selected>Contractor</option>
                                            <option value="Owner">Owner</option>
                                            <?php } if($punchitem->type == 'Owner'){ ?>
                                            <option value="Architect">Architect</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Owner" selected>Owner</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                <?php
                                                    $ass = explode(',',$punchitem->distribution);
                                                    if (in_array($employee->id, $ass)){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.description')</label>
                                        <textarea id="description" name="description" class="form-control summernote">{{ $punchitem->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" id="location" name="location" class="form-control" value="{{ $punchitem->location }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.startDate')</label>
                                        <?php
                                        $ex = explode('-',$punchitem->start_date);
                                        ?>
                                        <input type="text" name="start_date"  id="start_date2" class="form-control" value="{{ $ex[2].'-'.$ex[1].'-'.$ex[0] }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <?php
                                        $exd = explode('-',$punchitem->due_date);
                                        ?>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" value="{{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="privete">Private or Public</label><br>
                                        <input id="privete" name="private" value="1" type="checkbox" <?php if($punchitem->private == '1'){ echo 'checked';}?>>
                                        <label for="privete">Private</label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Priority</label>
                                            <select class="selectpicker form-control" name="priority" data-style="form-control" required>
                                                <option value="">Please Select Priority</option>
                                                <?php if($punchitem->priority == 'High'){ ?>
                                                <option value="High" selected>High</option>
                                                <option value="Medium">Medium</option>
                                                <option value="Low">Low</option>
                                                <?php } if($punchitem->priority == 'Medium'){ ?>
                                                <option value="High">High</option>
                                                <option value="Medium" selected>Medium</option>
                                                <option value="Low">Low</option>
                                                <?php } if($punchitem->priority == 'Low'){ ?>
                                                <option value="High">High</option>
                                                <option value="Medium">Medium</option>
                                                <option value="Low" selected>Low</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label class="control-label">Reference
                                            </label>
                                            <input type="text" name="reference" class="form-control" value="{{ $punchitem->reference }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    @foreach($files as $file)
                                        <div class="col-md-2" style="text-align: center;">
                                            <img src="{{ url('public/user-uploads/task-files/'.$file->task_id.'/'.$file->hashname) }}">
                                            <br>
                                            <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                                Remove
                                            </a>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="col-md-12">&nbsp;</div>
                                <!--/span-->
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url"type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="taskID" id="taskID">
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('member.punch-items.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = '{{ $punchitem->id }}';
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.taskUpdatedSuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('member.punch-items.index') }}'

        });

        //    update task
        $('#update-task').click(function () {

            $.easyAjax({
                url: '{{route('member.punch-items.update', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.Punch item updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('member.punch-items.index') }}'
                    }
                }
            })

        });

        //    update task
        function removeFile(id) {
            var url = "{{ route('member.punch-items.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        window.location.reload();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('member.punch-items.update', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.taskCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('member.punch-items.index') }}'
                    }
                }
            })
        }

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('member.punch-items.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#list ul.list-group').html(response.html);

                            }
                        }
                    });
                }
            });
        });

        $('#project_id').change(function () {
            var id = $(this).val();

            // For getting dependent task
            var dependentTaskUrl = '{{route('member.all-tasks.dependent-tasks', [':id', ':taskId'])}}';
            dependentTaskUrl = dependentTaskUrl.replace(':id', id);
            dependentTaskUrl = dependentTaskUrl.replace(':taskId', '{{ $punchitem->id }}');
            $.easyAjax({
                url: dependentTaskUrl,
                type: "GET",
                success: function (data) {
                    $('#dependent_task_id').html(data.html);
                }
            })
        });

    </script>
    <script>
        $('#createTaskCategory').click(function(){
            var url = '{{ route('member.taskCategory.create-cat')}}';
            $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
            $.ajaxModal('#taskCategoryModal', url);
        })

    </script>

@endpush
