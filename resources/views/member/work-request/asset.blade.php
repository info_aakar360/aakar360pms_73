@extends('layouts.member-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createLocation" class="btn btn-outline btn-success btn-sm">Add Asset <i class="fa fa-plus" aria-hidden="true"></i></a>

        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Area</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($assets as $key=>$category) {

                            if(count($assets)!=0) {
                            ?>
                               <tr id="cat-{{ $category->id }}">
                                <td>{{ $key+1 }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->area }}</td>
                        <td>{{get_asset_category($category->category)  }}</td>
                                   <td>
                                       <a type="button" class="btn btn-default editAsset" data-cat-id="{{ $category->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                       <a type="button" class="btn btn-default deleteAsset" data-cat-id="{{ $category->id   }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-times-circle-o" aria-hidden="true"></i></a>

                                   </td>
                        </tr>
                        <?php
                           }else{
                               ?>
                           <tr>
                               <td colspan="6">No Asset Found</td>
                           </tr>

                            <?php
                                } }
                               ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="createLocation">
        {!! Form::open(['id'=>'createAddAsset','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Asset</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Enter  Name"/>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Description</label>
                        <input type="text" class="form-control" id="inputName" name="description" placeholder="Enter  Name"/>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Area</label>
                        <input type="text" class="form-control" id="inputName" name="area" placeholder="Enter  Name"/>

                    </div>

                    <div class="form-group">
                        <label for="inputName">Category</label>
                        <select class="form-control selectpicker" data-live-search="true" name="category">
                            <option data-tokens="ketchup mustard">Select Project</option>
                            @foreach($assetCategory as $assetCategories)

                                <option  value="{{$assetCategories->id}}">{{$assetCategories->name}}</option>

                            @endforeach
                        </select>
                    </div>

                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary createAsset" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    $('#createAddAsset').submit(function () {
        $.easyAjax({
            url: '{{route('member.asset-category.create-asset')}}',
            container: '#createLocation',
            type: "POST",
            data: $('#createAddAsset').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    });

    $('.editAcategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.asset-category.edit-asset-cat',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });


    $('.editAsset').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('member.asset-category.edit-asset',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Asset");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });



    $('.deleteAsset').click(function(){

        var id = $(this).data('cat-id');
        var url = "{{ route('member.asset-category.delete-asset',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'POST'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        });
    });

</script>
@endpush