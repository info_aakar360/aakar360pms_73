@extends('layouts.public-quote')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.purchase-order.index') }}">@lang('app.menu.po')</a></li>
                <li>{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="@if($submitted) col-md-4 col-md-offset-4 @else col-md-12 @endif">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Purchase Order Info @if($supplier)<span style="float: right">Supplier : {{$supplier->company_name}}</span>@endif</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="box-title">Buyer Details</h3>
                                    <div class="row">
                                        <div class="col-md-4">Company Name :</div>
                                        <div class="col-md-8"><b>{{ $store->company_name }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Contact Person :</div>
                                        <div class="col-md-8"><b>{{ $store->contact_person }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Email :</div>
                                        <div class="col-md-8"><b>{{ $store->email }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Contact No. :</div>
                                        <div class="col-md-8"><b>{{ $store->phone }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Address :</div>
                                        <div class="col-md-8"><b>{{ $store->address }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">GST No. :</div>
                                        <div class="col-md-8"><b>{{ $store->gst_no }}</b></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="box-title">Purchase Order Details</h3>
                                    <div class="row">
                                        <div class="col-md-4">PO Number :</div>
                                        <div class="col-md-8"><b>{{ $po->po_number }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Dated :</div>
                                        <div class="col-md-8"><b>{{ \Carbon\Carbon::parse($po->dated)->format('d M, Y') }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Remark :</div>
                                        <div class="col-md-8"><b>{{ $po->remark }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Payment Terms :</div>
                                        <div class="col-md-8"><b>{{ $po->payment_terms }}</b></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h3 class="box-title">@lang('modules.rfq.productDetail')</h3>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th></thead><tbody>';
                                    if(count($tmpData)){
                                        $i = 1;
                                        foreach($tmpData as $data){
                                            $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->quantity.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->price.'</td><td>'.$data->tax.'</td></tr>';
                                            $i++;
                                        }
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                 ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('member.rfq.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('member.rfq.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');


            $.ajax({
                url: '{{route('member.rfq.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });
    </script>
@endpush

