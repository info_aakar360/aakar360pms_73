<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Add Module</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'addModule','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" id="title" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Display Name</label>
                        <input type="text" name="displayname" id="title" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" id="title" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div id="addMoreBox1" class="clearfix">
                        <div class="col-md-3">
                            <div id="dateBox" class="form-group ">
                                <input class="form-control" autocomplete="off" id="dateField1" name="name[0]" type="text" value="" placeholder="Name"/>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left: 5px;">
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="display_name[0]" placeholder="Display Name"/>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left: 5px;">
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="permission_description[0]" placeholder="Description"/>
                            </div>
                        </div>
                        <div class="col-md-1">
                            {{--<button type="button"  onclick="removeBox(1)"  class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>--}}
                        </div>
                    </div>
                    <div id="insertBefore"></div>
                    <div class="clearfix">

                    </div>
                    <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                        Add More <i class="fa fa-plus"></i>
                    </button>
                </div>
                <!--/row-->
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    var $insertBefore = $('#insertBefore');
    var $i = 0;

    // Add More Inputs
    $('#plusButton').click(function(){
        $i = $i+1;
        var indexs = $i+1;
        $(' <div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-3">' +
            '<div id="dateBox" class="form-group ">' +
            '<input class="form-control" autocomplete="off" id="dateField1" name="name['+$i+']" type="text" value="" placeholder="Name"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3" style="margin-left: 5px;">' +
            '<div class="form-group" id="occasionBox">' +
            '<input class="form-control"  type="text" name="display_name['+$i+']" placeholder="Display Name"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3" style="margin-left: 5px;">' +
            '<div class="form-group" id="occasionBox">' +
            '<input class="form-control"  type="text" name="permission_description['+$i+']" placeholder="Description"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' +
            '</div>').insertBefore($insertBefore);
    });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }

    $('#addModule').submit(function () {
        $.easyAjax({
            url: '{{route('super-admin.manageModules.store')}}',
            container: '#addModule',
            type: "POST",
            data: $('#addModule').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })



    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('super-admin.manageModules.store')}}',
            container: '#addModule',
            type: "POST",
            data: $('#addModule').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>