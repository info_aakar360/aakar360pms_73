@extends('layouts.super-admin')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.clients.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .mytooltip{
            z-index: 999 !important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Assign Permissions</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createClient','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="form-body">
                                <h3 class="box-title">Assign Permission</h3>
                                <hr>
                                <div class="row">
                                    <div class="input-group col-md-6" style="float: left; padding-right: 5px;">
                                        <select class="selectpicker form-control" name="role_id" data-style="form-control">
                                            <option value="">Please select Role</option>
                                            @foreach($roles as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-group col-md-6" style="float: left; padding-right: 5px;">
                                        <select class="selectpicker form-control" name="module_id" data-style="form-control" onchange="getPermissions(this.value);">
                                            <option value="">Please select Module</option>
                                            @foreach($modules as $category)
                                                <option value="{{ $category->id }}">{{ $category->module_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-lg-12 b-t permission-section" id="showData"></div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
<script>

    $('.toggle-permission').click(function () {
        var roleId = $(this).data('role-id');
        $('#role-permission-'+roleId).toggle();
    })


    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());

    });

    // Initialize multiple switches
    var animating = false;
    var masteranimate = false;

    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    function getPermissions(val) {
        var token = "{{ csrf_token() }}";
        var id = val;
        $.ajax({
            url: '{{route('super-admin.manageModules.getPermissions')}}',
            type: "POST",
            data: {'_token': token,'id': id},
            success: function (data) {
                console.log(data);
                $("#showData").html("");
                $("#showData").html(data);
            }
        });
    }

    $('#save-form').click(function () {
        $.ajax({
            url: '{{route('super-admin.manageModules.storeAssignPermissions')}}',
            container: '#createClient',
            type: "POST",
            data: $('#createClient').serialize(),
            success: function (data) {
                var msgs = "Permission applied successfully";
                $.showToastr(msgs, 'success');
                /*location.reload();*/
            }
        });
    });
</script>
@endpush

