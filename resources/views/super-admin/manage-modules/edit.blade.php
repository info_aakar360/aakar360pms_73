<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Module</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editModules','class'=>'ajax-form','method'=>'PUT']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Module Name</label>
                        <input type="text" name="module_name" id="module_name" class="form-control" value="{{ $module->module_name }}">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Display Name</label>
                        <input type="text" name="displayname" id="title" class="form-control" value="{{ $module->display_name }}">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control">{{ $module->description }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <label>Permissions</label>
                    <div class="row">
                        <?php $x=0;?>
                        @foreach($permissions as $per)
                        <div id="addMoreBox{{ $x }}" class="clearfix">
                            <div class="col-md-3">
                                <div id="dateBox" class="form-group ">
                                    <input class="form-control" autocomplete="off" id="dateField1" name="name[{{ $x }}]" type="text" value="{{ $per->name }}" placeholder="Name"/>
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-left: 5px;">
                                <div class="form-group" id="occasionBox">
                                    <input class="form-control"  type="text" name="display_name[{{ $x }}]" placeholder="Display Name" value="{{ $per->display_name }}" />
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-left: 5px;">
                                <div class="form-group" id="occasionBox">
                                    <input class="form-control"  type="text" name="permission_description[{{ $x }}]" placeholder="Description" value="{{ $per->description }}"/>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <input type="hidden" class="permission{{ $x }}" value="{{ $per->id }}" />
                                 <button type="button"  onclick="removeBox({{ $x }})"  class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                                <?php $x++;?>
                          @endforeach
                        <div id="insertBefore"></div>
                        <div class="clearfix">

                        </div>
                        <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                            Add More <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    var $insertBefore = $('#insertBefore');
    var $i = {{ $x }};

    // Add More Inputs
    $('#plusButton').click(function(){
        $i = $i+1;
        var indexs = $i+1;
        $(' <div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-3">' +
            '<div id="dateBox" class="form-group ">' +
            '<input class="form-control" autocomplete="off" id="dateField1" name="name['+$i+']" type="text" value="" placeholder="Name"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3" style="margin-left: 5px;">' +
            '<div class="form-group" id="occasionBox">' +
            '<input class="form-control"  type="text" name="display_name['+$i+']" placeholder="Display Name"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3" style="margin-left: 5px;">' +
            '<div class="form-group" id="occasionBox">' +
            '<input class="form-control"  type="text" name="permission_description['+$i+']" placeholder="Description"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' +
            '</div>').insertBefore($insertBefore);
    });
    // Remove fields
    function removeBox(index){
       var permission = $('.permission'+index).val();
       if(permission){
           var id = permission;
           var url = "{{ route('super-admin.manageModules.removePermissions',':id') }}";
           url = url.replace(':id', id);

           var token = "{{ csrf_token() }}";
           $.easyAjax({
               type: 'POST',
               url: url,
               data: {'_token': token, '_method': 'DELETE'},
               success: function (response) {

               }
           });
       }
        $('#addMoreBox'+index).remove();
    }
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('super-admin.manageModules.update', [$module->id])}}',
            container: '#editModules',
            type: "POST",
            data: $('#editModules').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    });
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('super-admin.manageModules.update', [$module->id])}}',
            container: '#editModules',
            type: "POST",
            data: $('#editModules').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>