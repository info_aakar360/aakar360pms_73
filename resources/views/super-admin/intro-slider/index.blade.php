@extends('layouts.super-admin')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('super-admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createTypeModal" class="btn btn-outline btn-success btn-sm">Add Slide <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.title')</th>
                            <th>@lang('app.image')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $x=1;
                        foreach($sliderarray as  $type) {
                            ?>
                               <tr id="tablerow{{ $type->id }}">
                                <td>{{ $x }}</td>
                                <td>{{ $type->title }}</td>
                                <td><?php if(!empty($type->image)){ ?><a  target="_blank" href="<?php echo uploads_url().'introslides/'.$type->image;?>"><img style="width: 100px;height: 100px;" src="<?php echo uploads_url().'introslides/'.$type->image;?>" /></a><?php }?></td>
                                   <td>
                                       <a type="button" class="btn btn-default editType" data-type-id="{{ $type->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-type-id="{{ $type->id }}" class="btn btn-sm btn-danger btn-circle sa-params"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                   </td>
                                </tr>
                        <?php
                        $x++;}
                               ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="createTypeModal">
        {!! Form::open(['id'=>'createTypeForm','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Slide</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                    <div class="form-group">
                        <label for="inputTitle">Title</label>
                        <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Enter title"/>
                    </div>
                    <div class="form-group">
                        <label for="inputSymbol">Image</label>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                            <div id="file-upload-box" >
                                <div class="row" id="file-dropzone">
                                    <div class="col-md-12">
                                        <div class="dropzone dropheight"
                                             id="file-upload-dropzone">
                                            {{ csrf_field() }}
                                            <div class="fallback">
                                                <input name="file" type="file" data-min-width="600" data-min-height="600" />
                                            </div>
                                            <input name="image_url" id="image_url" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="slideid" id="slideID">
                        </div>
                    </div>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    $('#createTypeForm').submit(function () {
        $.easyAjax({
            url: '{{route('super-admin.intro-slider.store')}}',
            container: '#createTypeForm',
            type: "POST",
            data: $('#createTypeForm').serialize(),
            success: function (response) {
                $('#createTypeForm').trigger("reset");
                if(myDropzone.getQueuedFiles().length > 0){
                    slideID = response.slideid;
                    $('#slideID').val(slideID);
                    myDropzone.processQueue();
                }
                else{
                    $("#createTypeModal").modal('hide');
                    var msgs = "File Updated Successfully";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('super-admin.intro-slider.index') }}'
                }
            }
        });
        return false;
    });

    $('.editType').click(function(){
        var id = $(this).data('type-id');
        var url = '{{ route('super-admin.intro-slider.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Intro Slider");
        $.ajaxModal('#taskCategoryModal', url);
    });

    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('super-admin.intro-slider.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 2,
        acceptedFiles: '.png, .jpeg, .jpg',
        maxFiles: 1,
        autoProcessQueue: false,
        uploadMultiple: false,
        thumbnailWidth: 600,
        thumbnailHeight: 600,
        addRemoveLinks:true,
        parallelUploads:1,
        init: function () {
            myDropzone = this;
        }
    });
    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#slideID').val();
        formData.append('slideid', ids);
    });

    myDropzone.on('completemultiple', function () {

        var msgs = "File updated successfully";
        $.showToastr(msgs, 'success');
        $("#createTypeModal").modal('hide');
        window.location.href = '{{ route('super-admin.intro-slider.index') }}'

    });


    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('type-id');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted type item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm') {
                var url = "{{ route('super-admin.intro-slider.destroy',':id') }}";
                url = url.replace(':id', id);
                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        $("#tablerow"+id).remove();
                    }
                });
            }
        });
    });

</script>
@endpush