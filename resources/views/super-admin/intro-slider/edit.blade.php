<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Intro Slide</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editType','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $slide->title }}">
                    </div>
                    <div class="form-group">
                        <label for="inputSymbol">Image</label>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                            <div id="file-upload-box" >
                                <div class="row" id="file-dropzone">
                                    <div class="col-md-12">
                                        <div class="dropzone dropheight"
                                             id="file-edit-dropzone">
                                            {{ csrf_field() }}
                                            <div class="fallback">
                                                <input name="file" type="file" data-min-width="600" data-min-height="600" />
                                            </div>
                                            <input name="image_url" id="image_url" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="slideid" id="slideID">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-type" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>

    myDropzone1 = new Dropzone("div#file-edit-dropzone", {
        url: "{{ route('super-admin.intro-slider.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 2,
        maxFiles: 1,
        acceptedFiles: '.png, .jpeg, .jpg',
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        thumbnailWidth: 600,
        thumbnailHeight: 600,
        parallelUploads:1,
        init: function () {
            myDropzone = this;
        }
    });
    myDropzone1.on('sending', function(file, xhr, formData) {
        console.log(myDropzone1.getAddedFiles().length,'sending');
        var ids = $('#slideID').val();
        formData.append('slideid', ids);
    });

    myDropzone1.on('completemultiple', function () {
        var msgs = "File updated successfully";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('super-admin.intro-slider.index') }}'
    });

    $('#save-type').click(function () {
        $.easyAjax({
            url: '{{route('super-admin.intro-slider.update', [$slide->id])}}',
            container: '#editType',
            type: "PATCH",
            data: $('#editType').serialize(),
            success: function (response) {
                $('#createTypeForm').trigger("reset");
                if(myDropzone1.getQueuedFiles().length > 0){
                    slideID = response.slideid;
                    $('#slideID').val(slideID);
                    myDropzone1.processQueue();
                }
                else{
                    var msgs = "File Updated Successfully";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('super-admin.intro-slider.index') }}'
                }
            }
        })
    });

</script>