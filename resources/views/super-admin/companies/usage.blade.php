@extends('layouts.super-admin')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('super-admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <div class="col-md-12">
            <div class="example">
                <h5 class="box-title">@lang('app.company')</h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="companyid" id="companyid" placeholder="@lang('app.company')" />
                 </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="example">
                <h5 class="box-title">@lang('app.selectDateRange')</h5>
                <div class="input-daterange input-group" id="date-range">
                    <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.startDate')" value="{{ \Carbon\Carbon::today()->subDays(7)->format($global->date_format) }}" />
                    <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                    <input type="text" class="form-control" name="enddate" id="end-date" placeholder="@lang('app.endDate')" value="{{ \Carbon\Carbon::today()->format($global->date_format) }}" />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h5 class="box-title m-t-10"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="pull-left">
                            <h2 style="color: #002f76">@lang('app.usage')</h2>
                            <a href="{{ route('super-admin.companies.index') }}" class="btn btn-outline btn-success btn-sm">@lang('app.company')</a>
                            <a href="{{ route('super-admin.companies.un-reg-users') }}" class="btn btn-outline btn-success btn-sm">Un Reg Users</a>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="pull-right">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                        class="fa fa-cog"></i></a>
                            {{-- <a onclick="exportTimeLog()" class="btn btn-info"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.name')</th>
                            <th>@lang('app.mobile')</th>
                            <th>@lang('app.projects')</th>
                            <th>@lang('app.menu.employee')</th>
                            <th>@lang('app.menu.client')</th>
                            <th>@lang('app.contractor')</th>
                            <th>@lang('app.menu.drawings')</th>
                            <th>@lang('app.menu.photos')</th>
                            <th>@lang('app.menu.documents')</th>
                            <th>@lang('app.menu.labourattendance')</th>
                            <th>@lang('app.menu.issues')</th>
                            <th>@lang('app.activity')</th>
                            <th>@lang('app.task')</th>
                            <th>@lang('app.menu.material')</th>
                            <th>@lang('app.menu.indent')</th>
                            <th>@lang('app.menu.materialreceive')</th>
                            <th>@lang('app.menu.materialissue')</th>
                            <th>@lang('app.menu.materialissuereturn')</th>
                            <th>@lang('app.attendance')</th>
                            <th>@lang('app.payments')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="packageUpdateModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <form class="ajax-form" id="update-company-form">
                @csrf
                @method('PUT')
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Change Package</span>
                    </div>
                    <div class="modal-body">
                        Loading...
                    </div>
                    <div class="modal-footer">
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.update')</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.back')</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="offlineMethod" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(function() {
            var modal = $('#packageUpdateModal');
            tableLoad();
            $('#reset-filters').click(function () {
                $('#filter-form')[0].reset();
                $('#filter-form').find('select').selectpicker('render');
                tableLoad();
            });
            var table;
            $('.toggle-filter').click(function () {
                $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
            });
            $('#filter-results').click(function () {
                tableLoad();
                $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
            });

            $('#reset-filters').click(function () {
                $('#filter-form')[0].reset();
                $('#status').val('all');
                $('.select2').val('all');
                $('#filter-form').find('select').select2();
                tableLoad();
            });

            $('body').on('click', '.package-update-button', function () {
                modal.find('.modal-body').html('Loading...');
                const url = '{{ route('super-admin.companies.edit-package.get', ':companyId') }}' . replace(':companyId', $(this).data(
                    'company-id'
                ));
                $.easyAjax({
                    type: 'GET',
                    url: url,
                    blockUI: false,
                    messagePosition: "inline",
                    success: function (response) {
                        if (response.status === "success" && response.data) {
                            modal.find('.modal-body').html(response.data).closest('#packageUpdateModal').modal('show');
                            tableLoad();
                        } else {
                            modal.find('.modal-body').html('Loading...').closest('#packageUpdateModal').modal('show');
                        }
                    }
                });
            });

            $('#offlineMethod').on('hidden.bs.modal', function (e) {
                $('body').addClass('modal-open');
            })
        });

        jQuery('#date-range').datepicker({
            toggleActive: true,
        });

        tableLoad = () => {
            var startdate = $('#start-date').val();
            var enddate = $('#end-date').val();
            var companyid = $('#companyid').val();

            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                stateSave: true,
                destroy: true,
                ajax: '{!! route('super-admin.companies.usagedata') !!}?companyid='+companyid+'&startdate='+startdate+'&enddate='+enddate,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'company_name', name: 'company_name' },
                    { data: 'company_phone', name: 'company_phone' },
                    { data: 'projects', name: 'projects' },
                    { data: 'employee', name: 'employee' },
                    { data: 'client', name: 'client' },
                    { data: 'contractor', name: 'contractor' },
                    { data: 'drawings', name: 'drawings' },
                    { data: 'photos', name: 'photos' },
                    { data: 'documents', name: 'documents' },
                    { data: 'labourattendance', name: 'labourattendance' },
                    { data: 'issues', name: 'issues' },
                    { data: 'activity', name: 'activity' },
                    { data: 'task', name: 'task' },
                    { data: 'products', name: 'products' },
                    { data: 'indents', name: 'indents' },
                    { data: 'materialreceive', name: 'materialreceive' },
                    { data: 'materialissue', name: 'materialissue' },
                    { data: 'materialissue', name: 'materialissuereturn' },
                    { data: 'attendance', name: 'attendance' },
                    { data: 'payments', name: 'payments' },
                ]
            });
        }
        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('user-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted company!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('super-admin.companies.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                var total = $('#totalCompanies').text();
                                $('#totalCompanies').text(parseInt(total) - parseInt(1));
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

    </script>
@endpush
