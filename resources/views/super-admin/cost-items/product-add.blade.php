@extends('layouts.super-admin')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('super-admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="white-box">

            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Task Name</label><br>
                        {{ $category->cost_item_name }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Name</td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($costlavel as $key=>$cl)
                                <?php
                                    $bcid = $cl->boq_category_id;
                                    $d = App\BoqCategory::where('id',$bcid)->first();
                                ?>
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $d->title }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Task Description</label><br>
                        {!! $category->cost_item_description !!}
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Category Name</td>
                                <td>Brand Name</td>
                                <td>Unit</td>
                                <td>Quantity</td>
                                <td>Wastage</td>
                                <td>Rate</td>
                                <td>Cost</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($costproducts as $key=>$cp)
                                <?php
                                $pc = $cp->product_category_id;
                                $pb = $cp->product_brand_id;
                                $pu = $cp->unit;
                                $pcn = App\ProductCategory::where('id',$pc)->first();
                                $pbn = App\ProductBrand::where('id',$pb)->first();
                                $pun = App\Units::where('id',$pu)->first();
                                ?>
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $pcn->name}}</td>
                                    <td>{{ $pbn->name}}</td>
                                    <td>{{ $pun->name}}</td>
                                    <td>{{ $cp->qty}}</td>
                                    <td>{{ $cp->wastage}}</td>
                                    <td>{{ $cp->rate}}</td>
                                    <td>{{ $cp->cost}}</td>
                                    <td>
                                        <a href="javascript:;" data-cat-id="{{ $cp->id }}" class="btn btn-info btn-circle editProduct"
                                           data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        &nbsp;
                                        <a href="javascript:;" data-cat-id="{{ $cp->id }}" class="btn btn-sm btn-danger btn-circle delete-category">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
<hr>

            {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
            <div class="form-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="form-group">
                            <label>Select Product</label>
                            <div id="sortable">
                                <div class="col-xs-12 item-row margin-top-5" style="padding-bottom: 10px;">
                                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="input-group col-md-2" style="float: left; padding-right: 5px;">
                                                    <select name="product_category_id[]" id="product_category" class="form-control col-md-12">
                                                        <option value="">Please select Product category</option>
                                                        @foreach($pcat as $category)
                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="input-group col-md-2" style="float: left; padding-right: 5px;">
                                                    <select name="product_brand_id[]" id="product_brand" class="form-control col-md-12">
                                                        <option value="">Please select Product brand</option>
                                                        @foreach($pbrand as $pb)
                                                            <option value="{{ $pb->id }}">{{ $pb->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="input-group col-md-1" style="float: left; padding-right: 5px;">
                                                    <select name="unit[]" id="unit" class="form-control col-md-12">
                                                        <option value="">Please select unit</option>
                                                        @foreach($units as $unit)
                                                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="input-group col-md-1" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" name="qty[]" id="qty" placeholder="Quantity">
                                                </div>
                                                <div class="input-group col-md-1" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" placeholder="Wastage (%)" name="wastage[]">
                                                </div>
                                                <div class="input-group col-md-1" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" name="rate[]" onchange="getCost();" id="rate" placeholder="Rate">
                                                </div>
                                                <div class="input-group col-md-1" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" name="cost[]" placeholder="Cost" id="cost" value="" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 m-t-5">
                                <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">&nbsp;</div>
            <div class="form-actions">
                <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script>
    function getCost(val) {
        var qty = $('#qty').val();
        var rate = $('#rate').val();
        var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
        $('#cost').val(total);
    }

    $('.editProduct').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('super-admin.costItems.product-edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('super-admin.costItems.store-product', [$costid])}}',
            container: '#createBoqCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })


    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('super-admin.costItems.store-product', [$costid])}}',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),

            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('super-admin.costItems.destroy-product',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        });
    });

    var r = 1;

    $('#add-item').click(function () {
        r++;
        var item = '<div class="col-xs-12 item-row margin-top-5">'
            +'<div class="col-md-2" style="float: left; padding-right: 20px;">'
            +'<div class="form-group">'
            +'<select name="product_category_id[]" class="form-control">'
                +'<option value="">Select product category</option>';
                @foreach($pcats as $cat)
                    item += '<option value="{{ $cat->id }}">{{ $cat->name }}</option>';
                @endforeach
            item += '</select>'
            +'</div>'
            +'</div>'
            +'<div class="col-md-2" style="float: left; padding-right: 20px;">'
            +'<div class="form-group">'
            +'<select name="product_brand_id[]" class="form-control">'
                +'<option value="">Select product brand</option>';
                @foreach($pbrands as $band)
                    item += '<option value="{{ $band->id }}">{{ $band->name }}</option>';
                @endforeach
            item += '</select>'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1" style="float: left; padding-right: 20px;">'
            +'<div class="form-group">'
            +'<select name="unit[]" class="form-control">'
                +'<option value="">Select unit</option>';
                @foreach($units as $unit)
                    item += '<option value="{{ $unit->id }}">{{ $unit->name }}</option>';
                @endforeach
            item += '</select>'
            +'</div>'
            +'</div>'
            +'<div class="input-group col-md-1" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" name="qty[]" id="qty'+r+'" placeholder="Quantity">'
            +'</div>'
            +'<div class="input-group col-md-1" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" placeholder="Wastage (%)" name="wastage[]">'
            +'</div>'
            +'<div class="input-group col-md-1" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" name="rate[]" onchange="getCoste();" id="rate'+r+'" placeholder="Rate">'
            +'</div>'
            +'<div class="input-group col-md-1" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" name="cost[]" placeholder="Cost" id="cost'+r+'" value="" readonly>'
            +'</div>'

            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortable").fadeIn(500);
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault()
        $form = $(evt.target).closest('form')
        $form[0].reset()
        $form.find('select').selectpicker('render')
    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
    function getCoste() {
        var qty = $('#qty'+r).val();
        var rate = $('#rate'+r).val();
        var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
        $('#cost'+r).val(total);
    }
</script>
@endpush