<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Product</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editProduct','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="item-row margin-top-5" style="padding-bottom: 10px;">
                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                        <div class="row">
                            <div class="form-group">
                                <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                    <select name="product_category_id" id="product_category" class="form-control col-md-12">
                                        <option value="">Please select Product category</option>
                                        @foreach($pcat as $category)
                                            <?php $selected = '';
                                            if($costproduct->product_category_id == $category->id){
                                                $selected = 'selected';
                                            }?>
                                            <option value="{{ $category->id }}" <?=$selected; ?>>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                    <select name="product_brand_id" id="product_brand" class="form-control col-md-12">
                                        <option value="">Please select Product brand</option>
                                        @foreach($pbrand as $pb)
                                            <?php $selected1 = '';
                                            if($costproduct->product_brand_id == $pb->id){
                                                $selected1 = 'selected';
                                            }?>
                                            <option value="{{ $pb->id }}" <?=$selected1; ?>>{{ $pb->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                    <select name="unit" id="unit" class="form-control col-md-12">
                                        <option value="">Please select unit</option>
                                        @foreach($units as $unit)
                                            <?php $selected2 = '';
                                            if($costproduct->unit == $unit->id){
                                                $selected2 = 'selected';
                                            }?>
                                            <option value="{{ $unit->id }}" <?=$selected2; ?>>{{ $unit->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12" style="float: left; padding-right: 20px;">&nbsp;</div>
                                <div class="input-group col-md-3" style="float: left; padding-right: 5px;">
                                    <input type="text" class="form-control" name="qty" value="{{ $costproduct->qty }}" id="qt" placeholder="Quantity">
                                </div>
                                <div class="input-group col-md-3" style="float: left; padding-right: 5px;">
                                    <input type="text" class="form-control" value="{{ $costproduct->wastage }}" placeholder="Wastage (%)" name="wastage">
                                </div>
                                <div class="input-group col-md-3" style="float: left; padding-right: 5px;">
                                    <input type="text" class="form-control" value="{{ $costproduct->rate }}" name="rate" onchange="getCo();" id="rat" placeholder="Rate">
                                </div>
                                <div class="input-group col-md-3" style="float: left; padding-right: 5px;">
                                    <input type="text" class="form-control" value="{{ $costproduct->cost }}" name="cost" placeholder="Cost" id="co" value="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="float: left; padding-right: 20px;">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="update-product" data-cat-id="{{ $costproduct->id }}" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    function getCo() {
        var qt = $('#qt').val();
        var rat = $('#rat').val();
        var total = (parseFloat(qt)*parseFloat(rat)).toFixed(2);
        $('#co').val(total);
    }

    $('#editProduct').submit(function () {
        $.easyAjax({
            url: '{{route('super-admin.costItems.update-product', [$costproduct->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('#update-product').click(function () {

        $.easyAjax({

            url : '{{route('super-admin.costItems.update-product', [$costproduct->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),

            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });


</script>