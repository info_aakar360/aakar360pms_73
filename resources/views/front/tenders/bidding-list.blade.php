@extends('layouts.public-quote')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.purchase-order.index') }}">@lang('app.menu.po')</a></li>
                <li>{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')
    <?php if(!empty($tenders)&&!empty($tendercontract)){?>
    <div class="row" >
    <div class="col-md-10">
    <div class="panel panel-inverse" style="overflow: auto;">
        <div class="panel-heading"> Tender Bidding <span style="float: right">Contractor : {{ get_employee_name($tendercontract->employee_id) }}</span></div>
        <div class="panel-wrapper collapse in">
            <div class="row">
                <table class="table-striped" style="width: 100%;">
                    <tr>
                        <td style="width: 15%;"><b>Tender</b></td>
                        <td  style="width: 35%;">{{ $tenders->name }}</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;"><b>@lang('app.number')</b></td>
                        <td  style="width: 35%;">{{ $tenders->number }}</td>
                        <td style="width: 15%;"><b>@lang('app.status')</b></td>
                        <td  style="width: 35%;">{{ $tenders->status }}</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;"><b>Start date</b></td>
                        <td  style="width: 35%;">{{ $tenders->startdate ? date('d-m-Y',strtotime($tenders->startdate)) : '' }}</td>

                        <td style="width: 15%;"><b>Due date</b></td>
                        <td  style="width: 35%;">{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}</td>
                        <td style="width: 15%;"><b>Anticipated Award date</b></td>
                        <td  style="width: 35%;">{{ $tenders->anticipated_date ? date('d-m-Y',strtotime($tenders->anticipated_date)) : '' }}</td>
                        <td style="width: 15%;"><b>Bid Documents</b></td>
                        <td  style="width: 35%;"> <?php if($tenders->include_bid_doc=='1'){ ?><i class="fa fa-check-circle"></i><?php }else{?><i class="fa fa-times"></i><?php }?> <b>Include Bid Documents</b></td>
                    </tr>
                </table>
        <div class="col-sm-12 col-xs-12">
            <label for="duedate"><b>Documents</b></label>
        </div>
        <div class="col-md-12">
            @foreach($files as $file)
                <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                    @if($file->external_link != '')
                        <?php $imgurl = $file->external_link;?>
                    @elseif($storage == 'local')
                        <?php $imgurl = uploads_url().'tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                    @elseif($storage == 's3')
                        <?php $imgurl = awsurl().'/tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                    @elseif($storage == 'google')
                        <?php $imgurl = $file->google_url;?>
                    @elseif($storage == 'dropbox')
                        <?php $imgurl = $file->dropbox_link;?>
                    @endif
                    {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                    <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                </div>
            @endforeach

        </div>
                <table class="table-striped" style="width: 100%;">
                    <tr>
                        <td style="width: 15%;"><b>Project Information</b></td>
                        <td  style="width: 35%;">{!! $tenders->bidding_information !!}</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;"><b>Bidding Information</b></td>
                        <td  style="width: 35%;">{!! $tenders->project_information !!}</td>
                    </tr>
                </table>
        <div class="col-sm-6 col-xs-6">
            <h3>Pre Bid RFI Information</h3>
            <hr>
        </div>
        <div class="col-sm-6 col-xs-6">
            <h3>Pre Bid WalkThrough Information</h3>
            <hr>
        </div>
                <table class="table-striped" style="width: 100%;">
                    <tr>
                        <td style="width: 15%;"><b>Pre-Bid RFI Deadline</b></td>
                        <td  style="width: 35%;">{{ $tenders->prebid_rfi_date ? date('d-m-Y',strtotime($tenders->prebid_rfi_date)) : '' }}</td>
                        <td style="width: 15%;"><b>Pre-Bid WalkThrough Deadline</b></td>
                        <td  style="width: 35%;">{{ $tenders->prebid_walkthrough_date ? date('d-m-Y',strtotime($tenders->prebid_walkthrough_date)) : '' }}</td>
                    </tr>
                    <tr>
                        <td  ><b>Walkthough Information</b></td>
                        <td > {!! $tenders->walkthough_information !!}</td>
                     </tr>
                </table>

        <div class="col-sm-6 col-xs-6">
            <div class="form-group">
                <label for="duedate"><b>Project</b></label>
                <p>{{ get_project_name($tenders->project_id) }}</p>
            </div>
        </div>
        </div>
        </div>
        </div>
            <hr/>
            <h3>Product details</h3>
            <hr/>
        <div class="col-sm-12 col-xs-12">
            <div class="table-responsive" id="pdata">
                <table class="table">
                    <thead >
                    <tr>
                        <th>Activity / Task</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Rate</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="costitemloop">

                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" name="projectID" id="projectID">
        <div class="col-sm-11 col-xs-10">
            @if(!empty($tenderbidding))
                <div class="alert alert-danger">
                    <p>You have already submitted tender bit</p>
                </div>
                @endif
        </div>
           <div class="col-sm-1 col-xs-2">
                <a href="javascript:void(0);" id="savebutton" class="btn btn-success waves-effect waves-light m-r-10">
                    @lang('app.submit')
                </a>
            </div>
    </div>
    </div>
    <?php }else{ ?>
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="form-group">
                   Bidding information not found
                </div>
            </div>
        </div>

    <?php }?>
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        var r = 0;
        $("body").on("change",".bidupdate",function () {
            var tenderproduct = $(this).data('tenderproduct');
            var cat = $(this).data('cat');
            var price = $(this).val();
            var qty = 0;
              qty = $(".qty"+tenderproduct).html();
            var token = "{{ csrf_token() }}";
            var tendercontract = '{{ $tendercontract->id }}';
            var famount = price*qty;
            if(famount){
                $(".finalamount"+tenderproduct).val(famount.toFixed(2));
            }
            $.ajax({
                type: "POST",
                url: "{{ route('front.contractor_bidding_product') }}",
                data: {'_token': token,'tenderproduct': tenderproduct,'tendercontract': tendercontract,'price': price},
                success: function(data){
                    if(data.amount>0){
                        $(".finalamount"+tenderproduct).val(data.amount.toFixed(2));
                    }
                }
            }).done(function(data){
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".catfinal"+cat).each(function() {
                    sum += parseFloat($(this).val());
                    $(".catamount"+cat).html(sum);
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".grandtotal").each(function() {
                    sum += parseFloat($(this).val());
                    $(".grandamount").html(sum.toFixed(2));
                });
            });
        });

        costitemloop();
        function  costitemloop() {
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var tender = '{{ $tenders->id }}';
            var tendercontract = '{{ $tendercontract->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                url : "{{ route('front.biddingloop') }}",
                method: 'POST',
                data: {'_token': token,'tender': tender,'tendercontract': tendercontract},
                success: function (response) {
                    $("#costitemloop").html("");
                    $("#costitemloop").html(response);
                }

            });
        }
        $("#savebutton").click(function () {
            var msgs = "Rate sheet has been updated";
            $.showToastr(msgs, 'success');
        });
    </script>
@endpush

