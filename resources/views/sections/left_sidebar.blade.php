<div class="navbar-default sidebar slimscrollsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">

        <!-- .User Profile -->
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span> </div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                @if(is_null($user->image))
                    <a href="#" class="waves-effect"><img src="{{ asset('default-profile-3.png') }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ (strlen($user->name) > 24) ? substr(ucwords($user->name), 0, 20).'..' : ucwords($user->name) }}
                    <span class="fa arrow"></span></span>
                    </a>
                @else
                    <a href="#" class="waves-effect"><img src="{{ asset('user-uploads/avatar/'.$user->image) }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ ucwords($user->name) }}
                            <span class="fa arrow"></span></span>
                    </a>
                @endif
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('member.dashboard') }}">
                            <i class="fa fa-sign-in"></i> @lang('app.loginAsEmployee')
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                        ><i class="fa fa-power-off"></i> @lang('app.logout')</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>

            <li>
                <a href="{{ route('admin.dashboard') }}" class="waves-effect">
                    <i class="icon-speedometer"></i>
                    <span class="hide-menu">@lang('app.menu.dashboard') </span>
                </a>
            </li>

            @if(in_array("suppliers", $modules))
                <li>
                    <a href="{{ route('admin.suppliers.index') }}" class="waves-effect">
                        <i class="icon-people"></i>
                        <span class="hide-menu">@lang('app.menu.suppliers') </span>
                    </a>
                </li>
            @endif

            @if(in_array('stores',$modules))
                    <li><a href="{{ route('admin.stores.index') }}" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">@lang('app.menu.stores') </span></a> </li>
            @endif

            @if(in_array('indent',$modules))
                    <li><a href="{{ route('admin.indent.index') }}" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">@lang('app.menu.indent') </span></a> </li>
            @endif

            @if(in_array('rfq',$modules))
                    <li><a href="{{ route('admin.rfq.index') }}" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">@lang('app.menu.rfq') </span></a> </li>
            @endif

            @if(in_array('quotes',$modules))
                    <li><a href="{{ route('admin.quotations.index') }}" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">@lang('app.menu.quotes') </span></a> </li>
           @endif

            @if(in_array('po',$modules))
                    <li><a href="{{ route('admin.purchase-order.index') }}" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">@lang('app.menu.po') </span></a> </li>
            @endif

            @if(in_array('inventory',$modules))
                <li><a href="#" class="waves-effect"><i class="ti-layout-list-thumb"></i> <span class="hide-menu"> @lang('app.menu.inventory') <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        {{--<li><a href="{{ route('member.inventory.inward') }}">@lang('app.menu.inward')</a></li>--}}
                        <li><a href="{{ route('admin.inventory.stock') }}">@lang('app.menu.stock')</a></li>
                        <li><a href="{{ route('admin.inventory.invoices') }}">@lang('app.menu.pinvoices')</a></li>
                        <li><a href="{{ route('admin.inventory.returns') }}">@lang('app.menu.return')</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("clients", $modules))
                <li>
                    <a href="{{ route('admin.clients.index') }}" class="waves-effect">
                        <i class="icon-people"></i>
                        <span class="hide-menu">@lang('app.menu.clients') </span>
                    </a>
                </li>
            @endif

            @if(in_array("leads", $modules) )
                <li>
                    <a href="{{ route('admin.leads.index') }}" class="waves-effect">
                        <i class="ti-receipt"></i>
                        <span class="hide-menu"> @lang('app.menu.lead')</span>
                    </a>
                </li>
            @endif

            @if(in_array('projects',$modules))
                    <li>
                        <a href="#" class="waves-effect">
                            <i class="icon-layers"></i>
                            <span class="hide-menu">@lang("app.menu.projects") </span>
                            {{--@if($unreadProjectCount > 0)--}}
                                {{--<div class="notify notification-color">--}}
                                    {{--<span class="heartbit"></span>--}}
                                    {{--<span class="point"></span>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('admin.projects.index') }}">@lang('app.menu.projects') </a></li>
                            <li><a href="{{ route('admin.projects.tenderBiddings') }}">Tender Bidding</a></li>
                        </ul>
                    </li>
            @elseif(in_array('stores',$modules))
                    <li><a href="{{ route('admin.projects.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">@lang('app.menu.projects') </span></a> </li>
            @endif

            @if(in_array("files", $modules))
                <li><a href="{{ route('admin.manage-files.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">@lang('app.menu.files') </span></a> </li>
            @endif

            @if(in_array("punch_items", $modules))
                <li><a href="{{ route('admin.punch-items.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">@lang('app.menu.punchItems') </span></a> </li>
            @endif

            @if(in_array("boq", $modules))
                <li><a href="{{ route('admin.product-category.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">Product Category</span></a> </li>
            @endif

            @if(in_array("boq", $modules))
                <li><a href="{{ route('admin.condition.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">Conditions</span></a> </li>
            @endif

            @if(in_array("boq", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Type <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.product-type-workforce.workforce-index') }}">Workforce</a></li>
                        <li><a href="{{ route('admin.product-type-equipment.equipment-index') }}">Equipment</a></li>
                        <li><a href="{{ route('admin.product-type-material.material-index') }}">Material</a></li>
                        <li><a href="{{ route('admin.product-type-commitment.commitment-index') }}">Commitment</a></li>
                        <li><a href="{{ route('admin.product-type-owner-cost.owner-cost-index') }}">Owner Cost</a></li>
                        <li><a href="{{ route('admin.product-type-professional.professional-index') }}">Professional Services</a></li>
                        <li><a href="{{ route('admin.product-type-other.other-index') }}">Other</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li><a href="{{ route('admin.product-brand.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">Product Brands</span></a> </li>
            @endif

            @if(in_array("rfi", $modules))
                <li><a href="{{ route('admin.rfi.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">RFI</span></a> </li>
            @endif

            @if(in_array("boq", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> BOQ <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.activity.index') }}">Boq Category</a></li>
                        <li><a href="{{ route('admin.cost-items.index') }}">Cost Items</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("location", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Location<span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.location.index') }}">Location</a></li>

                    </ul>
                </li>
            @endif

            @if(in_array("workrequest", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Work Request<span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.create-work-request') }}">Create Work Request</a></li>

                    </ul>
                </li>
            @endif

            @if(in_array("workorder", $modules) )
                <li>
                    <a href="#" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">Work Order</span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.work-order') }}">Work Order</a></li>
                        <li><a href="{{ route('admin.work-order.recurring') }}">Recurring Work Order</a></li>

                    </ul>

                </li>

            @endif


        @if(in_array("asset", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Asset <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.asset-category.index') }}">Category</a></li>
                        <li><a href="{{ route('admin.asset-category.assets') }}">Asset</a></li>
                        <li><a href="{{ route('admin.asset-category.sub-asset') }}">Sub Asset</a></li>

                    </ul>
                </li>
            @endif


        @if(in_array("inspection", $modules) )
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Inspection <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.inspection-type.index') }}">Inspection Type</a></li>
                        <li><a href="{{ route('admin.input-fields.index') }}">Input Fields</a></li>
                        <li><a href="{{ route('admin.inspection-name.index') }}">Inspection Form</a></li>

                        <li><a href="{{ route('admin.inspectionName.inspectionReplies') }}">Replies</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("tasks", $modules) )
                <li><a href="{{ route('admin.task.index') }}" class="waves-effect"><i class="ti-layout-list-thumb"></i> <span class="hide-menu"> @lang('app.menu.tasks') <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.all-tasks.index') }}">@lang('app.menu.tasks')</a></li>
                       {{-- <li><a href="{{ route('admin.taskboard.index') }}">@lang('modules.tasks.taskBoard')</a></li>--}}
                        <li><a href="{{ route('admin.taskscalender.index') }}">@lang('app.menu.taskCalendar')</a></li>
                    </ul>
                </li>
            @endif


            @if(in_array("products", $modules))
                <li><a href="{{ route('admin.products.index') }}" class="waves-effect"><i class="icon-basket"></i> <span class="hide-menu">@lang('app.menu.products') </span></a> </li>
            @endif


            @if((in_array("estimates", $modules)  || in_array("invoices", $modules)  || in_array("payments", $modules) || in_array("expenses", $modules)  ))
                <li><a href="{{ route('admin.finance.index') }}" class="waves-effect"><i class="fa fa-money"></i> <span class="hide-menu"> @lang('app.menu.finance') @if($unreadExpenseCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        @if(in_array("estimates", $modules))
                            <li><a href="{{ route('admin.estimates.index') }}">@lang('app.menu.estimates')</a> </li>
                        @endif

                        @if(in_array("invoices", $modules))
                            <li><a href="{{ route('admin.all-invoices.index') }}">@lang('app.menu.invoices')</a> </li>
                        @endif

                        @if(in_array("payments", $modules))
                            <li><a href="{{ route('admin.payments.index') }}">@lang('app.menu.payments')</a> </li>
                        @endif

                        @if(in_array("expenses", $modules))
                            <li><a href="{{ route('admin.expenses.index') }}">@lang('app.menu.expenses') @if($unreadExpenseCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
                        @endif

                        @if(in_array("invoices", $modules))
                            <li><a href="{{ route('admin.all-credit-notes.index') }}">@lang('app.menu.credit-note')</a> </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(in_array("timelogs", $modules))
                <li><a href="{{ route('admin.all-time-logs.index') }}" class="waves-effect"><i class="icon-clock"></i> <span class="hide-menu">@lang('app.menu.timeLogs') </span></a> </li>
            @endif

            @if(in_array("tickets", $modules))
                <li><a href="{{ route('admin.tickets.index') }}" class="waves-effect"><i class="ti-ticket"></i> <span class="hide-menu">@lang('app.menu.tickets')</span> @if($unreadTicketCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
            @endif


            @if(in_array("employees", $modules))
                <li><a href="{{ route('admin.employees.index') }}" class="waves-effect"><i class="ti-user"></i> <span class="hide-menu"> @lang('app.menu.employees') <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.employees.index') }}">@lang('app.menu.employeeList')</a></li>
                        <li><a href="{{ route('admin.employees.allowance') }}">@lang('app.menu.allowancelessdiduction')</a></li>
                        <li><a href="{{ route('admin.employees.overtime') }}">@lang('app.menu.overtime')</a></li>
                        <li><a href="{{ route('admin.employees.salary_slip') }}">@lang('app.menu.salarySlip')</a></li>
                        <li><a href="{{ route('admin.teams.index') }}">@lang('app.department')</a></li>
                        <li><a href="{{ route('admin.designations.index') }}">@lang('app.menu.designation')</a></li>
                    </ul>
                </li>
            @endif


            @if(in_array("attendance", $modules))
                <li><a href="#" class="waves-effect"><i class="icon-clock"></i> <span class="hide-menu"> @lang('app.menu.markAttendance') <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.attendances.singleAttendance') }}">@lang('app.menu.singleAttendance')</a></li>
                        <li><a href="{{ route('admin.attendances.byCamera') }}">@lang('app.menu.posAttendance')</a></li>
                        <li><a href="{{ route('admin.attendances.create') }}">@lang('app.menu.groupAttendace')</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('admin.attendances.summary') }}" class="waves-effect"><i class="icon-clock"></i> <span class="hide-menu">@lang('app.menu.attendanceReport') </span></a> </li>
            @endif
            @if(in_array("holidays", $modules))
                <li><a href="{{ route('admin.holidays.index') }}" class="waves-effect"><i class="ti-calendar"></i> <span class="hide-menu"> @lang('app.menu.holiday')</span></a>
                </li>
            @endif


            @if(in_array("messages", $modules))
                <li><a href="{{ route('admin.user-chat.index') }}" class="waves-effect"><i class="icon-envelope"></i> <span class="hide-menu">@lang('app.menu.messages') @if($unreadMessageCount > 0)<span class="label label-rouded label-custom pull-right">{{ $unreadMessageCount }}</span> @endif</span></a> </li>
            @endif

            @if(in_array("events", $modules))
                <li><a href="{{ route('admin.events.index') }}" class="waves-effect"><i class="icon-calender"></i> <span class="hide-menu">@lang('app.menu.Events')</span></a> </li>
            @endif

            @if(in_array("leaves", $modules))
                <li><a href="{{ route('admin.leave.all-leaves') }}" class="waves-effect"><i class="icon-logout"></i> <span class="hide-menu">@lang('app.menu.leaves')</span></a> </li>
            @endif

            @if(in_array("notices", $modules))
                <li><a href="{{ route('admin.notices.index') }}" class="waves-effect"><i class="ti-layout-media-overlay"></i> <span class="hide-menu">@lang('app.menu.noticeBoard') </span></a> </li>
            @endif
{{--            @if($company->status != 'license_expired')--}}
            <li><a href="{{ route('admin.reports.index') }}" class="waves-effect"><i class="ti-pie-chart"></i> <span class="hide-menu"> @lang('app.menu.reports') <span class="fa arrow"></span> </span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('admin.task-report.index') }}">@lang('app.menu.taskReport')</a></li>
                    <li><a href="{{ route('admin.time-log-report.index') }}">@lang('app.menu.timeLogReport')</a></li>
                    <li><a href="{{ route('admin.finance-report.index') }}">@lang('app.menu.financeReport')</a></li>
                    <li><a href="{{ route('admin.income-expense-report.index') }}">@lang('app.menu.incomeVsExpenseReport')</a></li>
                    <li><a href="{{ route('admin.leave-report.index') }}">@lang('app.menu.leaveReport')</a></li>
                    <li><a href="{{ route('admin.attendance-report.index') }}">@lang('app.menu.attendanceReport')</a></li>
                </ul>
            </li>
            {{--@endif--}}

            @if(in_array('contracts',$modules))
                <li><a href="{{ route('admin.contracts.index') }}" class="waves-effect"><i class="fa fa-file"></i> <span class="hide-menu"> @lang('app.menu.contracts') <span class="fa arrow"></span> </span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('admin.contract-type.index') }}">@lang('app.menu.contractType')</a></li>
                        <li><a href="{{ route('admin.contracts.index') }}">@lang('app.menu.contracts')</a></li>
                    </ul>
                </li>
            @endif

            @role('admin')
            <li><a href="{{ route('admin.billing') }}" class="waves-effect"><i class="icon-book-open"></i> <span class="hide-menu"> @lang('app.menu.billing')</span></a>
            </li>
            @endrole
            <li><a href="{{ route('admin.faqs.index') }}" class="waves-effect"><i class="icon-docs"></i> <span class="hide-menu"> @lang('app.menu.faq')</span></a></li>
                <li><a href="{{ route('admin.settings.index') }}" class="waves-effect"><i class="ti-settings"></i> <span class="hide-menu"> @lang('app.menu.settings')</span></a>
                </li>
        </ul>
    </div>
</div>