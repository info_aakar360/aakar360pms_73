<div class="navbar-default sidebar1 main-menu-shirnk1">
    <div class="sidebar-nav">

        <!-- .User Profile -->
        <ul class="nav" id="side-menu">
            {{--<li class="user-pro">
                @if(is_null($user->image))
                    <a href="#" class="waves-effect"><img src="{{ asset('default-profile-3.png') }}" alt="user-img" class="img-circle"><br/><span class="hide-menu">{{ (strlen($user->name) > 24) ? substr(ucwords($user->name), 0, 20).'..' : ucwords($user->name) }}
                            <span class="fa arrow"></span></span>
                    </a>
                @else
                    <a href="#" class="waves-effect"><img src="{{ asset('user-uploads/avatar/'.$user->image) }}" alt="user-img" class="img-circle"><br/><span class="hide-menu">{{ ucwords($user->name) }}
                            <span class="fa arrow"></span></span>
                    </a>
                @endif

                <ul class="nav nav-second-level">
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                        ><i class="fa fa-power-off"></i> @lang('app.logout')</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>--}}

            <li><a href="{{ route('super-admin.dashboard') }}" class="waves-effect"><i class="icon-speedometer"></i><br/><span class="hide-menu">@lang('app.menu.dashboard') </span></a> </li>

            <li><a href="{{ route('super-admin.intro-slider.index') }}" class="waves-effect"><i class="icon-layers"></i><br/><span class="hide-menu">Intro Slider</span></a> </li>
           {{-- <li><a href="{{ route('super-admin.product-category.index') }}" class="waves-effect"><i class="icon-layers"></i><br/><span class="hide-menu">Product Category</span></a> </li>

            <li><a href="{{ route('super-admin.product-brand.index') }}" class="waves-effect"><i class="icon-layers"></i><br/><span class="hide-menu">Product Brands</span></a> </li>

            <li>
                <a href="#" class="waves-effect">
                    <i class="ti-layout-list-thumb"></i><br/>
                    <span class="hide-menu"> BOQ <span class="fa arrow"></span> </span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('super-admin.activity.index') }}">Activity</a></li>
                    <li><a href="{{ route('super-admin.task.index') }}">Task</a></li>
                </ul>
            </li>--}}

            <li>
                <a href="#" class="waves-effect">
                    <i class="ti-layout-list-thumb"></i><br/>
                    <span class="hide-menu"> Modules & Permissions <span class="fa arrow"></span> </span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('super-admin.manageModules.index') }}">Manage Modules</a></li>
                    <li><a href="{{ route('super-admin.manageModules.assignPermissions') }}">Assign Permissions</a></li>
                </ul>
            </li>

            <li><a href="{{ route('super-admin.profile.index') }}" class="waves-effect"><i class="icon-user"></i><br/><span class="hide-menu">@lang('modules.employees.profile') </span></a> </li>
            <li><a href="{{ route('super-admin.appNotification') }}" class="waves-effect"><i class="icon-user"></i><br/><span class="hide-menu">@lang('modules.employees.send_app_notification') </span></a> </li>
            <li><a href="{{ route('super-admin.promotionalNotification') }}" class="waves-effect"><i class="icon-user"></i><br/><span class="hide-menu">@lang('modules.employees.promotional') </span></a> </li>
            <li><a href="{{ route('super-admin.packages.index') }}" class="waves-effect"><i class="icon-calculator"></i><br/><span class="hide-menu">@lang('app.menu.packages') </span></a> </li>

            <li><a href="{{ route('super-admin.companies.index') }}" class="waves-effect"><i class="icon-layers"></i><br/><span class="hide-menu">@lang('app.menu.companies') </span></a> </li>
            <li><a href="{{ route('super-admin.invoices.index') }}" class="waves-effect"><i class="icon-printer"></i><br/><span class="hide-menu">@lang('app.menu.invoices') </span></a> </li>
            <li><a href="{{ route('super-admin.faq-category.index') }}" class="waves-effect"><i class="icon-docs"></i><br/><span class="hide-menu">@lang('app.menu.faq') </span></a> </li>
            <li><a href="{{ route('super-admin.super-admin.index') }}" class="waves-effect"><i class="fa fa-user-secret"></i><br/><span class="hide-menu">@lang('app.superAdmin') </span></a> </li>
            <li><a href="{{ route('super-admin.offline-plan.index') }}" class="waves-effect"><i class="fa fa-user-secret"></i><br/><span class="hide-menu">@lang('app.offlineRequest') @if($offlineRequestCount > 0)<div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</span> </a> </li>
            <li><a href="{{ route('super-admin.settings.index') }}" class="waves-effect"><i class="icon-settings"></i><br/><span class="hide-menu">@lang('app.menu.settings') </span></a> </li>


        </ul>
    </div>
</div>
