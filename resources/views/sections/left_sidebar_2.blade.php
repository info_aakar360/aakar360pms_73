<style>
    .sidescrollable {
        height: 1100%;
        overflow-y: auto;

    }

</style>

@if(isset($activeMenu) && isset($menus[$activeMenu]))
    <div class="navbar-default sidebar1 main-menu-shirnk1" >
        <div class="sidebar-nav">
            <!-- .User Profile -->
            <ul class="nav " id="side-menu">

                <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                    <!-- input-group -->
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                                <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                </span> </div>
                    <!-- /input-group -->
                </li>
                <!--li class="user-pro">
                    @if(is_null($user->image))
                        <a href="#" class="waves-effect"><img src="{{ asset('default-profile-3.png') }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ (strlen($user->name) > 24) ? substr(ucwords($user->name), 0, 20).'..' : ucwords($user->name) }}
                        <span class="fa arrow"></span></span>
                        </a>
                    @else
                        <a href="#" class="waves-effect"><img src="{{ asset('user-uploads/avatar/'.$user->image) }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ ucwords($user->name) }}
                                <span class="fa arrow"></span></span>
                        </a>
                    @endif
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('member.dashboard') }}">
                                <i class="fa fa-sign-in"></i> @lang('app.loginAsEmployee')
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"
                            ><i class="fa fa-power-off"></i> @lang('app.logout')</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li-->
                @if(!empty($hidemenu))
                    <li><a href="{{ route('admin.projects.selectProjects') }}" class="waves-effect"><i class="icon-people"></i><br><span class="hide-menu">Select Projects</span></a> </li>
                @else

                <?php
                    $count = count($menus[$activeMenu])-3;
                    $i =0;
                ?>
                @foreach($menus[$activeMenu] as $menu)
                    <?php $i++; ?>
                    @if(!isset($menu['submenu']))
                          <li><a href="{{ $menu['link'] }}" class="waves-effect"><i class="{{ !empty($menu['icon']) ? $menu['icon'] : 'icon-people' }}"></i><br><span class="hide-menu">{{ $menu['title'] }} </span></a> </li>
                    @else
                        <li><a href="{{ $menu['link'] }}" class="waves-effect "><i class="{{ !empty($menu['icon']) ? $menu['icon'] : 'ti-layout-list-thumb' }}"></i><br><span class="hide-menu"> {{ $menu['title'] }}</span> <span class="fa arrow"></span></a>
                            <?php $top = 0;
                            if($i >= $count){
                                $top1 = count($menu['submenu'])-1;
                                $top = intval($top1)*40;
                                    } ?>

                            <ul class="nav nav-second-level sidescrollable">
                                @foreach($menu['submenu'] as $submenu)
                                    <li><a href="{{ $submenu['link'] }}">{{ $submenu['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach

                @endif
            </ul>
        </div>
    </div>
@endif