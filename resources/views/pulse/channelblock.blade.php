<div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ get_users_image_link($user->id) }}" />

    <h6 class="float-left" style="margin: 0px; margin-left: 10px; margin-top: 10px;"> {{ ucwords($user->name) }}</h6>

    <div class="dropdown show">

        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

        <div class="dropdown-menu dropdown-menu-right border p-0" aria-labelledby="dropdownMenuLink">

            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>
            <hr class="my-1"></hr>
            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>

        </div>
    </div>
</div>
<div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">
    <div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent" style="background:url({{ asset_url('back-study-photo.png') }});overflow-y: auto; height: 100vh;background-size: contain">

        <div id="pulseloop">

        </div>
        <div id="loadingsymbol" style="display: block;">
            <p>Loading please wait..</p>
        </div>

    </div>
</div>

<div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">

    <form class="m-0 p-0" action="" method="POST" autocomplete="off">

        <div class="row m-0 p-0">
            <div class="col-9 m-0 p-1">

                <input id="text" class="mw-100 border rounded form-control" type="text" name="text" title="Type a message..." placeholder="Type a message..." required>

            </div>
            <div class="col-3 m-0 p-1">

                <button class="btn btn-outline-secondary rounded border w-100" title="Gönder!" style="padding-right: 16px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>

            </div>
        </div>

    </form>

</div>