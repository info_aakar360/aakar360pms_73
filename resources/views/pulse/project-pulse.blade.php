@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.manage-drawings.index') }}">{{ __($pageTitle) }}</a></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>

        .image_upload{
            position: absolute;
            top: 3px;
            right:3px;
        }
        .image_upload >form >input {
            display: none;
        }
        .image_upload img{
            width: 24px;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

                                    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ get_users_image_link($user->id) }}" />

                                    <h6 class="float-left" style="margin: 0px; margin-left: 10px; margin-top: 10px;"> {{ ucwords($user->name) }}</h6>

                                    <div class="dropdown show">

                                        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

                                        <div class="dropdown-menu dropdown-menu-right border p-0" aria-labelledby="dropdownMenuLink">

                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>
                                            <hr class="my-1"></hr>
                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>

                                        </div>
                                    </div>

                                </div>

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">
                                    <div id="sohleft" class="card border-0 m-0 p-0 position-relative bg-transparent" style="overflow-y: auto; height: 100vh;">

                                        <div class="panel-group" id="accordion">

                                            @include('layouts.pulsemenu')

                                        </div>


                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">

                                    <div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent" style="background:url({{ asset_url('back-study-photo.png') }});overflow-y: auto; height: 100vh;">

                                         <div id="pulseloop">

                                         </div>
                                         <div id="loadingsymbol" style="display: none;">
                                            <p>Loading please wait..</p>
                                         </div>

                                    </div>

                                    <div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">

                                        <div class="row send-chat-box">
                                            <div class="col-sm-12">

                                                <input type="text" name="message" id="submitTexts" autocomplete="off" placeholder="@lang("modules.messages.typeMessage")"
                                                       class="form-control ">
                                                <div class="form-group">

                                                    <div class="image_upload">
                                                        <form id="uploadImage" method="POST" >
                                                            <label for="uploadDoc"><img src="https://banner2.cleanpng.com/20180714/etx/kisspng-computer-icons-hyperlink-agriculture-silhouette-5b49e2945e6606.7405367315315687883867.jpg" ></label>
                                                            <input type="file" name="uploadDoc" id="uploadDoc"  accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf">
                                                            <label for="uploadFile"><img src="https://www.pngitem.com/pimgs/m/365-3654427_cloud-upload-icon-svg-hd-png-download.png" ></label>
                                                            <input type="file" name="uploadFile" id="uploadFile"  accept=".jpg,.png,video/mp4,video/x-m4v,video/*">

                                                        </form>
                                                    </div>

                                                </div>
                                                <input id="userID" value="{{ $user->id }}" type="hidden"/>
                                                <input id="conversationid"  type="hidden"/>
                                                <input id="projectid"  type="hidden"/>

                                                <div class="custom-send">
                                                    <button id="submitBtn" class="btn btn-danger btn-rounded" type="button">@lang("modules.messages.send")
                                                    </button>
                                                </div>
                                                <div id="errorMessage"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <script>
        function getProjectLogData(projectid) {
            $("#projectid").val(projectid);
            $(".chatonline li").removeClass('active');
            $("#projectpulse_"+projectid).addClass('active');
            loadPulseHtml(0);
        }
        function  loadPulseHtml(page) {
            var token = '{{ csrf_token() }}';
            var projectid = $("#projectid").val();
            if(projectid){
                $.ajax({
                    url: '{{route('projectpulse.data')}}',
                    type: "POST",
                    data: {
                        '_token' : token,
                        'projectid' : projectid,
                        'page' : page
                    },
                    beforeSend:function(){
                        $("#loadingsymbol").show();
                    },
                    success: function(response){
                        $("#loadingsymbol").hide();
                        if(page=='0'){
                            $('#pulseloop').html("");
                            $('#pulseloop').html(response);
                        }else{
                            $('#pulseloop').prepend(response);
                        }
                    }
                });
            }
        }
        $('#submitTexts').keypress(function (e) {

            var key = e.which;
            if (key == 13)  // the enter key code
            {
                e.preventDefault();
                $('#submitBtn').click();
                return false;
            }
        });

        //submitting message
        $('#submitBtn').on('click', function (e) {
            e.preventDefault();
            //getting values by input fields
            var submitText = $('#submitTexts').val();
            var dpID = $('#dpID').val();
            var conversationid = $('#conversationid').val();
            var uploadDoc = $('#uploadDoc').val();
            uploadDoc = uploadDoc.split("\\");
            uploadDoc = uploadDoc[uploadDoc.length - 1];
            var page = 0;
            var projectid = $("#projectid").val();
            var uploadFile = $('#uploadFile').val();
            uploadFile = uploadFile.split("\\");
            uploadFile = uploadFile[uploadFile.length - 1];

                var url = "{{ route('projectpulse.message-submit') }}";
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {'projectid': projectid, 'message': submitText, 'doc':uploadDoc,'image':uploadFile, 'conversationid': conversationid,  'user_id': dpID, '_token': token},
                     success: function (response) {
                         $('#submitTexts').val('');
                    }
                });
        });
    </script>
@endpush