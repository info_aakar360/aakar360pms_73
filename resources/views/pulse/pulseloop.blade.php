
<?php

if(!empty($projectsarray)){
    foreach ($projectsarray as $projectslog){
    ?>
<div class="pulse-block">
     <div class="col-md-1">
            <img class="img-responsive img-circle" src="{{ $projectslog->userimage }}" alt="{{  $projectslog->username }}'s image">
     </div>
     <div class="col-md-11">
         <div class="w-70 float-left">
            <div class="fnt-18">{{ $projectslog->username }}</div>
         </div>
         <div class="w-30 float-left">
             <div class="fnt-15 text-right">{{ $projectslog->datetime }}</div>
         </div>
         <div class="w-100 float-left">
         <div class="fnt-15"><b>{{ $projectslog->project_name }}</b></div>
         <div class="fnt-15">{{ ucwords(str_replace("_"," ",$projectslog->modulename)) }}</div>
         <div class="fnt-15">{{ $projectslog->heading }}</div>
             <div class="fnt-15">{{ $projectslog->description }}</div>
            </div>
            <?php
         if(!empty($projectslog->images)){
                foreach ($projectslog->images as $image){
                    ?>
                     <div class="col-md-2">
                        <a href="{{ $image['image'] }}" class="fancybox">
                            <img src="{{ $image['image'] }}" />
                        </a>
                     </div>
            <?php }  }?>
         <div class="col-md-12 text-right">
             <div class="fnt-15">
                 <?php
                 $link = '';
                 if(!empty($projectslog->manpowerid)){
                     $link = route('admin.man-power-logs.edit',[$projectslog->manpowerid]);
                 }elseif(!empty($projectslog->indentsid)){
                     $link = route('admin.indent.edit',[$projectslog->indentsid]);
                 }elseif(!empty($projectslog->taskid)){
                     $link = route('admin.all-tasks.updateTask',[$projectslog->task_id]);
                 }elseif(!empty($projectslog->issueid)){
                     $link = route('admin.issue.reply',[$projectslog->issueid]);
                 }
                 if($link){
                 ?>
                 <a href="{{ $link }}" target="_blank">View Comment <i class="fa fa-comment"></i></a>
                 <?php }?>
             </div>
         </div>
     </div>
</div>
<?php } }?>