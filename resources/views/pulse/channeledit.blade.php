<form method="post" action="{{ route('channels.updateChannel') }}" autocomplete="off" enctype="multipart/form-data">
  <!-- Modal Header -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Update Channel</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body">
            <p class="statusMsg"></p>
            <div class="form-group">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" name="name" value="{{ $channels->name }}" placeholder="Enter  Name"/>
            </div>
            <div class="form-group">
                <label for="inputName">Image</label>
                <input type="file"  name="image" class="dropzone" />
            </div>
        </div>
        <!-- Modal Footer -->
        <div class="modal-footer">
            {{ csrf_field() }}
            <input type="hidden" name="channelid" value="{{ $channels->id }}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" >SUBMIT</button>
        </div>
    </form>

<script>

    </script>