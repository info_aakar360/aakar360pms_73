<div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ $project->imageurl }}" />

    <h5 class="float-left" style="margin: 10px 0px 0px 10px;font-size: 15px;"> {{ !empty($project) ? ucwords($project->project_name) : 'NA' }}  </h5>

    <div class="dropdown show">

        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

        <div class="dropdown-menu dropdown-menu-right border" aria-labelledby="dropdownMenuLink">

            {{-- <a class="dropdown-item p-10 text-secondary" href="javascript:void(0);" onclick="createGroupChat();"> <i class="fa fa-user m-1" aria-hidden="true"></i> Create Group </a>
              <a class="dropdown-item p-10 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>
              <a class="dropdown-item p-10 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>--}}

        </div>
    </div>

</div>
<div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent" style="background-color: transparent;overflow-y: auto; height: 100vh;">


    <div id="pulseloop" >
        {!! $chatdata['chatdata'] !!}
    </div>
    <div id="loadingsymbol" style="display: none;">
        <p>Loading please wait..</p>
    </div>


</div>

<div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">

    <div class="row send-chat-box">
        <form method="post" id="submitBtn" enctype="multipart/form-data" autocomplete="off">
            <div class="col-md-12  mt-10 " id="filebox" style="display: none;">
                <div class="  dropzone dropheight"  id="file-upload-dropzone" >
                    {{ csrf_field() }}
                    <div class="fallback">
                        <input name="image[]" type="file" multiple />
                    </div>
                </div>
            </div>
            <div class="col-md-12  mt-10">
        <div class="col-sm-11">
            <div class="form-group">

                <input type="text" name="message" id="submitTexts" autocomplete="off" placeholder="@lang("modules.messages.typeMessage")"
                       class="form-control ">
                <div class="image_upload">
                    <a href="javascript:void(0);" class="uploadfile"><img src="{{ uploads_url().'cloud-upload-icon.png' }}" ></a>

                </div>

            <input id="conversationid"  name="conversationid"  type="hidden"/>
            <input id="projectlogid"  name="projectlogid"  type="hidden"/>
            <input id="projectid" value="{{ $project->id  }}"  name="projectid"  type="hidden"/>
            <input id="chatmodule" name="chatmodule" value="projectpulse" type="hidden"/>
            </div>

        </div>
        <div class="col-sm-1">
            <div class="custom-send">
                {{ csrf_field() }}
                <button  class="btn btn-danger btn-rounded" type="submit">@lang("modules.messages.send")
                </button>
            </div>
            <div id="errorMessage"></div>
        </div>
        </div>
        </form>
    </div>

</div>

<script>

    Dropzone.autoDiscover = true;
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.user-chat.storeImage') }}",
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        paramName: "image",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks: true,
        parallelUploads: 10,
        init: function () {
            let mydropzone = this; // closure

            this.on('success', function(file, response){

                $("#filebox").hide();
            });
        }
    });

    myDropzone.on('sending', function (file, xhr, formData) {
        var chatmodule = $('#chatmodule').val();
        var projectid = $('#projectid').val();
        var projectlogid = $('#projectlogid').val();
        formData.append('chatmodule', chatmodule);
        formData.append('projectid', projectid);
        formData.append('projectlogid', projectlogid);
    });
    myDropzone.on("complete", function(file) {
        myDropzone.removeFile(file);
    });
</script>