@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('channels.index') }}">{{ __($pageTitle) }}</a></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>

    </style>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

                                    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ get_users_image_link($user->id) }}" />

                                    <h6 class="float-left" style="margin: 0px; margin-left: 10px; margin-top: 10px;"> {{ ucwords($user->name) }}</h6>

                                    <div class="dropdown show">

                                        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

                                        <div class="dropdown-menu dropdown-menu-right border p-0" aria-labelledby="dropdownMenuLink">

                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>
                                            <hr class="my-1"></hr>
                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>

                                        </div>
                                    </div>

                                </div>

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">
                                    <div id="sohleft" class="card border-0 m-0 p-0 position-relative bg-transparent" style="overflow-y: auto; height: 100vh;">

                                        <div class="panel-group" id="accordion">

                                            @include('layouts.pulsemenu')

                                        </div>


                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" id="channelblock" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">


                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="createChannelModal">
        {!! Form::open(['id'=>'createChannel','class'=>'ajax-form','method'=>'POST','enctype'=>'multipart/form-data','autocomplete'=>'off']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Create Channel</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <div class="form-group">
                        <label for="inputName">Projects</label>
                        <select class="select2 form-control" data-placeholder="@lang("app.projects")" required  id="category" name="project_id">
                            <option value="">Select Project</option>
                            @foreach($projectsarray as $projects)
                                <option  value="{{ $projects->id }}"  >{{ ucwords($projects->project_name) }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Enter  Name"/>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Image</label>
                        <input type="file"  name="image" class="dropzone" />
                    </div>
                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >SUBMIT</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('footer-script')
    <script>
        $('#createChannelModal').submit(function () {
            $.easyAjax({
                url: '{{route('channels.store')}}',
                container: '#createChannelModal',
                type: "POST",
                data: $('#createChannel').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                         location.reload();
                    }
                }
            });
            return false;
        });
        function getChannelData(channelid) {
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('channels.channelBlock')}}',
                type: "POST",
                data: {
                    '_token' : token,
                    'channelid' : channelid
                },
                beforeSend:function(){
                    $("#loadingsymbol").show();
                },
                success: function(response){
                    $("#loadingsymbol").hide();
                    $('#pulseloop').prepend(response);
                }
            })
        }
        loadHtml(0);
        function  loadHtml(page) {
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('pulse.data')}}',
                type: "POST",
                data: {
                    '_token' : token,
                    'userid' : 1,
                    'page' : page
                },
                beforeSend:function(){
                    $("#loadingsymbol").show();
                },
                success: function(response){
                    $("#loadingsymbol").hide();
                    $('#pulseloop').prepend(response);
                }
            })
        }
    </script>
@endpush