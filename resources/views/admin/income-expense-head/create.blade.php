@extends('layouts.app')
<?php
$moduleName = "  Ledger Name";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Create";
$breadcrumbMainIcon = "fas fa-file-invoice-dollar";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\IncomeExpenseHead';
$ParentRouteName = 'admin.income_expense_head';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Put {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post" action="{{ route($ParentRouteName.'.store') }}">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Name</label>
                                            <input autofocus value="{{ old('name')  }}" name="name" type="text"  class="form-control">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Ledger Group</label>
                                            <select data-live-search="true" class="form-control  show-tick" name="income_expense_group_id" id="">
                                                <option value="0">Select Ledger Group</option>
                                                @foreach($groupsarray as $IncomeExpenseGroups )
                                                    <option @if ( $IncomeExpenseGroups->id == old('income_expense_group_id' ))
                                                            selected
                                                            @endif
                                                   value="{{ $IncomeExpenseGroups->id  }}">{{ $IncomeExpenseGroups->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Opening Balance</label>
                                            <input autocomplete="off" value="{{ old('amount') }}" name="amount"
                                                   type="number"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <label class="control-label">Date</label>
                                            <input autocomplete="off" value="{{ old('voucher_date') }}"
                                                   name="voucher_date" type="text"
                                                   class="form-control date-picker"
                                                   placeholder="Please choose a voucher date...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Particulars</label>
                                            <textarea class="form-control summernote" name="particulars">{{ old('particulars')  }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone dropheight"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url" type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="ledgerID" id="ledgerID">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="save-form">
                                            Create
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route($ParentRouteName.'.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });
        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#ledgerID').val();
            formData.append('ledger_id', ids);
        });

        myDropzone.on('completemultiple', function () {

            var msgs = "Ledger updated successfully";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route($ParentRouteName) }}'

        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $('#store-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.store') }}',
                container: '#form_validation',
                type: "POST",
                data: $('#form_validation').serialize(),
                success: function (data) {
                    $('#form_validation').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        ledgerID = data.ledgerID;
                        $('#ledgerID').val(data.ledgerID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.issueCreated')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route($ParentRouteName) }}'
                    }
                }
            })
        });

    </script>
@endpush