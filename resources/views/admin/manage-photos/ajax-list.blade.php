<li class="list-group-item">
    <div class="table-responsive">
        <table class="table table-stripped">
            <thead>
            <th>Name</th>
            <th>Type</th>
            <th>Size</th>
            <th>Last Modified</th>
            <th>Action</th>
            </thead>
            <tbody>
            @forelse($files as $file)
                @php
                    $path = $file->id;
                @endphp
                <tr>
                    <td>
                        @if($file->type == 'file')
                            <i class="ti-file"></i> {{ $file->filename }}
                        @else
                            <a href="{{url('admin/manage-files/manage-files')}}?path={{$path}}">
                                <i class="ti-folder"></i> {{ $file->filename }}
                            </a>
                        @endif
                    </td>
                    <td>{{ $file->type }}</td>
                    <td>{{ $file->size }} Bytes</td>
                    <td><span class="m-l-10">{{ date("d-m-Y", strtotime($file->created_at)) }}</span></td>
                    <td>
                        @if($file->type == 'file')
                            @if($file->external_link != '')
                                <a target="_blank" href="{{ $file->external_link }}"
                                   data-toggle="tooltip" data-original-title="View"
                                   class="btn btn-info btn-circle"><i
                                            class="fa fa-search"></i></a>
                            @elseif(config('filesystems.default') == 'local')
                                <a target="_blank" href="{{ asset('user-uploads/project-files/'.$file->pid.'/'.$file->hashname) }}"
                                   data-toggle="tooltip" data-original-title="View"
                                   class="btn btn-info btn-circle"><i
                                            class="fa fa-search"></i></a>

                            @elseif(config('filesystems.default') == 's3')
                                <a target="_blank" href="{{ $url.'project-files/'.$file->pid.'/'.$file->filename }}"
                                   data-toggle="tooltip" data-original-title="View"
                                   class="btn btn-info btn-circle"><i
                                            class="fa fa-search"></i></a>
                            @elseif(config('filesystems.default') == 'google')
                                <a target="_blank" href="{{ $file->google_url }}"
                                   data-toggle="tooltip" data-original-title="View"
                                   class="btn btn-info btn-circle"><i
                                            class="fa fa-search"></i></a>
                            @elseif(config('filesystems.default') == 'dropbox')
                                <a target="_blank" href="{{ $file->dropbox_link }}"
                                   data-toggle="tooltip" data-original-title="View"
                                   class="btn btn-info btn-circle"><i
                                            class="fa fa-search"></i></a>
                            @endif

                            @if(is_null($file->external_link))
                                &nbsp;&nbsp;
                                <a href="{{ route('admin.files.download', $file->id) }}"
                                   data-toggle="tooltip" data-original-title="Download"
                                   class="btn btn-inverse btn-circle"><i
                                            class="fa fa-download"></i></a>
                            @endif
                        @endif
                        @if($file->type == 'folder')
                            <a href="javascript:;" onclick="editModal({{ $file->id }}, '{{ $file->filename }}');"
                               data-toggle="tooltip" data-original-title="Rename"
                               class="btn btn-info btn-circle"><i
                                        class="fa fa-pencil"></i></a>
                        @endif

                        <a href="javascript:;" data-toggle="tooltip"
                           data-original-title="Delete"
                           data-file-id="{{ $file->id }}"
                           class="btn btn-danger btn-circle sa-params" data-pk="list"><i
                                    class="fa fa-times"></i></a></td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">@lang('messages.noFileUploaded')</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</li>