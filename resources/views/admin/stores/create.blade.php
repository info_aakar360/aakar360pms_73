@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.stores.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.stores.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createSupplier','class'=>'ajax-form','method'=>'POST']) !!}
                        @if(isset($leadDetail->id))
                            <input type="hidden" name="lead" value="{{ $leadDetail->id }}">
                        @endif
                            <div class="form-body">
                                <h3 class="box-title">@lang('modules.stores.companyDetails')</h3>
                                <hr>
                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Project (*)</label>
                                            <select class="form-control select2" name="project_id" id="store" data-style="form-control" required>
                                                <option value="">Select Project</option>
                                                @forelse($projects as $supplier)

                                                    <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.stores.companyName') (*)</label>
                                            <input type="text" id="company_name" name="company_name" value="" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('app.address')</label>
                                            <textarea name="address"  id="address"  rows="5"  class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">@lang('app.address')</label>--}}
                                            {{--<textarea name="address"  id="address"  rows="5"  class="form-control"></textarea>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="gst_number">@lang('app.gstNumber')</label>--}}
                                            {{--<input type="text" id="gst_number" name="gst_number" class="form-control" value="">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!--/span-->--}}
                                {{--</div>--}}
                                <!--/row-->

                                {{--<h3 class="box-title m-t-40">@lang('modules.stores.supplierDetails')</h3>--}}
                                {{--<hr>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6 ">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label>@lang('modules.stores.supplierName') (*)</label>--}}
                                            {{--<input type="text" name="name" id="name"  value=""   class="form-control" required>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label>@lang('modules.stores.supplierEmail')</label>--}}
                                            {{--<input type="email" name="email" id="email" value=""  class="form-control">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!--/span-->--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label>@lang('modules.stores.mobile')</label>--}}
                                            {{--<input type="tel" name="mobile" id="mobile" value="" class="form-control">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--<label>@lang('app.note')</label>--}}
                                        {{--<div class="form-group">--}}
                                            {{--<textarea name="note" id="note" class="form-control" rows="5"></textarea>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!--/span-->--}}
                                {{--</div>--}}
                                <!--/row-->


                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.stores.store')}}',
            container: '#createSupplier',
            type: "POST",
            redirect: true,
            data: $('#createSupplier').serialize()
        })
    });
</script>
@endpush

