                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $percenttype = $boqarray['percenttype'];
                                $percentvalue = $boqarray['percentvalue'];
                                $invoice = $boqarray['invoice'];
                                $invoicepaymentid = $boqarray['invoicepaymentid'];
                                $projectid = $boqarray['projectid'];
                                $subprojectid = $boqarray['subprojectid'];
                                $proproget = $boqarray['categoryarray'];
                                $level = $boqarray['level'];
                                $parent = $boqarray['parent'];
                                $snorow = $boqarray['snorow'];

                                foreach ($proproget as $propro){

                                $level = (int)$propro->level;
                                $newlevel = $level+1;
                                $parent = $propro->id;

                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }

                                $proprogetdatarra = \App\AwardContractProducts::join('contract_invoices_boq','contract_invoices_boq.product_id','=','awarded_contracts_products.id')
                                                    ->join('cost_items','cost_items.id','=','awarded_contracts_products.cost_items')
                                                    ->select('contract_invoices_boq.*','cost_items.cost_item_name')
                                                    ->where('contract_invoices_boq.invoice_id',$invoice->id)->where('awarded_contracts_products.awarded_category',$propro->id)->orderBy('awarded_contracts_products.inc','asc')->get();

                                if(count($proprogetdatarra)>0){
                                ?>
                                    <tr >
                                        <td align="left">{{ $snorow }}</td>
                                        <td>{{ get_dots_by_level($propro->level)  }}<?php if (isset($propro->category)){ echo get_cat_name($propro->category); } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                $tid = 1;  foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                                    $totalamt = $proprogetdat->amount;
                                    $paidamount = $paidpercent= 0;
                               $previnvoice = \App\ContractInvoicePaymentsboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
                                    \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"),
                                ])->where('invoice_boq_id',$proprogetdat->id);
                                if(!empty($invoicepaymentid)){
                                    $previnvoice =    $previnvoice->where('invoice_payments_id',$invoicepaymentid);
                                }
                                $previnvoice = $previnvoice->first();
                                if(!empty($previnvoice->invpercent)&&!empty($previnvoice->invamount)){
                                    $paidamount = $previnvoice->invamount;
                                    $paidpercent = $previnvoice->invpercent;
                                }
                                    $remaining = $totalamt-$paidamount;
                                    $thisinvoicepercent = 100-$paidpercent;
                                    $thisinvoiceamt = $totalamt-$paidamount;
                                ?>
                                <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td></td>
                                    <td><input type="checkbox" name="taskid[]" checked value="{{ $proprogetdat->id }}"> {{  $proprogetdat->cost_item_name }}</td>
                                    <td>₹{{ $totalamt }}</td>
                                    <td>₹{{ $paidamount }}</td>
                                    <td>₹{{ $remaining }}</td>
                                    <?php if($percenttype=='percent'){
                                    $thisinvoiceamt = ($percentvalue / 100) * $totalamt;
                                        ?>
                                    <td>{{ $percentvalue }}%</td>
                                    <td>₹{{ $thisinvoiceamt }} <input type="hidden" class="invrowamt" value="{{ $thisinvoiceamt }}" />
                                    <?php }elseif($percenttype=='custom'){
                                        ?>
                                    <td>
                                        <input type="text" placeholder="10 %" name="invoiceboqpercent[{{ $proprogetdat->id }}]"  class="calinvrow invrowpercent{{ $proprogetdat->id }}" data-type="percent" data-totalamt="{{ $remaining }}" data-taskid="{{ $proprogetdat->id }}" value="">
                                    </td>
                                    <td>
                                        <input type="text" name="invoiceboqamt[{{ $proprogetdat->id }}]" class="calinvrow invrowamt invrowamt{{ $proprogetdat->id }}" data-type="amount" data-totalamt="{{ $remaining }}" data-taskid="{{ $proprogetdat->id }}" value="{{ $thisinvoiceamt }}" />
                                    </td>
                                    <?php }else{?>
                                    <td>{{ $thisinvoicepercent }}%</td>
                                    <td>₹{{ $thisinvoiceamt }} <input type="hidden" class="invrowamt" value="{{ $thisinvoiceamt }}" />
                                    <?php }?>
                                    </td>
                                </tr>
                                <?php
                                $tid++;
                                } }

                                }
                                $newlevel = $level+1;
                                $newparent = $propro->id;
                                $proproget = \App\AwardContractCategory::where('awarded_contract_id',$invoice->awarded_contract_id)->where('level',$newlevel)->where('parent',$newparent)->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['percenttype'] = $percenttype;
                                $boqarray['percentvalue'] = $percentvalue;
                                $boqarray['invoice'] = $invoice;
                                $boqarray['invoicepaymentid'] = $invoicepaymentid;
                                $boqarray['projectid'] = $projectid;
                                $boqarray['subprojectid'] = $subprojectid;
                                $boqarray['categoryarray'] = $proproget;
                                $boqarray['level'] = $newlevel;
                                $boqarray['parent'] = $newparent;
                                $boqarray['snorow'] = $snorow;
                                echo  boqhtml($boqarray);

                                } }
                                $invoice = \App\ContractInvoice::where('id',$invoiceid)->first();
                                $proproget = \App\AwardContractCategory::where('awarded_contract_id',$invoice->awarded_contract_id)->where('level',0)->where('parent',0)->orderBy('inc','asc')->get();

                                $project = $invoice->project_id ?: 0;
                                $subproject = $invoice->subproject_id ?: 0;
                                if($percentvalue>100){
                                    $percentvalue = 100;
                                }
                                $boqarray = array();
                                $boqarray['percenttype'] = $percenttype;
                                $boqarray['percentvalue'] = $percentvalue;
                                $boqarray['invoice'] = $invoice;
                                $boqarray['projectid'] = $project;
                                $boqarray['subprojectid'] = $subproject;
                                $boqarray['categoryarray'] = $proproget;
                                $boqarray['invoicepaymentid'] = $invoicepaymentid;
                                $boqarray['level'] = 0;
                                $boqarray['parent'] = 0;
                                $boqarray['snorow'] = 0;
                              echo  boqhtml($boqarray);
                                ?>