@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.invoices.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .dropdown-content {
            width: 250px;
            max-height: 250px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.invoices.addInvoice')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><strong>@lang('app.invoice') #</strong></label>
                                        <div>
                                            {{ $invoice->unique_id }}
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group" >
                                        <label class="control-label">@lang('app.project')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="select2 form-control" data-placeholder="Choose Project" id="projectid" name="project_id">
                                                    <option value="">--</option>
                                                    @foreach($projectsarray as $project)
                                                        <option
                                                                value="{{ $project->id }}" @if($invoice->project_id==$project->id) selected @endif >{{ ucwords($project->project_name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <?php  if(in_array('sub_projects', $user->modules)){?>
                                <div class="col-sm-4 col-xs-4" id="subprojectblock" style="display: @if(count($subprojectsarray)<=0)) none @else block @endif;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.subproject')</label>
                                        <select class="select2 form-control"  data-placeholder="Choose Sub Project" id="subprojectid" name="subproject_id">
                                            <option value="">-Select Sub project-</option>
                                            @foreach($subprojectsarray as $subproject)
                                                <option  value="{{ $subproject->id }}" @if($invoice->subproject_id==$subproject->id) selected @endif >{{ ucwords($subproject->title) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <?php  if(in_array('segment', $user->modules)){?>
                                <div class="col-sm-4 col-xs-4 segmentblock"  style="display: none;">
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.segment')</label>
                                        <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                            <option value="">Select @lang('app.segment')</option>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" >@lang('app.contractor')</label>
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <select class="select2 form-control"  data-placeholder="Choose Contractor" id="contractor" name="contractor">
                                                    <option value="">-Select Contractor-</option>
                                                    @foreach($contractorsarray as $contractor)
                                                        <option value="{{ $contractor->user_id }}" @if($invoice->contractor_id==$contractor->user_id) selected @endif>{{ ucwords($contractor->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" >@lang('app.menu.awardedcontracts')</label>
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <select class="select2 form-control"  data-placeholder="Choose Contractor" id="awardedcontractor" name="awarded_contract">
                                                    <option value="">-Select Awarded Contract-</option>
                                                    @foreach($awardedcontractsarray as $contractor)
                                                        <option value="{{ $contractor->id }}" @if($invoice->awarded_contract_id==$contractor->id) selected @endif>{{ ucwords($contractor->title) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3">

                                    <div class="form-group" >
                                        <label class="control-label">@lang('modules.invoices.invoiceDate')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control" name="issue_date" id="issue_date" value="{{ !empty($invoice->issue_date) ? date('Y-m-d',strtotime($invoice->issue_date)) : '' }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="due_date" id="due_date" value="{{ !empty($invoice->issue_date) ? date('Y-m-d',strtotime($invoice->due_date)) : '' }}" />
                                        </div>
                                    </div>

                                </div>

                            </div>

                            {{--<div class="row">
                                <div class="col-md-3">

                                    <div class="form-group" >
                                        <label class="control-label">@lang('modules.invoices.isRecurringPayment') </label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control" name="recurring_payment" id="recurring_payment" onchange="recurringPayment()">
                                                    <option value="no">@lang('app.no')</option>
                                                    <option value="yes">@lang('app.yes')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingFrequency')</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <select class="form-control" name="billing_frequency" id="billing_frequency">
                                                    <option value="day">@lang('app.day')</option>
                                                    <option value="week">@lang('app.week')</option>
                                                    <option value="month">@lang('app.month')</option>
                                                    <option value="year">@lang('app.year')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingInterval')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="billing_interval" id="billing_interval" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 recurringPayment" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.invoices.billingCycle')</label>
                                        <div class="input-icon">
                                            <input type="text" class="form-control" name="billing_cycle" id="billing_cycle" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>--}}

                            <hr />
                            {{--<div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>How Much do you want to invoice</strong>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="invoicepercenttype" @if($invoice->percenttype=='all') checked @endif class="invoicepercenttype" value="all" > @lang('modules.invoices.remainingalltasks') </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="invoicepercenttype" @if($invoice->percenttype=='percent') checked @endif class="invoicepercenttype"  value="percent" > <input type="text" onkeyup="boqinfo();" placeholder="%" name="invoicepercent" class="invoicepercent" value="{{ $invoice->percentvalue }}" > @lang('modules.invoices.ofalltasks')
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" name="invoicepercenttype" @if($invoice->percenttype=='custom') checked @endif class="invoicepercenttype"  value="custom" > @lang('modules.invoices.customalltasks')
                                    </div>
                                </div>
                            </div>
                            <hr />--}}

                            <div class="row">
                                <div class="col-md-12">
                                <div class="table-responsive">
                                    <table  class="table table2 table-bordered  table-center default footable-loaded footable">
                                        <thead>
                                        <tr>
                                            <th colspan="5">Description</th>
                                            <th colspan="2" align="center">Percentage</th>
                                            <th colspan="3" align="center">Quantities</th>
                                            <th colspan="2" align="center">Amount</th>
                                        </tr>
                                        <tr>
                                            <th>@lang('app.activity')</th>
                                            <th>@lang('app.task')</th>
                                            <th>@lang('app.units')</th>
                                            <th>@lang('app.quantity')</th>
                                            <th>@lang('app.rate')(₹)</th>
                                            <th>Invoiced to date (%)</th>
                                            <th>This Invoice %</th>
                                            <th>Up to last bill</th>
                                            <th>This bill</th>
                                            <th>Total</th>
                                            <th>Up to last bill(₹)</th>
                                            <th>This bill (₹)</th>
                                        </tr>
                                        </thead>
                                        <tbody id="invoiceboq">

                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            <div class="row">



                                <div class="col-xs-12 ">


                                    {{--<div class="row">
                                        <div class="col-md-offset-9 col-xs-6 col-md-1 text-right p-t-10" >@lang('modules.invoices.subTotal')</div>

                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="sub-total">0.00</span>
                                        </p>


                                        <input type="hidden" class="sub-total-field" name="sub_total" value="0">
                                    </div>--}}

                                    {{--<div class="row">
                                        <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                            @lang('modules.invoices.discount')
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <input type="number" min="0" value="0" name="discount_value" class="form-control discount_value">
                                        </div>
                                        <div class="form-group col-xs-6 col-md-1" >
                                            <select class="form-control" name="discount_type" id="discount_type">
                                                <option value="percent">%</option>
                                                <option value="fixed">@lang('modules.invoices.amount')</option>
                                            </select>
                                        </div>
                                    </div>--}}

                                    {{-- <div class="row m-t-5" id="invoice-taxes">
                                         <div class="col-md-offset-9 col-md-1 text-right p-t-10">
                                             @lang('modules.invoices.tax')
                                         </div>

                                         <p class="form-control-static col-xs-6 col-md-2" >
                                             <span class="tax-percent">0.00</span>
                                         </p>
                                     </div>--}}

                                    <div class="row m-t-5 font-bold">
                                        <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>

                                        <p class="form-control-static col-xs-6 col-md-2" >
                                            <span class="total">{{ $invoice->total }}</span>
                                        </p>


                                        <input type="hidden" class="total-field" name="total" value="{{ $invoice->total }}">
                                    </div>

                                </div>

                            </div>


                            <div class="col-md-12">

                                <div class="form-group" >
                                    <label class="control-label">@lang('app.note')</label>
                                    <textarea class="form-control" name="note" id="note" rows="5">{{ $invoice->note }}</textarea>
                                </div>

                            </div>

                        </div>
                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <input type="hidden" name="invoiceid" id="invoiceid" value="{{ $invoice->id }}">
                                    {{ csrf_field() }}
                                    <button type="button" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="rate-sheet rate-formula-sheet">
                <div class="loaderx">
                    <div class="cssload-speeding-wheel"></div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taxModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    @lang('app.loading')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn blue">@lang('app.save') @lang('changes')</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

    <script>
        jQuery('#due_date, #issue_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $("#projectid").change(function () {
            var id = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id},
                success: function(data){
                    $("#subprojectblock").hide();
                    $("#segmentsblock").hide();
                    $("select#subprojectlist").html("");
                    $("select#segmentslist").html("");
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.subprojectlist){
                        $("#subprojectblock").show();
                        $("select#subprojectlist").html(data.subprojectlist);
                        $('select#subprojectlist').select2();
                    }
                }
            });
        });
        $("#contractor").change(function () {
            var project = $("#projectid").val();
            var subproject = $("#subprojectid").val();
            var contractor = $(this).val();
            var subprojecttext = '';
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.contract-invoices.get-awarded-contracts') }}",
                    data: {'_token': token,'projectid': project,'subprojectid': subproject,'contractorid': contractor},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function(projectdata){
                        $(".preloader-small").hide();
                        $(".subprojectdiv").addClass('hide');
                        var titles = '<option value="">Select Contracts</option>';
                        $.each( projectdata.contracts, function( key, value ) {
                            titles += '<option value="'+key+'">'+value+'</option>';
                        });
                        $("select#awardedcontractor").html("");
                        $("select#awardedcontractor").html(titles);
                    }
                });
            }
        });
        boqinfo();
        $("#awardedcontractor").change(function () {
            boqinfo();
        });
        function boqinfo() {

            var project = $("#projectid").val();
            var subprojectid = $("#subprojectid").val();
            var contractorid = $("#contractor").val();
            var awardedcontract = $("#awardedcontractor").val();
            var invoiceid = $("#invoiceid").val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.contract-invoices.boqinfo') }}",
                    data: {'_token': token,'invoiceid': invoiceid,'awardedcontract': awardedcontract},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function(data){
                        $(".preloader-small").hide();
                        var totalamt = 0;
                        $("#invoiceboq").html("");
                        $("#invoiceboq").html(data);
                        $(".invrowamt").each(function () {
                            var price = $(this).text();
                            price = price.replace('₹','');
                            if(price){
                                totalamt += parseFloat(price);
                            }
                        });
                        $(".total").html("₹"+totalamt.toFixed(2));
                        $(".total-field").val(totalamt);
                    }
                });
            }
        }
        $("body").on('change',".calinvrow",function(){
            var calinvpercent = $(this).val();
            var type = $(this).data('type');
            var caltotalamt = $(this).data('totalamt');
            var caltaskid = $(this).data('taskid');
            if(type=='percent'){
                var percent = Math.round(((calinvpercent / 100) * caltotalamt));
                $(".invrowamt"+caltaskid).val(percent);
            }
            if(type=='amount'){
                var percent = Math.round(((calinvpercent / caltotalamt) * 100));
                $(".invrowpercent"+caltaskid).val(percent);
            }
            var totalamt = 0;
            $(".invrowamt").each(function () {
                var price = $(this).val();
                if(price){
                    totalamt += parseFloat(price);
                }
            });
            $(".total").html("₹"+totalamt.toFixed(2));
            $(".total-field").val(totalamt);
        });

        function calcTotal(prid){
            var calqty = 0;
            var qty = $(".qtysheet"+prid).val();
            var costitem = $("#costitem"+prid);
            var invoicetoqut = parseFloat(costitem.attr('data-invoicetodatequat'));
            var invoicetoamt = parseFloat(costitem.attr('data-invoicetodateamt'));
            var rateval = parseFloat(costitem.attr('data-rate'));
            var qtyval = parseFloat(costitem.attr('data-qty'));


            $.ajax({
                url: '{{route('admin.contract-invoices.amtcalculation')}}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}','qty':qty,'invoicetoqut':invoicetoqut,'rateval':rateval,'qtyval':qtyval},
                success: function(data){
                    $(".qtysheet"+prid).val(data.qty);
                    $(".qtytotal"+prid).html(data.qtytotal);
                    $(".amttotal"+prid).html("₹"+data.totalamount);
                    $(".prototal"+prid).html("₹"+data.amount);
                    $(".percenttotal"+prid).val(data.percentage.toFixed(2));
                    var totalamt = 0;
                    $(".invrowamt").each(function () {
                        var price = $(this).text();
                        price = price.replace('₹','');
                        if(price){
                            totalamt += parseFloat(price);
                        }
                    });
                    $(".total").html("₹"+totalamt.toFixed(2));
                    $(".total-field").val(totalamt);
                }
            });

        }

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.contract-invoices.update',$invoice->id )}}',
                container: '#storePayments',
                type: "PUT",
                data: $('#storePayments').serialize(),
                success: function(response){

                }
            })

        });
        var cTable = null;
        $(document).on('click', '.open-measurement-sheet', function(){
            var id = $(this).data('id');
            var invoiceid = $(this).data('invoiceid');
            var url = '{{ route('admin.contract-invoices.measurement-sheet', [':id',':invoiceid']) }}';
            url = url.replace(':id', id);
            url = url.replace(':invoiceid', invoiceid);
            $.ajax({
                url: url,
                type: 'GET',
                data: {_token: '{{ csrf_token() }}'},
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
                        '                        <div class="cssload-speeding-wheel"></div>' +
                        '                    </div>';
                    $('.rate-formula-sheet').html(html).show();
                },
                success: function(data){
                    $('.rate-formula-sheet').html(data);
                }
            })
        });
        $(document).on('click', '.close-rate-sheet', function(){
            $('.rate-formula-sheet').html('').hide();
        });
        $("#invoiceboq").on('keydown', '.numbervalid', function(evt){
            var charCode =  (evt.which) ? evt.which : evt.keyCode;
            if ((charCode > 47 && charCode < 58 ) || (charCode > 95 && charCode < 107 ) || charCode == 110 || charCode == 190 || charCode == 8|| charCode == 9 )
                return true;
            return false;
        });
    </script>
@endpush

