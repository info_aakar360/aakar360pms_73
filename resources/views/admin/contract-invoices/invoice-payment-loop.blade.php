                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $invoice = $boqarray['invoice'];
                                $projectid = $boqarray['projectid'];
                                $subprojectid = $boqarray['subprojectid'];
                                $proproget = $boqarray['categoryarray'];
                                $level = $boqarray['level'];
                                $parent = $boqarray['parent'];
                                $snorow = $boqarray['snorow'];

                                foreach ($proproget as $propro){

                                $level = (int)$propro->level;
                                $newlevel = $level+1;
                                $parent = $propro->id;

                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }
                                $proprogetdatarra = \App\AwardContractProducts::join('contract_invoices_boq','contract_invoices_boq.product_id','=','awarded_contracts_products.id')
                                                    ->join('cost_items','cost_items.id','=','awarded_contracts_products.cost_items')
                                                    ->select('contract_invoices_boq.*','awarded_contracts_products.qty','awarded_contracts_products.rate','awarded_contracts_products.finalrate','cost_items.cost_item_name')
                                                    ->where('contract_invoices_boq.invoice_id',$invoice->id)->where('awarded_contracts_products.awarded_category',$propro->id)->orderBy('awarded_contracts_products.inc','asc')->get();

                                if(count($proprogetdatarra)>0){
                                ?>
                                    <tr >
                                        <td align="left">{{ $snorow }}</td>
                                        <td>{{ get_dots_by_level($propro->level)  }}<?php if (isset($propro->category)){ echo get_cat_name($propro->category); } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                $tid=1; foreach ($proprogetdatarra as $item){
                                if(!empty($item->id)){
                                $totalamt = $item->finalrate;
                                $invoicepaidboq = \App\ContractInvoicePaymentsboq::where('invoice_boq_id',$item->id)->sum('amount');
                                ?>
                                <tr>
                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td>{{ ucfirst($item->cost_item_name) }}</td>
                                    <td>{{ $item->qty }}</td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $item->rate }}</td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $totalamt }}</td>
                                    <td> {!! htmlentities($invoice->currency_symbol)  !!}{{ $item->amount }} </td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $invoicepaidboq }}</td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $item->amount-$invoicepaidboq }}</td>
                                </tr>
                                <?php
                                $tid++;  } }

                                }
                                $newlevel = $level+1;
                                $newparent = $propro->id;
                                $proproget = \App\AwardContractCategory::where('awarded_contract_id',$invoice->awarded_contract_id)->where('level',$newlevel)->where('parent',$newparent)->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['invoice'] = $invoice;
                                $boqarray['projectid'] = $projectid;
                                $boqarray['subprojectid'] = $subprojectid;
                                $boqarray['categoryarray'] = $proproget;
                                $boqarray['level'] = $newlevel;
                                $boqarray['parent'] = $newparent;
                                $boqarray['snorow'] = $snorow;
                                echo  boqhtml($boqarray);

                                } }
                                $invoice = \App\ContractInvoice::where('id',$invoiceid)->first();
                                $proproget = \App\AwardContractCategory::where('awarded_contract_id',$invoice->awarded_contract_id)->where('level',0)->where('parent',0)->orderBy('inc','asc')->get();

                                $project = $invoice->project_id ?: 0;
                                $subproject = $invoice->subproject_id ?: 0;
                                $boqarray = array();
                                $boqarray['invoice'] = $invoice;
                                $boqarray['projectid'] = $project;
                                $boqarray['subprojectid'] = $subproject;
                                $boqarray['categoryarray'] = $proproget;
                                $boqarray['level'] = 0;
                                $boqarray['parent'] = 0;
                                $boqarray['snorow'] = 0;
                              echo  boqhtml($boqarray);
                                ?>