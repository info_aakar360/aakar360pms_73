@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="form-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Meeting Name</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ $meeting->title }}">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Meeting Date</label>
                                <input type="text" name="meeting_date" id="due_date2" class="form-control" autocomplete="off" value="{{ $meeting->meeting_date }}">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Start Time</label>
                                <input type="time" name="start_time" id="start_time" class="form-control" value="{{ $meeting->start_time }}">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Finish Time</label>
                                <input type="time" name="finish_time" id="finish_time" class="form-control" value="{{ $meeting->finish_time }}">
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Meeting Location</label>
                                <input type="text" name="meeting_location" id="meeting_location" class="form-control" value="{{ $meeting->meeting_location }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                <select class="selectpicker form-control" name="assigned_user[]" id="user_id" multiple>
                                    <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                    @foreach($employees as $employee)
                                        <option value="{{ $employee->user_id }}"<?php
                                            if (!empty($assignedusers)&&in_array($employee->user_id, $assignedusers)){
                                                echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Private Meeting</label><br>
                                <input type="checkbox" name="is_private" value="1" id="title" class="form-check-input" @if($meeting->is_private == '1') checked @endif>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Draft Meeting</label><br>
                                <input type="checkbox" name="is_draft" value="1" id="title" class="form-check-input" @if($meeting->is_draft == '1') checked @endif>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Meeting Overview</label>
                                <textarea id="description" name="meeting_overview" class="form-control summernote">{{ $meeting->meeting_overview }}</textarea>
                            </div>
                        </div>
                        @if(count($meetingFiles)>0)
                        <div class="col-md-12">
                            <b>Files</b>
                            <br>
                            <div class="row" id="list">
                                @foreach($meetingFiles as $file)
                                    <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                        @if($file->external_link != '')
                                            <?php $imgurl = $file->external_link;?>
                                        @elseif($storage == 'local')
                                            <?php $imgurl = uploads_url().'meeting-detail-files/'.$meeting->id.'/'.$file->hashname;?>
                                        @elseif($storage == 's3')
                                            <?php $imgurl = awsurl().'/meeting-detail-files/'.$meeting->id.'/'.$file->hashname;?>
                                        @elseif($storage == 'google')
                                            <?php $imgurl = $file->google_url;?>
                                        @elseif($storage == 'dropbox')
                                            <?php $imgurl = $file->dropbox_link;?>
                                        @endif
                                        {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                        <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                        <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                            Remove
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        <div class="row m-b-20">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box">
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone dropheight" id="file-upload-dropzone">
                                                {{ csrf_field() }}
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url"type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="meetingID" id="meetingID">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right">
                    <input  type="hidden" name="meeting_id" value="{{ $meeting->id }}">
                    <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.meetings.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#meetingID').val();
            formData.append('meeting_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Meeting Updated";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.meetings.index') }}'

        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $('#createBoqCategory').submit(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.updateMeeting')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        $('.summernote').summernote('code', '');
                        if(myDropzone.getQueuedFiles().length > 0){
                            meetingID = response.meetingID;
                            $('#meetingID').val(response.meetingID);
                            myDropzone.processQueue();
                        }
                        else{
                            var msgs = "Meeting Created";
                            $.showToastr(msgs, 'success');
                            window.location.href = '{{ route('admin.meetings.index') }}'
                        }
                    }
                }
            })
            return false;
        })

        $('#save-category').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.updateMeeting')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        meetingID = response.meetingID;
                        $('#meetingID').val(response.meetingID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "Meeting Updated";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.meetings.index') }}'
                    }
                }
            })
        });

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}'
        });
    </script>
@endpush