<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<link href="{{ asset('css/colors/default.css') }}" id="theme" rel="stylesheet">
<link href="{{ asset('plugins/froiden-helper/helper.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<style>
    .icon-bar {
        width: 55%;
        overflow: auto;
        padding-bottom: 10px;
        float: right;
    }
    .icon-bar a {
        float: right;
        width: 24%;
        text-align: center;
        padding: 8px 0;
        transition: all 0.3s ease;
        color: black;
        font-size: 15px;
    }
    .icon-bar a:hover {
        background-color: #002f76;
        color: white;
    }

    .btn-blue, .btn-blue.disabled {
        background: #002f76;
        border: 1px solid #002f76;
        margin-right: 5px;
    }

    /*div .icon-bar a.active {*/
    /*background-color: #002f76;*/
    /*color: white;*/
    /*}*/

    .btn-blue.btn-outline {
        color: #002f76;
        background-color: transparent;
    }

    .btn {
        padding: 6px !important;
    }
</style>
<style>
    .panel-black .panel-heading a, .panel-inverse .panel-heading a {
        color: unset!important;
    }
    .card-header {
        background: #efefef;
        padding: 10px;
    }
    .card-body {
        padding: 10px;
    }

    .note-editable .panel-body{
        height: 100px;
    }
</style>
<?php
$data = '
<div class="row"><img src="'.$global->logo().'" class="admin-logo"  alt="home" style="width: 150px;" /></div>
<div class="row">
        <div class="col-md-12">
            <div class="panel-heading"><h3> '.$meeting->title.'</h3></div>
            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                <tr>
                    <td>Meeting Date :</td>
                    <td>Start Time :</td>
                </tr>
                <tr>
                    <td><b>'.\Carbon\Carbon::parse($meetingDetail->meeting_date)->format('d M, Y').'</b></td>
                    <td><b>'.$meetingDetail->start_time.'</b></td>
                </tr>
                <tr>
                    <td>Finish Time :</td>
                    <td>Meeting Location :</td>
                </tr>
                <tr>
                    <td><b>'.$meetingDetail->finish_time.'</b></td>
                    <td><b>'.$meetingDetail->meeting_location.'</b></td>
                </tr>
                <tr>';
                    if($meetingDetail->is_draft == '1'){
                        $data .= '<td>Draft Meeting</td>';
                    }
                    if($meetingDetail->is_private == '1'){
                        $data .='<td>Private Meeting</td>';
                    }
                $data .='</tr>
            </table>

            <div class="card mb-10">
                <div class="card-header" style="background: #efefef; padding: 10px;">
                    <div class="col-md-12">
                        <p class="rowcat">Scheduled Attendees</p>
                    </div>
                </div>
                <div class="card-body" style="padding: 10px;">
                    <div class="table-responsive"><br><br><br><br>
                        <table class="table" id="example"  style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Person</th>
                                    <th style="width: 5%;">Attendence</th>
                                </tr>
                            </thead>
                            <tbody>';
                            foreach ($assignedUser as $key=>$u) {
                                $data .= '<tr>
                                    <td>'; $data .= $key + 1; $data .= '</td>
                                    <td>'.get_user_name($u->user).'</td>';
                                    if ($u->status == 0) {
                                        $data .= '<td style="text-align: center;">Absent</td>';
                                    } elseif ($u->status == 1) {
                                        $data .= '<td style = "text-align: center;" >Present</td >';
                                    } elseif ($u->status == 2) {
                                        $data .= '<td style="text-align: center;">For Distribution Only</td>';
                                    } elseif ($u->status == 3) {
                                        $data .= '<td style = "text-align: center;" >Conference</td >';
                                    } elseif ($u->status == 4) {
                                        $data .= '<td style = "text-align: center;" >Conference</td >';
                                    }
                                $data .= '</tr>';
                            }
                            $data .='</tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">';
                            foreach($meetingCat as $mcat) {
                                $data .= '<div class="row" id="cat-' . $mcat->id . '">
                                    <div class="col-md-12">
                                        <div class="card mb-10">
                                            <div class="card-header">
                                                <div class="col-md-12">
                                                    <p class="rowcat">' . $mcat->title . '
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="card-body">';
                                    $bitems = \App\MeetingBusinessItem::where('category_id', $mcat->id)->get();
                                    if (count($bitems)) {
                                        $data .= '<div class="table-responsive"><br><br><br><br>
                                        <table class="table" id="example" style="width: 100%;" border="1" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%">#</th>
                                                    <th>Title</th>
                                                    <th>Due Date</th>
                                                    <th>Priority</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>';
                                            foreach ($bitems as $key => $meetingD) {
                                                $data .= '<tr>
                                                    <td>';
                                                    $data .= $key + 1;
                                                    $data .= '</td>
                                                    <td>' . $meetingD->title . '</td>
                                                    <td>' . $meetingD->due_date . '</td>
                                                    <td>' . get_priority($meetingD->priority) . '</td>
                                                    <td>';
                                                    if ($meetingD->status == '0') {
                                                        $data .= 'Open';
                                                    }
                                                    if ($meetingD->status == '1') {
                                                        $data .= 'Closed';
                                                    }
                                                    if ($meetingD->status == '2') {
                                                        $data .= 'Pending';
                                                    }
                                                    if ($meetingD->status == '3') {
                                                        $data .= 'Completed';
                                                    }
                                                    if ($meetingD->status == '4') {
                                                        $data .= 'Hold';
                                                    }
                                                    $data .= '</td>
                                                </tr>';
                                                $catMinutes = \App\MeetingMinutes::where('meeting_id', $meeting->id)->where('category_id', $meetingD->category_id)->first();
                                                if ($catMinutes !== null) {
                                                    $data .= '<tr>
                                                        <td colspan="5">' . $catMinutes->minutes . '</td>
                                                    </tr>';
                                                } else {
                                                    $data .= '<tr>
                                                        <td colspan = "5" > Minutes have not been entered </td >
                                                    </tr >';
                                                }
                                            }
                                            $data .= '</tbody>
                                            </table>
                                        </div>';
                                    }
                                    $data .= '</div>
                                    </div>
                                </div>
                            </div>';
                        }
                        $data .='</div>
                    </div>
                </div>
            </div>
        </div>
    </div>';

    echo $data;
?>
</body>
</html>
