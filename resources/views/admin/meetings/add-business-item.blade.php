<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
{!! Form::open(['id'=>'createBusinessItem','class'=>'ajax-form','method'=>'POST']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Add Business Item</h4>
</div>
<div class="modal-body">
    <div class="form-body">
        <div class="row">
            <input type="hidden" name="meeting_id" id="title" value="{{ $meeting }}" class="form-control">
            <div class="col-md-6 ">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" id="title" class="form-control">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label class="control-label">@lang('modules.tasks.assignTo')</label>
                    <select class="form-control selectpicker" name="assigned_user[]" id="user_id" multiple>
                        <option value="">@lang('modules.tasks.chooseAssignee')</option>
                        @foreach($employees as $employee)
                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label>Due Date</label>
                    <input type="text" name="due_date" id="due_date2" class="form-control" autocomplete="off">
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label class="control-label">Status</label>
                    <select class="form-control  selectpicker" name="status" id="status">
                        <option value="">Select Status</option>
                        <option value="0">Open</option>
                        <option value="1">Closed</option>
                        <option value="2">Pending</option>
                        <option value="3">Completed</option>
                        <option value="4">Hold</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label class="control-label">@lang('modules.tasks.priority')</label>
                    <select class="form-control  selectpicker" name="priority" id="priority">
                        <option value="">Select Priority</option>
                        <option value="0">High</option>
                        <option value="1">Medium</option>
                        <option value="2">Low</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label class="control-label">Category</label>
                    <select class="form-control  selectpicker" name="category_id" id="category_id">
                        <option value="">Select Category</option>
                        @foreach($meetingCat as $mcat)
                            <option value="{{ $mcat->id }}" @if($mcat->id == $cat) {{ 'selected' }} @endif>{{ ucwords($mcat->title) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea id="description" name="description" class="form-control summernote"></textarea>
                </div>
            </div>
            <div class="row m-b-20">
                <div class="col-md-12">
                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                    <div id="file-upload-box">
                        <div class="row" id="file-dropzone">
                            <div class="col-md-12">
                                <div class="dropzone"
                                     id="file-upload-dropzone">
                                    {{ csrf_field() }}
                                    <div class="fallback">
                                        <input name="file" type="file" id="image" multiple/>
                                    </div>
                                    <input name="image_url" id="image_url"type="hidden" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="meetingID" id="meetingID">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
    <button type="button" id="save-business" class="btn blue"> <i class="fa fa-check"></i> @lang('app.save')</button>
    <button type="button" id="save-and-create" class="btn blue"> <i class="fa fa-check"></i> Save & Create Another</button>
</div>
{!! Form::close() !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script>
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.meetings.storeBusinessImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#meetingID').val();
        formData.append('rfi_id', ids);
        formData.append('rfitype', 'rfi');
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "Business Items Created";
        $.showToastr(msgs, 'success');
        $('.createBusinessItem').click();
    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    $('.selectpicker').selectpicker();
    $('#save-business').click(function () {
        $.easyAjax({
            url: '{{route('admin.meetings.storeBusinessItems')}}',
            container: '#createBusinessItem',
            type: "POST",
            data: $('#createBusinessItem').serialize(),

            success: function (data) {
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    meetingID = data.meetingID;
                    $('#meetingID').val(data.meetingID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "Business Items Created";
                    $.showToastr(msgs, 'success');
                    window.location.reload();
                }
//                if(response.status == 'success'){
//
//                }
            }
        })
    });

    $('#save-and-create').click(function () {
        $.easyAjax({
            url: '{{route('admin.meetings.storeBusinessItems')}}',
            container: '#createBusinessItem',
            type: "POST",
            data: $('#createBusinessItem').serialize(),
            success: function (data) {
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    meetingID = data.meetingID;
                    $('#meetingID').val(data.meetingID);
                    myDropzone.processQueue();
//                    $('#title').val('');
//                    $('#user_id').val('');
//                    $('#due_date2').val('');
//                    $('#status').val('');
//                    $('#priority').val('');
//                    $('#category_id').val('');
//                    this.empty();

                }
                else{
                    var msgs = "Business Items Created";
                    $.showToastr(msgs, 'success');
//                    $('#title').val('');
//                    $('#user_id').val('');
//                    $('#due_date2').val('');
//                    $('#status').val('');
//                    $('#priority').val('');
//                    $('#category_id').val('');
                }
            }
        })
    });
</script>