@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.purchase-order.index') }}">@lang('app.menu.po')</a></li>
                <li>{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')
    <style>
        .icon-bar {
            width: 80%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: right;
            width: 16%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        /*div .icon-bar a.active {*/
        /*background-color: #002f76;*/
        /*color: white;*/
        /*}*/

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }

        .btn {
            padding: 6px !important;
        }
    </style>
    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
        .card-header {
            background: #efefef;
            padding: 10px;
        }
        .card-body {
            padding: 10px;
        }

        .note-editable .panel-body{
            height: 100px;
        }
    </style>
    <div>
        <div class="icon-bar">
            <a class="btn btn-outline btn-blue btn-sm createTaskCategory" id="Bom" href="javascript:;"><i class="fa fa-plus"></i> Create Category</a>
            {{--<a class="btn btn-outline btn-blue btn-sm" id="Stock" href="#"><i class="fa fa-envelope"></i> Email</a>--}}
            @if($meeting->distributed == 0)
                <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.meetings.distributeAgenda',[$meeting->id]) }}"><i class="fa fa-bullhorn"></i> Distribute Agenda</a>
            @else
                <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.meetings.distributeAgenda',[$meeting->id]) }}"><i class="fa fa-bullhorn"></i> Redistribute Agenda</a>
            @endif
            @if($meeting->converted == 0)
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.meetingConvertToMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Convert to Minutes</a>
            @elseif($meeting->converted == 1)
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.distributeMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Distribute Minutes</a>
            @else
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.distributeMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Redistribute Minutes</a>
            @endif
            @if($meeting->converted !== 0)
                <a class="btn btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.followUpMeeting',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Follow up Meeting</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
            <div class="panel panel-inverse">
                <div class="panel-heading"><input type="hidden" name="parent_id" value="{{ $meeting->id }}"><input type="hidden" name="meeting_title" value="{{ $meeting->title }}"> {{ $meeting->title }}</div>


                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Meeting Name</label>
                                    <input type="text" name="title" id="title" class="form-control" value="{{ $meetingDetail->title }}">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Meeting Date</label>
                                    <input type="text" name="meeting_date" id="due_date2" class="form-control" autocomplete="off" value="{{ $meetingDetail->meeting_date }}">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="time" name="meeting_start_time" id="start_time" class="form-control" value="{{ $meetingDetail->start_time }}">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Finish Time</label>
                                    <input type="time" name="meeting_finish_time" id="finish_time" class="form-control" value="{{ $meetingDetail->finish_time }}">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Meeting Location</label>
                                    <input type="text" name="meeting_location" id="meeting_location" class="form-control" value="{{ $meetingDetail->meeting_location }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                    <select class="selectpicker form-control" name="assigned_user[]" id="user_id" multiple>
                                        <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}" <?php
                                                if (in_array($employee->id, $assignedUser)){
                                                    echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Private Meeting</label><br>
                                    <input type="checkbox" name="is_private" value="1" id="title" class="form-check-input" @if($meetingDetail->is_private == '1') checked @endif>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label>Draft Meeting</label><br>
                                    <input type="checkbox" name="is_draft" value="1" id="title" class="form-check-input" @if($meetingDetail->is_draft == '1') checked @endif>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Meeting Overview</label>
                                    <textarea id="description" name="meeting_overview" class="form-control summernote">{{ $meetingDetail->meeting_overview }}</textarea>
                                </div>
                            </div>
                            @if(count($meetingFiles))
                                <div class="col-md-12">
                                    <b>Files</b>
                                    <br>
                                    <div class="row" id="list">
                                        <ul class="list-group" id="files-list">
                                            @foreach($meetingFiles as $file)
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            {{ $file->filename }}
                                                        </div>
                                                        <div class="col-md-3 edit-rfi">
                                                            @if($file->external_link != '')
                                                                <a target="_blank" href="{{ $file->external_link }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>

                                                            @elseif(config('filesystems.default') == 'local')
                                                                <a target="_blank" href="{{ asset_url('meeting-detail-files/'.$meetingDetail->id.'/'.$file->hashname) }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>

                                                            @elseif(config('filesystems.default') == 's3')
                                                                <a target="_blank" href="{{ $url.'meeting-detail-files/'.$meetingDetail->id.'/'.$file->filename }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>
                                                            @elseif(config('filesystems.default') == 'google')
                                                                <a target="_blank" href="{{ $file->google_url }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>
                                                            @elseif(config('filesystems.default') == 'dropbox')
                                                                <a target="_blank" href="{{ $file->dropbox_link }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"><i
                                                                            class="fa fa-search"></i></a>
                                                            @endif

                                                            <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                               class="btn1 btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                            <span class="clearfix m-l-10 detail-month">{{ $file->created_at->diffForHumans() }}</span>
                                                        </div>
                                                    </div>
                                                </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box">
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone dropheight"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="meetingID" id="meetingID" value="{{ $meeting->id }}" >
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            @foreach($meetingCat as $mcat)
                                <?php $bite = \App\MeetingBusinessItem::where('category_id',$mcat->id)->where('status','=','0')->where('meeting_detail_id',$meeting->id)->get(); ?>
                                    @if(count($bite)>0)
                                    <div class="row" id="cat-{{ $mcat->id }}">
                                        <div class="col-md-12">
                                            <div class="card mb-10">
                                                <div class="card-header">
                                                    <div class="col-md-12">
                                                        <p class="rowcat">
                                                            <input type="hidden" name="meeting_category[]" value="{{ $mcat->title }}">
                                                            <input type="hidden" name="mcatId[]" value="{{ $mcat->id }}">
                                                            {{ $mcat->title }}
                                                            <a class="btn btn-outline btn-blue btn-sm createBusinessItem" data-id="{{ $mcat->id }}" id="Bom" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Add Items"><i class="fa fa-plus"></i> </a>
                                                            <a class="btn btn-outline btn-blue btn-sm editCategory" data-id="{{ $mcat->id }}" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Edit Items"><i class="fa fa-pencil"></i> </a>
                                                            <a class="btn btn-outline btn-blue btn-sm deleteButton" data-id="{{ $mcat->id }}" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="card-body">


                                                        <div class="table-responsive">
                                                            <table class="table" id="example">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Title</th>
                                                                        <th>Due Date</th>
                                                                        <th>Priority</th>
                                                                        <th>Status</th>
                                                                        <th>@lang('app.action')</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($bite as $key=>$meetingD)
                                                                    <tr>
                                                                        <td>{{ $key+1 }}</td>
                                                                        <td><input type="hidden" name="business_item_title[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $meetingD->title }}">{!! $meetingD->title !!}</td>
                                                                        <td><input type="hidden" name="business_item_due_date[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $meetingD->due_date }}">{{ $meetingD->due_date }}</td>
                                                                        <td><input type="hidden" name="business_item_priority[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $meetingD->priority }}">{{ get_priority($meetingD->priority) }}</td>
                                                                        <td>
                                                                            <textarea style="display: none" name="description[{{ $mcat->id }}][{{ $meetingD->id }}]" >{!! $meetingD->description !!}</textarea>
                                                                            <input type="hidden" name="business_item_status[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $meetingD->status }}">
                                                                            <input type="hidden" name="business_item_id[{{ $mcat->id }}][]" value="{{ $meetingD->id }}">
                                                                            <input type="hidden" name="business_assigned_user[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $meetingD->assigned_user }}">
                                                                            <input type="hidden" name="meeting_category_id[{{ $mcat->id }}][{{ $meetingD->id }}]" value="{{ $mcat->id }}">
                                                                            <input type="hidden" class="businessItemId" value="{{ $meetingD->id }}">
                                                                            <select class="selectpicker form-control changeStatus" name="status">
                                                                                <option value="">Select Status</option>
                                                                                <option value="0" @if($meetingD->status=='0') selected @endif>Open</option>
                                                                                <option value="1" @if($meetingD->status=='1') selected @endif>Closed</option>
                                                                                <option value="2" @if($meetingD->status=='2') selected @endif>Pending</option>
                                                                                <option value="3" @if($meetingD->status=='3') selected @endif>Completed</option>
                                                                                <option value="4" @if($meetingD->status=='4') selected @endif>Hold</option>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:;" class="btn btn-info btn-circle editBusinessItem"
                                                                               data-toggle="tooltip" data-original-title="Edit" data-id="{{ $meetingD->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                            {{--<a href="{{ route('admin.meetings.viewMeeting',[$meetingD->id]) }}" class="btn btn-info btn-circle"
                                                                               data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>--}}
                                                                            <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                                                               data-toggle="tooltip" data-cat-id="{{ $mcat->id }}" data-meeting-id="{{ $meetingD->id }}" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php $catMinutes = \App\MeetingMinutes::where('meeting_id',$meeting->id)->where('category_id',$meetingD->category_id)->where('business_item_id',$meetingD->id)->first(); ?>
                                                                    @if($catMinutes !== null)
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td colspan="5">
                                                                                <div style="float: left">
                                                                                    <a href="javascript:;" class="btn btn-info btn-circle editMinutes"
                                                                                       data-toggle="tooltip" data-original-title="Edit" data-id="{{ $catMinutes->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                                </div>
                                                                                <div style="float: left; padding-left: 10px;">
                                                                                    <textarea style="display: none" name="category_minutes[]" >{!! $catMinutes->minutes !!}</textarea>
                                                                                    {!! $catMinutes->minutes !!}
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @else
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td colspan="5">Minutes have not been entered <button class="meetingMinutes btn btn-sm btn-success" data-id="{{ $meetingDetail->id }}" data-cat-id="{{ $meetingDetail->category_id }}" href="javascript:;">Add Minutes</button></td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                            @endforeach
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>    <!-- .row -->

    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">

                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-12 ">
                                <div class="form-group">
                                    <label>Category Name</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                    <input type="hidden" name="meeting_id" id="title" value="{{ $meeting->id }}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" id="save-category" class="btn blue"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-md in" id="businessItem" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                {!! Form::open(['id'=>'createBusinessItem','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="heading"></span>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <input type="hidden" name="meeting_id" id="title" value="{{ $meeting->id }}" class="form-control">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                    <select class="selectpicker form-control" name="assigned_user[]" id="user_id" multiple>
                                        <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Due Date</label>
                                    <input type="text" name="due_date" id="due_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <select class="selectpicker form-control" name="status">
                                        <option value="">Select Status</option>
                                        <option value="0">Open</option>
                                        <option value="1">Closed</option>
                                        <option value="2">Pending</option>
                                        <option value="3">Completed</option>
                                        <option value="4">Hold</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.tasks.priority')</label>
                                    <select class="selectpicker form-control" name="priority">
                                        <option value="">Select Priority</option>
                                        <option value="0">High</option>
                                        <option value="1">Medium</option>
                                        <option value="2">Low</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label">Category</label>
                                    <select class="selectpicker form-control" name="category_id">
                                        <option value="">Select Category</option>
                                        @foreach($meetingCat as $mcat)
                                            <option value="{{ $mcat->id }}">{{ ucwords($mcat->title) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <textarea id="description" name="description" class="form-control summernote"></textarea>
                                </div>
                            </div>
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box">
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="meetingID" id="meetingID" value="{{ $meeting->id }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" id="save-business" class="btn blue"> <i class="fa fa-check"></i> @lang('app.save')</button>
                    <button type="button" id="save-and-create" class="btn blue"> <i class="fa fa-check"></i> Save & Create Another</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="categoryEditModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="businessItemsEditModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="meetingMinutes" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            {!! Form::open(['id'=>'createMeetingMinutes','class'=>'ajax-form','method'=>'POST']) !!}
            <input type="hidden" name="business_item_id" value="" id="businessId" class="form-control">
            <input type="hidden" name="category_id" value="" id="categoryId" class="form-control">
            <input type="hidden" name="meeting_id" value="{{ $meeting->id }}" class="form-control">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea id="description" name="description" class="form-control summernote"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue" id="saveMinutes">Save changes</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="minutesEditModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.meetings.storeBusinessImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#meetingID').val();
            formData.append('rfi_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Followup Meeting Created";
            $.showToastr(msgs, 'success');
            window.location.replace("{{route('admin.meetings.followUpMeeting',$meeting->id)}}");
        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $('.createTaskCategory').click(function(){
            $('#modelHeading').html("Create Category");
            $.ajaxModal('#taskCategoryModal');
        })

        $('.createBusinessItem').click(function(){
            var btn = $(this);
            var id = btn.data('id');
            var url = '{{ route('admin.meetings.createBusinessItem',[':id',$meeting->id])}}';
            url = url.replace(':id', id);
            $('#heading').html("Add a business item");
            $.ajaxModal('#businessItem', url);
            $('.selectpicker').selectpicker();
        })

        $('.meetingMinutes').click(function(){
            var btn = $(this);
            var id = btn.data('id');
            var cid = btn.data('cat-id');
            $('#businessId').val(id)
            $('#categoryId').val(cid)
            $('#modelHeading').html("Add a meeting minutes");
            $.ajaxModal('#meetingMinutes');
        })

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-category').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.storeMeetingCategory')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),

                success: function (response) {
                    if(response.status == 'success'){
                        $('.summernote').summernote('code', '');
                        if(myDropzone.getQueuedFiles().length > 0){
                            meetingID = data.meetingID;
                            $('#meetingID').val(data.meetingID);
                            myDropzone.processQueue();
                        }
                        else{
                            var msgs = "Meeting Created";
                            $.showToastr(msgs, 'success');
                            window.location.reload();
                        }
                    }
                }
            })
        });

        $('#saveMinutes').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.meetingMinutes')}}',
                container: '#createMeetingMinutes',
                type: "POST",
                redirect: true,
                data: $('#createMeetingMinutes').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
        });

        $('#save-business').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.storeBusinessItems')}}',
                container: '#createBusinessItem',
                type: "POST",
                data: $('#createBusinessItem').serialize(),

                success: function (response) {
                    if(response.status == 'success'){
                        $('.summernote').summernote('code', '');
                        if(myDropzone.getQueuedFiles().length > 0){
                            meetingID = data.meetingID;
                            $('#meetingID').val(data.meetingID);
                            myDropzone.processQueue();
                        }
                        else{
                            var msgs = "Business Items Created";
                            $.showToastr(msgs, 'success');
                        }
                    }
                }
            })
        });

        $('#save-and-create').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.storeBusinessItems')}}',
                container: '#createBusinessItem',
                type: "POST",
                data: $('#createBusinessItem').serialize(),

                success: function (response) {
                    if(response.status == 'success'){
                        $('.summernote').summernote('code', '');
                        if(myDropzone.getQueuedFiles().length > 0){
                            meetingID = data.meetingID;
                            $('#meetingID').val(data.meetingID);
                            myDropzone.processQueue();
                        }
                        else{
                            var msgs = "Business Items Created";
                            $.showToastr(msgs, 'success');
                        }
                    }
                }
            })
        });

        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.rfq.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.rfq.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }
                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');
            $.ajax({
                url: '{{route('admin.rfq.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('.deleteButton').click(function () {
            var id = $(this).data('id');
            var url = "{{ route('admin.meetings.deleteCat',':id') }}";
            url = url.replace(':id', id);
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                    }
                }
            });
        });

        $('.editCategory').click(function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.meetings.editCategory',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Category");
            $.ajaxModal('#categoryEditModel', url);
        })
        $('.editBusinessItem').click(function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.meetings.editBusinessItem',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Business Item");
            $.ajaxModal('#businessItemsEditModel', url);
        })
        $('.editMinutes').click(function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.meetings.editMinutes',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Category");
            $.ajaxModal('#minutesEditModel', url);
        })

        $('.changeStatus').change(function(){
            var id = $('.businessItemId').val();
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.meetings.updateStatus')}}',
                type: 'POST',
                data: {_token: token, status: pid, id: id},
                success: function (data) {
                    var msgs = "Status updated";
                    $.showToastr(msgs, 'success');
//                    window.location.reload();
                }
            });
        })

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.addFollowupMeeting')}}',
                container: '#createRfq',
                type: "post",
                redirect: true,
                data: $('#createRfq').serialize(),
                success: function (data) {
                    if(data.status=='fail'){
                        $.showToastr(data.message, 'success');
                    }else{
                        $('.summernote').summernote('code', '');
                        if(myDropzone.getQueuedFiles().length > 0){
                            meetingID = data.meetingID;
                            $('#meetingID').val(data.meetingID);
                            myDropzone.processQueue();
                        }
                        else{
                            var msgs = "Follow up Meeting Created";
                            $.showToastr(msgs, 'success');
                            window.location.replace("{{route('admin.meetings.index')}}");
                        }
                    }
                }
            })
        });
        $('body').on('click', '.sa-params', function () {
            var catid = $(this).data('cat-id');
            var meetingbusid = $(this).data('meeting-id');

            var buttons = {
                cancel: "No, cancel please!",
                confirm: {
                    text: "Yes, delete it!",
                    value: 'confirm',
                    visible: true,
                    className: "danger",
                }
            };

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted punch item!",
                dangerMode: true,
                icon: 'warning',
                buttons: buttons
            }).then(function (isConfirm) {
                if (isConfirm == 'confirm' || isConfirm == 'recurring') {

                    var url = "{{ route('admin.meetings.deleteBusinessItem',':id') }}";
                    url = url.replace(':id', meetingbusid);
                    var token = "{{ csrf_token() }}";
                    var dataObject = {'_token': token, '_method': 'DELETE'};
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: dataObject,
                        success: function (response) {
                            if (response.status == "success") {
//                            $.unblockUI();
//                            table._fnDraw();
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
    </script>
@endpush