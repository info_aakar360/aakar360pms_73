@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: right;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        /*div .icon-bar a.active {*/
            /*background-color: #002f76;*/
            /*color: white;*/
        /*}*/

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }

        .btn {
            padding: 6px !important;
        }
    </style>

    <div>
        <div class="icon-bar">
            <a class="btn btn-outline btn-blue btn-sm" id="Bom" href="{{ route('admin.meetings.createMeeting') }}"><i class="fa fa-plus"></i> Create Meetings</a>
            {{--<a class="btn btn-outline btn-blue btn-sm" id="Stock" href="#"><i class="fa fa-search"></i> @lang('app.menu.stock')</a>--}}
            {{--<a class="btn btn-outline btn-blue btn-sm" id="Indents" href="#"><i class="fa fa-envelope"></i> @lang('modules.module.indent')</a>--}}
            {{--<a class="btn btn-outline btn-blue btn-sm" id="Pi" href="#"><i class="fa fa-globe"></i> Product Issue</a>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

                @foreach($meetings as $meeting)
                <div class="table-responsive">
                    <div class="col-md-12">
                        <div class="col-md-6" style="float: left;">
                            <h5 class="heading">{{ $meeting->title }}</h5>
                        </div>
                    </div>
                    <table class="table" id="example">
                        <thead>
                            <tr>
                                <th>@lang('app.sno')</th>
                                <th width="50%">@lang('app.meeting_overview')</th>
                                <th>@lang('app.meeting_date')</th>
                                <th>@lang('app.meeting_time')</th>
                                <th>@lang('app.meeting_location')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $meetingDetails = \App\MeetingDetail::where('meeting_id',$meeting->id)->get();
                        $fumeeting = \App\Meetings::where('parent_id',$meeting->id)->get();
                        ?>
                        @foreach($meetingDetails as $key=>$meetingDetail)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{!! sub_str($meetingDetail->meeting_overview,100) !!}</td>
                                <td>{{ $meetingDetail->meeting_date }}</td>
                                <td>{{ $meetingDetail->start_time }}</td>
                                <td>{{ $meetingDetail->meeting_location }}</td>
                                {{--<td>4</td>--}}
                                <td>
                                    <a href="{{ route('admin.meetings.editMeeting',[$meetingDetail->id]) }}" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="{{ route('admin.meetings.viewMeeting',[$meetingDetail->id]) }}" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="javascript:;" class="btn btn-danger btn-circle sa-params delete-category-details"
                                       data-toggle="tooltip" data-cat-id="{{ $meetingDetail->id }}" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @if(count($fumeeting))
                                <?php $x =1;?> @foreach($fumeeting as $key=>$fum)
                                    <?php $mD = \App\MeetingDetail::where('meeting_id',$fum->id)->first(); ?>
                                    @if($mD !== null)
                                    <tr>
                                        <td><?=$x; ?></td>
                                        <td>{!! $mD->meeting_overview !!}</td>
                                        <td>{{ $mD->meeting_date }}</td>
                                        <td>{{ $mD->start_time }}</td>
                                        <td>{{ $mD->meeting_location }}</td>
                                        {{--<td>4</td>--}}
                                        <td>
                                            <a href="{{ route('admin.meetings.editMeeting',[$mD->id]) }}" class="btn btn-info btn-circle"
                                               data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a href="{{ route('admin.meetings.viewMeeting',[$mD->id]) }}" class="btn btn-info btn-circle"
                                               data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="javascript:;" class="btn btn-danger btn-circle sa-params delete-category"
                                               data-toggle="tooltip" data-cat-id="{{ $mD->id }}" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    @endif
                              <?php $x++;?>
                                @endforeach
                            @endif
                        @endforeach

                        </tbody>
                    </table>
                    <hr>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">

                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-12 ">
                                <div class="form-group">
                                    <label>Meeting Name</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="form-actions">
                        <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" id="save-category" class="btn blue"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
//    $(document).ready(function() {
//        $('#example').DataTable( {
//            deferRender:    true,
//            scrollCollapse: true,
//            scroller:       true
//        } );
//    } );
</script>
<script>
    $('.createTaskCategory').click(function(){
        {{--var id = $(this).data('id');--}}
        {{--var url = '{{ route('admin.meetings.createMeeting',':id')}}';--}}
        $('#modelHeading').html("Create New");
        $.ajaxModal('#taskCategoryModal');
    })

    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.meetings.storeMeeting')}}',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),

            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    $('ul.showProjectTabs .projectTasks').addClass('tab-current');

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('admin.meetings.destroy',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        });
    });
    $('.delete-category-details').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('admin.meetings.details-destroy',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        });
    });
</script>
@endpush