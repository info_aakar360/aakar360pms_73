@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.purchase-order.index') }}">@lang('app.menu.po')</a></li>
                <li>{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')
    <style>
        .icon-bar {
            width: 80%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: right;
            width: 16%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        /*div .icon-bar a.active {*/
        /*background-color: #002f76;*/
        /*color: white;*/
        /*}*/

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }

        .btn {
            padding: 6px !important;
        }
    </style>
    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
        .card-header {
            background: #efefef;
            padding: 10px;
        }
        .card-body {
            padding: 10px;
        }

        .note-editable .panel-body{
            height: 100px;
        }
    </style>
    {{--<div>
        <div class="icon-bar">
            <a class="btn btn-outline btn-blue btn-sm createTaskCategory" id="Bom" href="javascript:;"><i class="fa fa-plus"></i> Create Category</a>
            <a class="btn btn-outline btn-blue btn-sm" id="Stock" href="#"><i class="fa fa-envelope"></i> Email</a>
            @if($meeting->distributed == 0)
                <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.meetings.distributeAgenda',[$meeting->id]) }}"><i class="fa fa-bullhorn"></i> Distribute Agenda</a>
            @else
                <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.meetings.distributeAgenda',[$meeting->id]) }}"><i class="fa fa-bullhorn"></i> Redistribute Agenda</a>
            @endif
            @if($meeting->converted == 0)
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.meetingConvertToMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Convert to Minutes</a>
            @elseif($meeting->converted == 1)
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.distributeMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Distribute Minutes</a>
            @else
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.distributeMinute',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Redistribute Minutes</a>
            @endif
            @if($meeting->converted !== 0)
                <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.meetings.followUpMeeting',[$meeting->id]) }}"><i class="fa fa-check-square-o"></i> Follow up Meeting</a>
            @endif
        </div>
    </div>--}}
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> {{ $meeting->title }}</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">
                            <div class="row">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;">Meeting Date :</td>
                                        <td  style="width: 35%;"><b>{{ \Carbon\Carbon::parse($meetingDetail->meeting_date)->format('d M, Y') }}</b></td>
                                        <td style="width: 15%;">Meeting Location:</td>
                                        <td  style="width: 35%;"><b>{{ $meetingDetail->meeting_location }}</b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">Start Time :</td>
                                        <td  style="width: 35%;"><b>{{ $meetingDetail->start_time }}</b></td>
                                        <td style="width: 15%;">Finish Time:</td>
                                        <td  style="width: 35%;"><b>{{ $meetingDetail->finish_time }}</b></td>
                                    </tr>
                                    <tr>
                                        @if($meetingDetail->is_draft == '1')
                                        <td style="width: 15%;">Draft Meeting :</td>
                                        <td  style="width: 35%;"><b>Yes</b></td>
                                        @endif
                                            @if($meetingDetail->is_private == '1')
                                        <td style="width: 15%;">Private Meeting</td>
                                        <td  style="width: 35%;"><b>Yes</b></td>
                                            @endif
                                    </tr>
                                </table>
                            </div> 
                            @if(count($meetingFiles))
                                <div class="col-md-12">
                                    <b>Files</b>
                                    <br>
                                    <div class="row" id="list">
                                        @foreach($meetingFiles as $file)
                                            <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                                @if($file->external_link != '')
                                                    <?php $imgurl = $file->external_link;?>
                                                @elseif($storage == 'local')
                                                    <?php $imgurl = uploads_url().'meeting-detail-files/'.$meetingDetail->id.'/'.$file->hashname;?>
                                                @elseif($storage == 's3')
                                                    <?php $imgurl = awsurl().'/meeting-detail-files/'.$meetingDetail->id.'/'.$file->hashname;?>
                                                @elseif($storage == 'google')
                                                    <?php $imgurl = $file->google_url;?>
                                                @elseif($storage == 'dropbox')
                                                    <?php $imgurl = $file->dropbox_link;?>
                                                @endif
                                                {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card mb-10">
                                        <div class="card-header">
                                            <div class="col-md-12">
                                                <p class="rowcat">Scheduled Attendees</p>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table" id="example">
                                                    <thead>
                                                    <tr>
                                                        <th>Sl no</th>
                                                        <th>Person</th>
                                                        {{--<th style="width: 5%;">Present</th>--}}
                                                        {{--<th style="width: 5%;">Absent</th>--}}
                                                        {{--<th style="width: 5%;">For Distribution Only</th>--}}
                                                        {{--<th style="width: 5%;">Conference</th>--}}
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($assignedUser as $key=>$u)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td>{{ get_users_employee_name($u->user,$meeting->company_id) }}</td>
                                                            {{--@if($u->status == 0)--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'1']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'2']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'3']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'4']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                            {{--@elseif($u->status == 1)--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'0']) }}"><i class="fa fa-check" style="color: green;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'2']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'3']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'4']) }}"><i class="fa fa-times" style="color: red;"></i></td>--}}
                                                            {{--@elseif($u->status == 2)--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'1']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'0']) }}"><i class="fa fa-check" style="color: green;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'3']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'4']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                            {{--@elseif($u->status == 3)--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'1']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'2']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'0']) }}"><i class="fa fa-check" style="color: green;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'4']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                            {{--@elseif($u->status == 4)--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'1']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'2']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'3']) }}"><i class="fa fa-times" style="color: red;"></i></a></td>--}}
                                                                {{--<td style="text-align: center;"><a href="{{ route('admin.meetings.updateUserStatus',[$u->id,'0']) }}"><i class="fa fa-check" style="color: green;"></i></a></td>--}}
                                                            {{--@endif--}}
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @foreach($meetingCat as $mcat)
                                <div class="row" id="cat-{{ $mcat->id }}">
                                    <div class="col-md-12">
                                        <div class="card mb-10">
                                            <div class="card-header">
                                                <div class="col-md-12">
                                                    <p class="rowcat">{{ $mcat->title }}
                                                        <a class="btn btn-outline btn-blue btn-sm createBusinessItem" data-id="{{ $mcat->id }}" id="Bom" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Add Items"><i class="fa fa-plus"></i> </a>
                                                        <a class="btn btn-outline btn-blue btn-sm editCategory" data-id="{{ $mcat->id }}" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Edit Items"><i class="fa fa-pencil"></i> </a>
                                                        <a class="btn btn-outline btn-blue btn-sm deleteButton" data-id="{{ $mcat->id }}" href="javascript:;" style="float: right;" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <?php $bitems = \App\MeetingBusinessItem::where('category_id',$mcat->id)->get(); ?>
                                                @if(count($bitems))
                                                    <div class="table-responsive">
                                                        <table class="table" id="example">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Title</th>
                                                                <th>Assignment</th>
                                                                <th>Due Date</th>
                                                                <th>Priority</th>
                                                                <th>Status</th>
                                                                {{--<th># Items</th>--}}
                                                                <th>@lang('app.action')</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($bitems as $key=>$meetingD)
                                                                <tr>
                                                                    <td>{{ $key+1 }}</td>
                                                                    <td>{!! $meetingD->title !!}</td>
                                                                    <td>{{ get_user_name($meetingD->assigned_user) }}</td>
                                                                    <td>{{ $meetingD->due_date }}</td>
                                                                    <td>{{ get_priority($meetingD->priority) }}</td>
                                                                    <td>{{ get_status($meetingD->status) }}</td>
                                                                    {{--<td>4</td>--}}
                                                                    <td>
                                                                        <a href="javascript:;" class="btn btn-info btn-circle editBusinessItem"
                                                                           data-toggle="tooltip" data-original-title="Edit" data-id="{{ $meetingD->id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                        {{--<a href="{{ route('admin.meetings.viewMeeting',[$meetingD->id]) }}" class="btn btn-info btn-circle"
                                                                           data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>--}}
                                                                        <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                                                           data-toggle="tooltip" data-user-id="{{ $meetingD->id }}" data-user-id="{{ $meetingD->id }}" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-12 ">
                                <div class="form-group">
                                    <label>Category Name</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                    <input type="hidden" name="meeting_id" id="title" value="{{ $meeting->id }}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" id="save-category" class="btn blue"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-md in" id="businessItem" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="heading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="categoryEditModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="businessItemsEditModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>

        $('.createTaskCategory').click(function(){
            $('#modelHeading').html("Create Category");
            $.ajaxModal('#taskCategoryModal');
        })

        $('.createBusinessItem').click(function(){
            var btn = $(this);
            var id = btn.data('id');
            var url = '{{ route('admin.meetings.createBusinessItem',[':id',$meeting->id])}}';
            url = url.replace(':id', id);
            $('#heading').html("Add a business item");
            $.ajaxModal('#businessItem', url);
            $('.selectpicker').selectpicker();
        })

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-category').click(function () {
            $.easyAjax({
                url: '{{route('admin.meetings.storeMeetingCategory')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),

                success: function (data) {
//                    var msgs = "Category Created";
//                    $.showToastr(msgs, 'success');
                    window.location.reload();
                }
            })
        });



        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.rfq.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.rfq.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }
                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');
            $.ajax({
                url: '{{route('admin.rfq.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('.deleteButton').click(function () {
            var id = $(this).data('id');
            var url = "{{ route('admin.meetings.deleteCat',':id') }}";
            url = url.replace(':id', id);
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                    }
                }
            });
        });

        $('.editCategory').click(function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.meetings.editCategory',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Category");
            $.ajaxModal('#categoryEditModel', url);
        })
        $('.editBusinessItem').click(function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.meetings.editBusinessItem',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Business Item");
            $.ajaxModal('#businessItemsEditModel', url);
        })
    </script>
@endpush

