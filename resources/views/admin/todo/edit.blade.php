@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.todo.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.todo.editTodo')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storeTask','class'=>'ajax-form','method'=>'POST']) !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.title')</label>
                                        <input type="text" id="title" name="title" class="form-control" value="{{ $todo->title }}" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.status')</label>
                                        <select class="select2 form-control" data-placeholder="@lang("app.selectStatus")" id="status_id" name="status">
                                            <option value="">Select Status</option>
                                            <option value="notstarted"  @if('notstarted'==$todo->status) selected @endif>Not Started</option>
                                            <option value="inprogress"  @if('inprogress'==$todo->status) selected @endif>In Progress</option>
                                            <option value="inproblem"  @if('inproblem'==$todo->status) selected @endif>In Problem</option>
                                            <option value="delayed"  @if('delayed'==$todo->status) selected @endif>Delayed</option>
                                            <option value="completed"  @if('completed'==$todo->status) selected @endif>Completed</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.project')</label>
                                        <select class="select2 form-control" data-placeholder="@lang("app.selectProject")" id="project_id" name="project_id">
                                            <option value=""></option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" @if($project->id==$todo->project_id) selected @endif>{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <?php  if(in_array('sub_projects', $user->modules)){?>
                                <div class="col-sm-6 col-xs-6" id="subprojectblock" style="display: <?php if(empty($todo->subproject_id)){ echo 'none';}else{ echo 'block';}?>;">
                                    <div class="form-group">
                                        <label for="title_id">Sub Project</label>
                                        <select class="select2 form-control titlelist" id="subprojectlist" name="subproject_id" data-style="form-control" required>
                                            <option value="">Please Select Sub Project</option>
                                            @foreach($titlelist as $title)
                                                <option value="{{ $title->id }}" <?php if($todo->subproject_id==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <?php }?>

                                <?php  if(in_array('segment', $user->modules)){?>
                                <div class="col-sm-6 col-xs-6 segmentblock"  style="display: <?php if(empty($todo->segment_id)){ echo 'none';}else{ echo 'block';}?>;">
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.segment')</label>
                                        <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                            <option value="">Select @lang('app.segment')</option>
                                            @foreach($segmentlist as $segment)
                                                <option value="{{ $segment->id }}" <?php if($todo->segment_id==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <?php }?>

                                <div class="col-md-6 ">

                                    <div class="form-group">
                                        <label class="control-label">@lang('app.activity')
                                            {{--<a href="javascript:;" id="createTaskCategory"  class="btn btn-sm btn-outline btn-success"><i  class="fa fa-plus"></i> @lang('modules.taskCategory.addTaskCategory')</a>--}}
                                        </label>
                                        <select class="form-control" name="category_id" id="activitylist"
                                                data-style="form-control">
                                            <option value="">Select  @lang('app.activity')</option>
                                            @foreach($activitiesarray as $activities)
                                                <option value="{{ $activities->id }}"  @if($todo->task_category_id==$activities->id) selected @endif >{{ ucwords($activities->itemname) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 ">

                                    <div class="form-group">
                                        <label class="control-label">@lang('app.task')
                                            {{--<a href="javascript:;" id="createTaskCategory"  class="btn btn-sm btn-outline btn-success"><i  class="fa fa-plus"></i> @lang('modules.taskCategory.addTaskCategory')</a>--}}
                                        </label>
                                        <select class="form-control" name="costitem_id" id="tasklist" data-style="form-control">
                                            <option value="">Please Select Task</option>
                                            @if($costitemlist)
                                                @foreach($costitemlist as $costitem => $costitemname)
                                                    <option value="{{ $costitem }}" <?php if($todo->costitem_id==$costitem){ echo 'selected';}?>>{{ ucwords($costitemname) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.startDate')</label>
                                        <input type="text" name="start_date" id="start_date2" class="form-control" autocomplete="off" value="{{ !empty($todo->start_date) ? date('d-m-Y', strtotime($todo->start_date)) : '' }}">
                                    </div>
                                </div>
                                <!--/span-->

                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" autocomplete="off" value="{{ !empty($todo->due_date) ? date('d-m-Y', strtotime($todo->due_date)) : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.todo.assignTo')</label>
                                        <select class="select2 form-control" name="assign_to" id="user_id" >
                                            <option value="">@lang('modules.todo.chooseAssignee')</option>
                                            <?php
                                                $assignarray = explode(',',$todo->assign_to)
                                            ?>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}" @if(in_array($employee->user_id,$assignarray)) selected @endif>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="row m-b-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.todo.priority')</label>

                                        <div class="radio radio-danger">
                                            <input type="radio" name="priority" id="radio13" value="high" @if('high'==$todo->priority) checked @endif>
                                            <label for="radio13" class="text-danger">
                                                @lang('modules.todo.high') </label>
                                        </div>
                                        <div class="radio radio-warning">
                                            <input type="radio" name="priority"   id="radio14" checked value="medium"  @if('medium'==$todo->priority) checked @endif>
                                            <label for="radio14" class="text-warning">
                                                @lang('modules.todo.medium') </label>
                                        </div>
                                        <div class="radio radio-success">
                                            <input type="radio" name="priority" id="radio15" value="low"  @if('low'==$todo->priority) checked @endif>
                                            <label for="radio15" class="text-success">
                                                @lang('modules.todo.low') </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <div class="checkbox checkbox-info">
                                            <input id="private" name="private" value="1" type="checkbox" @if($todo->private=='1') checked @endif  >
                                            <label for="repeat-task">@lang('modules.todo.privatePublic')</label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <!--/span-->


                                <div class="row m-b-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">@lang('app.description')</label>
                                            <textarea id="description" name="description" class="form-control summernote">{{ $todo->description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">@lang('app.image')</label>
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url"type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="todoID" id="todoID">
                                    </div>
                                </div>

                                <!--/span-->
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php if(count($attachmentlist)>0){
                                            foreach ($attachmentlist as $attachment){?>
                                            <div class="col-md-1" id="fileid{{ $attachment->id }}">
                                                @if($attachment->external_link != '')
                                                    <?php $imgurl = $attachment->external_link;?>
                                                @elseif($storage == 'local')
                                                    <?php $imgurl = uploads_url().'todo-files/'.$attachment->todo_id.'/'.$attachment->hashname;?>
                                                @elseif($storage == 's3')
                                                    <?php $imgurl = awsurl().$attachment->company_id.'/todo-files/'.$attachment->todo_id.'/'.$attachment->hashname;?>
                                                @elseif($storage == 'google')
                                                    <?php $imgurl = $attachment->google_url;?>
                                                @elseif($storage == 'dropbox')
                                                    <?php $imgurl = $attachment->dropbox_link;?>
                                                @endif
                                                {!! mimetype_thumbnail($attachment->hashname,$imgurl)  !!}
                                                    <a href="javascript:;" onclick="removeFile({{ $attachment->id }})" style="text-align: center;">
                                                        Remove
                                                    </a>
                                            </div>
                                            <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 text-right">
                            <div class="form-actions">
                                {{ csrf_field() }}
                                <button type="button" id="store-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.todo.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = $('#todoID').val();
            formData.append('todo_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.taskCreatedSuccessfully')";
            $.showToastr(msgs, 'success');
            /*window.location.href = '{{ route('admin.todo.index') }}'*/

        });
        $('.summernote').summernote({
            height: 160,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        //    update task
        $('#store-task').click(function () {

            $.easyAjax({
                url: '{{route('admin.todo.update',$todo->id)}}',
                container: '#storeTask',
                type: "PUT",
                data: $('#storeTask').serialize(),
                success: function (data) {
                    $('#storeTask').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        todoID = data.todoID;
                        $('#todoID').val(data.todoID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.todoUpdatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.todo.index') }}'
                    }
                }
            })
        });

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('#project_id').change(function () {
            var id = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id},
                success: function(data){
                    $("#subprojectblock").hide();
                    $("#segmentsblock").hide();
                    $("select#tasklist").html("");
                    $("select#subprojectlist").html("");
                    $("select#activitylist").html("");
                    $("select#segmentslist").html("");
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.subprojectlist){
                        $("#subprojectblock").show();
                        $("select#subprojectlist").html(data.subprojectlist);
                        $('select#subprojectlist').select2();
                    }
                    if(data.activitylist){
                        $("select#activitylist").html(data.activitylist);
                        $('select#activitylist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });
        $('#subprojectlist').change(function () {
            var subprojecid = $(this).val();
            var id = $('#project_id').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id,'subprojectid': subprojecid},
                success: function(data){
                    $("#segmentsblock").hide();
                    $("select#activitylist").html("");
                    $("select#segmentslist").html("");
                    $("select#tasklist").html("");
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.activitylist){
                        $("select#activitylist").html(data.activitylist);
                        $('select#activitylist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });
        $("#activitylist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token,'projectid': project,'subprojectid': titlelist,'activity': activity},
                    success: function(data){
                        if(data.tasklist){
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $('#repeat-task').change(function () {
            if($(this).is(':checked')){
                $('#repeat-fields').show();
            }
            else{
                $('#repeat-fields').hide();
            }
        });

        $('#dependent-task').change(function () {
            if($(this).is(':checked')){
                $('#dependent-fields').show();
            }
            else{
                $('#dependent-fields').hide();
            }
        });
        $('select.costitem').change(function () {
            console.log($(this).val());
            if($(this).val()=='other'){
                $('#titlebox').show();
            }else{
                $('#titlebox').hide();
            }
        });
        //    update inspection
        function removeFile(id) {
            var url = "{{ route('admin.todo.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        $("#fileid"+id).remove();
                    }
                }
            })

        };
        $('#createTaskCategory').click(function(){
            var url = '{{ route('admin.taskCategory.create-cat')}}';
            $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
            $.ajaxModal('#taskCategoryModal', url);
        });
    </script>
@endpush

