
    @foreach($allTasks as $allTask)
        <option value="{{ $allTask->id }}">{{ $allTask->title }} (@lang('app.dueDate'): {{ $allTask->due_date->format($global->date_format) }})</option>
    @endforeach