                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $user = $boqarray['user'];
                                $proproget = $boqarray['proproget'];
                                $userarray = $boqarray['userarray'];
                                $id = $boqarray['id'];
                                $title = $boqarray['title'];
                                $costitemslist = $boqarray['costitemslist'];
                                $contractorarray = $boqarray['contractorarray'];
                                $colpositionarray =$boqarray['colpositionarray'];
                                $unitsarray =$boqarray['unitsarray'];
                                $typesarray = $boqarray['typesarray'];
                                $grandtotal = $boqarray['grandtotal'];
                                $categories = $boqarray['categories'];
                                $catvalue = $boqarray['catvalue'];
                                $parent = $boqarray['parent'];
                                $subtotcat = $boqarray['subtotcat'];
                                $columsarray = $boqarray['columsarray'];
                                $snorow = $boqarray['snorow'];
                                foreach ($proproget as $propro){
                                $cattotalamt = 0;
                                if(!empty($propro->catlevel)){
                                    $catvalue =  $propro->catlevel.','.$propro->itemid;
                                }else{
                                    if(!empty($propro->parent)){
                                        $catvalue =  $propro->parent.','.$propro->itemid;
                                    }else{
                                        $catvalue = (string)$propro->itemid;
                                    }
                                }
                                $level = (int)$propro->level;
                                $levelname = 'level'.$level;
                                $newlevel = $level+1;
                                $parent = $propro->itemid;
                                $catitem = $propro->itemid;
                                if($level==0){
                                    $subtotcat = $propro->itemid;
                                }
                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }
                                ?>
                                    <tr data-level1cat="{{ $propro->id }}" data-depth="0" class="maincat {{ $levelname }} collpse catitem{{ $catvalue }}">
                                        <td align="left">{{ $snorow }}</td>
                                        <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="{{ $propro->id }}" data-level="{{ $level }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                        <td class="text-center"><a href="javascript:void(0);" class="opntoggle iconcode @if($level>0) dotlevel @else dotlevel1 @endif">{!! get_dots_by_level($level) !!}</a></td>
                                        <td colspan="3"><?php if (isset($propro->itemname)){ echo $propro->itemname; } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php

                                 $proprogetdatarra = \App\ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('category',$catvalue)->orderBy('inc','asc')->get();
                                $tid=1;  foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                                $rate = $proprogetdat->rate;
                                $changeorderrate = $proprogetdat->changeorderrate;
                                $qty = $proprogetdat->qty;
                                $baseamount = $rate*$qty;
                                $basechangeorderamount = $changeorderrate*$qty;
                                $basetotalamt = $baseamount+$basechangeorderamount;
                                $proprogetdat->amount = $baseamount;
                                $proprogetdat->position_id = $propro->id;
                                $proprogetdat->save();
                                $cattotalamt += $baseamount;
                                $cattotalamt += $basechangeorderamount;
                                $grandtotal += $cattotalamt;
                                ?>
                                <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="{{ $proprogetdat->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                    <td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                    <?php foreach ($colpositionarray as $colposition){
                                    switch($colposition->itemslug){
                                    case 'costitem': ?>
                                    <td><input  class="cell-inp updateproject" data-item="cost_items_id"  data-positionid="{{ $propro->id }}" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="costitem{{ $proprogetdat->id }}"  value="{{ get_cost_name($proprogetdat->cost_items_id) }}">
                                        <datalist class="costitemslist"  id="costitem{{ $proprogetdat->id }}">
                                            <?php foreach($costitemslist as $costitem){ ?>
                                                <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                            <?php }?>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'description': ?>
                                    <td><textarea  class="cell-inp updateproject"  data-item="description" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onkeydown="textAreaAdjust(this)" style="height: 25px;">{{ !empty($proprogetdat->description) ? $proprogetdat->description : '' }}</textarea></td>
                                    <?php   break;
                                    case 'basecost': ?>
                                    <td style="position:relative;">  <input type="text"   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp ratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="rate" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($baseamount) ? $baseamount : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'coprice': ?>
                                    <td>
                                        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp changeorderratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="changeorderrate" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($basechangeorderamount) ? $basechangeorderamount : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'totalprice': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp grandvalue totalamount{{ $proprogetdat->id }} finalamount{{ $catitem }} subtotalamount{{ $subtotcat }}" data-level="{{ $level }}" data-item="totalamount" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" name="finalamount"  value="{{ !empty($basetotalamt) ? $basetotalamt : '' }}"  ></td>
                                    <?php   break;
                                    }
                                    }?>
                                </tr>
                                <?php
                                $tid++;    } }
                                $catdata = 1;
                                 $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['user'] = $user;
                                $boqarray['proproget'] = $proprogetlevel1;
                                $boqarray['userarray'] = $userarray;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = (int)$title ?: 0;
                                $boqarray['costitemslist'] = $costitemslist;
                                $boqarray['contractorarray'] = $contractorarray;
                                $boqarray['colpositionarray'] = $colpositionarray;
                                $boqarray['unitsarray'] = $unitsarray;
                                $boqarray['typesarray'] = $typesarray;
                                $boqarray['grandtotal'] = $grandtotal;
                                $boqarray['categories'] = $categories;
                                $boqarray['catvalue'] = $catvalue;
                                $boqarray['parent'] = $propro->itemid;
                                $boqarray['subtotcat'] = $subtotcat;
                                $boqarray['snorow'] = $snorow;
                                $boqarray['cattotalamt'] = $cattotalamt;
                                $boqarray['columsarray'] = $columsarray;
                                 echo boqhtml($boqarray);
                                ?>
                                    <tr  data-depth="1" class="collpse {{ $levelname }}" >
                                        <td></td>
                                        <td></td>
                                        <td colspan="3"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;"> <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New @lang('app.activity')" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemcategory" data-catlevel="{{ $catvalue }}" data-level="{{ $newlevel }}" data-parent="{{ $propro->itemid }}"  list="costitemlevel{{ $newlevel }}" placeholder="@lang('app.activity')">
                                            <datalist  id="costitemlevel{{ $newlevel }}">
                                                <?php foreach($categories as $category){ ?>
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                <?php  } ?>
                                            </datalist></div></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr data-depth="1" class="collpse level{{ $newlevel }}">
                                        <td></td>
                                        <td></td>
                                        <td colspan="3"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-cost-item" data-toggle="tooltip" title="Add New @lang('app.activity')" ><i class="fa fa-ellipsis-v"></i></a>
                                                <input  class="cell-inp costitemrow" @if($level=='0') data-cattype="single" @endif  data-positionid="{{ $propro->id }}"  data-cat="{{ $catvalue }}" data-level="{{ $newlevel }}" list="costitemcat{{ $catdata }}" placeholder="@lang('app.task')">
                                            <datalist class="costitemslist"  id="costitemcat{{ $catdata }}">
                                                <?php foreach($costitemslist as $costitem){ ?>
                                                    <option value="{{ $costitem->cost_item_name }}" data-id="{{ $costitem->id }}"  >{{ $costitem->cost_item_name }}</option>
                                                <?php  } ?>
                                            </datalist></div>
                                        </td>
                                        <td><input type="text" class="cell-inp" name="desc" ></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                    </tr>
                                    <tr data-depth="1" class="collpse level{{ $newlevel }}" style="background-color: #efefef;">
                                        @if($level>0)
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td  colspan="3" >
                                            <strong> Sub Total</strong>
                                            </td>
                                        @else
                                            <td  colspan="6" >
                                                <strong> Sub Total</strong>
                                            </td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="catotal{{ $catitem }} subtotal{{ $subtotcat }}level{{ $level }}" data-level="{{ $level }}" style="font-weight: bold;">
                                           <?php  echo '₹'.number_format($cattotalamt,2);   ?>
                                        </td>
                                    </tr>
                                <?php
                                } }
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();
                                $costitemslist = \App\CostItems::where('company_id',$projectdetails->company_id)->orderBy("id",'asc')->get();
                                $contractorarray = \App\Employee::getAllContractors($user,$projectdetails->id);

                                $userarray = \App\Employee::getAllEmployees($user,$projectdetails->id);
                                $colpositionarray = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->whereIn('itemslug',$columsarray)->orderBy('inc','asc')->get();

                                 $boqarray = array();
                                $boqarray['user'] = $user;
                                $boqarray['proproget'] = $proproget;
                                $boqarray['userarray'] = $userarray;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = $title ?: 0;
                                $boqarray['costitemslist'] = $costitemslist;
                                $boqarray['contractorarray'] = $contractorarray;
                                $boqarray['colpositionarray'] = $colpositionarray;
                                $boqarray['unitsarray'] = $unitsarray;
                                $boqarray['typesarray'] = $typesarray;
                                $boqarray['grandtotal'] = 0;
                                $boqarray['categories'] = $categories;
                                $boqarray['columsarray'] = $columsarray;
                                $boqarray['cattotalamt'] = 0;
                                $boqarray['snorow'] = 0;
                                $boqarray['catvalue'] = '';
                                $boqarray['parent'] = '';
                                $boqarray['subtotcat'] = '';

                              echo  boqhtml($boqarray);

                                $grandtotal = \App\ProjectCostItemsProduct::where('project_id',$id)->where('title',$title)->sum('amount');
                                $changegrandtotal = \App\ProjectCostItemsProduct::select(\Illuminate\Support\Facades\DB::raw('sum(changeorderrate*qty) as coamount'))->where('project_id',$id)->where('title',$title)->first();
                                $changegrandtotal = $changegrandtotal->coamount ?: 0;
                                 ?>
                                    <tr  data-depth="0" class="collpse " >
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="4">
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New @lang('app.activity')" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemcategory" list="costitemcatitem" data-level="0" placeholder="@lang('app.activity')">
                                            <datalist id="costitemcatitem">
                                                @foreach($categories as $category)
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                @endforeach
                                            </datalist>
                                            </div></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr  data-depth="0" class="maincat collpse level0" >
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="4"><strong> Grand Total</strong></td>
                                        <td></td>
                                        <td></td>
                                        <td class="grandtotal" style="font-weight: bold;">₹{{ !empty($grandtotal+$changegrandtotal) ? number_format($grandtotal+$changegrandtotal,2) : 0 }}</td>
                                    </tr>