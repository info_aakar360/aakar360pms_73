@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }} #{{ $project->id }} - <span class="font-bold">{{ ucwords($project->project_name) }} </span> / Store Name - <span class="font-bold">{{ ucwords($store->company_name) }} </span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.module.stores')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/icheck/skins/all.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    @include('admin.projects.show_project_menu')
                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="javascript:;" class="btn btn-outline btn-success btn-sm createTaskCategory">Add Material <i class="fa fa-plus" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Material Name</th>
                                                        <th>Trade</th>
                                                        <th>Unit</th>
                                                        <th>Estimated Quantity</th>
                                                        <th>Estimated Rate</th>
                                                    </tr>
                                                </thead>
                                                {!! Form::open(['id'=>'createBom','class'=>'ajax-form','method'=>'POST']) !!}
                                                {{ csrf_field() }}
                                                <thead>
                                                    @foreach($products as $key=>$pro)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td><input type="hidden" name="id[]" value="{{ $pro->id }}">{{ get_local_product_name($pro->product_id) }}</td>
                                                            <td>{{ get_trade_name($pro->trade_id) }}</td>
                                                            <td>
                                                                @php $units = explode(',', $pro->unit_id); @endphp
                                                                @foreach($units as $unit)
                                                                    {{ get_unit_name($unit).',' }}
                                                                @endforeach
                                                            </td>
                                                            <td><input type="text" name="est_qty[]" class="form-control" placeholder="Estimated Quantity" value="@if($pro->est_qty !== null) {{ $pro->est_qty }} @endif"></td>
                                                            <td><input type="text" name="est_rate[]" class="form-control" placeholder="Estimated Rate" value="@if($pro->est_rate !== null) {{ $pro->est_rate }} @endif"></td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="6"><button type="button" id="updateBom" class="btn btn-success" style="float: right;"> <i class="fa fa-check"></i> @lang('app.save')</button></td>
                                                    </tr>
                                                </thead>
                                                {!! Form::close() !!}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading....
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $('.createTaskCategory').click(function(){
        var url = '{{ route('admin.projects.bom',[$project->id, $store->id])}}';
        $('#modelHeading').html("Add Material");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $('#updateBom').click(function () {
        $.easyAjax({
            url: '{{route('admin.projects.updateBom')}}',
            container: '#createBom',
            type: "POST",
            data: $('#createBom').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })
    $('ul.showProjectTabs .Bom').addClass('tab-current');
</script>
@endpush
