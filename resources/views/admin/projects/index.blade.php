@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i  class=" {{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <style>
        .custom-action a {
            margin-right: 15px;
            margin-bottom: 15px;
        }
        .custom-action a:last-child {
            margin-right: 0px;
            float: right;
        }

        .dashboard-stats .white-box .list-inline {
            margin-bottom: 0;
        }

        .dashboard-stats .white-box {
            padding: 10px;
        }

        .dashboard-stats .white-box .box-title {
            font-size: 13px;
            text-transform: capitalize;
            font-weight: 300;
        }
        @media all and (max-width: 767px) {
            .custom-action a {
                margin-right: 0px;
            }

            .custom-action a:last-child {
                margin-right: 0px;
                float: none;
            }
        }
    </style>
@endpush
@section('content')
    <div class="white-box">
    <div class="row">
        <div class="col-md-12">
            <div class="transparent-box">
                <div class="row">
                    @if(in_array('projects', $user->modules))
                        <div class="col-sm-12">
                            <div class="form-group custom-action">
                                <a href="{{ route('admin.projects.selectProjects') }}" class="btn btn-outline btn-success btn-sm">@lang('app.selectprojectshere') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                <a href="{{ route('admin.projects.create') }}" class="btn btn-outline btn-success btn-sm">@lang('modules.projects.addNewProject') <i class="fa fa-plus" aria-hidden="true"></i></a>
                               {{--  <a href="javascript:;" id="createProject" class="btn btn-outline btn-info btn-sm">@lang('modules.projectCategory.addProjectCategory') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                  <a href="{{ route('admin.projects.gantt') }}" class="btn btn-outline btn-danger btn-sm"><i class="fa fa-bar-chart" aria-hidden="true"></i> @lang('modules.projects.viewGanttChart')</a>
                                  <a href="{{ route('admin.project-template.index') }}"  class="btn btn-outline btn-primary btn-sm">@lang('app.menu.addProjectTemplate') <i class="fa fa-plus" aria-hidden="true"></i></a>
                                  <a href="{{ route('admin.projects.archive') }}"  class="btn btn-outline btn-info btn-sm">@lang('app.menu.viewArchive') <i class="fa fa-trash" aria-hidden="true"></i></a>
                                  <a href="javascript:;" onclick="exportData()" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                            </div>
                        </div>
                    @elseif(in_array('stores', $user->modules))
                        <div class="col-sm-2">
                            <div class="form-group">
                                <a href="{{ route('member.projects.create') }}" class="btn btn-outline btn-success btn-sm">@lang('modules.projects.addNewProject') <i class="fa fa-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    @endif
                </div>

                </div>
        </div>
    </div>

    <!-- .row -->
    @if(count($projectListarray)>0)
      <div class="table-responsive">
          <table class="table table-bordered" id="project-table">
              <thead>
              @if(in_array('projects', $user->modules) && in_array('stores', $user->modules))
                  <tr>
                      <th>@lang('app.sno')</th>
                      <th>@lang('modules.projects.projectName')</th>
                      <th>@lang('app.type')</th>
                      <th>@lang('app.startdate')</th>
                      <th>@lang('app.deadline')</th>
                      <th>@lang('app.duration')</th>
                      <th>@lang('app.status')</th>
                      <th>@lang('app.action')</th>
                  </tr>
              @endif
              </thead>
          </table>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box text-center">
                        <p>Projects not found</p>
                        <a href="{{ route('admin.projects.selectProjects') }}" class="btn btn-outline btn-success btn-sm">@lang('app.selectprojectshere') <i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>
        @endif
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="projectCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
      </div>

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>

<script>
    var table;
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    $('.select2').val('all');
    $(function() {
      /*  showData();*/
        $('body').on('click', '.archive', function(){
            var id = $(this).data('user-id');
            swal({
                title: "@lang('messages.sweetAlertTitle')",
                text: "@lang('messages.archiveMessage')",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "@lang('messages.confirmArchive')",
                cancelButtonText: "@lang('messages.confirmNoArchive')",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.projects.archive-delete',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'GET',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('project-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted project!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.projects.destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#projectid"+id).remove();
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('#createProject').click(function(){
            var url = '{{ route('admin.projectCategory.create')}}';
            $('#modelHeading').html('Manage Project Category');
            $.ajaxModal('#projectCategoryModal',url);
        })

    });

    function initCounter() {
        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });
    }
    showData();
    function showData() {
        var status = $('#status').val();
        var clientID = $('#client_id').val();
        @if(in_array('projects', $user->modules) && in_array('stores', $user->modules))
            var searchQuery = "?status="+status+"&client_id="+clientID;
            table = $('#project-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: '{!! route('admin.projects.data') !!}'+searchQuery,
                "order": [[ 0, "desc" ]],
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'project_name', name: 'project_name'},
                    { data: 'type', name: 'type' },
                    { data: 'startdate', name: 'startdate' },
                    { data: 'deadline', name: 'deadline' },
                    { data: 'project_duration', name: 'project_duration' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action' }
                ]
            });
        @elseif(!in_array('projects', $user->modules) && in_array('stores', $user->modules))
            var searchQuery = "?status="+status;
            table = $('#project-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: '{!! route('member.projects.dataSimple') !!}'+searchQuery,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'project_name', name: 'project_name'},
                    { data: 'category_id', name: 'category_id' },
                    { data: 'status', name: 'status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'added_by', name: 'added_by' },
                    { data: 'action', name: 'action' }
                ]
            });
        @endif
    }

    $('#status').on('change', function(event) {
        event.preventDefault();
        showData();
    });

    $('#client_id').on('change', function(event) {
        event.preventDefault();
        showData();
    });

    initCounter();

    function exportData(){

        var status = $('#status').val();
        var clientID = $('#client_id').val();

        var url = '{{ route('admin.projects.export', [':status' ,':clientID']) }}';
        url = url.replace(':clientID', clientID);
        url = url.replace(':status', status);
        // alert(url);
        window.location.href = url;
    }

</script>
@endpush