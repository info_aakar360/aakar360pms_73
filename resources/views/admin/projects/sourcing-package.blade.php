@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.menu.sourcing-packages')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    {{--@include('admin.projects.show_project_menu')--}}

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <select class="select2 form-control" data-placeholder="@lang('app.selectProject')" id="project_id" >
                                            <option value="all">@lang('modules.client.all')</option>
                                            @foreach($projectlist as $projectddata)
                                                <option value="{{ $projectddata->id }}" <?php if(!empty($project)&&$project->id==$projectddata->id){ echo 'selected';}?>>{{ ucwords($projectddata->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(!empty($project)){?>
                    <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h4>Sourcing Packages
                                        <a href="{{ route('admin.projects.addSourcingPackages', [$id]) }}" class="btn btn-outline btn-success btn-sm createTaskCategory" style="float: right; margin-right: 10px;">Create New <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </section>
        </div>
    </div>
    <?php if(!empty($project)){?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <table class="table-responsive table-responsive" id="example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>BOQ</th>
                                <th>Category</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($packages as $key=>$package)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $package->title }}</td>
                                    <td>
                                        @if($package->create_from !== null)
                                            <?php $create = json_decode($package->create_from) ?>
                                            @foreach($create as $cf)
                                                {{ get_title($cf).', ' }}
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if($package->category !== null)
                                            <?php $cat = json_decode($package->category) ?>
                                            @foreach($cat as $c)
                                                {{ get_category($c).', ' }}
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if($package->type !== null)
                                            <?php $type = json_decode($package->type) ?>
                                            @foreach($type as $t)
                                                {{ $t.', ' }}
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.projects.editSourcingPackages', [$id, $package->id])}}" class="btn btn-info btn-circle"
                                           data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>

                                        <a href="{{ route('admin.projects.tendersList', [$id, $package->id])}}" class="btn btn-primary btn-circle"
                                           data-toggle="tooltip" data-task-id="{{ $package->id }}" data-original-title="Tender List">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </a>

                                        <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                           data-toggle="tooltip" data-task-id="{{ $package->id }}" data-original-title="Delete">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.projects.addSourcingPackages', [$id]) }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <table class="table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12">
                            @csrf
                            <input type="text" class="form-control" name="title" placeholder="Title *" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    <?php }?>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/sourcingPackages') }}'+'/'+projectId;

        });
        <?php if(!empty($project)){?>
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Sourcing').addClass('tab-current');
       $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('task-id');
            var recurring = $(this).data('recurring');

            var buttons = {
                cancel: "No, cancel please!",
                confirm: {
                    text: "Yes, delete it!",
                    value: 'confirm',
                    visible: true,
                    className: "danger",
                }
            };

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted punch item!",
                dangerMode: true,
                icon: 'warning',
                buttons: buttons
            }).then(function (isConfirm) {
                if (isConfirm == 'confirm' || isConfirm == 'recurring') {

                    var url = "{{ route('admin.projects.deleteSourcingPackages',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    var dataObject = {'_token': token, '_method': 'DELETE'};

                    if(isConfirm == 'recurring')
                    {
                        dataObject.recurring = 'yes';
                    }

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: dataObject,
                        success: function (response) {
                            if (response.status == "success") {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
        <?php }?>
    </script>

@endpush