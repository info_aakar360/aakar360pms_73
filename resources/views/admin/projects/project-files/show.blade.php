@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li ><a class="active" href="{{ route('admin.manage-drawings.index') }}">{{ __($pageTitle) }}</a></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

    <style>
        .file-bg {
            height: 150px;
            overflow: hidden;
            position: relative;
        }
        .file-bg .overlay-file-box {
            opacity: .9;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            text-align: center;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    {{--@include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="files-list-panel">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2 style="color: #002f76">{{ __($pageTitle) }}</h2>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="{{ route('admin.manage-drawings.index') }}" class="btn btn-outline btn-success btn-sm">Go back</a>
                                            </div>
                                        </div>
                                        <div class="row m-b-10">
                                            <?php if(!empty($_GET['path'])){ ?>
                                           <div class="col-md-2">
                                                <a href="javascript:;" id="show-folder-create-modal"
                                                   class="btn btn-success btn-outline createTitle"><i class="ti-folder"></i> @lang('modules.projects.createFolder')</a>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" id="show-dropzone"
                                                   class="btn btn-success btn-outline"><i class="ti-upload"></i> @lang('modules.projects.uploadFile')</a>
                                            </div>
                                            {{--<div class="col-md-2">
                                                <a href="javascript:;" id="show-link-form"
                                                   class="btn btn-success btn-outline"><i class="ti-link"></i> @lang('modules.projects.addFileLink')</a>
                                            </div>--}}
                                            <?php } ?>
                                        </div>

                                        <div class="row m-b-20 hide" id="file-dropzone-box">
                                            <div class="col-md-12">
                                                <form method="post" id="updateTask" action="{{ route('admin.manage-drawings.store') }}">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Revision file</label>
                                                            <select class="form-control" name="revisionid">
                                                                <option value="">Select Drawing</option>
                                                                <?php if(!empty($files)){
                                                                    foreach ($files as $filedata){
                                                                            if($filedata->type=='file'){  ?>
                                                                            <option value="{{ $filedata->id }}">{{ $filedata->filename }}</option>
                                                                        <?php } }     }?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Name</label>
                                                            <input type="text" class="form-control" name="name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <textarea  class="form-control" rows="7" name="description" ></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Files</label>
                                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                        <div id="file-upload-box" >
                                                            <div class="row" id="file-dropzone">
                                                                <div class="col-md-12">
                                                                    <div class="dropzone dropheight"
                                                                         id="file-upload-dropzone">
                                                                        {{ csrf_field() }}
                                                                        <div class="fallback">
                                                                            <input name="file" type="file"  accept="image/jpeg,image/png,application/pdf" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="fileID" id="fileID">
                                                    </div>
                                                    <div class="col-md-12 text-right" >
                                                        <div class="form-group">
                                                            <button type="submit" id="update-file" class="btn btn-success">@lang('app.upload')</button>
                                                            <input type="hidden" class="form-control" id="fileparent" name="parent" value="<?=isset($_GET['path'])? $_GET['path'] :'0';?>" placeholder="Folder Name *">
                                                            <input type="hidden" class="form-control" name="project_id" id="projectID" value="{{ !empty($filemanager->project_id) ? $filemanager->project_id : '' }}" required>
                                                            <input type="hidden" class="form-control" name="pathtype" value="outline">
                                                            <input name="view" type="hidden" id="view" value="list">
                                                            {{ csrf_field() }}
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="row m-b-20 hide" id="file-link">
                                            {!! Form::open(['id'=>'file-external-link','class'=>'ajax-form','method'=>'POST']) !!}
                                            <input type="hidden" class="form-control" name="parent" value="<?=!empty($_GET['path']) ? $_GET['path']:'0';?>" placeholder="Folder Name *">
                                             <input name="view" type="hidden" id="view" value="list">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">@lang('app.name')</label>
                                                    <input type="text" name="filename" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label for="">@lang('modules.projects.addFileLink')</label>
                                                    <input type="text" name="external_link" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input name="pathtype" type="hidden"  value="outline">
                                                    <button class="btn btn-success" id="save-link">@lang('app.submit')</button>
                                                </div>
                                            </div>

                                            {!! Form::close() !!}
                                        </div>

                                    {{--<ul class="nav nav-tabs" role="tablist" id="list-tabs">--}}
                                    {{--<li role="presentation" class="active nav-item" data-pk="list"><a href="#list" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">  @lang('app.list')</span></a></li>--}}
                                    {{--<li role="presentation" class="nav-item" data-pk="thumbnail"><a href="#thumbnail" class="nav-link thumbnail" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">@lang('app.thumbnail')</span></a></li>--}}
                                    {{--</ul>--}}
                                    <!-- Tab panes -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="timelog-table">
                                                <thead>
                                                <th>@lang('app.sno')</th>
                                                <th>File Name</th>
                                                <?php if(!empty($_GET['path'])){?>
                                                <th>Size</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Revision</th>
                                                <?php }?>
                                                <th>Created at</th>
                                                <?php if(!empty($_GET['path'])){?>
                                                <th>Action</th>
                                                <?php }?>
                                                </thead>
                                                <tbody>
                                                <?php $sno=1;?>
                                                @foreach($files as $file)
                                                    <?php
                                                    $showlist= true;
                                                    if($file->foldertype=='private'&&$user->id!=$file->user_id){
                                                        $showlist= false;
                                                    }
                                                    if($showlist){
                                                    $path = $file->id;
                                                    $revisionfiles = \App\FileManager::selectRaw("*,count(*) as count")->where('revisionfileid',$file->id)->orderBy('id','desc')->first();
                                                    ?>
                                                    <tr id="file{{ $file->id }}">
                                                        <td>{{ $sno }}</td>
                                                        <td>
                                                            @if($file->type == 'file')
                                                                <i class="ti-file"></i> {{ $file->filename }}
                                                            @else
                                                                <a href="{{url('admin/manage-drawings/manage-drawings')}}?path={{$path}}">
                                                                    <i class="ti-folder"></i> {{ $file->filename }}
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <?php if(!empty($_GET['path'])){?>
                                                        <td>{{ filesizes($file->size) }} @if($file->type == 'file') MB @endif</td>
                                                        <td>{{ $file->name }}</td>
                                                        <td>{{ $file->description }}</td>
                                                        <td>@if(!empty($revisionfiles))<a href="{{ route('admin.manage-drawings.revisionFiles',[$file->project_id,$file->id]) }}?path={{ $_GET['path'] }}">{{  $revisionfiles->count }}</a>@else 0 @endif</td>

                                                        <?php }?>
                                                        <td><span class="m-l-10">{{ date("d-m-Y", strtotime($file->created_at)) }}</span></td>
                                                        <?php if(!empty($_GET['path'])){?>
                                                        <td >
                                                            <a href="javascript:;" onclick="showName({{ $file->id }});"
                                                               data-toggle="tooltip" data-original-title="Rename"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-edit"></i></a>
                                                            {{--<a href="javascript:void(0);" class="btn btn-warning btn-circle movefile" data-file="{{ $file->id }}"  data-toggle="tooltip" data-original-title="Move to Outline" ><i class="fa fa-reply-all"></i></a>--}}
                                                            @if($file->type == 'file')
                                                                @if($file->external_link != '')
                                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif($storage == 'local')
                                                                    <?php
                                                                    $fx = explode('.', $file->hashname);
                                                                    $ext = $fx[(count($fx)-1)];
                                                                    ?>
                                                                    <?php
                                                                    $folder = '';
                                                                    $x=1;
                                                                    $p = $file->parent;
                                                                    while($x==1){
                                                                        $fn = \App\FileManager::where('id', $p)->where('type', 'folder')->first();
                                                                        if($fn !== null){
                                                                            $folder = $fn->id.'/'.$folder;
                                                                            $p = $fn->parent;
                                                                        }else{
                                                                            $x=0;
                                                                        }
                                                                    }
                                                                    $html5class = '';
                                                                    if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                                        $html5class = 'fancybox';
                                                                    }
                                                                    $fileurl = uploads_url().'project-files/'.$file->project_id.'/'.$folder.$file->hashname;
                                                                    ?>
                                                                    <a target="_blank" href="{{ $fileurl }}" download="{{ $file->filename }}" data-toggle="tooltip" data-original-title="Download"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-download"></i></a>

                                                                    @if($ext != 'pdf')

                                                                        <a href="javascript:void(0);" onClick="copyLink('id{{ $file->id }}')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>
                                                                        &nbsp;<a href="https://api.whatsapp.com/send?text={{ $fileurl }}" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                                                        <br><span style="display: none;" id="id{{ $file->id }}">{{ $fileurl }}</span>
                                                                        <a target="_blank" href="{{ $fileurl }}"
                                                                           data-toggle="tooltip" data-original-title="View"
                                                                           class="btn btn-info btn-circle {{ $html5class }}"><i
                                                                                    class="fa fa-search"></i></a>
                                                                    @else

                                                                        <a href="javascript:void(0);" onClick="copyLink('id{{ $file->id }}')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>
                                                                        &nbsp;<a href="https://api.whatsapp.com/send?text={{ $fileurl }}" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                                                        <br><span style="display: none;" id="id{{ $file->id }}">{{ $fileurl }}</span>
                                                                        <a href="{{ route('admin.projects.projectEditFile', [$file->project_id, $file->id]) }}"
                                                                           data-toggle="tooltip" data-original-title="View"
                                                                           class="btn btn-info btn-circle"><i
                                                                                    class="fa fa-eye"></i></a>
                                                                    @endif
                                                                @elseif($storage == 's3')
                                                                    <?php
                                                                    $fx = explode('.', $file->hashname);
                                                                    $ext = $fx[(count($fx)-1)];
                                                                    $folder = '';
                                                                    $x=1;
                                                                    $p = $file->parent;
                                                                    while($x==1){
                                                                        $fn = \App\FileManager::where('id', $p)->where('type', 'folder')->first();
                                                                        if($fn !== null){
                                                                            $folder = $fn->id.'/'.$folder;
                                                                            $p = $fn->parent;
                                                                        }else{
                                                                            $x=0;
                                                                        }
                                                                    }
                                                                    $html5class = '';
                                                                    if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                                        $html5class = 'fancybox';
                                                                    }
                                                                    $fileurl = awsurl().'project-files/'.$file->project_id.'/'.$folder.$file->hashname;
                                                                    ?>
                                                                    <a target="_blank" href="{{ $fileurl }}" download="{{ $file->filename }}" data-toggle="tooltip" data-original-title="Download"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-download"></i></a>
                                                                    @if($ext != 'pdf')

                                                                        <a href="javascript:void(0);" onClick="copyLink('id{{ $file->id }}')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>
                                                                        &nbsp;<a href="https://api.whatsapp.com/send?text={{ $fileurl }}" class="btn btn-success btn-circle" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                                                        <br><span style="display: none;" id="id{{ $file->id }}">{{ $fileurl }}</span>
                                                                        <a target="_blank" href="{{ $fileurl }}"
                                                                           data-toggle="tooltip" data-original-title="View"
                                                                           class="btn btn-info btn-circle {{ $html5class }}"><i
                                                                                    class="fa fa-search"></i></a>
                                                                    @else

                                                                        <a href="{{ route('admin.projects.projectEditFile', [$file->project_id, $file->id]) }}"
                                                                           data-toggle="tooltip" data-original-title="View"
                                                                           class="btn btn-info btn-circle"><i
                                                                                    class="fa fa-eye"></i></a>
                                                                    @endif
                                                                @elseif($storage == 'google')
                                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif($storage == 'dropbox')
                                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @endif

                                                                {{--@if(is_null($file->external_link))--}}
                                                                {{--&nbsp;&nbsp;--}}
                                                                {{--<a href="{{ route('admin.files.download', $file->id) }}"--}}
                                                                {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                                {{--class="btn btn-inverse btn-circle"><i--}}
                                                                {{--class="fa fa-download"></i></a>--}}
                                                                {{--@endif--}}
                                                            @endif
                                                            @if($file->type == 'folder')
                                                                @if($file->locked == '0')
                                                                    <a href="javascript:;" onclick="editModal({{ $file->id }}, '{{ $file->filename }}');"
                                                                       data-toggle="tooltip" data-original-title="Rename"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-pencil"></i></a>
                                                                @endif
                                                            @endif
                                                            @if($file->locked == '0')
                                                                <a href="javascript:void(0);" data-toggle="tooltip"
                                                                   data-original-title="Delete"
                                                                   data-file-id="{{ $file->id }}"
                                                                   class="btn btn-danger btn-circle sa-params-delete" data-pk="list"><i
                                                                            class="fa fa-times"></i></a>
                                                            @endif
                                                        </td>
                                                        <?php }?>
                                                    </tr>
                                                    <?php
                                                    $sno++; }
                                                    ?>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->
    <?php if(!empty($_GET['path'])){  ?>
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" id="createFolder" action="#">
                    <div class="modal-header">
                        <button type="button" class="close closeModel" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Create Folder</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="foldername" placeholder="Folder Name *" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <textarea name="description" class="form-control" placeholder="Description" ></textarea>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label><input type="radio" name="foldertype" value="public" checked > Public</label>
                                <label><input type="radio" name="foldertype" value="private" > Private</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="form-control" name="parent" value="{{ $filemanager->id }}" required>
                        <input type="hidden" class="form-control" name="project_id" value="{{ $filemanager->project_id }}" required>
                        <input type="hidden" class="form-control" name="type" value="folder" required>
                        <input type="hidden" class="form-control" name="pathtype" value="outline" required>
                        {{ csrf_field() }}
                        <button type="submit" name="submit" id="save-folder"  class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" id="editFileIDSubmit" action="{{ route('admin.manage-drawings.editFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close closeEditButton" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Edit Folder Name</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="foldername" id="editData" placeholder="Folder Name *" required>
                                <input type="hidden" name="fileid"  id="editFileIDData" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="submit" name="submit" id="save-file-update" class="btn blue">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="showName" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" id="createNameEdit" action="{{ route('admin.manage-drawings.editName') }}">
                    <div class="modal-header">
                        <button type="button" class="close closeEditButton" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Edit Name</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" id="editNameData" required>
                                <input type="hidden" name="fileid"  id="editNameIDData" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <textarea name="description" class="form-control" placeholder="Description"  id="editDescData" ></textarea>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label><input type="radio" name="foldertype" value="public" class="editFolType" > Public</label>
                                <label><input type="radio"  name="foldertype"  value="private" class="editFolType" > Private</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="submit" name="submit" id="save-form-name" class="btn btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    <?php }?>


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script>
     /*   $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ route('admin.manage-drawings.index') }}'+'/'+projectId;

        });*/

     $('#timelog-table').dataTable({
         stateSave: true
     });
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })

        $('.closeModel').click(function(){
            $('#taskCategoryModal').hide();
        })

        $('.closeEditButton').click(function(){
            $('#showName').hide();
            $('#editModal').hide();
        })

     function editModal(val, name) {
         $('#editModal').show();
         $('#modelHeading').html("Edit Name");
         $('#editData').val(name);
         $('#editFileIDData').val(val);

     }
     function showName(val) {
         $('#showName').show();
         $('#namemodelHeading').html("Edit Name");
         var fileid = $(this).data('file');
         var token = '{{ csrf_token() }}';
         $.easyAjax({
             url: '{{ route('admin.manage-drawings.showName') }}',
             type: "POST",
             data: {
                 '_token':token,
                 'fileid':val
             },
             success: function (response) {
                 $('#editNameIDData').val(response.id);
                 $('#editNameData').val(response.name);
                 $('#editDescData').text(response.description);
                 if(response.foldertype=='private'){
                     $('.editFolType[value="private"]').prop('checked', true);
                 }else{
                     $('.editFolType[value="public"]').prop('checked', true);
                 }
             }
         });
     }
        $('#show-dropzone').click(function () {
            $('#file-dropzone-box').toggleClass('hide show');
        });

        $('#show-link-form').click(function () {
            $('#file-link').toggleClass('hide show');
        });

        $("body").tooltip({
            selector: '[data-toggle="tooltip"]'
        });
     Dropzone.autoDiscover = false;
     //Dropzone class
     myDropzone = new Dropzone("div#file-upload-dropzone", {
         url: "{{ route('admin.manage-drawings.storeImage') }}",
         headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
         paramName: "file",
         maxFilesize: 50,
         maxFiles: 1,
         autoProcessQueue: false,
         acceptedFiles: '.png, .jpeg, .jpg, .pdf',
         uploadMultiple: false,
         addRemoveLinks:true,
         parallelUploads:1,
         maxfilesexceeded: function(file) {
             this.removeAllFiles();
             this.addFile(file);
         },
         init: function () {
             myDropzone = this;
            /* this.on("error", function(file){
                 var msgs = "File type not supported";
                 $.showToastr(msgs, 'error');
                 $('#update-file').prop('disabled', true);
             });
             this.on("success", function (file) {
                 var msgs = "File uploading successful.";
                 $.showToastr(msgs, 'success');
                 $('#update-file').prop('disabled', false);
             });*/
            this.on("complete", function (file) {
                 if (this.getUploadingFiles().length === 0) {
                     var msgs = "File updated successfully";
                     $.showToastr(msgs, 'success');
                     <?php if(!empty($_GET['path'])){?>
                         window.location.href = '{{route('admin.manage-drawings.index')}}?path=<?php echo $_GET['path'];?>';
                     <?php }else{?>
                         window.location.href = '{{route('admin.manage-drawings.index')}}';
                     <?php }?>
                 }
             });
         }
     });

     myDropzone.on('sending', function(file, xhr, formData) {
         console.log(myDropzone.getAddedFiles().length,'sending');
         var ids = $("#fileID").val();
         formData.append('file_id', ids);
         var pids = $("#projectID").val();
         formData.append('project_id', pids);
         var filepa = $("#fileparent").val();
         formData.append('parent', filepa);
     });
     myDropzone.on('completemultiple', function () {
         var msgs = "File updated successfully";
         $.showToastr(msgs, 'success');
         var filepa = $("#fileparent").val();
     });
     //    update task
     $('#update-file').click(function (e) {
         var filepa = $("#fileparent").val();
         e.preventDefault();
         $("#update-file").attr('disabled','disabled');
         $.easyAjax({
             url: '{{route('admin.manage-drawings.store')}}',
             container: '#updateTask',
             type: "POST",
             data: $('#updateTask').serialize(),
             success: function(response){
                 if(myDropzone.getQueuedFiles().length > 0){
                     fileID = response.fileID;
                     $('#fileID').val(response.fileID);
                     myDropzone.processQueue();
                     $("#update-file").removeAttr('disabled');
                 }
                 else{
                     var msgs = "File updated successfully";
                     $.showToastr(msgs, 'success');
                     <?php if(!empty($_GET['path'])){?>
                         window.location.href='{{route('admin.manage-drawings.index')}}?path=<?php echo $_GET['path'];?>';
                     <?php }else{?>
                         window.location.href='{{route('admin.manage-drawings.index')}}';
                     <?php }?>
                     $("#update-file").removeAttr('disabled');
                 }
             }
         })
     });
     $('#save-folder').click(function (e) {
         e.preventDefault();
         $.easyAjax({
             url: '{{ route('admin.manage-drawings.createFolder') }}',
             container: '#createFolder',
             type: "POST",
             redirect: true,
             data: $('#createFolder').serialize(),
             success: function(response){
                 <?php if(!empty($filemanager->id)){?>
                     window.location.href = '{{ route('admin.manage-drawings.index') }}?path={{ $filemanager->id }}';
                 <?php }else{?>
                     window.location.href = '{{ route('admin.manage-drawings.index') }}';
                 <?php }?>
             }
         })
     });

      /*  // "myAwesomeDropzone" is the camelized version of the HTML element's ID
        Dropzone.options.fileUploadDropzone = {
            paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
            dictDefaultMessage: "@lang('modules.projects.dropFile')",
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("success", function (file, response) {
                   /!* var viewName = $('#view').val();
                    if(viewName == 'list') {
                        $('#files-list-panel ul.list-group').html(response.html);
                    } else {
                        $('#thumbnail').empty();
                        $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                    }*!/
                })
            }
        };*/

        $('body').on('click', '.sa-params-delete', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.manage-drawings.destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            $("#file"+id).remove();
                        }
                    });
                }
            });
        });

        /*$('.thumbnail').on('click', function(event) {
            event.preventDefault();
            $('#thumbnail').empty();
            $.easyAjax({
                type: 'GET',
                data: {
                  id: projectID
                },
                success: function (response) {
                    $(response.view).hide().appendTo("#thumbnail").fadeIn(500);
                }
            });
        });*/

        $('#save-link').click(function () {
            $.easyAjax({
                url: '{{route('admin.manage-files.storeLink')}}',
                container: '#file-external-link',
                type: "POST",
                redirect: true,
                data: $('#file-external-link').serialize(),
                success: function () {
                  window.location.reload();
                }
            })
        });

        $('#list-tabs').on("shown.bs.tab",function(event){
            var tabSwitch = $('#list').hasClass('active');
            if(tabSwitch == true) {
                $('#view').val('list');
            } else {
                $('#view').val('thumbnail');
            }
        });
     $('#save-form-name').click(function (e) {
         e.preventDefault();
         $.easyAjax({
             url: '{{route('admin.manage-drawings.editName')}}',
             container: '#createNameEdit',
             type: "POST",
             redirect: true,
             data: $('#createNameEdit').serialize(),
             success: function(response){
                 $('#showName').hide();
                 $('#createNameEdit')[0].reset();
                 location.reload();
             }
         })
     });
     $('#save-file-update').click(function (e) {
         e.preventDefault();
         $.easyAjax({
             url: '{{route('admin.manage-drawings.editFolder')}}',
             container: '#editFileIDSubmit',
             type: "POST",
             redirect: true,
             data: $('#editFileIDSubmit').serialize(),
             success: function(response){
                 $('#editModal').hide();
                 $('#editFileIDSubmit')[0].reset();
                location.reload();
             }
         })
     });
        $('ul.showProjectTabs .projectFiles').addClass('tab-current');

     function copyLink(id){
         var copyText = document.getElementById(id);
         copyToClipboard(copyText);
     }
     function copyToClipboard(elem) {
         // create hidden text element, if it doesn't already exist
         var targetId = "_hiddenCopyText_";
         var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
         var origSelectionStart, origSelectionEnd;
         if (isInput) {
             // can just use the original source element for the selection and copy
             target = elem;
             origSelectionStart = elem.selectionStart;
             origSelectionEnd = elem.selectionEnd;
         } else {
             // must use a temporary form element for the selection and copy
             target = document.getElementById(targetId);
             if (!target) {
                 var target = document.createElement("textarea");
                 target.style.position = "absolute";
                 target.style.left = "-9999px";
                 target.style.top = "0";
                 target.id = targetId;
                 document.body.appendChild(target);
             }
             target.textContent = elem.textContent;
         }
         // select the content
         var currentFocus = document.activeElement;
         target.focus();
         target.setSelectionRange(0, target.value.length);

         // copy the selection
         var succeed;
         try {
             succeed = document.execCommand("copy");
         } catch(e) {
             succeed = false;
         }
         // restore original focus
         if (currentFocus && typeof currentFocus.focus === "function") {
             currentFocus.focus();
         }
         if (isInput) {
             // restore prior selection
             elem.setSelectionRange(origSelectionStart, origSelectionEnd);
         } else {
             // clear temporary content
             target.textContent = "";
         }
         return succeed;
     }

    </script>
@endpush
