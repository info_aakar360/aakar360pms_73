@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title active">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a class="active" href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.projects.files')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .file-bg {
            height: 150px;
            overflow: hidden;
            position: relative;
        }
        .file-bg .overlay-file-box {
            opacity: .9;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            text-align: center;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    {{--@include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="files-list-panel">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-md-9">
                                            </div>
                                            <div class="col-md-3">
                                                <a href="{{ route('admin.manage-drawings.index') }}" class="btn btn-outline btn-success btn-sm">Go back</a>
                                            </div>
                                        </div>

                                        @if(session()->has('message'))
                                            <div class="alert alert-info">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <div class="row">
                                        <div class="col-md-12 text-center">
                                        <?php
                                            $filePath = $files[0]->hashname;
                                            $mime = explode('.', $filePath);
                                            $urlx = $filePath;
                                            $ext = $mime[count($mime)-1];
                                            $parent = $files[0]->parent;
                                            $filePath = $files[0]->hashname;
                                            while ($parent){
                                                $filep = \App\FileManager::findOrFail($parent);
                                                /*$filePath = $filep->filename.'/'.$filePath;*/
                                                $filePath = $filep->filename.'/';
                                                $parent = $filep->parent;
                                            }
                                                $urlx = url('/admin/projects/edit-file/'.$files[0]->project_id.'/'.$files[0]->id); ?>
                                            <iframe src="{{ $url.'project-files/'.$filePath.$files[0]->hashname }}" width="100%" height="500px"></iframe>
                                        </div>
                                    <!-- Tab panes -->
                                        <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-stripped">
                                                <thead>
                                                <th>Sl no</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Size</th>
                                                <th>Last Modified</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                @php $c = count($files); @endphp
                                                @forelse($files as $file)
                                                    <?php
                                                    $path = $file->id;
                                                    ?>
                                                    <tr ID="row{{ $file->id }}">
                                                        <td>{{ $c }}</td>
                                                        <td>
                                                            @if($file->type == 'file')
                                                                <i class="ti-file"></i> {{ $file->filename }}
                                                            @else
                                                                <a href="{{url('admin/manage-drawings/manage-drawings')}}?path={{$path}}">
                                                                    <i class="ti-folder"></i> {{ $file->filename }}
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td>{{ $file->type }}</td>
                                                        <td>{{ $file->size }} Bytes</td>
                                                        <td><span class="m-l-10">{{ date("d-m-Y", strtotime($file->created_at)) }}</span></td>
                                                        <td>
                                                            <a href="javascript:;" onclick="showName({{ $file->id }});"
                                                               data-toggle="tooltip" data-original-title="Rename"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-edit"></i></a>
                                                            @if($file->type == 'file')
                                                                @if($file->external_link != '')
                                                                    <a href="{{ $file->external_link }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif(config('filesystems.default') == 'local')
                                                                    @if(isset($_GET['path']))
                                                                        <?php
                                                                        $parent = $file->parent;
                                                                        $filePath = $file->hashname;
                                                                        while ($parent){
                                                                            $filep = \App\FileManager::findOrFail($parent);
                                                                            $filePath = $filep->filename.'/'.$filePath;
                                                                            $parent = $filep->parent;
                                                                        }
                                                                        $parent = $file->parent;
                                                                        $filePath = $file->hashname;
                                                                        while ($parent){
                                                                            $filep = \App\FileManager::findOrFail($parent);
                                                                            $filePath = $filep->filename.'/'.$filePath;
                                                                            $parent = $filep->parent;
                                                                        }
                                                                        ?>
                                                                        <a  href="{{ asset('user-uploads/project-files/'.$filePath.'/'.$file->hashname) }}"></a>
                                                                        <a  href="{{ asset('user-uploads/project-files/'.$filePath) }}"
                                                                            data-toggle="tooltip" data-original-title="View"
                                                                            class="btn btn-info btn-circle"><i
                                                                                    class="fa fa-search"></i></a>
                                                                    @else
                                                                        <?php
                                                                        $parent = $file->parent;
                                                                        $filePath = $file->hashname;
                                                                        while ($parent){
                                                                            $filep = \App\FileManager::findOrFail($parent);
                                                                            $filePath = $filep->filename.'/'.$filePath;
                                                                            $parent = $filep->parent;
                                                                        }
                                                                        ?>
                                                                        <a  href="{{ asset('user-uploads/project-files/'.$filePath.'/'.$file->hashname) }}"
                                                                            data-toggle="tooltip" data-original-title="View"
                                                                            class="btn btn-info btn-circle"><i
                                                                                    class="fa fa-search"></i></a>
                                                                    @endif


                                                                @elseif(config('filesystems.default') == 's3')
                                                                    <?php
                                                                    $parent = $file->parent;
                                                                    $filePath = $file->hashname;
                                                                    $mime = explode('.', $filePath);
                                                                    while ($parent){
                                                                        $filep = \App\FileManager::findOrFail($parent);
                                                                        $filePath = $filep->filename.'/'.$filePath;
                                                                        $parent = $filep->parent;
                                                                    }

                                                                    $urlx = $filePath;
                                                                    $ext = $mime[count($mime)-1];
                                                                    ?>
                                                                    {{--  <a  href="{{ $urlx }}"--}}
                                                                    <?php
                                                                    $parent = $file->parent;
                                                                    $filePath = $file->hashname;
                                                                    while ($parent){
                                                                        $filep = \App\FileManager::findOrFail($parent);
                                                                        /*$filePath = $filep->filename.'/'.$filePath;*/
                                                                        $filePath = $filep->filename.'/';
                                                                        $parent = $filep->parent;
                                                                    }

                                                                        $urlx = url('/admin/projects/edit-file/'.$file->project_id.'/'.$file->id);
                                                                    ?>
                                                                    <a  href="{{ $urlx }}"
                                                                        data-toggle="tooltip" data-original-title="View"
                                                                        class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif(config('filesystems.default') == 'google')
                                                                    <a  href="{{ $file->google_url }}"
                                                                        data-toggle="tooltip" data-original-title="View"
                                                                        class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif(config('filesystems.default') == 'dropbox')
                                                                    <a  href="{{ $file->dropbox_link }}"
                                                                        data-toggle="tooltip" data-original-title="View"
                                                                        class="btn btn-info btn-circle"><i
                                                                                class="fa fa-search"></i></a>
                                                                @endif

                                                                {{--@if(is_null($file->external_link))--}}
                                                                {{--&nbsp;&nbsp;--}}
                                                                {{--<a href="{{ route('admin.files.download', $file->id) }}"--}}
                                                                {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                                {{--class="btn btn-inverse btn-circle"><i--}}
                                                                {{--class="fa fa-download"></i></a>--}}
                                                                {{--@endif--}}
                                                            @endif
                                                            @if($file->type == 'folder')
                                                                @if($file->locked == '0')
                                                                    <a href="javascript:;" onclick="editModal({{ $file->id }}, '{{ $file->filename }}');"
                                                                       data-toggle="tooltip" data-original-title="Rename"
                                                                       class="btn btn-info btn-circle"><i
                                                                                class="fa fa-pencil"></i></a>
                                                                @endif
                                                            @endif
                                                            @if($file->locked == '0')
                                                                <a href="javascript:;" data-toggle="tooltip"
                                                                   data-original-title="Delete"
                                                                   data-file-id="{{ $file->id }}"
                                                                   class="btn btn-danger btn-circle sa-params" data-pk="list"><i
                                                                            class="fa fa-times"></i></a></td>
                                                        @endif
                                                    </tr>
                                                    <?php $c--?>
                                                @empty
                                                    <tr>
                                                        <td colspan="5">@lang('messages.noFileUploaded')</td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        </div>

                                          </div>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.manage-files.createFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                @csrf
                                <input type="text" class="form-control" name="foldername" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="parent" value="<?=isset($_GET['path'])?$_GET['path']:'0';?>" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="type" value="folder" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="pathtype" value="outline" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeModel" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn blue">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.manage-files.editFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body" id="">
                        @csrf
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="foldername" id="editData" placeholder="Folder Name *" value="" required>
                                    <input type="hidden" class="form-control" name="id" value="0" id="folderId" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="parent" value="<?=isset($_GET['path'])?$_GET['path']:'0';?>" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="type" value="folder" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="pathtype" value="outline" placeholder="Folder Name *">
                                 </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeEditButton" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn blue">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>
    <script>
        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/showFiles') }}'+'/'+projectId;

        });
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })

        $('.closeModel').click(function(){
            $('#taskCategoryModal').hide();
        })

        $('.closeEditButton').click(function(){
            $('#editModal').hide();
        })

        function editModal(val, name) {
            $('#editModal').show();
            $('#modelHeading').html("Edit Name");
            $('#editData').val(name);
            $('#folderId').val(val);

        }
        $('#show-dropzone').click(function () {
            $('#file-dropzone').toggleClass('hide show');
        });

        $('#show-link-form').click(function () {
            $('#file-link').toggleClass('hide show');
        });

        $("body").tooltip({
            selector: '[data-toggle="tooltip"]'
        });

        // "myAwesomeDropzone" is the camelized version of the HTML element's ID
        Dropzone.options.fileUploadDropzone = {
            paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
            dictDefaultMessage: "@lang('modules.projects.dropFile')",
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("success", function (file, response) {
                    var viewName = $('#view').val();
                    if(viewName == 'list') {
                        $('#files-list-panel ul.list-group').html(response.html);
                    } else {
                        $('#thumbnail').empty();
                        $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                    }
                })
            }
        };

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.manage-files.destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $("#row"+id).remove();
                               /* if(deleteView == 'list') {
                                    $('#files-list-panel ul.list-group').html(response.html);
                                } else {
                                    $('#thumbnail').empty();
                                    $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                                }*/
                            }
                        }
                    });
                }
            });
        });

        /*$('.thumbnail').on('click', function(event) {
            event.preventDefault();
            $('#thumbnail').empty();
            $.easyAjax({
                type: 'GET',
                data: {
                  id: projectID
                },
                success: function (response) {
                    $(response.view).hide().appendTo("#thumbnail").fadeIn(500);
                }
            });
        });*/

        $('#save-link').click(function () {
            $.easyAjax({
                url: '{{route('admin.manage-files.storeLink')}}',
                container: '#file-external-link',
                type: "POST",
                redirect: true,
                data: $('#file-external-link').serialize(),
                success: function () {
                    window.location.reload();
                }
            })
        });

        $('#list-tabs').on("shown.bs.tab",function(event){
            var tabSwitch = $('#list').hasClass('active');
            if(tabSwitch == true) {
                $('#view').val('list');
            } else {
                $('#view').val('thumbnail');
            }
        });
        $('ul.showProjectTabs .projectFiles').addClass('tab-current');
    </script>
@endpush
