@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h4>{{ $tender->name }}
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th colspan="4"></th>
                            @foreach($users as $supplier)
                                <th colspan="2" class="bg-inverse text-center">{{ $supplier->name }}</th>
                            @endforeach
                        </tr>
                        <tr class="bg-inverse">
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Brand Name</th>
                            <th>Quantity</th>
                            @foreach($users as $supplier)
                                <th class="bg-inverse text-center">Rate</th>
                                <th class="bg-inverse text-center">Amount</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key=>$product)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ get_pcat_name($product->products) }}</td>
                                    <td>{{ get_pbrand_name($product->brands) }}</td>
                                    <td>{{ $product->qty }}</td>
                                    @foreach($users as $supplier)
                                        <?php
                                        $price = 0;
                                        $minQuote = 0;
                                        $q = \App\TenderBidding::where('tender_id', $tender->id)->where('user_id', $supplier->id)->first();
                                        if($q !== null){
                                            $qp = \App\TenderBiddingProduct::where('products',$product->products)->where('tender_bidding_id', $q->id)->where('brands', $product->brands)->where('qty', $product->qty)->first();
                                            if($qp !== null){
                                                $price = $qp->price;
                                            }
                                            $minQuote = get_min_price($tender->id, $product);
                                        }
                                        ?>
                                        <td class="@if($minQuote == $price) alert-success @endif text-center">{{ $price }}</td>
                                        <td class="@if($minQuote == $price) alert-success @endif text-center">{{ is_numeric($price) ? $product->qty*$price : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Sourcing').addClass('tab-current');
       $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('task-id');
            var recurring = $(this).data('recurring');

            var buttons = {
                cancel: "No, cancel please!",
                confirm: {
                    text: "Yes, delete it!",
                    value: 'confirm',
                    visible: true,
                    className: "danger",
                }
            };

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted punch item!",
                dangerMode: true,
                icon: 'warning',
                buttons: buttons
            }).then(function (isConfirm) {
                if (isConfirm == 'confirm' || isConfirm == 'recurring') {

                    var url = "{{ route('admin.projects.deleteSourcingPackages',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    var dataObject = {'_token': token, '_method': 'DELETE'};

                    if(isConfirm == 'recurring')
                    {
                        dataObject.recurring = 'yes';
                    }
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: dataObject,
                        success: function (response) {
                            if (response.status == "success") {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
    </script>

@endpush