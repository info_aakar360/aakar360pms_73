@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> @lang('app.menu.schedule')</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.menu.schedule')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">
    <link href="//cdn.dhtmlx.com/gantt/edge/skins/dhtmlxgantt_broadway.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">

    <style>
        .gantt_project {
            height: 10px !important;
        }

        .custom-project div {
            position: absolute;
        }

        .project-left, .project-right {
            top: 6px;
            background-color: transparent;
            border-style: solid;
            width: 0px;
            height: 0px;
        }

        .project-left {
            left: 0px;
            border-width: 0px 0px 8px 7px;
            border-top-color: transparent;
            border-right-color: transparent !important;
            border-bottom-color: transparent !important;
            border-left-color: #444444 !important;
        }

        .project-right {
            right: 0px;
            border-width: 0px 7px 8px 0px;
            border-top-color: transparent;
            border-right-color: #444444;
            border-bottom-color: transparent !important;
            border-left-color: transparent;
        }

        .project-line {
            font-weight: bold;
        }
        .gantt_task_drag {
            width: 6px;
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAACCAYAAAB7Xa1eAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYDDjkw3UJvAwAAABRJREFUCNdj/P//PwM2wASl/6PTAKrrBf4+lD8LAAAAAElFTkSuQmCC);
            z-index: 1;
            top: 0;
        }

        .gantt_task_drag.task_left{
            left: 0;
        }

        .gantt_task_drag.task_right{
            right: 0;
        }
        .gantt_message_area.dhtmlx_message_area {
            display: none;
        }
        .gantt_task_content{
            color:#333;
        }
        .resource_marker{
            text-align: center;
        }
        .gantt_grid_scale {
            background: #efefef;
            color: #555;
        }
        .gantt_grid_scale .gantt_grid_head_cell {
            border: 0 !important;
        }
        .gantt_task .gantt_task_scale .gantt_scale_cell {
            border: 0 !important;
            color: #666;
            font-size: 14px;
            background-color: #efefef;
        }
        .gantt_grid_scale, .gantt_task_scale{
            border: 0 !important;
        }
        .resource_marker div{
            width: 28px;
            height: 28px;
            line-height: 29px;
            display: inline-block;
            border-radius: 15px;
            color: #FFF;
            margin: 3px;
        }
        .resource_marker.workday_ok div {
            background: #51c185;
        }

        .resource_marker.workday_over div{
            background: #ff8686;
        }

        .baseline {
            position: absolute;
            border-radius: 2px;
            opacity: 0.6;
            margin-top: -7px;
            height: 30px;
            background: #a3a3a3;
        }
        .status_line {
            background-color: #0ca30a;
        }
        .bck-box {
            background: #efefef;
            color: #555;
            padding: 5px;
            border: 1px solid #bababa;
            border-bottom: 0;
        }
    </style>
@endpush

@section('content')
        <?php if(empty($project)){ ?>
        <div class="row"  >
            <?php $x=1;?>
            @foreach($projectslist as $project)
                <div class="col-md-4">
                    <div class="white-box-no-pad">
                        <div class="row">
                            <!-- .page title -->
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-0">
                                <img class="img-full" src="{{ $project->imageurl }}" alt="{{ $project->project_name }} Buildings"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                               @if($project->project_admin) @if($project->project_admin!=$user->id)<label class="badge badge-warning mt-10">Shared</label>@endif @endif
                                <h4 class="page-title"><a @if(sub_project_access($project->id)) href="{{ route('admin.projects.scheduling',$project->id) }}"  @else href="{{ route('admin.projects.schedulingChart',[$project->id,'0']) }}"  @endif>
                                        <i class="{{ $pageIcon }}"></i> {{ $project->project_name }}</a> </h4>
                                   <h5 class="b-b p-b-10"><i class="icon-clock"></i> {{ date('d M Y',strtotime($project->start_date)) }} @if(!empty($project->deadline)) - {{ date('d M Y',strtotime($project->deadline)) }} @endif</h5>
                            </div>
                            <!-- /.page title -->
                        </div>

                    </div>
                </div>
                @if($x%3==0)
        </div>
        <div class="row"  >
            @endif
            <?php $x++;?>
            @endforeach
        </div>

        <?php   }else{
        $acc_id = 0;
        $costitemslist = \App\CostItems::orderBy("id",'asc')->get(); ?>
        @foreach($titles as $title)
            <?php
            $proproget = \App\ProjectCostItemsPosition::where('project_id',$title->project_id)->where('position','row')->where('level','1')->orderBy('inc','asc')->get();

            $tv = DB::table('project_cost_item_final_qty')
                ->select(DB::raw('SUM(total_amount) as total_cost'))
                ->where('title', $title->id)
                ->where('project_id', $title->project_id)
                ->groupBy('title')
                ->first();

            $tq = DB::table('project_cost_items_product')
                ->select(DB::raw('count(finalamount) as finalamount'))
                ->where('title', $title->id)
                ->where('project_id', $title->project_id)
                ->groupBy('title')
                ->first();
            $tn = \App\Title::where('id',$title->title)->first();
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <div class="row pdl10">
                            <!-- .page title -->
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <img class="img-responsive  project-img" src="{{ $project->imageurl }}" alt="{{ $title->title }} Buildings"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $title->title }}</h4>
                                <small>Created on: {{ date('d M Y H:i A',strtotime($title->created_at)) }}</small>
                                <h5 class="b-b p-b-10">Total Construction Value : <?php if(isset($tv) && $tv !== ''){ echo $tv->total_cost; } ?></h5>
                                <h5 class="b-b p-b-10">Project type : HIGH RISE</h5>
                                <h5 class="b-b p-b-10">Project Duration : 1 year</h5>
                                <h5 class="b-b p-b-10">Build up area : 10,235 sqFt</h5>
                            </div>
                            <!-- /.page title -->
                            <!-- .breadcrumb -->
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <p>Created by: Admin</p>
                                <a href="{{ url('admin/projects/scheduling/'.$title->project_id.'/'.$title->id) }}" class="btn btn-primary btn-circle "   ><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <!-- /.breadcrumb -->
                        </div>

                    </div>
                </div>
            </div>

        @endforeach
        <?php }?>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
     {{--<script src="//cdn.dhtmlx.com/gantt/edge/dhtmlxgantt.js"></script>
    <script src="//cdn.dhtmlx.com/gantt/edge/locale/locale_{{ $global->locale }}.js"></script>--}}
    <script src="https://docs.dhtmlx.com/gantt/codebase/dhtmlxgantt.js?v=7.0.10"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script type="text/javascript">
        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/scheduling') }}'+'/'+projectId;

        });
        <?php
        if(!empty($project)){ ?>
        $('.closeButton').click(function(){
            $('#eventDetailModal').hide();
        });

        gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";

        gantt.templates.task_class = function (st, end, item) {
            if(item.$level == 0||item.$level == 1){
                return "gantt_project";
            }
            if (item.planned_end) {
                var classes = ['has-baseline'];
                if (end.getTime() > item.planned_end.getTime()) {
                    classes.push('overdue');
                }
                return classes.join(' ');
            }
        };
     /*   gantt.addTaskLayer(function myNewElement(task) {
            var main_el = document.createElement("div");
            var size = gantt.getTaskPosition(task);
            main_el.innerHTML = [
                "<div class='project-left'></div>",
                "<div class='project-right'></div>"
            ].join('');
            main_el.className = "custom-project";
            main_el.style.left = size.left + "px";
            main_el.style.top = size.top + 7 + "px";
            main_el.style.width = size.width + "px";
            return main_el;
            return el;
        });*/
        gantt.config.scale_height = 50;
        gantt.config.grid_resize = true;
        gantt.config.scale_unit = "month";
        gantt.config.date_scale = "%F, %Y";
        gantt.config.subscales = [
            {unit: "day", step: 1, date: "%j, %D"}
        ];
        gantt.plugins({         marker: true     });

        <?php if(!empty($baselinedate)){?>
            gantt.config.task_date = "{{ $baselinedate }}";
        var dateToStr = gantt.date.date_to_str(gantt.config.task_date);
        var start = new Date("{{ $baselinedate }}");
        markerId = gantt.addMarker({
            start_date: start,
            css: "status_line",
            text: "Baseline",
        });
        gantt.getMarker(markerId); //->{css:"today", text:"Now", id:...}
        <?php }?>
        var exportTo = function() {
            var output = document.getElementById("period").value;
            switch (output) {
                case "1":
                    gantt.config.subscales = [
                        {unit: "day", step: 1, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "2":
                    gantt.config.subscales = [
                    {unit: "day", step: 2, date: "%j, %D"}
                        ];
                    loadData();
                break;
                case "7":
                    gantt.config.subscales = [
                        {unit: "day", step: 7, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "15":
                    gantt.config.subscales = [
                        {unit: "day", step: 7, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "30":
                    gantt.config.subscales = [
                        {unit: "day", step: 30, date: "%j, %D"}
                    ];
                    loadData();
                    break;
            }
        };

        // adding baseline display
        gantt.addTaskLayer({
            renderer: {
                render: function draw_planned(task) {
                    if (task.planned_start && task.planned_end) {
                        var sizes = gantt.getTaskPosition(task, task.planned_start, task.planned_end);
                        console.log(task.planned_start);
                        var el = document.createElement('div');
                        var tops = 9+sizes.top;
                        el.className = 'baseline';
                        el.style.left = sizes.left + 'px';
                        el.style.width = sizes.width + 'px';
                        el.style.top = tops+ 'px';
                        return el;
                    }
                    return false;
                },
                // define getRectangle in order to hook layer with the smart rendering
                getRectangle: function(task, view){
                    if (task.planned_start && task.planned_end) {
                        return gantt.getTaskPosition(task, task.planned_start, task.planned_end);
                    }
                    return null;
                }
            }
        });
        gantt.attachEvent("onTaskLoading", function (task) {
            task.planned_start = gantt.date.parseDate(task.planned_start, "xml_date");
            task.planned_end = gantt.date.parseDate(task.planned_end, "xml_date");
            return true;
        });

        gantt.templates.rightside_text = function (start, end, task) {
            if (task.planned_end) {
                if (end.getTime() > task.planned_end.getTime()) {
                    var overdue = Math.ceil(Math.abs((end.getTime() - task.planned_end.getTime()) / (24 * 60 * 60 * 1000)));
                    var text = "<b>Overdue: " + overdue + " days</b>";
                    return text;
                }
            }
        };

        gantt.config.server_utc = false;

        gantt.config.layout = {
            css: "gantt_container",
            rows:[
                {
                    cols: [
                        {
                            // the default grid view
                            view: "grid",
                            scrollX:"scrollHor",
                            scrollY:"scrollVer"
                        },
                        { resizer: true, width: 1 },
                        {
                            // the default timeline view
                            view: "timeline",
                            scrollX:"scrollHor",
                            scrollY:"scrollVer"
                        }
                    ]},
                {
                    view: "scrollbar",
                    id:"scrollHor"
                }
            ]
        }

        // default columns definition
        gantt.config.columns=[
            {name:"text",       label:"Cost item",  tree:true, width:'*',resizer: true },
            {name:"start_date", label:"Start date", align: "center",width: 70,resizer: true },
            {name:"deadline", label:"Due date", align: "center",width: 70,resizer: true },
            {name:"duration",   label:"Duration",   align: "center",width: 40,resizer: true },
            {name:"qty",   label:"Qty",   align: "center",width: 40,resizer: true },
            {name: "owner", align: "center", width: 75, label: "Owner", resize: true,
                template: function (item) {
                    var store = gantt.getDatastore("resource");
                    var ownerValue = item.users;
                    var singleResult = "";
                    var result = "";
                    if (ownerValue) {
                        if (!(ownerValue instanceof Array)) {
                            ownerValue = [ownerValue];
                        }
                        ownerValue.forEach(function(ownerId) {
                            var owner = store.getItem(ownerId);
                            if (!owner)	{
                                return;
                            }
                            result += "<div class='owner-label' title='" + owner.text + "'>" + owner.text + "</div>";
                        });
                    }

                    return result;
                }
            },
            {name:"priority",    label:"Action",   width: 50,resizer: true, align: "center", template: function (item) {
                if(item.linktype=='project'){
                    return '<a href="javascript:void(0);"  onclick=addTask("'+item.id+'");><i class="fa fa-plus"></i></a>';
                }

            }}
        ];
        // keeps or not the width of the grid area when column is resized
        gantt.config.keep_grid_width = false;
        gantt.config.grid_resize = true;

        // return false to discard the resize


        //defines the text inside the tak bars
      /* gantt.templates.task_text = function (start, end, task) {
            if ( task.$level > 0 ){
                return task.text + ", <b> @lang('modules.tasks.assignTo'):</b> " + task.users;
            }
            return task.text;

        };*/

        gantt.config.resource_attribute = "data-resource-id";

        var resourceConfig = {
            columns: [
                {
                    name: "name", label: "Name", align: "center", template: function (resource) {
                        return resource.text;
                    }
                },
                {
                    name: "Works", label: "Works", align: "center", template: function (resource) {
                        return resource.work;
                    }
                }
            ]
        };

        gantt.templates.resource_cell_class = function(start_date, end_date, resource, tasks){
            var css = [];
            css.push("resource_marker");
            if (resource.work <= 1) {
                css.push("workday_ok");
            } else {
                css.push("workday_over");
            }
            return css.join(" ");
        };

        gantt.templates.resource_cell_value = function(start_date, end_date, resource, tasks){
            return "<div>" + tasks.length + "</div>";
        };
        gantt.locale.labels.section_resources = "Works";
        gantt.config.lightbox.sections = [
            {name: "description", height: 38, map_to: "text", type: "textarea", focus: true},
            {
                name: "resources", type: "resources", map_to: "work", options: gantt.serverList("people"), default_value:1
            },
            {name: "time", type: "duration", map_to: "auto"}
        ];


        gantt.config.resource_store = "resource";
        gantt.config.resource_property = "users";
        gantt.config.order_branch = true;
        gantt.config.open_tree_initially = true;
        gantt.config.layout = {
            css: "gantt_container",
            rows: [
                {
                    cols: [
                        {view: "grid", group:"grids", scrollY: "scrollVer"},
                        {resizer: true, width: 1},
                        {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
                        {view: "scrollbar", id: "scrollVer", group:"vertical"}
                    ],
                    gravity:2
                },
                {resizer: true, width: 1},
                {
                    config: resourceConfig,
                    cols: [
                        {view: "resourceGrid", group:"grids", width: 435, scrollY: "resourceVScroll" },
                        {resizer: true, width: 1},
                        {view: "resourceTimeline", scrollX: "scrollHor", scrollY: "resourceVScroll"},
                        {view: "scrollbar", id: "resourceVScroll", group:"vertical"}
                    ],
                    gravity:1
                },
                {view: "scrollbar", id: "scrollHor"}
            ]
        };

        var resourcesStore = gantt.createDatastore({
            name: gantt.config.resource_store
        });
      /*  resourcesStore.attachEvent("onParse", function(){
            var people = [];
            resourcesStore.eachItem(function(res){
                if(!resourcesStore.hasChild(res.id)){
                    var copy = gantt.copy(res);
                    copy.key = res.id;
                    copy.label = res.text;
                    people.push(copy);
                }
            });
            gantt.updateCollection("people", people);
        });*/
      /*  resourcesStore.parse([
            {id: 1, text: "Aluminium", parent:null},
            {id: 2, text: "Glass", parent:null},
            {id: 3, text: "Plywood", parent:null},
            {id: 4, text: "Aluminium frame", parent:null},
            {id: 5, text: "Unassigned", parent:4},
            {id: 6, text: "John", parent:1},
            {id: 7, text: "Mike", parent:2},
            {id: 8, text: "Anna", parent:2},
            {id: 9, text: "Bill", parent:3},
            {id: 10, text: "Floe", parent:3}
        ]);*/
            $(window).load(function () {
                resourceData();
            });

        gantt.config.type_renderers[gantt.config.types.project] = function(task){
            if (task.$level > 0&&task.linktype == 'category') {
                var main_el = document.createElement("div");
                var size = gantt.getTaskPosition(task);
                main_el.innerHTML = [
                    "<div class='project-left'></div>",
                    "<div class='project-right'></div>"
                ].join('');
                main_el.className = "custom-project";
                main_el.style.left = size.left + "px";
                main_el.style.top = size.top + 7 + "px";
                main_el.style.width = size.width + "px";
                return main_el;
            }
        };
        gantt.templates.grid_row_class = function (start, end, task) {
            if (task.$level > 0&&task.linktype == 'category') {
                return "project-line";
            }
        };

     gantt.attachEvent("onTaskCreated", function(task){
            return false;
        });

        gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e){
            return false;
        });
        gantt.attachEvent("onAfterTaskDrag", function(id, mode, e){
            return false;
        });

        /*
             gantt.attachEvent("onAfterTaskDrag", function(id, mode, e){
                 var task = gantt.getTask(id);
                 var taskId = task.taskid;
                 var token = '{{ csrf_token() }}';
            var url = '{{route('admin.projects.gantt-task-update', ':id')}}';
            url = url.replace(':id', taskId);
            var startDate = moment.utc(task.start_date.toDateString()).format('DD/MM/Y');
            var endDate = moment.utc(task.end_date.toDateString()).subtract(1, "days").format('DD/MM/Y');

            $.easyAjax({
                url: url,
                type: "POST",
                container: '#gantt_here',
                data: { '_token': token, 'start_date': startDate, 'end_date': endDate }
            })
        });

        gantt.attachEvent("onBeforeLightbox", function(id) {
            var task = gantt.getTask(id);

            if ( task.$level > 0task.$level > 0 ){
                $(".right-sidebar").slideDown(50).addClass("shw-rside");

                var taskId = task.taskid;
                var url = "{{ route('admin.all-tasks.show',':id') }}";
                url = url.replace(':id', taskId);

                $.easyAjax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#right-sidebar-content').html(response.view);
                        }
                    }
                });
            }
            return false;
        });
*/
        gantt.init("gantt_here");


        function addTask(id) {
            var token = '{{ csrf_token() }}';
            $.easyAjax({
                url: '{{route("admin.projects.costData")}}',
                type: "POST",
                data: { '_token': token, 'id': id},
                success: function (response) {
                    if (response.id) {
                        $("#assignto").select2('destroy');
                        $("#target").select2('destroy');
                        $('#modelHeading').html('Update Target');
                        $('#eventDetailModal').show();
                        $('#projectCostId').val(response.id);
                        $('#start_date2').val(response.start_date);
                        $('#due_date2').val(response.deadline);
                        $('#linktype').val(response.type);
                        $('#target').val(response.target);
                        $("#assignto").trigger('change');
                        var assing = response.assign_to.split(',');
                        $("#assignto").val(assing);
                        $("#assignto").select2();
                        $("#assignto").trigger('change');
                    }
                }
            })
        }

        //    update task
        function storeTask() {
            $.easyAjax({
                url: '{{route('admin.all-tasks.store')}}',
                container: '#storeTask',
                type: "POST",
                data: $('#storeTask').serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        $('#eventDetailModal').modal('hide');
                        var responseTasks = response.tasks;
                        var responseLinks = response.links;

                        responseTasks.forEach(function(responseTask) {
                            gantt.addTask(responseTask);
                        });

                        responseLinks.forEach(function(responseLink) {
                            gantt.addLink(responseLink);
                        });
                    }
                }
            })
        };


        function limitMoveRight(task, limit) {
            var dur = task.end_date - task.start_date;
            task.start_date = new Date(limit.end_date);
            task.end_date = new Date(+task.start_date + dur);
        }

        function limitResizeRight(task, limit) {
            task.start_date = new Date(limit.end_date)
        }


        gantt.attachEvent("onTaskDrag", function (id, mode, task, original, e) {

            if(task.dependent_task_id !== null && task.dependent_task_id !== undefined)
            {
                var parent = gantt.getTask(task.dependent_task_id),
                    modes = gantt.config.drag_mode;

                var limitLeft = null,
                    limitRight = null;

                if (!(mode == modes.move || mode == modes.resize)) return;

                if (mode == modes.move) {
                    limitRight = limitMoveRight;
                } else if (mode == modes.resize) {
                    limitRight = limitResizeRight;
                }

                if (parent && +parent.end_date > +task.start_date) {
                    limitRight(task, parent);
                }
            }
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });


        <?php }?>
    </script>
@endpush

