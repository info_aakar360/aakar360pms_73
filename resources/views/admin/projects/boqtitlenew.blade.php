@extends('layouts.app')
@section('page-title')
   {{-- <div class="row bg-title">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.projectboq')</li>
            </ol>
        </div>
    </div>--}}
@endsection
@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dragtable.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }

        .rate-formula-sheet .loaderx{
            padding: 18% 0;
        }
        .formula-sheet .loaderx{
            padding: 18% 0;
        }
        .context-menu {
            position: absolute;
            right: 5%;
            margin-top: 4px;
        }
        .row_position {
            overflow-y: scroll;
            max-height: 500px;
            display: block;
            width: 100%;
        }
        /*.row_head {
            display: table; !* to take the same width as tr *!
            width: calc(100% - 17px); !* - 17px because of the scrollbar width *!
        }*/
        .RowCategory{
            width: 63px;
        }
        .RowType{
            width: 45px;
        }
        .RowCode{
            width: 145px;
        }
        .RowDescription{
            width: 255px;
        }
        .RowUnit{
            width: 90px;
        }
        .RowBaseRate{
            width: 90px;
        }
        .RowSurcharge{
            width: 117px;
        }
        .RowDiscount{
            width: 108px;
        }
        .RowFactor{
            width: 90px;
        }
        .RowFinalRate{
            width: 90px;
        }
        .RowRemark{
            width: 360px;
        }
        .white-box{
            padding: 5px !important;
        }
        .pdl10{
            padding-left: 10px !important;
        }
        .page-title{
            color: #0f49bd;
            font-weight: 900;
        }
        .page-title i{
            margin-right: 5px;
        }
        .dotlevel1{
           font-size: 20px;
            color: #fff;
            display: contents;
            padding: 0px;
        }
        .dotlevel{
            font-size: 20px;
            color: #b5b8a5;
            display: contents;
            padding: 0px;
        }
    </style>

@endpush
@section('content')

    <?php $acc_id = 0;
    $levelname = 'level0';
    $level = '0';
    $costitemslist = \App\CostItems::orderBy("id",'asc')->get();
        $levelarray = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->groupBy('level')->pluck('level');
         if($title){
             $tn = \App\Title::where('id',$title)->where('project_id',$id)->first();
         }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pdl10">
                        <!-- .page title -->
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ !empty($tn) ? ucwords($tn->title) : ucwords(get_project_name($id)) }} Estimate</h4>
                        </div>
                        <!-- /.page title -->
                        <!-- .breadcrumb -->
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 text-right">
                            {{--<div class="btn-group m-r-10">
                                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info btn-outline  dropdown-toggle waves-effect waves-light" type="button"> Estimate <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="{{ route("admin.projects.boqbudget",[$id,$title]) }}"> Budget</a></li>
                                    <li><a href="{{ route("admin.projects.boqinvoices",[$id,$title]) }}"> Invoices</a></li>
                                </ul>
                            </div>--}}
                           <a href="javascript:void(0);" class="btn btn-info" onclick="boqitemloop();" ><i class="fa fa-refresh"></i></a>
                            <a href="{{ route("admin.projects.showitemno",[$id,$title]) }}" class="btn btn-info" >@if($itemnoslug) Remove @else Add @endif Item no</a>
                            <a href="javascript:void(0);" class="btn btn-warning" data-toggle="modal" data-target="#Boqtemplate" >Template</a>
                            <a href="javascript:void(0);" class="btn btn-danger" data-toggle="modal" data-target="#importExcell" >Import</a>
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#lockModal" >Lock table</a>
                          </div>
                        <!-- /.breadcrumb -->
                    </div>
                    <div class="combo-sheet">
                        <div class="table-wrapper">
                            <table id="boqtable" class="table table-bordered boqtable" data-resizable-columns-id="users-table">
                                <thead>
                                <tr >
                                    <th>@lang('app.sno')</th>
                                    @if($itemnoslug)
                                    <th>@lang('app.itemno')</th>
                                    @endif
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <?php $colpositionarray = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->whereIn('itemslug',$columsarray)->orderBy('inc','asc')->get();
                                       foreach ($colpositionarray as $colposition){ ?>
                                    <th col="{{ $colposition->id }}">{{ $colposition->itemname }}</th>
                                    <?php }?>
                                </tr>
                                </thead>
                                <tbody class="colposition" id="boqitemloop">

                                </tbody>
                            </table>
                            <input type="hidden" class="positionid" />
                        </div>
                    </div>
                    <div class="rate-sheet rate-formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                    <div class="combo-sheet formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- .row -->
    {{--Ajax Modal--}}

    <div class="modal fade bs-modal-md in" id="Boqtemplate" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.projects.templateSubmit') }}" enctype="multipart/form-data" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Templates</span>
                </div>
                <div class="modal-body">
                    <table>
                        <thead>
                            <tr>
                               <th></th>
                               <th>Name</th>
                               <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($boqtemplatearray)){
                            foreach($boqtemplatearray as $boqtemplate){
                            ?>
                            <tr>
                               <td><input type="radio" value="{{ $boqtemplate->id }}" name="templateid" required /></td>
                                <td>{{ $boqtemplate->name }}</td>
                                <td>{{ get_project_category_name($boqtemplate->category) }}</td>
                            </tr>
                        <?php } }?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <input type="hidden" name="title" value="{{ $title }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <div class="modal fade bs-modal-md in" id="importExcell" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.projects.boqExcellImport') }}" enctype="multipart/form-data" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Excell Import</span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <div class="col-md-6">
                            <h3>Excell Sheet</h3>
                            <input type="file" class="form-control" name="excellsheet" required />
                        </div>
                        <div class="col-md-12">
                             <p>Download Sample excell here <a href="{{ uploads_url().'boqupload.xlsx' }}">Click here</a></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-success">Upload</button>
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <input type="hidden" name="title" value="{{ $title }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <div class="modal fade bs-modal-md in" id="lockModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ url('admin/projects/projectBoqLock') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Lock Tables</span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <div class="col-md-6">
                            <h3>Columns</h3>
                            <?php $rowpostitioncolms = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->orderBy('inc','asc')->get();
                                foreach($rowpostitioncolms as $rowpostition){
                            ?>
                            <div class="">
                                <label><input type="checkbox" value="1" name="lockcolms[{{ $rowpostition->id }}]" <?php if($rowpostition->collock==1){ echo 'checked';}?> > {{ $rowpostition->itemname }}</label>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <input type="hidden" name="title" value="{{ $title }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-success">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <div class="modal fade bs-modal-md in" id="categoryModalPop" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    <div class="modal fade bs-modal-md in" id="CostitemsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form id="costItemsUpdate" action="" method="post" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Add Cost item</span>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label>Cost item Name</label>
                            <input type="text" class="form-control" name="title" placeholder="Cost item">
                        </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="category" id="costaddcat">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        let updatedata = false;
        $(".datepicker").datepicker();
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');

        "use strict";
        function textAreaAdjust(element) {
            element.style.height = "1px";
            element.style.height = (25+element.scrollHeight)+"px";
        }
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('costitem-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Cost item!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.costitem-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            beforeSend:function () {
                                $(".preloader-small").show();
                            },
                            success: function (response) {
                                if (response.status == "success") {
                                    $(".preloader-small").hide();
                                    boqitemloop();
                                }
                            }
                        });
                    }
                });
            });
            $('body').on('click', '.sa-params-cat', function(){
                var id = $(this).data('position-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Category!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.costitem-cat-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            beforeSend:function () {
                                $(".preloader-small").show();
                            },
                            success: function (response) {
                                $(".preloader-small").hide();
                                if (response.status == "success") {
                                    boqitemloop();
                                }
                            }
                        });
                    }
                });
            });

        });
        boqitemloop();
        function boqitemloop() {
            var project = '{{ $id }}';
            var titleid = '{{ $title }}';
            var token = "{{ csrf_token() }}";
            var positionid = $(".positionid").val();
            $.ajax({
                url : "{{ route('admin.projects.boqtitleloop') }}",
                method: 'POST',
                data: {'_token': token,'projectid': project,'subprojectid': titleid,'positionid': positionid},
                beforeSend:function () {
                    $(".preloader-small").show();
                    updatedata = false;
                },
                success: function (response) {
                    $(".preloader-small").hide();
                    $("#boqitemloop").html("");
                    $("#boqitemloop").html(response);

                    $(".datepicker").datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        weekStart:'{{ $global->week_start }}',
                        format: 'dd-mm-yyyy',
                    });
                },
                complete:function () {
                    updatedata = true;
                }
            });
        }

        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });
            $('#boqtable').on('click', '.opntoggle', function () {
                //Gets all <tr>'s  of greater depth below element in the table
                var findChildren = function (tr) {
                    var depth = tr.data('depth');
                    return tr.nextUntil($('tr').filter(function () {
                        return $(this).data('depth') <= depth;
                    }));
                };
                var el = $(this);
                var tr = el.closest('tr'); //Get <tr> parent of toggle button
                var children = findChildren(tr);

                //Remove already collapsed nodes from children so that we don't
                //make them visible.
                //(Confused? Remove this code and close Item 2, close Item 1
                //then open Item 1 again, then you will understand)
                var subnodes = children.filter('.expand');
                subnodes.each(function () {
                    var subnode = $(this);
                    var subnodeChildren = findChildren(subnode);
                    children = children.not(subnodeChildren);
                });
                //Change icon and hide/show children
                if (tr.hasClass('collpse')) {
                    tr.removeClass('collpse').addClass('expand');
                    children.hide();
                } else {
                    tr.removeClass('expand').addClass('collpse');
                    children.show();
                }
                return children;
            });
            function calmarkup(costitem){
                var calvalue  = 0;
                var cocalvalue  = 0;
                var  percentvalue = 0;
                var  finalrate = 0;
                var  amountcal = 0;
                var  qty = 1;
                var qtypar =$(".qty"+costitem).val();
                if(qtypar){
                    qty =   qtypar;
                }
                $(".qty"+costitem).val(qty);
                var rate = parseFloat($(".ratevalue"+costitem).val());
                var changeorderrate = parseFloat($(".changeorderratevalue"+costitem).val());
                if(!changeorderrate){
                    changeorderrate = 0;
                }
                var catitem =  $(".ratevalue"+costitem).data('cat');
                var level =  $(".ratevalue"+costitem).data('level');
                var subtotcat =  $(".qty"+costitem).data('subtotcat');
                calvalue = rate*qty;
                cocalvalue = changeorderrate*qty;
                var margintype = $(".margintype"+costitem).val();
                var markvalue = parseFloat($(".marginvalue"+costitem).val());

                $(".amount"+costitem).val(calvalue);
                $(".changeorderamountvalue"+costitem).val(cocalvalue);

                amountcal = calvalue+cocalvalue;

                if(markvalue>0){
                    if(margintype=='percent'){
                        percentvalue = (markvalue/100)*amountcal;
                        amountcal += percentvalue;
                    }
                    if(margintype=='amt'){
                        amountcal += markvalue;
                    }
                }
                finalrate = amountcal/qty;
                $(".totalrate"+costitem).val(finalrate);
                $(".totalamount"+costitem).val(amountcal);

              /*  $(".totalrate"+costitem).val(finalrate);*/

                var itemarray = [];
                itemarray['qty'] = qty;
                itemarray['rate'] = rate;
                itemarray['changeorderrate'] = changeorderrate;
                itemarray['markuptype'] = margintype;
                itemarray['markupvalue'] = markvalue;
                itemarray['finalrate'] = finalrate;
                var token = '{{ csrf_token() }}';
               /* $.ajax({
                    url: '{{ url('admin/projects/updateboqcostitem') }}',
                    method: 'POST',
                    data: {
                        '_token':token,
                        'projectid':'{{ $id }}',
                        'title':'{{ $title }}',
                        item:'rate',
                        itemid:costitem,
                        itemvalue:rate
                    }
                });*/

              /*  var sum = 0;
                $(".adjustment"+catitem).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });*/

                var sum = 0;
                //iterate through each textboxes and add the values
                $(".subtotalamount"+subtotcat).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".subtotal"+subtotcat+"level0").html("₹"+sum.toFixed(2));

                var sum = 0;
                //iterate through each textboxes and add the values
                $(".finalamount"+catitem).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".catotal"+catitem).html("₹"+sum.toFixed(2));

                var tosum = 0;
                //iterate through each textboxes and add the values
                $(".grandvalue").each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        tosum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".grandtotal").html("₹"+tosum.toFixed(2));
            }

            $(document).on("change",".costitemcategory",function () {
                var cat =  $(this).val();
                var level =  $(this).attr('data-level');
                var catlevel =  $(this).attr('data-catlevel');
                var catparent = 0;
                 catparent =  $(this).attr('data-parent');
               var itemid =  $(this).attr('data-itemid');
                var catid =    $("#costitemcatitem option[value='" + cat + "']").attr('data-value');
                if(catid=='add'){
                    var url = '{{ route('admin.activity.create')}}';
                    $('#modelHeading').html("Create New Category");
                    $.ajaxModal('#categoryModalPop', url);
                }else{
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var trindex = $(this).closest('tr').index();
                     var token = "{{ csrf_token() }}";
                     $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.addcostitemcategory') }}",
                        data: {'_token': token,'category': cat,'itemid': itemid, 'catid': catid, 'catlevel': catlevel, 'projectid': projectid, 'titleid': titleid, 'level': level, 'parent': catparent},
                        success: function(data){
                            if(data=='success'){
                                $(".positionid").val(catparent);
                                boqitemloop();
                            }
                          /*  window.location.reload();
                            if(trindex>0){
                                $("#boqtable > tbody > tr").eq(trindex-1).after(data);
                            }else{
                                $(".colposition").prepend(data);
                            }
                            $(".costitemcategory").val("");*/
                        }
                    });
                }
            });
        $(document).on("change",".costitemlevel1category",function () {
                var cat =  $(this).val();
                var catparent =  $(this).attr('data-parent');
                var catid =    $("#costitemlevel1 option[value='" + cat + "']").attr('data-value');
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var trindex = $(this).closest('tr').index();
                if(catid=='add'){
                    var url = '{{ route('admin.activity.create')}}';
                    $('#modelHeading').html("Create New Category");
                    $.ajaxModal('#categoryModalPop', url);
                }else{
                    if(cat){
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: "POST",
                            url: "{{ route('admin.projects.addcostitemcategory') }}",
                            data: {'_token': token,'category': cat, 'catid': catid, 'projectid': projectid, 'titleid': titleid, 'level': '1', 'parent': catparent},
                            success: function(data){
                                if(data=='success'){
                                    $(".positionid").val(catparent);
                                    boqitemloop();
                                }
                                 /*window.location.reload();
                                if(trindex>0){
                                    $("#boqtable > tbody > tr").eq(trindex-1).after(data);
                                }else{
                                    $(".colposition").prepend(data);
                                }
                                $(".costitemcategory").val("");*/
                            }
                        });
                    }
                }
            });
            $(".context-menu-cost-item").click(function(){
                $('#CostitemsModal').modal('show');
            });
            $(".context-menu-category").click(function(){
                var url = '{{ route('admin.activity.create')}}';
                $('#modelHeading').html("Create New Category");
                $.ajaxModal('#categoryModalPop', url);
            });
        $(document).on("change",".costitemrow",function () {
                var value = $(this).val();
                var cat = $(this).data('cat');
                var list = $(this).attr('list');
               var positionid =    $(this).attr('data-positionid');
               var itemid =    $("#"+list+" option[value='" + value + "']").attr('data-id');
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                if(value){
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.addcostitemrow') }}",
                        data: {'_token': token, 'itemid': itemid, 'positionid': positionid, 'value': value, 'category': cat, 'projectid': projectid, 'titleid': titleid},
                        success: function(data){
                            if(data=='success'){
                                $(".positionid").val(positionid);
                                boqitemloop();
                            }
                            /*$("#boqtable > tbody > tr").eq(trindex).after(data);
                            $(".costitemrow").val("");*/
                        }
                    });
                }
            });
            $(document).on("click",".opentasks",function () {

                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var positionid = $(this).attr('data-positionid');
                $(".positionid").val(positionid);
                boqitemloop();
            });

        /*$(document).on("change",".additemno",function () {

                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                if(projectid){
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.showitemno') }}",
                        data: { '_token': token, 'projectid': projectid, 'titleid': titleid },
                        success: function(data){
                            if(data=='success'){
                                boqitemloop();
                            }
                        }
                    });
                }
            });*/
        $(function () {
            $(".boqtable").sortable({
                items: 'tr:not(thead>tr)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                update: function (event, ui) {

                },
                stop: function (e, ui) {
                    var costitemarray = [];
                    var level1catarray = [];
                    var token = '{{ csrf_token() }}';
                    ui.item.removeClass("selected");
                    $('.colposition>tr').each(function (index) {
                        var costitemrow = $(this).attr('data-costitemrow');
                        if(costitemrow){
                            costitemarray.push(costitemrow);
                        }
                        var level1cat = $(this).attr('data-level1cat');
                        if(level1cat){
                            level1catarray.push(level1cat);
                        }
                    });
                    if(costitemarray){
                        $.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                position:costitemarray
                            },
                            type: 'POST',
                            url: '{{ url('admin/projects/boqcostitemchangeposition') }}'
                        });
                    }
                    if(level1catarray){
                        /*$.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                position:level1catarray
                            },
                            type: 'POST',
                            url: '{{ url('admin/projects/boqcatchangeposition') }}'
                        });*/
                    }
                }
            });
            $('.boqtable').dragtable();
            /*   $('.boqtable').dragtable({persistState: function(table) {
                  var token = '{{ csrf_token() }}';
                var selectarray = [];
                    table.sortOrder['_token']=token;
                    table.el.find('th').each(function(i) {
                        var col = $(this).attr("col");
                        if(col) {
                            selectarray.push(col);
                        }
                    });
                   $.ajax({
                        url: '{{ url('admin/projects/boqcolchangeposition') }}',
                        method: 'POST',
                        data: {
                            '_token':token,
                            'projectid':'{{ $id }}',
                            'title':'{{ $title }}',
                            position:selectarray
                        },
                    });
                }
            });*/
        });
        $(document).on("change",".combo-sheet .updateproject",function(e){
            e.preventDefault();
            if(updatedata) {

                var token = '{{ csrf_token() }}';
                var item = $(this).data('item');
                var itemid = $(this).data('itemid');
                var itemvalue = $(this).val();
                if (item == 'assign_to') {
                    var itvalue = $('#assign' + itemid).find('option[value="' + itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                if (item == 'contractor') {
                    var itvalue = $('#contractorid' + itemid).find('option[value="' + itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                if (item && itemid) {
                    $.ajax({
                        url: '{{ url('admin/projects/updateboqcostitem') }}',
                        method: 'POST',
                        data: {
                            '_token': token,
                            'projectid': '{{ $id }}',
                            'title': '{{ $title }}',
                            item: item,
                            itemid: itemid,
                            itemvalue: itemvalue
                        }
                    });
                }
            }
        });

        $('#costItemsUpdate').submit(function (e) {
            e.preventDefault();
            var costaddcat = $("#costaddcat").val();
            $.ajax({
                url: '{{url('admin/cost-items/name-store')}}',
                container: '#costItemsUpdate',
                type: "POST",
                data: $('#costItemsUpdate').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        /*window.location.reload();*/
                        $("#CostitemsModal").modal('hide');
                        costitemlist();
                    }
                }
            })
        });
        function costitemlist(){
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{url('admin/cost-items/cost-item-list')}}',
                type: "POST",
                data: { '_token':token},
                success: function (response) {
                        $(".costitemslist").html("");
                        $(".costitemslist").html(response);
                }
            })
        }
        var cTable = null;
        $(document).on('click', '.open-rate-sheet', function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.projects.rate-sheet', [':id']) }}';
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                data: {_token: '{{ csrf_token() }}'},
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
    '                        <div class="cssload-speeding-wheel"></div>' +
    '                    </div>';
                    $('.rate-formula-sheet').html(html).show();
                },
                success: function(data){
                    $('.rate-formula-sheet').html(data);
                   $('#rate-table').resizableColumns({
                        responsive: true,
                        processing: true,
                        destroy: true,
                        stateSave: true,
                        language: {
                            "url": "<?php echo __("app.datatable") ?>"
                        },
                        "fnDrawCallback": function (oSettings) {
                            $("body").tooltip({
                                selector: '[data-toggle="tooltip"]'
                            });
                        }
                    });
                }
            })
        });
        $(document).on('click', '.close-rate-sheet', function(){
            $('.rate-formula-sheet').html('').hide();
        });
        var cTable = null;
        $(document).on('click', '.combosheetbutton', function(){
            var id = $(this).attr('data-resource');
            if(id){
                var url = '{{ route('admin.resources.getComboSheet', [':id']) }}';
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                    beforeSend: function(){
                        var html = '<div class="loaderx">' +
                            '                        <div class="cssload-speeding-wheel"></div>' +
                            '                    </div>';
                        $('.rate-formula-sheet').hide();
                        $('.formula-sheet').html(html).show();
                    },
                    success: function(data){
                        $('.formula-sheet').html(data);
                        ctable = $('#combo-table').resizableColumns({
                            responsive: true,
                            processing: true,
                            destroy: true,
                            stateSave: true,
                            language: {
                                "url": "<?php echo __("app.datatable") ?>"
                            },
                            "fnDrawCallback": function (oSettings) {
                                $("body").tooltip({
                                    selector: '[data-toggle="tooltip"]'
                                });
                            }
                        });
                    }
                })

            }
        });
        $(document).on('click', '.close-combo-sheet', function(){
            $('.formula-sheet').html('').hide();
            $('.rate-formula-sheet').show();
        });
        $("#boqitemloop").on('keydown', '.numbervalid', function(evt){
            var charCode =  (evt.which) ? evt.which : evt.keyCode;
            if ((charCode > 47 && charCode < 58 ) || (charCode > 95 && charCode < 107 ) || charCode == 110 || charCode == 190 || charCode == 8 )
                return true;
            return false;
        });
    </script>
@endpush