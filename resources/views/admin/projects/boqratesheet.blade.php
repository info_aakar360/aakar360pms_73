@push('head-script')

@endpush

@section('content')
    <style>
        .RowResource{
            width: 255px;
        }
        .RowRate{
            width: 90px;
        }
        .RowUnitx{
            width: 90px;
        }
        .RowFormula{
            width: 255px;
        }
        .RowResult{
            width: 100px;
        }
        .close-rate-sheet{
            font-size: 16px;
        }
        .rate-formula-sheet .cell-inp[data-col="1"],.formula-sheet .cell-inp[data-col="1"]{
            text-align: right;
        }
        .formula-sheet thead th, .rate-formula-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;

        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .cubeicon {
            font-size: 16px;
        }

    </style>
    <form method="post" autocomplete="off" id="rate-sheet-form">
        @csrf

        <div class="row pdl10">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="padding: 5px 15px;">
                <a href="javascript:;" class="close-rate-sheet pull-right"><i class="fa fa-times"></i> </a>
                <div class="col-md-3 pull-right">
                    <select class="form-control selectfunction"  >
                        <option value="">Functions</option>
                        <option value="Waste()">Waste(n)%</option>
                        <option value="">---------------</option>
                        <option value="Min()">Min()</option>
                        <option value="Max()">Max()</option>
                        <option value="Sum()">Sum()</option>
                        <option value="Average()">Average()</option>
                        <option value="Abs()">Abs()</option>
                        <option value="Sqrt()">Sqrt()</option>
                        <option value="">---------------</option>
                        <option value="Round()">Round(n)</option>
                        <option value="RoundX()">Round(n;x)</option>
                        <option value="RoundUp()">RoundUp(n)</option>
                        <option value="RoundUpX()">RoundUp(n;x)</option>
                        <option value="RoundDown()">RoundDown(n)</option>
                        <option value="RoundDownX()">RoundDown(n;x)</option>
                        <option value="Ceiling()">Ceiling(n;x)</option>
                        <option value="Floor()">Floor(n;x)</option>
                        <option value="">---------------</option>
                        <option value="SinDeg()">SinDeg(n)</option>
                        <option value="CosDeg()">CosDeg(n)</option>
                        <option value="TanDeg()">TanDeg(n)</option>
                        <option value="">---------------</option>
                        <option value="AreaCir()">AreaCir(r)</option>
                        <option value="AreaTri()">AreaTri(b,h)</option>
                        <option value="AreaPyr()">AreaPyr(l,b,h)</option>
                        <option value="AreaRect()">AreaRect(x,y)</option>
                        <option value="AreaCyl()">AreaCyl(r,h)</option>
                        <option value="AreaCone()">AreaCone(r,h)</option>
                        <option value="AreaSph()">AreaSph(r)</option>
                        <option value="">---------------</option>
                        <option value="PerimCir()">PerimCir(r)</option>
                        <option value="PerimTriR()">PerimTriR(b,h)</option>
                        <option value="PerimRect()">PerimRect(x,y)</option>
                        <option value="">---------------</option>
                        <option value="VolPyr()">VolPyr(l,b,h)</option>
                        <option value="VolCyl()">VolCyl(r,h)</option>
                        <option value="VolCone()">VolCone(r,h)</option>
                        <option value="VolSph()">VolSph(r)</option>
                    </select>
                </div>
                <div class="col-md-3 pull-right">
                    <a href="javascript:;" disabled="disabled" class="btn btn-primary combosheetbutton" >Combo sheet</a>
                    <button class="btn btn-primary" id="rate-sheet-submit" >Save</button>
                </div>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="rate-formula-sheet">
        <div class="combo-sheet cs">
        <div class="table-wrapper">
            <table class="table table-bordered default footable-loaded footable" id="users-table" data-resizable-columns-id="users-table">
                @if(isset($product_cost_item))
                    <input type="hidden" name="product_cost_item" value="{{ $product_cost_item }}" class="product_cost_item" />
                @endif
                <thead>
                <tr>
                    <th class="RowResource" data-resizable-column-id="resource">@lang('modules.resources.resource')</th>
                    <th class="RowRate" data-resizable-column-id="rate">@lang('modules.resources.rate')</th>
                    <th class="RowUnitx" data-resizable-column-id="unitx">@lang('modules.resources.unit')</th>
                    <th class="RowFormula" data-resizable-column-id="formula">@lang('modules.resources.formula')</th>
                    <th class="RowResult" data-resizable-column-id="unit">@lang('modules.resources.result')</th>
                    <th class="RowRemove" data-resizable-column-id="remove">@lang('modules.resources.remove')</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $res_count = count($cost_item_resources);
                $totalRow = 35 - $res_count;
                if($totalRow <= 1){
                    $totalRow = 2;
                }
                $row = 1;
                $totalamt = 0;
                ?>
                @foreach($cost_item_resources as $costitemresource)
                    <?php
                    $totalamt += $costitemresource->final_rate;
                    $curresource = \App\Resource::find($costitemresource->resource_id);
                    ?>
                    <tr class="resource_list" data-row="{{$row}}" data-id="{{ $costitemresource->id }}">
                        <td data-col="0">
                            <span data-row="{{$row}}" class="cubeicon"><?php if($curresource->category=='1'){?><i class="fa fa-th-large"></i><?php }else{?><i class="fa fa-square"></i><?php }?></span>
                            <input name="ratesheetid[]" type="hidden" data-row="{{ $row }}" class="ratesheetid" value="{{ $costitemresource->id }}" >
                            <input name="resource[]" data-col="0" data-row="{{$row}}" data-category="{{ $curresource->category }}" data-resourceid="{{ $curresource->id }}" list="costitem{{ $costitemresource->id }}" class="cell-inp category" value="{{ $costitemresource->name }}" >
                            <datalist class="resourceslist" id="costitem{{ $costitemresource->id }}">
                                 @foreach($resources as  $resource)
                                    <option value="{{$resource->id }}" @if($resource->id == $costitemresource->resource_id) selected @endif>{{ getResourceType($resource->type).' - '.$resource->description }}</option>
                                @endforeach
                            </datalist>
                        </td>
                        <td data-col="1"><input type="text" name="rate[]" data-col="1" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->rate }}" disabled="disabled"></td>
                        <td data-col="2"><input type="text" name="unit[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->unit }}" disabled="disabled"></td>
                        <td data-col="3"><input type="text" name="formula[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->formula }}"/></td>
                        <td data-col="4"><input type="text" name="result[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->final_rate }}" disabled="disabled"></td>
                        <td data-col="5"><a href="javascript:;" class="removeresourcerow" data-id="{{ $costitemresource->id }}" data-col="5" data-row="{{$row}}"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    <?php $row++ ?>
                @endforeach
                @for($i=0;$i<$totalRow;$i++)
                        <tr data-row="{{$row}}" class="resource_list">
                            <td data-col="0">
                                <span data-row="{{$row}}" class="cubeicon"></span>
                                <input name="ratesheetid[]" type="hidden" data-row="{{ $row }}" class="ratesheetid">
                                <input name="resource[]"  data-col="0" data-row="{{$row}}" list="costitemlist" class="cell-inp resource" >
                                <datalist class="resourceslist" id="costitemlist">
                                    <option value="add" >Create New</option>
                                    @foreach($resources as  $resource)
                                        <option value="{{$resource->id }}" >{{ $resource->type.' - '.$resource->description }}</option>
                                    @endforeach
                                </datalist>
                            </td>
                            <td data-col="1"><input type="text" name="rate[]"  data-col="1" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                            <td data-col="2"><input type="text" name="unit[]"  data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                            <td data-col="3"><input type="text"  name="formula[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value=""/></td>
                            <td data-col="4"><input type="text" name="result[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                            <td data-col="5"><a href="javascript:;" class="removeresourcerow" data-col="5" data-row="{{$row}}"></a></td>
                        </tr>
                    <?php
                    $row++;
                    ?>
                @endfor
                </tbody>
            </table>
        </div>
        </div>
        </div>
    </form>
    <div class="modal fade bs-modal-md in" id="comboModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form id="resourceCreate" action="" method="post" autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" >Add Task</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label>Category</label>
                            <select name="category"  class="form-control">
                                <option value="">Select Category</option>
                                @foreach($categories as $key => $category)
                                    <option value="{{$key}}" >{{ $category }}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Type</label>
                                <select name="type"  class="form-control">
                                    <option value="">Select Type</option>
                                    @foreach($types as  $type)
                                        <option value="{{$type->id}}" >{{ $type->symbol }} - {{ $type->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                            <div class="col-md-12">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="description" placeholder="Description" />
                            </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label>Rate</label>
                            <input type="text" class="form-control" name="rate" placeholder="Rate" />
                        </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Unit</label>
                                <input type="text" class="form-control" name="unit" placeholder="Unit" />
                            </div>
                        </div>
                            <div class="col-md-12">
                        <div class="form-group">
                            <label>Remark</label>
                            <input type="text" class="form-control" name="remark" placeholder="Remark" />
                        </div>
                        </div>
                     </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue" id="resourceSave">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script>
        "use strict";
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            var cat = inp.data('category');
            var row = inp.data('row');
            var resourceid = inp.attr('data-resourceid');
            $(".combosheetbutton").attr('disabled','disabled');
            $('.combosheetbutton').removeAttr('data-resource');
            if(cat==1){
                $('.combosheetbutton').removeAttr('disabled');
                $('.combosheetbutton').attr('data-resource',resourceid);
            }
        });
        function calcTotal(){
            var total = 0;
            $('.rate-formula-sheet tr.resource_list input.cell-inp[data-col=4]').each(function () {
                var inp = $(this).val();
                if(inp){
                    total = total + parseFloat(inp);
                }
            });
            var resource = $(".resource_id").val();
            $('.cs table tr[data-id="'+resource+'"] td[data-col=4] input.cell-inp[data-col=4]').val(total.toFixed(2));
            $('.rate-formula-sheet tr.final_result input.cell-inp[data-col=4]').val(total.toFixed(2));
        }
        $(document).on('change', '.rate-formula-sheet input.cell-inp[data-col=0]', function (e) {
            var inp = $(this);
            var id = inp.val();
            var row = inp.data('row');
            var resource = inp.val();
            var ratesheetid = $('.rate-formula-sheet .ratesheetid[data-row='+row+']').val();
            var costitem = $(".product_cost_item").val();
            var token = '{{ csrf_token() }}';
            if(resource=='add'){
                $("#comboModal").modal('show');
            }else{
                if(id != ''){
                    $.ajax({
                        url: '{{ route('admin.projects.addratetocostitem') }}',
                        type: 'POST',
                        data: {'_token': token,'costitem': costitem,'ratesheetid': ratesheetid,'resource': resource,'row': row },
                        success: function(data){
                            var rData = JSON.parse(data);
                            console.log(rData);
                            console.log(rData.id);
                          /*  var catBox = '<input name="resource[]" data-col="0" data-row="'+(row+1)+'" list="costitem'+rData.id+'" class="cell-inp category" disabled="disabled">'+
                                       '<datalist class="resourceslist" id="costitem'+rData.id+'">';
                                    @foreach($resources as  $resource)
                                        catBox+='<option value="{{$resource->id }}">{{ $resource->type.' - '.$resource->description }}</option>';
                                        @endforeach
                                            catBox+='</datalist>';*/
                            if(rData.category==0){
                                $('.rate-formula-sheet .cubeicon[data-row='+row+']').html("<i class=\"fa fa-square\"></i>");
                            }
                            if(rData.category==1){
                                $('.rate-formula-sheet .cubeicon[data-row='+row+']').html("<i class=\"fa fa-th-large\"></i>");
                            }
                            $('.rate-formula-sheet tr.resource_list[data-row='+row+']').attr('data-id',rData.id);
                            $('.rate-formula-sheet .ratesheetid[data-row='+row+']').val(rData.id);
                            $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=0]').val(rData.name);
                            $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val(rData.rate);
                            $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val(rData.unit);
                            $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=4]').val(rData.final_rate);
                            $('.rate-formula-sheet .removeresourcerow[data-row='+row+'][data-col=5]').html("<i class='fa fa-trash'></i>");
                            $('.ratevalue'+costitem).val(rData.result);
                        }
                    });
                    e.stopImmediatePropagation();
                    return false;
                }else{
                    $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
                    $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val('');
                }
            }
        });
        $(document).on('change', '.rate-formula-sheet tr.resource_list input.cell-inp[data-col=3]', function () {
            var inp = $(this);
            var row = inp.data('row');
            var productcostitem = $(".product_cost_item").val();
            var ratesheetid  = $('.rate-formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id');
            var rate = $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=1]').prop('disabled', false).val();
            var unit = $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=2]').prop('disabled', false).val();
            var applied = $('.rate-formula-sheet tr.final_result .cell-inp[data-col=3]').val();
            $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=1]').prop('disabled', true);
            $.ajax({
                url: '{{ route('admin.resources.sheetcalculate') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', str: inp.val(), ratesheetid: ratesheetid, unit: unit, rate: rate,productcostitem: productcostitem, applied: applied},
                success: function (data) {
                    var rData = JSON.parse(data);
                    if (rData.status == 'done') {
                        $(".ratevalue"+productcostitem).val(rData.costitemrate);
                        $('.rate-formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id',rData.ratesheetid);
                        $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=4]').val(rData.result);
                        calcTotal();
                    } else {
                        alert(rData.message);
                        $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=4]').val('#error');
                    }
                }
            });
        });
        $(document).on('change', '.rate-formula-sheet tr.final_result input.cell-inp[data-col=3]', function () {
            var app = $(this).val();
            if(app != '') {
                $.ajax({
                    url: '{{ route('admin.resources.validate') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}', str: app},
                    success: function(data){
                        var rData = JSON.parse(data);
                        if(rData.status == 'done') {
                            $('.rate-formula-sheet tr.resource_list input.cell-inp[data-col=3]').each(function () {
                                $(this).trigger('change');
                            });
                        }else{
                            alert('Applied factor field has some error!');
                            $('.rate-formula-sheet tr.final_result input.cell-inp[data-col=4]').val('#error');
                        }
                    }
                });
            }else{
                $('.rate-formula-sheet tr.resource_list input.cell-inp[data-col=3]').each(function () {
                    $(this).trigger('change');
                });
            }
        });
        $(document).on('focus', '.rate-formula-sheet tr.resource_list input.cell-inp[data-col=3]', function () {
            var app = $(this).data('row');
            $(".selectfunction").data('row',app);

            /*  if(app != '') {
                  $(".selectfunction").data('row',app);
                  $(".selectfunction").removeAttr('disabled');
              }else{
                  $(".selectfunction").data('row','');
                  $(".selectfunction").attr('disabled','disabled');
              }*/
        });
        $(document).on('click', '.rate-formula-sheet tr.resource_list td a.removeresourcerow[data-col=5]', function (e) {
            var row = $(this).parent().parent().attr('data-row');
            var rateid = $(this).parent().parent().attr('data-id');
            $.ajax({
                url: '{{ route('admin.projects.removeratesheetid') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}',rateid: rateid },
                success: function (data) {
                    var rData = JSON.parse(data);
                    if (rData.status == 'success') {
                        $('.rate-formula-sheet tr.resource_list[data-row=' + row + '][data-id=' + rateid + ']').remove();
                        calcTotal();
                        $('.ratevalue'+rData.costitem).val(rData.result);
                    } else {
                        alert(rData.message);
                        $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=4]').val('#error');
                    }
                }
            });
            e.stopImmediatePropagation();
            return false;
        });
        $(document).on('change','.selectfunction',function (e) {
            var datarow = $(this).data('row');
            var val = $(this).val();
            var forrow = $('.rate-formula-sheet tr.resource_list input[name="formula[]"][data-row='+datarow+']');
            var forrowval = forrow.val();
            var newtext = forrowval+''+val;
            forrow.val(newtext);
        });
        $('#resourceCreate').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ route('admin.resources.store') }}',
                container: '#resourceCreate',
                type: "POST",
                data: $('#resourceCreate').serialize(),
                success: function (rData) {
                    var response = JSON.parse(rData);
                    if(response.status == 'success'){
                        $("#comboModal").modal('hide');
                        var costitemlist = '';
                        costitemlist += '<option value="'+response.id+'" >'+response.name+'</option>';
                        $("#costitemlist").append(costitemlist);
                    }
                }
            })
        });
        $('#rate-sheet-submit').click(function (e) {
            $('#rate-sheet-form .cell-inp').removeAttr('disabled');
            e.preventDefault();
            $.ajax({
                url: '{{ route('admin.projects.ratesheetstore') }}',
                container: '#rate-sheet-form',
                type: "POST",
                data: $('#rate-sheet-form').serialize(),
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
                        '                        <div class="cssload-speeding-wheel"></div>' +
                        '                    </div>';
                    $('.rate-formula-sheet').html(html).show();
                },
                success: function (rData) {
                    var response = JSON.parse(rData);
                    if(response.status == 'success'){
                        $('.ratevalue'+response.costitem).val(response.result);
                        $('.rate-formula-sheet').html("").hide();
                        calmarkup(response.costitem);
                    }
                }
            })
        });

    </script>
@endsection

@push('footer-script')

@endpush