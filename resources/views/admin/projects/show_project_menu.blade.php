<div class="white-box">
    @if(in_array('projects', $user->modules))
        <nav>
            <ul class="showProjectTabs">
                <li class="projects">
                    <a href="{{ route('admin.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                </li>
                @if(in_array('employees',$modules))
                    <li class="projectMembers">
                        <a href="{{ route('admin.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a>
                    </li>
                @endif
                <li class="projectMilestones">
                    <a href="{{ route('admin.milestone.show', $project->id) }}"><span>@lang('modules.projects.milestones')</span></a>
                </li>
                @if(in_array('tasks',$modules))
                    <li class="projectTasks">
                        <a href="{{ route('admin.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a>
                    </li>
                @endif
                <li class="projectFiles">
                    <a href="{{ route('admin.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a>
                </li>
                {{--<li class="projectDesigns">--}}
                    {{--<a href="{{ route('admin.projects.showDesignFiles', $project->id) }}"><span>Designs</span></a>--}}
                {{--</li>--}}
                @if(in_array('invoices',$modules))
                    <li class="projectInvoices">
                        <a href="{{ route('admin.invoices.show', $project->id) }}"><span>@lang('app.menu.invoices')</span></a>
                    </li>
                @endif
                @if(in_array('timelogs',$modules))
                    <li class="projectTimelogs">
                        <a href="{{ route('admin.time-logs.show', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a>
                    </li>
                @endif
                <li class="burndownChart">
                    <a href="{{ route('admin.projects.burndown-chart', $project->id) }}"><span>@lang('modules.projects.burndownChart')</span></a>
                </li>
                <li class="Boq">
                    <a href="{{ route('admin.projects.boq', $project->id) }}"><span>BOQ</span></a>
                </li>
                <li class="Scheduling">
                    <a href="{{ route('admin.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                </li>
                <li class="Sourcing">
                    <a href="{{ route('admin.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                </li>
                <li class="ProductIssue">
                    <a href="{{ route('admin.projects.productIssue', $project->id) }}"><span>Product Issue</span></a>
                </li>
                <li class="Stores">
                    <a href="{{ route('admin.projects.stores', $project->id) }}"><span>@lang('app.menu.stores')</span></a>
                </li>
                @if(isset($store->id))
                    <li class="Bom"><a href="{{ route('admin.projects.showStoresProducts', [$project->id, $store->id]) }}"><span>BOM</span></a></li>
                    <li class="Stock"><a href="{{ route('admin.projects.showStoresStock', [$project->id, $store->id]) }}"><span>@lang('app.menu.stock')</span></a></li>
                    <li class="Indents"><a href="{{ route('admin.projects.indents', [$project->id, $store->id]) }}"><span>@lang('modules.module.indent')</span></a></li>
                    <li class="Rfqs"><a href="{{ route('admin.projects.rfqs', [$project->id, $store->id]) }}"><span>@lang('modules.module.rfq')</span></a></li>
                    <li class="Quotations"><a href="{{ route('admin.projects.quotations', [$project->id, $store->id]) }}"><span>@lang('modules.module.quotes')</span></a></li>
                    <li class="Po"><a href="{{ route('admin.projects.purchaseOrders', $project->id) }}"><span>@lang('modules.module.po')</span></a></li>
                @endif
            </ul>
        </nav>
    @else
        <nav>
            <ul>
                <li class="Bom"><a href="{{ route('admin.projects.showStoresProducts', [$project->id, $store->id]) }}"><span>BOM</span></a></li>
                <li class="Stock"><a href="{{ route('admin.projects.showStoresStock', [$project->id, $store->id]) }}"><span>@lang('app.menu.stock')</span></a></li>
                <li class="Indents"><a href="{{ route('admin.projects.indents', [$project->id, $store->id]) }}"><span>@lang('modules.module.indent')</span></a></li>
                <li class="Rfqs"><a href="{{ route('admin.projects.rfqs', [$project->id, $store->id]) }}"><span>@lang('modules.module.rfq')</span></a></li>
                <li class="Quotations"><a href="{{ route('admin.projects.quotations', [$project->id, $store->id]) }}"><span>@lang('modules.module.quotes')</span></a></li>
                <li class="Po"><a href="{{ route('admin.projects.purchaseOrders', $project->id) }}"><span>@lang('modules.module.po')</span></a></li>
            </ul>
        </nav>
    @endif
</div>