<div class="sidebarbox" >
    <?php if(!empty($task->id)){?>
    <div class="col-md-11 mb-10">
        <div class="col-md-12">
            <label><b>Name</b></label>
        </div>
        <h3>{{ ucwords($task->heading) }}</h3>

    </div>
    <div class="col-md-1 mb-10 text-right">
        <a href="javascript:void(0);" onclick="closeTaskview();"><i class="fa fa-times-circle"></i></a>
    </div>
    <div class="col-md-12 mb-10">
        <div class="col-md-2">
            <?php
            $statusicon = '<img data-toggle="tooltip" data-original-title="Not Started" class="img-circle" width="30" src="'.asset('user-uploads/not-started.png').'" />';
            switch ($task->status){
                case 'notstarted':
                    $statusicon = '<img data-toggle="tooltip" data-original-title="Not Started"  class="img-circle" width="30" src="'.asset('user-uploads/not-started.png').'" />';
                    break;
                case 'inprogress':
                    $statusicon = '<img data-toggle="tooltip" data-original-title="In Progress"  class="img-circle" width="30" src="'.asset('user-uploads/in-progress.png').'" />';
                    break;
                case 'inproblem':
                    $statusicon = '<img data-toggle="tooltip" data-original-title="In Problem"  class="img-circle" width="30" src="'.asset('user-uploads/in-problem.png').'" />';
                    break;
                case 'delayed':
                    $statusicon = '<img data-toggle="tooltip" data-original-title="Delayed"  class="img-circle" width="30" src="'.asset('user-uploads/delay.png').'" />';
                    break;
                case 'completed':
                    $statusicon = '<img data-toggle="tooltip" data-original-title="Completed"  class="img-circle" width="30" src="'.asset('user-uploads/completed.png').'" />';
                    break;
            }
            echo $statusicon;
            ?>
        </div>
        <div class="col-md-8">
            <label><b>Status</b></label>
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{ $task->percentage }}%" aria-valuenow="{{ $task->percentage }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="col-md-2 text-right">
            <label><b>{{ $task->percentage }}%</b></label>
        </div>
    </div>
    <div class="col-md-12 mb-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">@lang('app.startDate')</label>
                <input type="text" name="start_date" value="{{ \Carbon\Carbon::parse($task->start_date)->format('d-m-Y') }}" id="start_date2" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">@lang('app.dueDate')</label>
                <input type="text" name="deadline" value="{{ \Carbon\Carbon::parse($task->due_date)->format('d-m-Y') }}"  id="due_date2" class="form-control" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="col-md-6 mb-10">
        <p>Total {{ $taskissues }} Issues <a href="{{ route('admin.issue.index')  }}" target="_blank" >View all</a></p>


    </div>
    <div class="col-md-6 mb-10">
                <p>Total {{ $tasklabourlog }} Labour logs  <a href="{{ route('admin.man-power-logs.index')  }}" target="_blank" >View all</a></p>

    </div>
    <div class="col-md-12 mb-10">
        <div class="col-md-4">
            <a href="{{ route('admin.all-tasks.updateTask', $task->id) }}" class="btn btn-outline btn-success btn-sm">Update Task</a>
        </div>
        <div class="col-md-4">
            <a href="{{ route('admin.all-tasks.updateTask', $task->id) }}" class="btn btn-outline btn-success btn-sm">Raise Issue</a>
        </div>
        <div class="col-md-4">
            <a href="{{ route('admin.all-tasks.updateTask', $task->id) }}" class="btn btn-outline btn-success btn-sm">Man Power</a>
        </div>
    </div>
    <div class="col-md-12 mb-10">
        <div class="col-md-12">
            <label><b>Activities</b></label>
        </div>
    </div>
    <div class="timeline-list">
        <?php if(count($timelinearray)>0){
        ?>
            <?php
            foreach ($timelinearray as $timeline){
            $tablename = $timeline->getTable();
            switch($tablename){
            case 'task_percentage': ?>
             <div class="col-md-11 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                 <div class="col-md-2">
                     <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                 </div>
                 <div class="col-md-10">
                     <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                     <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                     <div class="commenttext">
                         @if($timeline->percentage)
                                <a href="javascript:void(0);" > Percentage <b>{!! $timeline->percentage.'%' !!}</b> Task Update from {{ get_user_name($timeline->added_by) }} with updated quantity <b>{{ $timeline->taskqty }}</b></a>
                            @endif
                @if($timeline->comment)
                    <div><strong>Comment: </strong><br>
                        {{ $timeline->comment }}</div>
                @endif
                             <div class="row">
                                 <?php $files = \App\TaskFile::where('task_id',$timeline->task_id)->where('task_percentage_id',$timeline->percentid)->get();

                                 $storage = storage();
                                 ?>
                                 @foreach($files as $file)
                                     <div class="col-md-3">
                                         @if($file->external_link != '')
                                             <?php $imgurl = $file->external_link;?>
                                         @elseif($storage == 'local')
                                             <?php $imgurl = uploads_url().'task-files/'.$file->task_id.'/'.$file->hashname;?>
                                         @elseif($storage == 's3')
                                             <?php $imgurl = awsurl().'/task-files/'.$file->task_id.'/'.$file->hashname;?>
                                         @elseif($storage == 'google')
                                             <?php $imgurl = $file->google_url;?>
                                         @elseif($storage == 'dropbox')
                                             <?php $imgurl = $file->dropbox_link;?>
                                         @endif
                                         {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                         <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                     </div>
                                 @endforeach
                             </div>

                 </div>
             </div>
             </div>
            <?php    break;
            case 'manpower_logs': ?>
                 <div class="col-md-11 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                     <div class="col-md-2">
                         <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                     </div>
                     <div class="col-md-10">
                         <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                         <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                         <div class="commenttext">
                <div class="row">
                    <div class="col-md-11 form-group">
                        <a href="javascript:void(0);" > Manpower <b>{!! $timeline->manpower !!}</b> With Work time <b>{!! $timeline->workinghours !!}</b> Update from {{ get_user_name($timeline->added_by) }}</a>
                     </div>
                    <div class="col-md-1">
                        <a href="{{ route('admin.man-power-logs.edit',$timeline->id) }}" class="float-right"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
                             <div class="row">
                                 <?php $files = \App\ManpowerLogFiles::where('manpower_id',$timeline->manpowerid)->get();
                                 ?>
                                 @foreach($files as $file)
                                     <div class="col-md-3">
                                         @if($file->external_link != '')
                                             <?php $imgurl = $file->external_link;?>
                                         @elseif($storage == 'local')
                                             <?php $imgurl = uploads_url().'manpower-log-files/'.$file->task_id.'/'.$file->hashname;?>
                                         @elseif($storage == 's3')
                                             <?php $imgurl = awsurl().'/manpower-log-files/'.$file->task_id.'/'.$file->hashname;?>
                                         @elseif($storage == 'google')
                                             <?php $imgurl = $file->google_url;?>
                                         @elseif($storage == 'dropbox')
                                             <?php $imgurl = $file->dropbox_link;?>
                                         @endif
                                         {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                         <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                     </div>
                                 @endforeach
                             </div>
                </div>
        </div>
        </div>
            <?php    break;
            case 'punch_item': ?>
                <div class="col-md-11 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                    <div class="col-md-2">
                        <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                    </div>
                    <div class="col-md-10">
                        <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                        <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                        <div class="commenttext">
                        <div class="row">
                            <div class="col-md-11 form-group">
                                <a href="javascript:void(0);" > Punch item <b>{!! $timeline->title !!}</b> With <b>{!! $timeline->status !!}</b> Status Update by {{ get_user_name($timeline->added_by) }}</a>
                             </div>
                            <div class="col-md-1">
                                <a href="{{ route('admin.issue.edit',$timeline->id) }}" class="float-right"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                            <div class="row">
                                <?php $files = \App\PunchItemFiles::where('task_id',$timeline->punchitemid)->get();  ?>
                                @foreach($files as $file)
                                    <div class="col-md-3">
                                        @if($file->external_link != '')
                                            <?php $imgurl = $file->external_link;?>
                                        @elseif($storage == 'local')
                                            <?php $imgurl = uploads_url().'punch-files/'.$file->task_id.'/'.$file->hashname;?>
                                        @elseif($storage == 's3')
                                            <?php $imgurl = awsurl().'/punch-files/'.$file->task_id.'/'.$file->hashname;?>
                                        @elseif($storage == 'google')
                                            <?php $imgurl = $file->google_url;?>
                                        @elseif($storage == 'dropbox')
                                            <?php $imgurl = $file->dropbox_link;?>
                                        @endif
                                        {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                        <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                    </div>
                                @endforeach
                            </div>
                     </div>
                </div>
            </div>
            <?php    break;
            }
            ?>
            <?php }?>
        </ul>
        <?php }else{?>
        <p>Timeline not found</p>
        <?php }?>
    </div>
        <div class="replyblock col-md-12">
            <div id="task-upload-box" style="display: none;" >
                <div class="row" id="file-dropzone">
                    <div class="col-md-12">
                        <div class="dropzone dropheight"
                             id="file-upload-dropzone">
                            {{ csrf_field() }}
                            <div class="fallback">
                                <input name="file" type="file" multiple/>
                            </div>
                            <input name="image_url" id="image_url" type="hidden" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <input type="text" class="form-control replycomment" placeholder="Reply.." >
                <input type="hidden" class="replytaskid" value="{{ $task->id }}" >
                <input type="hidden" class="replycostid" value="" >
            </div>
            <div class="col-md-1 replyicon">
                <a href="javascript:void(0);" class="replyfileblock"><i class="fa fa-paperclip"></i></a>
            </div>
            <div class="col-md-1 replyicon">
                <a href="javascript:void(0);" class="replysubmit"><i class="fa fa-send"></i></a>
            </div>

            <input type="hidden" name="taskID" id="taskID">
            <input type="hidden" name="percentageID" id="percentageID">
        </div>
        <?php }else{?>
            <p>Task Not found!!</p>
        <?php }?>
</div>