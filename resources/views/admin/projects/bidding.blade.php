@extends('layouts.public-quote')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <?php $tenderCheck = \App\TenderBidding::where('company_id', $company_id)
                    ->where('project_id', $project_id)
                    ->where('tender_id', $tender_id)
                    ->where('sourcing_id', $sourcing_id)
                    ->where('user_id', $user_id)
                    ->first(); ?>
                @if($tenderCheck)
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body text-center">
                            <h1><i class="fa fa-exclamation-circle" style="color: orange; font-size: 120px"></i></h1>
                            <h2>Bidding already submitted.</h2>
                            <h5>You can not submit same bidding multiple times.</h5>
                        </div>
                    </div>
                @else
                <div class="panel-heading"> Tender Bidding <span style="float: right">Tender Name : {{$tender->name}}</span></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form method="post" id="productCostItem" enctype="multipart/form-data">
                            <div class="row">

                                {{ csrf_field() }}
                                <input type="hidden" name="project_id" value="{{ $project_id }}"/>
                                <input type="hidden" name="sourcing_id" value="{{ $sourcing_id }}"/>
                                <input type="hidden" name="company_id" value="{{ $company_id }}"/>
                                <input type="hidden" name="tender_id" value="{{ $tender_id }}"/>
                                <input type="hidden" name="user_id" value="{{ $user_id }}"/>
                                <div class="col-lg-12">&nbsp;</div>
                                <div class="row">
                                    <div class="col-xs-12 ">
                                        <b>Tender Payment Conditions</b><br>
                                        <table class="table" width="100%" cellpadding="0" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th width="10%"><b>#</b></th>
                                                    <th width="40%"><b>Conditions</b></th>
                                                    <th width="50%"><b>Terms</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($cons as $key=>$con)
                                                    <tr>
                                                        <td width="10%">{{ $key+1 }}</td>
                                                        <td width="40%">
                                                            {{ $tender->name }}
                                                        </td>
                                                        <td width="50%">
                                                            {{ $con->terms }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-lg-12">&nbsp;</div>
                                <div class="col-lg-12">
                                    <b>Tender Products</b><br>
                                    <table class="table" width="100%" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="10%"><b>#</b></th>
                                                <th width="20%"><b>Product</b></th>
                                                <th width="20%"><b>Quantity</b></th>
                                                <th width="30%"><b>Brand</b></th>
                                                <th width="20%"><b>Your Price</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $cf = json_decode($package->create_from);
                                        $p  = 1;
                                        foreach ($pros as $key=>$product) {
                                        ?>
                                            <tr>
                                                <td width="10%">{{ $key+1 }}</td>
                                                <td width="20%">
                                                    <label style="padding: 5px;">
                                                        <?php $cat = \App\ProductCategory::where('id', $product->products)->first(); //dd($cat); ?>
                                                        <input type="hidden" name="products[{{ $key }}]" value="{{ $product->products }}" class="form-check-input" checked>
                                                        {{ get_pcat_name($product->products) }}
                                                    </label>
                                                </td>
                                                <td width="20%">
                                                    <input type="hidden" name="qty[{{ $key }}]" value="{{ $product->qty }}" class="form-check-input">
                                                    <label style="padding: 5px;">
                                                        {{ $product->qty }}
                                                    </label>
                                                </td>
                                                <td width="30%">
                                                    <?php
                                                        $bid = '';
                                                    $bex = explode(',', $product->brands);
                                                    foreach($bex as $be){
                                                    $brand = \App\ProductBrand::where('id',$be)->first();
                                                    $bid = $bid.','.$brand->id;
                                                    ?>

                                                    <span class="tm-tag tm-tag-info" id="BVrkl_1"><span>{{ $brand->name }}</span></span>
                                                    <?php } ?>
                                                        <input type="hidden" name="brands[{{ $key }}]" value="{{ $bid }}"/>
                                                </td>
                                                <td width="20%">
                                                    <input type="text" name="price[{{ $key }}]" class="form-control"/>
                                                </td>
                                            </tr>
                                        <?php }?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12" style="text-align: right;">&nbsp;</div>
                                <!--/span-->
                                <div class="col-md-12">
                                    @if(count($files))
                                    <b>Tender Files</b>
                                    <br>
                                    @endif
                                    <div class="row" id="list">
                                        <ul class="list-group" id="files-list">
                                            @foreach($files as $file)
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        {{ $file->filename }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?php $rfi = \App\Tenders::where('id',$file->tender_id)->first(); ?>
                                                        @if($file->external_link != '')
                                                            <a target="_blank" href="{{ $file->external_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 'local')
                                                            <a target="_blank" href="{{ asset_url('tender-files/'.$rfi->name.'/'.$file->hashname) }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 's3')
                                                            <a target="_blank" href="{{ $url.'tender-files/'.$rfi->name.'/'.$file->filename }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'google')
                                                            <a target="_blank" href="{{ $file->google_url }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'dropbox')
                                                            <a target="_blank" href="{{ $file->dropbox_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @endif


                                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                           class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                        <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                                    </div>
                                                </div>
                                            </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: right;">
                                    <input type="button" name="submit" class="btn btn-success saveProduct" value="Submit" id="saveProduct">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $('.saveProduct').click(function () {
            $.easyAjax({
                url: '{{route('projects.biddingSubmit')}}',
                container: '#productCostItem',
                type: "POST",
                data: $('#productCostItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        alert('Submitted Successfully');
                        window.location.href('{{ route('projects.success') }}');
                    }
                }
            })
        });
    </script>
@endpush
