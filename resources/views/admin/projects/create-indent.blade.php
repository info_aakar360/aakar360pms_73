@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }} #{{ $project->id }} - <span class="font-bold">{{ ucwords($project->project_name) }} </span> / Store Name - <span class="font-bold">{{ ucwords($store->company_name) }} </span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">

        <section>
            <div class="sttabs tabs-style-line">
                <div class="white-box">
                    @if(in_array('projects', $user->modules) && $user->can('view_projects'))
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('admin.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('admin.project-admins.show', $project->id) }}"><span>@lang('modules.projects.admins')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('admin.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('admin.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('admin.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('admin.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('admin.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('admin.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    @else
                        <nav>
                            <ul>
                                <li ><a href="{{ route('admin.projects.stores', $project->id) }}"><span>@lang('modules.module.stores')</span></a>
                                </li>

                                <li><a href="{{ route('admin.projects.showStoresProducts', [$project->id, $store->id]) }}"><span>BOM</span></a></li>
                                <li><a href="{{ route('admin.projects.showStoresStock', [$project->id, $store->id]) }}"><span>@lang('app.menu.stock')</span></a></li>
                                <li class="tab-current"><a href="{{ route('admin.projects.indents', [$project->id, $store->id]) }}"><span>@lang('modules.module.indent')</span></a></li>

                                <li><a href="{{ route('admin.projects.rfqs', [$project->id, $store->id]) }}"><span>@lang('modules.module.rfq')</span></a></li>
                                <li><a href="{{ route('admin.projects.quotations', [$project->id, $store->id]) }}"><span>@lang('modules.module.quotes')</span></a></li>
                                <li><a href="{{ route('admin.projects.purchaseOrders', $project->id) }}"><span>@lang('modules.module.po')</span></a></li>
                            </ul>
                        </nav>
                    @endif
                </div>
            </div><!-- /tabs -->
        </section>

        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.indent.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createIndent','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}

                        <div class="form-body">
                            <h3 class="box-title">@lang('modules.indent.indentDetails')</h3>
                            <hr>
                            <input type="hidden" name="store_id" value="{{ $sid }}">
                            <input type="hidden" name="project_id" value="{{ $pid }}">
                            <div class="row">

                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.remark')</label>
                                        <input type="text" id="remark" name="remark" value="" class="form-control" >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                            <hr>
                            <div class="row" style="background-color: #efefef; padding-top: 5px;">
                                <div class="proentry">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.category')</label>
                                            <select class="form-control select2" name="product" data-style="form-control product">
                                                <option value="">Select Category</option>
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}">{{ $product->name }}</option>
                                                @endforeach
                                                @foreach($lproducts as $lproduct)
                                                    <option value="{{$lproduct->id}}">{{ $lproduct->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.brand')</label>
                                            <select class="form-control select2" name="brand" id="brandData" data-style="form-control brand">
                                                <option value="">Select Brand</option>
                                                @forelse($brands as $brand)
                                                    <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Estimated Quantity</label>
                                            <input type="text" readonly id="estQty" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Requested Quantity</label>
                                            <input type="text" readonly id="reqQty" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.quantity')</label>
                                            <input type="text" name="quantity" value="" class="form-control quantity" placeholder="Enter Quantity">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.unit')</label>
                                            <select class="form-control select2" name="unit" data-style="form-control unit">
                                                <option value="">Select Unit</option>
                                                @forelse($units as $unit)
                                                    <option value="{{$unit->id}}">{{ $unit->symbol }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.dateRequired')</label>
                                            <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.remark')</label>
                                            <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label" style="width: 100%;">@lang('modules.indent.action')</label>
                                            <a href="javascript:void(0)" class="add-button btn btn-primary" style="color: #ffffff;">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">

                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                            <a href="{{ route('admin.indent.index') }}" class="btn btn-default">@lang('app.cancel')</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->


    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                {!! Form::open(['id'=>'createProductBrand','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="modelClose"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.indent.brand')</label>
                                    <input type="text" name="brand" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button" id="save-brand" class="btn btn-success">Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.stores.projects.indent.store')}}',
                container: '#createIndent',
                type: "POST",
                redirect: true,
                data: $('#createIndent').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            var store_id = $('input[name=store_id]').val();
            var project_id = $('input[name=project_id]').val();
            $.ajax({
                url: '{{route('admin.stores.projects.indent.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
                success: function (data) {
                    $('#brandData').html(data.brands);
                    $('#estQty').val(data.estQty);
                    $('#reqQty').val(data.reqQty);
                    $("#brandData").select2("destroy");
                    $("#brandData").select2();
                }
            });

        });
        $(document).on('change', '#brandData', function(){
            var pid = $(this).val();
            if(pid == 'add_brand'){
                $('#modelHeading').html("Create New Brand");
                $.ajaxModal('#taskCategoryModal');
            }
        });

        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('#brandData').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var store_id = $('input[name=store_id]').val();
            var project_id = $('input[name=project_id]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || qty == '' || qty == 0 || unit == '' || dated == '' || store_id == '' || project_id == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.stores.projects.indent.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark, store_id: store_id, project_id: project_id},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');
            var store_id = $('input[name=store_id]').val();
            var project_id = $('input[name=project_id]').val();
            if(store_id == '' || project_id == ''){
                alert('Invalid Data. Store and Project required.');
            }else {
                $.ajax({
                    url: '{{route('admin.stores.projects.indent.deleteTmp')}}',
                    type: 'POST',
                    data: {_token: '{{ csrf_token()  }} ', did: did, store_id: store_id, project_id: project_id},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Deleting...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Delete');
                    }

                });
            }
        });

        $('#save-brand').click(function () {
            $.easyAjax({
                url: '{{route('admin.projects.indent.brand')}}',
                container: '#createProductBrand',
                type: "POST",
                data: $('#createProductBrand').serialize(),
                success: function (data) {
                    $('#modelClose').click();
                    $('#brandData').html(data);
                    $("#brandData").select2("destroy");
                    $("#brandData").select2();

                }
            })
        });
    </script>
@endpush

