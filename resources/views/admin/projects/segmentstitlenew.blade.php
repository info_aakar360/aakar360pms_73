@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.projectboq')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dragtable.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            overflow-x: scroll;
            overflow-y: auto;
            max-height: 75vh;
            border: 2px solid #bdbdbd;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet table {
            border-collapse: collapse;
            border-spacing: 0px;
        }
        .combo-sheet thead{
            background-color: #C2DCE8;
        }
        .combo-sheet thead tr th{
            background-color: #C2DCE8 !important;
        }
        .combo-sheet thead tr th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        }
        .combo-sheet td{
            padding: 0px !important;
            font-size: 12px;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .open-rate-sheet{
            position: absolute;
            right: 5px;
            font-size: 16px;
            padding: 0 3px;
        }
        .combo-sheet.formula-sheet, .rate-sheet.rate-formula-sheet{
            position: absolute;
            left: 0;
            top: 0;
            overflow-x: scroll;
            overflow-y: auto;
            z-index: 2;
            background: #ffffff;
            margin: 0 5px;
            right: 0;
            display: none;
            max-height: 100vh;
            bottom: 0;
        }
        .combo-sheet select.cell-inp {
            padding: 2.5px 0;
            width: 100%;
            border: none;
        }
        .rate-formula-sheet .loaderx{
            padding: 18% 0;
        }
        .formula-sheet .loaderx{
            padding: 18% 0;
        }
        .context-menu {
            position: absolute;
            right: 5%;
            margin-top: 4px;
        }
        .row_position {
            overflow-y: scroll;
            max-height: 500px;
            display: block;
            width: 100%;
        }
        /*.row_head {
            display: table; !* to take the same width as tr *!
            width: calc(100% - 17px); !* - 17px because of the scrollbar width *!
        }*/
        .RowCategory{
            width: 63px;
        }
        .RowType{
            width: 45px;
        }
        .RowCode{
            width: 145px;
        }
        .RowDescription{
            width: 255px;
        }
        .RowUnit{
            width: 90px;
        }
        .RowBaseRate{
            width: 90px;
        }
        .RowSurcharge{
            width: 117px;
        }
        .RowDiscount{
            width: 108px;
        }
        .RowFactor{
            width: 90px;
        }
        .RowFinalRate{
            width: 90px;
        }
        .RowRemark{
            width: 360px;
        }
        .white-box{
            padding: 5px !important;
        }
        .pdl10{
            padding-left: 10px !important;
        }
        .page-title{
            color: #0f49bd;
            font-weight: 900;
        }
        .page-title i{
            margin-right: 5px;
        }
        .dotlevel1{
            font-size: 20px;
            color: #fff;
            display: contents;
            padding: 0px;
        }
        .dotlevel{
            font-size: 20px;
            color: #b5b8a5;
            display: contents;
            padding: 0px;
        }
    </style>

@endpush
@section('content')
    <?php $acc_id = 0;
    $costitemslist = \App\ProjectSegmentsProduct::select('id','cost_items_id')->where('segment',$segment)->where('project_id',$id)->where('title',$title)->orderBy("id",'asc')->get();
        $proproget = \App\ProjectSegmentsPosition::where('segment',$segment)->where('project_id',$id)->where('title',$title)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();
    if($title){
        $tn = \App\Title::where('id',$title)->where('project_id',$id)->first();
    }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pdl10">
                        <!-- .page title -->
                        <div class="col-md-3">
                            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ !empty($tn) ? $tn->title : get_project_name($id) }}</h4>
                        </div>
                        <!-- /.page title -->
                        <!-- .breadcrumb -->
                        <div class="col-md-3">
                             <select class="form-control select2 selectsegment" name="segment">
                                 <option value="">Select Segment</option>
                                 @if($segmentsarray)
                                        @foreach($segmentsarray as $segments)
                                            <option value="{{ $segments->id }}" @if($segments->id==$segment) selected @endif>{{ ucwords($segments->name) }}</option>
                                          @endforeach
                                 @endif
                             </select>
                          </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#lockModal" >Lock table</a>
                          </div>
                        <!-- /.breadcrumb -->
                    </div>
                    <div class="combo-sheet">
                        <div class="table-wrapper">
                            <table id="boqtable" class="table table-bordered default footable-loaded footable boqtable" data-resizable-columns-id="users-table">
                                <thead>
                                <tr >
                                    <th colspan="3"></th>
                                    <?php $colpositionarray = \App\ProjectSegmentsPosition::where('segment',$segment)->where('project_id',$id)->where('title',$title)->where('position','col')->orderBy('inc','asc')->get();
                                        foreach ($colpositionarray as $colposition){ ?>
                                        <th col="{{ $colposition->id }}">{{ $colposition->itemname }}</th>
                                    <?php }?>
                                </tr>
                                </thead>
                                <tbody class="colposition" id="boqitemloop">

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="rate-sheet rate-formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                    <div class="combo-sheet formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- .row -->
    {{--Ajax Modal--}}

    <div class="modal fade bs-modal-md in" id="lockModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ url('admin/projects/projectBoqLock') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Lock Tables</span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <div class="col-md-6">
                            <h3>Columns</h3>
                            <?php $rowpostitioncolms = \App\ProjectSegmentsPosition::where('segment',$segment)->where('project_id',$id)->where('title',$title)->where('position','col')->orderBy('inc','asc')->get();
                                foreach($rowpostitioncolms as $rowpostition){
                            ?>
                            <div class="">
                                <label><input type="checkbox" value="1" name="lockcolms[{{ $rowpostition->id }}]" <?php if($rowpostition->collock==1){ echo 'checked';}?> > {{ $rowpostition->itemname }}</label>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <input type="hidden" name="title" value="{{ $title }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-success">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <div class="modal fade bs-modal-md in" id="categoryModalPop" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    <div class="modal fade bs-modal-md in" id="CostitemsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form id="costItemsUpdate" action="" method="post" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Add Task</span>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label>Task Name</label>
                            <input type="text" class="form-control" name="title" placeholder="Task">
                        </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="category" id="costaddcat">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('.selectsegment').click(function(){
           var segment = $(this).val();
           if(segment){
               window.location.href = '{{ url('admin/projects/segments/'.$id.'/'.$title) }}'+'/'+segment;
           }
        });

        "use strict";
        function textAreaAdjust(element) {
            element.style.height = "1px";
            element.style.height = (25+element.scrollHeight)+"px";
        }
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        boqitemloop();
        function boqitemloop() {
            var project = '{{ $id }}';
            var titleid = '{{ $title }}';
            var segmentid = '{{ $segment }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                url : "{{ route('admin.projects.segment-boqtitleloop') }}",
                method: 'POST',
                data: {'_token': token,'projectid': project,'subprojectid': titleid,'segmentid': segmentid},
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function (response) {
                    $(".preloader-small").hide();
                    $("#boqitemloop").html("");
                    $("#boqitemloop").html(response);

                    $(".datepicker").datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        weekStart:'{{ $global->week_start }}',
                        format: 'dd-mm-yyyy',
                    });
                }
            });
        }
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('costitem-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Task!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.segment-costitem-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $("#costitem"+id).remove();
                                }
                            }
                        });
                    }
                });
            });
            $('body').on('click', '.sa-params-cat', function(){
                var id = $(this).data('position-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Activity!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.segment-costitem-cat-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {

                                window.location.reload();
                                if (response.status == "success") {
                                    $("#catrow"+id).remove();
                                }
                            }
                        });
                    }
                });
            });


        });


        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });
            $('#boqtable').on('click', '.opntoggle', function () {
                //Gets all <tr>'s  of greater depth below element in the table
                var findChildren = function (tr) {
                    var depth = tr.data('depth');
                    return tr.nextUntil($('tr').filter(function () {
                        return $(this).data('depth') <= depth;
                    }));
                };
                var el = $(this);
                var tr = el.closest('tr'); //Get <tr> parent of toggle button
                var children = findChildren(tr);

                //Remove already collapsed nodes from children so that we don't
                //make them visible.
                //(Confused? Remove this code and close Item 2, close Item 1
                //then open Item 1 again, then you will understand)
                var subnodes = children.filter('.expand');
                subnodes.each(function () {
                    var subnode = $(this);
                    var subnodeChildren = findChildren(subnode);
                    children = children.not(subnodeChildren);
                });
                //Change icon and hide/show children
                if (tr.hasClass('collpse')) {
                    tr.removeClass('collpse').addClass('expand');
                    children.hide();
                } else {
                    tr.removeClass('expand').addClass('collpse');
                    children.show();
                }
                return children;
            });
            function calmarkup(costitem){
                var calvalue = 0;
                var  percentvalue = 0;
                var qty = parseInt($(".qty"+costitem).val());
                var rate = parseInt($(".ratevalue"+costitem).val());
                var catitem =  $(".ratevalue"+costitem).data('cat');
                var subtotcat =  $(".qty"+costitem).data('subtotcat');
                if(qty){
                    calvalue = rate*qty;
                }else{
                    calvalue = rate*1;
                }
                var marktype = $(".markuptype"+costitem).val();
                $(".totalrate"+costitem).val(calvalue);
                var itemarray = [];
                itemarray['qty'] = qty;
                itemarray['rate'] = rate;
                itemarray['finalrate'] = calvalue;
                var token = '{{ csrf_token() }}';
              /*  $.ajax({
                    url: '{{ route('admin.projects.segment-updateboqcostitem') }}',
                    method: 'POST',
                    data: {
                        '_token':token,
                        'projectid':'{{ $id }}',
                        'title':'{{ $title }}',
                        'segment':'{{ $segment }}',
                        itemarray:itemarray,
                        itemid:costitem
                    }
                });*/
                $(".totalamount"+costitem).val(calvalue);

                var sum = 0;
                //iterate through each textboxes and add the values
                $(".subtotalamount"+subtotcat).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".subtotal"+subtotcat+"level0").html("₹"+sum.toFixed(2));


                var sum = 0;
                //iterate through each textboxes and add the values
                $(".finalamount"+catitem).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".catotal"+catitem).html("₹"+sum.toFixed(2));
                var tosum = 0;
                //iterate through each textboxes and add the values
                $(".grandvalue").each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        tosum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".grandtotal").html("₹"+tosum.toFixed(2));
            }
        $(document).on("change",".costitemcategory",function () {
                var positionid =  $(this).val();
                var catlevel = $(this).attr('data-catlevel');
                var level = $(this).attr('data-level');
                var parent = $(this).attr('data-parent');
                var itemid =    $("#costitemcatitem"+parent+" option[value='" + positionid + "']").attr('data-itemid');
                var itemname =    $("#costitemcatitem"+parent+" option[value='" + positionid + "']").attr('data-itemname');
                if(itemid=='add'){
                    var url = '{{ route('admin.activity.create')}}';
                    $('#modelHeading').html("Create New Activity");
                    $.ajaxModal('#categoryModalPop', url);
                }else{
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                    var segment = '{{ $segment }}';
                var trindex = $(this).closest('tr').index();
                     var token = "{{ csrf_token() }}";
                     $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.segment-addcostitemcategory') }}",
                        data: {
                            '_token': token,
                            'itemname': itemname,
                            'itemid': itemid,
                            'positionid': positionid,
                            'catlevel': catlevel,
                            'projectid': projectid,
                            'subprojectid': titleid,
                            'segmentid': segment,
                            'level': level,
                            'parent':parent
                        },
                        success: function(data){
                            boqitemloop();
                        }
                    });
                }
            });

            $(".context-menu-cost-item").click(function(){
                $('#CostitemsModal').modal('show');
            });
            $(".context-menu-category").click(function(){
                var url = '{{ route('admin.activity.create')}}';
                $('#modelHeading').html("Create New Activity");
                $.ajaxModal('#categoryModalPop', url);
            });
            $(document).on("change",".costitemrow",function () {
                var itemid = $(this).val();
                var cat = $(this).data('cat');
                var cattype = $(this).data('cattype');
                var positionid = $(this).data('positionid');
                var list = $(this).attr('list');
                if(itemid=='add'){
                    $('#costaddcat').val(cat);
                    $('#CostitemsModal').modal('show');
                }else{
                var projectid = '{{ $id }}';
                var subprojectid = '{{ $title }}';
                var segment = '{{ $segment }}';
                var trindex = $(this).closest('tr').index();
                if(cattype=='single'){
                    var catitem = $("tr.catitem"+cat).last().index();
                    trindex = catitem;
                    var catrow = $("tr.catrow"+cat).last().index();
                    if(catrow>0){
                        trindex = catrow;
                    }
                }else{
                    trindex = trindex-1;
                }
                    if(itemid){
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: "POST",
                            url: "{{ route('admin.projects.segment-addcostitemrow') }}",
                            data: {'_token': token, 'positionid': positionid,  'itemid': itemid,  'category': cat, 'projectid': projectid, 'subprojectid': subprojectid, 'segmentid': segment, 'cattype': cattype},
                            success: function(data){
                                boqitemloop();
                            }
                        });
                    }
                }
            });
        $(function () {

            $(".boqtable").sortable({
                items: 'tr:not(thead>tr)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                update: function (event, ui) {

                },
                stop: function (e, ui) {
                    var costitemarray = [];
                    var level1catarray = [];
                    var token = '{{ csrf_token() }}';
                    ui.item.removeClass("selected");
                    $('.colposition>tr').each(function (index) {
                        var costitemrow = $(this).attr('data-costitemrow');
                        if(costitemrow){
                            costitemarray.push(costitemrow);
                        }
                        var level1cat = $(this).attr('data-level1cat');
                        if(level1cat){
                            level1catarray.push(level1cat);
                        }
                    });
                    if(costitemarray){
                        $.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                'segment':'{{ $segment }}',
                                'position':costitemarray
                            },
                            type: 'POST',
                            url: '{{ route('admin.projects.segment-boqcostitemchangeposition') }}'
                        });
                    }
                    if(level1catarray){
                        $.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                'segment':'{{ $segment }}',
                                position:level1catarray
                            },
                            type: 'POST',
                            url: '{{ route('admin.projects.segment-boqcatchangeposition') }}'
                        });
                    }
                }
            });
           /* $('.boqtable').dragtable({persistState: function(table) {
                var token = '{{ csrf_token() }}';
                var selectarray = [];
                    table.sortOrder['_token']=token;
                    table.el.find('th').each(function(i) {
                        var col = $(this).attr("col");
                        if(col) {
                            selectarray.push(col);
                        }
                    });
                    $.ajax({
                        url: '{{ route('admin.projects.segment-boqcolchangeposition') }}',
                        method: 'POST',
                        data: {
                            '_token':token,
                            'projectid':'{{ $id }}',
                            'title':'{{ $title }}',
                            'segment':'{{ $segment }}',
                            'position':selectarray
                        },
                    });
                }
            });*/
            $(document).on("change",".updateproject",function(){
                var token = '{{ csrf_token() }}';
                var item = $(this).data('item');
                var itemid = $(this).data('itemid');
                var itemvalue = $(this).val();
                if(item=='assign_to'){
                    var itvalue = $('#assign'+itemid).find('option[value="' +itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                if(item=='contractor'){
                    var itvalue = $('#contractorid'+itemid).find('option[value="' +itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                $.ajax({
                    url: '{{ route('admin.projects.segment-updateboqcostitem') }}',
                    method: 'POST',
                    data: {
                        '_token':token,
                        'projectid':'{{ $id }}',
                        'subprojectid':'{{ $title }}',
                        'segmentid':'{{ $segment }}',
                        item:item,
                        itemid:itemid,
                        itemvalue:itemvalue
                    }
                });
            });
        });
        $('#costItemsUpdate').submit(function (e) {
            e.preventDefault();
            var costaddcat = $("#costaddcat").val();
            $.ajax({
                url: '{{url('admin/cost-items/name-store')}}',
                container: '#costItemsUpdate',
                type: "POST",
                data: $('#costItemsUpdate').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        /*window.location.reload();*/
                        $("#CostitemsModal").modal('hide');
                        costitemlist();
                    }
                }
            })
        });
        function costitemlist(){
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{url('admin/cost-items/cost-item-list')}}',
                type: "POST",
                data: { '_token':token},
                success: function (response) {
                        $(".costitemslist").html("");
                        $(".costitemslist").html(response);
                }
            })
        }
    </script>
@endpush