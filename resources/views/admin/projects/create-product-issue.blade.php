@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h4>Issue Products
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row">
                        {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                        {{ csrf_field() }}
                        <input type="hidden" name="project_id" value="{{ $projectId }}">
                        <div id="sortable">
                            <div class="col-xs-12 item-row margin-top-5" style="padding-bottom: 10px;">
                                <div class="col-md-12" style="float: left; padding-right: 20px;">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                                <select class="selectpicker form-control" name="product_id[1]" data-style="form-control">
                                                    <option value="">Please select Product</option>
                                                    @foreach($products as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                                <select class="selectpicker form-control" name="unit_id[1]" data-style="form-control">
                                                    <option value="">Please select Unit</option>
                                                    @foreach($units as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                                <input type="text" name="quantity[1]" class="form-control" placeholder="Quantity">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <button type="button" class="btn btn-info" id="add-item">
                                <i class="fa fa-plus"></i>
                                @lang('modules.invoices.addItem')
                            </button>
                            <button type="button" id="storeProduct" class="btn btn-success" style="float: right;"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>

        </div>
    <!-- .row -->


@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        var r = 1;
        $('#add-item').click(function () {
            r++;
            var item = '<div class="col-xs-12 item-row margin-top-5">'
                +'<div class="input-group col-md-4" style="float: left; padding-right: 5px;">'
                +'<select class="select2 form-control" name="product_id['+r+']" data-style="form-control">'
                +'<option value="">Select Product</option>';
            @foreach($products as $category)
                item += '<option value="{{ $category->id }}">{{ $category->name }}</option>';
            @endforeach
                item += '</select>'
                +'</div>'
                +'<div class="input-group col-md-4" style="float: left; padding-right: 5px;">'
                +'<select class="select2 form-control" name="unit_id['+r+']" data-style="form-control">'
                +'<option value="">Select unit</option>';
            @foreach($units as $category)
                item += '<option value="{{ $category->id }}">{{ $category->name }}</option>';
            @endforeach
                item += '</select>'
                +'</div>'
                +'<div class="col-md-3" style="float: left; padding-right: 20px;">'
                +'<div class="form-group">'
                +'<input type="text" class="form-control" name="quantity['+r+']" id="rate" placeholder="Quantity">'
                +'</div>'
                +'</div> <input type="hidden" name="serial" value="'+r+'">'
                +'<div class="col-md-1 text-right visible-md visible-lg">'
                +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
                +'<div class="row">'
                +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'</div>'
                +'</div>';
            $(item).hide().appendTo("#sortable").fadeIn(500);
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

        $('#storeProduct').click(function () {
            $.easyAjax({
                url: '{{route('admin.projects.storeIssuedProduct')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        })

        $('ul.showProjectTabs .ProductIssue').addClass('tab-current');


    </script>

@endpush