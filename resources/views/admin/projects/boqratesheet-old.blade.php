@extends('layouts.app')

@section('page-title')

@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            overflow-x: scroll;
            overflow-y: auto;
            max-height: 75vh;
            border: 2px solid #bdbdbd;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead{
            background-color: #C2DCE8;
        }
        .combo-sheet thead tr th{
            background-color: #C2DCE8 !important;
        }
        .combo-sheet thead tr th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;

        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 0 !important;
            font-size: 12px;
        }
        .combo-sheet tr td {
            position: relative;
        }
        .combo-sheet td input.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input, .combo-sheet tr.inFocus select{
            background-color: #eaf204;
        }
        .combo-sheet select.cell-inp{
            padding: 2.5px 0;
            width: 100%;
            border: none;
        }
        .combo-sheet tr td input[data-col="4"]{
            text-align: right;
            padding-right: 16px;
        }
        .rate-sheet.formula-sheet tr td input[data-col="4"]{
            text-align: left;
        }
        .open-combo-sheet{
            position: absolute;
            right: 5px;
            font-size: 16px;
            padding: 0 3px;
        }
        .rate-sheet.formula-sheet{
            position: absolute;
            left: 0;
            top: 0;
            overflow-x: scroll;
            overflow-y: auto;
            z-index: 2;
            background: #ffffff;
            margin: 0 5px;
            right: 0;
            display: none;
            max-height: 100vh;
            bottom: 0;
        }
        .combo-sheet table{
            border-collapse: collapse;
            border-spacing: 0px;
        }
        .combo-sheet select.cell-inp {
            padding: 2.5px 0;
            width: 100%;
            border: none;
        }
        .formula-sheet .loaderx{
            padding: 18% 0;
        }
        .white-box{
            position: relative;
        }
        .RowCategory{
            width: 63px;
        }
        .RowType{
            width: 45px;
        }
        .RowCode{
            width: 145px;
        }
        .RowDescription{
            width: 255px;
        }
        .RowUnit{
            width: 90px;
        }
        .RowBaseRate{
            width: 90px;
        }
        .RowSurcharge{
            width: 117px;
        }
        .RowDiscount{
            width: 108px;
        }
        .RowFactor{
            width: 90px;
        }
        .RowFinalRate{
            width: 90px;
        }
        .RowRemark{
            width: 360px;
        }
        .white-box{
            padding: 5px !important;
        }
        .pdl10{
            padding-left: 10px !important;
        }
        .page-title{
            color: #0f49bd;
            font-weight: 900;
        }
        .page-title i{
            margin-right: 5px;
        }
        input.cell-inp:disabled{
            background: #dddddd !important;
        }

    </style>

@endpush

@section('content')
    <div class="row pdl10">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="padding: 5px 15px;">
            <a href="javascript:;" class="close-rate-sheet pull-right"><i class="fa fa-times"></i> </a>
            <div class="col-md-3 pull-right">
                <select class="form-control selectfunction"  >
                    <option value="">Functions</option>
                    <option value="Waste()">Waste(n)%</option>
                    <option value="">---------------</option>
                    <option value="Min()">Min()</option>
                    <option value="Max()">Max()</option>
                    <option value="Sum()">Sum()</option>
                    <option value="Average()">Average()</option>
                    <option value="Abs()">Abs()</option>
                    <option value="Sqrt()">Sqrt()</option>
                    <option value="">---------------</option>
                    <option value="Round()">Round(n)</option>
                    <option value="RoundX()">Round(n;x)</option>
                    <option value="RoundUp()">RoundUp(n)</option>
                    <option value="RoundUpX()">RoundUp(n;x)</option>
                    <option value="RoundDown()">RoundDown(n)</option>
                    <option value="RoundDownX()">RoundDown(n;x)</option>
                    <option value="Ceiling()">Ceiling(n;x)</option>
                    <option value="Floor()">Floor(n;x)</option>
                    <option value="">---------------</option>
                    <option value="SinDeg()">SinDeg(n)</option>
                    <option value="CosDeg()">CosDeg(n)</option>
                    <option value="TanDeg()">TanDeg(n)</option>
                    <option value="">---------------</option>
                    <option value="AreaCir()">AreaCir(r)</option>
                    <option value="AreaTri()">AreaTri(b,h)</option>
                    <option value="AreaPyr()">AreaPyr(l,b,h)</option>
                    <option value="AreaRect()">AreaRect(x,y)</option>
                    <option value="AreaCyl()">AreaCyl(r,h)</option>
                    <option value="AreaCone()">AreaCone(r,h)</option>
                    <option value="AreaSph()">AreaSph(r)</option>
                    <option value="">---------------</option>
                    <option value="PerimCir()">PerimCir(r)</option>
                    <option value="PerimTriR()">PerimTriR(b,h)</option>
                    <option value="PerimRect()">PerimRect(x,y)</option>
                    <option value="">---------------</option>
                    <option value="VolPyr()">VolPyr(l,b,h)</option>
                    <option value="VolCyl()">VolCyl(r,h)</option>
                    <option value="VolCone()">VolCone(r,h)</option>
                    <option value="VolSph()">VolSph(r)</option>
                </select>
            </div>
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="combo-sheet cs">
                    <div class="table-wrapper">
                        <input type="hidden" name="product_cost_item" value="{{ $product_cost_item }}" class="product_cost_item" />
                        <table class="table table-bordered default footable-loaded footable" id="users-table" data-resizable-columns-id="users-table">
                            <form method="post" autocomplete="off">
                                <thead>
                                <tr>
                                    <th class="RowResource" data-resizable-column-id="category">@lang('modules.resources.resource')</th>
                                    <th class="RowUnit" data-resizable-column-id="unit">@lang('modules.resources.unit')</th>
                                    <th class="RowFinalRate" data-resizable-column-id="final_rate">@lang('modules.resources.rate')</th>
                                    <th class="RowFormula" data-resizable-column-id="remark">@lang('modules.resources.formula')</th>
                                    <th class="RowResult" data-resizable-column-id="remark">@lang('modules.resources.result')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $res_count = 0; @endphp
                                @if(count($resources)>0)
                                    <?php
                                    $res_count = count($resources);
                                    $totalRow = 35 - $res_count;
                                    if($totalRow <= 1){
                                        $totalRow = 2;
                                    }
                                    $row = 1;
                                    ?>
                                    @foreach($cost_item_resources as $costitemresource)
                                        <tr data-row="{{$row}}" data-id="{{ $costitemresource->id }}">
                                            <td data-col="0">
                                                <input name="resource[]" data-col="0" data-row="{{$row}}" list="resourceslist" list="costitem{{ $costitemresource->id }}" class="cell-inp category" disabled="disabled">
                                                <datalist class="resourceslist" id="costitem{{ $costitemresource->id }}">
                                                    <option value=""></option>
                                                    @foreach($resources as  $resource)
                                                        <option value="{{$resource->id }}" @if($resource->id == $costitemresource->id) selected @endif>{{ $resource->type.' - '.$resource->description }}</option>
                                                    @endforeach
                                                </datalist>
                                            </td>
                                            <td data-col="1"><input type="text" name="unit[]" data-col="1" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->unit }}"></td>
                                            <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->rate }}" /></td>
                                            <td data-col="3"><input type="text" name="formula[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->formula }}"></td>
                                            <td data-col="4"><input type="text" name="result[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->remark }}"></td>
                                        </tr>
                                        <?php $row++ ?>
                                    @endforeach
                                    @for($i=0;$i<$totalRow;$i++)
                                        @if($i==0)
                                            <tr data-row="{{$row}}">
                                                <td data-col="0">
                                                    <input name="resource[]" data-col="0" data-row="{{$row}}" list="costitemlist" class="cell-inp resource" >
                                                    <datalist class="resourceslist" id="costitemlist">
                                                        <option value=""></option>
                                                        @foreach($resources as  $resource)
                                                            <option value="{{$resource->id }}" @if($resource->id == $costitemresource->id) selected @endif>{{ $resource->type.' - '.$resource->description }}</option>
                                                        @endforeach
                                                    </datalist>
                                                </td>
                                                <td data-col="1"><input type="text" name="unit[]" data-col="1" data-row="{{$row}}" class="cell-inp"></td>
                                                <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp"></td>
                                                <td data-col="3"><input type="text" name="formula[]" data-col="3" data-row="{{$row}}" class="cell-inp"></td>
                                                <td data-col="4"><input type="text" name="result[]" data-col="4" data-row="{{$row}}" class="cell-inp"></td>
                                            </tr>
                                        @else
                                            <tr data-row="{{$row}}">
                                                <td data-col="1"><input type="text" name="resource[]" data-col="1" data-row="{{$row}}" class="cell-inp cell-new category"></td>
                                                 <td data-col="2"><input type="text" name="unit[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                                <td data-col="3"><input type="text" name="rate[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                                <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                                <td data-col="5"><input type="text" name="remark[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                            </tr>
                                        @endif
                                        <?php
                                        $row++;
                                        ?>
                                    @endfor
                                @endif
                                </tbody>
                            </form>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('member.rfq.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this PO?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this PO?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('member.purchase-order.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                  swal("Updated!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

        });

        function loadTable() {
            table = $('#users-table').resizableColumns({
                responsive: true,
                processing: true,
                destroy: true,
                stateSave: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                }
            });
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var rfq = $('#rfq').val();
            var status = $('#status').val();

            var url = '{{ route('member.rfq.export', [':status', ':rfq']) }}';
            url = url.replace(':rfq', rfq);
            url = url.replace(':status', status);

            window.location.href = url;
        }
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });


    </script>
@endpush