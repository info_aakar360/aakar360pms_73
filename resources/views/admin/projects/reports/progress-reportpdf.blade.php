<!Doctype>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" media="all" href="{{  base_path('bootstrap/dist/css/bootstrap.css') }}" />
    {{--<link rel="stylesheet" type="text/css" media="all" href="https://localhost/aakar360pms_new/bootstrap/dist/css/bootstrap.css" />--}}
    <link rel="stylesheet"type="text/css" media="all"  href="{{  base_path('css/style.css') }}" />
    <link rel="stylesheet" type="text/css" media="all"  href="{{  base_path('css/custom.css') }}" />
    <style>
        @page { margin: 0cm 0cm; }
        .page-break {
            page-break-after: always;
        }
        body{
            background-color: #fff;
        }
        .carddesign{
            background-color: #dae3f4;
            padding: 8px;
            border-top-left-radius:6px;
            border-top-right-radius:6px;
        }
        table {
            border-collapse:collapse; table-layout:fixed;
        }
        table tr td,  table tr th {
            border: 1px solid #dae3f4;
            padding:2px;
            font-weight: normal;
            word-wrap:break-word;
            page-break-after: always !important;
        }
        .full-image img {
            width: 100% !important;
            height: 200px;
            object-fit: contain;
            background: #fff;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            left: 8.5cm;
            right: 0cm;
            height: 1cm;
        }
        header {
            position: fixed;
            top:0cm;
            left: 8.5cm;
            right: 0cm;
            height: 1cm;
        }
    </style>
</head>
<body>
{{--
<header>
    <p style="font-size: 17px">&nbsp;</p>
</header>--}}
<footer>
    <p style="font-size: 17px">Powered By: <img src="{{ public_path('uploads/aakar-logo.png') }}" width="100px" style="margin-top:5px;" /></p>
</footer>

<main>
<img style="width: 800px;height: 1100px;margin-top: -1cm;" src="{{ public_path('uploads/pdf-front.png') }}" >

<div class="row" style="left: 45%;bottom: 34%;margin-bottom:10px;display:block;position: absolute" >
    <div class="col-md-6" style="float:left;width:15%;margin-bottom: 20px;">
                @if(!empty($company->logo))
            <div style="width: 60px;height:60px;border-radius: 20px;background-color: #dae3f4;">
                <img style="width: 90%;object-fit:cover;display: block;margin-top: 25px;margin-left: 3px; "  src="{{ public_path('uploads/app-logo/'.$company->logo) }}"  alt=""/>
            </div>
                @else
        <div style="width: 45px;height:45px;border-radius: 10px;background-color: #dae3f4;">
                <p style="width: 90%;font-size:25px;display: block;margin-left: 13px;">{{ strtoupper($company->company_name[0]) }}</p>
            </div>
                @endif
    </div>
    <div class="col-md-6" style="float:left;left: 70%;margin-top: 10px;margin-bottom: 20px;">
        <span style="font-size:25px;text-align: left;top:-0.7%;position: absolute">{{ ucwords($company->company_name) }}</span>
    </div>
</div>
<div class="row  mb-20" style="left: 45%;bottom: 18%;position: absolute;" >
    <p style="border-bottom:1px solid #dae3f4;margin-right: 20px;bottom: 13%;position: relative;">&nbsp;</p>
    <table style="margin-top: 10px;border: 0px !important;">
        <tbody>
        <tr>
            <td style="border: 0px !important;">Project Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ sub_str(get_project_name($projectid),25) }}</td>
         </tr>
        <tr>
            <td style="border: 0px !important;">Report Duration: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d M Y',strtotime($startdate)) }} - {{ date('d M Y',strtotime($enddate)) }}</td>
          </tr>
        <tr>
            <td style="border: 0px !important;">This Report Include: @if(!empty($reporttext)){{ ucwords(implode(', ',array_slice($reporttext, 0, 3))) }}@endif</td>

        </tr>
        @if(!empty($reporttext)&&array_slice($reporttext, 3, 3))
        <tr>
            <td style="width: 30%;border: 0px !important;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@if(!empty($reporttext)){{ ucwords(implode(', ',array_slice($reporttext, 3, 3))) }}@endif</td>

        </tr>
            @endif

        </tbody>
    </table>
</div>
    <div class="page-break"></div>
    <?php $count = 0;?>
    @if(!empty($reporttype)&&in_array('progressreport',$reporttype))
        <div class="row" style="margin:10px;margin-top:60px;" >
<div  class="carddesign" >
    <div class="row">
        <div style="background-color:#fbfcf8;border-radius:10px;padding: 10px;margin: 5px;text-align: center;display: inline-block;width: 95%;">
            <div class="row">
                <div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <img style="width: 25px;height: 25px;"  src="{{ public_path('user-uploads/not-started.png') }}" ></div><div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <strong>Not Started</strong></div><div class="col-md-4 pull-left" style="width: 33%;"> <strong> {{ $notstarted ?: 0 }}</strong></div>
             </div>
        </div>
    </div>
    <div class="row">
        <div style="background-color:#fbfcf8;border-radius:10px;padding: 10px;margin: 5px;text-align: center;display: inline-block;width: 95%;">
            <div class="row">
                <div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <img style="width: 25px;height: 25px;"  src="{{ public_path('user-uploads/in-progress.png') }}" ></div><div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <strong>In Progress</strong></div><div class="col-md-4 pull-left" style="width: 33%;"> <strong> {{ $inprogress ?: 0 }}</strong></div>
             </div>
        </div>
    </div>
    <div class="row">
        <div style="background-color:#fbfcf8;border-radius:10px;padding: 10px;margin: 5px;text-align: center;display: inline-block;width: 95%;">
            <div class="row">
                <div class="col-md-4 pull-left" style="text-align: left;width:33%;"> <img style="width: 25px;height: 25px;"  src="{{ public_path('user-uploads/in-problem.png') }}" ></div><div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <strong>In Problem</strong></div><div class="col-md-4 pull-left" style="width: 33%;"> <strong> {{ $inproblem ?: 0 }}</strong></div>
             </div>
        </div>
    </div>
    <div class="row">
        <div style="background-color:#fbfcf8;border-radius:10px;padding: 10px;margin: 5px;text-align: center;display: inline-block;width: 95%;">
            <div class="row">
                <div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <img style="width: 25px;height: 25px;"  src="{{ public_path('user-uploads/delay.png') }}" ></div><div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <strong>Delayed</strong></div><div class="col-md-4 pull-left" style="width: 33%;"> <strong> {{ $delayed ?: 0 }}</strong></div>
             </div>
        </div>
    </div>
    <div class="row">
        <div style="background-color:#fbfcf8;border-radius:10px;padding: 10px;margin: 5px;text-align: center;display: inline-block;width: 95%;">
            <div class="row">
                <div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <img style="width: 25px;height: 25px;"  src="{{ public_path('user-uploads/completed.png') }}" ></div><div class="col-md-4 pull-left" style="text-align: left;width: 33%;"> <strong>Completed</strong></div><div class="col-md-4 pull-left" style="width: 33%;"> <strong> {{ $completed ?: 0 }}</strong></div>
             </div>
        </div>
    </div>
</div>
</div>
        <div class="row" style="margin:10px;" >
    <div  class="carddesign">
        <h6>Progress Report</h6>
         </div>
    <table  style="font-size: 8px;text-align: center; width:100%; margin-bottom:10px;border-spacing: 0px;" >
        <thead>
        <tr style="text-align: center;border:1px solid #dae3f4;">
            <th  colspan="5"></th>
            <th  colspan="3"  style="text-align: center;border:1px solid #dae3f4;" >Quantity</th>
            <th ></th>
            <th  colspan="3"  style="text-align: center;border:1px solid #dae3f4;" >Amount</th>
        </tr>
        <tr style="text-align: center;border:1px solid #dae3f4;">
            <th style="text-align: center;"><strong>@lang('app.contractor')</strong></th>
            <th  style="text-align: center;"><strong>@lang('app.activity')</strong></th>
            <th  style="text-align: center;"><strong>@lang('app.task')</strong></th>
            <th  style="text-align: center;"><strong>Unit</strong></th>
            <th  style="text-align: center;"><strong>Total Qty</strong></th>
            <th  style="text-align: center;"><strong>Previous</strong></th>
            <th  style="text-align: center;"><strong>Progress</strong></th>
            <th  style="text-align: center;"><strong>Cumulative</strong></th>
            <th  style="text-align: center;"><strong>Rate</strong></th>
            <th  style="text-align: center;"><strong>Previous</strong></th>
            <th  style="text-align: center;"><strong>Progress</strong></th>
            <th  style="text-align: center;"><strong>Cumulative</strong></th>
        </tr>
        </thead>
        <tbody>
        @if($projectid)
            <?php
            function reporthtml($reportarray){

            $projectid = $reportarray['projectid'];
            $segmentid = $reportarray['segmentid'];
            $subprojectid = $reportarray['subprojectid'];
            $parent = $reportarray['parent'];
            $level = $reportarray['level'];
            $contracid = $reportarray['contractor'];
            $dateformat = $reportarray['dateformat'];
            $startdate = $reportarray['startdate'];
            $enddate = $reportarray['enddate'];
            $exportype = $reportarray['exportype'];
            $proproget = $reportarray['categoryarray'];
            $count = $reportarray['count'];

            foreach ($proproget as $propro){
            $catvalue = $propro->itemid;
            if(!empty($propro->catlevel)){
                $catvalue = $propro->catlevel.','.$propro->itemid;
            }
            $start_date = $startdate.' 00:00:01';
            $end_date = $enddate.' 23:59:59';
            if(!empty($segmentid)){
                $projectcostitemsarray = \App\Task::join('project_segments_product','project_segments_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_segments_product.unit','project_segments_product.finalrate','project_segments_product.qty','project_segments_product.contractor','project_segments_product.description')
                    ->where('project_segments_product.position_id',$propro->id)->where('project_segments_product.project_id',$projectid)
                    ->where('project_segments_product.segment',$segmentid);
                if(!empty($subprojectid)){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_segments_product.title',$subprojectid);
                }
            }else{
                $projectcostitemsarray = \App\Task::join('project_cost_items_product','project_cost_items_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_cost_items_product.unit','project_cost_items_product.finalrate','project_cost_items_product.qty','project_cost_items_product.contractor','project_cost_items_product.description')
                    ->where('project_cost_items_product.position_id',$propro->id)->where('project_cost_items_product.project_id',$projectid);

                if(!empty($subprojectid)){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.title',$subprojectid);
                }
                if($contracid){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor',$contracid);
                }else{
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor','0');
                }
            }
            if(!empty($start_date)&&!empty($end_date)){
                $projectcostitemsarray = $projectcostitemsarray->where('task_percentage.created_at','>=',$start_date)->where('task_percentage.created_at','<=',$end_date);
            }
            $projectcostitemsarray = $projectcostitemsarray->groupby('task_percentage.task_id')->get();

            if(count($projectcostitemsarray)>0){
            ?>
            <tr class="subcat"  style="text-align: center;border:1px solid #dae3f4;">
                <td></td>
                <td><strong>{{ ucwords($propro->itemname) }}</strong></td>
                <td colspan="10"></td>
            </tr>
            <?php
            foreach ($projectcostitemsarray as $projectcostitems){
            if(!empty($projectcostitems)){
            $totalquantity = !empty($projectcostitems->qty) ? $projectcostitems->qty : 0;
            $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
            $progress = $prevcumiliquantity = 0;
            $prevcumiliquantity = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->sum('todayqty');
            $cumiliquantitytill = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->orderBy('id','desc')->first();
            $cumiliquantitypercenttill = !empty($cumiliquantitytill) ? $cumiliquantitytill->percentage : 0;
            if(!empty($cumiliquantity)&&!empty($totalquantity)){
                $cumiliquantitypercenttill =   ($cumiliquantity/$totalquantity)*100;
            }
            $progress = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->sum('todayqty');
            $percentagetable = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->orderBy('id','desc')->first();
            $percentage = !empty($percentagetable) ? $percentagetable->percentage : 0;
            $percentage = $percentage-$cumiliquantitypercenttill;
            if(!empty($progress)&&!empty($totalquantity)){
                $percentage =  !empty($progress) ? ($progress/$totalquantity)*100 : 0;
            }
            $cumiliquantity = $prevcumiliquantity+$progress;
            $cumiliquantitypercent = $cumiliquantitypercenttill+$percentage;
            $rate = $projectcostitems->finalrate;
            $cumiliquantitytillamt = $prevcumiliquantity*$rate;
            $cumiliquantityamt = $cumiliquantity*$rate;
            $progressamt = $progress*$rate;
            ?>
            <tr  style="text-align: center;border:1px solid #dae3f4;">
                @if($projectcostitemsarray[0]->id==$projectcostitems->id)
                    <td  rowspan="<?php echo count($projectcostitemsarray);?>"  ><strong>{{ get_users_contractor_name($projectcostitems->contractor,$projectcostitems->company_id) ?: 'Departmental' }}</strong></td>
                @endif
                <td  ></td>
                    <td  >{{ ucwords($projectcostitems->heading) }}</td>
                    <td  >{{ get_unit_name($projectcostitems->unit) }}</td>
                    <td  >{{  $totalquantity }}</td>
                    <td  >{{ $prevcumiliquantity }} ({{ !empty($cumiliquantitypercenttill) ? numberformat($cumiliquantitypercenttill).'%' : '0%' }})</td>
                    <td  >{{ numberformat($progress) }} ({{ numberformat($percentage) }}%)</td>
                    <td  >{{ numberformat($cumiliquantity) }} ({{ numberformat($cumiliquantitypercent) }}%)</td>
                    <td  >{{ numberformat($rate) }}</td>
                    <td  >{{ numberformat($cumiliquantitytillamt) }}</td>
                    <td  >{{ numberformat($progressamt) }}</td>
                    <td  >{{ numberformat($cumiliquantityamt) }}</td>
            </tr>
            <?php
            $count++;
            }

            }
            }
            $newlvel = (int)$propro->level+1;
            $parent = $propro->id;
            if(!empty($segmentid)){
                $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
            }else{
                $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
            }
            if($subprojectid){
                $proproget = $proproget->where('title',$subprojectid);
            }
            $proproget = $proproget->where('position','row')->where('level',$newlvel)->where('parent',$parent)->orderBy('inc','asc')->get();

            $count++;
            $reportarray = array();
            $reportarray['projectid'] = $projectid;
            $reportarray['segmentid'] = $segmentid;
            $reportarray['subprojectid'] = $subprojectid;
            $reportarray['parent'] = $propro->id;
            $reportarray['level'] = $newlvel;
            $reportarray['contractor'] = $contracid;
            $reportarray['dateformat'] = $dateformat;
            $reportarray['startdate'] = $startdate;
            $reportarray['enddate'] = $enddate;
            $reportarray['exportype'] = $exportype;
            $reportarray['categoryarray'] = $proproget;
            $reportarray['count'] = $count;
            echo reporthtml($reportarray);

                }
            }
            $contr = array('0'=>'Departmental');
            $contractorarray = $contractorarray+$contr;
            if(!empty($contractorarray)){
            foreach ($contractorarray as $contracid => $contracname){
            ?>
            {{--<tr>
                <td  ><b>{{ ucwords($contracname) }}</b></td>
                <td   colspan="11"></td>
            </tr>--}}
            <?php
            if(!empty($segmentid)){
                $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
            }else{
                $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
            }
            if($subprojectid){
                $proproget = $proproget->where('title',$subprojectid);
            }
            $proproget = $proproget->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();


            $reportarray = array();
            $reportarray['projectid'] = $projectid;
            $reportarray['segmentid'] = $segmentid;
            $reportarray['subprojectid'] = $subprojectid;
            $reportarray['parent'] = 0;
            $reportarray['level'] = 0;
            $reportarray['contractor'] = $contracid;
            $reportarray['dateformat'] = $dateformat;
            $reportarray['startdate'] = $startdate;
            $reportarray['enddate'] = $enddate;
            $reportarray['exportype'] = $exportype;
            $reportarray['categoryarray'] = $proproget;
            $reportarray['count'] = 0;
            echo reporthtml($reportarray);

            }  } ?>

        @endif
        </tbody>
    </table>
</div>
    @endif
    <div style="page-break-after:auto;"></div>
    @if(!empty($reporttype)&&in_array('photos',$reporttype))
        @if(count($tasksimagesarray)>0)
            <?php
            $count = 0;
                if(!empty($reporttype)&&in_array('progressreport',$reporttype)){
            $start_date = $startdate.' 00:00:01';
            $end_date = $enddate.' 23:59:59';
            if(!empty($segmentid)){
                $projectcostitemsarray = \App\Task::join('project_segments_product','project_segments_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_segments_product.position_id','project_segments_product.unit','project_segments_product.finalrate','project_segments_product.qty','project_segments_product.contractor','project_segments_product.description')
                    ->where('project_segments_product.project_id',$projectid)
                    ->where('project_segments_product.segment',$segmentid);
                if(!empty($subprojectid)){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_segments_product.title',$subprojectid);
                }
            }else{
                $projectcostitemsarray = \App\Task::join('project_cost_items_product','project_cost_items_product.id','=','tasks.cost_item_id')
                    ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                    ->select('tasks.*','project_cost_items_product.position_id','project_cost_items_product.unit','project_cost_items_product.finalrate','project_cost_items_product.qty','project_cost_items_product.contractor','project_cost_items_product.description')
                    ->where('project_cost_items_product.project_id',$projectid);
                if(!empty($subprojectid)){
                    $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.title',$subprojectid);
                }
            }
            if(!empty($start_date)&&!empty($end_date)){
                $projectcostitemsarray = $projectcostitemsarray->where('task_percentage.created_at','>=',$start_date)->where('task_percentage.created_at','<=',$end_date);
            }
            $projectcostitems = $projectcostitemsarray->where('tasks.percentage','<>','0')->groupby('task_percentage.task_id')->get()->toArray();
             if(!empty($projectcostitems)){
                $count += count($projectcostitems);
                $activtyrow = array_unique(array_filter(array_column($projectcostitems,'position_id')));
                if(!empty($activtyrow)){
                    $count += count($activtyrow);
                }
            }
                }
            $storage = storage();
                ?>
            <div class="row" style="margin:10px 10px 30px 10px;" >
            <div  class="carddesign" style="margin-bottom: 10px;">
                <h6>Photos</h6>
            </div>
            </div>
                <div class="row" style="text-align: left;margin:10px 10px 15px 10px;" >
                    <table>
                        <tbody>
                        <tr style="margin-bottom: 20px;">
                        <?php $x=1; $pb = 0; $row = 1;?>
                        @foreach($tasksimagesarray as $file)
                            <?php
                                if(!empty($file->hashname)&&file_exists(public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname))){
                                $imagename = $file->hashname;
                                $fx = explode('.', $imagename);
                                $ext = $fx[(count($fx)-1)];
                                if($ext=='jpg'||$ext=='jpeg'||$ext=='png'){
                                    ?>
                            <td style="width:245px;padding: 5px;display: inline-block;border: 1px solid #dae3f4;page-break-after: always !important;">
                                <?php if($file->external_link != ''){
                                    $imgurl = $file->external_link;
                                    }elseif($storage == 'local'){
                                        $imgurl = public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname);
                                    }elseif($storage == 's3'){
                                            $imgurl = public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname);
                                    }elseif($storage == 'google'){
                                       $imgurl = $file->google_url;
                                    }elseif($storage == 'dropbox'){
                                        $imgurl = $file->dropbox_link;
                                    }
                                ?>
                                    <img  style="padding:0px 5px;display: inline-block;margin-top:15px;width: 235px;height: 160px"  src="{{ $imgurl }}"  />
                                <p style="text-align: center;"><?php echo date('d M Y',strtotime($file->created_at));?></p>
                            </td>
                            <?php

                                if($x%3==0){?>
                             </tr>
                        </tbody>
                    </table>
                </div>
                <?php
                    $margintop = '20px';
                if(empty($count)&&$row%4==0){
                    echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }
                if(($count>4&&$row==1)&&$count<7){
                    $pb = 2;
                    echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }
                if(($count>=7&&$row==1)&&$count<=12){
                    $pb = 1;
                    echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }
                if($count>12&&$row==1){
                    $pb = 1;
                    echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }
                $newrow = $row-$pb;
                if(($count>4&&$newrow>3&&$newrow%4==0)||($count>=7&&$newrow>3&&$newrow%4==0)||($count>=13&&$newrow>3&&$newrow%4==0)){
                    $pb = $pb+4;
                    echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }
               /*  if($row-$pb == 4){
                     echo '<div class="page-break"></div>';
                    $margintop = '35px';
                }*/

                ?>
                <div class="row" style="text-align: left;margin:{{ $margintop }} 10px 10px 10px;" >
                    <table>
                        <tbody>
                        <tr style="margin-bottom: 20px;">
                            <?php $row++; } ?>
                            <?php  $x++; } }?>
                          @endforeach
                        </tr>
                    </tbody>
                    </table>
                </div>
        @endif
    @endif

    @if(!empty($reporttype)&&in_array('labourattendance',$reporttype))
        @if(!empty($contractorarray))
         <?php
            $contr = array('0'=>'Departmental');
            $contractorarray = $contractorarray+$contr;
            $start_date = $startdate.' 00:00:01';
            $end_date = $enddate.' 23:59:59';
            $contractkeys = array_keys($contractorarray);
            $mancontractorsarry= \App\ManpowerLog::where('company_id',$projectdetails->company_id)
                ->whereIn('contractor',$contractkeys)
                ->where('created_at','>=',$start_date)
                ->where('created_at','<=',$end_date)->first();
            if(!empty($mancontractorsarry->id)){
            ?>   <div class="row" style="margin: 0px 10px 10px 10px;" >
                <div  class="carddesign">
                    <h6>Labour Attendance Report</h6>
                </div>
                <table  style="font-size: 8px;text-align: center; width:100%; margin-bottom:10px;border-spacing: 0px;" >
                    <thead>
                    <tr  style="text-align: center;border:1px solid #dae3f4;">
                        <th style="text-align: center;"><strong>Contractor</strong></th>
                        <th style="text-align: center;"><strong>Attendance</strong></th>
                        <th style="text-align: center;"><strong>Date</strong></th>
                        <th style="text-align: center;"><strong>No. of Workers</strong></th>
                        <th style="text-align: center;"><strong>Work Hours</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contractorarray as $contractor => $name)
                        {{-- <tr>
                             <th ><strong>{{ ucwords($name) }}</strong></th>
                             <th colspan="2"></th>
                         </tr>--}}
                        <?php
                        $x=1;

                        $mancatarry= \App\ManpowerLog::join('manpower_attendance','manpower_attendance.manpower_id','=','manpower_logs.id')
                                            ->where('manpower_logs.company_id',$projectdetails->company_id);
                        $mancatarry = $mancatarry->where('manpower_attendance.created_at','>=',$start_date)->where('manpower_attendance.created_at','<=',$end_date);
                        if(!empty($contractor)){
                            $mancatarry = $mancatarry->where('manpower_logs.contractor',$contractor);
                        }else{
                            $mancatarry = $mancatarry->where('manpower_logs.contractor','0');
                        }
                        if(!empty($projectid)){
                            $mancatarry = $mancatarry->where('manpower_logs.project_id',$projectid);
                        }
                        if(!empty($subprojectid)){
                            $mancatarry = $mancatarry->where('manpower_logs.title_id',$subprojectid);
                        }
                        if(!empty($segmentid)){
                            $mancatarry = $mancatarry->where('manpower_logs.segment_id',$segmentid);
                        }
                        $mancontcount = $mancatarry->where('manpower_logs.manpower','>','0')->count();
                        $mancatpluck = $mancatarry->pluck('manpower_attendance.manpower_category')->toArray();
                        if(!empty($mancontcount)&&!empty($mancatpluck)){

                        $manpowercategoryarray = \App\ManpowerCategory::where('company_id',$projectdetails->company_id)->whereIn('id',$mancatpluck)->get();

                        $alldates = diffRanges($startdate,$enddate);
                        if(count($manpowercategoryarray)>0){
                        foreach ($manpowercategoryarray as $manpowercategory){

                            $mancount = \App\ManpowerLog::join('manpower_attendance','manpower_attendance.manpower_id','=','manpower_logs.id')
                                                ->where('manpower_attendance.manpower_category',$manpowercategory->id)->where('manpower_attendance.created_at','>=',$start_date)->where('manpower_attendance.created_at','<=',$end_date);
                            if(!empty($contractor)){
                                $mancount = $mancount->where('manpower_logs.contractor',$contractor);
                            }else{
                                $mancount = $mancount->where('manpower_logs.contractor','0');
                            }
                            if(!empty($projectid)){
                                $mancount = $mancount->where('manpower_logs.project_id',$projectid);
                            }
                            if(!empty($subprojectid)){
                                $mancount = $mancount->where('manpower_logs.title_id',$subprojectid);
                            }
                            if(!empty($segmentid)){
                                $mancount = $mancount->where('manpower_logs.segment_id',$segmentid);
                            }
                            $manpowerarray = $mancount->orderBy('manpower_attendance.created_at','asc')->get();
                            if(count($manpowerarray)>0){
                              foreach ($manpowerarray as $manpower){
                            ?>
                            <tr  style="border:1px solid #dae3f4;">
                                @if($x==1)
                                    <td  rowspan="{{ $mancontcount }}">{{ ucwords($name) }}</td>
                                @endif
                                <td >{{ ucwords($manpowercategory->title) }}</td>
                                <td >{{ date('d M Y',strtotime($manpower->created_at)) }}</td>
                                    <td >{{ ucwords($manpower->manpower) }}</td>
                                <td >{{ ucwords($manpower->workinghours).':'.$manpower->workingminutes }}</td>
                            </tr>
                        <?php  $x++;  } }   } }  }?>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <?php }?>
        @endif
    @endif

@if(!empty($reporttype)&&in_array('issues',$reporttype))
@if(count($issueslist)>0)
            <div class="row" style="margin: 0px 10px 10px 10px;" >
    <div  class="carddesign">
        <h6>Issues Report</h6>
    </div>
        <table  style="font-size: 8px;text-align: center; width:100%; margin-bottom:10px;border-spacing: 0px;" >
            <thead>
            <tr  style="text-align: center;border:1px solid #dae3f4;">
                <th style="text-align: center;"><strong>Issues</strong></th>
                <th style="text-align: center;"><strong>Created Date</strong></th>
                <th style="text-align: center;"><strong>Task</strong></th>
                <th style="text-align: center;"><strong>Due date</strong></th>
                <th style="text-align: center;"><strong>Status</strong></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($issueslist as $issues){
            if(!empty($issues->task_id)){
                $taskdetails = \App\Task::where('id',$issues->task_id)->first();
            }
            ?>
            <tr  style="border:1px solid #dae3f4;">
                <td >{{ ucwords(sub_str($issues->title,30)) }}</td>
                <td >{{ !empty($issues->created_at) ? date('d M Y',strtotime($issues->created_at)) : '' }}</td>
                <td >@if(!empty($taskdetails->heading)) {{ ucwords(sub_str($taskdetails->heading,30)) }} @endif</td>
                <td >{{ !empty($issues->due_date) ? date('d M Y',strtotime($issues->due_date)) : '' }}</td>
                <td >{{ $issues->status }}</td>
            </tr>
            <?php }?>
            </tbody>
        </table>
        </div>
@endif
@endif
@if(!empty($reporttype)&&in_array('indent',$reporttype))
@if(count($indentslist)>0)
            <div class="row" style="margin: 0px 10px 10px 10px;" >
    <div  class="carddesign">
        <h6>Indents Report</h6>
    </div>
        <table  style="font-size: 8px;text-align: center; width:100%; margin-bottom:10px;border-spacing: 0px;" >
            <thead>
            <tr  style="text-align: center;border:1px solid #dae3f4;">
                <th style="text-align: center;"><strong>Indent no</strong></th>
                <th style="text-align: center;"><strong>Material</strong></th>
                <th style="text-align: center;"><strong>Quantity</strong></th>
                <th style="text-align: center;"><strong>Unit</strong></th>
                <th style="text-align: center;"><strong>Created Date</strong></th>
                <th style="text-align: center;"><strong>Expected date</strong></th>
            </tr>
            </thead>
            <tbody>
            <?php   foreach($indentslist as $indentsl){   ?>
            <tr style="text-align: center;border:1px solid #dae3f4;">
                <td>{{ ucwords($indentsl->indent_no) }}</td>
                <td >{{ get_ind_pro_name($indentsl->cid) }}</td>
                <td >{{ $indentsl->quantity }}</td>
                <td >{{ get_unit_name($indentsl->unit) }}</td>
                <td >{{ date('d M Y',strtotime($indentsl->created_at)) }}</td>
                <td >{{ date('d M Y',strtotime($indentsl->expected_date)) }}</td>
            </tr>
            <?php } ?>

            </tbody>
        </table>
        </div>
@endif
@endif
</main>
</body>
</html>

