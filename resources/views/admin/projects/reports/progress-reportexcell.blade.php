<?php
    $exportarray = array();
    if($exportype=='mobile'){
        $exportarray[] = ['Activity',  '% Progress', '% Completed'];
    }else{
        $exportarray[] = ['Activity', 'Segments', 'Unit', 'Total qty', 'Cumilative Achived Till', 'Planned for the day', 'Achived', 'Cumilative Achived', 'Balance', '% Progress', '% Complete', 'Planned for next', 'Remarks'];
    }

    if($projectid){
                            function reportexcell($reportarray){
                                $exportarray = array();
                                $projectid = $reportarray['projectid'];
                                $subprojectid = $reportarray['subprojectid'];
                                $segmentid = $reportarray['segmentid'];
                                $parent = $reportarray['parent'];
                                $level = $reportarray['level'];
                                $contracid = $reportarray['contractor'];
                                $dateformat = $reportarray['dateformat'];
                                $startdate = $reportarray['startdate'];
                                $enddate = $reportarray['enddate'];
                                $exportype = $reportarray['exportype'];

                                if(!empty($segmentid)){
                                    $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
                                }else{
                                    $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
                                }
                                if($subprojectid){
                                    $proproget = $proproget->where('title',$subprojectid);
                                }
                                $proproget = $proproget->where('position','row')->where('level',$level)->where('parent',$parent)->orderBy('inc','asc')->get();

                                foreach ($proproget as $propro){
                                    $catvalue = $propro->itemid;
                                    if(!empty($propro->catlevel)){
                                        $catvalue = $propro->catlevel.','.$propro->itemid;
                                    }
                                    if($exportype=='mobile'){
                                        $exportarray[] = [ucwords($propro->itemname), '',''];
                                    }else{
                                      $exportarray[] = [ucwords($propro->itemname), '','', '', '','', '', '', '', '', '', ''];
                                    }
                                    if(!empty($segmentid)){
                                        $projectcostitemsarray = \App\ProjectSegmentsProduct::where('category',$catvalue)->where('project_id',$projectid)->where('segment',$segmentid);
                                    }else{
                                        $projectcostitemsarray = \App\ProjectCostItemsProduct::where('category',$catvalue)->where('project_id',$projectid);
                                    }
                                    if(!empty($contracid)){
                                        $projectcostitemsarray = $projectcostitemsarray->where('contractor',$contracid);
                                    }else{
                                        $projectcostitemsarray = $projectcostitemsarray->whereNull('contractor');
                                    }
                                    if($subprojectid){
                                        $projectcostitemsarray = $projectcostitemsarray->where('title',$subprojectid);
                                    }
                                    $projectcostitemsarray = $projectcostitemsarray->where('position_id',$propro->id)->get();

                                    $exportdata = array();
                                    foreach ($projectcostitemsarray as $projectcostitems){
                                        if($projectcostitems->id){
                                            $tasks = \App\Task::where('cost_item_id',$projectcostitems->id)->first();
                                            $totalquantity = $projectcostitems->qty;
                                            $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
                                            if(!empty($tasks->id)){
                                                $comarchive = 0;
                                                $percentage =  $tasks->percentage;
                                                $start_date = $startdate.' 00:00:00';
                                                $end_date = $enddate.' 23:59:59';
                                                $prevarchived = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<',$start_date)->orderBy('id','desc')->max('percentage');
                                                $prevarchived = !empty($prevarchived) ? (int) $prevarchived : 0;
                                                $perachivied = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<=',$end_date)->orderBy('id','desc')->max('percentage');
                                                $perachivied = !empty($perachivied) ? (int) $perachivied : 0;
                                                $comarchive = $perachivied-$prevarchived;
                                                if(empty($exportype)){
                                                    $cumiliquantitytill = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<',$start_date)->sum('taskqty');
                                                    $achivied = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','>',$start_date)->where('created_at','<',$end_date)->sum('taskqty');
                                                    $cumiliquantity = $cumiliquantitytill+$achivied;
                                                    $balance = $totalquantity-$cumiliquantity;
                                                }
                                                 if($exportype=='mobile'){
                                                     $exportarray[] = [ucwords($tasks->heading),$comarchive,$percentage];
                                                 }else{
                                                    $exportarray[] = [ucwords($tasks->heading),'',get_unit_name($projectcostitems->unit),$totalquantity, $cumiliquantitytill,'' ,$achivied, $cumiliquantity, $balance, $comarchive,  $percentage, '', $projectcostitems->description];
                                                 }
                                          }
                                      }
                                    }
                                    $newlevel = (int)$propro->level+1;
                                    $reportarray = array();
                                    $reportarray['projectid'] = $projectid;
                                    $reportarray['subprojectid'] = $subprojectid;
                                    $reportarray['segmentid'] = $segmentid;
                                    $reportarray['contractor'] = $contracid;
                                    $reportarray['level'] = $newlevel;
                                    $reportarray['parent'] = $propro->itemid;
                                    $reportarray['dateformat'] = $dateformat;
                                    $reportarray['startdate'] = $startdate;
                                    $reportarray['enddate'] = $enddate;
                                    $reportarray['exportype'] = $exportype;
                                    $exportarray = array_merge($exportarray, reportexcell($reportarray));
                                }
                                return $exportarray;
                            }
                            $contractorarray = \App\Contractors::get()->pluck('name','user_id')->toArray();
                            $contr = array('0'=>'Company');
                            $contractorarray = $contractorarray+$contr;
                                if(!empty($contractorarray)){
                                    foreach ($contractorarray as $contracid => $contraname){
                                        if($exportype=='mobile'){
                                             $exportarray[] = [ucwords($contraname),'',''];
                                            }else{
                                             $exportarray[] = [ucwords($contraname),'','', '', '','', '', '', '', '', '', ''];
                                             }
                                        $reportarray = array();
                                        $reportarray['projectid'] = $projectid;
                                        $reportarray['subprojectid'] = $subprojectid;
                                        $reportarray['segmentid'] = $segmentid;
                                        $reportarray['contractor'] = $contracid;
                                        $reportarray['level'] = '0';
                                        $reportarray['parent'] = '0';
                                        $reportarray['dateformat'] = $dateformat;
                                        $reportarray['startdate'] = $startdate;
                                        $reportarray['enddate'] = $enddate;
                                        $reportarray['exportype'] = $exportype;
                                        $exportarray = array_merge($exportarray, reportexcell($reportarray));
                                    }
                                }
                            echo json_encode($exportarray);
                            }
                            ?>