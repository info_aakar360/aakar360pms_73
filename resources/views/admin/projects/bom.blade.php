<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Material</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
        {{ csrf_field() }}
        <input type="hidden" name="store_id" value="{{ $sid }}">
        <input type="hidden" name="project_id" value="{{ $pid }}">
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Material</th>
                                <th>Trade</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product) {?>
                            <tr>
                                <th>
                                    <label style="padding: 5px;">
                                        <input type="checkbox" name="product_id[]" value="{{ $product->id }}" class="form-check-input">
                                        <input type="hidden" name="trade_id[]" value="{{ $product->trade_id }}" class="form-check-input">
                                    </label>
                                </th>
                                <th>{{ $product->name }}</th>
                                <th>{{ get_trade_name($product->trade_id) }}</th>
                            </tr>
                        <?php } ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div class="form-actions" style="padding-top: 10px;">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('member.projects.storeBom')}}',
            container: '#createBoqCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('member.projects.storeBom')}}',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>