@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76" ><i class="{{ $pageIcon }}"></i> BOQ</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">BOQ</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>

    <style>
        body{
            background-color: #f4f5fa;
        }
    </style>

@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dragtable.css') }}">

@endpush
@section('content')

    <?php
    if(!empty($project)){ ?>
        <div class="row">
            <div class="col-lg-10">
                <div class="form-group">
                    <h4>Sub projects</h4>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <?php  if(in_array('sub_projects', $user->modules)) {?>
                    <a href="javascript:;" class="btn btn-outline btn-success btn-sm createTitle" style="float: right">Create New @lang('app.subproject')<i class="fa fa-plus" aria-hidden="true"></i></a>
                    {{--<a href="{{ route('admin.projects.projectBoqCreate', [$id]) }}" class="btn btn-outline btn-success btn-sm createTaskCategory" style="float: right; margin-right: 10px;">Create New <i class="fa fa-plus" aria-hidden="true"></i></a>--}}
                    <?php }?>
                </div>
            </div>
        </div>
    <?php }?>
    <?php if(empty($project)){ ?>
    <div class="row"  >
        <?php $x=1;?>
        @foreach($projectlist as $projectli)
            <div class="col-md-4">
                <div class="white-box-no-pad">
                    <div class="row">
                        <!-- .page title -->
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-0">
                            <img class="img-full" src="{{ $projectli->imageurl }}" alt="{{ $projectli->project_name }} Buildings"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            @if($projectli->added_by) @if($projectli->added_by!=$user->id)<label class="badge badge-warning mt-10">Shared</label>@endif @endif
                            <h4 class="page-title"><a @if(in_array('sub_projects', $user->modules)&&sub_project_access($projectli->id)) href="{{ route('admin.projects.boqtitle',$projectli->id) }}"  @else href="{{ route('admin.projects.boqtitle',[$projectli->id,'0']) }}"  @endif>
                                    <i class="{{ $pageIcon }}"></i> {{ $projectli->project_name }}</a> </h4>
                            <h5 class="b-b p-b-10"><i class="icon-clock"></i> {{ date('d M Y',strtotime($projectli->start_date)) }} @if(!empty($projectli->deadline)) - {{ date('d M Y',strtotime($projectli->deadline)) }} @endif</h5>
                        </div>
                    </div>

                </div>
            </div>
            @if($x%3==0)
    </div>
    <div class="row"  >
        @endif
        <?php $x++;?>
        @endforeach
    </div>
    <?php   }else{
    $acc_id = 0; ?>
    @foreach($titles as $title)
        <?php
        $proproget = \App\ProjectCostItemsPosition::where('project_id',$title->project_id)->where('position','row')->where('level','1')->orderBy('inc','asc')->get();

            $tv = DB::table('project_cost_item_final_qty')
            ->select(DB::raw('SUM(total_amount) as total_cost'))
            ->where('title', $title->id)
            ->where('project_id', $title->project_id)
            ->groupBy('title')
            ->first();

            $tq = DB::table('project_cost_items_product')
            ->select(DB::raw('count(finalamount) as finalamount'))
            ->where('title', $title->id)
            ->where('project_id', $title->project_id)
            ->groupBy('title')
            ->first();
        $tn = \App\Title::where('id',$title->title)->first();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pdl10">
                        <!-- .page title -->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                             <img class="img-responsive  project-img" src="@if(!empty($project)) {{ $project->imageurl }} @endif" alt="{{ $title->title }} Buildings"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $title->title }}</h4>
                            <small>Created on: {{ date('d M Y H:i A',strtotime($title->created_at)) }}</small>
                            <h5 class="b-b p-b-10">Total Construction Value : <?php if(isset($tv) && $tv !== ''){ echo $tv->total_cost; } ?></h5>
                            <h5 class="b-b p-b-10">Project type : HIGH RISE</h5>
                            <h5 class="b-b p-b-10">Project Duration : 1 year</h5>
                            <h5 class="b-b p-b-10">Build up area : 10,235 sqFt</h5>
                        </div>
                        <!-- /.page title -->
                        <!-- .breadcrumb -->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <p>Created by: Admin</p>
                            <a href="{{ url('admin/projects/boq/'.$title->project_id.'/'.$title->id) }}" class="btn btn-primary btn-circle "   ><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="javascript:;" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-title-id="{{ $title->id }}" data-original-title="Delete">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.breadcrumb -->
                    </div>

                </div>
            </div>
        </div>

    @endforeach
    <?php }?>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.projects.titleCreate', [$id]) }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <table class="table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('app.sno')</th>
                                    <th>@lang('app.subproject')</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($titles as $key=>$t)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $t->title }}</td>
                                    {{--<td>
                                        <a href="javascript:;" data-cat-id="{{ $t->id }}" class="btn btn-info btn-circle editSubproject"
                                           data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        &nbsp;
                                        <a href="javascript:;" data-cat-id="{{ $t->id }}" class="btn btn-sm btn-danger btn-circle delete-category" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>--}}

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12">
                            @csrf
                            <input type="text" class="form-control" name="title" placeholder="Subproject Name *" required>
                            <input type="hidden" name="project_id" value="{{ $id }}" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" id="taskCategoryModalClose" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="lockModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ url('admin/projects/projectBoqLock') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Lock Tables</span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <div class="col-md-6">
                            <h3>Columns</h3>
                            <?php $rowpostitioncolms = \App\ProjectCostItemsPosition::where('project_id',$id)->where('position','col')->orderBy('inc','asc')->get();
                                foreach($rowpostitioncolms as $rowpostition){
                            ?>
                            <div class="">
                                <label><input type="checkbox" value="1" name="lockcolms[{{ $rowpostition->id }}]" <?php if($rowpostition->collock==1){ echo 'checked';}?> > {{ $rowpostition->itemname }}</label>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-success">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        });

        $('#taskCategoryModalClose').click(function () {
            $('#taskCategoryModal').hide();
        });
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');
    </script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/boq') }}'+'/'+projectId;

        });
        function textAreaAdjust(element) {
            element.style.height = "1px";
            element.style.height = (25+element.scrollHeight)+"px";
        }
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('title-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.title-destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this PO?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this PO?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('member.purchase-order.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                  swal("Updated!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

        });

        function loadTable() {
            table = $('#users-table').resizableColumns({
                responsive: true,
                processing: true,
                destroy: true,
                stateSave: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                }
            });
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var rfq = $('#rfq').val();
            var status = $('#status').val();

            var url = '{{ route('member.rfq.export', [':status', ':rfq']) }}';
            url = url.replace(':rfq', rfq);
            url = url.replace(':status', status);

            window.location.href = url;
        }
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });


    </script>
@endpush