@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    @include('admin.projects.show_project_menu')
                </div>
            </section>
        </div>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form method="post" id="productCostItem">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="project_id" value="{{ $id }}"/>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control" placeholder="Title">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <select name="create_from[]" class="selectpicker form-control" onchange="getProducts(this)" id="create_from" multiple>
                                        <option value="">Please select BOQ</option>
                                        @foreach($costitems as $category)
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select name="category[]" class="selectpicker form-control" id="category" multiple>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select name="type[]" class="selectpicker form-control" id="type" multiple>
                                        <option value="">Please select type</option>
                                        <option value="Workforce">Workforce</option>
                                        <option value="Equipment">Equipment</option>
                                        <option value="Material">Material</option>
                                        <option value="Commitment">Commitment</option>
                                        <option value="Owner Cost">Owner Cost</option>
                                        <option value="Professional Services">Professional Services</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <input type="button" name="filter" class="btn btn-success" value="Filter" onclick="filterData()">
                                </div>
                            </div>
                    </div>
                </form>

            </div>

            <div class="white-box">
                <form method="post" id="proData">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12" id="productData"></div>
                        <div class="col-lg-12">&nbsp;</div>
                        <div class="col-lg-12">
                            <input type="submit" name="submit" class="btn btn-success" value="Save" id="saveProduct" style="float: right;">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(document).on('click', '#saveProduct', function(){
            $.easyAjax({
                url: '{{ route('admin.projects.storeSourcingPackages',[$id]) }}',
                container: '#proData',
                type: "POST",
                data: $('#proData').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });

        function getProducts(val) {
            var formData = $('form#productCostItem').serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getProducts') }}",
                data: formData,
                success: function(data){
//                    $("#productData").html(data.products);
                    $("#category").html(data);
                    $('#category').selectpicker('refresh');
                }
            });
        }

        function filterData(val) {
            var formData = $('form#productCostItem').serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.filterData') }}",
                data: formData,
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function getProduct(val) {
            var token = "{{ csrf_token() }}";
            var id = "{{ $id }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getProduct') }}",
                data: {'_token': token, 'cost_item_id': val, 'id': id},
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Sourcing').addClass('tab-current');

        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#rate'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#cost'+id).val(total);
        }

    </script>
@endpush