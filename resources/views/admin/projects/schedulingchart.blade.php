@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> @lang('app.menu.schedule')</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.menu.schedule')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
     <link href="//cdn.dhtmlx.com/gantt/edge/skins/dhtmlxgantt_broadway.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/colorpicker/colorpicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .gantt_project {
            height: 10px !important;
        }
        .custom-project {
            position: absolute;
            height: 6px;
            color: #ffffff;
            background-color: #444444;
        }
        .custom-project div {
            position: absolute;
        }

        .project-left, .project-right {
            top: 6px;
            background-color: transparent;
            border-style: solid;
            width: 0px;
            height: 0px;
        }

        .project-left {
            left: 0px;
            border-width: 0px 0px 8px 7px;
            border-top-color: transparent;
            border-right-color: transparent !important;
            border-bottom-color: transparent !important;
            border-left-color: #444444 !important;
        }

        .project-right {
            right: 0px;
            border-width: 0px 7px 8px 0px;
            border-top-color: transparent;
            border-right-color: #444444;
            border-bottom-color: transparent !important;
            border-left-color: transparent;
        }

        .project-line {
            font-weight: bold;
        }
        .gantt_task_drag {
            width: 6px;
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAACCAYAAAB7Xa1eAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYDDjkw3UJvAwAAABRJREFUCNdj/P//PwM2wASl/6PTAKrrBf4+lD8LAAAAAElFTkSuQmCC);
            z-index: 1;
            top: 0;
        }

        .gantt_task_drag.task_left{
            left: 0;
        }

        .gantt_task_drag.task_right{
            right: 0;
        }
        .gantt_message_area.dhtmlx_message_area {
            display: none;
        }
        .gantt_task_content{
            padding-left: 50px !important;
        }
        .gantt_milestone .gantt_task_content{
            color:#333;
            padding-left: 0px !important;
        }
        .resource_marker{
            text-align: center;
        }
        .gantt_grid_scale {
            background: #fdfdfd;
            color: #555;
        }
        .gantt_grid_scale .gantt_grid_head_cell {
            border: 0 !important;
        }

        .gantt_task > .gantt_scale_line > .gantt_scale_cell {
            border-bottom: 1px solid #cbcbcb;
        }
        .gantt_task .gantt_task_scale .gantt_scale_cell {
            border-left: 0 !important;
            border-top: 0 !important;
            border-bottom: 0 !important;
            border-right: 1px solid #cbcbcb;
            color: #666;
            font-size: 14px;
            background-color: #fdfdfd;
        }
        .gantt_task .gantt_task_scale .gantt_scale_cell.bdrbtm {
            border-bottom: 1px solid #cbcbcb !important;
        }
        .gantt_side_content.gantt_left{
            padding: 0px;
        }
        .gantt_task_cell{
            -webkit-border-right: 1px !important;
            -moz-border-right: 1px !important;
            border-right: 1px solid #cbcbcb;
        }
        .gantt_row, .gantt_task_row{
            border-bottom: 0px !important;
        }
        .resource_marker div{
            width: 28px;
            height: 28px;
            line-height: 29px;
            display: inline-block;
            border-radius: 15px;
            color: #FFF;
            margin: 3px;
        }
        .resource_marker.workday_ok div {
            background: #51c185;
        }

        .resource_marker.workday_over div{
            background: #ff8686;
        }

        .baseline {
            position: absolute;
            border-radius: 2px;
            opacity: 0.6;
            margin-top: 13px;
        }
        .status_line {
            background-color: #0ca30a;
        }
        .sidebarbox{
            background-color: #fff;
            padding: 10px;
            display: inline-block;
            width: 100%;
            height: 100vh;
        }
        /* Add padding and border to inner content
        for better animation effect */
        .w-100{
            width: 100%;
        }
        .smalltext{
            font-size: 10px;
            text-align: right;
        }
        .float-left{
            float: left;
        }
        .float-right{
            float: right;
        }
        .taskusername {
            font-size: 15px;
            font-weight: bold;
            display: inline-block;
        }
        .tasksdate {
            font-size: 10px;
            float: right;
        }
        .timelineblock {
            background-color: #efefef;
            padding: 10px;
            border-radius: 10px;
            margin-bottom: 10px;
        }
        .timeline-list {
            height: 45%;
            overflow: auto;
            display: inline-block;
        }
        .replyicon {
            padding: 5px;
            font-size: 20px;
        }
        .gantt_task_progress {
            text-align: left;
            padding-left: 10px;
            box-sizing: border-box;
            color: white;
            font-weight: bold;
            border-right: 0px !important;
            box-shadow: inset 0px 0 0 0px;
        }
        .gantt_task_line.gantt_project .gantt_task_progress{
            box-shadow: inset 0px 0 0 0px;
        }

        .gantt_task_line.gantt_task_inline_color .gantt_task_progress {
            opacity: 0.4;
            padding-left: 0px;
        }
        .gantt_tree_content img {
            width: 20px;
        }
        .gantt_scale_line{
            border-top: 0px !important;
        }
        .gantt_cell {
            border-right: 1px solid #cbcbcb !important;
            border-bottom: 1px solid #cbcbcb !important;
        }
        .gantt_row_project {
            background: #e3e3e3 !important;
        }
        .bcklightblue{
            background-color: #e3edf0 !important;
        }
        .gantt_task_cell.week_end {
            background-color: #EFF5FD;
        }

        .gantt_task_row.gantt_selected .gantt_task_cell.week_end {
            background-color: #F8EC9C;
        }
        /* high */
        .gantt_task_line.darkred {
            background-color: #2A2A2A;
        }
        .gantt_task_line.darkred .gantt_task_content {
            color: #fff;
        }
        .gantt_task_line.lightgreen {
            background-color: #5EF561;
        }
        .gantt_task_line.lightgreen .gantt_task_content {
            color: #fff;
        }
        .gantt_task_line.blue  {
            background-color: #1D7448;
        }
        .gantt_task_line.blue .gantt_task_content {
            color: #fff;
        }
        .gantt_task_line.orrange   {
            background-color: #FAEE5A;
        }
        .gantt_task_line.blue .gantt_task_content {
            color: #fff;
        }
        .gantt_task_line.grey   {
            background-color: #80807F;
        }
        .gantt_task_line.grey .gantt_task_content {
            color: #fff;
        }
    </style>
@endpush

@section('content')
    <div class="white-box-height">
        <div class="row">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <strong>Scheduling chart</strong>
                    </div>
                    <div class="col-md-6 text-right">
                        <select id="period" class="form-control-normal" name="period"  onchange='exportTo()'>
                            <option value="">Select</option>
                            <option value="1" >1 day</option>
                            <option value="2"  >2 day</option>
                            <option value="7"  >7 day</option>
                            <option value="15" >15 day</option>
                            <option value="30" >30 day</option>
                        </select>
                        <a href="{{ route('admin.projects.setresource',[$ganttProjectId,$ganttTitle]) }}" title="Resources" data-toggle="tooltip"  class="btn btn-primary"  ><i class="fa fa-users"></i> Resource</a>
                        <a href="javascript:void(0);"  data-toggle="modal" data-target="#SetbaselineModal" title="Set Baseline" data-toggle="tooltip"   class="btn btn-primary"><i class="fa fa-lines"></i> Set Baseline</a>
                        <a href="javascript:void(0);"   data-toggle="modal" data-target="#addNewModal" title="Add New" data-toggle="tooltip"   class="btn btn-primary"><i class="fa fa-plus"></i> Add new</a>
                        <a href="javascript:void(0);" onclick="loadData();"  title="Refresh" data-toggle="tooltip"   class="btn btn-primary" ><i class="fa fa-refresh"></i></a>
                    </div>
                </div>
            </div>
        </div>
        {{--<h2 style="color: #002f76">@lang('modules.projects.viewGanttChart')</h2>--}}
        <div class="row">
            <div class="gnattchart float-left" style='width:100%;height:100%;' >
                <div id="gantt_here" style='width:100%;height:100%;'></div>
            </div>
            <div class="taskview float-left" style="width: 30%;display: none;">

            </div>
        </div>
    </div>
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="" id="costitemupdate">
            @csrf
            <div class="modal-dialog modal-lg" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="eventDetailModalClose"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        <div class="costupdateerror" style="color: red;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                    <select class="select2  selectuser" name="assign_to[]" id="assignto"  multiple>
                                        <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.startDate')</label>
                                    <input type="text" name="start_date"  id="start_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.dueDate')</label>
                                    <input type="text" name="deadline"  id="due_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Dependency</label>
                                    <select class="select2 form-control" data-placeholder="Choose Dependent Task" name="target" id="target" >
                                        <option value="">Select Task</option>
                                        @foreach($projectcostitems as $allTask)
                                            <optgroup label="{{ $allTask['name'] }}" ><strong>{{ $allTask['name'] }}</strong>
                                                <?php $tasklist = $allTask['tasklist'];
                                                if(!empty($tasklist)){
                                                foreach ($tasklist as $tasks){
                                                if(!empty($tasks['pid'])){ ?>
                                                <option value="{{ $tasks['pid'] }}">{{ $tasks['taskname'] }}</option>
                                                <?php   } }  } ?>
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Predecessors</label>
                                    <select class="form-control" data-placeholder="Choose Type" name="linktype" id="linktype" >
                                        <option value="">Select</option>
                                        <option value="0">Finish to Start (FS)</option>
                                        <option value="1">Start to Start (SS)</option>
                                        <option value="2">Finish to Finish (FF)</option>
                                        <option value="3">Start to Finish (SF)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Set as Milestone</label>
                                    <input type="checkbox" name="milestone" id="milestone1" value="1">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="project_cost_id" id="projectCostId">
                        <!--/span-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>

    {{--Ajax Modal Ends--}}

    {{-- <div class="modal fade bs-modal-md in"  id="baselineModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-md" id="modal-data-application">
             <div class="modal-content">
                 <form method="post" action="" id="baselineupdate">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                     <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Set Baselink</span>
                 </div>
                 <div class="modal-body">
                     <div class="form-group">
                     <div class="input-group">
                         <input type="text" class="form-control" name="baselinedate"  id="baselinedate"  value="{{ !empty($baselinedate) ? date('d-m-Y',strtotime($baselinedate)) : '' }}" />
                         <a id="cleardate" href="javascript:" class="input-group-addon">Clear</a>
                     </div>
                     </div>
                     <div class="form-group">
                         <input type="text" class="form-control" name="baselinelabel" placeholder="Base line label" value="{{ $baselinelabel }}" />
                     </div>
                 </div>
                 <div class="modal-footer">
                     {{ csrf_field() }}
                     <input type="hidden" name="projectid" value="{{ $ganttProjectId }}" />
                     <input type="hidden" name="titleid" value="{{ $ganttTitle }}" />
                     <button type="button" class="btn default" data-dismiss="modal">Close</button>
                     <button type="submit"  class="btn btn-success">Save changes</button>
                 </div>
                 </form>
             </div>
             <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->.
     </div>--}}

    <div class="modal fade bs-modal-md in"  id="SetbaselineModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" autocomplete="off" action="" id="setbaselineupdate">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Set Baseline</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <table class="table table-bordered">
                                <tr>
                                    <th>@lang('app.sno')</th>
                                    <th>@lang('app.lable')</th>
                                    <th>@lang('app.show')</th>
                                    <th>@lang('app.date')</th>
                                    <th>@lang('app.action')</th>
                                </tr>
                                <tbody>
                                <?php if(count($baselinearray)>0){
                                $x=1; foreach ($baselinearray as $baselines){ ?>
                                <tr id="baselinerow{{ $baselines->id }}">
                                    <td>{{ $x }}</td>
                                    <td>{{ $baselines->label }}</td>
                                    <td><input type="checkbox" value="{{ $baselines->id }}" class="baselinestatus"  @if($baselines->status=='1') checked @endif /></td>
                                    <td>{{ $baselines->created_at }}</td>
                                    <td><a href="javascript:;" onclick="removebaseline({{ $baselines->id }});" class="btn1 btn-circle btn-primary"><i class="fa fa-times"></i></a></td>
                                </tr>
                                <?php  $x++;  }   }?>
                                </tbody>
                            </table>
                            <a href="" class="btn btn-primary">Update</a>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" required name="baselinelabel" placeholder="Base line label" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" required name="baselinecolor" id="pickcolor" placeholder="Base line color"  />
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input type="hidden" name="projectid" value="{{ $ganttProjectId }}" />
                        <input type="hidden" name="titleid" value="{{ $ganttTitle }}" />
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in"  id="addNewModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" id="modal-data-application">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="scheduleviewblock">
                            <a href="javascript:void(0);" onclick="addtaskrow();" ><i class="fa fa-plus"></i> <br/> Add Task</a>
                        </div>
                        <div class="scheduleviewblock">
                            <a href="javascript:void(0);" onclick="addactivityrow();" ><i class="fa fa-plus"></i> <br/> Add Activity</a>
                        </div>
                        <div class="scheduleviewblock">
                            <a href="javascript:void(0);" onclick="addmilestonerow();" ><i class="fa fa-plus"></i> <br/> Add Milestone</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="createTaskModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="" id="costitemcreate"  autocomplete="off">
            @csrf
            <div class="modal-dialog modal-lg" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="eventDetailModalClose"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Add Task</span>
                    </div>
                    <div class="modal-body">
                        <div class="costupdateerror" style="color: red;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.task')</label>
                                    <input  class="form-control" name="taskname" list="costitemdata" >
                                    <datalist class="costitemslist"  id="costitemdata">
                                        <?php foreach($costitemslist as $costitem){ ?>
                                        <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                        <?php }?>
                                    </datalist>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.startDate')</label>
                                    <input type="text" name="start_date"  id="start_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.dueDate')</label>
                                    <input type="text" name="deadline"  id="due_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Dependency</label>
                                    <select class="select2 form-control" data-placeholder="Choose Dependent Task" name="target" id="target" >
                                        <option value="">Select Task</option>
                                        @foreach($projectcostitems as $allTask)
                                            <optgroup label="{{ $allTask['name'] }}" ><strong>{{ $allTask['name'] }}</strong>
                                                <?php $tasklist = $allTask['tasklist'];
                                                if(!empty($tasklist)){
                                                foreach ($tasklist as $tasks){
                                                if(!empty($tasks['pid'])){ ?>
                                                <option value="{{ $tasks['pid'] }}">{{ $tasks['taskname'] }}</option>
                                                <?php   } }  } ?>
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Predecessors</label>
                                    <select class="form-control" data-placeholder="Choose Type" name="linktype" id="linktype" >
                                        <option value="">Select</option>
                                        <option value="0">Finish to Start (FS)</option>
                                        <option value="1">Start to Start (SS)</option>
                                        <option value="2">Finish to Finish (FF)</option>
                                        <option value="3">Start to Finish (SF)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="prevtaskid" class="prevtaskid">
                        <input type="hidden" name="project_id"  value="{{ $ganttProjectId }}">
                        <input type="hidden" name="subproject_id"  value="{{ $ganttTitle }}">
                        <!--/span-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="createMilestoneModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="" id="costitemmilestonecreate"  autocomplete="off">
            @csrf
            <div class="modal-dialog modal-lg" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="eventDetailModalClose"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Add Milestone</span>
                    </div>
                    <div class="modal-body">
                        <div class="costupdateerror" style="color: red;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.task')</label>
                                    <input  class="form-control" name="taskname" list="costitemdata" >
                                    <datalist class="costitemslist"  id="costitemdata">
                                        <?php foreach($costitemslist as $costitem){ ?>
                                        <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                        <?php }?>
                                    </datalist>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.startDate')</label>
                                    <input type="text" name="start_date"  id="start_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.dueDate')</label>
                                    <input type="text" name="deadline"  id="due_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Dependency</label>
                                    <select class="select2 form-control" data-placeholder="Choose Dependent Task" name="target" id="target" >
                                        <option value="">Select Task</option>
                                        @foreach($projectcostitems as $allTask)
                                            <optgroup label="{{ $allTask['name'] }}" ><strong>{{ $allTask['name'] }}</strong>
                                                <?php $tasklist = $allTask['tasklist'];
                                                if(!empty($tasklist)){
                                                foreach ($tasklist as $tasks){
                                                if(!empty($tasks['pid'])){ ?>
                                                <option value="{{ $tasks['pid'] }}">{{ $tasks['taskname'] }}</option>
                                                <?php   } }  } ?>
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Predecessors</label>
                                    <select class="form-control" data-placeholder="Choose Type" name="linktype" id="linktype" >
                                        <option value="">Select</option>
                                        <option value="0">Finish to Start (FS)</option>
                                        <option value="1">Start to Start (SS)</option>
                                        <option value="2">Finish to Finish (FF)</option>
                                        <option value="3">Start to Finish (SF)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Set as Milestone</label>
                                    <input type="checkbox" name="milestone" checked id="milestone1" value="1">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="prevtaskid" class="prevtaskid">
                        <input type="hidden" name="project_id"  value="{{ $ganttProjectId }}">
                        <input type="hidden" name="subproject_id"  value="{{ $ganttTitle }}">
                        <!--/span-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md in" id="createActivityModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form method="post" action="" id="activitycreate"  autocomplete="off">
            @csrf
            <div class="modal-dialog modal-lg" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="eventDetailModalClose"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Create Activity</span>
                    </div>
                    <div class="modal-body">
                        <div class="costupdateerror" style="color: red;"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.activity')</label>
                                    <input  class="form-control" name="activity" list="activitydata" >
                                    <datalist class="costitemslist"  id="activitydata">
                                        <?php foreach($activityarray as $activity){ ?>
                                        <option data-value="{{ $activity->id }}" >{{ $activity->title }}</option>
                                        <?php }?>
                                    </datalist>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="prevtaskid" class="prevtaskid">
                        <input type="hidden" name="project_id"  value="{{ $ganttProjectId }}">
                        <input type="hidden" name="subproject_id"  value="{{ $ganttTitle }}">
                        <!--/span-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
    {{--<script src="//cdn.dhtmlx.com/gantt/edge/dhtmlxgantt.js"></script>
   <script src="//cdn.dhtmlx.com/gantt/edge/locale/locale_{{ $global->locale }}.js"></script>--}}
    <script src="https://docs.dhtmlx.com/gantt/codebase/dhtmlxgantt.js?v=7.1.3"></script>
  {{--  <script src="https://docs.dhtmlx.com/gantt/codebase/dhtmlxgantt.js?v=7.0.10"></script>--}}
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/colorpicker/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script type="text/javascript">
        $('#pickcolor').colorpicker();
        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/scheduling') }}'+'/'+projectId;
        });
        $('.closeButton').click(function(){
            $('#eventDetailModal').hide();
        });
        gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";

        gantt.templates.task_class = function (st, end, item) {
            if(item.linktype=='category'){
                return "gantt_project";
            }
            if (item.planned_end) {
                var classes = ['has-baseline'];
                if (end.getTime() > item.planned_end.getTime()) {
                    classes.push('overdue');
                }
                return classes.join(' ');
            }
            if(item.color){
                return item.color;
            }
        };

        gantt.config.autofit = true;
        gantt.config.grid_width = 700;
        gantt.config.scale_height = 60;
        gantt.config.grid_resize = true;
        gantt.config.scale_unit = "month";
        gantt.config.date_scale = "%F, %Y";
        gantt.config.autoscroll = false;
        gantt.config.autosize = "y";
        gantt.config.min_column_width = 40;
        gantt.config.work_time = true;
        gantt.config.row_height = 40;
        gantt.config.bar_height = 25;
        gantt.config.server_utc = false;
        gantt.config.fit_tasks = false;
        gantt.config.show_unscheduled = true;
        gantt.config.placeholder_task = true;
        gantt.config.auto_types = true;
        gantt.config.show_task_cells = true;

        gantt.config.auto_scheduling = true;
        gantt.config.auto_scheduling_strict = true;
        gantt.config.auto_scheduling_compatibility = true;


        gantt.plugins({         marker: true, auto_scheduling: true     });


        gantt.templates.timeline_cell_class = function (task, date) {
            var holidaysarrray = [<?php echo '"'.implode('","',$holidaysarray).'"' ?>];
            var convdate = date.getFullYear()+'-';
            var mnth = date.getMonth()+1;
            if(mnth<10){
                convdate += '0'+mnth+'-';
            }else{
                convdate += date.mnth+'-';
            }
            if(date.getDate()<10){
                convdate += '0'+date.getDate();
            }else{
                convdate += date.getDate();
            }
            var weeksarray = [<?php echo '"'.implode('","',$weeksarray).'"' ?>];
            var daynum = Math.floor(((date.getDate()-1) / 7)+1);
            var dayname = date.getDay();
            if(dayname==0){
                dayname = 7;
            }
            var curday = daynum+''+dayname;

            if(jQuery.inArray( convdate, holidaysarrray )!='-1'||jQuery.inArray( curday, weeksarray )!='-1'){
                return "week_end";
            }else{
                return "";
            }
        };
        gantt.config.subscales = [
            {unit:"day", step:1, date:"%j",css: function(date) {
                return "bcklightblue";
            }},
            {unit:"day", step:1, date:"%D",css: function(date) {
                return "bcklightblue";
            } }
        ];
        var exportTo = function() {
            var output = document.getElementById("period").value;
            switch (output) {
                case "1":
                    gantt.config.subscales = [
                        {unit: "day", step: 1, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "2":
                    gantt.config.subscales = [
                        {unit: "day", step: 2, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "7":
                    gantt.config.subscales = [
                        {unit: "day", step: 7, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "15":
                    gantt.config.subscales = [
                        {unit: "day", step: 7, date: "%j, %D"}
                    ];
                    loadData();
                    break;
                case "30":
                    gantt.config.subscales = [
                        {unit: "day", step: 30, date: "%j, %D"}
                    ];
                    loadData();
                    break;
            }
        };

        // adding baseline display
        <?php if(count($baselinearray)>0){
        $x=1;    foreach ($baselinearray as $baseline) {
        if($baseline->status=='1'){
        ?>
        {{-- gantt.config.task_height = 15;--}}
        gantt.addTaskLayer({
            renderer: {
                render: function draw_planned(task) {
                    var pstart = task.planned_start{{ $x }}, pend = task.planned_end{{ $x }};
                    var basecount = '{{ count($baselinearray) }}';
                    var c = {{ $x*7 }};
                    if (pstart && pend) {
                        var sizes = gantt.getTaskPosition(task, pstart, pend);
                        var el = document.createElement('div');
                        var tops =   c+sizes.top;
                        el.className = 'baseline';
                        el.style.left = sizes.left + 'px';
                        el.style.width = sizes.width + 'px';
                        el.style.height = '2px';
                        el.style.background =  '{{ $baseline->color }}';
                        el.style.top =  tops+ 'px';
                        return el;
                    }
                    return false;
                },
                // define getRectangle in order to hook layer with the smart rendering
                getRectangle: function(task, view){
                    var pstart = task.planned_start{{ $x }};
                    var pend = task.planned_end{{ $x }};
                    if (pstart && pend) {
                        return gantt.getTaskPosition(task, pstart, pend);
                    }
                    return null;
                }
            }
        });
        gantt.attachEvent("onTaskLoading", function (task) {

            task.planned_start{{ $x }} = gantt.date.parseDate(task.planned_start{{ $x }}, "xml_date");
            task.planned_end{{ $x }} = gantt.date.parseDate(task.planned_end{{ $x }}, "xml_date");
            return true;
        });

        <?php $x++; }}}?>
            gantt.templates.rightside_text = function (start, end, task) {
            if (task.planned_end) {
                if (end.getTime() > task.planned_end.getTime()) {
                    var overdue = Math.ceil(Math.abs((end.getTime() - task.planned_end.getTime()) / (24 * 60 * 60 * 1000)));
                    var text = "<b>Overdue: " + overdue + " days</b>";
                    return text;
                }
            }
        };
        gantt.templates.leftside_text = function (start, end, task) {
            if (task.statusimage) {
                if(task.type==''){
                    var text = task.statusimage;
                    return text;
                }
            }
        };
        gantt.config.layout = {
            css: "gantt_container",
            cols: [
                {
                    width:400,
                    height:300,
                    min_width: 300,
                    rows:[
                        {view: "grid", scrollX: "gridScroll", scrollable: true, scrollY: "scrollVer"},
                        {view: "scrollbar", id: "gridScroll", group:"horizontal"}
                    ]
                },
                {resizer: true, width: 1},
                {
                    rows:[
                        {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
                        {view: "scrollbar", id: "scrollHor", group:"horizontal"}
                    ]
                },
                {view: "scrollbar", id: "scrollVer"}
            ],
            rows:[
                {
                    cols: [
                        {view: "grid", id: "grid", scrollX:"scrollHor", scrollY:"scrollVer"},
                        {resizer: true, width: 1},
                        {view: "timeline", id: "timeline", scrollX:"scrollHor", scrollY:"scrollVer"},
                        {view: "scrollbar", scroll: "y", id:"scrollVer"},
                    ]
                },
                {view: "scrollbar", scroll: "x", id:"scrollHor", height:20}
            ],
        };

        gantt.templates.progress_text = function (start, end, task) {
            return "<span class='progresspercent'>&nbsp;&nbsp;" + Math.round(task.progress*100) + "% </span>";
        };

        var textEditor = {type: "text", map_to: "text"};
        var startdateEditor = {type: "date", map_to: "start_date"};
        var enddateEditor = {type: "date", map_to: "deadline"};
        var durationEditor = {type: "number", map_to: "duration"};
        var caldaysEditor = {type: "number", map_to: "caldays"};
        var qtyEditor = {type: "number", map_to: "qty"};
        var predecessorsEditor = {type: "text", map_to: "predecessors"};

        // default columns definition
        gantt.config.columns=[
            {name: "sno", label: "Sno", width: '20', resize: true,align: "center"},
            {name:"text", label:"Task",  tree:true, width: '800', resize: true, editor: textEditor },
            {name:"start_date", label:"Start Date", align: "center", width: '200', resize: true, editor: startdateEditor },
            {name:"deadline", label:"Due Date", align: "center",width:"200", resize: true, editor: enddateEditor},
            {name:"duration",   label:"Duration",   align: "center",width:"100",resize: true, editor: durationEditor},
            {name:"caldays",   label:"Cal days",   align: "center",width:"100",resize: true, editor: caldaysEditor},
            {name:"qty",   label:"Qty",   align: "center",width: "100",resize: true, editor: qtyEditor},
            {name: "predecessors", label: "Predecessors", align: "center", width: "100", editor: predecessorsEditor, resize: true},

                <?php if(!empty($setresource)){?>
            {name: "owner", align: "center", width: "*", label: "Owner", resize: true,
                template: function (item) {
                    var store = gantt.getDatastore("resource");
                    var ownerValue = item.users;
                    var singleResult = "";
                    var result = "";
                    if (ownerValue) {
                        if (!(ownerValue instanceof Array)) {
                            ownerValue = [ownerValue];
                        }
                        ownerValue.forEach(function(ownerId) {
                            var owner = store.getItem(ownerId);
                            if (!owner)	{
                                return;
                            }
                            result += "<div class='owner-label' title='" + owner.text + "'>" + owner.text + "</div>";
                        });
                    }

                    return result;
                }
            },
                <?php }?>

            {name:"action",    label:"Action",   width: "*",resize: true, align: "center", template: function (item) {
                if(item.linktype=='project'){
                    var url = '{{ url('/') }}';
                    var viewtask = '<a href="javascript:void(0);"  onclick=viewTask("'+item.id+'");><img src="'+url+'/img/message-icon.png"></a>';
                    viewtask += ' ';
                    viewtask += '<a href="javascript:void(0);"  onclick=addTask("'+item.id+'");><img src="'+url+'/img/edit-icon.png"></a>';
                    return viewtask;
                }

            }}
        ];

        gantt.ext.inlineEditors.attachEvent("onSave", function (task) {

            task._token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route("admin.projects.editGnattTask")}}',
                type: "POST",
                data: task,
                success: function (response) {
                    if(response){
                        alert(response);
                    }else{
                        loadData();
                    }
                }
            })
        });

        <?php if(!empty($setresource)){?>
            gantt.config.resource_attribute = "data-resource-id";

        var resourceConfig = {
            columns: [
                {
                    name: "name", label: "Name", align: "center", template: function (resource) {
                    return resource.text;
                }
                },
                {
                    name: "Works", label: "Works", align: "center", template: function (resource) {
                    return resource.work;
                }
                }
            ]
        };
        gantt.templates.resource_cell_class = function(start_date, end_date, resource, tasks){
            var css = [];
            css.push("resource_marker");
            if (resource.work <= 1) {
                css.push("workday_ok");
            } else {
                css.push("workday_over");
            }
            return css.join(" ");
        };

        gantt.templates.resource_cell_value = function(start_date, end_date, resource, tasks){
            return "<div>" + tasks.length + "</div>";
        };
        gantt.locale.labels.section_resources = "Works";
        gantt.config.lightbox.sections = [
            {name: "description", height: 38, map_to: "text", type: "textarea", focus: true},
            {
                name: "resources", type: "resources", map_to: "work", options: gantt.serverList("people"), default_value:1
            },
            {name: "time", type: "duration", map_to: "auto"}
        ];


        gantt.config.resource_store = "resource";
        gantt.config.resource_property = "users";
        gantt.config.order_branch = true;
        gantt.config.open_tree_initially = true;
        gantt.config.layout = {
            css: "gantt_container",
            rows: [
                {
                    cols: [
                        {view: "grid", group:"grids", scrollY: "scrollVer"},
                        {resizer: true, width: 1},
                        {view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer"},
                        {view: "scrollbar", id: "scrollVer", group:"vertical"}
                    ],
                    gravity:2
                },
                {resizer: true, width: 1},
                {
                    config: resourceConfig,
                    cols: [
                        {view: "resourceGrid", group:"grids", width: 435, scrollY: "resourceVScroll" },
                        {resizer: true, width: 1},
                        {view: "resourceTimeline", scrollX: "scrollHor", scrollY: "resourceVScroll"},
                        {view: "scrollbar", id: "resourceVScroll", group:"vertical"}
                    ],
                    gravity:1
                },
                {view: "scrollbar", id: "scrollHor"}
            ]
        };

        var resourcesStore = gantt.createDatastore({
            name: gantt.config.resource_store
        });

            <?php }?>

      {{--  var holidays = [//USA, DC holidays
                new Date(2021, 6, 1),
                new Date(2021, 6, 21),
                new Date(2021, 6, 16),
                new Date(2021, 6, 12),
                new Date(2021, 6, 27)
            ];

        for (var i = 0; i < holidays.length; i++) {
            gantt.setWorkTime({
                date: holidays[i],
                hours: false
            });
        }--}}

        /*  resourcesStore.attachEvent("onParse", function(){
              var people = [];
              resourcesStore.eachItem(function(res){
                  if(!resourcesStore.hasChild(res.id)){
                      var copy = gantt.copy(res);
                      copy.key = res.id;
                      copy.label = res.text;
                      people.push(copy);
                  }
              });
              gantt.updateCollection("people", people);
          });*/
        /*  resourcesStore.parse([
              {id: 1, text: "Aluminium", parent:null},
              {id: 2, text: "Glass", parent:null},
              {id: 3, text: "Plywood", parent:null},
              {id: 4, text: "Aluminium frame", parent:null},
              {id: 5, text: "Unassigned", parent:4},
              {id: 6, text: "John", parent:1},
              {id: 7, text: "Mike", parent:2},
              {id: 8, text: "Anna", parent:2},
              {id: 9, text: "Bill", parent:3},
              {id: 10, text: "Floe", parent:3}
          ]);*/

        gantt.config.type_renderers[gantt.config.types.project] = function(task){
                if ( task.linktype == 'category') {
                var main_el = document.createElement("div");
                var size = gantt.getTaskPosition(task);
                main_el.innerHTML = [
                    "<div class='project-left'></div>",
                    "<div class='project-right'></div>"
                ].join('');
                main_el.className = "custom-project";
                main_el.style.left = size.left + "px";
                main_el.style.top = size.top + 7 + "px";
                main_el.style.width = size.width + "px";
                return main_el;
            }
        };
        gantt.templates.grid_row_class = function (start, end, task) {
            if (task.linktype == 'category') {
                return "project-line";
            }
        };
        gantt.attachEvent("onTaskCreated", function(task){
            return false;
        });

        gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e){
            return false;
        });
        gantt.attachEvent("onAfterTaskDrag", function(id, mode, e){
            return false;
        });

        gantt.init("gantt_here");

        @if($ganttProjectId == '')
        gantt.load('{{ route("admin.projects.schedulingData") }}');
        @else
            gantt.config.open_tree_initially = true;

        gantt.load('{{ url("admin/projects/schedulingData/".$ganttProjectId."/".$ganttTitle) }}');
        @endif

        function viewTask(id) {
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route("admin.projects.viewTask")}}',
                type: "POST",
                data: { '_token': token, 'id': id},
                success: function (response) {
                    $(".gnattchart").css('width','70%');
                    gantt.config.autosize = '70%';
                    gantt.init("gantt_here");
                    $(".taskview").css('width','30%');
                    $(".taskview").show();
                    $(".taskview").html("");
                    $(".taskview").html(response);
                    $(".replycostid").val(id);
                    myDropzone = new Dropzone("div#file-upload-dropzone", {
                        url: "{{ route('admin.task-files.store') }}",
                        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                        paramName: "file",
                        maxFilesize: 10,
                        maxFiles: 10,
                        acceptedFiles: "image/*,application/pdf",
                        autoProcessQueue: false,
                        uploadMultiple: true,
                        addRemoveLinks:true,
                        parallelUploads:10,
                        init: function () {
                            myDropzone = this;
                        }
                    });
                    myDropzone.on('sending', function(file, xhr, formData) {
                        var ids = $("#taskID").val();
                        var percentageID = $('#percentageID').val();
                        formData.append('task_id', ids);
                        formData.append('percentage_id', percentageID);
                    });
                    myDropzone.on('completemultiple', function () {
                        var msgs = "@lang('messages.taskUpdatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                    });

                }
            })
        }
        function closeTaskview() {
            $(".gnattchart").css('width','100%');
            gantt.config.autosize = '100%';
            gantt.init("gantt_here");
            $(".taskview").hide();
        }
        function addTask(id) {
            var token = '{{ csrf_token() }}';
            $.easyAjax({
                url: '{{route("admin.projects.costData")}}',
                type: "POST",
                data: { '_token': token, 'id': id},
                success: function (response) {
                    if (response.id) {
                        $("#assignto").select2('destroy');
                        $("#target").select2('destroy');
                        $('#modelHeading').html('Update Target');
                        $('#eventDetailModal').show();
                        $('#projectCostId').val(response.id);
                        $('#start_date2').val(response.start_date);
                        $('#due_date2').val(response.deadline);
                        $('#linktype').val(response.type);
                        $('#target').val(response.target);
                        $("#assignto").trigger('change');
                        $("#assignto").select2();
                        if(response.milestone>1){
                            $("#milestone1").prop('checked');
                        }
                        if(response.assign_to){
                            var assing = response.assign_to.split(',');
                            $("#assignto").val(assing);
                            $("#assignto").trigger('change');
                        }
                    }
                }
            })
        }

        //    update task
        function storeTask() {
            $.easyAjax({
                url: '{{route('admin.all-tasks.store')}}',
                container: '#storeTask',
                type: "POST",
                data: $('#storeTask').serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        $('#eventDetailModal').modal('hide');
                        var responseTasks = response.tasks;
                        var responseLinks = response.links;
                        responseTasks.forEach(function(responseTask) {
                            gantt.addTask(responseTask);
                        });

                        responseLinks.forEach(function(responseLink) {
                            gantt.addLink(responseLink);
                        });
                    }
                }
            })
        };

        function loadData() {
            var url = '{{ route("admin.projects.schedulingData") }}';
                    @if($ganttProjectId != '')
            var url = '{{ route("admin.projects.schedulingData", [$ganttProjectId,$ganttTitle]) }}';
            @endif
            gantt.clearAll();
            gantt.load(url);
            $(".right-sidebar").slideDown(50).removeClass("shw-rside");
        }

        function limitMoveRight(task, limit) {
            var dur = task.end_date - task.start_date;
            task.start_date = new Date(limit.end_date);
            task.end_date = new Date(+task.start_date + dur);
        }

        function limitResizeRight(task, limit) {
            task.start_date = new Date(limit.end_date)
        }


        gantt.attachEvent("onTaskDrag", function (id, mode, task, original, e) {

            if(task.dependent_task_id !== null && task.dependent_task_id !== undefined)
            {
                var parent = gantt.getTask(task.dependent_task_id),
                    modes = gantt.config.drag_mode;

                var limitLeft = null,
                    limitRight = null;

                if (!(mode == modes.move || mode == modes.resize)) return;

                if (mode == modes.move) {
                    limitRight = limitMoveRight;
                } else if (mode == modes.resize) {
                    limitRight = limitResizeRight;
                }

                if (parent && +parent.end_date > +task.start_date) {
                    limitRight(task, parent);
                }
            }
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $("#baselineupdate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{url('admin/projects/baselineupdate')}}',
                container: '#baselineupdate',
                type: "POST",
                data: $('#baselineupdate').serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        $('#baselineModal').modal('hide');
                        loadData();
                    }
                }
            })
        });
        $("#setbaselineupdate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{url('admin/projects/createbaseline')}}',
                container: '#setbaselineupdate',
                type: "POST",
                data: $('#setbaselineupdate').serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        $('#SetbaselineModal').modal('hide');
                        loadData();
                    }
                }
            })
        });
        $("#costitemupdate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{ route('admin.projects.costDataUpdate')}}',
                container: '#costitemupdate',
                type: "POST",
                data: $('#costitemupdate').serialize(),
                success: function (response) {
                    $('.costupdateerror').html("");
                    if (response.status == "success") {
                        $('#eventDetailModal').hide();
                        loadData();
                        $("#costitemupdate")[0].reset();
                    }else if(response.status == "fail"){
                        $('.costupdateerror').html(response.message);

                    }
                    setInterval(2000,function () {
                        $('.costupdateerror').html("");
                    })
                }
            })
        });

        jQuery('#cleardate').click(function(){
            $("#baselinedate").val("");
        });
        jQuery('#due_date2, #start_date2, #baselinedate').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#dependent-task').change(function () {
            if($(this).is(':checked')){
                $('#dependent-fields').show();
            }
            else{
                $('#dependent-fields').hide();
            }
        })
        function removebaseline(baselineid){
            if(confirm("Are you sure want to delete??")){
                var token = '{{ csrf_token() }}';
                $.easyAjax({
                    url: '{{ route('admin.projects.removeBaseline')}}',
                    type: "POST",
                    data: { '_token': token, 'baselineid': baselineid },
                    success: function (response) {
                        if (response.status == "success") {
                            $('#baselinerow'+baselineid).remove();
                            loadData();
                        }
                    }
                });
            }
        }
        $(".baselinestatus").change(function () {
            var token = '{{ csrf_token() }}';
            var baselineid = $(this).val();
            $.easyAjax({
                url: '{{ route('admin.projects.activeBaseline')}}',
                type: "POST",
                data: { '_token': token, 'baselineid': baselineid },
                success: function (response) {

                }
            });
        });
        $(document).on("click",".replyfileblock",function () {
            $("#task-upload-box").toggle();
        });


        $(document).on("click",".replysubmit",function () {
            var token = '{{ csrf_token() }}';
            var comment = $(".replycomment").val();
            var taskid = $(".replytaskid").val();
            var costid = $(".replycostid").val();
            if(comment&&taskid){
                $.easyAjax({
                    url: '{{ route('admin.projects.replyComment')}}',
                    type: "POST",
                    data: { '_token': token, 'comment': comment, 'taskid': taskid },
                    success: function (response) {
                        $(".timeline-list").append(response.taskhtml);
                        if(myDropzone.getQueuedFiles().length > 0){
                            taskID = response.taskID;
                            percentageID = response.percentageID;
                            $('#taskID').val(response.taskID);
                            $('#percentageID').val(response.percentageID);
                            myDropzone.processQueue();
                        }
                        $(".replycomment").val("");
                        $("#task-upload-box").toggle();
                        myDropzone.destroy();
                        viewTask(costid);
                    }
                });
            }
        });
        function addtaskrow(){
            var taskid = $(".gantt_selected").data('task-id');
            $("#addNewModal").modal("hide");
            $("#createTaskModal").modal("show");
            $(".prevtaskid").val(taskid);
        }
        function addmilestonerow(){
            var taskid = $(".gantt_selected").data('task-id');
            $("#addNewModal").modal("hide");
            $("#createMilestoneModal").modal("show");
            $(".prevtaskid").val(taskid);
        }
        function addactivityrow(){
            var taskid = $(".gantt_selected").data('task-id');
            $("#addNewModal").modal("hide");
            $("#createActivityModal").modal("show");
            $(".prevtaskid").val(taskid);
        }

        $("#costitemcreate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{ route('admin.projects.costDataCreate')}}',
                container: '#costitemcreate',
                type: "POST",
                data: $('#costitemcreate').serialize(),
                success: function (response) {
                    $('.costupdateerror').html("");
                    if (response.status == "success") {
                        $('#createMilestoneModal').modal('hide');
                        $('#createTaskModal').modal('hide');
                        loadData();
                        $("#costitemcreate")[0].reset();
                    }else if(response.status == "fail"){
                        $('.costupdateerror').html(response.message);
                    }

                    setInterval(2000,function () {
                        $('.costupdateerror').html("");
                    })
                }
            })
        });

        $("#costitemmilestonecreate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{ route('admin.projects.costDataCreate')}}',
                container: '#costitemmilestonecreate',
                type: "POST",
                data: $('#costitemmilestonecreate').serialize(),
                success: function (response) {
                    $('.costupdateerror').html("");
                    if (response.status == "success") {
                        $('#createMilestoneModal').modal('hide');
                        $('#createTaskModal').modal('hide');
                        loadData();
                        $("#costitemmilestonecreate")[0].reset();
                    }else if(response.status == "fail"){
                        $('.costupdateerror').html(response.message);
                    }

                    setInterval(2000,function () {
                        $('.costupdateerror').html("");
                    })
                }
            })
        });
        $("#activitycreate").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{ route('admin.projects.activityCreate')}}',
                container: '#activitycreate',
                type: "POST",
                data: $('#activitycreate').serialize(),
                success: function (response) {
                    $('.costupdateerror').html("");
                    if (response.status == "success") {
                        $('#createActivityModal').modal('hide');
                        loadData();
                        $("#activitycreate")[0].reset();
                    }else if(response.status == "fail"){
                        $('.costupdateerror').html(response.message);
                    }

                    setInterval(2000,function () {
                        $('.costupdateerror').html("");
                    })
                }
            })
        });
    </script>
@endpush

