@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('modules.projects.milestones')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <?php if(empty($project)){ ?>
    @foreach($projectlist as $projectddata)
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <a @if(sub_project_access($projectddata->id)) href="{{ route('admin.milestone.showList',$projectddata->id) }}"  @else href="{{ route('admin.milestone.milestoneslist',[$projectddata->id,'0']) }}"  @endif>
                            <img class="img-responsive project-img" src="{{ $projectddata->imageurl }}" alt="{{ $projectddata->project_name }} Buildings" />
                            <h3 class="text-center">{{ ucwords($projectddata->project_name) }} @if($projectddata->added_by!=$user->id)<label class="badge badge-warning">Shared</label>@endif</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <?php   }else{
    $acc_id = 0;
    $costitemslist = \App\CostItems::orderBy("id",'asc')->get(); ?>
    @foreach($titles as $title)
        <?php
        $proproget = \App\ProjectCostItemsPosition::where('project_id',$title->project_id)->where('position','row')->where('level','1')->orderBy('inc','asc')->get();

        $tv = DB::table('project_cost_item_final_qty')
            ->select(DB::raw('SUM(total_amount) as total_cost'))
            ->where('title', $title->id)
            ->where('project_id', $title->project_id)
            ->groupBy('title')
            ->first();

        $tq = DB::table('project_cost_items_product')
            ->select(DB::raw('count(finalamount) as finalamount'))
            ->where('title', $title->id)
            ->where('project_id', $title->project_id)
            ->groupBy('title')
            ->first();
        $tn = \App\Title::where('id',$title->title)->first();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pdl10">
                        <!-- .page title -->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="img-responsive  project-img" src="{{ $project->imageurl }}" alt="{{ $title->title }} Buildings"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $title->title }}</h4>
                            <small>Created on: {{ date('d M Y H:i A',strtotime($title->created_at)) }}</small>
                            <h5 class="b-b p-b-10">Total Construction Value : <?php if(isset($tv) && $tv !== ''){ echo $tv->total_cost; } ?></h5>
                            <h5 class="b-b p-b-10">Project type : HIGH RISE</h5>
                            <h5 class="b-b p-b-10">Project Duration : 1 year</h5>
                            <h5 class="b-b p-b-10">Build up area : 10,235 sqFt</h5>
                        </div>
                        <!-- /.page title -->
                        <!-- .breadcrumb -->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <p>Created by: Admin</p>
                            <a href="{{ url('admin/projects/boq/'.$title->project_id.'/'.$title->id) }}" class="btn btn-primary btn-circle "   ><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="javascript:;" class="btn btn-danger btn-circle sa-params" data-toggle="tooltip" data-title-id="{{ $title->id }}" data-original-title="Delete">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                        <!-- /.breadcrumb -->
                    </div>

                </div>
            </div>
        </div>

    @endforeach
    <?php }?>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script>

        $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ url('admin/projects/milestones') }}'+'/'+projectId;

        });
            <?php if(!empty($project)){?>
        var table = $('#timelog-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.milestone.data', $project->id) !!}',
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'milestone_title', name: 'milestone_title'},
                    {data: 'cost', name: 'cost'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'}
                ]
            });

        <?php }?>
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.milestone.store')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        $('#logTime').trigger("reset");
                        $('#logTime').toggleClass('hide', 'show');
                        table._fnDraw();
                    }
                }
            })
        });

        $('#show-add-form, #close-form').click(function () {
            $('#logTime').toggleClass('hide', 'show');
        });


        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('milestone-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted milestone!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.milestone.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('body').on('click', '.edit-milestone', function () {
            var id = $(this).data('milestone-id');

            var url = '{{ route('admin.milestone.edit', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('{{ __('app.edit') }} {{ __('modules.projects.milestones') }}');
            $.ajaxModal('#editTimeLogModal', url);

        });

        $('body').on('click', '.milestone-detail', function () {
            var id = $(this).data('milestone-id');
            var url = '{{ route('admin.milestone.detail', ":id")}}';
            url = url.replace(':id', id);
            $('#modelHeading').html('@lang('app.update') @lang('modules.projects.milestones')');
            $.ajaxModal('#editTimeLogModal',url);
        })
        $('ul.showProjectTabs .projectMilestones').addClass('tab-current');

    </script>
@endpush
