@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('modules.projects.milestones')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    {{--   @include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">

                                <div class="col-md-12" id="issues-list-panel">
                                    <div class="white-box">
                                        <h2 style="color: #002f76">@lang('modules.projects.milestones')</h2>

                                        <div class="row m-b-10">
                                            <div class="col-md-12">
                                                <a href="{{ route('admin.milestone.show','all') }}" class="btn btn-success btn-outline"><i class="fa fa-flag"></i> @lang('app.back')
                                                </a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! Form::open(['id'=>'logTime','class'=>'ajax-form','method'=>'POST']) !!}

                                                <div class="form-body">
                                                    <div class="row m-t-30">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>@lang('modules.projects.milestoneTitle')</label>
                                                                <input  name="title" type="text" class="form-control" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>@lang('app.status')</label>
                                                                <select name="status" id="status" class="form-control">
                                                                    <option value="">@lang('app.select') @lang('app.status')</option>
                                                                    <option value="open">@lang('app.open')</option>
                                                                    <option value="complete">@lang('app.complete')</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>@lang('app.startdate')</label>
                                                                <input name="start_date" type="text"  class="form-control datepicker" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>@lang('app.duedate')</label>
                                                                <input name="due_date" type="text"  class="form-control datepicker" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>@lang('app.project')</label>
                                                                <select name="project_id" id="project_id" class="form-control">
                                                                    <option value="">Select Project</option>
                                                                    @if(count($projectlist)>0)
                                                                        @foreach($projectlist as $project)
                                                                            <option value="{{ $project->id }}">{{ $project->project_name }}</option>
                                                                        @endforeach
                                                                        @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php  if(in_array('sub_projects', $user->modules)) {?>
                                                        <div class="col-md-4" id="subprojectblock" style="display: none">
                                                            <div class="form-group">
                                                                <label>@lang('app.subproject')</label>
                                                                <select name="sub_project" id="subprojectlist" class="form-control">
                                                                    <option value="">Select Sub Project</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php }?>
                                                        <?php  if(in_array('segments', $user->modules)) {?>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>@lang('app.segments')</label>
                                                                <select name="segment_id" id="segmentid" class="form-control">
                                                                    <option value="">Select @lang('app.segments')</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php }?>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>@lang('app.activity')</label>
                                                                <select name="activity" id="activitylist" class="form-control">
                                                                    <option value="">Select Activity</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>@lang('app.createmilestonesafter')</label>
                                                                <select name="tasks" id="tasklist" class="form-control">
                                                                    <option value="">Select Task</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="memo">@lang('app.description')</label>
                                                            <textarea name="description" id="" rows="4" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions m-t-30 text-right">
                                                    {{ csrf_field() }}
                                                    <button type="button" id="save-form" class="btn btn-success"><i  class="fa fa-check"></i> @lang('app.save')</button>
                                                    <button type="button" id="close-form" class="btn btn-default"><i class="fa fa-times"></i> @lang('app.close')</button>
                                                </div>
                                                {!! Form::close() !!}

                                                <hr>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.milestone.store')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        window.location.href='{{ route('admin.milestone.show','all') }}';
                    }
                }
            })
        });
        $('#project_id').change(function () {
            var id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id},
                success: function(data){
                    $("#subprojectblock").hide();
                    if(data.subprojectlist){
                        $("#subprojectblock").show();
                        $("select#subprojectlist").html("");
                        $("select#subprojectlist").html(data.subprojectlist);
                        $('select#subprojectlist').select2();
                    }
                    if(data.activitylist){
                        $("select#activitylist").html("");
                        $("select#activitylist").html(data.activitylist);
                        $('select#activitylist').select2();
                    }
                }
            });
        });

        $("#subprojectlist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token,'projectid': project,'subprojectid': titlelist},
                    success: function(data){
                        if(data.activitylist){
                            $("select#activitylist").html("");
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                    }
                });
            }
        });
        $("#activitylist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token,'projectid': project,'subprojectid': titlelist,'activity': activity},
                    success: function(data){
                        if(data.tasklist){
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        jQuery('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $('body').on('click', '.edit-milestone', function () {
            var id = $(this).data('milestone-id');

            var url = '{{ route('admin.milestone.edit', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('{{ __('app.edit') }} {{ __('modules.projects.milestones') }}');
            $.ajaxModal('#editTimeLogModal', url);

        });

        $('body').on('click', '.milestone-detail', function () {
            var id = $(this).data('milestone-id');
            var url = '{{ route('admin.milestone.detail', ":id")}}';
            url = url.replace(':id', id);
            $('#modelHeading').html('@lang('app.update') @lang('modules.projects.milestones')');
            $.ajaxModal('#editTimeLogModal',url);
        })
        $('ul.showProjectTabs .projectMilestones').addClass('tab-current');

    </script>
@endpush
