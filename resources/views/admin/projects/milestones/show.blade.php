@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ $pageTitle }}</a></li>
                <li class="active">@lang('modules.projects.milestones')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3 " style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form method="post" action="{{ route('admin.milestone.show','all') }}"  >
            <div class="col-md-12">
                <div class="example">
                    <h5 class="box-title">@lang('app.selectDateRange')</h5>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.startDate')" value="{{ \Carbon\Carbon::today()->subDays(7)->format($global->date_format) }}" />
                        <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                        <input type="text" class="form-control" name="enddate" id="end-date" placeholder="@lang('app.endDate')" value="{{ \Carbon\Carbon::today()->format($global->date_format) }}" />
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">
                    @lang('app.project') </h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="project_id" data-placeholder="@lang('app.selectProject')" id="project_id">
                                <option value="">@lang('modules.client.all')</option>
                                @foreach($projectlist as $project)
                                    <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">
                    @lang('app.subproject') </h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="subproject_id" data-placeholder="@lang('app.subproject')" id="titlelist">
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">@lang('app.segment') </h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="segment_id"  data-placeholder="@lang('app.segment')" id="segmentlist">
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    {{--   @include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">

                            <div class="row">
                                <div class="col-md-12" id="issues-list-panel">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-sm-6 hidden-xs">
                                                <div class="pull-left">
                                                    <h2 style="color: #002f76">@lang('modules.projects.milestones')</h2>
                                                    <div class="row m-b-10">
                                                        <div class="col-md-12">
                                                            <a href="{{ route('admin.milestone.create') }}"
                                                               class="btn btn-success btn-outline"><i
                                                                        class="fa fa-flag"></i> @lang('modules.projects.createMilestone')
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <div class="pull-right">
                                                    <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                                                class="fa fa-cog"></i></a>
                                                    {{-- <a onclick="exportTimeLog()" class="btn btn-info"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                        <div class="table-responsive m-t-30">
                                            <table class="table table-bordered"  id="timelog-table">
                                                <thead>
                                                <tr>
                                                    <th>@lang('app.sno')</th>
                                                    <th>@lang('app.task')</th>
                                                    <th>@lang('app.project')</th>
                                                    <?php  if(in_array('sub_projects', $user->modules)) {?>
                                                    <th>@lang('app.subproject')</th>
                                                    <?php }?>
                                                    <?php  if(in_array('segments', $user->modules)) {?>
                                                    <th>@lang('app.segments')</th>
                                                    <?php }?>
                                                    <th>@lang('app.startdate')</th>
                                                    <th>@lang('app.duedate')</th>
                                                    <th>@lang('app.status')</th>
                                                    <th>@lang('app.action')</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script>
        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });
        $('#filter-results').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
        });
        loadTable();
        function loadTable() {

            var project_id = $('#project_id').val();
            var subproject_id = $('#subproject_id').val();
            var startdate = $('#start-date').val();
            var enddate = $('#end-date').val();
            var status = $('#status').val();
            var table = $('#timelog-table').dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.milestone.data') !!}?project_id=' + project_id+ '&subproject_id=' + subproject_id+ '&startdate=' + startdate,
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    {data: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'task', name: 'task'},
                    {data: 'project_id', name: 'project_id'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'deadline', name: 'deadline'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'}
                ]
            });
        }
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.milestone.store')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        $('#logTime').trigger("reset");
                        $('#logTime').toggleClass('hide', 'show');
                        loadTable();
                    }
                }
            })
        });

        $('#show-add-form, #close-form').click(function () {
            $('#logTime').toggleClass('hide', 'show');
        });


        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('milestone-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted milestone!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.milestone.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                loadTable();
                            }
                        }
                    });
                }
            });
        });

        $('body').on('click', '.edit-milestone', function () {
            var id = $(this).data('milestone-id');

            var url = '{{ route('admin.milestone.edit', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('{{ __('app.edit') }} {{ __('modules.projects.milestones') }}');
            $.ajaxModal('#editTimeLogModal', url);

        });

        $('body').on('click', '.milestone-detail', function () {
            var id = $(this).data('milestone-id');
            var url = '{{ route('admin.milestone.detail', ":id")}}';
            url = url.replace(':id', id);
            $('#modelHeading').html('@lang('app.update') @lang('modules.projects.milestones')');
            $.ajaxModal('#editTimeLogModal',url);
        })
        $('ul.showProjectTabs .projectMilestones').addClass('tab-current');
        $('#project_id').change(function () {
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        var projectdata = JSON.parse(data);
                        var titles = '<option value="">Select Title</option>';
                        var cositem = '<option value="">Select Cost item</option>';
                        if(projectdata.titles){
                            $.each( projectdata.titles, function( key, value ) {
                                titles += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#titlelist").html("");
                        $("select#titlelist").html(titles);
                        $('select#titlelist').select2();
                        if(projectdata.cositems){
                            $.each( projectdata.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projectcostitems') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        var titles = JSON.parse(data);
                        var cositem = '';
                        if(titles.cositems){
                            $.each( titles.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
    </script>
@endpush
