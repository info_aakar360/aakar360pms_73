@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.projects.members')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/icheck/skins/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                  {{--  @include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-2" class="show">

                            <div class="row">
                                <?php if(empty($project)){ ?>
                                @foreach($projectlist as $projectddata)
                                    <div class="col-md-3">
                                        <div class="panel panel-default">
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body">
                                                    <a @if(sub_project_access($projectddata->id)) href="{{ route('admin.project-members.show',$projectddata->id) }}"  @else href="{{ route('admin.projects.boqtitle',[$projectddata->id,'0']) }}"  @endif>
                                                        <img class="img-responsive project-img" src="{{ $projectddata->imageurl }}" alt="{{ $projectddata->project_name }} Buildings"/>
                                                        <h3 class="text-center">{{ ucwords($projectddata->project_name) }}</h3>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <?php   }else{?>

                                    <div class="col-md-6">
                                        <div class="white-box">
                                            <h3>@lang('modules.projects.inviteMember')</h3>

                                            {!! Form::open(['id'=>'createInvitemember','class'=>'ajax-form','autocomplete'=>'off','method'=>'POST']) !!}

                                            <div class="form-body">
                                                {!! Form::hidden('project_id', $project->id) !!}
                                                <div class="form-group">
                                                    <select class="select2 m-b-10 form-control"  data-placeholder="Choose User type" name="user_type">
                                                        <option value="">Select User type</option>
                                                        <option value="contractor">Contractor</option>
                                                        <option value="client">Client</option>
                                                        <option value="employee">Employee</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" id="user_id">
                                                    <input type="text" class="form-control" name="mobile" placeholder="Mobile / Email">
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" id="save-invite" class="btn btn-success"><i
                                                                class="fa fa-check"></i> @lang('app.invite')
                                                    </button>
                                                </div>
                                            </div>

                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="white-box">
                                            <h3>@lang('modules.projects.addMemberTitle')</h3>

                                            {!! Form::open(['id'=>'createMembers','class'=>'ajax-form','autocomplete'=>'off','method'=>'POST']) !!}

                                            <div class="form-body">

                                                {!! Form::hidden('project_id', $project->id) !!}

                                                <div class="form-group">
                                                    <select class="select2 form-control"  data-placeholder="Choose User type" name="user_type">
                                                        <option value="">Select User type</option>
                                                        <option value="contractor">Contractor</option>
                                                        <option value="client">Client</option>
                                                        <option value="employee">Employee</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" id="user_id">
                                                    <select class="select2 m-b-10 select2-multiple " multiple="multiple"
                                                            data-placeholder="Choose Members" name="user_id[]">
                                                        @foreach($employees as $emp)
                                                            <option value="{{ $emp->id }}">{{ ucwords($emp->name). ' ['.$emp->email.']' }} @if($emp->id == $user->id)
                                                                    (YOU) @endif</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-actions">
                                                    <button type="submit" id="save-members" class="btn btn-success"><i
                                                                class="fa fa-check"></i> @lang('app.save')
                                                    </button>
                                                </div>
                                            </div>

                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">@lang('modules.projects.members')</div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    <ul class="nav nav-pills">
                                                        <li class="active"><a data-toggle="pill" href="#inviites">@lang('app.invitation')</a></li>
                                                        <li ><a data-toggle="pill" href="#contractors">@lang('app.menu.contractors')</a></li>
                                                        <li><a data-toggle="pill" href="#clients">@lang('app.menu.clients')</a></li>
                                                        <li><a data-toggle="pill" href="#employees">@lang('app.menu.employees')</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="inviites" class="tab-pane fade in active">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('app.username')</th>
                                                                <th>@lang('app.invited_by')</th>
                                                                <th>@lang('app.action')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse($invitiesarray as $invitie)
                                                            <tr>
                                                                <td>
                                                                    {!! $invitie->mobile !!}
                                                                </td>
                                                                <td>
                                                                    {{ get_user_name($invitie->invited_by) }}
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" data-invite-id="{{ $invitie->id }}" class="btn btn-sm btn-warning btn-rounded invite-again"><i class="fa fa-check"></i> @lang('app.invite_again')</a>
                                                                    <a href="javascript:;" data-invite-id="{{ $invitie->id }}" class="btn btn-sm btn-danger btn-rounded delete-invite"><i class="fa fa-times"></i> @lang('app.remove')</a>
                                                                </td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td>
                                                                    @lang('messages.noMemberAddedToProject')
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                        </div>
                                                        <div id="contractors" class="tab-pane fade">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('app.name')</th>
                                                                <th>@lang('app.action')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse($contractorarray as $contractor)
                                                            <tr>
                                                                <td>
                                                                    {!! get_users_images($contractor->user_id) !!}
                                                                    {{ get_user_name($contractor->user_id) }}
                                                                </td>
                                                                <td><a href="javascript:;" data-member-id="{{ $contractor->id }}" class="btn btn-sm btn-danger btn-rounded delete-members"><i class="fa fa-times"></i> @lang('app.remove')</a></td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td>
                                                                    @lang('messages.noMemberAddedToProject')
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                        </div>
                                                        <div id="clients" class="tab-pane fade">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('app.name')</th>
                                                                <th>@lang('app.action')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse($clientarray as $client)
                                                            <tr>
                                                                <td>
                                                                    {!! get_users_images($client->user_id) !!}
                                                                    {{ get_user_name($client->user_id) }}
                                                                </td>
                                                                <td><a href="javascript:;" data-member-id="{{ $client->id }}" class="btn btn-sm btn-danger btn-rounded delete-members"><i class="fa fa-times"></i> @lang('app.remove')</a></td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td>
                                                                    @lang('messages.noMemberAddedToProject')
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                        </div>
                                                        <div id="employees" class="tab-pane fade">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>@lang('app.name')</th>
                                                                <th>@lang('app.action')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @forelse($employeearray as $employee)
                                                            <tr>
                                                                <td>
                                                                    {!! get_users_images($employee->user_id) !!}
                                                                    {{ get_user_name($employee->user_id) }}
                                                                </td>
                                                                <td><a href="javascript:;" data-member-id="{{ $employee->id }}" class="btn btn-sm btn-danger btn-rounded delete-members"><i class="fa fa-times"></i> @lang('app.remove')</a></td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td>
                                                                    @lang('messages.noMemberAddedToProject')
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                        </tbody>
                                                    </table>
                                                        </div>
                                                        </div>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php }?>
                            </div>

                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
//    (function () {
//
//        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
//            new CBPFWTabs(el);
//        });
//
//    })();

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    //    save project members
    $('#save-members').click(function () {
        $.easyAjax({
            url: '{{route('admin.project-members.store')}}',
            container: '#createMembers',
            type: "POST",
            data: $('#createMembers').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                    window.location.reload();
                }
            }
        })
    });
    //    save project members
    $('#save-invite').click(function () {
        $.easyAjax({
            url: '{{route('admin.project-members.invitemember')}}',
            container: '#createInvitemember',
            type: "POST",
            data: $('#createInvitemember').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        })
    });
    //    save project members
    $('.invite-again').click(function () {
        var token = '{{ csrf_token() }}';
        var id = $(this).data('invite-id');
        $.easyAjax({
            url: '{{route('admin.project-members.invitemember')}}',
            type: "POST",
            data: {'_token':token,'id':id},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                   /* swal("Sent", 'Invitation Sent Successfully', "success");*/
                }
            }
        })
    });

//    add group members
$('#save-group').click(function () {
    $.easyAjax({
        url: '{{route('admin.project-members.storeGroup')}}',
        container: '#saveGroup',
        type: "POST",
        data: $('#saveGroup').serialize(),
        success: function (response) {
            if (response.status == "success") {
                $.unblockUI();
                window.location.reload();
            }
        }
    })
});



$('body').on('click', '.delete-members', function(){
    var id = $(this).data('member-id');
    swal({
        title: "Are you sure?",
        text: "This will remove the member from the project.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {

            var url = "{{ route('admin.project-members.destroy',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                        window.location.reload();
                    }
                }
            });
        }
    });
});
$('body').on('click', '.delete-invite', function(){
    var id = $(this).data('invite-id');
    swal({
        title: "Are you sure?",
        text: "This will remove the invitation from the project.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {

            var url = "{{ route('admin.project-members.invitedestroy',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                        window.location.reload();
                    }
                }
            });
        }
    });
});

$('#project_id').change(function(){
    var projectId = $(this).val();
    window.location.href = '{{ url('admin/projects/project-members') }}'+'/'+projectId;

});
<?php if(!empty($project)){?>
$('body').on('click', '.assign_role', function(){
    var userId = $(this).val();
    var projectId = '{{ $project->id ?: '' }}';
    var token = "{{ csrf_token() }}";

    $.easyAjax({
        url: '{{route('admin.employees.assignProjectAdmin')}}',
        type: "POST",
        data: {userId: userId, projectId: projectId, _token : token},
        success: function (response) {
            if(response.status == "success"){
                $.unblockUI();
            }
        }
    })

});
<?php }?>
$('ul.showProjectTabs .projectMembers').addClass('tab-current');
</script>
@endpush
