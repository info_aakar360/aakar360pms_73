@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    @include('admin.projects.show_project_menu')
                </div>
            </section>
        </div>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form method="post" id="productCostItem">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="project_id" value="{{ $id }}"/>
                            <div class="row">
                                <div class="col-lg-3 ">
                                    <div class="form-group">
                                        <input type="text" name="title" id="title" class="form-control" placeholder="Title" value="{{ $package->title }}">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <select name="create_from[]" class="selectpicker form-control" onchange="getProducts(this)" id="create_from" multiple>
                                        <option value="">Please select BOQ</option>
                                        <?php $ass = json_decode($package->create_from); ?>
                                        @foreach($costitems as $category)
                                            <option value="{{ $category->id }}"
                                            <?php

                                                if (in_array($category->id, $ass)){
                                                    echo 'selected'; } ?>
                                            >{{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select name="category[]" class="selectpicker form-control" id="category" multiple>
                                       <?php
                                        if ($package->category){
                                        $xcat = json_decode($package->category);
                                        for ($i = 0; $i < count($xcat); $i++) {
                                            $name = \App\BoqCategory::where('id', $xcat[$i])->first();
                                            echo '<option value="' . $name->id .'"';
                                            $assa = json_decode($package->category);
                                            if (in_array($name->id, $assa)){
                                                echo 'selected'; }
                                            echo '>' . $name->title . '</option>';
                                        }
                                        }?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select name="type[]" class="selectpicker form-control" id="type" multiple>
                                        <option value="">Please select type</option>
                                        <option value="Workforce"
                                        <?php $tp = json_decode($package->type);
                                        if (in_array('Workforce', $tp)){ echo 'selected'; } ?>
                                        >Workforce</option>
                                        <option value="Equipment"
                                        <?php if (in_array('Equipment', $tp)){ echo 'selected'; } ?>
                                        >Equipment</option>
                                        <option value="Material"
                                        <?php if (in_array('Material', $tp)){ echo 'selected'; } ?>
                                        >Material</option>
                                        <option value="Commitment"
                                        <?php if (in_array('Commitment', $tp)){ echo 'selected'; } ?>
                                        >Commitment</option>
                                        <option value="Owner Cost"
                                        <?php if (in_array('Owner Cost', $tp)){ echo 'selected'; } ?>
                                        >Owner Cost</option>
                                        <option value="Professional Services"
                                        <?php if (in_array('Professional Services', $tp)){ echo 'selected'; } ?>
                                        >Professional Services</option>
                                        <option value="Other"
                                        <?php if (in_array('Other', $tp)){ echo 'selected'; } ?>
                                        >Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                <div style="padding: 5px;"><b>Product</b></div>
                            </div>
                            <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                <div style="padding: 5px;"><b>Quantity</b></div>
                            </div>
                            <div class="col-lg-12">
                                <?php
                                $cf = json_decode($package->create_from);
                                $p  = 1;
                                foreach ($packageProducts as $product) {
//                                    for($j = 0; $j < count($cf); $j++ ) {
                                    $fqty = DB::table('project_cost_item_final_qty')
                                        ->where('title',$cf[$p])
                                        ->where('project_id', $id)
                                        ->sum('qty');
                                    echo '<div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                        <label style="padding: 5px;">
                                            <input type="checkbox" name="products[]" value="' . $product->id . '" class="form-check-input" checked>
                                            ' . $product->name . '
                                        </label>
                                    </div>
                                    <div class="col-lg-6" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 39px;">
                                        <label style="padding: 5px;">';
                                            if($fqty) echo $fqty;
                                        echo '</label>
                                    </div>';
                               } //}?>
                            </div>
                            <div class="col-lg-12" id="productData"></div>
                        <div class="col-md-12" style="text-align: right;">&nbsp;</div>
                        <div class="col-md-12" style="text-align: right;">
                            <input type="button" name="submit" class="btn btn-success saveProduct" value="Submit" id="saveProduct">
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script>

        $(document).on('click', '#saveProduct', function(){
            $.easyAjax({
                url: '{{ route('admin.projects.updateSourcingPackages',[$id]) }}',
                container: '#productCostItem',
                type: "POST",
                data: $('#productCostItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });

        function getProducts(val) {
            var formData = $('form#productCostItem').serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getProducts') }}",
                data: formData,
                success: function(data){
                    $("#productData").html(data.products);
                    $("#category").html(data.category);
                    $('#category').selectpicker('refresh');
                }
            });
        }

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function getProduct(val) {
            var token = "{{ csrf_token() }}";
            var id = "{{ $id }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getProduct') }}",
                data: {'_token': token, 'cost_item_id': val, 'id': id},
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Sourcing').addClass('tab-current');

        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#rate'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#cost'+id).val(total);
        }

    </script>
@endpush