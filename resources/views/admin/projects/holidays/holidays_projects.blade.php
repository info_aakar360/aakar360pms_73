@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> @lang('app.selectprojects')</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <style>
        .custom-action a {
            margin-right: 15px;
            margin-bottom: 15px;
        }
        .custom-action a:last-child {
            margin-right: 0px;
            float: right;
        }

        .dashboard-stats .white-box .list-inline {
            margin-bottom: 0;
        }

        .dashboard-stats .white-box {
            padding: 10px;
        }

        .dashboard-stats .white-box .box-title {
            font-size: 13px;
            text-transform: capitalize;
            font-weight: 300;
        }
        @media all and (max-width: 767px) {
            .custom-action a {
                margin-right: 0px;
            }

            .custom-action a:last-child {
                margin-right: 0px;
                float: none;
            }
        }
    </style>
@endpush

@section('content')
        <div class="row"  >
            <?php $x=1;?>
            @foreach($projectlist as $project)
                <div class="col-md-3">
                    <div class="white-box-no-pad">
                        <div class="row">
                            <!-- .page title -->
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 p-0">
                                <img class="img-full" src="{{ $project->imageurl }}" alt="{{ $project->project_name }} Buildings"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                @if($project->added_by) @if($project->added_by!=$user->id)<label class="badge badge-warning mt-10">Shared</label>@endif @endif
                                <h4 class="page-title"><a href="{{ route('admin.projectholidays.holidayindex',$project->id) }}"><i class="{{ $pageIcon }}"></i> {{ $project->project_name }}</a> </h4>
                                <h5 class="b-b p-b-10"><i class="icon-clock"></i> {{ date('d M Y',strtotime($project->start_date)) }} - {{ date('d M Y',strtotime($project->deadline)) }}</h5>
                            </div>
                            <!-- /.page title -->
                        </div>

                    </div>
                </div>
                @if($x%4==0)
        </div>
        <div class="row"  >
            @endif
            <?php $x++;?>
            @endforeach
        </div>

@endsection

@push('footer-script')
    <script>

    </script>
@endpush