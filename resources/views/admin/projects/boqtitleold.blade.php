@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.projectboq')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dragtable.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            overflow-x: scroll;
            overflow-y: auto;
            max-height: 75vh;
            border: 2px solid #bdbdbd;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet table {
            border-collapse: collapse;
            border-spacing: 0px;
        }
        .combo-sheet thead{
            background-color: #C2DCE8;
        }

        .combo-sheet thead tr th{
            font-size: 12px;
            background-color: #DFDFDF !important;
            color: #002F76 !important;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        }
        .combo-sheet td{
            padding: 0px !important;
            font-size: 12px;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .open-rate-sheet{
            position: absolute;
            right: 5px;
            font-size: 16px;
            padding: 0 3px;
        }
        .combo-sheet.formula-sheet, .rate-sheet.rate-formula-sheet{
            position: absolute;
            left: 0;
            top: 0;
            overflow-x: scroll;
            overflow-y: auto;
            z-index: 2;
            background: #ffffff;
            margin: 0 5px;
            right: 0;
            display: none;
            max-height: 100vh;
            bottom: 0;
        }
        .combo-sheet select.cell-inp {
            padding: 2.5px 0;
            width: 100%;
            border: none;
        }
        .rate-formula-sheet .loaderx{
            padding: 18% 0;
        }
        .formula-sheet .loaderx{
            padding: 18% 0;
        }
        .context-menu {
            position: absolute;
            right: 5%;
            margin-top: 4px;
        }
        .row_position {
            overflow-y: scroll;
            max-height: 500px;
            display: block;
            width: 100%;
        }
        /*.row_head {
            display: table; !* to take the same width as tr *!
            width: calc(100% - 17px); !* - 17px because of the scrollbar width *!
        }*/
        .RowCategory{
            width: 63px;
        }
        .RowType{
            width: 45px;
        }
        .RowCode{
            width: 145px;
        }
        .RowDescription{
            width: 255px;
        }
        .RowUnit{
            width: 90px;
        }
        .RowBaseRate{
            width: 90px;
        }
        .RowSurcharge{
            width: 117px;
        }
        .RowDiscount{
            width: 108px;
        }
        .RowFactor{
            width: 90px;
        }
        .RowFinalRate{
            width: 90px;
        }
        .RowRemark{
            width: 360px;
        }
        .white-box{
            padding: 5px !important;
        }
        .pdl10{
            padding-left: 10px !important;
        }
        .page-title{
            color: #0f49bd;
            font-weight: 900;
        }
        .page-title i{
            margin-right: 5px;
        }
    </style>

@endpush
@section('content')

    <?php $acc_id = 0;
    $costitemslist = \App\CostItems::orderBy("id",'asc')->get();

        $proproget = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();
         if($title){
             $tn = \App\Title::where('id',$title)->where('project_id',$id)->first();
         }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pdl10">
                        <!-- .page title -->
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ !empty($tn) ? $tn->title : get_project_name($id) }}</h4>
                        </div>
                        <!-- /.page title -->
                        <!-- .breadcrumb -->
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#lockModal" >Lock table</a>
                          </div>
                        <!-- /.breadcrumb -->
                    </div>
                    <div class="combo-sheet">
                        <div class="table-wrapper">
                            <table id="boqtable" class="table table-bordered default footable-loaded footable boqtable" data-resizable-columns-id="users-table">
                                <thead>
                                <tr >
                                    <th colspan="4"></th>
                                    <?php $colpositionarray = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->orderBy('inc','asc')->get();
                                        foreach ($colpositionarray as $colposition){ ?>
                                    <th col="{{ $colposition->id }}">{{ $colposition->itemname }}</th>
                                    <?php }?>
                                </tr>
                                </thead>
                                <tbody class="colposition">
                                <?php
                                 /* Level 0 category */
                                        $grandtotal = 0;
                                foreach ($proproget as $propro){
                                $cattotalamt = 0;
                                $catvalue = (string)$propro->itemid;
                                ?>
                                    <tr data-level1cat="{{ $propro->id }}" data-depth="0" class="maincat  collpse level0 catitem{{ $catvalue }}">
                                        <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="{{ $propro->id }}" data-level="0" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                        <td colspan="4"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a> <?php if (isset($propro->itemname)){ echo $propro->itemname; } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <!-- level 0 parent category issue -->
                                <?php
                                $proprogetdatarra = \App\ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('category',$catvalue)->orderBy('inc','asc')->get();
                               foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                                $catitem = $propro->itemid;
                                $proprogetdat->position_id = $propro->id;
                                $proprogetdat->save();
                                ?>
                                <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                    <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="{{ $proprogetdat->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                    <td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                    <?php foreach ($colpositionarray as $colposition){
                                    switch($colposition->itemslug){
                                    case 'costitem': ?>
                                    <td><input  class="cell-inp updateproject" data-item="cost_items_id"  data-positionid="{{ $propro->id }}" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="costitem{{ $proprogetdat->id }}"  value="{{ get_cost_name($proprogetdat->cost_items_id) }}">
                                        <datalist class="costitemslist"  id="costitem{{ $proprogetdat->id }}">
                                            @foreach($costitemslist as $costitem)
                                                <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                            @endforeach
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'description': ?>
                                    <td><textarea  class="cell-inp updateproject"  data-item="description" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onkeydown="textAreaAdjust(this)" style="height: 25px;">{{ !empty($proprogetdat->description) ? $proprogetdat->description : '' }}</textarea></td>
                                    <?php   break;
                                    case 'assign_to': ?>
                                    <td><input  class="cell-inp updateproject" data-item="assign_to" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="assign{{ $proprogetdat->id }}"  value="{{ get_user_name($proprogetdat->assign_to) }}">
                                        <datalist class="assignlist"  id="assign{{ $proprogetdat->id }}">
                                            @foreach($userarray as $assign)
                                                <option  value="{{ $assign->id }}" data-value="{{ $assign->name }}" >{{ $assign->name }}</option>
                                            @endforeach
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'contractor': ?>
                                    <td><input  class="cell-inp updateproject" data-item="contractor" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="contractorid{{ $proprogetdat->id }}"  value="{{ get_contractors_name($proprogetdat->contractor) }}">
                                        <datalist class="contractorlist"  id="contractorid{{ $proprogetdat->id }}">
                                            @foreach($contractorarray as $contractor)
                                                <option  value="{{ $contractor->id }}"  data-value="{{ $contractor->name }}">{{ $contractor->name }}</option>
                                            @endforeach
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'startdate': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="start_date" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->start_date) ? date('d-m-Y',strtotime($proprogetdat->start_date)) : '' }}"></td>
                                    <?php   break;
                                    case 'enddate': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="deadline" data-itemid="{{ $proprogetdat->id }}"   value="{{ !empty($proprogetdat->deadline) ? date('d-m-Y',strtotime($proprogetdat->deadline)) : '' }}"></td>
                                    <?php   break;
                                    case 'rate': ?>
                                    <td style="position:relative;"><a href="javascript:;" class="open-rate-sheet" title="Open Rate Sheet" data-toggle="tooltip" data-id="{{ $proprogetdat->id }}"><i class="fa fa-ellipsis-v"></i></a>
                                        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject ratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-cat="{{ $catitem }}"  data-item="rate" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->rate) ? $proprogetdat->rate : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'qty': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject qty{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-item="qty" value="{{ !empty($proprogetdat->qty) ? $proprogetdat->qty : '' }}"></td>
                                    <?php   break;
                                    case 'unit':?>
                                    <td>
                                        <input  class="cell-inp updateproject"  <?php if($colposition->collock=='1'){ echo 'disabled';}?> data-item="unit" data-itemid="{{ $proprogetdat->id }}" list="unitdata{{ $proprogetdat->id }}"   value="{{ get_unit_name($proprogetdat->unit) }}">
                                    <datalist id="unitdata{{ $proprogetdat->id }}">
                                        @foreach($unitsarray as $units)
                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                        @endforeach
                                    </datalist>
                                    </td>
                                    <?php   break;
                                    case 'worktype': ?>
                                    <td> <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject" data-item="worktype"  data-itemid="{{ $proprogetdat->id }}" list="type{{ $proprogetdat->id }}"  value="{{ get_work_type_name($proprogetdat->worktype) }}">
                                        <datalist id="type{{ $proprogetdat->id }}">
                                            @foreach($typesarray as $types)
                                                <option data-value="{{ $types->id }}" >{{ ucwords($types->title) }}</option>
                                            @endforeach
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'markuptype': ?>
                                    <td>
                                        <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onchange="calmarkup({{ $proprogetdat->id }})"  class="updateproject cell-inp markuptype{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" data-item="markuptype" data-itemid="{{ $proprogetdat->id }}" list="markuptype{{ $proprogetdat->id }}"  value="{{ $proprogetdat->markuptype }}">
                                        <datalist id="markuptype{{ $proprogetdat->id }}">
                                            <option value="percent">Percent</option>
                                            <option value="amt">Amount</option>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'markupvalue': ?>
                                    <td><input  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  type="text" class="updateproject cell-inp markupvalue{{ $proprogetdat->id }}" data-item="markupvalue"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="markupvalue" onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->markupvalue) ? $proprogetdat->markupvalue : '' }}" ></td>
                                    <?php   break;
                                    case 'adjustment': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp adjustment{{ $proprogetdat->id }} adjustcat{{ $catitem }}" data-item="adjustment"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="adjustment" onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->adjustment) ? $proprogetdat->adjustment : '' }}" ></td>
                                    <?php   break;
                                    case 'finalrate': ?>
                                    <td><input type="text"   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp totalrate{{ $proprogetdat->id }} finalrate{{ $catitem }}" data-item="finalrate"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="finalrate"   value="{{ !empty($proprogetdat->finalrate) ? $proprogetdat->finalrate : '' }}" ></td>
                                    <?php   break;
                                    case 'totalamount': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp grandvalue totalamount{{ $proprogetdat->id }} finalamount{{ $catitem }}" data-item="totalamount" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" name="finalamount"  value="{{ !empty($proprogetdat->finalamount) ? $proprogetdat->finalamount : '' }}"  ></td>
                                    <?php   break;
                                    }
                                    }?>
                                </tr>
                                <?php  $grandtotal += $proprogetdat->finalamount;
                                $cattotalamt +=$proprogetdat->finalamount;
                                } }
                               /* next level category */
                                 $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level','1')->where('parent',$propro->itemid)->orderBy('inc','asc')->get();?>

                                @php $catdata = 1;
                                $catitem = 0;
                                @endphp
                                @if(count($proprogetlevel1)>0)
                                     @foreach($proprogetlevel1 as $propolevel1)
                                       <?php  if(!empty($propolevel1->itemid)){
                                                   $catvalue .=   ','.$propolevel1->itemid;
                                               }
                                        $catitem = $propolevel1->itemid;
                                        $proprogetdatarra = \App\ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('category',$catvalue)->orderBy('inc','asc')->get();
                                       ?>
                                        <tr  col="row{{ $catdata }}"  data-depth="1" class="subcat collpse level1">
                                            <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="{{ $propolevel1->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                            <td colspan="3" class="text-center"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                            <td> {{ $propolevel1->itemname }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        $cattotalamt = 0;
                                        foreach ($proprogetdatarra as $proprogetdat){
                                                if(!empty($proprogetdat->id)){
                                        $proprogetdat->position_id = $propolevel1->id;
                                        $proprogetdat->save();
                                         /*   $catarray = explode(',',$proprogetdat->category);
                                            var_dump($catarray);
                                        \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('itemid',$catarray[1])->update(['parent'=>$catarray[0],'level'=>1]);
                                               */ ?>
                                                   <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="2" class="collpse level2 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                                       <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="{{ $proprogetdat->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                                       <td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                                       <?php foreach ($colpositionarray as $colposition){
                                                           switch($colposition->itemslug){
                                                               case 'costitem': ?>
                                                               <td><input  class="cell-inp updateproject" data-item="cost_items_id" data-positionid="{{ $propolevel1->id }}" data-itemid="{{ $proprogetdat->id }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="costitem{{ $proprogetdat->id }}"  value="{{ get_cost_name($proprogetdat->cost_items_id) }}">
                                                                   <datalist class="costitemslist" id="costitem{{ $proprogetdat->id }}">
                                                                       @foreach($costitemslist as $costitem)
                                                                           <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                                                       @endforeach
                                                                   </datalist>
                                                               </td>
                                                                <?php   break;
                                                               case 'description': ?>
                                                                   <td><textarea  class="cell-inp updateproject"  data-item="description" data-itemid="{{ $proprogetdat->id }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onkeydown="textAreaAdjust(this)" style="height: 25px;">{{ !empty($proprogetdat->description) ? $proprogetdat->description : '' }}</textarea></td>
                                                               <?php   break;
                                                               case 'assign_to': ?>
                                                               <td><input  class="cell-inp updateproject" data-item="assign_to" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="assign{{ $proprogetdat->id }}"  value="{{ get_user_name($proprogetdat->assign_to) }}">
                                                                   <datalist class="assignlist"  id="assign{{ $proprogetdat->id }}">
                                                                       @foreach($userarray as $assign)
                                                                           <option  value="{{ $assign->id }}" data-value="{{ $assign->name }}" >{{ $assign->name }}</option>
                                                                       @endforeach
                                                                   </datalist>
                                                               </td>
                                                               <?php   break;
                                                               case 'contractor': ?>
                                                               <td><input  class="cell-inp updateproject" data-item="contractor" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="contractorid{{ $proprogetdat->id }}"  value="{{ get_contractors_name($proprogetdat->contractor) }}">
                                                                   <datalist class="contractorlist"  id="contractorid{{ $proprogetdat->id }}">
                                                                       @foreach($contractorarray as $contractor)
                                                                           <option  value="{{ $contractor->id }}"  data-value="{{ $contractor->name }}" >{{ $contractor->name }}</option>
                                                                       @endforeach
                                                                   </datalist>
                                                               </td>
                                                               <?php   break;
                                                               case 'startdate': ?>
                                                                   <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject" data-item="start_date" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->start_date) ? date('d-m-Y',strtotime($proprogetdat->start_date)) : '' }}"></td>
                                                                   <?php   break;
                                                               case 'enddate': ?>
                                                               <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject" data-item="deadline" data-itemid="{{ $proprogetdat->id }}"   value="{{ !empty($proprogetdat->deadline) ? date('d-m-Y',strtotime($proprogetdat->deadline)) : '' }}"></td>
                                                               <?php   break;
                                                               case 'rate': ?>
                                                               <td style="position:relative;"><a href="javascript:;" class="open-rate-sheet" title="Open Rate Sheet" data-toggle="tooltip" data-id="{{ $proprogetdat->id }}"><i class="fa fa-ellipsis-v"></i></a>
                                                                   <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject ratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-cat="{{ $catitem }}"  data-item="rate" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->rate) ? $proprogetdat->rate : 0 }}">
                                                               </td>
                                                               <?php   break;
                                                               case 'qty': ?>
                                                               <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject qty{{ $proprogetdat->id }}" data-cat="{{ $catitem }}"  data-itemid="{{ $proprogetdat->id }}" data-item="qty"  onchange="calmarkup({{ $proprogetdat->id }})" value="{{ !empty($proprogetdat->qty) ? $proprogetdat->qty : '' }}"></td>
                                                                 <?php   break;
                                                               case 'unit': ?>
                                                               <td>
                                                                   <input  class="cell-inp updateproject"  <?php if($colposition->collock=='1'){ echo 'disabled';}?> data-item="unit" data-itemid="{{ $proprogetdat->id }}" list="unitdata{{ $proprogetdat->id }}"  value="{{ get_unit_name($proprogetdat->unit) }}">
                                                                   <datalist id="unitdata{{ $proprogetdat->id }}">
                                                                       @foreach($unitsarray as $units)
                                                                           <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                                                       @endforeach
                                                                   </datalist>
                                                               </td>
                                                              <?php   break;
                                                               case 'worktype': ?>
                                                                   <td> <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject" data-item="worktype" data-itemid="{{ $proprogetdat->id }}" list="type{{ $proprogetdat->id }}"  value="{{ get_work_type_name($proprogetdat->worktype) }}">
                                                                       <datalist id="type{{ $proprogetdat->id }}">
                                                                           @foreach($typesarray as $types)
                                                                               <option data-value="{{ $types->id }}" >{{ ucwords($types->title) }}</option>
                                                                           @endforeach
                                                                       </datalist>
                                                                   </td>
                                                               <?php   break;
                                                               case 'markuptype': ?>
                                                               <td>
                                                                   <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onchange="calmarkup({{ $proprogetdat->id }})" data-cat="{{ $catitem }}"  class="updateproject cell-inp markuptype{{ $proprogetdat->id }}" data-item="markuptype" data-itemid="{{ $proprogetdat->id }}" list="markuptype{{ $proprogetdat->id }}"  value="{{ $proprogetdat->markuptype }}">
                                                                   <datalist id="markuptype{{ $proprogetdat->id }}">
                                                                       <option value="percent">Percent</option>
                                                                       <option value="amt">Amount</option>
                                                                   </datalist>
                                                               </td>
                                                               <?php   break;
                                                               case 'markupvalue': ?>
                                                                    <td><input  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  type="text" class="updateproject cell-inp markupvalue{{ $proprogetdat->id }}" data-item="markupvalue"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="markupvalue" onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->markupvalue) ? $proprogetdat->markupvalue : '' }}" ></td>
                                                                <?php   break;
                                                               case 'adjustment': ?>
                                                       <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp adjustment{{ $proprogetdat->id }} adjustcat{{ $catitem }}" data-item="adjustment"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="adjustment" onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->adjustment) ? $proprogetdat->adjustment : '' }}" ></td>
                                                   <?php   break;
                                                               case 'finalrate': ?>
                                                       <td><input type="text"   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp totalrate{{ $proprogetdat->id }} finalrate{{ $catitem }}" data-item="finalrate"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="finalrate"   value="{{ !empty($proprogetdat->finalrate) ? $proprogetdat->finalrate : '' }}" ></td>
                                                   <?php   break;
                                                               case 'totalamount': ?>
                                                       <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp grandvalue totalamount{{ $proprogetdat->id }} finalamount{{ $catitem }}" data-item="totalamount" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" name="finalamount"  value="{{ !empty($proprogetdat->finalamount) ? $proprogetdat->finalamount : '' }}"  ></td>
                                                   <?php   break;
                                                           }
                                                       }?>
                                                   </tr>
                                          <?php  $cattotalamt += $proprogetdat->finalamount;
                                                } } ?>
                                                   <tr data-depth="2" class="collpse level2">
                                                       <td></td>
                                                       <td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                                       <td>
                                                           <div style="position:relative;">
                                                           <a class="context-menu context-menu-cost-item" data-toggle="tooltip" title="Add New Issue" ><i class="fa fa-ellipsis-v"></i></a>
                                                          <input  class="cell-inp costitemrow" data-cat="{{ $catvalue }}" data-positionid="{{ $propolevel1->id }}" list="costitemcat{{ $catdata }}" placeholder="Cost item">
                                                           <datalist class="costitemslist"  id="costitemcat{{ $catdata }}">
                                                               @foreach($costitemslist as $costitem)
                                                                   <option value="{{ $costitem->cost_item_name }}" data-id="{{ $costitem->id }}"  >{{ $costitem->cost_item_name }}</option>
                                                               @endforeach
                                                           </datalist></div>
                                                       </td>
                                                       <td><input type="text" class="cell-inp" name="desc" ></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td>
                                                           <input  class="cell-inp" list="unitdatacat{{ $catdata }}" >
                                                           <datalist id="unitdatacat{{ $catdata }}">
                                                               @foreach($unitsarray as $units)
                                                                   <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                                               @endforeach
                                                           </datalist>
                                                       </td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input  class="cell-inp" list="typecat{{ $catdata }}">
                                                           <datalist id="typecat{{ $catdata }}">
                                                               @foreach($typesarray as $types)
                                                                   <option data-value="{{ $types->id }}" >{{ ucwords($types->title) }}</option>
                                                               @endforeach
                                                           </datalist>
                                                       </td>
                                                       <td>
                                                           <input  onchange="calmarkup({{ $catdata }})"  class="cell-inp markuptypecat{{ $catdata }}" list="markuptypecat{{ $catdata }}">
                                                           <datalist id="markuptypecat{{ $catdata }}">
                                                               <option value="percent">Percent</option>
                                                               <option value="amt">Amount</option>
                                                           </datalist>
                                                       </td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                       <td><input type="text" class="cell-inp"></td>
                                                   </tr>
                                                   <tr data-depth="2" class="collpse level2" style="background-color: #efefef;">
                                                       <td colspan="4"></td>
                                                       <td><strong> Sub Total</strong></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td></td>
                                                       <td class="catotal{{ $catitem }}" style="font-weight: bold;">₹{{ number_format($cattotalamt,2) }}</td>
                                                   </tr>
                                       <?php $catdata++;
                                            $grandtotal += $cattotalamt;
                                            ?>
                                        @endforeach
                                @endif
                                    <!--  Level 2 add new category -->
                                    <tr  data-depth="1" class="collpse level1" >
                                        <td></td>
                                        <td colspan="3"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;"> <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Category" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemlevel1category" data-parent="{{ $propro->itemid }}"  list="costitemlevel1" placeholder="Category">
                                            <datalist  id="costitemlevel1">
                                                @foreach($categories as $category)
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                @endforeach
                                            </datalist></div></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr data-depth="1" class="collpse level1">
                                        <td></td>
                                        <td colspan="3"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-cost-item" data-toggle="tooltip" title="Add New Category" ><i class="fa fa-ellipsis-v"></i></a>
                                                <input  class="cell-inp costitemrow" data-cattype="single"  data-positionid="{{ $propro->id }}"  data-cat="{{ $propro->itemid }}" list="costitemcat{{ $catdata }}" placeholder="Cost item">
                                            <datalist class="costitemslist"  id="costitemcat{{ $catdata }}">
                                                @foreach($costitemslist as $costitem)
                                                    <option value="{{ $costitem->cost_item_name }}" data-id="{{ $costitem->id }}"  >{{ $costitem->cost_item_name }}</option>
                                                @endforeach
                                            </datalist></div>
                                        </td>
                                        <td><input type="text" class="cell-inp" name="desc" ></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td>
                                            <input  class="cell-inp" list="unitdatacat{{ $catdata }}" >
                                            <datalist id="unitdatacat{{ $catdata }}">
                                                @foreach($unitsarray as $units)
                                                    <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                                @endforeach
                                            </datalist>
                                        </td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input  class="cell-inp" list="typecat{{ $catdata }}">
                                            <datalist id="typecat{{ $catdata }}">
                                                @foreach($typesarray as $types)
                                                    <option data-value="{{ $types->id }}" >{{ ucwords($types->title) }}</option>
                                                @endforeach
                                            </datalist>
                                        </td>
                                        <td>
                                            <input  onchange="calmarkup({{ $catdata }})"  class="cell-inp markuptypecat{{ $catdata }}" list="markuptypecat{{ $catdata }}">
                                            <datalist id="markuptypecat{{ $catdata }}">
                                                <option value="percent">Percent</option>
                                                <option value="amt">Amount</option>
                                            </datalist>
                                        </td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                        <td><input type="text" class="cell-inp"></td>
                                    </tr>
                                <tr data-depth="1" class="collpse level1" style="background-color: #efefef;">
                                    <td colspan="5">
                                        <strong> Sub Total</strong></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="catotal{{ $catitem }}" style="font-weight: bold;">₹{{ number_format($cattotalamt,2) }}</td>
                                </tr>
                                <?php $acc_id++;
                                } ?>
                                    <tr  data-depth="0" class="collpse level0" >
                                        <td></td>
                                        <td colspan="4">
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Category" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemcategory" list="costitemcatitem" placeholder="Category">
                                            <datalist id="costitemcatitem">
                                                @foreach($categories as $category)
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                @endforeach
                                            </datalist>
                                            </div></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr  data-depth="0" class="maincat collpse level0" >
                                        <td></td>
                                        <td colspan="4"><strong> Grand Total</strong></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="grandtotal" style="font-weight: bold;">₹{{ number_format($grandtotal,2) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="rate-sheet rate-formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                    <div class="combo-sheet formula-sheet">
                        <div class="loaderx">
                            <div class="cssload-speeding-wheel"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- .row -->
    {{--Ajax Modal--}}

    <div class="modal fade bs-modal-md in" id="lockModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ url('admin/projects/projectBoqLock') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Lock Tables</span>
                </div>
                <div class="modal-body">
                    <div class="row responsive">
                        <div class="col-md-6">
                            <h3>Columns</h3>
                            <?php $rowpostitioncolms = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->orderBy('inc','asc')->get();
                                foreach($rowpostitioncolms as $rowpostition){
                            ?>
                            <div class="">
                                <label><input type="checkbox" value="1" name="lockcolms[{{ $rowpostition->id }}]" <?php if($rowpostition->collock==1){ echo 'checked';}?> > {{ $rowpostition->itemname }}</label>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="projectid" value="{{ $id }}"/>
                    <input type="hidden" name="title" value="{{ $title }}"/>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-success">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <div class="modal fade bs-modal-md in" id="categoryModalPop" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    <div class="modal fade bs-modal-md in" id="CostitemsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form id="costItemsUpdate" action="" method="post" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" >Add Cost item</span>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label>Cost item Name</label>
                            <input type="text" class="form-control" name="title" placeholder="Cost item">
                        </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <input type="hidden" name="category" id="costaddcat">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');
    </script>
    <script>
        "use strict";
        function textAreaAdjust(element) {
            element.style.height = "1px";
            element.style.height = (25+element.scrollHeight)+"px";
        }
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('costitem-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Cost item!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.costitem-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $("#costitem"+id).remove();
                                }
                            }
                        });
                    }
                });
            });
            $('body').on('click', '.sa-params-cat', function(){
                var id = $(this).data('position-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted Category!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.projects.costitem-cat-destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $("#costitem"+id).remove();
                                }
                            }
                        });
                    }
                });
            });


        });

        function loadTable() {
            table = $('#users-table').resizableColumns({
                responsive: true,
                processing: true,
                destroy: true,
                stateSave: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                }
            });
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });
            $('#boqtable').on('click', '.opntoggle', function () {
                //Gets all <tr>'s  of greater depth below element in the table
                var findChildren = function (tr) {
                    var depth = tr.data('depth');
                    return tr.nextUntil($('tr').filter(function () {
                        return $(this).data('depth') <= depth;
                    }));
                };
                var el = $(this);
                var tr = el.closest('tr'); //Get <tr> parent of toggle button
                var children = findChildren(tr);

                //Remove already collapsed nodes from children so that we don't
                //make them visible.
                //(Confused? Remove this code and close Item 2, close Item 1
                //then open Item 1 again, then you will understand)
                var subnodes = children.filter('.expand');
                subnodes.each(function () {
                    var subnode = $(this);
                    var subnodeChildren = findChildren(subnode);
                    children = children.not(subnodeChildren);
                });
                //Change icon and hide/show children
                if (tr.hasClass('collpse')) {
                    tr.removeClass('collpse').addClass('expand');
                    children.hide();
                } else {
                    tr.removeClass('expand').addClass('collpse');
                    children.show();
                }
                return children;
            });
            function calmarkup(costitem){
                var calvalue = 0;
                var  percentvalue = 0;
                var qty = parseInt($(".qty"+costitem).val());
                var rate = parseInt($(".ratevalue"+costitem).val());
                var catitem =  $(".ratevalue"+costitem).data('cat');
                calvalue += rate;
                var marktype = $(".markuptype"+costitem).val();
                var markvalue = parseInt($(".markupvalue"+costitem).val());
                if(markvalue>0){
                    if(marktype=='percent'){
                        percentvalue = (markvalue/100)*rate;
                        calvalue += percentvalue;
                    }
                    if(marktype=='amt'){
                        calvalue += markvalue;
                    }
                }
                $(".totalrate"+costitem).val(calvalue);
                var itemarray = [];
                itemarray['qty'] = qty;
                itemarray['rate'] = rate;
                itemarray['markuptype'] = marktype;
                itemarray['markupvalue'] = markvalue;
                itemarray['finalrate'] = calvalue;
                var token = '{{ csrf_token() }}';
                $.ajax({
                    url: '{{ url('admin/projects/updateboqcostitem') }}',
                    method: 'POST',
                    data: {
                        '_token':token,
                        'projectid':'{{ $id }}',
                        'title':'{{ $title }}',
                        itemarray:itemarray,
                        itemid:costitem
                    }
                });

                var adjustment = $(".adjustment"+costitem).val();
                var famt = calvalue*qty;
                var famt = famt-adjustment;
                $(".totalamount"+costitem).val(famt);
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".adjustment"+catitem).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".finalamount"+catitem).each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        sum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".catotal"+catitem).html("₹"+sum.toFixed(2));
                var tosum = 0;
                //iterate through each textboxes and add the values
                $(".grandvalue").each(function() {
                    //add only if the value is number
                    if(!isNaN(this.value) && this.value.length!=0) {
                        tosum += parseFloat(this.value);
                    }
                });
                //.toFixed() method will roundoff the final sum to 2 decimal places
                $(".grandtotal").html("₹"+tosum.toFixed(2));
            }

            $(".costitemcategory").change(function () {
                var cat =  $(this).val();
                var catid =    $("#costitemcatitem option[value='" + cat + "']").attr('data-value');
                if(catid=='add'){
                    var url = '{{ route('admin.activity.create')}}';
                    $('#modelHeading').html("Create New Category");
                    $.ajaxModal('#categoryModalPop', url);
                }else{
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var trindex = $(this).closest('tr').index();
                     var token = "{{ csrf_token() }}";
                     $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.addcostitemcategory') }}",
                        data: {'_token': token,'category': cat, 'catid': catid, 'projectid': projectid, 'titleid': titleid, 'level': '0', 'parent': '0'},
                        success: function(data){
                            window.location.reload();
                            if(trindex>0){
                                $("#boqtable > tbody > tr").eq(trindex-1).after(data);
                            }else{
                                $(".colposition").prepend(data);
                            }
                            $(".costitemcategory").val("");
                        }
                    });
                }
            });
            $(".costitemlevel1category").change(function () {
                var cat =  $(this).val();
                var catparent =  $(this).attr('data-parent');
                var catid =    $("#costitemlevel1 option[value='" + cat + "']").attr('data-value');
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var trindex = $(this).closest('tr').index();
                if(catid=='add'){
                    var url = '{{ route('admin.activity.create')}}';
                    $('#modelHeading').html("Create New Category");
                    $.ajaxModal('#categoryModalPop', url);
                }else{
                    if(cat){
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: "POST",
                            url: "{{ route('admin.projects.addcostitemcategory') }}",
                            data: {'_token': token,'category': cat, 'catid': catid, 'projectid': projectid, 'titleid': titleid, 'level': '1', 'parent': catparent},
                            success: function(data){
                                /*window.location.reload();*/
                                if(trindex>0){
                                    $("#boqtable > tbody > tr").eq(trindex-1).after(data);
                                }else{
                                    $(".colposition").prepend(data);
                                }
                                $(".costitemcategory").val("");
                            }
                        });
                    }
                }
            });
            $(".context-menu-cost-item").click(function(){
                $('#CostitemsModal').modal('show');
            });
            $(".context-menu-category").click(function(){
                var url = '{{ route('admin.activity.create')}}';
                $('#modelHeading').html("Create New Category");
                $.ajaxModal('#categoryModalPop', url);
            });
            $(".costitemrow").change(function () {
                var value = $(this).val();
                var cat = $(this).data('cat');
                var cattype = $(this).data('cattype');
                var list = $(this).attr('list');
               var positionid =    $(this).attr('data-positionid');
               var itemid =    $("#"+list+" option[value='" + value + "']").attr('data-id');
                if(itemid=='add'){
                    $('#costaddcat').val(cat);
                    $('#CostitemsModal').modal('show');
                }else{
                var projectid = '{{ $id }}';
                var titleid = '{{ $title }}';
                var trindex = $(this).closest('tr').index();
                if(cattype=='single'){
                    var catitem = $("tr.catitem"+cat).last().index();
                    trindex = catitem;
                    var catrow = $("tr.catrow"+cat).last().index();
                    if(catrow>0){
                        trindex = catrow;
                    }
                }else{
                    trindex = trindex-1;
                }
                    if(value){
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: "POST",
                            url: "{{ route('admin.projects.addcostitemrow') }}",
                            data: {'_token': token, 'itemid': itemid, 'positionid': positionid,  'value': value, 'category': cat, 'projectid': projectid, 'titleid': titleid, 'cattype': cattype},
                            success: function(data){
                                /!*window.location.reload();*!/
                                $("#boqtable > tbody > tr").eq(trindex).after(data);
                                $(".costitemrow").val("");
                            }
                        });
                    }
                }
            });
        $(function () {
            $(".boqtable").sortable({
                items: 'tr:not(thead>tr)',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                update: function (event, ui) {

                },
                stop: function (e, ui) {
                    var costitemarray = [];
                    var level1catarray = [];
                    var token = '{{ csrf_token() }}';
                    ui.item.removeClass("selected");
                    $('.colposition>tr').each(function (index) {
                        var costitemrow = $(this).attr('data-costitemrow');
                        if(costitemrow){
                            costitemarray.push(costitemrow);
                        }
                        var level1cat = $(this).attr('data-level1cat');
                        if(level1cat){
                            level1catarray.push(level1cat);
                        }
                    });
                    if(costitemarray){
                        $.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                position:costitemarray
                            },
                            type: 'POST',
                            url: '{{ url('admin/projects/boqcostitemchangeposition') }}'
                        });
                    }
                    if(level1catarray){
                        $.ajax({
                            data: {
                                '_token':token,
                                'projectid':'{{ $id }}',
                                'title':'{{ $title }}',
                                position:level1catarray
                            },
                            type: 'POST',
                            url: '{{ url('admin/projects/boqcatchangeposition') }}'
                        });
                    }
                }
            });
            $('.boqtable').dragtable({persistState: function(table) {
                var token = '{{ csrf_token() }}';
                var selectarray = [];
                    table.sortOrder['_token']=token;
                    table.el.find('th').each(function(i) {
                        var col = $(this).attr("col");
                        if(col) {
                            selectarray.push(col);
                        }
                    });
                    $.ajax({
                        url: '{{ url('admin/projects/boqcolchangeposition') }}',
                        method: 'POST',
                        data: {
                            '_token':token,
                            'projectid':'{{ $id }}',
                            'title':'{{ $title }}',
                            position:selectarray
                        },
                    });
                }
            });
            $(document).on("change",".updateproject",function(){
                var token = '{{ csrf_token() }}';
                var item = $(this).data('item');
                var itemid = $(this).data('itemid');
                var itemvalue = $(this).val();
                if(item=='assign_to'){
                    var itvalue = $('#assign'+itemid).find('option[value="' +itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                if(item=='contractor'){
                    var itvalue = $('#contractorid'+itemid).find('option[value="' +itemvalue + '"]').attr('data-value');
                    $(this).val(itvalue);
                }
                $.ajax({
                    url: '{{ url('admin/projects/updateboqcostitem') }}',
                    method: 'POST',
                    data: {
                        '_token':token,
                        'projectid':'{{ $id }}',
                        'title':'{{ $title }}',
                        item:item,
                        itemid:itemid,
                        itemvalue:itemvalue
                    }
                });
            });
        });
        $('#costItemsUpdate').submit(function (e) {
            e.preventDefault();
            var costaddcat = $("#costaddcat").val();
            $.ajax({
                url: '{{url('admin/cost-items/name-store')}}',
                container: '#costItemsUpdate',
                type: "POST",
                data: $('#costItemsUpdate').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        /*window.location.reload();*/
                        $("#CostitemsModal").modal('hide');
                        costitemlist();
                    }
                }
            })
        });
        function costitemlist(){
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{url('admin/cost-items/cost-item-list')}}',
                type: "POST",
                data: { '_token':token},
                success: function (response) {
                        $(".costitemslist").html("");
                        $(".costitemslist").html(response);
                }
            })
        }
        var cTable = null;
        $(document).on('click', '.open-rate-sheet', function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.projects.rate-sheet', [':id']) }}';
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                data: {_token: '{{ csrf_token() }}'},
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
    '                        <div class="cssload-speeding-wheel"></div>' +
    '                    </div>';
                    $('.rate-formula-sheet').html(html).show();
                },
                success: function(data){
                    $('.rate-formula-sheet').html(data);
                    ctable = $('#rate-table').resizableColumns({
                        responsive: true,
                        processing: true,
                        destroy: true,
                        stateSave: true,
                        language: {
                            "url": "<?php echo __("app.datatable") ?>"
                        },
                        "fnDrawCallback": function (oSettings) {
                            $("body").tooltip({
                                selector: '[data-toggle="tooltip"]'
                            });
                        }
                    });
                }
            })
        });
        $(document).on('click', '.close-rate-sheet', function(){
            $('.rate-formula-sheet').html('').hide();
        });
        var cTable = null;
        $(document).on('click', '.combosheetbutton', function(){
            var id = $(this).attr('data-resource');
            if(id){
                var url = '{{ route('admin.resources.getComboSheet', [':id']) }}';
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                    beforeSend: function(){
                        var html = '<div class="loaderx">' +
                            '                        <div class="cssload-speeding-wheel"></div>' +
                            '                    </div>';
                        $('.rate-formula-sheet').hide();
                        $('.formula-sheet').html(html).show();
                    },
                    success: function(data){
                        $('.formula-sheet').html(data);
                        ctable = $('#combo-table').resizableColumns({
                            responsive: true,
                            processing: true,
                            destroy: true,
                            stateSave: true,
                            language: {
                                "url": "<?php echo __("app.datatable") ?>"
                            },
                            "fnDrawCallback": function (oSettings) {
                                $("body").tooltip({
                                    selector: '[data-toggle="tooltip"]'
                                });
                            }
                        });
                    }
                })

            }
        });
        $(document).on('click', '.close-combo-sheet', function(){
            $('.formula-sheet').html('').hide();
            $('.rate-formula-sheet').show();
        })

    </script>
@endpush