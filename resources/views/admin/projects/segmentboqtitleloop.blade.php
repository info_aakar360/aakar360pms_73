<?php
/* Level 0 category */
function boqhtml($boqarray){
$user = $boqarray['user'];
$proproget = $boqarray['proproget'];
$userarray = $boqarray['userarray'];
$projectid = $boqarray['projectid'];
$subprojectid = $boqarray['subprojectid'];
$segmentid = $boqarray['segmentid'];
$contractorarray = $boqarray['contractorarray'];
$colpositionarray =$boqarray['colpositionarray'];
$unitsarray =$boqarray['unitsarray'];
$typesarray = $boqarray['typesarray'];
$categories = $boqarray['categories'];
$catvalue = $boqarray['catvalue'];
$parent = $boqarray['parent'];
$level = $boqarray['level'];
$subtotcat = $boqarray['subtotcat'];
$snorow = $boqarray['snorow'];

foreach ($proproget as $propro){
$cattotalamt = 0;
if(!empty($propro->catlevel)){
    $catvalue =  $propro->catlevel.','.$propro->itemid;
}else{
    if(!empty($propro->parent)){
        $catvalue =  $propro->parent.','.$propro->itemid;
    }else{
        $catvalue = (string)$propro->itemid;
    }
}
$level = (int)$propro->level;
$levelname = '';
$levelname = 'level'.$level;
$newlevel = $level+1;
$parent = $propro->itemid;
$catitem = $propro->itemid;
if($level==0){
    $subtotcat = $propro->itemid;
}

if((int)$propro->level==0){
    $snorow = $snorow+1;
}else{
    $snorow = $snorow.'.'.$propro->level;
}
?>
<tr data-level1cat="{{ $propro->id }}" data-depth="0" class="maincat {{ $levelname }} collpse catitem{{ $catvalue }}">
    <td align="left">{{ $snorow }}</td>
    <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="{{ $propro->id }}" data-level="{{ $level }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
    <td class="text-center"><a href="javascript:void(0);" class="opntoggle iconcode <?php if($level>0){ echo 'dotlevel'; }else{ echo 'dotlevel1'; }?>">{!! get_dots_by_level($level) !!}</a></td>
    <td ><?php if (isset($propro->itemname)){ echo $propro->itemname; } ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<?php
$proprogetdatarra = \App\ProjectSegmentsProduct::where('segment',$segmentid)->where('title',$subprojectid)->where('project_id',$projectid)->where('position_id',$propro->id)->orderBy('inc','asc')->get();
$costitemslist = \App\ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('position_id',$propro->productposition_id)->orderBy('inc','asc')->get();

$tid =1;
foreach ($proprogetdatarra as $proprogetdat){
if(!empty($proprogetdat->id)){
$proprogetdat->position_id = $propro->id;
$proprogetdat->save();
?>
<tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
    <td align="left">{{ $snorow.'.'.$tid }}</td>
    <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="{{ $proprogetdat->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
    <td class="text-center"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
    <?php foreach ($colpositionarray as $colposition){
    switch($colposition->itemslug){
    case 'task': ?>
    <td><input  class="cell-inp updateproject" data-item="cost_items_id" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="costitem{{ $tid.$proprogetdat->id }}"  value="{{ get_cost_name($proprogetdat->cost_items_id) }}">
        <datalist class="costitemslist"  id="costitem{{ $tid.$proprogetdat->id }}">
            <?php foreach($costitemslist as $costitem){

                ?>
            <option value="{{ $costitem->id }}" >{{ get_cost_name($costitem->cost_items_id) }}</option>
            <?php  } ?>
        </datalist>
    </td>
    <?php   break;
    case 'description': ?>
    <td><textarea  class="cell-inp updateproject"  data-item="description" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onkeydown="textAreaAdjust(this)" style="height: 25px;">{{ !empty($proprogetdat->description) ? $proprogetdat->description : '' }}</textarea></td>
    <?php   break;
    case 'assign_to': ?>
    <td><input  class="cell-inp updateproject" data-item="assign_to" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="assign{{ $proprogetdat->id }}"  value="{{ get_employee_name($proprogetdat->assign_to) }}">
        <datalist class="assignlist"  id="assign{{ $proprogetdat->id }}">
            <?php foreach($userarray as $assign){ ?>
            <option  value="{{ $assign->id }}" data-value="{{ $assign->name }}" >{{ $assign->name }}</option>
            <?php  } ?>
        </datalist>
    </td>
    <?php   break;
    case 'contractor': ?>
    <td><input  class="cell-inp updateproject" data-item="contractor" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="contractorid{{ $proprogetdat->id }}"  value="{{ get_contractors_name($proprogetdat->contractor) }}">
        <datalist class="contractorlist"  id="contractorid{{ $proprogetdat->id }}">
            <?php foreach($contractorarray as $contractor){ ?>
            <option  value="{{ $contractor->id }}"  data-value="{{ $contractor->name }}">{{ $contractor->name }}</option>
            <?php  } ?>
        </datalist>
    </td>
    <?php   break;
    case 'startdate': ?>
    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="start_date" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->start_date) ? date('d-m-Y',strtotime($proprogetdat->start_date)) : '' }}"></td>
    <?php   break;
    case 'enddate': ?>
    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="deadline" data-itemid="{{ $proprogetdat->id }}"   value="{{ !empty($proprogetdat->deadline) ? date('d-m-Y',strtotime($proprogetdat->deadline)) : '' }}"></td>
    <?php   break;
    case 'rate': ?>
    <td style="position:relative;">
        {{--<a href="javascript:;" class="open-rate-sheet" title="Open Rate Sheet" data-toggle="tooltip" data-id="{{ $proprogetdat->id }}"><i class="fa fa-ellipsis-v"></i></a>--}}
        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject ratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-cat="{{ $catitem }}"  data-item="rate" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->rate) ? $proprogetdat->rate : 0 }}">
    </td>
    <?php   break;
    case 'qty': ?>
    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject qty{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})"   data-subtotcat="{{ $subtotcat }}"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-item="qty" value="{{ !empty($proprogetdat->qty) ? $proprogetdat->qty : '' }}"></td>
    <?php   break;
    case 'totalqty': ?>
    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject totalqty{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-item="totalqty" value="{{ !empty($proprogetdat->totalqty) ? $proprogetdat->totalqty : '' }}"></td>
    <?php   break;
    case 'unit':?>
    <td>
        <input  class="cell-inp updateproject"  <?php if($colposition->collock=='1'){ echo 'disabled';}?> data-item="unit" data-itemid="{{ $proprogetdat->id }}" list="unitdata{{ $proprogetdat->id }}"   value="{{ get_unit_name($proprogetdat->unit) }}">
        <datalist id="unitdata{{ $proprogetdat->id }}">
            <?php foreach($unitsarray as $units){ ?>
            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
            <?php  } ?>
        </datalist>
    </td>
    <?php   break;
    case 'finalrate': ?>
    <td><input type="text"   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp totalrate{{ $proprogetdat->id }} finalrate{{ $catitem }}" data-item="finalrate"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="finalrate"   value="{{ !empty($proprogetdat->finalrate) ? $proprogetdat->finalrate : '' }}" ></td>
    <?php   break;
    case 'totalamount': ?>
    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp grandvalue totalamount{{ $proprogetdat->id }} finalamount{{ $catitem }} subtotalamount{{ $subtotcat }}" data-item="totalamount" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" name="finalamount"  value="{{ !empty($proprogetdat->finalamount) ? $proprogetdat->finalamount : '' }}"  ></td>
    <?php   break;
   }
    }?>
</tr>
<?php
$cattotalamt +=$proprogetdat->finalamount;
    $tid++; } }
$newlevel = $level+1;
$parent = $propro->id;

$proprogetlevel1 = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();
$boqarray = array();
$boqarray['user'] = $user;
$boqarray['proproget'] = $proprogetlevel1;
$boqarray['userarray'] = $userarray;
$boqarray['projectid'] = $projectid;
$boqarray['subprojectid'] = (int)$subprojectid ?: 0;
$boqarray['segmentid'] = (int)$segmentid ?: 0;
$boqarray['contractorarray'] = $contractorarray;
$boqarray['colpositionarray'] = $colpositionarray;
$boqarray['unitsarray'] = $unitsarray;
$boqarray['typesarray'] = $typesarray;
$boqarray['categories'] = $categories;
$boqarray['catvalue'] = $catvalue;
$boqarray['level'] = $newlevel;
$boqarray['parent'] = $parent;
$boqarray['subtotcat'] = $subtotcat;
$boqarray['cattotalamt'] = $cattotalamt;
$boqarray['snorow'] = $snorow;
echo boqhtml($boqarray);

$positionparent = $propro->productposition_id;
$procostpositions = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$newlevel)->where('parent',$positionparent)->orderBy('inc','asc')->get();
$costitemslist = \App\ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('position_id',$positionparent)->orderBy('inc','asc')->get();

?>
<tr  data-depth="1" class="collpse {{ $levelname }}" >
    <td></td>
    <td></td>
    <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
    <td>
        <div style="position:relative;"> <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i></a>
            <input  class="cell-inp costitemcategory"  data-catlevel="{{ $catvalue }}" data-level="{{ $newlevel }}" data-parent="{{ $propro->id }}"  list="costitemcatitem{{ $propro->id }}" placeholder="Activity">
            <datalist  id="costitemcatitem{{ $propro->id }}">
                <?php foreach($procostpositions as $procostpos){ ?>
                    <option value="{{ $procostpos->id }}" data-itemname="{{ $procostpos->itemname }}"  data-itemid="{{ $procostpos->itemid }}" >{{ $procostpos->itemname }}</option>
                <?php  } ?>
            </datalist>
        </div>
    </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr data-depth="1" class="collpse level{{ $newlevel }}">
    <td></td>
    <td></td>
    <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
    <td>
        <div style="position:relative;">
            <a class="context-menu context-menu-cost-item" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i></a>
            <input  class="cell-inp costitemrow" <?php if($level=='0'){ ?> data-cattype="single" <?php }?>  data-positionid="{{ $propro->id }}"  data-cat="{{ $catvalue }}" data-level="{{ $newlevel }}" list="costitemcat{{ $propro->id }}" placeholder="Task">
            <datalist class="costitemslist"  id="costitemcat{{ $propro->id }}">
                <?php foreach($costitemslist as $costitem){
                    $costname = get_cost_name($costitem->cost_items_id);
                    ?>
                <option value="{{ $costitem->id }}" >{{ $costname }}</option>
                <?php  } ?>
            </datalist></div>
    </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr data-depth="1" class="collpse level{{ $newlevel }}" style="background-color: #efefef;">
    <td></td>
    <td></td>
    <td></td>
    <td><strong> Sub Total</strong></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td class="catotal{{ $catitem }} subtotal{{ $subtotcat }}level{{ $level }}" data-level="{{ $level }}" style="font-weight: bold;">
        <?php
        if($level==0){
            $maxlevelamt = \App\ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('category','LIKE','%'.$subtotcat.'%')->orderBy('inc','asc')->sum('finalamount');
            echo '₹'.number_format($maxlevelamt,2);
        }else{
            echo '₹'.number_format($cattotalamt,2);
        }
        ?>
    </td>
</tr>
<?php
} }
$proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();
$contractorarray = \App\Employee::getAllContractors($user);
$colpositionarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','col')->orderBy('inc','asc')->get();

$boqarray = array();
$boqarray['user'] = $user;
$boqarray['proproget'] = $proproget;
$boqarray['userarray'] = $userarray;
$boqarray['projectid'] = $projectid;
$boqarray['subprojectid'] = $subprojectid ?: 0;
$boqarray['segmentid'] = $segmentid ?: 0;
$boqarray['contractorarray'] = $contractorarray;
$boqarray['colpositionarray'] = $colpositionarray;
$boqarray['unitsarray'] = $unitsarray;
$boqarray['typesarray'] = $typesarray;
$boqarray['categories'] = $categories;
$boqarray['subtotcat'] = '';
$boqarray['catvalue'] = '';
$boqarray['level'] = 0;
$boqarray['parent'] = 0;
$boqarray['snorow'] = 0;

echo  boqhtml($boqarray);

$procostpositions = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();


$grandtotal = \App\ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->sum('finalamount');
?>
<tr  data-depth="0" class="collpse" >
    <td></td>
    <td></td>
    <td></td>
    <td>
        <div style="position:relative;">
            <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i> </a>
            <input  class="cell-inp costitemcategory" list="costitemcatitem0" data-level="0" data-parent="0" placeholder="Activity">
            <datalist id="costitemcatitem0">
                <?php foreach($procostpositions as $procostpos){ ?>
                    <option value="{{ $procostpos->id }}" data-itemname="{{ $procostpos->itemname }}"  data-itemid="{{ $procostpos->itemid }}" >{{ $procostpos->itemname }}</option>
                <?php  } ?>
            </datalist>
        </div></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr  data-depth="0" class="maincat collpse level0" >
    <td></td>
    <td></td>
    <td></td>
    <td><strong> Grand Total</strong></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td class="grandtotal" style="font-weight: bold;">₹{{ !empty($grandtotal) ? number_format($grandtotal,2) : 0 }}</td>
</tr>