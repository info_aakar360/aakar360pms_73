
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="col-md-1 col-xs-2">
                    {!!  ($contractor->image) ? '<img src="'.get_employee_image_link($contractor->id).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset_url('user.png').'" alt="user" class="img-circle" width="40">' !!}
                </div>
                <div class="col-md-8 col-xs-6">
                    {{ ucwords($contractor->name) }} ( {{ get_manpower_category($contractor->category) }} )
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">

                    <?php if(count($workerslist)){
                    foreach ($workerslist as $row){ ?>
                        <div class="row">
                            <div class="form-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <p>{{ ucwords($row->name) }} ({{ get_manpower_category($row->category) }})</p>
                                                    <p>{{ $row->salary }} Per {{ $row->salarytype }} ({{ $row->workinghours }}) hr</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <a href=""><i class="fa fa-plus"></i></a>
                                                    <input type="text" class="form-control" value="1" />
                                                    <a href=""><i class="fa fa-minus"></i></a>
                                                </div>
                                                <div class="col-md-2">
                                                    <select class="form-control" name="shift" >
                                                        <option value="">Select Shift</option>
                                                        <option value="Day">Day</option>
                                                        <option value="Night">Night</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" value="1" readonly />
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:void(0);"><i class="fa fa-ellipsis-v"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-1">
                                                     <input type="text" class="form-control" value="1" readonly />
                                                </div>
                                                <div class="col-md-2">
                                                     <select class="form-control" name="shift" >
                                                         <option value="">Select Shift</option>
                                                         <option value="Day">Day</option>
                                                         <option value="Night">Night</option>
                                                     </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" value="1" readonly />
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:void(0);"><i class="fa fa-ellipsis-v"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    <?php  } }?>

                </div>
            </div>
        </div>
    </div>
</div>