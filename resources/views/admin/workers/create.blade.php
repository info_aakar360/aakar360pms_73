@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.workers.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.workers.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createLabour','class'=>'ajax-form','method'=>'POST']) !!}
                        @if(isset($leadDetail->id))
                            <input type="hidden" name="lead" value="{{ $leadDetail->id }}">
                        @endif
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>@lang('modules.workers.worker')</label>
                                            <input type="text" name="name" id="name"   class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('app.category')</label>
                                            <select class="form-control" name="category">
                                                <option value="">Select @lang('app.category')</option>
                                                <?php if(count($manpowercategoryarray)>0){
                                                foreach ($manpowercategoryarray as $manpowercategory){
                                                ?>
                                                <option value="<?php echo $manpowercategory->id?>">{{ $manpowercategory->title }}</option>
                                                <?php }}?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="project_id" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('app.contractor')</label>
                                            <select class="form-control" name="contractor" id="contractors">
                                                <option value="">Select Contractor</option>
                                                <?php if(count($contractorsarray)>0){
                                                foreach ($contractorsarray as $contractors){
                                                ?>
                                                <option value="<?php echo $contractors->id; ?>">{{ $contractors->name }}</option>
                                                <?php }}?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>@lang('app.workinghours')</label>
                                            <input type="text" name="workinghours" placeholder="@lang('app.workinghours')"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('app.salary')</label>
                                             <input type="text" name="salary" value="0" placeholder="@lang('app.salary')" class="form-control" />
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('app.salarytype')</label>
                                            <select name="salarytype" class="form-control" >
                                                <option value="">@lang('app.select') @lang('app.salarytype')</option>
                                                <option value="daily">@lang('app.daily')</option>
                                                <option value="weekly">@lang('app.weekly')</option>
                                                <option value="fifteendays">@lang('app.fifteendays')</option>
                                                <option value="monthly">@lang('app.monthly')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    $("#project_id").change(function () {
        var project = $("#project_id").val();
        if(project){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.workers.get-contractors') }}",
                data: {'_token': token,'projectid': project},
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function(titles){
                    $(".preloader-small").hide();
                    $("select#contractors").html("");
                    $("select#contractors").html(titles);
                }
            });
        }
    });
    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.workers.store')}}',
            container: '#createLabour',
            type: "POST",
            redirect: true,
            data: $('#createLabour').serialize()
        })
    });
</script>
@endpush

