<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><i class="icon-note"></i> @lang("modules.messages.startConversation")</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createChat','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body modalbox">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                        <input type="text" class="form-control" name="search" placeholder="Search.." />
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <?php if(count($employeearray)>0){
                    foreach ($employeearray as $employee){
                    ?>
                    <div class="col-md-12 timelineblock">
                        <div class="col-md-2">
                            <input type="checkbox" value="{{ $employee->user_id }}" class="employeeusers" @if(!empty($joinedusers)&&in_array($employee->user_id,$joinedusers)) checked @endif  />
                        </div>
                        <div class="col-md-2">
                            <img class="img-circle w-100"  src="{{ get_employee_image_link($employee->id) }}" />
                        </div>
                        <div class="col-md-8">
                            <div class="taskusername">{{ ucwords($employee->name) }} <br> <small>{{ ucwords($employee->user_type) }} </small></div>
                        </div>
                    </div>
                    <?php } }else{ ?>
                    <p>Members not found</p>
                    <?php }?>
                </div>
            </div>

        </div>
        <div class="form-actions text-right">
            <button type="button" id="post-group-message" class="btn btn-success"><i class="fa fa-send-o"></i> @lang("app.add")</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script>

    $('.select2').select2();

    $("input[name=user_type]").click(function () {
        if($(this).val() == 'client'){
            $('#member-list').hide();
            $('#client-list').show();
        }
        else{
            $('#client-list').hide();
            $('#member-list').show();
        }
    })

    $('#post-group-message').click(function () {

        var token = '{{ csrf_token() }}';
        var open = [];
        $.each($("input:checkbox[class='employeeusers']:checked"), function () {
            open.push($(this).val());
        });
        var page =0;
        var groupid = $("#groupchatid").val();
        var selectedusers = open.join(",");
        if(groupid){
            $.ajax({
                url: '{{route('admin.user-chat.group-chat-join-user')}}',
                type: "POST",
                data: {
                    '_token' : token,
                    'selectedusers' : selectedusers,
                    'groupid' : groupid
                },
                beforeSend:function(){
                    $("#loadingsymbol").show();
                },
                success: function(response){
                    $("#loadingsymbol").hide();
                    $("#newChatModal").modal('hide');
                    $('#usersmenulist').html("");
                    $('#usersmenulist').html(response.menulist);
                    getGroupChatBox(groupid);
                    if(page=='0'){
                        $('#pulseloop').html("");
                        $('#pulseloop').html(response.chatdata);
                    }else{
                        $('#pulseloop').prepend(response.chatdata);
                    }
                }
            });
        }else{
            alert('Group Not Found!!');
        }
    });
</script>