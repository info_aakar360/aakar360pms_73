<div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ uploads_url().'user.png' }}" />

    <h5 class="float-left" style="margin: 10px 0px 0px 10px;font-size: 15px;"> {{ !empty($group) ? ucwords($group->name) : 'NA' }}
    <br>
    <small>{{ !empty($employeenames) ? ucwords(implode(', ',$employeenames)) : '' }}</small> </h5>
    <div class="dropdown show">

        <a id="groupmenulink"  class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>


    </div>

</div>
<div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent" style="background-color: transparent;overflow-y: auto; height: 100vh;">


    <div id="pulseloop" >
        {!! $chatdata !!}
    </div>
    <div id="loadingsymbol" style="display: none;">
        <p>Loading please wait..</p>
    </div>


</div>

<div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">

    <div class="row send-chat-box">

              <div class="col-md-12  mt-10 " id="filebox" style="display: none;">
              <div class="  dropzone dropheight"  id="file-upload-dropzone" >
                    {{ csrf_field() }}
                    <div class="fallback">
                        <input name="image[]" type="file" multiple />
                    </div>
                </div>
                </div>
            <div class="col-md-12  mt-10">
                <form method="post" id="submitBtn" enctype="multipart/form-data" autocomplete="off">
                <div class="col-sm-11">
                    <div class="form-group">

                    <input type="text" name="message" id="submitTexts" autocomplete="off" placeholder="@lang("modules.messages.typeMessage")"
                           class="form-control ">
                    <div class="image_upload">
                        <a href="javascript:void(0);" class="uploadfile"><img src="{{ uploads_url().'cloud-upload-icon.png' }}" ></a>

                    </div>

                    <input id="groupchatid" value="{{ $group->id  }}" name="groupid" type="hidden"/>
                    <input id="chatmodule"  value="groupchat"  type="hidden"/>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="custom-send">
                        {{ csrf_field() }}
                        <button class="btn btn-danger btn-rounded" type="submit">@lang("modules.messages.send")
                        </button>

                    </div>
                    <div id="errorMessage"></div>
                </div>
                </form>
            </div>
    </div>

</div>
<div class="sidebarbox" style="display: none;" >
    <div class="col-md-11 mb-10">
        <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;margin-right:10px;" src="{{ uploads_url().'user.png' }}" />
        <h3>{{ ucwords($group->name) }}</h3>
    </div>
    <div class="col-md-1 mb-10 text-right">
        <a href="javascript:void(0);" onclick="closeSidebarview();"><i class="fa fa-times-circle"></i></a>
    </div>

    <div class="col-md-12 mb-10">
        <div class="col-md-6">
            <label><b>Members</b></label>
        </div>
        <div class="col-md-6">
            <a href="javascript:void(0);"  id="new-group-chat">Add New Member</a>
        </div>
        <div class="col-md-12">
            <?php if(count($groupemployeearray)>0){
                    foreach ($groupemployeearray as $employee){
                ?>
            <div class="col-md-11 timelineblock">
                <div class="col-md-2">
                    <img class="img-circle w-100"  src="{{ get_employee_image_link($employee->id) }}" />
                </div>
                <div class="col-md-8">
                    <div class="taskusername">{{ ucwords($employee->name) }} <br> <small>{{ ucwords($employee->user_type) }} </small></div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <?php } }else{ ?>
                <p>Members not found</p>
            <?php }?>
        </div>

    </div>
    <div class="timeline-list">

    </div>
</div>
<script>

    Dropzone.autoDiscover = true;
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.user-chat.storeImage') }}",
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        paramName: "image",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks: true,
        parallelUploads: 10,
        init: function () {
            let mydropzone = this; // closure

            this.on('success', function(file, response){

                $("#filebox").hide();
            });
        }
    });

    myDropzone.on('sending', function (file, xhr, formData) {
        var chatmodule = $('#chatmodule').val();
        var groupchatid = $('#groupchatid').val();
        formData.append('chatmodule', chatmodule);
        formData.append('groupchatid', groupchatid);
    });
    myDropzone.on("complete", function(file) {
        myDropzone.removeFile(file);
    });
</script>