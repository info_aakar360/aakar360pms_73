<?php if(count($employeearray)>0){
foreach ($employeearray as $employee){
?>
<div class="col-md-11 timelineblock">
    <div class="col-md-2">
        <input type="checkbox" value="{{ $employee->user_id }}" class="users[]" @if(!empty($joinedusers)&&in_array($employee->user_id,$joinedusers)) checked @endif  />
    </div>
    <div class="col-md-2">
        <img class="img-circle w-100"  src="{{ get_employee_image_link($employee->id) }}" />
    </div>
    <div class="col-md-8">
        <div class="taskusername">{{ ucwords($employee->name) }} <br> <small>{{ ucwords($employee->user_type) }} </small></div>
    </div>
</div>
<?php } }else{ ?>
<p>Members not found</p>
<?php }?>