@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang("app.menu.home")</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
   <style>

        .image_upload{
            position: absolute;
            top: 6px;
            right: 14px;
        }
        .image_upload >input {
            display: none;
        }
        .image_upload img{
            width: 24px;
            cursor: pointer;
        }

        .sidebarbox{
            background-color: #fff;
            padding: 10px;
            display: inline-block;
            width: 50%;
            height: 100vh;
            position: absolute;
            right: 0px;
        }
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

                                    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ get_users_image_link($user->id) }}" />

                                    <h5 class="float-left" style="margin: 10px 0px 0px 10px;font-size: 15px;"> {{ ucwords($user->name) }}</h5>

                                    <div class="dropdown show">

                                        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

                                        <div class="dropdown-menu dropdown-menu-right border" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item p-10 text-secondary" href="javascript:void(0);" id="new-chat" > <i class="fa fa-user m-1" aria-hidden="true"></i> Add Member </a>
                                            <a class="dropdown-item p-10 text-secondary" href="javascript:void(0);" onclick="createGroupChat();"> <i class="fa fa-user m-1" aria-hidden="true"></i> Create Group </a>

                                           {{-- <a class="dropdown-item p-10 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>

                                            <a class="dropdown-item p-10 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>--}}

                                        </div>
                                    </div>

                                </div>

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">

                                    <div id="sohleft" class="card border-0 m-0 p-0 position-relative bg-transparent" style="overflow-y: auto; height: 100vh;">

                                        <div class="panel-group" id="usersmenulist" >

                                            {!! $menulist !!}

                                        </div>

                                    </div>
                                </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div id="chattingbox" class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;background:url({{ asset_url('back-study-photo.png') }});">



                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- .row -->


    <div class="modal fade bs-modal-md in" id="newChatModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskActivityModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


<script type="text/javascript">

    $('.chat-left-inner > .chatonline').slimScroll({
        height: '100%',
        position: 'right',
        size: "0px",
        color: '#dcdcdc',

    });
    $(function () {
        $(window).load(function () { // On load
            $('.chat-list').css({'height': (($(window).height()) - 370) + 'px'});
        });
        $(window).resize(function () { // On resize
            $('.chat-list').css({'height': (($(window).height()) - 370) + 'px'});
        });
    });

    // this is for the left-aside-fix in content area with scroll

    $(function () {
        $(window).load(function () { // On load
            $('.chat-left-inner').css({
                'height': (($(window).height()) - 240) + 'px'
            });
        });
        $(window).resize(function () { // On resize
            $('.chat-left-inner').css({
                'height': (($(window).height()) - 240) + 'px'
            });
        });
    });



    $(".open-panel").click(function () {
        $(".chat-left-aside").toggleClass("open-pnl");
        $(".open-panel i").toggleClass("ti-angle-left");
    });

    $(function () {
        $('#userList').slimScroll({
            height: '350px'
        });
    });


    $('#submitTexts').keypress(function (e) {

        var key = e.which;
        if (key == 13)  // the enter key code
        {
            e.preventDefault();
            $('#submitBtn').click();
            return false;
        }
    });


    //submitting message
    $(document).on('submit', '#submitBtn',function (e) {
        e.preventDefault();
        //getting values by input fields
        var dpID = $('#dpID').val();
        var conversationid = $('#conversationid').val();
        var projectid = $("#projectid").val();
        var groupid = $("#groupchatid").val();
        var chatmodule = $("#chatmodule").val();
        var formdata = new FormData($('#submitBtn')[0]);
        var submitText = $('#submitTexts').val();
         if (dpID == '' || submitText == undefined) {
            $('#errorMessage').html('<div class="alert alert-danger"><p>@lang('messages.noUser ')</p></div>');
            return;
        } else {
             var token = "{{ csrf_token() }}";

             if(chatmodule=='chat'){

                var url = "{{ route('admin.user-chat.message-submit') }}";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if(myDropzone.getQueuedFiles().length > 0){
                            myDropzone.processQueue();
                        }
                        var blank = "";
                        $('#submitTexts').val(blank);
                    }
                });

             }
             if(chatmodule=='projectpulse'){
                 if(projectid){

                     var url = "{{ route('admin.user-chat.pulse-message-submit') }}";
                     $.ajax({
                         type: 'POST',
                         url: url,
                         data: formdata,
                         processData: false,
                         contentType: false,
                         success: function (response) {
                             $("#projectlogid").val(response);
                             if(myDropzone.getQueuedFiles().length > 0){
                                 myDropzone.processQueue();
                             }
                             var blank = "";
                             $('#submitTexts').val(blank);
                         }
                     });
                 }
             }
             if(chatmodule=='groupchat'){
                 if(groupid){
                     var url = "{{ route('admin.user-chat.group-chat-submit') }}";
                     $.ajax({
                         type: 'POST',
                         url: url,
                         data: formdata,
                         processData: false,
                         contentType: false,
                         success: function (response) {
                             if(myDropzone.getQueuedFiles().length > 0){
                                 myDropzone.processQueue();
                             }
                                 var blank = "";
                                 $('#submitTexts').val(blank);

                                 scrollChat();
                         }
                     });
                 }
             }
        }
        return false;
    });


    //getting all chat data according to user
    //submitting message
    $("#userSearch").keyup(function (e) {
        var url = "{{ route('admin.user-chat.user-search') }}";

        $.easyAjax({
            type: 'GET',
            url: url,
            messagePosition: '',
            data: {'term': this.value},
            container: ".userList",
            success: function (response) {
                //set messages in box
                $('.userList').html(response.userList);
            }
        });
    });

    function chatMenuList(){


        var url = "{{ route('admin.user-chat.chat-menu-list') }}";

        $.ajax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}'},
            success: function (response) {
                $('#usersmenulist').html("");
                $('#usersmenulist').html(response);
            }
        });
    }
    function getChatBox(userid){

        $("#chattingbox").html("");

        $(".chatonline li").removeClass('active');
        $("#userchat_"+userid).addClass('active');

        var url = "{{ route('admin.user-chat.chat-box') }}";

        $.ajax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}','userID': userid},
            success: function (response) {
                $('#chattingbox').html("");
                $('#chattingbox').html(response);
                scrollChat();
            }
        });
    }
    function getPulseProjectBox(projectid){

        $("#chattingbox").html("");

        $(".chatonline li").removeClass('active');
        $("#projectpulse_"+projectid).addClass('active');

        var url = "{{ route('admin.user-chat.pulse-chat-box') }}";

        $.ajax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}','projectid': projectid},
            success: function (response) {
                $('#chattingbox').html("");
                $('#chattingbox').html(response);
                scrollChat();
            }
        });
    }
    function getGroupChatBox(groupid){

        $("#chattingbox").html("");

        $(".chatonline li").removeClass('active');
        $("#groupchat_"+groupid).addClass('active');

        var url = "{{ route('admin.user-chat.groupchat-box') }}";

        $.ajax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}','groupid': groupid},
            success: function (response) {
                var blank = "";
                $('#submitTexts').val(blank);
                $('#chattingbox').html("");
                $('#chattingbox').html(response);
                scrollChat();
            }
        });
    }


    //getting all chat data according to user
    function getChatData(userID,id, dpName, scroll) {
        var getID = '';
        $('#errorMessage').html('');
        if (id != "" && id != undefined && id != null) {
            $('.userList li a.active ').removeClass('active');
            $('#dpa_' + id).addClass('active');
            $('#dpID').val(id);

            getID = id;
            $('#badge_' + id).val('');
        } else {
            $('.userList li:first-child a').addClass('active');
            getID = $('#dpID').val();
        }

        $("#chatmodule").val('chat');
        $("#pulseloop").hide();
        $("#chatbox").show();
        $(".chatonline li").removeClass('active');
        $("#userchat_"+id).addClass('active');

        var url = "{{ route('admin.user-chat.chat-messages') }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}','userID': getID},
            container: ".chats",
            success: function (response) {
                var blank = "";
                $('#submitTexts').val(blank);
                //set messages in box
                $('#usersmenulist').html("");
                $('#usersmenulist').html(response.menulist);
                $('.chats').html(response.chatData);
                $('#conversationid').val(response.conversionid);
            }
        });
    }
    function conversationchat(id){

        var url = "{{ route('admin.user-chat.conversation-chat-messages') }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': '{{ csrf_token() }}','conversationid': id},
            container: ".chats",
            success: function (response) {
                var blank = "";
                $('#submitTexts').val(blank);
                //set messages in box
                $('#usersmenulist').html("");
                $('#usersmenulist').html(response.menulist);
                $('.chats').html(response.chatData);
                $('#conversationid').val(response.conversionid);
                scrollChat();
            }
        });
    }
    function getProjectLogData(projectid) {
        $("#projectid").val(projectid);
        $(".chatonline li").removeClass('active');
        $("#projectpulse_"+projectid).addClass('active');
        $("#chatmodule").val('projectpulse');
        $("#pulseloop").show();
        $("#chatbox").hide();
        loadPulseHtml(0);
    }
    function  loadPulseHtml(page) {
        var token = '{{ csrf_token() }}';
        var projectid = $("#projectid").val();
        if(projectid){
            $.ajax({
                url: '{{route('admin.user-chat.pulse-data')}}',
                type: "POST",
                data: {
                    '_token' : token,
                    'projectid' : projectid,
                    'page' : page
                },
                beforeSend:function(){
                    $("#loadingsymbol").show();
                },
                success: function(response){
                    var blank = "";
                    $('#submitTexts').val(blank);
                    $("#loadingsymbol").hide();
                    $('#usersmenulist').html("");
                    $('#usersmenulist').html(response.menulist);

                    if(page=='0'){
                        $('#pulseloop').html("");
                        $('#pulseloop').html(response.chatdata);
                    }else{
                        $('#pulseloop').prepend(response.chatdata);
                    }
                    scrollChat();
                }
            });
        }
    }
    function getGroupChatData(groupid) {
        $("#groupchatid").val(groupid);
        $(".chatonline li").removeClass('active');
        $("#chatmodule").val('groupchat');
        $('#groupchat_' + groupid).addClass('active');
        $("#pulseloop").show();
        $("#chatbox").hide();
        loadGroupChatHtml(0);
    }
    function  loadGroupChatHtml(page) {
        var token = '{{ csrf_token() }}';
        var groupid = $("#groupchatid").val();
        if(groupid){
            $.ajax({
                url: '{{route('admin.user-chat.group-chat-data')}}',
                type: "POST",
                data: {
                    '_token' : token,
                    'groupid' : groupid,
                    'page' : page
                },
                beforeSend:function(){
                    $("#loadingsymbol").show();
                },
                success: function(response){
                    var blank = "";
                    $('#submitTexts').val(blank);
                    $("#loadingsymbol").hide();
                    $(".chatonline li").removeClass('active');
                    $('#groupchat_'+groupid).addClass('active');

                    $('#usersmenulist').html("");
                    $('#usersmenulist').html(response.menulist);

                    if(page=='0'){
                        $('#pulseloop').html("");
                        $('#pulseloop').html(response.chatdata);
                    }else{
                        $('#pulseloop').prepend(response.chatdata);
                    }
                    scrollChat();
                }
            });
        }
    }

    function scrollChat() {

       var height = $("body #sohbet")[0].scrollHeight;
        $("body #sohbet").animate({ scrollTop: height }, "slow");

    }

    $(document).on('click','.uploadfile',function () {
        $('#filebox').toggle();
    });
    $('#new-chat').click(function () {
        var url = '{{ route('admin.user-chat.create')}}';
        $('#modelHeading').html('Start Conversation');
        $.ajaxModal('#newChatModal',url);
    });
    $(document).on("click",'#new-group-chat',function () {
        var url = '{{ route('admin.user-chat.group-create')}}';
        $('#modelHeading').html('Start Conversation');
        $.ajaxModal('#newChatModal',url);
    });
    function createGroupChat() {
        var url = '{{ route('admin.user-chat.create-group-chat')}}';
        $('#modelHeading').html("Create New");
        $.ajaxModal('#taskActivityModal', url);
    }
    $(document).on("click","#groupmenulink",function () {
        $(".sidebarbox").show();
    });
    $(document).on("click",".groupemployee",function () {
        var userid = $(this).val();
        var token = '{{ csrf_token() }}';
        var groupid = $("#groupchatid").val();
        $.ajax({
            url: '{{route('admin.user-chat.group-chat-join-user')}}',
            type: "POST",
            data: {
                '_token' : token,
                'groupid' : groupid,
                'userid' : userid
            },
            beforeSend:function(){
                $("#loadingsymbol").show();
            },
            success: function(response){
                $("#loadingsymbol").hide();
                $(".chatonline li").removeClass('active');
                $('#groupchat_' + groupid).addClass('active');
                if(page=='0'){
                    $('#pulseloop').html("");
                    $('#pulseloop').html(response);
                }else{
                    $('#pulseloop').prepend(response);
                }
            }
        });
    });
    function closeSidebarview() {
        $(".sidebarbox").hide();
    }
</script>
@endpush