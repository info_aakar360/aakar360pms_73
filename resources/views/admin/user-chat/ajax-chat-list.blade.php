@forelse($chatDetails as $chatDetail)
    <li class="@if($chatDetail->from == $user->id) odd @else  @endif">
        <div class="chat-image"> <img alt="user" src="{{ get_users_image_link($chatDetail->from) }}"> </div>
        <div class="chat-body">
            <div class="chat-text">
                <h4>@if($chatDetail->from == $user->id) you @else {{$chatDetail->fromUser->name}} @endif</h4>
                <?php if($chatDetail->type=='image'&&!empty($chatDetail->image)){
                        $url = uploads_url().'chat-image/'.$chatDetail->image
                    ?>
                <div class="col-md-4">
                    {!! mimetype_thumbnail($chatDetail->image,$url) !!}
                </div>
                <?php }else{?>
                <p>{{ $chatDetail->message }}</p>
                <?php }?>
                <b>{{ $chatDetail->created_at->timezone($global->timezone)->format($global->date_format.' '. $global->time_format) }}</b>
            </div>
        </div>
    </li>

@empty
    <li><div class="message">@lang('messages.noMessage')</div></li>
@endforelse
