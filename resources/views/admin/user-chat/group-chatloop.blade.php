<?php
if(count($groupmessagesarray)>0){
    foreach ($groupmessagesarray as $groupmessage){
        ?>
<div class="pulse-block @if($groupmessage->user_id == $user->id) odd @else  @endif">
     <div class="col-md-1">
            <img class="img-responsive img-circle" src="{{ $groupmessage->userimage }}" alt="{{  $groupmessage->username }}'s image">
     </div>
     <div class="col-md-11">
         <div class="w-60 float-left">
            <div class="fnt-18">{{ $groupmessage->username }}</div>
         </div>
         <div class="w-40 float-right">
             <div class="fnt-15 text-right">{{ $groupmessage->datetime }}</div>
         </div>
            <div class="w-100 float-left">
                <?php
                if($groupmessage->type=='image'&&!empty($groupmessage->image)){
                $url = uploads_url().'group-chat-image/'.$groupmessage->image
                ?>
                <div class="col-md-4">
                {!! mimetype_thumbnail($groupmessage->image,$url) !!}
                </div>
                <?php }else{?>
             <div class="fnt-15">{{ $groupmessage->message }}</div>
                    <?php }?>
            </div>
         <div class="col-md-12 text-right">

         </div>

     </div>
</div>
<?php } }else{ ?>

        <p>No Messages Found</p>
<?php }?>