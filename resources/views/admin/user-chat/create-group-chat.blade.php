<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('app.create') @lang('app.group')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createBoqActivity','class'=>'ajax-form','method'=>'POST','autocomplete'=>'off']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Group Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Group Name" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions text-right">
            <button type="submit" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>

    $('#createBoqActivity').submit(function (e) {
        e.preventDefault();
        $.easyAjax({
            url: '{{route('admin.user-chat.group-chat-store')}}',
            container: '#createBoqActivity',
            type: "POST",
            data: $('#createBoqActivity').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#taskActivityModal').modal('hide');
                    window.location.href = '{{ route('admin.user-chat.index') }}';
                }
            }
        });
        return false;
    })
</script>