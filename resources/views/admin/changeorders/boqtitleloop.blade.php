                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $user = $boqarray['user'];
                                $proproget = $boqarray['proproget'];
                                $employeesarray = $boqarray['employeesarray'];
                                $id = $boqarray['id'];
                                $title = $boqarray['title'];
                                $contractorid = $boqarray['contractorid'];
                                $costitemslist = $boqarray['costitemslist'];
                                $contractorarray = $boqarray['contractorarray'];
                                $colpositionarray =$boqarray['colpositionarray'];
                                $unitsarray =$boqarray['unitsarray'];
                                $typesarray = $boqarray['typesarray'];
                                $grandtotal = $boqarray['grandtotal'];
                                $categories = $boqarray['categories'];
                                $tdcount =$boqarray['tdcount'];
                                $itemnoslug =$boqarray['itemnoslug'];
                                $catvalue = $boqarray['catvalue'];
                                $parent = $boqarray['parent'];
                                $subtotcat = $boqarray['subtotcat'];
                                $snorow = $boqarray['snorow'];
                                $act = 1;
                                foreach ($proproget as $propro){
                                $cattotalamt = 0;
                                if(!empty($propro->catlevel)){
                                    $catvalue =  $propro->catlevel.','.$propro->itemid;
                                }else{
                                    if(!empty($propro->parent)){
                                        $catvalue =  $propro->parent.','.$propro->itemid;
                                    }else{
                                        $catvalue = (string)$propro->itemid;
                                    }
                                }
                                $level = (int)$propro->level;
                                $levelname = 'level'.$level;
                                $catitem = $propro->itemid;
                                if($level==0){
                                    $subtotcat = $propro->itemid;
                                }
                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                    $actrow = $snorow;
                                }else{
                                    $actrow = $snorow.'.'.$act;
                                }
                                ?>
                                    <tr data-level1cat="{{ $propro->id }}" data-depth="0" class="maincat {{ $levelname }} collpse catitem{{ $catvalue }}">
                                        <td align="center">{{ $actrow }}</td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="{{ $propro->id }}" data-level="{{ $level }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                        <td class="text-center"><a href="javascript:void(0);" class="opntoggle iconcode @if($level>0) dotlevel @else dotlevel1 @endif">{!! get_dots_by_level($level) !!}</a></td>
                                        <td><?php if (isset($propro->itemname)){ echo $propro->itemname; } ?></td>
                                        <td colspan="{{ $tdcount }}"></td>
                                    </tr>
                                <?php
                                 $proprogetdatarra = \App\ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('position_id',$propro->id)->where('ordertype','changeorder')->orderBy('inc','asc')->get();
                                $tid=1;
                                foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                             /*   $proprogetdat->position_id = $propro->id;
                                $proprogetdat->save();*/
                                ?>
                                <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                    <td align="center">{{ $actrow.'.'.$tid }}</td>
                                    @if($itemnoslug)
                                        <td>
                                            <input type="text" class="cell-inp updateproject itemnovalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="itemno" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->itemno) ? $proprogetdat->itemno : '' }}">
                                        </td>
                                    @endif
                                    <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="{{ $proprogetdat->id }}" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                    <td class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>
                                    <?php foreach ($colpositionarray as $colposition){
                                    switch($colposition->itemslug){
                                    case 'costitem': ?>
                                    <td><input  class="cell-inp updateproject" data-item="cost_items_id"  data-positionid="{{ $propro->id }}" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="costitem{{ $proprogetdat->id }}"  value="{{ get_cost_name($proprogetdat->cost_items_id) }}">
                                        <datalist class="costitemslist"  id="costitem{{ $proprogetdat->id }}">
                                            <?php foreach($costitemslist as $costitem){ ?>
                                                <option data-value="{{ $costitem->id }}" >{{ $costitem->cost_item_name }}</option>
                                            <?php }?>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'description': ?>
                                    <td><textarea  class="cell-inp updateproject"  data-item="description" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?>  onkeydown="textAreaAdjust(this)" style="height: 25px;">{{ !empty($proprogetdat->description) ? $proprogetdat->description : '' }}</textarea></td>
                                    <?php   break;
                                    case 'assign_to': ?>
                                    <td><input  class="cell-inp updateproject" data-item="assign_to" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="assign{{ $proprogetdat->id }}"  value="{{ get_user_name($proprogetdat->assign_to) }}">
                                        <datalist class="assignlist"  id="assign{{ $proprogetdat->id }}">
                                            <?php foreach($employeesarray as $assign){?>
                                                <option  value="{{ $assign->id }}" data-value="{{ $assign->name }}" >{{ $assign->name }}</option>
                                        <?php  } ?>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'contractor': ?>
                                    <td><input  class="cell-inp updateproject" data-item="contractor" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" <?php if($colposition->collock=='1'){ echo 'disabled';}?> list="contractorid{{ $proprogetdat->id }}"  value="{{ get_user_name($proprogetdat->contractor) }}">
                                        <datalist class="contractorlist"  id="contractorid{{ $proprogetdat->id }}">
                                            <?php foreach($contractorarray as $contractor){ ?>
                                                <option  value="{{ $contractor->id }}"  data-value="{{ $contractor->name }}">{{ $contractor->name }}</option>
                                            <?php } ?>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'ordertype': ?>
                                    <td>
                                        <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  data-subtotcat="{{ $subtotcat }}"  onchange="calmarkup({{ $proprogetdat->id }})"  class="updateproject cell-inp ordertype{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" data-item="ordertype" data-itemid="{{ $proprogetdat->id }}" list="ordertype{{ $proprogetdat->id }}"  value="{{ $proprogetdat->ordertype }}">
                                        <datalist id="ordertype{{ $proprogetdat->id }}">
                                            <option value="estimate">Estimate</option>
                                            <option value="changeorder">Changeorder</option>
                                        </datalist>
                                    </td>

                                    <?php   break;
                                    case 'startdate': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="start_date" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->start_date) ? date('d-m-Y',strtotime($proprogetdat->start_date)) : '' }}"></td>
                                    <?php   break;
                                    case 'enddate': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp datepicker updateproject"  data-cat="{{ $catitem }}" data-item="deadline" data-itemid="{{ $proprogetdat->id }}"   value="{{ !empty($proprogetdat->deadline) ? date('d-m-Y',strtotime($proprogetdat->deadline)) : '' }}"></td>
                                    <?php   break;
                                    case 'rate': ?>
                                    <td style="position:relative;"><a href="javascript:;" class="open-rate-sheet" title="Open Rate Sheet" data-toggle="tooltip" data-id="{{ $proprogetdat->id }}"><i class="fa fa-ellipsis-v"></i></a>
                                        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject ratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="rate" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->rate) ? $proprogetdat->rate : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'qty': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject qty{{ $proprogetdat->id }}"  data-subtotcat="{{ $subtotcat }}"   onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-item="qty" value="{{ !empty($proprogetdat->qty) ? $proprogetdat->qty : '' }}"></td>
                                    <?php   break;
                                    case 'unit':    ?>
                                    <td>
                                        <input  class="cell-inp updateproject"  <?php if($colposition->collock=='1'){ echo 'disabled';}?> data-item="unit" data-itemid="{{ $proprogetdat->id }}" list="unitdata{{ $proprogetdat->id }}" data-level="{{ $level }}"   value="{{ get_unit_name($proprogetdat->unit) }}">
                                    <datalist id="unitdata{{ $proprogetdat->id }}">
                                        <?php foreach($unitsarray as $units){ ?>
                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                        <?php } ?>
                                    </datalist>
                                    </td>
                                    <?php   break;
                                    case 'amount': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject amount{{ $proprogetdat->id }}"  data-subtotcat="{{ $subtotcat }}"   onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-item="amount" value="{{ !empty($proprogetdat->amount) ? $proprogetdat->amount : '' }}"></td>
                                    <?php   break;
                                    case 'worktype': ?>
                                    <td> <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject" data-item="worktype"  data-itemid="{{ $proprogetdat->id }}" list="type{{ $proprogetdat->id }}"  value="{{ get_work_type_name($proprogetdat->worktype) }}">
                                        <datalist id="type{{ $proprogetdat->id }}">
                                           <?php foreach($typesarray as $types){ ?>
                                                <option data-value="{{ $types->id }}" >{{ ucwords($types->title) }}</option>
                                                <?php } ?>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'markuptype': ?>
                                    <td>
                                        <input   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  data-subtotcat="{{ $subtotcat }}"  onchange="calmarkup({{ $proprogetdat->id }})"  class="updateproject cell-inp markuptype{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" data-item="markuptype" data-itemid="{{ $proprogetdat->id }}" list="markuptype{{ $proprogetdat->id }}"  value="{{ $proprogetdat->markuptype }}">
                                        <datalist id="markuptype{{ $proprogetdat->id }}">
                                            <option value="percent">Percent</option>
                                            <option value="amt">Amount</option>
                                        </datalist>
                                    </td>
                                    <?php   break;
                                    case 'changeorderrate': ?>
                                    <td>
                                        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject changeorderratevalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="changeorderrate" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->changeorderrate) ? $proprogetdat->changeorderrate : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'changeorderamount': ?>
                                    <td>
                                        <input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="cell-inp updateproject changeorderamountvalue{{ $proprogetdat->id }}"  onchange="calmarkup({{ $proprogetdat->id }})" data-level="{{ $level }}" data-cat="{{ $catitem }}"  data-item="changeorderamount" data-level="{{ $level }}" data-itemid="{{ $proprogetdat->id }}"  value="{{ !empty($proprogetdat->changeorderamount) ? $proprogetdat->changeorderamount : 0 }}">
                                    </td>
                                    <?php   break;
                                    case 'markupvalue': ?>
                                    <td><input  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  type="text" class="updateproject cell-inp markupvalue{{ $proprogetdat->id }}" data-item="markupvalue"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" data-subtotcat="{{ $subtotcat }}" name="markupvalue"  data-subtotcat="{{ $subtotcat }}"  onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->markupvalue) ? $proprogetdat->markupvalue : '' }}" ></td>
                                    <?php   break;
                                    case 'adjustment': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp adjustment{{ $proprogetdat->id }} adjustcat{{ $catitem }}" data-item="adjustment"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="adjustment"  data-subtotcat="{{ $subtotcat }}"  onchange="calmarkup({{ $proprogetdat->id }})"  value="{{ !empty($proprogetdat->adjustment) ? $proprogetdat->adjustment : '' }}" ></td>
                                    <?php   break;
                                    case 'finalrate': ?>
                                    <td><input type="text"   <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp totalrate{{ $proprogetdat->id }} finalrate{{ $catitem }}" data-item="finalrate"  data-cat="{{ $catitem }}" data-itemid="{{ $proprogetdat->id }}" name="finalrate"   value="{{ !empty($proprogetdat->finalrate) ? $proprogetdat->finalrate : '' }}" ></td>
                                    <?php   break;
                                    case 'totalamount': ?>
                                    <td><input type="text"  <?php if($colposition->collock=='1'){ echo 'disabled';}?>  class="updateproject cell-inp grandvalue totalamount{{ $proprogetdat->id }} finalamount{{ $catitem }} subtotalamount{{ $subtotcat }}" data-level="{{ $level }}" data-item="totalamount" data-itemid="{{ $proprogetdat->id }}"  data-cat="{{ $catitem }}" name="finalamount"  value="{{ !empty($proprogetdat->finalamount) ? $proprogetdat->finalamount : '' }}"  ></td>
                                    <?php   break;
                                    }
                                    }?>
                                </tr>
                                <?php
                                $cattotalamt +=$proprogetdat->finalamount;
                                $tid++;
                                } }
                                $catdata = 1;
                                $newlevel = $level+1;
                                $parent = $propro->id;
                                 $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['user'] = $user;
                                $boqarray['proproget'] = $proprogetlevel1;
                                $boqarray['employeesarray'] = $employeesarray;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = (int)$title ?: 0;
                                $boqarray['contractorid'] = $contractorid ?: '';
                                $boqarray['costitemslist'] = $costitemslist;
                                $boqarray['contractorarray'] = $contractorarray;
                                $boqarray['colpositionarray'] = $colpositionarray;
                                $boqarray['unitsarray'] = $unitsarray;
                                $boqarray['typesarray'] = $typesarray;
                                $boqarray['grandtotal'] = $grandtotal;
                                $boqarray['categories'] = $categories;
                                $boqarray['catvalue'] = $catvalue;
                                $boqarray['tdcount'] = $tdcount;
                                $boqarray['itemnoslug'] = $itemnoslug;
                                $boqarray['parent'] = $parent;
                                $boqarray['level'] = $newlevel;
                                $boqarray['subtotcat'] = $subtotcat;
                                $boqarray['snorow'] = $actrow;
                                $boqarray['cattotalamt'] = $cattotalamt;
                                 echo boqhtml($boqarray);
                                ?>
                                    <tr  data-depth="1" class="collpse {{ $levelname }}" >
                                        <td></td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;"> <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemcategory" data-catlevel="{{ $catvalue }}" data-level="{{ $newlevel }}" data-parent="{{ $parent }}"  list="costitemlevel{{ $newlevel }}" placeholder="Activity">
                                            <datalist  id="costitemlevel{{ $newlevel }}">
                                                <?php foreach($categories as $category){ ?>
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                <?php  } ?>
                                            </datalist></div></td>
                                        <td colspan="{{ $tdcount }}"></td>
                                    </tr>
                                    <tr data-depth="1" class="collpse level{{ $newlevel }}">
                                        <td></td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a></td>
                                        <td>
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-cost-item" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i></a>
                                                <input  class="cell-inp costitemrow" @if($level=='0') data-cattype="single" @endif  data-positionid="{{ $parent }}"  data-cat="{{ $catvalue }}" data-level="{{ $newlevel }}" list="costitemcat{{ $catdata }}" placeholder="Task">
                                            <datalist class="costitemslist"  id="costitemcat{{ $catdata }}">
                                                <?php foreach($costitemslist as $costitem){ ?>
                                                    <option value="{{ $costitem->cost_item_name }}" data-id="{{ $costitem->id }}"  >{{ $costitem->cost_item_name }}</option>
                                                <?php  } ?>
                                            </datalist></div>
                                        </td>
                                        <td colspan="{{ $tdcount }}"><input type="text" class="cell-inp"></td>
                                    </tr>
                                <tr data-depth="1" class="collpse level{{ $newlevel }}" style="background-color: #efefef;">
                                    @if($level>0)
                                        <td  colspan="3" ></td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td> <strong>Sub Total</strong></td>
                                    @else
                                        <td  colspan="3" ></td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td  >
                                            <strong>Sub Total</strong>
                                        </td>
                                    @endif
                                    <td colspan="{{ $tdcount-1 }}"></td></td>
                                    <td class="catotal{{ $catitem }} subtotal{{ $subtotcat }}level{{ $level }}" data-level="{{ $level }}" style="font-weight: bold;">
                                        <?php
                                        if($level==0){
                                            $maxlevelamt = \App\ProjectCostItemsProduct::where('title',$title)->where('project_id',$id)->where('category','LIKE','%'.$subtotcat.'%')->where('ordertype','changeorder')->orderBy('inc','asc')->sum('finalamount');
                                            echo '₹'.number_format($maxlevelamt,2);
                                        }else{
                                            echo '₹'.number_format($cattotalamt,2);
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $act++;
                                } }
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();
                                $costitemslist = \App\CostItems::where('company_id',$projectdetails->company_id)->orderBy("id",'asc')->get();
                                $contractorarray = \App\Employee::getAllContractors($user,$projectdetails->id);
                                $colpositionarray = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','col')->whereIn('itemslug',$columsarray)->orderBy('inc','asc')->get();
                                $tdcount = 14;
                                 $boqarray = array();
                                $boqarray['user'] = $user;
                                $boqarray['proproget'] = $proproget;
                                $boqarray['employeesarray'] = $employeesarray;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = $title ?: 0;
                                $boqarray['contractorid'] = $contractorid ?: '';
                                $boqarray['costitemslist'] = $costitemslist;
                                $boqarray['contractorarray'] = $contractorarray;
                                $boqarray['colpositionarray'] = $colpositionarray;
                                $boqarray['tdcount'] = $tdcount;
                                $boqarray['itemnoslug'] = $itemnoslug;
                                $boqarray['unitsarray'] = $unitsarray;
                                $boqarray['typesarray'] = $typesarray;
                                $boqarray['grandtotal'] = 0;
                                $boqarray['categories'] = $categories;
                                $boqarray['grandtotal'] = 0;
                                $boqarray['cattotalamt'] = 0;
                                $boqarray['catvalue'] = '';
                                $boqarray['parent'] = '';
                                $boqarray['subtotcat'] = '';
                                $boqarray['snorow'] = 0;
                              echo  boqhtml($boqarray);

                                $grandtotal = \App\ProjectCostItemsProduct::where('project_id',$id)->where('title',$title)->where('ordertype','changeorder')->sum('finalamount');
                                ?>
                                    <tr  data-depth="0" class="collpse " >
                                        <td></td>
                                        @if($itemnoslug)
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div style="position:relative;">
                                                <a class="context-menu context-menu-category" data-toggle="tooltip" title="Add New Activity" ><i class="fa fa-ellipsis-v"></i></a>
                                            <input  class="cell-inp costitemcategory" list="costitemcatitem" data-level="0" placeholder="Activity">
                                            <datalist id="costitemcatitem">
                                                @foreach($categories as $category)
                                                    <option data-value="{{ $category->id }}" value="{{ $category->title }}" >{{ $category->title }}</option>
                                                @endforeach
                                            </datalist>
                                            </div></td>
                                        <td colspan="{{ $tdcount }}"></td>
                                    </tr>
                                <tr  data-depth="0" class="maincat collpse level0" >
                                    <td></td>
                                    @if($itemnoslug)
                                        <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                    <td><strong> Grand Total</strong></td>
                                    <td colspan="{{ $tdcount-1 }}"></td>
                                    <td class="grandtotal" style="font-weight: bold;">₹{{ !empty($grandtotal) ? number_format($grandtotal,2) : 0 }}</td>
                                </tr>