@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
<style>
    .counter{
        font-size: large;
    }
</style>
<style>
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .ps-body {
        background: #ffffff;
    }

    .img-fluid {
        max-width: 100%;
        height: auto;
    }

    .ps-table-common {
        width: 100%;
        height: 100%;
        table-layout: fixed;
        border-collapse: collapse;
    }

    .ps-table-common td,
    .ps-table-common th {
        height: 100%;
    }

    .ps-table-common.br {
        border: 1px solid #000000;
    }

    .ps-table-common tr.header {
        font-weight: 600;
        border-bottom: 1px solid #000000;
    }

    .ps-table-common tr.footer {
        font-weight: 600;
        border-top: 1px solid #000000;
    }

    .ps-table-common.br > tbody > tr > td:first-child {
        border-right: 1px solid #000000;
    }

    .half-75 {
        width: 75%;
        padding: 5px 0 5px 10px;
        display: inline-block;
        text-transform: capitalize;
    }
    .half-25 {
        width: 25%;
        padding: 5px 0 5px 10px;
        display: inline-block;
        text-transform: capitalize;
    }

    .half {
        width: 50%;
        padding: 5px 0 5px 10px;
        display: inline-block;
        text-transform: capitalize;
    }

    .half-2 {
        width: 25%;
        padding: 5px 0;
        display: inline-block;
        text-transform: capitalize;
    }

    .half:last-child,
    .half-2:last-child,
    .half-25:last-child,
    .half-75:last-child {
        padding: 5px 10px 5px 0;
    }

    .semibold {
        font-weight: 600;
    }

    .ps-wrapper {
        padding: 20px;
        font-size: 14px;
        position: relative;
        background: #ffffff;
        padding-bottom: 100px;
        border: 10px solid #ddd;
        font-family: "Open Sans";
    }

    .ps-company-logo {
        max-height: 100px;
    }

    .ps-company-title-wrapper {
        width: 100%;
    }

    .ps-main-title {
        font-size: 30px;
        font-weight: 600;
        line-height: 1;
        margin: 10px 0;
    }

    .ps-main-address {
        font-size: 14px;
    }

    .ps-time-period {
        font-weight: 400;
        text-align: center;
        font-size: 20px;
        margin: 30px 0 20px;
    }

    .ps-ul {
        margin: 0;
        height: 100%;
        position: relative;
        list-style-type: none;
        border: 1px solid #000;
        text-transform: capitalize;
    }

    .ps-ul.ps-ul-amount {
        padding-bottom: 35px;
    }

    .ps-ul > li {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .ps-ul.ps-ul-amount > li.header {
        margin-top: 0;
        font-weight: 700;
        margin-bottom: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        border-bottom: 1px solid #000;
    }

    .ps-ul.ps-ul-amount > li.footer {
        bottom: 0;
        width: 100%;
        margin-top: 0;
        font-weight: 700;
        margin-bottom: 0;
        padding-top: 5px;
        position: absolute;
        padding-bottom: 5px;
        border-top: 1px solid #000;
    }

    .ps-ul.ps-ul-amount > li > div:last-child {
        text-align: right;
    }

    .ps-wrapper-footer {
        left: 0;
        bottom: 0;
        width: 100%;
        position: absolute;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 10px;
    }

    .amount-text {
        font-style: italic;
        text-transform: capitalize;
    }

    hr.ps-hr {
        margin: 10px 0;
        border-color: #000000;
    }

    .eq-row {
        margin: 0;
        width: 100%;
        display: table;
    }

    .eq-row > [class*="col-"] {
        float: none;
        padding: 0;
        display: table-cell;
        vertical-align: top;
    }

    .abs {
        position: absolute;
        width: 100%;
    }

    .text-center {
        text-align: center;
    }

    .text-right {
        text-align: right;
    }

    .mt {
        margin-top: 2.5rem;
    }

    .m-0 {
        margin: 0;
    }

    .br-0 {
        border-right: none;
    }

    .float-right {
        float: right;
    }

    .bt {
        border-top: 1px solid #000000;
    }

    #payslipTitle {
        font-size: 18px;
        font-weight: 600;
    }

    .payslip-container p.err,
    .payslip-mobile-container p.err {
        margin: 10px 0 0 0;
        padding: 15px;
        text-align: center;
    }

    .payslip-mobile-container p.err {
        margin: 0;
    }

    .alt-loader {
        margin: 10px 0 0 0;
        height: 100px;
        display: flex;
        align-items: center;
    }

    .alt-loader > .loader {
        position: relative;
    }

    .payslip-mobile-container .alt-loader {
        margin: 0;
    }

    @media screen and (max-width: 767px) {
        .main-wrapper {
            margin-top: 215px;
        }
        .salary-slip-navigation-buttons {
            float: none;
            text-align: center;
            margin-top: 20px;
        }
    }

    @media screen and (max-width: 768px) {
        .payslip-wrapper {
            padding: 5px;
        }
    }

    .default-rule-btn-wrapper {
        float: right;
        display: flex;
    }
    .default-rule-btn {
        cursor: pointer;
        min-width: 185px;
        text-transform: none !important;
    }

    .default-rule-btn-wrapper.default .not-default,
    .default-rule-btn-wrapper .is-default {
        display: none;
    }

    .default-rule-btn-wrapper.default .is-default,
    .default-rule-btn-wrapper .not-default {
        display: block;
    }

    .default-rule-btn-wrapper.default .default-text {
        display: flex;
    }

    .default-rule-btn.default {
        min-width: auto;
        font-size: 14px;
        box-shadow: none;
        padding-right: 15px;
        color: #ea4033 !important;
        background-color: #ffe5e3 !important;
    }

    .default-rule-btn.default .is-default span {
        position: relative;
        top: 1px;
    }
    .default-text {
        color: #21c70f;
        display: flex;
        font-size: 14px;
        font-weight: 600;
        margin-right: 20px;
        align-items: center;
    }

    .default-text .material-icons {
        margin-right: 5px;
    }
    #salarySlipDefaultBtn.default{
        background-color: #2818c7 !important;
        color: #fff !important;
    }
    #salarySlipDefaultBtn.default .material-icons{
        color: #fff !important;
    }


</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/icheck/skins/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@endpush

@section('content')
        <!-- .row -->
<div class="row">
    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <div class="user-bg">
                {!! ($employee->image) ? '<img src="'.asset('user-uploads/avatar/'.$employee->image).'"
                                                            alt="user" width="100%">' : '<img src="'.asset_url('user.png').'"
                                                            alt="user" width="100%">' !!}
                <div class="overlay-box" style="width:98%; !important;">
                    <div class="user-content"> <a href="javascript:void(0)">
                            {!! ($employee->image) ? '<img src="'.asset('user-uploads/avatar/'.$employee->image).'"
                                                            alt="user" class="thumb-lg img-circle">' : '<img src="'.asset_url('user.png').'"
                                                            alt="user" class="thumb-lg img-circle">' !!}
                            </a>
                        <h4 class="text-white">{{ ucwords($employee->name) }}</h4>
                        <h5 class="text-white">{{ $employee->email }}</h5>
                    </div>
                </div>
            </div>
            <div class="user-btm-box">
                <div class="row row-in">
                    <div class="col-md-6 row-in-br">
                        <div class="col-in row">
                                <h3 class="box-title">@lang('modules.employees.tasksDone')</h3>
                                <div class="col-xs-4"><i class="ti-check-box text-success"></i></div>
                                <div class="col-xs-8 text-right counter">{{ $taskCompleted }}</div>
                        </div>
                    </div>
                    <div class="col-md-6 row-in-br  b-r-none">
                        <div class="col-in row">
                                <h3 class="box-title">@lang('modules.employees.hoursLogged')</h3>
                            <div class="col-xs-2"><i class="icon-clock text-info"></i></div>
                            <div class="col-xs-10 text-right counter" style="font-size: 13px">{{ $hoursLogged }}</div>
                        </div>
                    </div>
                </div>
                <div class="row row-in">
                    <div class="col-md-6 row-in-br">
                        <div class="col-in row">
                                <h3 class="box-title">@lang('modules.leaves.leavesTaken')</h3>
                                <div class="col-xs-4"><i class="icon-logout text-warning"></i></div>
                                <div class="col-xs-8 text-right counter">{{ count($leaves) }}</div>
                        </div>
                    </div>
                    <div class="col-md-6 row-in-br  b-r-none">
                        <div class="col-in row">
                                <h3 class="box-title">@lang('modules.leaves.remainingLeaves')</h3>
                            <div class="col-xs-4"><i class="icon-logout text-danger"></i></div>
                            <div class="col-xs-8 text-right counter">{{ ($allowedLeaves-count($leaves)) }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12">
        <div class="white-box">
            <ul class="nav nav-tabs tabs customtab">
                <li class="active tab"><a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('modules.employees.activity')</span> </a> </li>
                <li class="tab"><a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">@lang('modules.employees.profile')</span> </a> </li>
                <li class="tab"><a href="#projects" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs">@lang('app.menu.projects')</span> </a> </li>
                <li class="tab"><a href="#tasks" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-list"></i></span> <span class="hidden-xs">@lang('app.menu.tasks')</span> </a> </li>
                <li class="tab"><a href="#leaves" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-logout"></i></span> <span class="hidden-xs">@lang('app.menu.leaves')</span> </a> </li>
                <li class="tab"><a href="#payslip" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs">@lang('app.menu.payslip')</span> </a> </li>
                <li class="tab"><a href="#time-logs" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.timeLogs')</span> </a> </li>
                <li class="tab"><a href="#docs" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-docs"></i></span> <span class="hidden-xs">@lang('app.menu.documents')</span> </a> </li>
                <li class="tab"><a href="#role" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-docs"></i></span> <span class="hidden-xs">@lang('app.menu.role')</span> </a> </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div class="steamline">
                        @forelse($activities as $key=>$activity)
                        <div class="sl-item">
                            <div class="sl-left">
                                {!!  ($employee->image) ? '<img src="'.asset('user-uploads/avatar/'.$employee->image).'"
                                                            alt="user" class="img-circle">' : '<img src="'.asset('default-profile-2.png').'"
                                                            alt="user" class="img-circle">' !!}
                            </div>
                            <div class="sl-right">
                                <div class="m-l-40"><a href="#" class="text-info">{{ ucwords($employee->name) }}</a> <span  class="sl-date">{{ $activity->created_at->diffForHumans() }}</span>
                                    <p>{!! ucfirst($activity->activity) !!}</p>
                                </div>
                            </div>
                        </div>
                            @if(count($activities) > ($key+1))
                                <hr>
                            @endif
                        @empty
                            <div>@lang('messages.noActivityByThisUser')</div>
                        @endforelse
                    </div>
                </div>
                <div class="tab-pane" id="payslip">
                    <div class="row">
                        @if(!empty($payRegister))
                            <!-- payslip components -->
                            <div class="section-details p-3 mt-0">
                                <div id="payslipContainer">
                                    <meta charset="utf-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                                    <meta http-equiv="x-ua-compatible" content="ie=edge; charset=utf-8">
                                    <title>Salary Slip | Format 1</title>
                                    <link type="image/png" href="/static/images/favicon-16x16.png" rel="shortcut icon">

                                    <!-- Font family -->
                                    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
                                    <style>
                                        .ps-wrapper {
                                            margin-top: 10px;
                                        }
                                    </style>
                                    <div class="mbl-responsive-table">
                                        <div class="ps-wrapper container-fluid">
                                            <table class="ps-table-common">
                                                <tbody>
                                                <tr>
                                                    <td width="20%" class="for_mobile-res-td-wd-mtop-20 min-wd-320">
                                                        <div class="ps-logo-wrapper">
                                                            <p><img src="{{ $global->logo() }}" class="admin-logo"  alt="logo" width="150px" /></p>
                                                        </div>
                                                    </td>
                                                    <td width="60%" class="for_mobile-res-td-width min-wd-320">
                                                        <div class="col-xs-12">
                                                            <div class="ps-company-title-wrapper text-center">
                                                                <h2 class="ps-main-title">{{$company->company_name}}</h2>
                                                                <p class="m-0 ps-main-address">

                                                                    [ {{$payRegister->address}} ]

                                                                </p>
                                                                <p class="m-0 ps-main-address">
                                                                    Registered office address of Aakar360 Mentors Private Limited is D-06, Sector 2, <br>Agroha Society Ring Road No. 01, Raipura Raipur Raipur-492013 Chhattisgarh
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td width="20%"></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <h5 class="text-center ps-time-period">Payslip for the Month of {{\Carbon\Carbon::createFromDate($payRegister->month)->format('F')}}, {{$payRegister->year}}</h5>
                                            <table class="ps-table-common br displayBlock">
                                                <tbody>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Name:</div><div class="half">{{$payRegister->name}}</div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Employee ID:</div><div class="half">{{$payRegister->employee_id}}</div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Designation:</div><div class="half whitespace-normal">{{$designation->name}}</div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Bank Name:</div><div class="half whitespace-normal">  </div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Department:</div><div class="half">{{$team->team_name}}</div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Bank Account No:</div><div class="half">  </div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Location:</div><div class="half">{{$payRegister->address}}</div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">PAN No.:</div><div class="half">ABCDE1234F</div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Effective Work Days:</div><div class="half">{{$payRegister->working_days}}</div></div>
                                                    </td>
                                                    @if($pfesidata->pf_applicable == 'on')
                                                        <td class="mobile-res-wd" width="50%">
                                                            <div class="d-flex h-100">
                                                                <div class="half">PF No.:</div>
                                                                <div class="half wordBreak">Pf Number</div>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td class="mobile-res-wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">LOP:</div><div class="half">{{$payRegister->lop}}</div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <table class="ps-table-common br">
                                                <tbody>
                                                <tr class="header">
                                                    <td width="50%" class="min-wd min-420">
                                                        <div class="d-flex h-100"><div class="half mobile-res-65">Earnings</div><div class="half mobile-res-35  text-right"></div></div>
                                                    </td>
                                                    <td width="50%" class="min-wd min-420">
                                                        <div class="d-flex h-100"><div class="half mobile-res-65">Deductions</div><div class="half mobile-res-35  text-right"></div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <?php if(!empty($ahdocearning->compo_name)) { $ername = App\AdhocEarning::where('id',$ahdocearning->compo_name)->first(); }
                                                    if(!empty($ahdocearning->compo_name)) { $didname = App\AdhocDeduction::where('id',$ahdocearning->compo_name)->first(); }
                                                    ?>
                                                    <td width="50%">
                                                        <div class="d-flex h-100"><div class="half">@if(!empty($ername->component_name)){{$ername->component_name}} @endif</div>
                                                            <div class="half text-right">@if(!empty($ahdocearning->amount)) {{$ahdocearning->amount}} @endif</div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex h-100"><div class="half">@if(!empty($didname->component_name)) {{$didname->component_name}} @endif</div>
                                                            <div class="half text-right">@if(!empty($ahdocdiduction->amount)) {{$ahdocdiduction->amount}} @endif</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    @if(!empty($salarysettings->status) && $salarysettings->status == 'yes')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">BASIC</div><div class="half text-right">{{($payRegister->monthly_rate*$salary->basic)/100}}</div></div>
                                                        </td>
                                                    @endif
                                                    @if(!empty($pfesidata->pf_applicable) && $pfesidata->pf_applicable == 'on')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">PF</div><div class="half text-right"></div></div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    @if(!empty($salarysettings->status) && $salarysettings->status == 'yes')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">HRA</div><div class="half text-right">{{(($payRegister->monthly_rate*$salary->basic)/100*$salary->hra)/100}}</div></div>
                                                        </td>
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">INCOME TAX</div><div class="half text-right">{{$payRegister->income_tax}}</div></div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    @if(!empty($salarysettings->status) && $salarysettings->status == 'yes')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">@foreach($allowance as $key=>$value){{$value}} @endforeach</div><div class="half text-right">@foreach($amount as $key=>$value){{$value}} @endforeach</div></div>
                                                        </td>
                                                    @endif
                                                    @if(!empty($ptSettings->pt_applicable) && $ptSettings->pt_applicable == 'on')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">PROF TAX</div><div class="half text-right"></div></div>
                                                        </td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    @if(!empty($salarysettings->status) && $salarysettings->status == 'yes')
                                                        <td width="50%">
                                                            <div class="d-flex h-100"><div class="half">SPECIAL ALLOWANCE</div><div class="half text-right">{{$payRegister->monthly_rate - ((($payRegister->monthly_rate*$salary->basic)/100)+(($payRegister->monthly_rate*$salary->basic)/100*$salary->hra)/100 + $totalAllowance) }}</div></div>
                                                        </td>
                                                    @endif
                                                    <td width="50%">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="ps-table-common br">
                                                <tbody>
                                                <tr class="header">
                                                    <td width="50%">
                                                        <div class="d-flex h-100"><div class="half mobile-res-65">Total Earnings (Rs)</div><div class="half mobile-res-35 text-right"> @if(!empty($payRegister->monthly_rate)){{$payRegister->monthly_rate }} @endif  @if(!empty($ahdocearning->amount)){{ + $ahdocearning->amount}} @endif</div></div>
                                                    </td>
                                                    <td width="50%">
                                                        <div class="d-flex h-100"><div class="half mobile-res-65">Total Deductions (Rs)</div><div class="half mobile-res-35 text-right">@if(!empty($ahdocdiduction->amount)) {{$ahdocdiduction->amount}} @endif</div></div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table class="ps-table-common">
                                                <tbody>
                                                <tr>
                                                    <td class="mobile-res-wd mtop-10 mbott-10 for_mobile_large_wd" width="50%">
                                                        <div class="d-flex h-100"><div class="half">Net Pay for the Month:</div><div class="half text-right"><span class="semibold" id="netpay">{{ ceil((!empty($payRegister->net_pay)?$payRegister->net_pay:0) + (!empty($ahdocearning->amount)?$ahdocearning->amount:0) - (!empty($ahdocdiduction->amount)?$ahdocdiduction->amount:0)) }}</span></div></div>
                                                    </td>
                                                    <td class="mobile-res-wd" width="50%"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div>
                                                <p class="amount-text m-0 text-capitalize" id="netpayinword"></p>
                                                <hr class="ps-hr">
                                                <p class="text-center m-0">This is a system generated payslip and does not require signature.</p>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- end payslip components -->
                        @else
                            <div class="alert">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                <strong>Info!</strong> PaySlip Not Generated For This Month.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="profile">
                    <div class="row">

                        <div class="col-xs-6 col-md-4 b-r"> <strong>@lang('modules.employees.fullName')</strong> <br>
                            <p class="text-muted">{{ ucwords($employee->name) }}</p>
                        </div>
                        <div class="col-xs-6 col-md-4"> <strong>@lang('app.mobile')</strong> <br>
                            <p class="text-muted">{{ $employee->mobile ?? 'NA'}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('app.email')</strong> <br>
                            <p class="text-muted">{{ $employee->email }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>@lang('app.address')</strong> <br>
                            <p class="text-muted">{{ (!is_null($employee->employeeDetail)) ? $employee->employeeDetail->address : 'NA'}}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('app.designation')</strong> <br>
                            <p class="text-muted">{{ (!is_null($employee->employeeDetail) && !is_null($employee->employeeDetail->designation)) ? ucwords($employee->employeeDetail->designation->name) : 'NA' }}</p>
                        </div>
                        <div class="col-md-6 col-xs-6"> <strong>@lang('app.department')</strong> <br>
                            <p class="text-muted">{{ (!is_null($employee->employeeDetail) && !is_null($employee->employeeDetail->department)) ? ucwords($employee->employeeDetail->department->team_name) : 'NA' }}</p>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('modules.employees.slackUsername')</strong> <br>
                            <p class="text-muted">{{ (!is_null($employee->employeeDetail)) ? '@'.$employee->employeeDetail->slack_username : 'NA' }}</p>
                        </div>
                        <div class="col-md-6 col-xs-6"> <strong>@lang('modules.employees.joiningDate')</strong> <br>
                            <p class="text-muted">{{ (!is_null($employee->employeeDetail)) ? $employee->employeeDetail->joining_date->format($global->date_format) : 'NA' }}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 b-r"> <strong>@lang('modules.employees.gender')</strong> <br>
                            <p class="text-muted">{{ $employee->gender }}</p>
                        </div>
                        <div class="col-md-6 col-xs-6"> <strong>@lang('app.skills')</strong> <br>
                          {{--  {{implode(', ', $employee->skills()) }--}}
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6"> <strong>@lang('modules.employees.hourlyRate')</strong> <br>
                            <p class="text-muted">{{--{{ (count($employee->employee) > 0) ? $employee->employee[0]->hourly_rate : 'NA' }}--}}</p>
                        </div>
                    </div>
                    {{--Custom fields data--}}
                        @if(isset($fields))
                        <div class="row">
                            <hr>
                            @foreach($fields as $field)
                                <div class="col-md-6">
                                    <strong>{{ ucfirst($field->label) }}</strong> <br>
                                    <p class="text-muted">
                                        @if( $field->type == 'text')
                                            {{$employeeDetail->custom_fields_data['field_'.$field->id] ?? ''}}
                                        @elseif($field->type == 'password')
                                            {{$employeeDetail->custom_fields_data['field_'.$field->id] ?? ''}}
                                        @elseif($field->type == 'number')
                                            {{$employeeDetail->custom_fields_data['field_'.$field->id] ?? ''}}

                                        @elseif($field->type == 'textarea')
                                        {{$employeeDetail->custom_fields_data['field_'.$field->id] ?? ''}}

                                        @elseif($field->type == 'radio')
                                            {{ $field->values[$employeeDetail->custom_fields_data['field_'.$field->id]] }}
                                        @elseif($field->type == 'select')
                                                {{ $field->values[$employeeDetail->custom_fields_data['field_'.$field->id]] }}
                                        @elseif($field->type == 'checkbox')
                                            {{ $field->values[$employeeDetail->custom_fields_data['field_'.$field->id]] }}
                                        @elseif($field->type == 'date')
                                            {{ isset($employeeDetail->dob)?Carbon\Carbon::parse($employeeDetail->dob)->format($global->date_format):Carbon\Carbon::now()->format($global->date_format)}}
                                        @endif
                                    </p>

                                </div>
                            @endforeach
                        </div>
                        @endif

                    {{--custom fields data end--}}

                </div>
                <div class="tab-pane" id="projects">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('app.project')</th>
                                <th>@lang('app.deadline')</th>
                                <th>@lang('app.completion')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($projects as $key=>$project)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="{{ route('admin.projects.show', $project->id) }}">{{ ucwords($project->project_name) }}</a></td>
                                    <td>@if($project->deadline){{ $project->deadline->format($global->date_format) }}@else - @endif</td>
                                    <td>
                                        <?php

                                        if ($project->completion_percent < 50) {
                                        $statusColor = 'danger';
                                        }
                                        elseif ($project->completion_percent >= 50 && $project->completion_percent < 75) {
                                        $statusColor = 'warning';
                                        }
                                        else {
                                        $statusColor = 'success';
                                        }
                                        ?>

                                        <h5>@lang('app.completed')<span class="pull-right">{{  $project->completion_percent }}%</span></h5><div class="progress">
                                            <div class="progress-bar progress-bar-{{ $statusColor }}" aria-valuenow="{{ $project->completion_percent }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project->completion_percent }}%" role="progressbar"> <span class="sr-only">{{ $project->completion_percent }}% @lang('app.completed')</span> </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">@lang('messages.noProjectFound')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tasks">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="hide-completed-tasks">
                                <label for="hide-completed-tasks">@lang('app.hideCompletedTasks')</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                               id="tasks-table" width="100%">
                            <thead>
                            <tr>
                                <th>@lang('app.id')</th>
                                <th>@lang('app.project')</th>
                                <th>Task</th>
                                <th>@lang('app.dueDate')</th>
                                <th>@lang('app.status')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
                <div class="tab-pane" id="leaves">
                    <div class="row">

                        <div class="col-md-6">
                            <ul class="basic-list">
                                @forelse($leaveTypes as $leaveType)
                                    <li>{{ ucfirst($leaveType->type_name) }}
                                        <span class="pull-right label-{{ $leaveType->color }} label">{{ (isset($leaveType->leavesCount[0])) ? $leaveType->leavesCount[0]->count : '0' }}</span>
                                    </li>
                                @empty
                                    <li>@lang('messages.noRecordFound')</li>
                                @endforelse
                            </ul>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table" id="leave-table">
                                <thead>
                                <tr>
                                    <th>@lang('modules.leaves.leaveType')</th>
                                    <th>@lang('app.date')</th>
                                    <th>@lang('modules.leaves.reason')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($leaves as $key=>$leave)
                                    <tr>
                                        <td>
                                            <label class="label label-{{ $leave->type->color }}">{{ ucwords($leave->type->type_name) }}</label>
                                        </td>
                                        <td>
                                            {{ $leave->leave_date->format($global->date_format) }}
                                        </td>
                                        <td>
                                            {{ $leave->reason }}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>@lang('messages.noRecordFound')</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="time-logs">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="timelog-table" width="100%">
                            <thead>
                            <tr>
                                <th>@lang('app.id')</th>
                                <th>@lang('app.project')</th>
                                <th>@lang('modules.employees.startTime')</th>
                                <th>@lang('modules.employees.endTime')</th>
                                <th>@lang('modules.employees.totalHours')</th>
                                <th>@lang('modules.employees.memo')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="docs">
                    <button class="btn btn-sm btn-info addDocs" onclick="showAdd()"><i
                                class="fa fa-plus"></i> @lang('app.add')</button>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="70%">@lang('app.name')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                            <tbody id="employeeDocsList">
                            @forelse($employeeDocs as $key=>$employeeDoc)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td width="70%">{{ ucwords($employeeDoc->name) }}</td>
                                    <td>
                                        <a href="{{ route('admin.employee-docs.download', $employeeDoc->id) }}"
                                           data-toggle="tooltip" data-original-title="Download"
                                           class="btn btn-inverse btn-circle"><i
                                                    class="fa fa-download"></i></a>
                                        <a target="_blank" href="{{ asset('user-uploads/employee-docs/'.$employeeDoc->user_id.'/'.$employeeDoc->hashname) }}"
                                           data-toggle="tooltip" data-original-title="View"
                                           class="btn btn-info btn-circle"><i
                                                    class="fa fa-search"></i></a>
                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="{{ $employeeDoc->id }}"
                                                                                    data-pk="list" class="btn btn-danger btn-circle sa-params"><i class="fa fa-times"></i></a>
                                    </td>

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">@lang('messages.noDocsFound')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="role">
                    <div class="row">
                        <div class="col-md-12">
                            @if($employee->id != $firstAdmin->id)
                            <form class="form" id="saveRoleForm" action="{{ route('admin.employees.saveRole', [Auth::user()->id]) }}" method="post">
                                {{ csrf_field() }}
                                @php
                                    $custom = false;
                                @endphp
                                @if(in_array("0", $user_roles)) @php $custom=true; @endphp @endif
                                <div class="col-md-12">
                                    <select class="select2 m-b-10 form-control" data-placeholder="Choose Project" name="project_id" id="project_id">
                                        <option value="">Please Select</option>
                                        @foreach($projects as $role)
                                            <option value="{{ $role->id }}">{{ $role->project_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <select class="select2 m-b-10 form-control" data-placeholder="Choose Roles" name="roles" id="roles_permission"  @if($custom) disabled="disabled" @endif>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if(in_array($role->id, $user_roles)) selected @endif>{{ $role->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="custom" style="cursor: pointer"><input type="checkbox" name="custom" id="custom" @if($custom) checked @endif> Custom Permissions</label>
                                </div>
                                <div class="col-md-12 custom-div" @if(!$custom) style="display: none;" @endif>
                                    {{--<div class="col-md-4">--}}
                                        {{--<label for="admin_radio"><input type="radio" name="role_type" value="admin" id="admin_radio" @if(in_array('admin', $user_rolesx)) checked  @endif> As Admin</label>--}}
                                    {{--</div>--}}
                                    <div class="col-md-4">
                                        <label for="client_radio"><input type="radio" name="role_type" value="client" id="client_radio" @if(in_array('client', $user_rolesx)) checked  @endif> As Client</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="contractor_radio"><input type="radio" name="role_type" value="contractor" id="contractor_radio" @if(in_array('contractor', $user_rolesx)) checked  @endif> As Contractor</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="employee_radio"><input type="radio" name="role_type" value="employee" id="employee_radio" @if(in_array('employee', $user_rolesx)) checked  @endif> As Employee</label>
                                    </div>

                                </div>
                                <div class="col-md-12 b-t permission-section" @if(!$custom) style="display: none;" @endif id="role-permission-data" >
                                    <table class="table ">
                                        <thead>
                                        <tr class="bg-white">
                                            <th>
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-info  col-md-10">
                                                        <input id="select_all_permission" class="select_all_permission" type="checkbox">
                                                        <label for="select_all_permission">@lang('modules.permission.selectAll')</label>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>@lang('app.add')</th>
                                            <th>@lang('app.view')</th>
                                            <th>@lang('app.update')</th>
                                            <th>@lang('app.delete')</th>
                                            <th>@lang('app.approve')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($modulesData as $moduleData)
                                            @if($moduleData->module_name != 'messages')
                                                <tr>
                                                    <td>@lang('modules.module.'.$moduleData->module_name)
                                                        @if($moduleData->description != '')
                                                            <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">{{ $moduleData->description  }}</span></span></span></a>
                                                        @endif
                                                    </td>
                                                    @foreach($moduleData->permissions as $permission)
                                                        <td>
                                                            <div class="switchery-demo">
                                                                <input type="checkbox"
                                                                       @if($role->hasCustomPermission($permission->name, $employee->id))
                                                                       checked
                                                                       @endif
                                                                       class="js-switch assign-role-permission permission" data-size="small" data-color="#00c292" name="rolesx[{{ $permission->id }}]" />
                                                            </div>
                                                        </td>
                                                    @endforeach

                                                    @if(count($moduleData->permissions) < 5)
                                                        @for($i=1; $i<=(5-count($moduleData->permissions)); $i++)
                                                            <td>&nbsp;</td>
                                                        @endfor
                                                    @endif

                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary button-outline pull-right" id="submit_role" type="button" style="margin-top: 10px">Save</button>
                                </div>
                            </form>
                            @else
                                <p class="text-center">Role of this user cannot be changed.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
        {{--Ajax Modal--}}
        <div class="modal fade bs-modal-md in" id="edit-column-form" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        Loading...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $(document).on('change', '#custom', function(){
        if($(this).prop('checked')){
            $('#roles_permission').attr('disabled', true);
            $('#role-permission-data').show();
            $('.custom-div').show();
        }else{
            $('#roles_permission').attr('disabled', false);
            $('#role-permission-data').hide();
            $('.custom-div').hide();
        }
    });
    $(document).on('click', '#submit_role', function(){
        $.easyAjax({
            url: "{{ route('admin.employees.saveRole', [$employee->id]) }}",
            type: "POST",
            data: $('#saveRoleForm').serialize()
        });
    });
    // Show Create employeeDocs Modal
    function showAdd() {
        var url = "{{ route('admin.employees.docs-create', [$employee->id]) }}";
        $.ajaxModal('#edit-column-form', url);
    }

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.employee-docs.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#employeeDocsList').html(response.html);
                        }
                    }
                });
            }
        });
    });

    $('#leave-table').dataTable({
        responsive: true,
        "columnDefs": [
            { responsivePriority: 1, targets: 0, 'width': '20%' },
            { responsivePriority: 2, targets: 1, 'width': '20%' }
        ],
        "autoWidth" : false,
        searching: false,
        paging: false,
        info: false
    });

    var table;

    function showTable() {

        if ($('#hide-completed-tasks').is(':checked')) {
            var hideCompleted = '1';
        } else {
            var hideCompleted = '0';
        }

        var url = '{{ route('admin.employees.tasks', [$employee->id, ':hideCompleted']) }}';
        url = url.replace(':hideCompleted', hideCompleted);

        table = $('#tasks-table').dataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: url,
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function (oSettings) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            "order": [[0, "desc"]],
            columns: [
                {data: 'id', name: 'id'},
                {data: 'project_name', name: 'projects.project_name', width: '20%'},
                {data: 'heading', name: 'heading', width: '20%'},
                {data: 'due_date', name: 'due_date'},
                {data: 'column_name', name: 'taskboard_columns.column_name'},
            ]
        });
    }

    $('#hide-completed-tasks').click(function () {
        showTable();
    });

    showTable();
</script>

<script>
    var table2;

    function showTable2(){

        var url = '{{ route('admin.employees.time-logs', [$employee->id]) }}';

        table2 = $('#timelog-table').dataTable({
            destroy: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: url,
            deferRender: true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            "order": [[ 0, "desc" ]],
            columns: [
                { data: 'id', name: 'id' },
                { data: 'project_name', name: 'projects.project_name' },
                { data: 'start_time', name: 'start_time' },
                { data: 'end_time', name: 'end_time' },
                { data: 'total_hours', name: 'total_hours' },
                { data: 'memo', name: 'memo' }
            ]
        });
    }

    showTable2();
</script>
<script>
    $(document).ready(function(){
        $val = convertNumberToWords($("#netpay").text());
        $("#netpayinword").text('( Repees  ' + $val + ')');
    });
</script>
<script>

    $('#save-form-2').click(function () {
        $.easyAjax({
            url: '{{route('admin.payroll.store')}}',
            container: '#createPaySettings',
            type: "POST",
            redirect: true,
            data: $('#createPaySettings').serialize()
        })
    });
    function convertNumberToWords(amount) {
        var words = new Array();
        words[0] = '';
        words[1] = 'One';
        words[2] = 'Two';
        words[3] = 'Three';
        words[4] = 'Four';
        words[5] = 'Five';
        words[6] = 'Six';
        words[7] = 'Seven';
        words[8] = 'Eight';
        words[9] = 'Nine';
        words[10] = 'Ten';
        words[11] = 'Eleven';
        words[12] = 'Twelve';
        words[13] = 'Thirteen';
        words[14] = 'Fourteen';
        words[15] = 'Fifteen';
        words[16] = 'Sixteen';
        words[17] = 'Seventeen';
        words[18] = 'Eighteen';
        words[19] = 'Nineteen';
        words[20] = 'Twenty';
        words[30] = 'Thirty';
        words[40] = 'Forty';
        words[50] = 'Fifty';
        words[60] = 'Sixty';
        words[70] = 'Seventy';
        words[80] = 'Eighty';
        words[90] = 'Ninety';
        amount = amount.toString();
        var atemp = amount.split(".");
        var number = atemp[0].split(",").join("");
        var n_length = number.length;
        var words_string = "";
        if (n_length <= 9) {
            var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
            var received_n_array = new Array();
            for (var i = 0; i < n_length; i++) {
                received_n_array[i] = number.substr(i, 1);
            }
            for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                n_array[i] = received_n_array[j];
            }
            for (var i = 0, j = 1; i < 9; i++, j++) {
                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    if (n_array[i] == 1) {
                        n_array[j] = 10 + parseInt(n_array[j]);
                        n_array[i] = 0;
                    }
                }
            }
            value = "";
            for (var i = 0; i < 9; i++) {
                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    value = n_array[i] * 10;
                } else {
                    value = n_array[i];
                }
                if (value != 0) {
                    words_string += words[value] + " ";
                }
                if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Crores ";
                }
                if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Lakhs ";
                }
                if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                    words_string += "Thousand ";
                }
                if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                    words_string += "Hundred and ";
                } else if (i == 6 && value != 0) {
                    words_string += "Hundred ";
                }
            }
            words_string = words_string.split("  ").join(" ");
        }
        return words_string;
    }
</script>
@endpush

