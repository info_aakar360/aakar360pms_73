<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-pencil"></i> Reporting Manager</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'editreportDirectTeam', 'class'=>'form-horizontal ','method'=>'POST')) !!}

    <div class="form-body">
        <div class="row">
            <div id="addMoreBox1" class="clearfix">
                <input type="number" style="display: none" name="user_id" value="{{$userid}}">
                <div class="col-md-12">
                    <div class="form-group ">
                        <select name="employee" class="form-control" id="employee_id">
                            <option value=""> Select Employee</option>
                            @foreach($employees as $employee)
                                <option @if($reportingteam->team_id == $employee->id) selected @endif value="{{$employee->id}}">{{get_employee_name($employee->id)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button"  class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="storeManager()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>

<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    function storeManager() {
        $.easyAjax({
            url: '{{route('admin.employee.updateDirectReportTeam')}}',
            container: '#editreportDirectTeam',
            type: "POST",
            redirect: true,
            data: $('#editreportDirectTeam').serialize(),
            success: function (response) {
                location.reload();
            }
        })
    }


</script>

