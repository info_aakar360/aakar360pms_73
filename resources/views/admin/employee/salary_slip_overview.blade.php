<div class="row dashboard-stats">
    <div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title"> Total Employees</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">Payroll Processed</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">Gross Pay</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">Net Pay</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">Income Tax</span><br>
                        <span class="counter">{{$tax->incometax}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div><div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">PF</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div><div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">ESI</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div><div class="col-md-3 col-sm-6">
        <a href="{{ route('admin.clients.index') }}">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-3">
                        <div>
                            <span class="bg-success-gradient"><i class="icon-user"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-9 text-right">
                        <span class="widget-title">PT</span><br>
                        <span class="counter">{{$counts->totalEmployees}}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
