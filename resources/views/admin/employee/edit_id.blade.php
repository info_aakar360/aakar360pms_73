<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Document</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <form method="post" id="createBoqCategory" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Select Document Type</label>
                        <select name="document_type" id="parent" class="form-control">
                            <option value="">Select Document Type</option>
                            @foreach($types as $type)
                                <option value="{{ $type->id }}" @if($doc->doc_type == $type->id) selected @endif>{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @if(config('filesystems.default') == 'local')
                        <img src="{{ asset_url('documents/'.$doc->filename) }}" style="width: 100px; height: 100px;">
                    @elseif(config('filesystems.default') == 's3')
                        <img src="{{ $url.'documents/'.$doc->filename }}"  style="width: 100px; height: 100px;">
                    @elseif(config('filesystems.default') == 'google')
                        <img src="{{ $doc->filename }}"  style="width: 100px; height: 100px;">
                    @elseif(config('filesystems.default') == 'dropbox')
                        <img src="{{ $doc->filename }}"  style="width: 100px; height: 100px;">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Document</label>
                        <input type="file" name="file" id="image" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        </form>
    </div>
</div>

<script>
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.employee.updateId',[$id])}}',
            container: '#createBoqCategory',
            type: "POST",
            file: (document.getElementById("image").files.length == 0) ? false : true,
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>