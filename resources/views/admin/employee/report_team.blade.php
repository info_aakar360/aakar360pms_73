@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employee.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="sttabs tabs-style-line col-md-12">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li><a href="{{route('admin.employee.edit', [$userDetail->id])}}"><span>@lang('modules.employee.general')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.employee_job_profile', [$userDetail->id])}}"><span>@lang('modules.employee.job')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.employee_profile_documents', [$userDetail->id])}}"><span>@lang('modules.employee.documents')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{route('admin.employee.team', [$userDetail->id])}}"><span>@lang('modules.employee.team')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.education', [$userDetail->id])}}"><span>@lang('modules.employee.education')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.family', [$userDetail->id])}}"><span>@lang('modules.employee.family')</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'reportTeam','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="col-md-12">
                                <label>REPORTING MANAGER</label><br>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($employeeTeam) == 0)
                                        <tr>
                                            <td colspan="4">No Primary managers assigned.<br>
                                            No Secondary managers assigned.</td>
                                        </tr>
                                    @endif
                                    @foreach($employeeTeam as $et)
                                        <tr>
                                            <td>
                                                {{ get_employee_name($et->team_id) }}
                                            </td>
                                            <td>
                                                {{ $et->type }}
                                            </td>
                                            <td>
                                                {{ get_department_name($et->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_designation_name($et->team_id) }}
                                            </td>
                                            <td>
                                                <button type="button"  class="btn btn-sm btn-primary" onclick="editReportingmanager('{{$et->id}}')"><i class="fa fa-pencil"></i></button>
                                                <button type="button"  class="btn btn-sm btn-danger" onclick="deleteManager('{{$et->id}}')"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <div id="insertBefore"></div>
                                <div class="clearfix">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                        Add <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<button type="submit" id="saveManager" class="btn btn-sm btn-info" style="margin-bottom: 20px; float: right;">--}}
                                        {{--Save <i class="fa fa-plus"></i>--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="saveManager" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>

            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'directReport','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="form-body">
                        <div class="col-md-12">
                            <label>DIRECT REPORT</label><br>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($employeeDirect as $ed)
                                        <input type="number" name="user_id" style="display: none" value="{{$ed->id}}">
                                        <tr>
                                            <td>
                                                {{ get_employee_name($ed->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_department_name($ed->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_designation_name($ed->team_id) }}
                                            </td>
                                            <td>
                                                <button type="button"  class="btn btn-sm btn-primary" onclick="editDirectReportingmanager('{{$ed->id}}')" data-id="{{$ed->team_id}}" ><i class="fa fa-pencil"></i></button>
                                                <button type="button"  class="btn btn-sm btn-danger" onclick="deleteDirectManager('{{$ed->id}}')"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div id="directinsertBefore"></div>
                            <div class="clearfix">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" id="addButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                        Add <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                         <div class="form-actions">
                             <button type="submit" id="saveDirect" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                         </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTeamModel" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Edit Direct Report Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editDirectModel" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script>
        var $insertBefore = $('#insertBefore');
        var $i = 0;
        // Add More Inputs
        $('#plusButton').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-5"><input type="hidden" name="user_id" value="{{ $userDetail->id }}"><div class="form-group "><select name="employee['+$i+']" class="form-control" id="employee_id'+indexs+'"><option value=""> Select Employee</option></select></div></div>' +
            '<div class="col-md-5"style="margin-left:5px;"><div class="form-group"><select name="type['+$i+']" class="form-control" id="type['+$i+']"   ><option value=""> Select Type</option><option value="primary"> Primary</option><option value="secondary">Seondary</option></select></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);
            $('#employee_id'+indexs).html('{!! $employeelists !!}');
        });

        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        var $directinsertBefore = $('#directinsertBefore');
        var $i = 0;
        // Add More Inputs
        $('#addButton').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-10"><div class="form-group "><input type="hidden" name="user_id" value="{{ $userDetail->id }}"><select name="employee['+$i+']" class="form-control" id="employee_id'+indexs+'"><option value=""> Select Employee</option></select></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($directinsertBefore);
            $('#employee_id'+indexs).html('{!! $employeelists !!}');
        });
        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        $('#saveManager').click(function () {
            $.easyAjax({
                url: '{{route('admin.employee.storeReportTeam')}}',
                container: '#reportTeam',
                type: "POST",
                redirect: true,
                data: $('#reportTeam').serialize(),
                success: function (response) {
                    location.reload();
                }
            })
        });

        $('#saveDirect').click(function () {
            $.easyAjax({
                url: '{{route('admin.employee.storeDirectTeam')}}',
                container: '#directReport',
                type: "POST",
                redirect: true,
                data: $('#directReport').serialize(),
                success: function (response) {
                        location.reload();
                }
            })
        });
        function editReportingmanager(id){
            event.preventDefault();
           $('#modelHeading').html("EDIT REPORTING MANAGER");
            var url = '{{ route('admin.employee.editReportingManager', ':id')}}';
            url = url.replace(':id', id);
            $.ajaxModal('#editTeamModel', url);
        }
        function editDirectReportingmanager(id){
            event.preventDefault();
           $('#modelHeading').html("EDIT DIRECT REPORTING MANAGER");
            var url = '{{ route('admin.employee.editDirectReportingManager', ':id')}}';
            url = url.replace(':id', id);
            $.ajaxModal('#editDirectModel', url);
        }
        function deleteManager(id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.employee.deleteReportTeam',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status === "success") {
                                location.reload();
                            }
                        }
                    });
                }
            });
          }
          function deleteDirectManager(id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.employee.deleteDirectReportTeam',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status === "success") {
                                location.reload();
                            }
                        }
                    });
                }
            });
          }
    </script>
@endpush

