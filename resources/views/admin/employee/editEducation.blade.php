<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-pencil"></i> Edit Education Details</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'editreportTeam', 'class'=>'form-horizontal ','method'=>'POST')) !!}
    <div class="form-body">
        <div class="row">
            <div id="addMoreBox1" class="clearfix">
                <input type="number" style="display: none" name="user_id" value="{{$userid}}">
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>Qualification</label>
                        <select  class="form-control" id="qualification_type" name="qualification_type">
                            <option value="">Select Qualification Type</option>
                            <option @if($educationDetail->qualification_type == 'graduation') selected @endif value="graduation">Graduation</option>
                            <option @if($educationDetail->qualification_type == 'post_graduation') selected @endif value="post_graduation">Post Graduation</option>
                            <option @if($educationDetail->qualification_type == 'doctorate') selected @endif value="doctorate">Doctorate</option>
                            <option @if($educationDetail->qualification_type == 'diploma') selected @endif value="diploma">Diploma</option>
                            <option @if($educationDetail->qualification_type == 'pre_univercity') selected @endif value="pre_univercity">Pre Univercity</option>
                            <option @if($educationDetail->qualification_type == 'other_education') selected @endif value="other_education">Other Education</option>
                            <option @if($educationDetail->qualification_type == 'certification') selected @endif value="certification">Certification</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 "style="margin-left:5px;">
                    <div class="form-group">
                        <label>Course</label>
                        <input class="form-control " name="course_name" id = "course_name" type="text" value="{{$educationDetail->course_name}}" placeholder="Course Name"/>
                    </div>
                </div>
                <div class="col-md-3 "style="margin-left:5px;">
                    <div class="form-group">
                        <label>Course Type</label>
                        <select class="form-control " name="course_type" id="course_type" >
                            <option value="">Select Course Type</option>
                            <option @if($educationDetail->course_type == 'full_time') selected @endif value="full_time">Full Time</option>
                            <option @if($educationDetail->course_type == 'part_time') selected @endif value="part_time">Part Time</option>
                            <option @if($educationDetail->course_type == 'correpondence') selected @endif value="correpondence">Correspondence</option>
                            <option @if($educationDetail->course_type == 'certificate') selected @endif value="certificate">Certificate</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 "style="margin-left:5px;">
                    <div class="form-group">
                        <label>Stream</label>
                        <input class="form-control " name="stream" id="stream" type="text" value="{{$educationDetail->stream}}" placeholder="Stream"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>Start Date</label>
                        <input autocomplete="off" class="form-control date-picker" id="startDate" name="startDate" data-date-format="dd/mm/yyyy" type="text" value="{{$educationDetail->course_start_date}}" placeholder="Course Start Date"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label>End Date</label>
                        <input autocomplete="off" class="form-control date-picker" id="endDate" name="endDate" data-date-format="dd/mm/yyyy" type="text" value="{{$educationDetail->course_end_date}}" placeholder="Course End Date"/>
                    </div>
                </div>
                <div class="col-md-5 "style="margin-left:5px;">
                    <div class="form-group">
                        <label>College Name</label>
                        <input class="form-control " name="college_name" id="college_name" type="text" value="{{$educationDetail->college_name}}" placeholder="College Name"/>
                    </div>
                </div>
                <div class="col-md-5 "style="margin-left:5px;">
                    <div class="form-group">
                        <label>University Name</label>
                        <input class="form-control " name="univercity_name" id="univercity_name" type="text" value="{{$educationDetail->univercity_name}}" placeholder="University Name"/>
                    </div>
                </div>
            </div>
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button"  class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="storeManager()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>

<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    function storeManager() {
        $.easyAjax({
            url: '{{route('admin.employee.updateEducation')}}',
            container: '#editreportTeam',
            type: "POST",
            redirect: true,
            data: $('#editreportTeam').serialize(),
            success: function (response) {
                location.reload();
            }
        })
    }
    jQuery('#startDate').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });// Recently Added End date picker assign
    jQuery('#endDate').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

</script>

