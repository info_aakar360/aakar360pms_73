
@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employee.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="sttabs tabs-style-line col-md-12">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li><a href="{{route('admin.employee.edit', [$userDetail->id])}}"><span>@lang('modules.employee.general')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.employee_job_profile', [$userDetail->id])}}"><span>@lang('modules.employee.job')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.employee_profile_documents', [$userDetail->id])}}"><span>@lang('modules.employee.documents')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.team', [$userDetail->id])}}"><span>@lang('modules.employee.team')</span></a>
                                </li>
                                <li><a href="{{route('admin.employee.education', [$userDetail->id])}}"><span>@lang('modules.employee.education')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{route('admin.employee.family', [$userDetail->id])}}"><span>@lang('modules.employee.family')</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'familyDetail','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            {{--<div class="col-md-12">--}}
                                {{--<label>FAMILY MEMBER</label><br>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<label>Name</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<label>Relationship</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<label>Phone Number</label>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Relationship</th>
                                    <th>Phone Number</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($familyDetail as $et)
                                    <tr>
                                        <td>
                                            {{ $et->family_memeber_name }}
                                        </td>
                                        <td>
                                            {{ $et->family_memeber_relation }}
                                        </td>
                                        <td>
                                            {{ $et->family_memeber_number }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12">
                                <div id="contactinsertBefore"></div>
                                <div class="clearfix">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="button" id="addButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                            Add<i class="fa fa-plus"></i>
                                        </button>
                                    </div>

                                </div>

                            </div>
                            <div class="form-actions">
                                 <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                 <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                             </div>
                            {!! Form::hidden('employee_id', $userDetail->id) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>    <!-- .row -->
    </div>
        @endsection

        @push('footer-script')
            <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
            <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script>

                var $contactinsertBefore = $('#contactinsertBefore');
                var $i = 0;
                // Add More Inputs
                $('#addButton').click(function(){

                    $i = $i+1;
                    var indexs = $i+1;
                    $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
                        '<div class="col-md-4"><div class="form-group "><input autocomplete="off" class="form-control" name="emg_name['+$i+']" id="name['+$i+']"  type="text" value="" placeholder="Name"/></div></div>' +
                        '<div class="col-md-3 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="emg_relation['+$i+']" id="relation['+$i+']" type="text" value="" placeholder="Relationship"/></div></div>' +
                        '<div class="col-md-3 "style="margin-left:5px;"><div class="form-group"><input class="form-control "  onkeypress="return isNumberKey(event)" required maxlength="10" pattern="\\d{10}" name="number['+$i+']" id= "number['+$i+']" type="text" value=""  placeholder="Phone Number"/></div></div>' +
                        '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($contactinsertBefore);

                        // Recently Added statrt date picker assign
                        jQuery('#dob'+indexs).datepicker({
                            autoclose: true,
                            todayHighlight: true,
                            weekStart:'{{ $global->week_start }}',
                            format: '{{ $global->date_picker_format }}',
                        });
                });
                // Remove fields
                function removeBox(index){
                    $('#addMoreBox'+index).remove();
                }

                $('#save-form').click(function () {
                    var url = "{{ route('admin.employee.storeFamilyDetail') }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        container: '#familyDetail',
                        data: $('#familyDetail').serialize(),
                        success: function (response) {
                            location.reload();
                        }
                    });
                });

            </script>
    @endpush