@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->

    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}"><!--Owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.css') }}"><!--Owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.theme.default.css') }}"><!--Owl carousel CSS -->

    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

    <style>
        .col-in {
            padding: 0 20px !important;

        }
         .swal-button--confirm {
             background-color: #DD6B55;
         }

        .fc-event{
            font-size: 10px !important;
        }

        @media (min-width: 769px) {
            #wrapper .panel-wrapper{
                height: 500px;
                overflow-y: auto;
            }
        }

    </style>
@endpush

@section('content')

    <div class="row dashboard-stats">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">@lang('app.select') @lang('app.month')</label>
                <select class="select2 form-control" data-placeholder="" id="month">
                    <option @if($month == '01') selected @endif value="01">@lang('app.january')</option>
                    <option @if($month == '02') selected @endif value="02">@lang('app.february')</option>
                    <option @if($month == '03') selected @endif value="03">@lang('app.march')</option>
                    <option @if($month == '04') selected @endif value="04">@lang('app.april')</option>
                    <option @if($month == '05') selected @endif value="05">@lang('app.may')</option>
                    <option @if($month == '06') selected @endif value="06">@lang('app.june')</option>
                    <option @if($month == '07') selected @endif value="07">@lang('app.july')</option>
                    <option @if($month == '08') selected @endif value="08">@lang('app.august')</option>
                    <option @if($month == '09') selected @endif value="09">@lang('app.september')</option>
                    <option @if($month == '10') selected @endif value="10">@lang('app.october')</option>
                    <option @if($month == '11') selected @endif value="11">@lang('app.november')</option>
                    <option @if($month == '12') selected @endif value="12">@lang('app.december')</option>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">@lang('app.select') @lang('app.year')</label>
                <select class="select2 form-control" data-placeholder="" id="year">
                    @for($i = $year; $i >= ($year-4); $i--)
                        <option @if($i == $year) selected @endif value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group m-t-25">
                <button type="button" id="apply-filter" class="btn btn-success btn-block">@lang('app.apply')</button>
            </div>
        </div>
        </div>
    </div>
    <!-- .row -->
    <!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab"><a href="#overview" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('app.menu.overview')</span> </a> </li>
                    <li class="tab"><a href="#attendances" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">@lang('app.menu.attendance')</span> </a> </li>
                    <li class="tab"><a href="#leave" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs">@lang('app.menu.leave')</span> </a> </li>
                    <li class="tab"><a href="#salaryrevision" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-list"></i></span> <span class="hidden-xs">@lang('app.menu.salaryrevision')</span> </a> </li>
                    <li class="tab"><a href="#variableandadhoc" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-logout"></i></span> <span class="hidden-xs">@lang('app.menu.variableandadhoc')</span> </a> </li>
                    <li class="tab"><a href="#salaryonhold" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.salaryonhold')</span> </a> </li>
                    <li class="tab"><a href="#taxoverride" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.taxoverride')</span> </a> </li>
                    <li class="tab"><a href="#runpayroll" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.runpayroll')</span> </a> </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="overview">

                    </div>
                    <div class="tab-pane" id="attendances">
                        <div class="table-responsive">
                        <div class="form-group">
                            <a @if(in_array('attendance',$verified)) class="btn btn-success btn-sm" @else onclick="varify('attendance')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('attendance',$verified)) <i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                        </div>
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                               id="attendance_overall" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employee.employeeName')</th>
                                    <th>@lang('modules.employee.absentdate')</th>
                                    <th>@lang('modules.employee.absenddays')</th>
                                    <th>@lang('modules.employee.outstandinganamoly')</th>
                                    <th>@lang('modules.employee.lopdays')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="leave">
                        <div class="table-responsive">
                        <div class="form-group">
                            <a  @if(in_array('leave',$verified)) class="btn btn-success btn-sm" @else onclick="varify('leave')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('leave',$verified))<i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                        </div>
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                               id="leave-table" style="width: 100%">
                            <thead>
                            <tr>
                                <th>@lang('app.id')</th>
                                <th>@lang('modules.employee.employeeName')</th>
                                <th>@lang('modules.employee.date')</th>
                                <th>@lang('modules.employee.totaldays')</th>
                                <th>@lang('modules.employee.leavetype')</th>
                                <th>@lang('app.status')</th>
                                <th>@lang('modules.employee.manager')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                     <div class="tab-pane" id="salaryrevision">
                         <div class="table-responsive">
                             <div class="form-group">
                                 <a  @if(in_array('salrevision',$verified)) class="btn btn-success btn-sm" @else onclick="varify('salrevision')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('salrevision',$verified)) <i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                             </div>
                               <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                      id="salaryrevision-table" style="width: 100%">
                                   <thead>
                                   <tr>
                                   <tr>
                                       <th>@lang('app.id')</th>
                                       <th>@lang('modules.employee.employeeName')</th>
                                       <th>@lang('modules.employee.oldctc')</th>
                                       <th>@lang('modules.employee.newctc')</th>
                                       <th>@lang('modules.employee.changes')</th>
                                       <th>@lang('modules.payrollsettings.effectivedate')</th>
                                       <th>@lang('modules.employee.comment')</th>
                                   </tr>
                                   </tr>
                                   </thead>
                               </table>
                           </div>
                       </div>
                       <div class="tab-pane" id="variableandadhoc">
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="white-box">
                                       <ul class="nav nav-tabs tabs customtab">
                                           <li class="active tab"><a href="#variable" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('app.menu.variable')</span> </a> </li>
                                           <li class="tab"><a href="#adhoc" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">@lang('app.menu.adhoc')</span> </a> </li>
                                       </ul>
                                       <div class="tab-content">
                                       <div class="tab-pane active" id="variable">
                                           <div class="table-responsive">
                                           <div class="form-group">
                                               <a  @if(in_array('variable',$verified)) class="btn btn-success btn-sm" @else onclick="varify('variable')" class="btn btn-outline btn-success btn-sm" @endif> @if(in_array('variable',$verified)) <i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                                           </div>
                                           <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                                  id="variableandadhoc-table" style="width: 100%">
                                               <thead>
                                               <tr>
                                                   <th>@lang('app.id')</th>
                                                   <th>@lang('modules.employee.employeeName')</th>
                                                   <th>@lang('modules.employee.variableType')</th>
                                                   <th>@lang('modules.employee.amount')</th>
                                                   <th>@lang('modules.employee.payaction')</th>
                                                   <th>@lang('modules.employee.comment')</th>
                                                   <th>@lang('app.status')</th>
                                               </tr>
                                               </thead>
                                           </table>
                                           </div>
                                       </div>
                                       <div class="tab-pane" id="adhoc">
                                           <div class="table-responsive">
                                           <div class="form-group">
                                               <a onclick="adhocAction()" class="btn btn-outline btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>@lang('modules.employee.addNewEmployee') </a>
                                           </div>
                                           <div class="form-group">
                                               <a @if(in_array('adhoc',$verified)) class="btn btn-success btn-sm" @else onclick="varify('adhoc')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('adhoc',$verified)) <i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                                           </div>
                                           <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                                  id="adhocAction-table" style="width: 100%">
                                               <thead>
                                               <tr>
                                                   <th>@lang('app.id')</th>
                                                   <th>@lang('modules.employee.employeeName')</th>
                                                   <th>@lang('modules.employee.compotype')</th>
                                                   <th>@lang('modules.employee.componame')</th>
                                                   <th>@lang('app.amount')</th>
                                                   <th>@lang('app.action')</th>
                                               </tr>
                                               </thead>
                                           </table>
                                           </div>
                                       </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane" id="salaryonhold">
                           <div class="table-responsive">
                           <div class="form-group">
                               <a onclick="payAction()" class="btn btn-outline btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>@lang('modules.employee.addNewEmployee') </a>
                           </div>
                           <div class="form-group">
                               <a @if(in_array('salhold',$verified)) class="btn btn-success btn-sm" @else onclick="varify('salhold')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('salhold',$verified))<i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif</a>
                           </div>
                           <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                  id="salaryonhold-table" style="width: 100%">
                               <thead>
                               <tr>
                                   <th>@lang('app.id')</th>
                                   <th>@lang('modules.employee.employeeName')</th>
                                   <th>@lang('modules.employee.payaction')</th>
                                   <th>@lang('modules.employee.startperiod')</th>
                                   <th>@lang('modules.employee.comment')</th>
                                   <th>@lang('app.status')</th>
                               </tr>
                               </thead>
                           </table>
                           </div>
                       </div>
                       <div class="tab-pane" id="taxoverride">
                           <div class="table-responsive">
                           <div class="form-group">
                               <a onclick="addEmployee()" class="btn btn-outline btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>@lang('modules.employee.addNewEmployee') </a>
                           </div>
                           <div class="form-group">
                               <a  @if(in_array('tax',$verified)) class="btn btn-success btn-sm" @else onclick="varify('tax')" class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('tax',$verified))<i class="fa fa-check" aria-hidden="true"></i>Verified @else Verify @endif </a>
                           </div>
                           <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                  id="taxoverride-table" style="width: 100%">
                               <thead>
                               <tr>
                                   <th>@lang('app.id')</th>
                                   <th>@lang('modules.employee.employeeName')</th>
                                   <th>@lang('modules.declaration.incometax')</th>
                                   <th>@lang('modules.employee.incometax-ytd')</th>
                                   <th>@lang('modules.employee.comment')</th>
                                   <th>@lang('app.status')</th>
                               </tr>
                               </thead>
                           </table>
                           </div>
                       </div>
                    <div class="tab-pane" id="runpayroll">
                        <div class="table-responsive">
                            <div class="form-group">
                                <a  @if(in_array('attendance',$verified)&&in_array('leave',$verified)&&in_array('salrevision',$verified)&&in_array('variable',$verified)&&in_array('adhoc',$verified)&&in_array('salhold',$verified)&&in_array('tax',$verified)) class="btn btn-success btn-sm" onclick="runPayroll()" @else  class="btn btn-outline btn-success btn-sm" @endif>@if(in_array('payroll',$verified))<i class="fa fa-check" aria-hidden="true"></i>Payroll Processed @else Run Payroll @endif</a>
                            </div>
                           <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                  id="runpayroll-table" style="width: 100%">
                               <thead>
                                   <tr>
                                       <th>@lang('app.id')</th>
                                       <th id="payid">user id</th>
                                       <th>@lang('modules.employee.employeeName')</th>
                                       <th>@lang('modules.employee.location')</th>
                                       @if($ptsettings[0]->pt_applicable == 'on')
                                       <th>@lang('modules.employee.pt-location')</th>
                                       @endif
                                       <th>@lang('app.department')</th>
                                       <th>@lang('app.designation')</th>
                                       <th>@lang('modules.employee.pan')</th>
                                       @if($pfesidata[0]->pf_applicable == 'on')
                                       <th>@lang('modules.employee.pf_number')</th>
                                       @endif
                                       @if($pfesidata[0]->esi_applicable == 'on')
                                       <th>@lang('modules.employee.esic_number')</th>
                                       <th>@lang('modules.employee.uan_number')</th>
                                       @endif
                                       <th id ="workingday">@lang('modules.employee.working_days')</th>
                                       <th id="paylop">@lang('modules.employee.lop')</th>
                                       <th id="paystatus">@lang('modules.employee.payoutstatus')</th>
                                       <th id="paygross">@lang('modules.employee.gross')</th>
                                       @if($ptsettings[0]->pt_applicable == 'on')
                                       <th id="paypt">@lang('modules.employee.pt')</th>
                                       @endif
                                       <th id="payincome">@lang('modules.employee.incometax')</th>
                                       <th id="netpay">@lang('modules.employee.netpay')</th>
                                   </tr>
                               </thead>
                           </table>
                            <div class="row">
                                <div class="form-group">
                                    <a  onclick="revert()" class="btn btn-outline btn-danger">Revert</a>
                                    <a  onclick="preview()" class="btn btn-outline btn-primary">Preview Paysilp</a>
                                    <a  onclick="payGenerate()" class="btn btn-outline btn-primary">Generate Payslip</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="employeeTaxDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="payActionModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="employeeTaxDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="employeeAdhocDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="employeeLopDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editSalaryDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTaxDetailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="previewSlip" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}


    <div class="modal fade bs-example-modal-lg" id="leave-details" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}



@endsection


@push('footer-script')


    <script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/morrisjs/morris.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>

    <!-- jQuery for carousel -->
    <script src="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/owl.carousel/owl.custom.js') }}"></script>

    <!--weather icon -->
    <script src="{{ asset('plugins/bower_components/skycons/skycons.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/calendar/dist/locale-all.js') }}"></script>
    <script src="{{ asset('js/event-calendar.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        $('#apply-filter').click(function () {
            loadTable();
        });
        function addEmployee() {
            var url = '{{route('admin.declaration.employeeTaxDetail')}}';
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#employeeTaxDetailModal',url);
        }
        function payAction() {
            var url = '{{route('admin.declaration.salaryOnHold')}}';
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#employeeTaxDetailModal',url);
        }
        function adhocAction() {
            var url = '{{route('admin.declaration.adhocAction')}}';
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#employeeAdhocDetailModal',url);
        }
        function addLop(id) {
            var url = '{{route('admin.declaration.addLop')}}';
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#employeeLopDetailModal',url);
        }
        function editAdhoc(id) {
            var url = '{!! route('admin.declaration.editAdhocAction') !!}?id='+id;
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#employeeAdhocDetailModal',url);
        }
        function editSalaryOnHold(id) {
            var url = '{!! route('admin.declaration.editSalaryOnHold') !!}?id='+id;
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#editSalaryDetailModal',url);
        }
        function editTaxOverride(id) {
            var url = '{!! route('admin.declaration.editTaxOverride') !!}?id='+id;
            $('#modelHeading').html('@lang('modules.employee.addNewEmployee')');
            $.ajaxModal('#editTaxDetailModal',url);
        }
        function runPayroll() {
            jsonObj = [];
            var month = $("#month").val();
            var year = $("#year").val();
           /* $('#runpayroll-table tbody tr').each( (tr_idx,tr) => {
                $(tr).children('td').each( (td_idx, td) => {
                console.log( '[' +tr_idx+ ',' +td_idx+ '] => ' + $(td).text());
                 jsonObj.push({tridx:tr_idx,tdidx:td_idx,fields:$(td).text()});
                });
            });*/
           $('#runpayroll-table td').each(function(e) {
                jsonObj.push($(this).html());
                /*$.each(this.cells, function(){
                    jsonObj.push($(this).html());
                });*/
            });
            var jsonstring = JSON.stringify(jsonObj)
            var url = '{{route('admin.declaration.payregister')}}';
            $.easyAjax({
                type: 'POST',
                url: url,
                data: { 'month': month, 'year': year,'jsonstring': jsonstring, '_token': '{{ csrf_token() }}' },
                success: function (response) {
                   window.location.reload();
                }
            });
        }
        function preview() {
            var url = '{{route('admin.declaration.previewSlip')}}';
            $('#modelHeading').html('Preview Slip');
            $.ajaxModal('#previewSlip',url);
        }
        function payGenerate() {
            var user_id = '1';
            var month = $("#month").val();
            var year = $("#year").val();


        }
        var table;
        $(function() {
            loadTable();

        });
        function loadTable(){
            var year = $('#year').val();
            var month = $('#month').val();

            $.ajax({
                url: "{{route('admin.employee.getSalaryOvervies')}}",
                type: 'POST',
                // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: {

                    _token:     '{{ csrf_token() }}'
                },success:function(data){
                    $('#overview').html(data);
                }
            })

            table = $('#attendance_overall').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getAttendanceOverallData') !!}?'+'month=' +month+'&year='+year,

                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'absentdates', name: 'absentdates' },
                    { data: 'absentdays', name: 'absentdays' },
                    { data: 'autstandingannamoly', name: 'autstandingannamoly'},
                    { data: 'lopdays', name: 'lopdays'},
                    { data: 'action', name: 'action', width: '15%' }
                ]

        });
            table = $('#leave-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getLeaveData') !!}?'+'month=' +month+'&year='+year,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'date', name: 'date' },
                    { data: 'totaldays', name: 'totaldays' },
                    { data: 'leave_type', name: 'leave_type'},
                    { data: 'status', name: 'status'},
                    { data: 'manager', name: 'manager'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
            table = $('#salaryrevision-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getSalaryRevision') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'oldctc', name: 'oldctc' },
                    { data: 'newctc', name: 'newctc' },
                    { data: 'changes', name: 'changes'},
                    { data: 'effectivedate', name: 'effectivedate'},
                    { data: 'comment', name: 'comment', width: '15%' }

                ]
            });
            table = $('#variableandadhoc-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getVariableAdhoc') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'variabletype', name: 'variabletype' },
                    { data: 'amount', name: 'amount' },
                    { data: 'payaction', name: 'payaction'},
                    { data: 'comment', name: 'comment'},
                    { data: 'action', name: 'action', width: '15%' }

                ]
            });table = $('#adhocAction-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getAdhocData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'compotype', name: 'compotype' },
                    { data: 'componame', name: 'componame' },
                    { data: 'amount', name: 'amount'},
                    { data: 'action', name: 'action', width: '15%' }

                ]
            });
            table = $('#salaryonhold-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.geSalaryOnHold') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'payaction', name: 'payaction' },
                    { data: 'startperiod', name: 'startperiod' },
                    { data: 'comment', name: 'comment'},
                    { data: 'action', name: 'action', width: '15%' }

                ]
            });
            table = $('#taxoverride-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.geTaxOverride') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'incometax', name: 'incometax' },
                    { data: 'incometax-ytd', name: 'incometax-ytd' },
                    { data: 'comment', name: 'comment'},
                    { data: 'action', name: 'action', width: '15%' }

                ]
            });
            table = $('#runpayroll-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.getPreview') !!}?'+'month=' +month+'&year='+year,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    @if($ptsettings[0]->pt_applicable == 'on')
                    { data: 'pt-location', name: 'pt-location' },
                    @endif
                    { data: 'department', name: 'department'},
                    { data: 'designation', name: 'designation' },
                    { data: 'pan', name: 'pan' },
                    @if($pfesidata[0]->pf_applicable == 'on')
                    { data: 'pf_number', name: 'pf_number'},
                    @endif
                    @if($pfesidata[0]->esi_applicable == 'on')
                    { data: 'esic_number', name: 'esic_number' },
                    { data: 'uan_number', name: 'uan_number' },
                    @endif
                    { data: 'workingdays', name: 'workingdays'},
                    { data: 'lop', name: 'lop' },
                    { data: 'payoutstatus', name: 'payoutstatus' },
                    { data: 'gross', name: 'gross' },
                    @if($ptsettings[0]->pt_applicable == 'on')
                    { data: 'pt', name: 'pt' },
                    @endif
                    { data: 'incometax', name: 'incometax' },
                    { data: 'netpay', name: 'netpay' },

                ]
            });

        }

        $('#leave-table').on('click', '.leave-action-reject', function () {
            console.log('test');
            var action = $(this).data('leave-action');
            var leaveId = $(this).data('leave-id');
            var searchQuery = "?leave_action="+action+"&leave_id="+leaveId;
            var url = '{!! route('admin.leaves.show-reject-modal') !!}'+searchQuery;
            $('#modelHeading').html('Reject Reason');
            $.ajaxModal('#eventDetailModal', url);
        });

        $('#leave-table').on('click', '.leave-action', function() {
            var action = $(this).data('leave-action');
            var leaveId = $(this).data('leave-id');
            var url = '{{ route("admin.leaves.leaveAction") }}';

            $.easyAjax({
                type: 'POST',
                url: url,
                data: { 'action': action, 'leaveId': leaveId, '_token': '{{ csrf_token() }}' },
                success: function (response) {
                    if(response.status == 'success'){
                        /*window.location.reload();*/
                        loadTable();
                    }
                }
            });

        })

        $('body').on('click', '.show-leave', function () {
            var leaveId = $(this).data('leave-id');

            var url = '{{ route('admin.leaves.show', ':id') }}';
            url = url.replace(':id', leaveId);

            $('#modelHeading').html('Leave Details');
            $.ajaxModal('#leave-details', url);
        });

        $('body').on('click', '.nofitytomanager', function () {
            alert('hello');
        });

        $('#pending-leaves').click(function() {
            window.location = '{{ route("admin.leaves.pending") }}';
        })

        function varify(value) {
            var month = $("#month").val();
            var url = '{{route("admin.declaration.verify") }}';
            $.easyAjax({
                type: 'POST',
                url: url,
                data: { 'value': value, 'month': month, '_token': '{{csrf_token()}}'},
                success: function (response) {
                        window.location.reload();
                }
            });

        }
    </script>

@endpush