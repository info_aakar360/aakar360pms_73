@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">


        <div class="col-md-12">
            <div class="white-box">

                <div class="row" >
                    <div class="col-md-12"   >
                    <form action="{{ route('admin.role-permission.save-permission') }}"  method="post" id="storeTask" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="employee_id" value="{{ $employee->id }}" />
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Permission for</label>
                                <p><strong>{{ $employee->name }}</strong></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">@lang('app.projects')</label>
                                <select class="form-control select2" required name="project_id" id="projects" data-style="form-control">
                                    <option value="">@lang('modules.permission.selectProject')</option>
                                    @forelse($projectlist as $projects)
                                        <option value="{{$projects->id}}" @if($projects->id==$projectid) selected @endif>{{ ucfirst($projects->project_name) }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label col-md-12">@lang('modules.permission.sharetoproject')</label>
                                <label class="switch">
                                    <input type="checkbox" name="sharetoproject" @if($sharetoproject=='1') checked @endif value="1" class="sharetoproject" >
                                    <div class="slider round customercheckbox"><!--ADDED HTML --><span class="on"></span><span class="off"></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label col-md-12">@lang('modules.permission.caneditpermission')</label>
                                <label class="switch">
                                    <input type="checkbox" name="checkpermission"  @if($checkpermission=='1') checked @endif  value="1" class="caneditpermission" >
                                    <div class="slider round customercheckbox"><!--ADDED HTML --><span class="on"></span><span class="off"></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pull-right">
                                <label class="control-label col-xs-12">&nbsp;</label>
                                <button type="button"  id="save-form" class="btn btn-success col-md-6"><i class="fa fa-check"></i> @lang('app.apply')</button>
                                <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="accordion" id="myAccordion">
                        @foreach($modulesData as $moduleData)
                                <div class="card">
                                    <div class="card-header" id="heading{{ $moduleData->module_name }}">
                                        <h2 class="mb-0">
                                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $moduleData->module_name }}">
                                                @lang('modules.module.'.$moduleData->module_name)
                                                @if($moduleData->description != '')
                                                    <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">{{ $moduleData->description  }}</span></span></span></a>
                                                @endif
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapse{{ $moduleData->module_name }}" class="collapse in" aria-labelledby="heading{{ $moduleData->module_name }}" data-parent="#myAccordion">
                                        <div class="card-body">
                                            <div class="col-md-12 mb-20">
                                            @foreach($moduleData->permissions as $permission)
                                                <div class="col-md-3">
                                                    <div class="switchery-demo">
                                                        <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" class="js-switch assign-role-permission" @if(!empty($sharepermissions)&&in_array($permission->id,$sharepermissions)) checked @endif  /> {{ $permission->display_name }}
                                                    </div>
                                                </div>
                                            @endforeach

                                             </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach

                    </div>
                    </div>

                    </form>
                </div>
                </div>
                </div>


            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>


    $("#projects").change(function(){
        var projectid = $(this).val();
        var employeeid = '{{ $employee->id }}';
        var url = "{{ route('admin.role-permission.permission',[':id',':projectid']) }}";
        url = url.replace(':id', employeeid);
        url = url.replace(':projectid', projectid);
        window.location.href = url;
    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    var table;

    $(function() {
        loadTable();

        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('user-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.employees.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.role-permission.save-permission')}}',
                container: '#storeTask',
                type: "POST",
                data: $('#storeTask').serialize(),
                success: function (data) {

                }
            })
        });
    });
    function loadTable(){

        var employee = $('#employee').val();
        var status   = $('#status').val();
        var role     = $('#role').val();
        var skill     = $('#skill').val();
        var designation     = $('#designation').val();
        var department     = $('#department').val();
        var projects     = $('#projects').val();

        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.role-permission.employeedata') !!}?projects=' + projects + '&employee=' + employee + '&status=' + status + '&role=' + role + '&skill=' + skill + '&designation=' + designation + '&department=' + department,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'status', name: 'status'},
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action', width: '15%' }
            ]
        });
    }

    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle('slide');
    })

    $('#apply-filters').click(function () {
        loadTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        loadTable();
    })

    function exportData(){

        var employee = $('#employee').val();
        var status   = $('#status').val();
        var role     = $('#role').val();

        var url = '{{ route('admin.employees.export', [':status' ,':employee', ':role']) }}';
        url = url.replace(':role', role);
        url = url.replace(':status', status);
        url = url.replace(':employee', employee);

        window.location.href = url;
    }

</script>
@endpush