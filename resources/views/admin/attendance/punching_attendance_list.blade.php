<div class="row">
    <div class="col-md-12">
        <div class="white-box">

            <div class="panel panel-inverse">

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'attendance-container-'.Auth::id(),'class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 id="attendance-table">
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.timeLogs.employeeName')</label>
                                    <select class="select2 form-control" data-placeholder="Choose Employee" id="user_id" name="user_id">
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-info" onclick="setClockIn('{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}')";>@lang('modules.attendance.clock_in')</button>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <input type="text" name="clock_in" id="clock_is_in_time" class="form-control"
                                                   autocomplete="nope" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-info" onclick="setClockOut('{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}');">@lang('modules.attendance.clock_out')</button>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <input type="text" name="clock_out" id="clock_is_out_time" class="form-control"
                                                   autocomplete="nope" value="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">@lang('modules.attendance.remark')</label>
                                    <input type="text" name="remark id="remark" class="form-control"
                                    autocomplete="nope">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>@lang('modules.profile.profilePicture')</label>
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 300px; height: 250px;">
                                            <img src="https://via.placeholder.com/300x250.png?text={{ str_replace(' ', '+', __('modules.profile.uploadPicture')) }}"   alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 300px; max-height: 250px;"></div>
                                        <div>
                                            <div id="my_result"></div>

                                            <button class="btn btn-info save-attendance" data-user-id="{{ Auth::id() }}"><a href="javascript:void(take_snapshot())">@lang('modules.attendance.clock_in')</a> </button>
                                            <button class="btn btn-info save-attendance" data-user-id="{{ Auth::id() }}"><a href="javascript:void(take_snapshot())">@lang('modules.attendance.clock_out')</a> </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!--/span-->

                    </div>
                    <div class="form-actions">
                        {{-- <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        <button type="reset" class="btn btn-default">@lang('app.reset')</button> --}}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script>
    Webcam.attach( '.thumbnail' );

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }

    function setClockOut(time) {
        $('#clock_is_out_time').val(time);
    };
    function setClockIn(time) {
        $('#clock_is_in_time').val(time);
    }
</script>