@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">

@endpush

@section('content')
    {{--Loader--}}
    <div class="combo-sheet formula-sheet">
        <div class="loaderx">
            <div class="cssload-speeding-wheel"></div>
        </div>
    </div>
    {{--End Loader--}}
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="panel panel-inverse">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            {!! Form::open(['id'=>'attendance-container-'.Auth::id(),'class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6" id="attendance-table">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.menu.attendance') @lang('app.date')</label>
                                        <input type="text" class="form-control" name="attendance_date" id="attendance_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                        <label class="control-label">@lang('modules.timeLogs.employeeName')</label>
                                        <select class="select2 form-control" data-placeholder="Choose Employee" id="user_id" name="user_id" onchange="getData(this.value)">
                                            <option value="" >Please Select</option>
                                            @foreach($employees as $employee)
                                                <option data-user-id = "{{$employee->user_id}}" value="emp-{{ $employee->id }}" >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <button class="btn btn-info" id="clockinbutton" style="width: 87px;" onclick="setClockIn('clockin')";>@lang('modules.attendance.clock_in')</button>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" name="clock_in" id="clock_is_in_time" class="form-control" autocomplete="off" value="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <button class="btn btn-info" id="clockoutbutton" onclick="setClockOut('clockout');">@lang('modules.attendance.clock_out')</button>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" name="clock_out" id="clock_is_out_time" class="form-control" autocomplete="nope" value="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>@lang('modules.profile.profilePicture')</label>
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 300px; height: 230px;">
                                                <img src="https://via.placeholder.com/300x250.png?text={{ str_replace(' ', '+', __('modules.profile.uploadPicture')) }}"   alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 250px;"></div>
                                            <div>
                                                <div id="my_result"></div>
                                            </div>
                                            <input type="hidden" id="btnpressed" value="">
                                            <input type="hidden" id="attendance_id" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="departmentModel" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script language="JavaScript">
        Webcam.attach( '.thumbnail' );
        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                document.getElementById('my_result').innerHTML = '<img id="base64image" src="'+data_uri+'"/>';
            } );
        }
        function setClockIn(btnpressed) {
            $('#clock_is_in_time').val('{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}');
            $('#btnpressed').val(btnpressed);
            saveattendance();
        }
        function setClockOut(btnpressed) {
            $('#clock_is_out_time').val('{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}');
            $('#btnpressed').val(btnpressed);
            saveattendance();

        };
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        function getData (val) {
            var id = val;
            var date = $('#attendance_date').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.declaration.getData')}}',
                container: '#rules-container',
                type: "POST",
                data: {_token: token, id: id, date: date},
                beforeSend: function(){
                    $('#clockinbutton').prop('disabled', true);
                    $('#clockoutbutton').prop('disabled', true);

                },
                success: function (data) {
                    $('#clock_is_in_time').val(data.clock_in);
                    $('#attendance_id').val(data.id);
                },
                complete:function(data){
                    // Hide image container
                    $('#clockinbutton').prop('disabled', false);
                    $('#clockoutbutton').prop('disabled', false);
                },
            })
        };

        function saveattendance()
        {
            take_snapshot();
            var url = '';
            var attid = $('#attendance_id').val();
            var userId = $('#user_id').val();
            var datauserId = $('#user_id').attr('data-user-id');
            var clockInTime = $('#clock_is_in_time').val();
            var clockOutTime = $('#clock_is_out_time').val();
            var btnpressed = $('#btnpressed').val();
            if(btnpressed=='clockin'){
                url = '{{route('admin.attendances.store')}}';
            } else {
                url = '{{route('admin.attendances.updateattendance',':id')}}';
                url = url.replace(':id', attid);
            }
            var workingFrom = 'office';
            var date = $('#attendance_date').val();
            var late = 'no';
            if($('#late-'+userId).is(':checked')){
                late = 'yes';
            }
            var halfDay = 'no';
            if($('#halfday-'+userId).is(':checked')){
                halfDay = 'yes';
            }
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                type: "POST",
                container: '#attendance-container-'+userId,
                data: {
                    id: attid,
                    user_id: userId,
                    clock_in_time: clockInTime,
                    clock_in_ip: "::1",
                    clock_out_time: clockOutTime,
                    half_day: halfDay,
                    btnpressed: btnpressed,
                    working_from: workingFrom,
                    file: (document.getElementById("base64image").src == 0) ? false : document.getElementById("base64image").src,
                    date: date,
                    _token: token
                },
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }

                }
            })
        }
    </script>

@endpush