@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.attendances.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.attendance.markAttendance')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

<link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="sttabs tabs-style-line col-md-12">
                <div class="white-box">
                    <nav>
                        <ul>
                            <li class="tab-current"><a href="{{route('admin.attendances.create')}}">By Date</span></a>
                            </li>
                            <li><a href="{{route('admin.attendances.singleAttendanceRange')}}">By Date Range</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="white-box">
                <div class="row">
                   <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">@lang('app.menu.attendance') @lang('app.date')</label>
                            <input type="text" class="form-control" name="attendance_date" id="attendance_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">@lang('modules.timeLogs.employeeName')</label>
                            <select class="select2 form-control" data-placeholder="Choose Employee" id="user_id" name="user_id">
                                <option   value="">Select Option</option>
                                @foreach($empdata as $emp)
                                    <option   value="emp-{{ $emp->id }}">{{ ucwords($emp->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="tableBox">
                    <table  id="attendance-table" width="100%">
                    </table>
                </div>
                <div id="holidayBox" style="display: none">
                    <div class="alert alert-primary"> @lang('modules.attendance.holidayfor') <span id="holidayReason"> </span>. </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="attendancesDetailsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

<script>


    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    checkHoliday();
    jQuery("#user_id").on('change',function (ev) {
        checkHoliday();
    });
    jQuery('#attendance_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        endDate: '+0d',
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    }).on('changeDate', function (ev) {
        checkHoliday();
    });
    function checkHoliday() {
        var selectedDate =$('#attendance_date').val();
        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: '{{route('admin.attendances.check-holiday')}}',
            type: "GET",
            data: {
                date: selectedDate,
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    if(response.holiday != null){
                        $('#holidayBox').show();
                        $('#tableBox').hide();
                        $('#holidayReason').html(response.holiday.occassion);
                    }else{
                        $('#holidayBox').hide();
                        $('#tableBox').show();
                        showTable();
                    }

                }
            }
        })
    }

    var table;

    function showTable(){

        table = $('#attendance-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            bFilter: false,
            bInfo: false,
            ajax: {
                url: "{!! route('admin.attendances.data' ) !!}",
                data: function (d) {
                    console.log(d);
                    d.date = $('#attendance_date').val();
                    d.user_id = $('#user_id').val();

                }
            },
            "bStateSave": true,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            // "aoColumns": [
            //     {'sClass': 'center', "bSortable": false}
            // ],
            // "columnDefs": [{
            //     "defaultContent": "-",
            //     "targets": "_all"
            // }],

            columns: [
            { data: 'id', name: 'id', width:50 },
        ],

            "fnDrawCallback": function (oSettings) {
                $(oSettings.nTHead).hide();
                $('.a-timepicker').timepicker({
                    @if($global->time_format == 'H:i')
                    showMeridian: false,
                    @endif
                    minuteStep: 1,
                    defaultTime: false

                });

                $('.b-timepicker').timepicker({
                    @if($global->time_format == 'H:i')
                    showMeridian: false,
                    @endif
                    minuteStep: 1,
                    defaultTime: false
                });

                $('#attendance-table_wrapper').removeClass( 'form-inline' );

                // Switchery
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                $('.js-switch').each(function() {
                    new Switchery($(this)[0], $(this).data());

                });

            },

            "destroy" : true
        });


    }

    $('#attendance-table').on('click', '.save-attendance', function () {
        var userId = $(this).data('user-id');
        var user ='';
       if(userId.startsWith("emp-")){
           user = userId.slice(4);
       }else{
           user = userId;
       }

        var clockInTime = $('#clock-in-'+user).val();
       // var clockInIp = $('#clock-in-ip-'+userId).val();
        var clockOutTime = $('#clock-out-'+user).val();
       // var clockOutIp = $('#clock-out-ip-'+userId).val();
        var workingFrom = 'Office';
        var date = $('#attendance_date').val();

        var late = 'no';
        if($('#late-'+user).is(':checked')){
            late = 'yes';
        }
        var halfDay = 'no';
        if($('#halfday-'+user).is(':checked')){
            halfDay = 'yes';
        }
        var token = "{{ csrf_token() }}";

        $.easyAjax({
            url: '{{route('admin.attendances.store')}}',
            type: "POST",
            container: '#attendance-container-'+user,
            data: {
                user_id: userId,
                clock_in_time: clockInTime,
                clock_in_ip: "::1",
                clock_out_time: clockOutTime,
               // clock_out_ip: clockOutIp,
               // late: late,
                half_day: halfDay,
                working_from: workingFrom,
                date: date,
                _token: token
            },
            success: function (response) {
                if(response.status == 'success'){
                    showTable();
                }
            }
        })
    })

    // Attendance Detail
    function attendanceDetail(id,attendanceDate) {
        var url = "{{ route('admin.attendances.detail') }}?userID="+id+"&date="+attendanceDate;

        $('#modelHeading').html('@lang('modules.attendance.attendanceDetail')');
        $.ajaxModal('#attendancesDetailsModal',url);
    }
    //Check Out Time
    function setClockOut(id,time) {
        $('#clock-out-'+id).val(time)
    };

    //Check In Time
    function setClockIn(id,time) {

        $('#clock-in-'+id).val(time)
    }

</script>
@endpush