<?php  $absentdata = array(); $halfdaydata = array();  ?>
@foreach($final as $key => $attendance)
    @foreach($attendance as $key2=>$day)
        @if($key === $id.'#'.$name)
            <td class="text-center">
                @if($day == 'Anomaly Absent' || $day == 'Absent')
                    <?php
                        $absentdata[] = array_push($absentdata,$key2);
                    ?>
                @endif
               @if($day == 'Half Day')
                    <?php
                        $halfdaydata[] = array_push($halfdaydata,$key2);
                    ?>
                @endif
            </td>
        @endif
    @endforeach
@endforeach
<?php  $lop = ((count($absentdata)/2) + (count($halfdaydata)/4));  ?>
{{ $lop }}