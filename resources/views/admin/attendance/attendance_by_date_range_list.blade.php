<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="col-md-1 col-xs-2">
                    {!!  ($row->image) ? '<img src="'.asset('user-uploads/avatar/'.$row->image).'" alt="user" class="img-circle" width="40">' : '<img src="'.asset_url('user.png').'" alt="user" class="img-circle" width="40">' !!}
                </div>
                <div class="col-md-8 col-xs-6">
                    {{ ucwords($row->name) }} <br>
                    <span class="font-light text-muted">{{ ucfirst($row->designation_name) }}</span>
                </div>
                <div class="col-md-3 col-xs-4">
                    @if($row->clock_in > 0)
                        <label class="label label-success"><i class="fa fa-check"></i> @lang('modules.attendance.present')</label>
                        <button type="button" title="Attendance Detail" class="btn btn-info btn-sm btn-rounded" onclick="attendanceDetail('{{ $row->id }}', '{{ \Carbon\Carbon::createFromFormat('Y-m-d', $row->atte_date)->timezone($global->timezone)->format('Y-m-d')   }}')">
                            <i class="fa fa-search"></i> Detail
                        </button>
                    @else
                        <label class="label label-danger"><i class="fa fa-exclamation-circle"></i> @lang('modules.attendance.absent')</label>
                    @endif

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row">
                        @if($row->total_clock_in < $maxAttandenceInDay)
                            {!! Form::open(['id'=>'attendance-container-'.$row->id,'class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="form-body ">
                                @for($i=0;$i<=$different_day;$i++)

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>@lang('app.date')</label>
                                                <label id="current_date<?php echo $i; ?>">@if($i>0){{$startDate->addDays(1)->format($global->date_format)}}@else {{$startDate->format($global->date_format)}}@endif</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <button type="button"  class="btn btn-info" onclick="setClockIn('{{ $i }}','{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}');">@lang('modules.attendance.clock_in')</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input type="text" name="clock_in_time" id="clock-in-{{ $i }}"
                                                               class="form-control a-timepicker"   autocomplete="off"  @if($row->id == Auth::id())  disabled @endif
                                                               @if(!is_null($row->clock_in_time) || !empty($row->clock_in_time)) value="{{ \Carbon\Carbon::parse($row->clock_in_time)->timezone($global->timezone)->format($global->time_format) }}"  @endif>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <button type="button" class="btn btn-info " onclick="setClockOut('{{ $i }}','{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',\Carbon\Carbon::now()->toDateTimeString())->timezone($global->timezone)->format($global->time_format) }}');">@lang('modules.attendance.clock_out')</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="input-group bootstrap-timepicker timepicker">
                                                        <input type="text" name="clock_out_time" id="clock-out-{{ $i }}"
                                                               class="form-control b-timepicker"   autocomplete="off" @if($row->id == Auth::id())  disabled @endif
                                                               @if(!is_null($row->clock_out_time)) value="{{ \Carbon\Carbon::parse($row->clock_out_time)->timezone($global->timezone)->format($global->time_format)}}" @endif>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-success text-white save-attendance" @if($table =='employee')
                                                        data-user-id="emp-{{ $row->id }}" @else data-user-id="{{ $row->id }}" @endif data-index="{{$i}}"><i
                                                            class="fa fa-check"></i> @lang('app.save')</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                @endfor
                            </div>

                    </div>
                    {!! Form::close() !!}
                    @else
                        <div class="col-xs-12">
                            <div class="alert alert-info">@lang('modules.attendance.maxColckIn')</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>