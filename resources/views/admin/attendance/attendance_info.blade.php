<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" ><i class="icon-clock"></i> @lang('app.menu.attendance') @lang('app.details') </h4>
    @if(!empty($attendanceData->id) && !empty($roleuser))
        <button class="btn btn-primary pull-right"  data-id="{{$attendanceData->id}}" name="markabsent"  id="showupdatediv">Update Attendance</button>
        <button class="btn btn-primary pull-right"  data-id="{{$attendanceData->id}}" name="markabsent" style="margin-right:10px;" onclick="deleteAttendance({{$attendanceData->id}})">Mark Absent</button>
    @endif
</div>
<div class="modal-body">
    <div class="row" style="display: none;" id="updatedata">
            {!! Form::open(['id'=>'attendance-container','class'=>'ajax-form','method'=>'POST']) !!}
            {{ csrf_field() }}
            <input type="hidden" name="date" value="{{ $startTime->format($global->date_format) }}">
            <input type="hidden" name="user_id" value="{{ $attendanceData->emp_id }}">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-5">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <label>@lang('modules.attendance.clock_in') </label>
                            <input type="text" name="clock_in_time"
                                   class="form-control a-timepicker"   autocomplete="off"   id="clock-in-time"
                                   @if(!empty($row->clock_in_time)) value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_in_time)->timezone($global->timezone)->format($global->time_format) }}" @endif>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <label>@lang('modules.attendance.clock_out')</label>
                            <input type="text" name="clock_out_time" id="clock-out"
                                   class="form-control b-timepicker"   autocomplete="off"
                                   @if(!empty($row->clock_out_time)) value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->clock_out_time)->timezone($global->timezone)->format($global->time_format) }}" @endif>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="button" class="btn btn-success text-white save-attendance"><i
                                        class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
    </div>
     <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="card punch-status">
                <div class="white-box">
                    <h4>@lang('app.menu.attendance') <small class="text-muted">{{ $startTime->format($global->date_format) }}</small></h4>
                    <div class="punch-det">
                        <div class="col-md-12" style="display: flex;">
                            <div class="col-md-4">
                                <h6>@lang('modules.attendance.clock_in')</h6>
                                <p>{{ $attendanceData->clock_in_time->format($global->time_format) }}</p>
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <img src="../../{{$attendanceData->clock_in_image}}" alt="clock-in-image" class="pull-right" style="width: 132px;height: 124px;margin-top: -10px;margin-right: -30px;">
                            </div>
                        </div>
                        <div class="col-md-12" style="display: flex">
                            <div class="col-md-3">
                                <div id="map" style="width: 110px;height: 110px;margin-top: -10px;margin-right: -30px;margin-left: -31px;"></div>
                               {{-- <a href="#open-modal3"><button type="button"  class="btn btn-info btn-sm btn-rounded" onclick="setinmap('{{$attendanceData->clock_in_lat}}','{{$attendanceData->clock_in_lang}}')">
                                        <i class="fa fa-map-marker"></i> Location
                                    </button></a>--}}
                            </div>
                            <div class="col-md-9">
                                <h6>Address</h6>
                                <p>{{$attendanceData->clock_in_address}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="punch-info">
                        <div class="punch-hours">
                            <span>{{ $totalTime }} hrs</span>
                        </div>
                    </div>
                    <div class="punch-det">
                        <div class="col-md-12" style="display: flex;">
                            <div class="col-md-4">
                                <h6>@lang('modules.attendance.clock_out')</h6>
                                <p>@if(!empty($attendanceData->clock_out_time)){{ $attendanceData->clock_out_time->format($global->time_format) }} @endif
                                    @if (isset($notClockedOut))
                                        (@lang('modules.attendance.notClockOut'))
                                    @endif</p>
                            </div>
                            <div class="col-md-4">
                                <div id="map1"></div>
                                <a href="#open-modal3"><button type="button"  class="btn btn-info btn-sm btn-rounded" onclick="setinmap('{{$attendanceData->clock_out_lat}}','{{$attendanceData->clock_out_lang}}')">
                                    <i class="fa fa-map-marker"></i> Location
                                    </button></a>
                                {{--<p>{{$attendanceData->clock_out_lat}}</p>
                                <p>{{$attendanceData->clock_out_lang}}</p>--}}
                            </div>
                            <div class="col-md-4">
                                <img src="../../{{$attendanceData->clock_out_image}}" alt="clock-in-image" class="pull-right" style="width: 132px;height: 124px;margin-top: -10px;margin-right: -30px;">
                            </div>
                        </div>
                        <h6>Address</h6>
                        <p>{{$attendanceData->clock_out_address}}</p>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-md-6">
            <div class="card recent-activity">
                <div class="white-box">
                    <h5 class="card-title">@lang('modules.employees.activity')</h5>
                    @foreach ($attendanceActivity->reverse() as $item)
                        <div class="row res-activity-box" id="timelogBox{{ $item->aId }}">
                            <ul class="res-activity-list col-md-9">
                                <li>
                                    <p class="mb-0">@lang('modules.attendance.clock_in')</p>
                                    <p class="res-activity-time">
                                        <i class="fa fa-clock-o"></i>
                                        {{ $item->clock_in_time->timezone($global->timezone)->format($global->time_format) }}.
                                    </p>
                                </li>
                                <li>
                                    <p class="mb-0">@lang('modules.attendance.clock_out')</p>
                                    <p class="res-activity-time">
                                        <i class="fa fa-clock-o"></i>
                                        @if (!is_null($item->clock_out_time))
                                            {{ $item->clock_out_time->timezone($global->timezone)->format($global->time_format) }}.
                                        @else
                                            @lang('modules.attendance.notClockOut')
                                        @endif
                                    </p>
                                </li>
                            </ul>
                            <div class="col-md-3">
                                <a href="javascript:;" onclick="editAttendance({{ $item->aId }})" style="display: inline-block;" id="attendance-edit" data-attendance-id="{{ $item->aId }}" ><label class="label label-info"><i class="fa fa-pencil"></i> </label></a>
                                <a href="javascript:;" onclick="deleteAttendance({{ $item->aId }})" style="display: inline-block;" id="attendance-edit" data-attendance-id="{{ $item->aId }}" ><label class="label label-danger"><i class="fa fa-times"></i></label></a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>--}}
    </div>
</div>
<script>
    var map, marker, infoWindow, title, contentString, directionsService, directionsDisplay, pos;
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        const uluru = { lat: 21.2620511, lng: 81.5490322 };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 4,
            center: uluru,
            disableDefaultUI: true,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
        });

    }

    $('.a-timepicker').timepicker({
        @if($global->time_format == 'H:i')
        showMeridian: false,
        @endif
        minuteStep: 1
    });
    $('.b-timepicker').timepicker({
        @if($global->time_format == 'H:i')
        showMeridian: false,
        @endif
        minuteStep: 1,
    });
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());

    });
    $('#showupdatediv').click(function () {
        $("#updatedata").toggle();
    });
    $('#attendance-container').on('click','.save-attendance', function () {
        var url = '{{route('admin.attendances.updateattendance',$attendanceData->id)}}';
        $.easyAjax({
            url: url,
            type: "POST",
            container: '#attendance-container',
            data: $('#attendance-container').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#projectTimerModal').modal('hide');
                }
            }
        })
    });
    function deleteAttendance(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted Attendance!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                var url = "{{ route('admin.attendances.destroy',':id') }}";
                url = url.replace(':id', id);
                var token = "{{ csrf_token() }}";
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();

                            $('#timelogBox'+id).remove();
//                                    swal("Deleted!", response.message, "success");
                            showTable();
                            $('#projectTimerModal').modal('hide');
                        }
                    }
                });
            }
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_OBgIAejX2JWtPU1o2Qqhx9NNnaT4uYg&callback=initMap"></script>