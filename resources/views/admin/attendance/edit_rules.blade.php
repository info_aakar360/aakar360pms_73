@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">

@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab"><a href="#general_rules" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('modules.attendance.generalrules')</span> </a> </li>
                    <li class="tab"><a href="#advance_rules" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">@lang('modules.attendance.advancerules')</span> </a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="general_rules">
                        <div class="steamline">
                            {!! Form::open(['id'=>'rules-container','class'=>'ajax-form','method'=>'POST','autocomplete'=>'off']) !!}
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <span class="font-light text-muted">General Rules</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label>Rule Name</label>
                                        <input type="text" name="rulename" value="{{$rules->name}}" contentEditable class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label>Description</label>
                                        <textarea  name="description"  class="form-control">{{$rules->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                        <label style="padding: 5px;">
                                            <input type="checkbox" name="shifttime" id="shifttime" value="1" @if(!empty(strtotime($rules->shift_in_time))&&!empty(strtotime($rules->shift_out_time))&&strtotime($rules->shift_in_time)>0&&strtotime($rules->shift_out_time)>0) checked @endif class="form-check-input">
                                            Enable Shift Timings
                                        </label>
                                    </div>
                                    <div id="shifttimefield" style="display: @if(!empty(strtotime($rules->shift_in_time))&&!empty(strtotime($rules->shift_out_time))&&strtotime($rules->shift_in_time)>0&&strtotime($rules->shift_out_time)>0) block @else none @endif;">
                                        <h3>Shift Timings</h3>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label>@lang('modules.attendance.clock_in')</label>
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input type="text" name="shift_in_time" id="shift_in_time"
                                                       class="form-control a-timepicker"     value="{{\Carbon\Carbon::parse($rules->shift_in_time)->timezone($global->timezone)->format($global->time_format) }}">
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.attendance.clock_out')</label>
                                                <div class="input-group bootstrap-timepicker timepicker">
                                                    <input type="text" name="shift_out_time" id="shift_out_time"
                                                           class="form-control b-timepicker"     value="{{\Carbon\Carbon::parse($rules->shift_out_time)->timezone($global->timezone)->format($global->time_format) }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Anomaly Settings</h3><br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('modules.attendance.clock_in')</label><br>
                                            <label>In Time Grace Period (In Minuts)</label>
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input type="number" name="anomaly_in_time" id="anomaly_in_time"
                                                       class="form-control"     value="{{$rules->anomaly_grace_in}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('modules.attendance.clock_out')</label><br>
                                            <label>Out Time Grace Period (In Minuts)</label>
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input type="number" name="anomaly_out_time" id="anomaly_out_time"
                                                       class="form-control"     value="{{$rules->anomaly_grace_out}}">
                                            </div>
                                        </div>
                                    </div>

                                    <h3>Work Duration</h3><br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Full day (In Hours)</label>
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input type="number" name="full_day" id="full_day"
                                                       class="form-control "     value="{{$rules->work_full_time}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Half day (In Hours)</label>
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input type="number" name="half_day" id="half_day"
                                                       class="form-control "     value="{{$rules->work_half_time}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>Auto Clock Out  </span><input type="checkbox"  id="auto_clock_out" name="auto_clock_out"  value="{{$rules->auto_clock_out}}" @if($rules->auto_clock_out ==1) checked @endif>
                                        </div>
                                    </div>
                                     <div class="form-actions">
                                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                @lang('app.save')
                                            </button>
                                            <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
                                      </div>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="tab-pane" id="advance_rules">
                        <div class="row">
                            {{--span--}}
                            {!! Form::open(['id'=>'advance-rules-container','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="col-md-12">

                                <div class="col-lg-12" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                    <label style="padding: 5px;">
                                        <input type="checkbox" name="overtime" @if(!empty($adrules->enable_overtime)) value="{{$adrules->enable_overtime}}" @endif class="form-check-input" @if(!empty($adrules->enable_overtime)&&$adrules->enable_overtime==1) {{ 'checked' }}  @endif>
                                        Enable Overtime
                                    </label>
                                </div>
                                <div class="col-lg-12" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                                    <label style="padding: 5px;">
                                        <input type="checkbox" name="penaltyrules" id= "penaltyrules" value="" class="form-check-input" @if(!empty($adrules->rules_id)&&!empty($rules->id)&&$adrules->rules_id==$rules->id) {{ 'checked' }}  @endif>
                                        Enable Penalty Rules
                                    </label>
                                </div>
                                <div id="autoUpdate" class="input-group" style="display: none">

                                    <h3>In Time
                                        <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2"></span></span></span></a>
                                    </h3>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label>Early Leaving Allowed</label>
                                            <input type="number" name="yearlyleave" id="yearlyleave" class="form-control" @if(!empty($adrules->in_time_leave_allow)) value="{{$adrules->in_time_leave_allow}}" @endif>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Penalty</label>
                                            <input type="number" name="penaly"  id = "penaly" class="form-control" @if(!empty($adrules->in_time_penalty)) value="{{$adrules->in_time_penalty}}" @endif>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Leave Deduction</label>
                                            <input type="number" name="leavededuction" id= "leavededuction" class="form-control" @if(!empty($adrules->in_time_deduction)) value="{{$adrules->in_time_deduction}}" @endif>
                                        </div>

                                    </div>

                                {{--span--}}
                                {{--span--}}
                                <div class="col-md-12">

                                    <h3>Out Time
                                        <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2"></span></span></span></a></h3>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label>Early Leaving Allowed</label>
                                            <input type="number" name="outyearlyleave"  id = "outyearlyleave" class="form-control" @if(!empty($adrules->out_time_leave_allow)) value="{{$adrules->out_time_leave_allow}}" @endif>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Penalty</label>
                                            <input type="number" name="outpenaly" id= "outpenaly" class="form-control" @if(!empty($adrules->out_time_penalty)) value="{{$adrules->out_time_penalty}}" @endif>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Leave Deduction</label>
                                            <input type="number" name="outleavededuction"  id= "outleavededuction" class="form-control" @if(!empty($adrules->out_time_deduction)) value="{{$adrules->out_time_deduction}}" @endif>
                                        </div>

                                    </div>
                                </div>
                                {{--span--}}
                                {{--span--}}
                                <div class="col-md-12">

                                    <h3>Work Duration
                                        <a class="mytooltip" href="javascript:void(0)"> <i class="fa fa-info-circle"></i><span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2"></span></span></span></a></h3>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label>Early Leaving Allowed</label>
                                            <input type="number" name="workyearlyleave" class="form-control" @if(!empty($adrules->work_time_leave_allow)) value="{{$adrules->work_time_leave_allow}}" @endif>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Penalty</label>
                                            <input type="number" name="workpenalty" class="form-control" @if(!empty($adrules->work_time_penalty)) value="{{$adrules->work_time_penalty}}" @endif>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Leave Deduction</label>
                                            <input type="number" name="workleavededuction" class="form-control" @if(!empty($adrules->work_time_deduction)) value="{{$adrules->work_time_deduction}}" @endif>
                                        </div>

                                    </div>
                                </div>
                                {{--span--}}
                                <div class="form-actions">
                                    <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                                        @lang('app.save')
                                    </button>
                                    <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
                                </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

    <script>
        $('#shifttime').change(function () {
            if (!this.checked){

                $('#shifttimefield').fadeOut('slow');
                $('#shift_in_time').val("");
                $('#shift_out_time').val("");

            }else{
                $('#shifttimefield').fadeIn('slow');
            }
        });
        $('.a-timepicker').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false,
            @endif
            minuteStep: 1,
            defaultTime: false

        });

        $('.b-timepicker').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false,
            @endif
            minuteStep: 1,
            disableFocus: true,
            defaultTime: false
        });
        $('.grace-a-timepicker').timepicker({
            @if($global->time_format == 'h:mm')
            showMeridian: false,
            @endif
            minuteStep: 1,
            defaultTime: false
        });

        $('.grace-b-timepicker').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false,
            @endif
            minuteStep: 1,
            defaultTime: false
        });
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.attendances.updateGeneralRules',[$rules->id])}}',
                container: '#rules-container',
                type: "POST",
                redirect: true,
                data: $('#rules-container').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.href = '{{ route('admin.attendances.rules') }}';
                    }
                }
            })
        });
        $('#penaltyrules').change(function () {
            if (!this.checked)

                $('#autoUpdate').fadeOut('slow');
            else
                $('#autoUpdate').fadeIn('slow');
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.attendances.updateAdvanceRules',[$rules->id])}}',
                container: '#advance-rules-container',
                type: "POST",
                redirect: true,
                data: $('#advance-rules-container').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.href = '{{ route('admin.attendances.rules') }}';
                    }
                }
            })
        });

    </script>
@endpush

