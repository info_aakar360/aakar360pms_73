@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab"><a href="#attendacerule" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Attendace Rule</span> </a> </li>
                                <li class="tab"><a href="#assignattendacerule" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Assign Attendace Rule</span> </a> </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="attendacerule">
                        <div class="form-group">
                            <a href="{{ route('admin.attendances.createrules') }}" class="btn btn-outline btn-success btn-sm">@lang('modules.attendance.creatnewrules') <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table" width="100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.sno')</th>
                                    <th>@lang('app.name')</th>
                                    <th>@lang('modules.attendance.description')</th>
                                    <th >@lang('modules.attendance.shift_in_time')</th>
                                    <th>@lang('modules.attendance.shift_out_time')</th>
                                    <th>@lang('modules.attendance.anomaly_grace_in_time')</th>
                                    <th>@lang('modules.attendance.anomaly_grace_out_time')</th>
                                    <th>@lang('modules.attendance.work_full_time')</th>
                                    <th>@lang('modules.attendance.work_half_time')</th>
                                    <th>@lang('app.action')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="assignattendacerule">
                        <div class="form-group">
                            <a onclick="addWorkRules()" class="btn btn-outline btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i>@lang('modules.leaves.assignRule') </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="rules-table" width="100%">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" id="master"></th>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('app.department')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.payrollsettings.effectivedate')</th>
                                    <th>@lang('modules.attendance.ruleapplied')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="attendancesRuleAssignModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        // Assign Rules
        function assignRules(id) {


            var url = '{{ route('admin.attendances.assignRules', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('@lang('modules.attendance.assignrules')');
            $.ajaxModal('#attendancesRuleAssignModal',url);
        }

        $('#master').on('click', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub_chk").prop('checked', true);
            } else {
                $(".sub_chk").prop('checked',false);
            }
        });
        function addWorkRules() {
            var allVals = [];
            $(".sub_chk:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });
            if(allVals.length <=0)
            {
                alert("Please select atleast one employee.");
            }  else {

                var join_selected_values = allVals.join(",");
                var ids = "?ids="+join_selected_values;
                var url = '{{route('admin.attendances.assignRules')}}'+ids;
                $('#modelHeading').html('@lang('modules.leaves.addNewRules')');
                $.ajaxModal('#attendancesRuleAssignModal',url);

            }

        }

        var table;

        $(function() {
            loadTable();

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.attendances.deleteRules',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });


        });
        function loadTable(){


            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.attendances.rulesdata') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'description', name: 'description' },
                    { data: 'shift_in_time', name: 'shift_in_time' },
                    { data: 'shift_out_time', name: 'shift_out_time'},
                    { data: 'anomaly_grace_in_time', name: 'anomaly_grace_in_time'},
                    { data: 'anomaly_grace_out_time', name: 'anomaly_grace_out_time'},
                    { data: 'work_full_time', name: 'work_full_time' },
                    { data: 'work_half_time', name: 'work_half_time' },
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
            table = $('#rules-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.attendances.attendaceRulesData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'department', name: 'department' },
                    { data: 'location', name: 'location' },
                    { data: 'effective_date', name: 'effective_date' },
                    { data: 'rulesapplied', name: 'rulesapplied' },
                ]
            });

        }



    </script>
@endpush