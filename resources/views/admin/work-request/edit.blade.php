@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.work-order') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">

                <div class="panel-heading"> #{{ $workrequest->id }} - Edit Work Request
                    <!-- div style="float: right;">

                            <button type="button" class="btn btn-danger" >Decline</button>



                            <button type="submit" class="btn btn-primary" >Approve</button>


                    </div -->

                </div>

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateWorkRequestf','class'=>'ajax-form','method'=>'POST']) !!}
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Title</label>
                            <input type="text" class="form-control" id="inputName" name="title" value="{{$workrequest->title}}" placeholder="Enter  Title"/>
                            <input type="hidden" class="form-control" id="inputName" name="work_order_id" value="{{ $workrequest->id }}"/>

                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Description</label>
                            <input type="text" class="form-control" id="inputName" name="description" value="{{$workrequest->description}}" placeholder="Enter  Description"/>
                        </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Complaint By Name</label>
                            <input type="text" class="form-control" id="inputName" name="user_name" value="{{$workrequest->user_name}}" placeholder="Enter  Name"/>
                        </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputName">Phone Number</label>
                            <input type="text" class="form-control" id="inputName" name="user_phone" value="{{$workrequest->user_phone}}" placeholder="Enter  Phone Number"/>
                        </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Email</label>
                            <input type="text" class="form-control" id="inputName" name="user_email" value="{{$workrequest->user_email}}"  placeholder="Enter  Email"/>
                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Priority</label>

                            <select class="form-control selectpicker"  name="request_priority">
                                <?php
                                if($workrequest->request_priority == 0){

                                    echo '<option data-tokens="ketchup mustard" value="0" selected>Low</option>';
                                    echo '<option data-tokens="ketchup mustard" value="1">Medium</option>';
                                    echo '<option data-tokens="ketchup mustard" value="2">High</option>';
                                }
                                if($workrequest->request_priority == 1){

                                    echo '<option data-tokens="ketchup mustard" value="0">Low</option>';
                                    echo '<option data-tokens="ketchup mustard" value="1" selected>Medium</option>';
                                    echo '<option data-tokens="ketchup mustard" value="2">High</option>';
                                }
                                if($workrequest->request_priority == 2){

                                    echo '<option data-tokens="ketchup mustard" value="0">Low</option>';
                                    echo '<option data-tokens="ketchup mustard" value="1" selected>Medium</option>';
                                    echo '<option data-tokens="ketchup mustard" value="2">High</option>';
                                }


                                ?>





                            </select>
                        </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Image</label>

                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="request_image" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="taskID" id="taskID">
                                </div>
                            </div>



                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">PDF</label>

                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="request_pdf" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="taskID" id="taskID">
                                </div>
                            </div>




                        </div>
                            </div>
                        </div>
                        <div class="panel-heading"> Option</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Select Status</label>

                                    <select class="form-control selectpicker request_status"  name="status_id">

                                        <?php
                                        if($workrequest->status_id == 0 ){

                                          echo  '<option data-tokens="ketchup mustard" selected>Select Status</option>
                                                 <option data-tokens="ketchup mustard" value="1">Accept</option>
                                                 <option data-tokens="ketchup mustard" value="2">Deny</option>';

                                        }
                                        if($workrequest->status_id == 1){
                                            echo  '<option data-tokens="ketchup mustard">Select Status</option>
                                                 <option data-tokens="ketchup mustard" value="1" selected>Accept</option>
                                                 <option data-tokens="ketchup mustard" value="2">Deny</option>';
                                        }

                                        if($workrequest->status_id == 2){

                                            echo  '<option data-tokens="ketchup mustard">Select Status</option>
                                                 <option data-tokens="ketchup mustard" value="1" >Accept</option>
                                                 <option data-tokens="ketchup mustard" value="2" selected>Deny</option>';
                                        }

                                        ?>






                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Assign Employee</label>

                                    <select class="form-control selectpicker"  name="assigned_emp">
                                        <option data-tokens="ketchup mustard">Select Employee</option>
                                        @foreach($users as $allusers )
                                            <?php
                                            if($allusers->id ==$workrequest->assigned_emp ){
?>
                                            <option value='{{$allusers->id}}' selected>{{$allusers->name}}</option>
                                            <?php
                                            }
                                            ?>

                                            <option value="{{$allusers->id}}">{{$allusers->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>

                        </div>
                        <div class="panel-heading"> Asset</div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Select Asset</label>

                                    <select class="form-control selectpicker"  name="asset_id">
                                        <option data-tokens="ketchup mustard">Select Asset</option>
                                        @foreach($asset as $assets )

                                            <?php
                                            if($assets->id ==$workrequest->asset_id ){

                                                ?>
                                                <option value='{{$assets->id}}' selected>{{$assets->name}}</option>
                                                <?php
                                                }
                                                ?>
                                            <option  value="{{$assets->id}}">{{$assets->name}}</option>



                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName"> Select Sub Asset</label>

                                    <select class="form-control selectpicker"  name="subasset_id">
                                        <option data-tokens="ketchup mustard">Select Sub Asset</option>
                                        @foreach($subasset as $subassets )
                                                <?php
                                                  if($subassets->id ==$workrequest->subasset_id ){
                                                ?>
                                                      <option value='{{$subassets->id}}' selected>{{$subassets->name}}</option>

                                             <?php  }

                                                ?>
                                            <option value="{{$subassets->id}}">{{$subassets->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="panel-heading"> Recurring</div>
                        <div class="row ">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputName"> Recurring Option </label>

                                    <select class="form-control selectpicker recurrung_status"  name="recurring_status" onchange="recurring_set(this.value)">
                                        <option data-tokens="ketchup mustard">Select Status</option>
                                        <?php
                                        if($workrequest->recurring_status == 0){
                                            echo '  <option data-tokens="ketchup mustard" value="0" selected>No</option>';
                                            echo '  <option data-tokens="ketchup mustard" value="1" >Yes</option>';

                                        }
                                        if($workrequest->recurring_status == 1){
                                            echo '  <option data-tokens="ketchup mustard" value="0" >No</option>';
                                            echo '  <option data-tokens="ketchup mustard" value="1" selected >Yes</option>';

                                        }
                                        ?>




                                    </select>
                                </div>
                            </div>



                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputName"> Recurring Mode </label>
                                    <select class="form-control selectpicker  recurring_mode" id="recurring_mode" onchange="recurring_cutom(this.value);"  name="recurring_mode" >



                                        <option data-tokens="ketchup mustard">Select Mode</option>

                                        <?php

                                            $mode = array('0'=>'Custom','1'=>"Every Day",'2'=>"Weekly",'3'=>"After Every 15 Day",'4'=>"Monthly",'5'=>"After 2 Month",
                                                '6'=>"After 3 Month",'7'=>"After 4 Month",'8'=>"After 5 Month",'9'=>"After 6 Month",'10'=>"After 7 Month",
                                                '11'=>"After 8 Month",'12'=>"After 9 Month",'13'=>"After 10 Month",'14'=>"After 11 Month",'15'=>"Every Year");

                                            $i=0;
                                            foreach($mode as $key => $value){

                                                if($workrequest->recurring_mode == $key){

                                                    echo " <option  value='".$key."'  selected>".$value."</option>";

                                                    }
                                                echo " <option  value='".$key."' >".$value."</option>";

                                            }


                                        ?>




                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4" id="recurring_datepicker">

                                <div class="form-group">
                                    <label for="inputName"> Select Date Time</label>
                                <?php

                              $g='';
                                     if($workrequest->recurring_mode == 0){


                                    }else{

                                            $g=$workrequest->recurring_time;

                                    } ?>
                                    <input type="text"  name="recurring_time" value="<?php echo $g; ?>" class="form-control date-picker recurring_time" placeholder="Select Date">
                                </div>
                            </div>
                            <?php

                                                       $custom_time  =   $workrequest->recurring_time;


                                                       if($custom_time !='' && $workrequest->recurring_mode == 0)
                                {

                                    $time_mode =    explode('/',$custom_time);
                                    $custom_count = count($time_mode);
                                    dd($custom_count);

                                }



                            ?>
                             <div id="recurring_custom">
                                <div class="col-md-2" >

                                <div class="form-group">
                                    <label for="inputName"> Number </label>
                                    <input type="text"  name="recurring_number" value="<?php if($custom_time !='' && $workrequest->recurring_mode == 0){echo $time_mode[0];} ?>" class="form-control  recurring_time" placeholder="">
                                </div>
                            </div>

                                 <div class="col-md-2" >

                                     <div class="form-group">
                                         <label for="inputName"> Option </label>

                                         <select class="form-control selectpicker " onchange="recurring_cutom(this.value);"  name="recurring_type" >




                                             <?php

                                             if($custom_time !='' && $workrequest->recurring_mode == 0){


                                             if($time_mode[1] == 'day'){
                                                 echo '<option value="day" selected>Days </option>';

                                                 echo '<option value="week" >Week </option>';
                                                 echo '<option value="month" >Month </option>';
                                                 echo '<option value="year" >Years </option>';
                                             }

                                             if($time_mode[1] == 'week'){
                                                 echo '<option value="week" selected>Days </option>';

                                                 echo '<option value="day" >Days </option>';
                                                 echo '<option value="month" >Month </option>';
                                                 echo '<option value="year" >Years </option>';
                                             }

                                             if($time_mode[1] == 'month'){
                                                 echo '<option value="month" selected>Month </option>';

                                                 echo '<option value="day" >Days </option>';
                                                 echo '<option value="week" >Week </option>';
                                                 echo '<option value="year" >Years </option>';
                                             }

                                             if($time_mode[1] == 'year'){
                                                 echo '<option value="year" selected>Year </option>';

                                                 echo '<option value="day" >Days </option>';
                                                 echo '<option value="week" >Week </option>';
                                                 echo '<option value="month" >Month </option>';
                                             }

                                                 if($time_mode[1] == ''){
                                                     echo '<option value="day">Days </option>
                                             <option value="week">Week </option>
                                             <option value="month">Month </option>
                                             <option value="year">Years </option>';

                                                 }
                                             }

                                            ?>


                                         </select>

                                     </div>
                                 </div>
                        </div>

                        </div>

                        <div class="panel-heading"> Form Field  <button type="button" class="form-control btn btn-primary" id="add_form_field" style="width: auto;float: right;">Add form Field</button></div>


                        <div class="row">
                            <div class="col-md-12" id="form_field">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="panel panel-inverse">
                                    <div class="panel-heading"> @lang('modules.indent.createTitle')</div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">


                                            <div class="form-body">
                                                <h3 class="box-title">@lang('modules.indent.indentDetails')</h3>
                                                <hr>
                                                <div class="row ">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">@lang('modules.indent.store') (*)</label>
                                                            <select class="form-control select2" name="store_id" id="store" data-style="form-control" required>
                                                                <option value="">Select Store</option>
                                                                @forelse($stores as $supplier)
                                                                    <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                                                                @empty
                                                                @endforelse
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">@lang('modules.indent.remark')</label>
                                                            <input type="text" id="remark" name="remark" value="" class="form-control" >
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                                                <hr>
                                                <div class="row " style="background-color: #efefef; padding-top: 5px;">
                                                    <div class="proentry">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.category')</label>
                                                                <select class="form-control select2" name="product" data-style="form-control product">
                                                                    <option value="">Select Category</option>
                                                                    @forelse($products as $product)
                                                                        <option value="{{$product->id}}">{{ $product->name }}</option>
                                                                    @empty
                                                                    @endforelse
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.brand')</label>
                                                                <select class="form-control select2" name="brand" data-style="form-control brand">
                                                                    <option value="">Select Brand</option>
                                                                    @forelse($brands as $brand)
                                                                        <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                                    @empty
                                                                    @endforelse
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.quantity')</label>
                                                                <input type="text" name="quantity" value="" class="form-control quantity" placeholder="Enter Quantity">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.unit')</label>
                                                                <select class="form-control select2" name="unit" data-style="form-control unit">
                                                                    <option value="">Select Unit</option>
                                                                    @forelse($units as $unit)
                                                                        <option value="{{$unit->id}}">{{ $unit->symbol }}</option>
                                                                    @empty
                                                                    @endforelse
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.dateRequired')</label>
                                                                <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">@lang('modules.indent.remark')</label>
                                                                <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                                <label class="control-label" style="width: 100%;">@lang('modules.indent.action')</label>
                                                                <a href="javascript:void(0)" class="add-button btn btn-primary">Add</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="table-responsive" id="pdata">

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    <!-- .row -->

                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                            </div>

                        </div>


                    </div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary submitBtn" id="submit_wo" >Save</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>



  $( document ).ready(function() {
      var valfd =$('#recurring_mode :selected').val();

      if(valfd==0){

          $('#recurring_datepicker').css("display", "none");
          $('#recurring_custom').css("display", "block");
      }else {
          $('#recurring_datepicker').css("display", "block");
          $('#recurring_custom').css("display", "none");

      }
        });

        function recurring_set(id) {
            if (id == 0) {
                $(".recurring_mode").prop('disabled', true);
                $(".recurring_time").prop('disabled', true);

            } else {

                $(".recurring_mode").prop('disabled', false);
                $(".recurring_time").prop('disabled', false);
            }
        }
            function recurring_cutom(id) {

                if ( id == 0||id == 'day' || id == 'week' || id == 'month' || id == 'year' ) {
              //  if ( ) {

                    $('#recurring_datepicker').css("display", "none");
                    $('#recurring_custom').css("display", "block");
                } else {
                    $('#recurring_datepicker').css("display", "block");
                    $('#recurring_custom').css("display", "none");
                }
            }

            var ff_id = 1;
            $("#add_form_field").click(function () {

                // Finding total number of elements added
                var total_element = $(".add_form_field").length;
                //alert(total_element);
                if (total_element == 0) {
                    var nextindex = ff_id;
                } else {
                    var lastid = $(".add_form_field:last").attr("id");

                    var split_id = lastid.split("_");
                    var nextindex = parseInt(split_id[1]) + 1;

                    ff_id = nextindex;
                }

                var htmlf = '    <div class="row add_form_field" id="addformfield_' + ff_id + '">\n' +
                    '                                <div class="col-md-4" >\n' +
                    '\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <label for="inputName"> Field name</label>\n' +
                    '                                    <input type="text"  name="fieldname[]" value="" class="form-control  fn_' + ff_id + '" id="fn_' + ff_id + '" placeholder="">\n' +
                    '                                    <input type="hidden"  name="wr_id[]" value="' + ff_id + '" >\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '\n' +
                    '                                 <div class="col-md-4" >\n' +
                    '\n' +
                    '                                     <div class="form-group">\n' +
                    '                                         <label for="inputName"> Field Type </label>\n' +
                    '                                         <select class="form-control selectpicker    ft_' + ff_id + ' " id="ft_' + ff_id + '" name="fieldtype[]"  onchange="multiplecheck(this.value,this.id)">\n' +
                    '\n' +
                    '\n' +
                    '\n' +
                    '                                             <option >Select </option>\n' +
                    '                                             <option value="text">Text Field </option>\n' +
                    '                                             <option value="status">Task Status </option>\n' +
                    '                                             <option value="number">Number Field </option>\n' +
                    '                                             <option value="checkbox">Inspection Check </option>\n' +
                    '                                             <option value="multiple">Multiple Choice </option>\n' +
                    '\n' +
                    '                                         </select>\n' +
                    '\n' +
                    '                                     </div>\n' +
                    '\n <div id="multipleoption_' + ff_id + '"></div>' +
                    '                                 </div>\n' +
                    '                                <div class="col-md-4" >\n' +
                    '\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <label for="inputName"> Action</label>\n' +
                    '                                    <button type="button"  class="form-control  btn btn-primary remove_btnw" id="remove_row_' + ff_id + '" >Remove</button>\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '                        </div>';

                $("#form_field").append(htmlf);
                //  $("#form_field").append(htmlf);
                $('.selectpicker').selectpicker('refresh')

            });


            function multiplecheck(id, ff_id) {

                var split_id = ff_id.split("_");

                var fdssgg = split_id[1];
                //  alert(fdssgg);
                if (id == 'multiple') {

                    var html_mlaa = '                        <div class="row multiplerow_' + fdssgg + '">\n' +

                        '                            <div class="col-md-6" >\n' +
                        '                                <div class="form-group">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                            </div>\n' +
                        '                            <div class="col-md-6" >\n' +
                        '                                <div class="form-group">\n' +
                        '\n' +
                        '                                    <button type="button" class="form-control btn btn-primary addbtnmultiple_' + fdssgg + '" id="addformfield_' + fdssgg + '"  onclick="muti_choice_add(this.id);">Add</button>\n' +
                        '                                    <input type="hidden" name="multi_id[]" value="' + fdssgg + '">\n' +
                        '\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '\n' +
                        '                                <div id="multirow_' + split_id[1] + '" class="multpleelement"></div>\n' +
                        '                        </div>';


                    $("#multipleoption_" + fdssgg).append(html_mlaa);
                } else {

                    $("#multipleoption_" + fdssgg).empty();
                }

            }

            var inbtn_no_id = 1;

            function muti_choice_add(hdjs) {

                // alert(hdjs);
                var split_id = hdjs.split("_");


                var html_ml = '                        <div class="row " id ="inbtmulti_' + split_id[1] + '_' + inbtn_no_id + '">\n' +

                    '                            <div class="col-md-6" >\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <label for="inputName"> Title </label>\n' +
                    '                                    <input type="text"  name="multi_row_title[]"  class="form-control  recurring_time" placeholder="">\n' +
                    '                                    <input type="hidden"  name="multi_row_id[]" value="' + split_id[1] + '_' + inbtn_no_id + '" >\n' +
                    '                                </div>\n' +
                    '\n' +
                    '                            </div>\n' +
                    '                            <div class="col-md-6" >\n' +
                    '                                <div class="form-group">\n' +
                    '                                    <label for="inputName"> Action  </label>\n' +
                    '                                    <button type="button" class="form-control btn btn-primary remove_btnw_multioption" onclick="remove_multiplebox(this.id)" id="removemultiplebox_' + split_id[1] + '_' + inbtn_no_id + '"  >Remove</button>\n' +
                    '\n' +
                    '\n' +
                    '                                </div>\n' +
                    '                            </div>\n' +
                    '\n' +
                    '                        </div>';
                //multiple_option

                $("#multirow_" + split_id[1]).append(html_ml);
                inbtn_no_id = inbtn_no_id + 1;
            }

            $('#form_field').on('click', '.remove_btnw', function () {
                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[2];

                $("#addformfield_" + deleteindex).remove();
            });


            function remove_multiplebox(id) {

                var split_id = id.split("_");
                var deleteindex = split_id[1] + '_' + split_id[2];
                $("#inbtmulti_" + deleteindex).remove();

            }

            $('.multpleelement').on('click', '.remove_btnw_multioption', function () {


                var id = this.id;

                var split_id = id.split("_");
                var deleteindex = split_id[2];
                alert(deleteindex);
                $("#inbtmulti_" + deleteindex).remove();
            });


            $('#createWorkRequest').submit(function () {
                $.easyAjax({
                    url: '{{route('admin.work-request.add')}}',
                    container: '#createWorkRequestf',
                    type: "POST",
                    data: $('#createWorkRequestf').serialize(),
                    success: function (response) {
                        if (response.status == 'success') {
                            window.location.reload();
                        }
                    }
                });
                return false;
            });

            $('#submit_wo').click(function () {
                $.easyAjax({
                    url: '{{route('admin.work-order.update', $workrequest->id)}}',
                    container: '#updateWorkRequestf',
                    type: "POST",
                    data: $('#updateWorkRequestf').serialize(),
                    success: function (response) {
                        {{--if(myDropzone.getQueuedFiles().length > 0){--}}
                        {{--projectID = response.projectID;--}}
                        {{--$('#projectID').val(response.projectID);--}}
                        {{--myDropzone.processQueue();--}}
                        {{--}--}}
                        {{--else{--}}
                        {{--var msgs = "@lang('modules.projects.projectUpdated')";--}}
                        {{--$.showToastr(msgs, 'success');--}}
                        window.location.href = '{{ route('admin.work-order') }}'
                        //}
                    }
                });
                return false;
            });


            Dropzone.autoDiscover = false;
            //Dropzone class
            myDropzone = new Dropzone("div#file-upload-dropzone", {
                url: "{{ route('admin.work-order.storeImage') }}",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                paramName: {"imageFile": "request_image", "pdfFile": "request_pdf"},
                maxFilesize: 10,
                maxFiles: 10,
                acceptedFiles: "image/*,application/pdf",
                autoProcessQueue: false,
                uploadMultiple: true,
                addRemoveLinks: true,
                parallelUploads: 10,
                init: function () {
                    myDropzone = this;
                }
            });

            myDropzone.on('sending', function (file, xhr, formData) {
                console.log(myDropzone.getAddedFiles().length, 'sending');
                var ids = $('#taskID').val();
                formData.append('task_id', ids);
            });

            myDropzone.on('completemultiple', function () {
                var msgs = "Work Order Created";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('admin.work-order') }}'

            });
            $('.summernote').summernote({
                height: 200,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ["view", ["fullscreen"]]
                ]
            });

            $(".select2").select2({
                formatNoMatches: function () {
                    return "{{ __('messages.noRecordFound') }}";
                }
            });
            $(".date-picker").datepicker({
                todayHighlight: true,
                autoclose: true,
                weekStart: '{{ $global->week_start }}',
                format: '{{ $global->date_picker_format }}',
            });

            $('#save-form').click(function () {
                $.easyAjax({
                    url: '{{route('admin.indent.store')}}',
                    container: '#createIndent',
                    type: "POST",
                    redirect: true,
                    data: $('#createIndent').serialize()
                })
            });
            $(document).on('change', 'select[name=product]', function () {
                var pid = $(this).val();
                var token = '{{ csrf_token() }}';
                $.ajax({
                    url: '{{route('admin.indent.getBrands')}}',
                    type: 'POST',
                    data: {_token: token, pid: pid},
                    success: function (data) {
                        $('select[name=brand]').html(data);
                        $("select[name=brand]").select2("destroy");
                        $("select[name=brand]").select2();
                    }
                });

            });
            $(document).on('click', '.add-button', function () {
                var btn = $(this);
                var cid = $('select[name=product]').val();
                var bid = $('select[name=brand]').val();
                var qty = $('input[name=quantity]').val();
                var unit = $('select[name=unit]').val();
                var dated = $('input[name=date]').val();
                var remark = $('input[name=remarkx]').val();
                if (cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == '') {
                    alert('Invalid Data. All fields are mandatory.');
                } else {
                    $.ajax({
                        url: '{{route('admin.indent.storeTmp')}}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token()  }} ',
                            cid: cid,
                            bid: bid,
                            qty: qty,
                            unit: unit,
                            dated: dated,
                            remark: remark
                        },
                        redirect: false,
                        beforeSend: function () {
                            btn.html('Adding...');
                        },
                        success: function (data) {
                            $('#pdata').html(data);
                        },
                        complete: function () {
                            btn.html('Add');
                        }

                    });
                }
            });
            $(document).on('click', '.deleteRecord', function () {
                var btn = $(this);
                var did = btn.data('key');


                $.ajax({
                    url: '{{route('admin.indent.deleteTmp')}}',
                    type: 'POST',
                    data: {_token: '{{ csrf_token()  }} ', did: did},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Deleting...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Delete');
                    }

                });
            });


    </script>
@endpush

