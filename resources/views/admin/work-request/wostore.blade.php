@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.work-order') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Products</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateWorkRequestf','class'=>'ajax-form','method'=>'POST']) !!}
                        @csrf
                        <div class="form-body">
                            <input type="hidden" id="id" name="id" value="<?php echo $work_order_id;?>" class="form-control" >
                            <h3 class="box-title">Manage Required Products</h3>
                            <hr>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.store') (*)</label>
                                        <select class="form-control select2" name="store_id" id="store" data-style="form-control" required>
                                            <option value="">Select Store</option>
                                            @forelse($stores as $supplier)
                                                <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.remark')</label>
                                        <input type="text" id="remark" name="remark" value="" class="form-control" >
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                            <hr>
                            <div class="row " style="background-color: #efefef; padding-top: 5px;">
                                <div class="proentry">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.category')</label>
                                            <select class="form-control select2" name="product" data-style="form-control product">
                                                <option value="">Select Category</option>
                                                @forelse($products as $product)
                                                    <option value="{{$product->id}}">{{ $product->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.brand')</label>
                                            <select class="form-control select2" name="brand" data-style="form-control brand">
                                                <option value="">Select Brand</option>
                                                @forelse($brands as $brand)
                                                    <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.quantity')</label>
                                            <input type="text" name="quantity" value="" class="form-control quantity" placeholder="Enter Quantity">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.unit')</label>
                                            <select class="form-control select2" name="unit" data-style="form-control unit">
                                                <option value="">Select Unit</option>
                                                @forelse($units as $unit)
                                                    <option value="{{$unit->id}}">{{ $unit->symbol }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.dateRequired')</label>
                                            <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.remark')</label>
                                            <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label" style="width: 100%;">@lang('modules.indent.action')</label>
                                            <a href="javascript:void(0)" class="add-button btn btn-primary">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">

                                </div>
                            </div>
                        </div>

                        <!-- Modal Footer -->
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-primary submitBtn" id="save-form" style="float: right;" >Save</button>
                        </div>

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>    <!-- .row -->


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>

            $(".select2").select2({
                formatNoMatches: function () {
                    return "{{ __('messages.noRecordFound') }}";
                }
            });
            $(".date-picker").datepicker({
                todayHighlight: true,
                autoclose: true,
                weekStart: '{{ $global->week_start }}',
                format: '{{ $global->date_picker_format }}',
            });

            $('#save-form').click(function () {
                $.easyAjax({
                    url: '{{route('admin.work-order.wostore.update',[':id',$work_order_id])}}',
                    container: '#updateWorkRequestf',
                    type: "POST",
                    redirect: true,
                    data: $('#updateWorkRequestf').serialize()
                })
            });
            $(document).on('change', 'select[name=product]', function () {
                var pid = $(this).val();
                var token = '{{ csrf_token() }}';
                $.ajax({
                    url: '{{route('admin.indent.getBrands')}}',
                    type: 'POST',
                    data: {_token: token, pid: pid},
                    success: function (data) {
                        $('select[name=brand]').html(data.brands);
                        $("select[name=brand]").select2("destroy");
                        $("select[name=brand]").select2();
                    }
                });

            });
            $(document).on('click', '.add-button', function () {
                var btn = $(this);
                var cid = $('select[name=product]').val();
                var bid = $('select[name=brand]').val();
                var qty = $('input[name=quantity]').val();
                var unit = $('select[name=unit]').val();
                var dated = $('input[name=date]').val();
                var remark = $('input[name=remarkx]').val();
                if (cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == '') {
                    alert('Invalid Data. All fields are mandatory.');
                } else {
                    $.ajax({
                        url: '{{route('admin.indent.storeTmp')}}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token()  }} ',
                            cid: cid,
                            bid: bid,
                            qty: qty,
                            unit: unit,
                            dated: dated,
                            remark: remark
                        },
                        redirect: false,
                        beforeSend: function () {
                            btn.html('Adding...');
                        },
                        success: function (data) {
                            $('#pdata').html(data);
                        },
                        complete: function () {
                            btn.html('Add');
                        }

                    });
                }
            });
            $(document).on('click', '.deleteRecord', function () {
                var btn = $(this);
                var did = btn.data('key');


                $.ajax({
                    url: '{{route('admin.indent.deleteTmp')}}',
                    type: 'POST',
                    data: {_token: '{{ csrf_token()  }} ', did: did},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Deleting...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Delete');
                    }

                });
            });


    </script>
@endpush

