<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Category</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'statusWorkRequestf','class'=>'ajax-form','method'=>'POST']) !!}
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label for="inputName">Status</label>

                        <select class="form-control "  name="status">
                            <option data-tokens="ketchup mustard">Select Status</option>
                            <option data-tokens="ketchup mustard" value="0">Disable</option>
                            <option data-tokens="ketchup mustard" value="1">Active</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.work-request.status-update', [$workrequest->id])}}',
            container: '#statusWorkRequestf',
            type: "POST",
            data: $('#statusWorkRequestf').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

</script>