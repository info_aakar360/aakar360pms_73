@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>

        .red-box{  background-color: #073984;
            width: 50px;
            height: 50px;
            border-radius: 33px 33px 33px 33px;
            margin-left: 82px;
        }

        .gray-box{  background-color: #c2c8d0;
            width: 50px;
            height: 50px;
            border-radius: 33px 33px 33px 33px;
            margin-left: 82px;
        }
        .fa1{
            margin: 10px 0px 8px 5px;
            padding: 2px 1px 1px 10px;
            color: white;
        }
        .newtext{
            font-family: 'Poppins';
            font-size: 1em;
            font-weight: 700;
            color: #073984;
            margin-left: 3px;
        }

        .graytext{
            font-family: 'Poppins';
            font-size: 1em;
            font-weight: 700;
            color: #c2c8d0;
            margin-left: 7px;
        }
        .hl {
            border-bottom: 1.5px solid #eeeeee;
            width: 100%;
        }

    </style>
@endpush

@section('content')




    <div id="exTab2" class="container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a  href="#1" data-toggle="tab">Details</a>
            </li>
            <li><a href="#2" data-toggle="tab">Updates</a>
            </li>

            <li><a href="#3" data-toggle="tab">Files</a>
            </li>

        </ul>

        <div class="tab-content ">
            <div class="tab-pane active" id="1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">

                                <div class="col-md-3">
                                    @if($workrequest->request_status == 1)
                                        <div class="red-box">
                                         <i class="fa1 fa fa-unlock-alt fa-2x" aria-hidden="true" style="margin-left: 5px !important;">
                                            </i>
                                            <span class="newtext">Open</span>

                                        </div>
                                    @endif
                                    @if($workrequest->request_status != 1)

                                        <div class="gray-box">
                                            <?php $s = 1; ?>
                                            <a href="{{ route('admin.work-order.changestatus',[$workrequest->id, $s])}}">     <i class="fa1 fa fa-unlock-alt fa-2x" aria-hidden="true" style="margin-left: 5px !important;">
                                            </i>
                                            </a>
                                            <span class="graytext">Open</span>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    @if($workrequest->request_status == 2)
                                    <div class="red-box">
                                        <i class="fa1 fa  fa-stop-circle fa-2x" aria-hidden="true" style="margin-left: 3px;"></i>
                                        <span class="newtext">Hold</span>
                                    </div>

                                    @endif
                                    @if($workrequest->request_status != 2)
                                            <div class="gray-box">
                                                <?php $s = 2; ?>
                                                <a href="{{ route('admin.work-order.changestatus',[$workrequest->id, $s])}}">          <i class="fa1 fa  fa-stop-circle fa-2x" aria-hidden="true" style="margin-left: 3px;"></i>
                                                <span class="graytext">Hold</span>
                                                </a>
                                            </div>

                                        @endif
                                </div>
                                <div class="col-md-3">
                                    @if($workrequest->request_status == 3)
                                    <div class="red-box">
                                        <i class="fa1 fa fa-clock-o fa-2x" aria-hidden="true" style="margin-left: 3px;"></i>
                                        <span class="newtext"  style="margin-left: 2px !important;"> Progress</span>
                                    </div>
                                    @endif
                                    @if($workrequest->request_status != 3)
                                            <div class="gray-box">
                                                <?php $s = 3; ?>
                                                <a href="{{ route('admin.work-order.changestatus',[$workrequest->id, $s])}}">     <i class="fa1 fa fa-clock-o fa-2x" aria-hidden="true" style="margin-left: 3px;"></i>
                                                <span class="graytext" style="margin-left: 2px !important;" > Progress</span>
                                                </a>
                                            </div>
                                    @endif

                                </div>
                                <div class="col-md-3">

                                    @if($workrequest->request_status == 4)

                                    <div class="red-box">
                                        <i class="fa1 fa fa-lock fa-2x" aria-hidden="true" style="margin-left:6px !important;"></i>
                                        <span class="newtext">Closed</span>
                                    </div>
                                    @endif
                                    @if($workrequest->request_status != 4)
                                            <div class="gray-box">
                                                <?php $s = 4; ?>
                                                <a href="{{ route('admin.work-order.changestatus',[$workrequest->id, $s])}}">       <i class="fa1 fa fa-lock fa-2x" aria-hidden="true" style="margin-left:6px !important;"></i>

                                                <span class="graytext">Closed</span>
                                                </a>
                                            </div>

                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                <div class="white-box">
                    <div class="row hl" style="margin-bottom: 10px;">
                        <a  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" >
                        <div class="col-md-8">
                            <h3>Consumable Product Details</h3>
                        </div>
                        <div class="col-md-2">

                                <a  class="btn btn-outline btn-success btn-sm"  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Details <i class="fa fa-warning" aria-hidden="true"></i></a>


                        </div>
                            <div class="col-md-2">

                                    <a href="{{ route('admin.work-order.wostore', $workrequest->id )}}" class="btn btn-outline btn-success btn-sm">Add Products <i class="fa fa-plus" aria-hidden="true"></i></a>


                            </div>
                        </a>
                        <div class="clear"></div>
                        <div class="collapse" id="collapseExample" style="
    margin-top: 2%;
">
                            <div class="card card-body">
                                <?php
                                $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Req. Date</th><th>Remark</th></thead><tbody>';
                                //dd($indent);
                                if(count($tmpData)){
                                    $i = 1;

                                    foreach($tmpData as $data){
                                        $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td></tr>';
                                        $i++;
                                    }
                                }else{
                                    $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                }
                                $html .= '</tbody></table>';
                                echo $html;
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="white-box">
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Asset Name</h4>
                        </div>
                        <div class="col-md-3">
                            {{get_asset_name($workrequest->asset_id)}}
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Asset Building Location</h4>
                        </div>
                        <div class="col-md-3">
                            {{get_location_name($workrequest->location_id)}}-
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Ticket</h4>
                        </div>
                        <div class="col-md-3">
                            #{{$workrequest->id}}
                        </div>

                    </div>
                </div>
                <div class="white-box">
                    <div class="row hl">
                        <div class="col-md-4">
                            <h4>Task / Form items</h4>
                        </div>
                        <div class="col-md-8">


                            @foreach($formfield as $ff)

                                @if($ff->field_type != "multiple")
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="inputName">{{$ff->field_title}}</label>
                                                <input type="text" class="form-control" id="inputName" name="{{$ff->field_title}}" placeholder="Enter  {{$ff->field_title}}"/>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                    @if($ff->field_type == "multiple")
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    @if($ff->parent_id != "0" && $ff->child_id == "0")
                                                    <label for="inputName">{{$ff->field_title}}</label>
                                                        @foreach($formfield as $dd)

                                                            @if($dd->parent_id != "0" && $dd->child_id != "0" && $dd->multiple == "YES" )
                                                                <div class="checkbox">
                                                                    <label><input type="checkbox" value="">{{$dd->field_title}}</label>
                                                                </div>
                                                            @endif

                                                            @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif


                                @endforeach

                        </div>

                    </div>
                </div>

                <div class="white-box">

                    <div class="row hl">
                        <div class="col-md-4">
                            <h4>Task Timer</h4>
                            <div id="countdowntimer"><span id="hms_timer"></span></div>
                        </div>


                        <div class="col-md-3">

                        </div>




                            <div class="col-md-4">
                            <div class="form-group">

                                    <span id="time1"> </span>



                            </div>
                        </div>
                            <div class="col-md-">
                                <div class="form-group">
                                    <button id="start1">start/stop</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="white-box">

                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Created On</h4>
                        </div>
                        <div class="col-md-3">
                            <h4>{{$workrequest->created_at}}</h4>
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Assigned To</h4>
                        </div>
                        <div class="col-md-3">
                            <h4>{{ get_user_name($workrequest->assigned_emp) }}</h4>
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Repeating Schedule </h4>
                        </div>
                        <div class="col-md-3">
                            <h4>{{$workrequest->recurring_time}}</h4>
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Due Date</h4>
                        </div>
                        <div class="col-md-3">
                            <h4>{{$workrequest->recurring_time}}</h4>
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Last Updated </h4>
                        </div>
                        <div class="col-md-3">
                            <h4>{{$workrequest->updated_at}}   </h4>
                        </div>

                    </div>
                    <div class="row hl">
                        <div class="col-md-9">
                            <h4>Created By </h4>
                        </div>
                        <div class="col-md-3">
                            <h4>Admin</h4>
                        </div>

                    </div>
                </div>

            </div>
            <div class="tab-pane" id="2">

                <div class="row ">
                    <div class="col-md-12">
                        <label for="inputName hl" style="font-weight: 600;">
                            @foreach($woupdates as $woup)

                            {{$woup->wo_update}} - created on {{$woup->created_at}}
                                <br>
                        @endforeach


                        </label>
                    </div>
                </div>

                <div class="row hl">

                    <div class="col-md-9">
                        <div class="form-group">

                            <input type="text" class="form-control" id="woupdatess" name="woupdatess" placeholder="Enter  Updates"/>
                            <input type="hidden" class="form-control" id="wo_id" name="wo_id" value="{{$workrequest->id}}"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="button" id="save-category" class="btn btn-success woupdate" cat-id ="{{$workrequest->id}}"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="tab-pane" id="3">

                <div class="row hl">
                    <div class="col-md-9">
                        <div class="form-group">


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                        </div>
                    </div>

                </div>
            </div>

            </div>

        </div>
    </div>








    <!-- Start Modal add sub location location -->
    <div class="modal" id="createWorkRequest">

        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                </div>
            </div>

        </div>
        <!-- End Modal sub add location -->

        <!-- Start Modal edit sub location location -->
        <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        Loading...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn blue">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->.
        </div>
        <!-- End Modal sub edit location -->





        @endsection

        @push('footer-script')


            <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

            <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


            <style>
                .select2-container-multi .select2-choices .select2-search-choice {
                    background: #ffffff !important;
                }
            </style>
            <script>
                $(document).ready(function() {
                    $('#example').DataTable();
                });



                $('.editWorkOrder').click(function(){
                    var id = $(this).data('cat-id');
                    var url = '{{ route('admin.work-order.edit',':id')}}';
                    url = url.replace(':id', id);
                    $('#modelHeading').html("Edit Work Request");
                    $.ajaxModal('#taskCategoryModal', url);
                    $('#category_id').selectpicker('refresh')
                });



                $('.assignWorkRequest').click(function(){
                    var id = $(this).data('cat-id');
                    var url = '{{ route('admin.work-order.assign',':id')}}';
                    url = url.replace(':id', id);
                    $('#modelHeading').html("Assign Employee");
                    $.ajaxModal('#taskCategoryModal', url);
                    $('#category_id').selectpicker('refresh')
                });

                $('.statusWorkRequest').click(function(){
                    var id = $(this).data('cat-id');
                    var url = '{{ route('admin.work-order.status',':id')}}';
                    url = url.replace(':id', id);
                    $('#modelHeading').html("Edit Status");
                    $.ajaxModal('#taskCategoryModal', url);
                    $('#category_id').selectpicker('refresh')
                });



                $('.deleteWorkRequest').click(function(){

                    var id = $(this).data('cat-id');
                    var url = "{{ route('admin.work-order.delete',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'POST'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                window.location.reload();
                            }
                        }
                    });
                });

                $('.woupdate').click(function(){

                    //var id = $(this).data('cat-id');
                    var woupdatess = $('#woupdatess').val();
                    var wo_id = $('#wo_id').val();
                    var url = "{{ route('admin.work-order.woupdate',':id') }}";
                    url = url.replace(':id', wo_id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'POST','woupdatess':woupdatess,'wo_id':wo_id },
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
                                window.location.reload();
                            }
                        }
                    });
                });
                {{--// image file--}}
                {{--Dropzone.autoDiscover = false;--}}
                {{--//Dropzone class--}}
                {{--myDropzone = new Dropzone("div#file-upload-dropzone", {--}}
                    {{--url: "{{ route('admin.rfi.storeImage') }}",--}}
                    {{--headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },--}}
                    {{--paramName: "file",--}}
                    {{--maxFilesize: 10,--}}
                    {{--maxFiles: 10,--}}
                    {{--acceptedFiles: "image/*,application/pdf",--}}
                    {{--autoProcessQueue: false,--}}
                    {{--uploadMultiple: true,--}}
                    {{--addRemoveLinks:true,--}}
                    {{--parallelUploads:10,--}}
                    {{--init: function () {--}}
                        {{--myDropzone = this;--}}
                    {{--}--}}
                {{--});--}}

                {{--myDropzone.on('sending', function(file, xhr, formData) {--}}
                    {{--console.log(myDropzone.getAddedFiles().length,'sending');--}}
                    {{--var ids = $('#taskID').val();--}}
                    {{--formData.append('task_id', ids);--}}
                {{--});--}}

                {{--myDropzone.on('completemultiple', function () {--}}
                    {{--var msgs = "RFI Created";--}}
                    {{--$.showToastr(msgs, 'success');--}}
                    {{--window.location.href = '{{ route('admin.rfi.index') }}'--}}

                {{--});--}}

                {{--// pdf file--}}
                {{--Dropzone.autoDiscover = false;--}}
                {{--//Dropzone class--}}
                {{--myDropzone = new Dropzone("div#file-upload-dropzone1", {--}}
                    {{--url: "{{ route('admin.rfi.storeImage') }}",--}}
                    {{--headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },--}}
                    {{--paramName: "file",--}}
                    {{--maxFilesize: 10,--}}
                    {{--maxFiles: 10,--}}
                    {{--acceptedFiles: "image/*,application/pdf",--}}
                    {{--autoProcessQueue: false,--}}
                    {{--uploadMultiple: true,--}}
                    {{--addRemoveLinks:true,--}}
                    {{--parallelUploads:10,--}}
                    {{--init: function () {--}}
                        {{--myDropzone = this;--}}
                    {{--}--}}
                {{--});--}}

                {{--myDropzone.on('sending', function(file, xhr, formData) {--}}
                    {{--console.log(myDropzone.getAddedFiles().length,'sending');--}}
                    {{--var ids = $('#taskID').val();--}}
                    {{--formData.append('task_id', ids);--}}
                {{--});--}}

                {{--myDropzone.on('completemultiple', function () {--}}
                    {{--var msgs = "RFI Created";--}}
                    {{--$.showToastr(msgs, 'success');--}}
                    {{--window.location.href = '{{ route('admin.rfi.index') }}'--}}

                {{--});--}}


                var Stopwatch;
                if (!Stopwatch)
                    Stopwatch = {};


                function Stopwatch(displayTime){
                    this.runtime = 0; // milliseconds
                    this.timer = null; // nonnull iff runnig
                    this.displayTime = displayTime; // not showing runtime anywhere
                }

                /**
                 * The increment in milliseconds.
                 * (This is a class variable shared by all Stopwatch instances.)
                 */
                Stopwatch.INCREMENT = 200;

                /**
                 * Displays the time using the appropriate display strategy.
                 */
                Stopwatch.prototype.doDisplay = function(){
                    if (!this.laptime)
                        this.displayTime(this.runtime);
                    else
                        this.displayTime(this.laptime);
                };


                Stopwatch.prototype.startStop = function(){
                    if (!this.timer) {
                        var instance = this;
                        this.timer = window.setInterval(function(){
                            instance.runtime += Stopwatch.INCREMENT;
                            instance.doDisplay();
                        }, Stopwatch.INCREMENT);
                    }
                    else {
                        window.clearInterval(this.timer);
                        this.timer = null;
                        this.doDisplay();
                    }
                };


                Stopwatch.prototype.resetLap = function(){
                    if (!this.laptime) {
                        if (this.timer) {
                            this.laptime = this.runtime;
                        }
                        else {
                            this.runtime = 0;
                        }
                    }
                    else {
                        delete this.laptime;
                    }
                    this.doDisplay();
                };



                var s = new Stopwatch(function(runtime) {
                    var hours = Math.floor(runtime / 3600000);
                    var minutes = Math.floor(runtime / 60000);
                    var seconds = Math.floor(runtime % 60000 / 1000);
                    var decimals = Math.floor(runtime % 1000 / 100);
                    var displayText =  hours + ":" +  minutes + ":" + (seconds < 10 ? "0" : "") + seconds ;
                    $("#time1").text(displayText);
                });


                $(document).ready(function(){

                    $("#start1").bind("click", function(){ s.startStop();
                        $("#reset1").val("Stop");
                    });
                    $("#reset1").bind("click", function(){ s.resetLap();
                        $("#start1").val("Start");
                    });
                    s.doDisplay();
                });

            </script>
    @endpush