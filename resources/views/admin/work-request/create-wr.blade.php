@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfi.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> New Work Request</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createWorkRequestf','class'=>'ajax-form','method'=>'POST']) !!}
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="inputName">Select Project</label>
                                    <select class="form-control selectpicker"  name="project_id">
                                        <option data-tokens="ketchup mustard">Select Project</option>
                                        @foreach($projects as $project)

                                            <option value="{{$project->id}}">{{$project->project_name}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>



                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Title</label>
                            <input type="text" class="form-control" id="inputName" name="title" placeholder="Enter  Title"/>
                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Description</label>
                            <input type="text" class="form-control" id="inputName" name="description" placeholder="Enter  Description"/>
                        </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Complaint By Name</label>
                            <input type="text" class="form-control" id="inputName" name="user_name" placeholder="Enter  Name"/>
                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Phone Number</label>
                            <input type="text" class="form-control" id="inputName" name="user_phone" placeholder="Enter  Phone Number"/>
                        </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Email</label>
                            <input type="text" class="form-control" id="inputName" name="user_email" placeholder="Enter  Email"/>
                        </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Priority</label>

                                    <select class="form-control selectpicker"  name="request_priority">
                                        <option data-tokens="ketchup mustard">Select Priority</option>
                                        <option data-tokens="ketchup mustard" value="0">Low</option>
                                        <option data-tokens="ketchup mustard" value="1">Medium</option>
                                        <option data-tokens="ketchup mustard" value="2">High</option>

                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Location</label>

                                    <select class="form-control selectpicker"  name="location_id" onchange="getsublocation(this.value)">
                                        <option data-tokens="ketchup mustard">Select Location</option>
                                        @forelse($location as $supplier)
                                            <option value="{{$supplier->id}}">{{ $supplier->location_name }}</option>
                                        @empty
                                        @endforelse

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputName">Sub Location</label>

                                    <select  id="sub_location" class="form-control"  name="sub_location_id">
                                        <option >Select Sub Location</option>


                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="row">

                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Image</label>

                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="request_image" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="taskID" id="taskID">
                                </div>
                            </div>



                        </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">PDF</label>

                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="request_pdf" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="taskID" id="taskID">
                                </div>
                            </div>

                        </div>
                            </div>

                        </div>
                        </div>






                    </div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="submitBtn" class="btn btn-primary" >SUBMIT</button>
                    </div>

                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>

        function getsublocation(id) {
            var token = '{{ csrf_token() }}';
            $.easyAjax({
                url: '{{route('admin.work-order.get-sub-location',':id')}}',
                type: "POST",
                data: {_token: token,'id':id},
                success: function (response) {
                    if(response.status == 'success'){
                   var subloaction=response.option;
                    $("#sub_location").html(subloaction);



                        $('#sub_location').addClass('selectpicker');
                     //   $('#sub_location').attr('data-live-search', 'true');
                        $('#sub_location').selectpicker('refresh');
                       // $('.sub_location')
                      //  window.location.reload();
                    }
                }
            })


        }


        $('#submitBtn').click(function () {
            $.easyAjax({
                url: '{{route('admin.work-request.add')}}',
                container: '#createWorkRequestf',
                type: "POST",
                data: $('#createWorkRequestf').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        });


        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.rfi.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#taskID').val();
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "RFI Created";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.rfi.index') }}'

        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });


    </script>
@endpush

