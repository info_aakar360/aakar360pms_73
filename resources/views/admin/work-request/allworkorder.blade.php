@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createWorkRequest" class="btn btn-outline btn-success btn-sm">Add Work Request <i class="fa fa-plus" aria-hidden="true"></i></a>

        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table " id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Due Date</th>
                            <th>WO#</th>
                            <th>Status</th>
                            <th>Work Order Title</th>
                            <th>Priority</th>
                            <th>Assigned</th>
                            <th>Project</th>
                            <th>Location</th>
                            <th>Asset</th>
                            <th>Last Updated</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($categories as $key=>$category) {

                            if(count($categories)!=0) {
                            ?>

                               <tr id="cat-{{ $category->id }}">



                                   <td>
                                       <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                                       {{ $key+1 }}
                                       </a>
                                   </td>
                                       <td>
                                           <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                                           {{ $category->recurring_time }}
                                           </a>
                                       </td>
                                       <td>
                                           <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                                           #{{ $category->id }}
                                           </a>
                                       </td>

                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            <?php
                            if($category->request_status == 1){echo "Open";}
                            if($category->request_status == 2){echo "Hold";}
                            if($category->request_status == 3){echo "Progress";}
                            if($category->request_status == 4){echo "Closed";}

                            ?>

                            </a>

                        <td>
                                       <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            {{ $category->title }}
                                       </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            <?php
                            if($category->request_priority == 0){echo "Low";}
                            if($category->request_priority == 1){echo "Medium";}
                            if($category->request_priority == 2){echo "High";}

                            ?>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            @foreach($users as $user)

                                @if($user->id == $category->assigned_emp)
                              {{$user->name}}
                                @endif
                            @endforeach

                            </a>

                        </td>
                                   <td>
                                       <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                                           {{ get_project_name($category->project_id)  }}
                                       </a>
                                   </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            {{ get_location_name($category->location_id)  }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            {{ get_asset_name($category->asset_id ) }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            {{ $category->updated_at }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.work-order.editworkorder', $category->id )}}">
                            {{ $category->created_at }}
                            </a>
                        </td>

                        </tr>
                        <?php
                           }else{
                               ?>
                           <tr>
                               <td colspan="11">No Work Orders Found</td>
                           </tr>

                            <?php
                                } }
                               ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="createWorkRequest">

        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

            </div>
        </div>

    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );



    $('.editWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Work Request");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('.assignWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.assign',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Assign Employee");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('.statusWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.status',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Status");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });



    $('.deleteWorkRequest').click(function(){

        var id = $(this).data('cat-id');
        var url = "{{ route('admin.work-order.delete',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'POST'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        });
    });



</script>
@endpush