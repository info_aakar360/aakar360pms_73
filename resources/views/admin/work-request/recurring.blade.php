@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">



<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">


@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createWorkRequest" class="btn btn-outline btn-success btn-sm">Add Work Request <i class="fa fa-plus" aria-hidden="true"></i></a>

        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Pdf</th>
                            <th>Priority</th>
                            <th>Status</th>
                            <th>Recurring Mode</th>
                            <th>Recurring Start Date</th>

                            <th>Recurring Next Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($categories as $key=>$category) {

                            if(count($categories)!=0) {
                            ?>
                               <tr id="cat-{{ $category->id }}">
                                <td>{{ $key+1 }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->user_name }}</td>
                        <td>{{ $category->user_phone }}</td>
                        <td>{{ $category->user_email }}</td>
                        <td>{{ $category->request_image }}</td>
                        <td>{{ $category->request_pdf }}</td>
                        <td>
                        <?php
                            if($category->request_priority == 0){echo "Low";}
                            if($category->request_priority == 1){echo "Medium";}
                            if($category->request_priority == 2){echo "High";}

                            ?>


                        </td>
                                   <td>
                                       <?php
                                       if($category->status == 2){echo "Declined";}
                                       else{echo "Accepted";}


                                       ?>
                                   </td>
                                   <td>
                                      <?php
                                       if($category->recurring_mode == 1){echo "Every Day";}
                                       if($category->recurring_mode == 2){echo "Weekly";}
                                       if($category->recurring_mode == 3){echo "After Every 15 Day";}
                                       if($category->recurring_mode == 4){echo "Monthly";}
                                       if($category->recurring_mode == 5){echo "After 2 Month";}
                                       if($category->recurring_mode == 6){echo "After 3 Month";}
                                       if($category->recurring_mode == 7){echo "After 4 Month";}
                                       if($category->recurring_mode == 8){echo "After 5 Month";}
                                       if($category->recurring_mode == 9){echo "After 6 Month";}
                                       if($category->recurring_mode == 10){echo "After 7 Month";}
                                       if($category->recurring_mode == 11){echo "After 8 Month";}
                                       if($category->recurring_mode == 12){echo "After 9 Month";}
                                       if($category->recurring_mode == 13){echo "After 10 Month";}
                                       if($category->recurring_mode == 14){echo "After 11 Month";}
                                       if($category->recurring_mode == 15){echo "Every Year";}
                                      ?>

                                   </td>
                                   <td>
                                       {{ $category->recurring_time }}
                                   </td>
                                   <td>


                                       <?php
                                       $Date=$category->recurring_time;
                                       if($category->recurring_mode == 1){echo date('d-m-Y', strtotime($Date. ' + 1 days'));}
                                       if($category->recurring_mode == 2){echo date('d-m-Y', strtotime($Date. ' + 7 days'));}
                                       if($category->recurring_mode == 3){echo date('d-m-Y', strtotime($Date. ' + 15 days'));}
                                       if($category->recurring_mode == 4){echo date('d-m-Y', strtotime($Date. ' + 30 days'));}
                                       if($category->recurring_mode == 5){echo date('d-m-Y', strtotime($Date. ' + 60 days'));}
                                       if($category->recurring_mode == 6){echo date('d-m-Y', strtotime($Date. ' + 90 days'));}
                                       if($category->recurring_mode == 7){echo date('d-m-Y', strtotime($Date. ' + 120 days'));}
                                       if($category->recurring_mode == 8){echo date('d-m-Y', strtotime($Date. ' + 150 days'));}
                                       if($category->recurring_mode == 9){echo date('d-m-Y', strtotime($Date. ' + 180 days'));}
                                       if($category->recurring_mode == 10){echo date('d-m-Y', strtotime($Date. ' + 210 days'));}
                                       if($category->recurring_mode == 11){echo date('d-m-Y', strtotime($Date. ' + 240 days'));}
                                       if($category->recurring_mode == 12){echo date('d-m-Y', strtotime($Date. ' + 270 days'));}
                                       if($category->recurring_mode == 13){echo date('d-m-Y', strtotime($Date. ' + 300 days'));}
                                       if($category->recurring_mode == 14){echo date('d-m-Y', strtotime($Date. ' + 330 days'));}
                                       if($category->recurring_mode == 15){echo date('d-m-Y', strtotime($Date. ' + 360 days'));}

                                       ?>
                                   </td>
                        </tr>
                        <?php
                           }else{
                               ?>
                           <tr>
                               <td colspan="11">No Work Request Found</td>
                           </tr>

                            <?php
                                } }
                               ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="createWorkRequest">

        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

            </div>
        </div>

    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
            <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );



    $('.editWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Work Request");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('.assignWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.assign',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Assign Employee");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('.statusWorkRequest').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('admin.work-order.status',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Status");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });



    $('.deleteWorkRequest').click(function(){

        var id = $(this).data('cat-id');
        var url = "{{ route('admin.work-order.delete',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'POST'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    window.location.reload();
                }
            }
        });
    });


    // image file
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.rfi.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#taskID').val();
        formData.append('task_id', ids);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "RFI Created";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.rfi.index') }}'

    });

    // pdf file
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone1", {
        url: "{{ route('admin.rfi.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#taskID').val();
        formData.append('task_id', ids);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "RFI Created";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.rfi.index') }}'

    });



</script>
@endpush