@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #042c74"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
        {{--<div class="col-md-12">
            <div class="form-group">
                <div class="example">
                    <h5 class="box-title m-t-30">@lang('app.selectDateRange')</h5>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"
                               value="{{ \Carbon\Carbon::today()->subDays(15)->format('d-m-Y') }}"/>
                        <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                        <input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"
                               value="{{ \Carbon\Carbon::today()->addDays(15)->format('d-m-Y') }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.status')</label>
                <select class="form-control" name="status" id="status" data-style="form-control">
                    <option value="all">@lang('modules.client.all')</option>
                    <option value="active">@lang('app.active')</option>
                    <option value="deactive">@lang('app.inactive')</option>
                </select>
            </div>
        </div>--}}

        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('modules.employee.projects')</label>
                <select class="form-control select2" name="projects" id="projects" data-style="form-control">
                    <option value="">@lang('modules.client.all')</option>
                    @forelse($projectslist as $projects)
                        <option value="{{$projects->id}}">{{ ucfirst($projects->project_name) }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>

        {{--
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">@lang('modules.employee.role')</label>
                        <select class="form-control select2" name="role" id="role" data-style="form-control">
                            <option value="all">@lang('modules.client.all')</option>
                            @forelse($roles as $role)
                                @if ($role->id <= 3)
                                    <option value="{{$role->id}}">{{ __('app.' . $role->name) }}</option>
                                @else
                                    <option value="{{$role->id}}">{{ ucfirst($role->name )}}</option>
                                @endif
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
        --}}
        {{-- <div class="col-md-12">
             <div class="form-group">
                 <label class="control-label">@lang('app.designation')</label>
                 <select class="form-control select2" name="designation" id="designation" data-style="form-control">
                     <option value="all">@lang('modules.client.all')</option>
                     @forelse($designations as $designation)
                         <option value="{{$designation->id}}">{{ ucfirst($designation->name) }}</option>
                     @empty
                     @endforelse
                 </select>
             </div>
         </div>--}}
        {{--<div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.department')</label>
                <select class="form-control select2" name="department" id="department" data-style="form-control">
                    <option value="all">@lang('modules.client.all')</option>
                    @forelse($departments as $department)
                        <option value="{{$department->id}}">{{ ucfirst($department->team_name) }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>--}}
        {{--<div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.skills')</label>
                <select class="select2 m-b-10 select2-multiple " multiple="multiple"
                        data-placeholder="Choose Skills" name="skill[]" id="skill" data-style="form-control">
                    <option value="all">@lang('modules.client.all')</option>
                    @forelse($skills as $skill)
                        <option value="{{$skill->id}}">{{ ucfirst($skill->name) }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>--}}
        <div class="col-md-12">
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')
            </button>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <a href="{{ route('admin.contractors.create') }}" class="btn btn-outline btn-success btn-sm">@lang('modules.contractors.addNewContractor') <i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm pull-right toggle-filter"><i class="fa fa-cogs"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="table-responsive">
                <table class="table table-bordered" id="users-table">
                    <thead>
                    <tr>
                        <th>@lang('app.sno')</th>
                        <th>@lang('app.name')</th>
                        <th>@lang('app.email')</th>
                        <th>@lang('app.mobile')</th>
                        <th>@lang('app.status')</th>
                        <th>@lang('app.createdAt')</th>
                        <th>@lang('app.action')</th>
                    </tr>
                    </thead>
                </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        "use strict";
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        });

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        $('#filter-results').click(function () {
            loadTable();
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.contractors.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            });

            function loadTable() {
                var startDate = $('#start-date').val();
                var projects = $('#projects').val();
                if (startDate == '') {
                    startDate = null;
                }

                var endDate = $('#end-date').val();

                if (endDate == '') {
                    endDate = null;
                }
                var status = $('#status').val();
                var client = $('#client').val();

                table = $('#users-table').dataTable({
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    stateSave: true,
                    ajax: '{!! route('admin.contractors.data') !!}?projects=' + projects + '&startDate=' + startDate + '&endDate=' + endDate + '&client=' + client + '&status=' + status,
                    language: {
                        "url": "<?php echo __("app.datatable") ?>"
                    },
                    "fnDrawCallback": function (oSettings) {
                        $("body").tooltip({
                            selector: '[data-toggle="tooltip"]'
                        });
                    },
                    columns: [
                        { data: 'DT_RowIndex', orderable: false, searchable: false },
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'mobile', name: 'mobile'},
                        {data: 'status', name: 'status'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action'}
                    ]
                })
            }

        function exportData(){

            var client = $('#client').val();
            var status = $('#status').val();

            var url = '{{ route('admin.clients.export', [':status', ':client']) }}';
            url = url.replace(':client', client);
            url = url.replace(':status', status);

            window.location.href = url;
        }

    </script>
@endpush