@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #042c74" ><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.contractors.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.contractors.updateContractor')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateContractor','class'=>'ajax-form','method'=>'PUT']) !!}
                        <div class="form-body">

                            <h3 class="box-title">@lang('modules.contractors.contractorDetails')</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>@lang('modules.contractors.contractorName')</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{ $contractor->name }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.contractors.contractorEmail')</label>
                                        <input type="email" name="email" id="email" class="form-control" value="{{ $contractor->email }}">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.contractors.mobile')</label>
                                        <input type="number" name="mobile" id="mobile" onkeypress="return isNumberKey(event)" required maxlength="10" pattern="\d{10}" class="form-control" value="{{ $contractor->mobile }}">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.contractors.gender')</label>
                                        <select name="gender" id="gender" class="form-control">
                                            <option value="male" @if($contractor->gender=='male') selected @endif>@lang('app.male')</option>
                                            <option value="female" @if($contractor->gender=='female') selected @endif>@lang('app.female')</option>
                                            <option value="others" @if($contractor->gender=='others') selected @endif>@lang('app.others')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                           {{-- <h3 class="box-title m-t-40">@lang('modules.contractors.companyDetails')</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.contractors.companyName')</label>
                                        <input type="text" id="company_name" name="firmname" class="form-control"  value="{{ $contractor->firmname ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gst_number">@lang('app.gstNumber')</label>
                                        <input type="text" id="gst_number" name="gst_number" class="form-control" value="{{ $contractor->gst_no ?? '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.address')</label>
                                        <textarea name="address"  id="address"  rows="5" class="form-control">{{ $contractor->address ?? '' }}</textarea>
                                    </div>
                                </div>
                            </div>--}}
                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.update')</button>
                            <a href="{{ route('admin.contractors.index') }}" class="btn btn-default">@lang('app.back')</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    
    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.contractors.update', [$contractor->id])}}',
            container: '#updateContractor',
            type: "POST",
            redirect: true,
            data: $('#updateContractor').serialize()
        })
    });
</script>
@endpush