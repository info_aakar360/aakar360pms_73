@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')

    <div class="row">
        {{--<div class="col-md-3">--}}
            {{--<div class="white-box bg-inverse">--}}
                {{--<h3 class="box-title text-white">@lang('app.total') @lang('app.menu.products')</h3>--}}
                {{--<ul class="list-inline two-part">--}}
                    {{--<li><i class="icon-basket text-white"></i></li>--}}
                    {{--<li class="text-right"><span id="totalWorkingDays" class="counter text-white">{{ $totalProducts }}</span></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--Filter Pannel--}}
        <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;height: auto" id="ticket-filters">
            <div class="col-md-12 m-t-50">
                <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
            </div>
            {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
            <div class="col-md-12">
                <div class="form-group">
                    <div class="example">
                        <h5 class="box-title m-t-30">@lang('app.selectDateRange')</h5>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"
                                   value=""/>
                            <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                            <input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"
                                   value=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">@lang('app.name')</label>
                    <input type="text" name="name" value="" id="name" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Product Category</label>
                    <select class="form-control select2" name="product_trade" id="product_trade" data-style="form-control">
                        <option value="all">@lang('modules.client.all')</option>
                        @forelse($trades as $trade)
                            <option value="{{$trade->id}}">{{ ucfirst($trade->name) }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">@lang('app.price') (@lang('app.inclusiveAllTaxes'))</label>
                    <input type="text" name="price" value="" id="price" class="form-control">
                </div>
            </div> <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">@lang('app.unit')</label>
                    <select class="form-control select2" name="unit" id="unit" data-style="form-control">
                        <option value="all">@lang('modules.client.all')</option>
                        @forelse($units as $unit)
                            <option value="{{$unit->id}}">{{ ucfirst($unit->name) }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')
                </button>
            </div>
            {!! Form::close() !!}
        </div>
        {{--End Filter Pannel--}}
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.products.create') }}" class="btn btn-outline btn-success btn-sm">@lang('app.addNew') @lang('app.menu.products') <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm pull-right toggle-filter" style="margin-left: 10px;"><i class="fa fa-times-circle-o"></i></a>
                            <a href="javascript:;" onclick="exportData()" class="btn btn-info btn-sm pull-right"><i class="ti-export" aria-hidden="true" ></i> @lang('app.exportExcel')</a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="products-table">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.name')</th>
                            <th>Product Category</th>
                            <th>@lang('app.price') (@lang('app.inclusiveAllTaxes'))</th>
                            <th>@lang('app.unit')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script>
        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        });
        $('#filter-results').click(function () {
            loadTable();
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        });
        var table;
        function exportData(){
            var url = '{{ route('admin.products.export') }}';
            window.location.href = url;
        }
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted product!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.products.destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });
        });
        function loadTable() {
            var startDate = $('#start-date').val();
            if (startDate == '') {
                startDate = null;
            }
            var endDate = $('#end-date').val();
            if (endDate == '') {
                endDate = null;
            }
            var name = $('#name').val();
            var product_trade   = $('#product_trade').val();
            var price     = $('#price').val();
            var unit     = $('#unit').val();
            table = $('#products-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.products.data') !!}?name=' + name + '&product_trade=' + product_trade + '&price=' + price + '&unit=' + unit + '&startDate=' + startDate+ '&endDate=' + endDate ,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'trade_id', name: 'trade_id'},
                    {data: 'price', name: 'price'},
                    {data: 'unit_id', name: 'unit_id'},
                    {data: 'action', name: 'action'}
                ]
            });
        }

    </script>
@endpush