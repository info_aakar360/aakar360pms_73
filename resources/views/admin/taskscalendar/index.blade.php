@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

@endpush

@section('content')

    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <form method="post" action="">
            <div class="col-md-12 m-t-50">
                <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
            </div>
           {{-- <div class="col-md-12">
                <div class="form-group" >
                    <h5 class="box-title m-t-30">@lang('app.date')</h5>
                    <div class="input-daterange" id="date-range">
                        <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.date')"   />
                    </div>
                </div>
            </div>--}}
            <div class="col-md-12">
                <div class="form-group" >
                    <h5 class="box-title">  @lang('app.project') </h5>
                    <select class="select2 form-control" name="project" data-placeholder="@lang('app.selectProject')" id="selectproject">
                        <option value=""> @lang('app.project')</option>
                        @foreach($projectslist as $employe)
                            <option value="{{ $employe->id }}">{{ ucwords($employe->project_name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title m-t-10"> </h5>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.apply')</button>
                <button type="button" class="btn btn-danger" id="reset-filters"><i class="fa fa-check"></i> @lang('app.reset')</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
                <div class="col-md-10">
                </div>
        </div>
    </div>
    <div class="row">
        <div class="white-box" style="padding:0px !important">
        <div class="col-md-11">
                <h3 class="box-title">@lang('app.menu.taskCalendar')</h3>
            </div>
        </div>
        <div class="col-sm-1 text-right hidden-xs">
            <div class="form-group">
                <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box" style="padding:0px !important">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Sub Task e</span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')

<script>
    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });
    $('#filter-results').click(function () {
        loadTable();
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });

    $('#reset-filters').click(function () {
        $('#selectuser').val("");
        $('#start-date').val("");
        $('#status').val("");
        loadTable();
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });


    var taskEvents = [
        @foreach($tasks as $task)
            @if (is_null($task->start_date))
                {
                    id: '{{ $task->id }}',
                    url: '{{ route('admin.all-tasks.updateTask',$task->id) }}',
                    title: 'Task - {{ ucfirst($task->heading) }}',
                    start: '{{ $task->due_date }}',
                    className: 'bg-success'
                },
            @else
                {
                    id: '{{ $task->id }}',
                    url: '{{ route('admin.all-tasks.updateTask',$task->id) }}',
                    title: 'Task - {{ ucfirst($task->heading) }}',
                    start: '{{ $task->start_date }}',
                    end:  '{{ $task->due_date->addDay() }}',
                    className: 'bg-success'
                },
            @endif
        @endforeach
        @foreach($todolist as $task)
            @if (is_null($task->start_date))
                {
                    id: '{{ $task->id }}',
                    title: 'Todo - {{ ucfirst($task->title) }}',
                    url: '{{ route('admin.todo.edit',$task->id) }}',
                    start: '{{ $task->due_date }}',
                    className: 'bg-dander'
                },
            @else
                {
                    id: '{{ $task->id }}',
                    title: 'Todo - {{ ucfirst($task->title) }}',
                    url: '{{ route('admin.todo.edit',$task->id) }}',
                    start: '{{ $task->start_date }}',
                    end:  '{{ $task->due_date->addDay() }}',
                    className: 'bg-dander'
                },
            @endif
        @endforeach
        @foreach($issueslist as $issue)
            @if (is_null($issue->start_date))
                {
                    id: '{{ $issue->id }}',
                    title: 'Issue - {{ ucfirst($issue->title) }}',
                    url: '{{ route('admin.issue.reply',$issue->id) }}',
                    start: '{{ $issue->due_date }}',
                    className: 'bg-warning'
                },
            @else
                {
                    id: '{{ $issue->id }}',
                    title: 'Issue - {{ ucfirst($issue->title) }}',
                    url: '{{ route('admin.issue.reply',$issue->id) }}',
                    start: '{{ $issue->start_date }}',
                    end:  '{{ $issue->due_date }}',
                    className: 'bg-warning'
                },
            @endif
            @endforeach
            @foreach($rfilist as $rfi)
            @if (!is_null($rfi->due_date))
                {
                    id: '{{ $rfi->id }}',
                    title: 'Rfi - {{ ucfirst($rfi->title) }}',
                    url: '{{ route('admin.rfi.details',$rfi->id) }}',
                    start: '{{ $rfi->due_date }}',
                    className: 'bg-info'
                },
             @endif
            @endforeach
            @foreach($indentlist as $indent)
            @if (!is_null($indent->expected_date)){
                 <?php $duedate = \Carbon\Carbon::createFromFormat($global->date_format, $indent->expected_date)->format('Y-m-d'); ?>
                    id: '{{ $indent->id }}',
                    title: 'Indent - {{ ucfirst($indent->indent_no) }}',
                    url: '{{ route('admin.indent.viewIndent',$indent->id) }}',
                    start: '{{ $duedate }}',
                    className: 'bg-info'
                },
             @endif
            @endforeach
            @foreach($submittalslist as $submittals)
            @if (!is_null($submittals->submitdate))
                {
                    id: '{{ $submittals->id }}',
                    title: 'Submittals - {{ ucfirst($submittals->title) }}',
                    url: '{{ route('admin.submittals.details',$submittals->id) }}',
                    start: '{{ $submittals->submitdate }}',
                    className: 'bg-info'
                },
        @endif
        @endforeach
            @foreach($inspectionlist as $inspection)
            @if (!is_null($inspection->due_date)){
                <?php $duedate = \Carbon\Carbon::createFromFormat($global->date_format, $inspection->due_date)->format('Y-m-d'); ?>
                    id: '{{ $inspection->id }}',
                    title: 'Inspection - {{ ucfirst($inspection->name) }}',
                    url: '{{ route('admin.inspectionName.inspectionAssignForm',$inspection->id) }}',
                    start: '{{ $duedate }}',
                    className: 'bg-primary'
                },
             @endif
             @endforeach
            @foreach($observationslist as $observations)
            @if (is_null($observations->start_date))
        {
            id: '{{ $observations->id }}',
            title: 'Observation - {{ ucfirst($observations->title) }}',
            url: '{{ route('admin.observations.reply',$inspection->id) }}',
            start: '{{ $observations->due_date }}',
            className: 'bg-primary'
        },
            @else
        {
            id: '{{ $observations->id }}',
            title: 'Observation - {{ ucfirst($observations->title) }}',
            url: '{{ route('admin.observations.reply',$observations->id) }}',
            start: '{{ $observations->start_date }}',
            end:  '{{ $observations->due_date }}',
            className: 'bg-primary'
        },
        @endif
        @endforeach

            @foreach($meetingslist as $meetings)
            @if (!is_null($meetings->meeting_date)){
            <?php
                $duedate = \Carbon\Carbon::createFromFormat($global->date_format, $meetings->meeting_date)->format('Y-m-d');
                $starttime = $duedate.'T'.$meetings->start_time.':00';
                $endtime = $duedate.'T'.$meetings->finish_time.':00';
            ?>
            id: '{{ $meetings->id }}',
            title: 'Meetings - {{ ucfirst($meetings->meetingtitle) }}',
            url: '{{ route('admin.meetings.viewMeeting',$meetings->id) }}',
            start: '{{ $starttime }}',
            end: '{{ $endtime }}',
            className: 'bg-danger'
        },
        @endif
        @endforeach
];
    // only use for sidebar call method
    function loadData(){}

    // Task Detail show in sidebar
    var getEventDetail = function (id) {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('admin.all-tasks.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    }
    var calendarLocale = '{{ $global->locale }}';
    var firstDay = '{{ $global->week_start }}';
</script>

<script src="{{ asset('plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/locale-all.js') }}"></script>
<script src="{{ asset('js/task-calendar.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    jQuery('#date-range').datepicker({
        toggleActive: true,
        language: '{{ $global->locale }}',
        autoclose: true,
        format: '{{ $global->date_picker_format }}',
    });
</script>

@endpush
