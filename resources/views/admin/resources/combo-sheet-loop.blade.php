                    <?php
                        $res_count = count($combos);
                        $totalRow = 15 + $res_count;
                        if($totalRow <= 1){
                            $totalRow = 2;
                        }
                        $row = 1;
                        $totalamt = 0;
                    ?>
                    @foreach($combos as $combo)
                        <?php
                        $totalamt += $combo->final_rate;
                        ?>
                        <tr class="resource_list" data-row="{{$row}}" data-id="{{ $combo->id }}">
                            <td data-col="0"><a href="javascript:void(0);" data-col="0" data-row="{{$row}}" class="removecombosheet" ><i class="fa fa-trash"></i></a></td>
                            <td data-col="1">
                                <span class="cubeicon" data-col="1" data-row="{{$row}}">@if(get_resource_category($combo->combo_resource_id)==0)<i class="fa fa-square"></i> @else <i class="fa fa-th-large"></i> @endif</span>
                                <input  list="resourcelist{{$row}}" data-row="{{$row}}" class="cell-inp" value="{{ get_resource_name($combo->combo_resource_id) }}"  />
                                <datalist id="resourcelist{{$row}}">
                                    @foreach($resources as $resource)
                                        <?php
                                        $type = getResourceType($resource->id);
                                        ?>
                                        <option value="{{$resource->id}}" @if($combo->combo_resource_id == $resource->id) selected @endif>{{ $type }} - {{ $resource->description }}</option>
                                    @endforeach
                                </datalist>
                                <input type="hidden" name="resource[]" value="{{ $combo->combo_resource_id }}" data-col="1" data-row="{{$row}}" class="cell-inp resourceList-hidden" />
                                <input type="hidden" name="sheetid[]" value="{{ $combo->id }}" data-row="{{$row}}" class="cell-inp sheetid" />
                             </td>
                            <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->rate }}" disabled="disabled"></td>
                            <td data-col="3"><input type="text" name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="{{ get_unit_name($combo->unit) }}" disabled="disabled"></td>
                            <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->formula }}"/></td>
                            <td data-col="5"><input type="text" name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->final_rate }}" disabled="disabled"></td>
                            <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->remark }}"></td>
                        </tr>
                        <?php $row++ ?>
                    @endforeach
                    @for($i=0;$i<$totalRow;$i++)
                        @if($i==0)
                            <tr data-row="{{$row}}" class="resource_list">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1">
                                    <span class="cubeicon" data-col="1" data-row="{{$row}}"></span>
                                    <input  list="resourcelist{{$row}}" data-col="1" class="cell-inp resourceList" data-row="{{$row}}"  />
                                    <datalist id="resourcelist{{$row}}">
                                        @foreach($resources as $resource)
                                            <?php
                                            $type = getResourceType($resource->id);
                                            ?>
                                            <option value="{{$resource->id}}" >{{ $type }} - {{ $resource->description }}</option>
                                        @endforeach
                                    </datalist>
                                    <input type="hidden" name="resource[]" data-col="1" data-row="{{$row}}" class="cell-inp resourceList-hidden" />
                                    <input type="hidden" name="sheetid[]"  data-row="{{$row}}" class="cell-inp sheetid" />
                                </td>
                                <td data-col="2"><input type="text"  name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="3"><input type="text" name="unit[]"  data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]"  data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value=""/></td>
                                <td data-col="5"><input type="text"  name="result[]"  data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="6"><input type="text"  name="remark[]"  data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                            </tr>
                        @elseif($i==1)
                            <tr data-row="{{$row}}" class="final_result">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1"><input type="text"  data-col="1" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                <td data-col="2"><input type="text" name="rate[]"  data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="= APPLIED FACTOR" disabled="disabled"></td>
                                <td data-col="3"><input type="text"  name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value=""/></td>
                                <td data-col="5"><input type="text" name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="{{ number_format($totalamt, 2, '.', '') }}" disabled="disabled"></td>
                                <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new"  disabled="disabled"></td>
                            </tr>
                        @else
                            <tr data-row="{{$row}}" class="extra_rows">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1"><input type="text" data-col="1" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="3"><input type="text" name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"/></td>
                                <td data-col="5"><input type="text"  name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="6"><input type="text"  name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                             </tr>
                        @endif
                        <?php
                            $row++;
                        ?>
                    @endfor