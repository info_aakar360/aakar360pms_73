@extends('layouts.app')

@section('page-title')

@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<style>
    .rc-handle-container {
        position: relative;
    }
    .rc-handle {
        position: absolute;
        width: 7px;
        cursor: ew-resize;
        margin-left: -3px;
        z-index: 2;
    }
    table.rc-table-resizing {
        cursor: ew-resize;
    }
    table.rc-table-resizing thead,
    table.rc-table-resizing thead > th,
    table.rc-table-resizing thead > th > a {
        cursor: ew-resize;
    }
  /*  .combo-sheet{
        overflow-x: scroll;
        overflow-y: auto;
        max-height: 75vh;
        border: 2px solid #bdbdbd;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
    }
    .combo-sheet .table-wrapper{
        min-width: 1350px;
    }
    .combo-sheet thead{
        background-color: #C2DCE8;
    }
    .combo-sheet thead tr th{
        background-color: #C2DCE8 !important;
    }
    .combo-sheet thead tr th{
        font-size: 12px;
        background-color: #C2DCE8;
        color: #3366CC;
        padding: 3px 5px !important;
        text-align: center;
        position: sticky;
        top: -1px;
        z-index: 1;
    }
    .combo-sheet td{
        padding: 0 !important;
        font-size: 12px;
    }
    .combo-sheet td[data-col="0"] {
        text-align: center;
    }
    .combo-sheet td[data-col="1"] {
        background-color: #ddd;
    }
    .combo-sheet tr td {
        position: relative;
    }
    .combo-sheet td input.cell-inp{
        min-width: 100%;
        max-width: 100%;
        width: 100%;
        border: none !important;
        padding: 3px 5px;
        cursor: default;
        color: #000000;
        font-size: 1.2rem;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
    }
    .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border: 1px solid #bdbdbd;
    }
    .combo-sheet tr:hover td, .combo-sheet tr:hover input{
        background-color: #ffdd99;
    }
    .combo-sheet table{
        border-collapse: collapse;
        border-spacing: 0px;
    }
    .formula-sheet .loaderx{
        padding: 18% 0;
    }
    .white-box{
        position: relative;
    }
    .RowCategory{
        width: 63px;
    }
    .RowType{
        width: 45px;
    }
    .RowCode{
        width: 145px;
    }
    .RowDescription{
        width: 255px;
    }
    .RowUnit{
        width: 90px;
    }
    .RowBaseRate{
        width: 90px;
    }
    .RowSurcharge{
        width: 117px;
    }
    .RowDiscount{
        width: 108px;
    }
    .RowFactor{
        width: 90px;
    }
    .RowFinalRate{
        width: 90px;
    }
    .RowRemark{
        width: 360px;
    }
    .white-box{
        padding: 5px !important;
    }
    .pdl10{
        padding-left: 10px !important;
    }
    .page-title{
        color: #0f49bd;
        font-weight: 900;
    }
    .page-title i{
        margin-right: 5px;
    }
    input.cell-inp:disabled{
        background: #dddddd !important;
    }
*/

    .combo-sheet tr td {
        position: relative;
    }
    .combo-sheet select.cell-inp{
        padding: 2.5px 0;
        width: 100%;
    }
    .combo-sheet tr td input[data-col="5"]{
        text-align: right;
        padding-right: 16px;
    }
    .combo-sheet.formula-sheet tr td input[data-col="5"]{
        text-align: left;
    }
    .combo-sheet.formula-sheet{
        position: absolute;
        left: 0;
        top: 0;
        overflow-x: scroll;
        overflow-y: auto;
        z-index: 2;
        background: #ffffff;
        margin: 0 5px;
        right: 0;
        display: none;
        max-height: 100vh;
        bottom: 0;
    }
    .open-combo-sheet{
        position: absolute;
        right: 5px;
        font-size: 16px;
        padding: 0 3px;
    }
</style>

@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row pdl10">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
                    </div>
                    <!-- /.page title -->
                    <!-- .breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    </div>
                    <!-- /.breadcrumb -->
                </div>
                <div class="combo-sheet cs">
                    <div class="table-wrapper">
                        <table class="table table-bordered "   >
                             <thead>
                                    <tr>
                                        <th class="RowRemove" data-resizable-column-id="remove" style="width: 2%;"></th>
                                        <th class="RowCategory" data-resizable-column-id="category">@lang('modules.resources.category')</th>
                                        <th class="RowType" data-resizable-column-id="type">@lang('modules.resources.type')</th>
                                        <th class="RowDescription" data-resizable-column-id="description">@lang('modules.resources.description')</th>
                                        <th class="RowUnit" data-resizable-column-id="unit">@lang('modules.resources.unit')</th>
                                        <th class="RowFinalRate" data-resizable-column-id="final_rate">@lang('modules.resources.rate')</th>
                                        <th class="RowRemark" data-resizable-column-id="remark">@lang('modules.resources.remark')</th>
                                    </tr>
                                </thead>
                                <?php
                                $res_count = count($resources);?>
                                <tbody id="resourceloop">

                                </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="combo-sheet formula-sheet">
                <div class="loaderx">
                    <div class="cssload-speeding-wheel"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('js/store.min.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        "use strict";
       /* document.querySelector('#categoryList').addEventListener('input', function(e) {
            var input = e.target,
                list = input.getAttribute('list'),
                options = document.querySelectorAll('#' + list + ' option[value="'+input.value+'"]'),
                hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
            if (options.length > 0) {
                hiddenInput.value = input.value;
                input.value = options[0].innerText;
            }
        });
        document.querySelector('#typeList').addEventListener('input', function(e) {
            var input = e.target,
                list = input.getAttribute('list'),
                options = document.querySelectorAll('#' + list + ' option[value="'+input.value+'"]'),
                hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
            if (options.length > 0) {
                hiddenInput.value = input.value;
                input.value = options[0].innerText;
            }
        });*/
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        loadTable();

        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){

                var row = $(this).attr('data-row');
                var id = $(this).attr('data-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = '{{ route('admin.resources.remove') }}';
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {_token: '{{ csrf_token() }}',id:id},
                            success: function(data){
                                var rData = JSON.parse(data);
                                if(rData.status=='success'){
                                    loadTable();
                                }
                            }
                        })
                    }
                });
            });

            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this PO?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this PO?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('member.purchase-order.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    loadTable();
                                }
                            }
                        });
                    }
                });
            });

            });

            function loadTable() {
             /*   table = $('#users-table').resizableColumns({
                    responsive: true,
                    processing: true,
                    destroy: true,
                    stateSave: true,
                    language: {
                        "url": "<?php echo __("app.datatable") ?>"
                    },
                    "fnDrawCallback": function (oSettings) {
                        $("body").tooltip({
                            selector: '[data-toggle="tooltip"]'
                        });
                    }
                });*/
                var url = "{{ route('admin.resources.resourceloop') }}";
                var token = "{{ csrf_token() }}";
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {'_token': token},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function (response) {

                        $(".preloader-small").hide();
                        $("#resourceloop").html("");
                        $("#resourceloop").html(response);
                    }
                });
            }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })


        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });

        $(document).on('change', '.cs input.cell-inp[data-col=1]', function () {
            var cat = $(this);
            var row = cat.data('row');
            if(cat.val() == 1){
                $('.cs select.cell-inp[data-row='+row+'][data-col=2]').val(0);
                $('.cs input.cell-inp[data-row='+row+'][data-col=5]').attr('disabled', 'disabled');
            }else{
                $('.cs select.cell-inp[data-row='+row+'][data-col=2]').val('');
                $('.cs input.cell-inp[data-row='+row+'][data-col=5]').removeAttr('disabled');
            }
        });
        $(document).on('change', '.cs select.cell-inp[data-col=2]', function () {
            var type = $(this);
            var row = type.data('row');
            if(type.val() == 0){
                $('.cs select.cell-inp[data-row='+row+'][data-col=1]').val(1);
                $('.cs input.cell-inp[data-row='+row+'][data-col=5]').attr('disabled', 'disabled');
            }else{
                $('.cs input.cell-inp[data-row='+row+'][data-col=5]').removeAttr('disabled');
            }
        });

        var resCount = parseInt('{{$res_count ?: 0}}');
        $(document).on('change', '.cs input.cell-inp[data-col=3]', function () {
            var desc = $(this);
            var row = desc.data('row');
            var cat = $('.cs input.cell-inp[data-row='+row+'][data-col=1]');
            var type = $('.cs input.cell-inp[data-row='+row+'][data-col=2]');
            var old = $('.cs tr[data-row='+row+']').data('id');
            var catName = $("#categoryList").val();
            var typeName = $("#typeList").val();
            var url = '';
            var method = 'POST';
            if(!old) {
                resCount+=1;
                url = '{{ route('admin.resources.store') }}';
            }else{
                url = '{{ route('admin.resources.update', [':id']) }}';
                url = url.replace(':id', old);
                method = 'PATCH';
            }
            if (cat.val() !== '' && type.val() !== '') {
                var fcat = cat.val();
                var ftype = type.val();
                var description = desc.val();
                var unit = $('.cs input.cell-inp[data-row=' + row + '][data-col=4]').val();
                var rate = $('.cs input.cell-inp[data-row=' + row + '][data-col=5]').val();
                var remark = $('.cs input.cell-inp[data-row=' + row + '][data-col=6]').val();

                $.ajax({
                    url: url,
                    type: method,
                    data: '_token={{csrf_token()}}&category=' + fcat + '&type=' + ftype + '&description=' + description + '&unit=' + unit + '&rate=' + rate + '&remark=' + remark,
                    beforeSend: function () {
                        $('.cs tr[data-row=' + row + '] td,.cs tr[data-row=' + row + '] td select,.cs tr[data-row=' + row + '] td input').css('cursor', 'wait');
                    },
                    success: function (data) {
                        var rData = JSON.parse(data);
                        if (rData.status == 'success') {
                           loadTable();
                        }
                    }
                })
            } else {
                alert('Category and Type is compulsory!');
                cat.focus();
            }
        });
        $(document).on('change', '.combo-sheet.cs  input.cell-inp[data-col=4],.combo-sheet.cs  input.cell-inp[data-col=5],.combo-sheet.cs  input.cell-inp[data-col=6]', function () {
            var desc = $(this);
            var row = desc.data('row');
            var cat = $('.cs input.cell-inp[data-row='+row+'][data-col=1]');
            var type = $('.cs input.cell-inp[data-row='+row+'][data-col=2]');
            var old = $('.cs tr[data-row='+row+']').data('id');
            var url = '';
            var method = 'POST';
            if(!old) {
                url = '{{ route('admin.resources.store') }}';
            }else{
                url = '{{ route('admin.resources.update', [':id']) }}';
                url = url.replace(':id', old);
                method = 'PATCH';
            }
                if (cat.val() !== '' && type.val() !== '') {
                    var fcat = cat.val();
                    var ftype = type.val();
                    var description = $('.cs input.cell-inp[data-row=' + row + '][data-col=3]').val();
                    var unit = $('.cs input.cell-inp[data-row=' + row + '][data-col=4]').val();
                    var rate = $('.cs input.cell-inp[data-row=' + row + '][data-col=5]').val();
                    var remark = $('.cs input.cell-inp[data-row=' + row + '][data-col=6]').val();

                    $.ajax({
                        url: url,
                        type: method,
                        data: '_token={{csrf_token()}}&category=' + fcat + '&type=' + ftype + '&description=' + description + '&unit=' + unit + '&rate=' + rate + '&remark=' + remark,
                        beforeSend: function () {
                            $('.cs tr[data-row=' + row + '] td,.cs tr[data-row=' + row + '] td select,.cs tr[data-row=' + row + '] td input').css('cursor', 'wait');
                        },
                        success: function (data) {
                            loadTable();
                        }
                    })
                } else {
                    alert('Category and Type are compulsory!');
                    cat.focus();
                }
        });
        var cTable = null;
        $(document).on('click', '.open-combo-sheet', function(){
            var id = $(this).data('id');
            var url = '{{ route('admin.resources.getComboSheet', [':id']) }}';
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'POST',
                data: {_token: '{{ csrf_token() }}'},
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
                        '                        <div class="cssload-speeding-wheel"></div>' +
                        '                    </div>';

                    $('.formula-sheet').html(html).show();
                },
                success: function(data){
                    $('.formula-sheet').html(data);
                    loadTable();
                }
            })
        });

        $(document).on('click', '.close-combo-sheet', function(){
            $('.formula-sheet').html('').hide();
        })
    </script>
@endpush