@php $res_count = 0; @endphp
<?php
$res_count = count($resources);
$totalRow = 35 - $res_count;
if($totalRow <= 1){
    $totalRow = 2;
}
$row = 1;
?>
@if(count($resources)>0)
    @foreach($resources as $resource)
        <tr class="resourcelist" data-row="{{$row}}"  data-id="{{ $resource->id }}">
            <td data-col="0"><a href="javascript:;" data-placement="right" data-id="{{ $resource->id }}" data-toggle="tooltip" title="Delete" data-col="0" data-row="{{$row}}" class="cell-inp cell-new sa-params"><i class="fa fa-trash"></i></a></td>
            <td data-col="1">
                <span class="cubeicon" data-col="1" data-row="{{$row}}">@if($resource->category==0)<i class="fa fa-square"></i> @else <i class="fa fa-th-large"></i> @endif</span>
                <input name="category[]" data-col="1" list="categorylist{{ $resource->id }}" data-row="{{$row}}" value="{{ $categories[$resource->category] }}" class="cell-inp category" disabled="disabled" />
                <datalist id="categorylist{{ $resource->id }}">
                    @foreach($categories as $key=>$category)
                        <option value="{{$key}}">{{ $category }}</option>
                    @endforeach
                </datalist>
            </td>
            <td data-col="2">
                <input name="type[]" data-col="2" list="typelist{{ $resource->id }}"  data-row="{{$row}}" value="{{ get_type_name($resource->type) }}" class="cell-inp category" disabled="disabled" />
                <datalist id="typelist{{ $resource->id }}">
                    @foreach($types as $type)
                        <option value="{{ $type->id }}" @if($resource->type == $type->id) selected @endif>{{ ucwords($type->symbol) }} - {{ $type->title }}</option>
                    @endforeach
                    <option value="0" @if($resource->type == 0) selected @endif>MIX</option>
                </datalist>
            </td>
            <td data-col="3"><input type="text" name="description[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $resource->description }}"></td>
            <td data-col="4">
                <input name="unit[]" list="unitlist{{ $resource->id }}" data-col="4"  data-row="{{$row}}" class="cell-inp" value="{{ get_unit_name($resource->unit) }}" />
                <datalist id="unitlist{{ $resource->id }}">
                    @foreach($unitsarray as $units)
                        <option value="{{ $units->name }}" >{{ $units->name }}</option>
                    @endforeach
                </datalist>
            </td>
            <td data-col="5"><input type="text" name="rate[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $resource->rate }}" @if($resource->category == 1) disabled="disabled" @endif>@if($resource->category == 1) <a href="javascript:;" class="open-combo-sheet" title="Open Combo Sheet" data-toggle="tooltip" data-id="{{ $resource->id }}"><i class="fa fa-ellipsis-v"/></a>@endif</td>
            <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $resource->remark }}"></td>
        </tr>
        <?php $row++ ?>
    @endforeach
@endif
@for($i=0;$i<$totalRow;$i++)
    @if($i==0)
        <tr data-row="{{$row}}">
            <td data-col="0"><a href="javascript:;" data-col="0" data-row="{{$row}}" class="cell-inp cell-new sa-params"></a></td>
            <td data-col="1">
                <input id="categoryList" data-col="1"  data-row="{{$row}}"  list="categorylist" class="cell-inp"  />
                <datalist id="categorylist">
                    @foreach($categories as $key=>$category)
                        <option value="{{$key}}">{{ $category }}</option>
                    @endforeach
                </datalist>
              </td>
            <td data-col="2">
                <input id="typeList"  data-col="2"  list="typelist" data-row="{{$row}}" class="cell-inp" />
                <datalist id="typelist">
                    @foreach($types as $type)
                        <option value="{{ $type->id }}" data-title="{{ $type->title }}" >{{ ucwords($type->symbol) }} - {{ $type->title }}</option>
                    @endforeach
                    <option value="0" data-title="MIX"  >MIX</option>
                </datalist>
            </td>
            <td data-col="3"><input type="text" name="description[]" data-col="3" data-row="{{$row}}" class="cell-inp"></td>
            <td data-col="4">
                <input   name="unit[]" data-col="4"  list="unitlist" disabled="disabled" data-row="{{$row}}" class="cell-inp" />
                <datalist id="unitlist">
                    @foreach($unitsarray as $units)
                        <option value="{{ $units->name }}" >{{ $units->name }}</option>
                    @endforeach
                </datalist>
            </td>
             <td data-col="5"><input type="text" name="rate[]" disabled="disabled"  data-col="5" data-row="{{$row}}" class="cell-inp"></td>
            <td data-col="6"><input type="text" name="remark[]" disabled="disabled" data-col="6" data-row="{{$row}}" class="cell-inp"></td>
        </tr>
    @else

        <tr data-row="{{$row}}">
            <td data-col="0"><a href="javascript:;" data-col="0" data-row="{{$row}}" class="cell-inp cell-new sa-params"></a></td>
            <td data-col="1"><input type="text" name="category[]" data-col="1" data-row="{{$row}}" class="cell-inp cell-new category"></td>
            <td data-col="2"><input type="text" name="type[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"></td>
            <td data-col="3"><input type="text" name="description[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new"></td>
            <td data-col="4"><input type="text" name="unit[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new"></td>
            <td data-col="5"><input type="text" name="rate[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new"></td>
            <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new"></td>
        </tr>
    @endif
    <?php
    $row++;
    ?>
@endfor