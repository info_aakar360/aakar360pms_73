@push('head-script')

@endpush

@section('content')
<style>
    .RowResource{
        width: 255px;
    }
    .RowRate{
        width: 90px;
    }
    .RowUnitx{
        width: 90px;
    }
    .RowFormula{
        width: 255px;
    }
    .RowResult{
        width: 100px;
    }
    .close-combo-sheet{
        font-size: 16px;
    }
    .formula-sheet .cell-inp[data-col="2"]{
        text-align: right;
    }
    .combo-sheet td[data-col="0"] {
        text-align: center;
    }
    .formula-sheet thead th{
        font-size: 12px;
        background-color: #C2DCE8;
        color: #3366CC;
        padding: 3px 5px !important;
        text-align: center;
        position: sticky;
        top: -1px;
        z-index: 1;

    &:first-of-type {
         left: 0;
         z-index: 3;
     }
    }
</style>
<form method="post" autocomplete="off" id="combo-sheet-form">
    @csrf
    <div class="row pdl10">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="padding: 5px 15px;">
            <a href="javascript:;" class="btn btn-danger close-combo-sheet pull-right"><i class="fa fa-times"></i> Clost </a>
            <div class="col-md-3 pull-right">
                <select class="form-control selectfunction"  >
                    <option value="">Functions</option>
                    <option value="Waste()">Waste(n)%</option>
                    <option value="">---------------</option>
                    <option value="Min()">Min()</option>
                    <option value="Max()">Max()</option>
                    <option value="Sum()">Sum()</option>
                    <option value="Average()">Average()</option>
                    <option value="Abs()">Abs()</option>
                    <option value="Sqrt()">Sqrt()</option>
                    <option value="">---------------</option>
                    <option value="Round()">Round(n)</option>
                    <option value="RoundX()">Round(n;x)</option>
                    <option value="RoundUp()">RoundUp(n)</option>
                    <option value="RoundUpX()">RoundUp(n;x)</option>
                    <option value="RoundDown()">RoundDown(n)</option>
                    <option value="RoundDownX()">RoundDown(n;x)</option>
                    <option value="Ceiling()">Ceiling(n;x)</option>
                    <option value="Floor()">Floor(n;x)</option>
                    <option value="">---------------</option>
                    <option value="SinDeg()">SinDeg(n)</option>
                    <option value="CosDeg()">CosDeg(n)</option>
                    <option value="TanDeg()">TanDeg(n)</option>
                    <option value="">---------------</option>
                    <option value="AreaCir()">AreaCir(r)</option>
                    <option value="AreaTri()">AreaTri(b,h)</option>
                    <option value="AreaPyr()">AreaPyr(l,b,h)</option>
                    <option value="AreaRect()">AreaRect(x,y)</option>
                    <option value="AreaCyl()">AreaCyl(r,h)</option>
                    <option value="AreaCone()">AreaCone(r,h)</option>
                    <option value="AreaSph()">AreaSph(r)</option>
                    <option value="">---------------</option>
                    <option value="PerimCir()">PerimCir(r)</option>
                    <option value="PerimTriR()">PerimTriR(b,h)</option>
                    <option value="PerimRect()">PerimRect(x,y)</option>
                    <option value="">---------------</option>
                    <option value="VolPyr()">VolPyr(l,b,h)</option>
                    <option value="VolCyl()">VolCyl(r,h)</option>
                    <option value="VolCone()">VolCone(r,h)</option>
                    <option value="VolSph()">VolSph(r)</option>
                </select>
            </div>
            <div class="col-md-1 pull-right">
                <button class="btn btn-primary" id="combo-sheet-submit" >Save</button>
            </div>
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="table-wrapper">
        <table class="table table-bordered default footable-loaded footable" id="combo-table" data-resizable-columns-id="users-table">
                @if(isset($resourceId))
                    <input type="hidden" class="resource_id" name="resource_id" value="{{ $resourceId }}"/>
                @endif
                <thead>
                    <tr>
                        <th class="RowRemove" data-resizable-column-id="unit" style="width: 2%;"></th>
                        <th class="RowResource" data-resizable-column-id="resource">@lang('modules.resources.resource')</th>
                        <th class="RowRate" data-resizable-column-id="rate">@lang('modules.resources.rate')</th>
                        <th class="RowUnitx" data-resizable-column-id="unitx">@lang('modules.resources.unit')</th>
                        <th class="RowFormula" data-resizable-column-id="formula">@lang('modules.resources.formula')</th>
                        <th class="RowResult" data-resizable-column-id="unit">@lang('modules.resources.result')</th>
                        <th class="RowRemark" data-resizable-column-id="unit">@lang('modules.resources.remark')</th>
                    </tr>
                </thead>
                    <tbody id="comboloop">

                    </tbody>
        </table>
    </div>
</form>
<script>
    "use strict";
    $(document).on("change",".resourceList",function () {
        var val =$(this).val();
        var row = $(this).attr('data-row');
        var  list = $(this).attr('list');
        var options = $('#' + list + ' option[value="'+val+'"]');
        $('.resourceList-hidden[data-row='+row+']').val(val);
        if (options.length > 0) {
            $(this).val(options[0].innerText);
        }
    });
    /* document.querySelector('.resourceList').addEventListener('input', function(e) {
        var input = e.target,
            row = input.getAttribute('data-row'),
            list = input.getAttribute('list'),
            options = document.querySelectorAll('#' + list + ' option[value="'+input.value+'"]'),
            hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
        if (options.length > 0) {
            hiddenInput.value = input.value;
            input.value = options[0].innerText;
        }
    });*/
    function calcTotal(){
        var total = 0;
        $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
            var inp = $(this).val();
            if(inp){
                total = total + parseFloat(inp);
            }
        });
        var resource = $(".resource_id").val();
        $('.cs table tr[data-id="'+resource+'"] td[data-col=5] input.cell-inp[data-col=5]').val(total.toFixed(2));
        $('.formula-sheet tr.final_result input.cell-inp[data-col=5]').val(total.toFixed(2));
    }
    loadTable();
    function loadTable() {
        var url = "{{ route('admin.resources.comboloop') }}";
        var token = "{{ csrf_token() }}";
        var resourceid = "{{ $resourceId }}";
        $.ajax({
            method: 'POST',
            url: url,
            data: {'_token': token,'resourceid': resourceid},
            beforeSend:function () {
                $(".preloader-small").show();
            },
            success: function (response) {

                $(".preloader-small").hide();
                $("#comboloop").html("");
                $("#comboloop").html(response);
                calcTotal();
            }
        });
    }
    $(document).on('change', '.formula-sheet .cell-inp[data-col=1]', function (e) {
        var inp = $(this);
        var row = inp.attr('data-row');
        var id = $(".resourceList-hidden[data-row="+row+"]").val();
        var resource = $(".resource_id").val();
        if(id != ''){
            $.ajax({
                url: '{{ route('admin.resources.getResource') }}',
                type: 'POST',
                data: {_token:'{{ csrf_token() }}', id: id, resource: resource},
                success: function(rData){
                    /*var rData = JSON.parse(data);*/
                    loadTable();
                }
            });
            e.stopImmediatePropagation();
            return false;
        }else{
            $('.formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
            $('.formula-sheet .cell-inp[data-row='+row+'][data-col=3]').val('');
        }
    });
    $(document).on('change', '.formula-sheet tr.resource_list input.cell-inp[data-col=4],.formula-sheet tr.resource_list input.cell-inp[data-col=6]', function () {
        var inp = $(this);
        if(inp.val()){
        var row = inp.data('row');
        var resource = $(".resource_id").val();
        var comboid = $('.formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id');
        var comboresource = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=1]').prop('disabled', false).val();
        var rate = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=2]').prop('disabled', false).val();
        var unit = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=3]').prop('disabled', false).val();
        var remark = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=6]').prop('disabled', false).val();
        var applied = $('.formula-sheet tr.final_result .cell-inp[data-col=4]').val();
        $.ajax({
            url: '{{ route('admin.resources.calculate') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}', str: inp.val(), comboid: comboid, unit: unit, rate: rate,resource: resource, comboresource: comboresource, applied: applied, remark: remark},
            success: function (data) {
                var rData = JSON.parse(data);
                if (rData.status == 'done') {
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=3]').prop('disabled', true)
                    $('.formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id',rData.comboid);
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=5]').val(rData.result);
                    calcTotal();
                } else {
                    alert(rData.message);
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=5]').val('#error');
                }
            }
        });
        }
    });
    $(document).on('change', '.formula-sheet tr.final_result input.cell-inp[data-col=4]', function () {
        var app = $(this).val();
        if(app != '') {
            $.ajax({
                url: '{{ route('admin.resources.validate') }}',
                type: 'POST',
                data: {_token:'{{ csrf_token() }}', str: app},
                success: function(data){
                    var rData = JSON.parse(data);
                    if(rData.status == 'done') {
                        $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
                            $(this).trigger('change');
                        });
                    }else{
                        alert('Applied factor field has some error!');
                        $('.formula-sheet tr.final_result input.cell-inp[data-col=5]').val('#error');
                    }
                }
            });

        }else{
            $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
                $(this).trigger('change');
            });
        }
    });
    $(document).on('focus', '.formula-sheet tr.resource_list input.cell-inp[data-col=4]', function () {
        var app = $(this).data('row');
        $(".selectfunction").data('row',app);

      /*  if(app != '') {
            $(".selectfunction").data('row',app);
            $(".selectfunction").removeAttr('disabled');
        }else{
            $(".selectfunction").data('row','');
            $(".selectfunction").attr('disabled','disabled');
        }*/
    });
    $(document).on('change','.selectfunction',function (e) {
         var datarow = $(this).data('row');
         var val = $(this).val();
         var forrow = $('.formula-sheet tr.resource_list input[name="formula[]"][data-row='+datarow+']');
        var forrowval = forrow.val();
            var newtext = forrowval+''+val;
        forrow.val(newtext);
    });
    $('#combo-sheet-submit').click(function (e) {
        $('#combo-sheet-form .cell-inp').removeAttr('disabled');
        e.preventDefault();
        $.ajax({
            url: '{{ route('admin.resources.combosheetstore') }}',
            container: '#combo-sheet-form',
            type: "POST",
            data: $('#combo-sheet-form').serialize(),
            beforeSend: function(){
                var html = '<div class="loaderx">' +
                    '                        <div class="cssload-speeding-wheel"></div>' +
                    '                    </div>';
                $('.formula-sheet').html(html).show();
            },
            success: function (rData) {
                var response = JSON.parse(rData);
                if(response.status == 'success'){
                    $('.formula-sheet').html("").hide();
                    $('.rate-formula-sheet').show();
                }
            }
        })
    });
    $(document).on('click', '.removecombosheet[data-col=0]', function (e) {
        var row = $(this).attr('data-row');
        var comboid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: '{{ route('admin.resources.removecombosheet') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}',comboid: comboid },
            success: function (data) {
                var rData = JSON.parse(data);
                if (rData.status == 'success') {
                    $('tr.resource_list[data-row=' + row + ']').remove();
                    calcTotal();
                    $(this).html("");
                    $('tr.resourcelist .cell-inp[data-row=' + row + '][data-col=4]').val(rData.result);
                } else {
                    alert(rData.message);
                    $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=4]').val('#error');
                }
            }
        });
        e.stopImmediatePropagation();
        return false;
    });
</script>
@endsection

@push('footer-script')

@endpush