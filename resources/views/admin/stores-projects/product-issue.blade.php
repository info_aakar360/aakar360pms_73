@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('admin.stores-projects.show_project_menu')
        </div>
    </div>
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form action="" id="filter-form">
            <div class="col-md-12">
                <h5 >@lang('app.selectDateRange')</h5>
                <div class="input-daterange input-group" id="date-range">
                    <input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')" value=""/>
                    <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                    <input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')" value=""/>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <h5 >Select Issued By</h5>
                    <select class="select2 form-control" data-placeholder="Issued By" id="issued_by">
                        <option value="">Select Issued By</option>
                        @foreach($issuedBy as $employee)
                            <option value="{{ $employee->issued_by }}">{{ get_user_name($employee->issued_by) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group p-t-10">
                    <label class="control-label col-xs-12">&nbsp;</label>
                    <button type="button" id="apply-filters" class="btn btn-success col-md-6" onclick="applyFilter();"><i class="fa fa-check"></i> @lang('app.apply')</button>
                    <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="col-lg-12">
                    <div class="form-group">
                        <h4>Issued Products
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11">
                        <div class="form-group">
                            <a href="{{ route('admin.stores.projects.createIssuedProduct',[$projectId,$store->id]) }}" class="btn btn-outline btn-success btn-sm">Issue Product<i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="col-sm-1 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <table class="table-responsive table-responsive" id="example">
                    <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>Product Issue Id</th>
                            <th>Total Quantity</th>
                            <th>Issued By</th>
                            <th>Issued Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="filterData">
                        @foreach($issuedProducts as $key=>$product)
                            <?php $qcount = \App\ProductIssue::where('unique_id',$product->unique_id)->get(); ?>
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $product->unique_id }}</td>
                                <td>{{ count($qcount) }}</td>
                                <td>{{ get_user_name($product->issued_by) }}</td>
                                <td>{{ Carbon\Carbon::parse($product->created_at)->format('d-m-Y') }}</td>
                                <td>
                                    <a href="{{ route('admin.stores.projects.issuedProductDetail',[$projectId, $product->unique_id])}}" class="btn btn-success btn-circle"
                                       data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="View Issued Products">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('admin.stores.projects.returnIssuedProduct',[$projectId, $sid, $product->unique_id])}}" class="btn btn-warning btn-circle"
                                       data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="Return Issued Products">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!-- .row -->


@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.stores.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function applyFilter() {
            var token = "{{ csrf_token() }}";
            var startDate = $('#start-date').val();
            var endDate = $('#end-date').val();
            var issuedBy = $('#issued_by').val();
            var issuedTo = $('#issued_to').val();
            $.ajax({
                type: "POST",
                url: "{{ route('admin.stores.projects.applyIssuedFilter', [$projectId, $sid]) }}",
                data: {'_token': token, 'start_date': startDate, 'end_date': endDate, 'issued_by': issuedBy},
                success: function(data){
                    $("#filterData").html(data);
                }
            });
        }

        $('ul.productIssue').addClass('tab-current');

        $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );
        $("#Pi").addClass("active");

        jQuery('#date-range').datepicker({
            toggleActive: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });
    </script>

@endpush