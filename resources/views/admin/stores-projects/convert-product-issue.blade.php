@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('admin.stores-projects.show_project_menu')
        </div>
    </div>

        <div class="row">

            <div class="col-md-12">
                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                {{ csrf_field() }}
                <div class="white-box">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <h4>Issue Products
                            </h4>
                        </div>
                    </div>
                    <input type="hidden" name="project_id" value="{{ $projectId }}">
                    <input type="hidden" name="store_id" value="{{ $sid }}">
                    <input type="hidden" name="indent_no" value="{{ $ino }}">
                    <div class="row" id="indentData">
                        <table class="table">
                            <thead>
                                <th>S.No.</th>
                                <th>Product</th>
                                <th>Brand</th>
                                <th>Unit</th>
                                <th>Required Quantity</th>
                                <th>Required Date</th>
                                <th>Issued Quantity</th>
                                <th>To Be Issue Quantity</th>
                            </thead>
                            <tbody>
                            @foreach($tmpData as $key=>$data)
                                @php $iQty = get_issued_qty($podata->project_id,$podata->store_id,$data->cid); @endphp
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ get_local_product_name($data->cid) }}<input type="hidden" name="product_id[]" value="{{ $data->cid }}"> </td>
                                    <td>{{ get_pbrand_name($data->bid) }}<input type="hidden" name="brand[]" value="{{ $data->bid }}"></td>
                                    <td>{{ get_unit_name($data->unit) }}<input type="hidden" name="unit_id[]" value="{{ $data->unit }}"></td>
                                    <td>{{$data->quantity}}<input type="hidden" name="recQty[]" value="{{ $data->quantity }}"></td>
                                    <td>{{ $data->expected_date }}</td>
                                    <td>{{ $iQty }}<input type="hidden" name="issuedQty[]" value="{{ $iQty }}"></td>
                                    <td>
                                        <input type="number" min="1" name="quantity[]" class="form-control" {{ ($data->quantity == $iQty) ? 'disabled' : '' }} value="0">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 m-t-5">
                            <button type="button" id="storeProduct" class="btn btn-success" style="float: right;"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    <!-- .row -->


@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        var r = 1;
        $('#add-item').click(function () {
            r++;
            var item = '<div class="col-xs-12 item-row margin-top-5">'
                +'<div class="input-group col-md-3" style="float: left; padding-right: 5px;">'
                +'<select class="select2 form-control product" name="product_id['+r+']" data-style="form-control">'
                +'<option value="">Select Product</option>';
            @foreach($products as $category)
                item += '<option value="{{ $category->id }}">{{ $category->name }}</option>';
            @endforeach
                item += '</select>'
                +'</div>'
                +'<div class="input-group col-md-2" style="float: left; padding-right: 5px;">'
                +'<select class="select2 form-control product" name="unit_id['+r+']" data-style="form-control">'
                +'<option value="">Select Unit</option>';
            @foreach($units as $category)
                item += '<option value="{{ $category->id }}">{{ $category->name }}</option>';
            @endforeach
                item += '</select>'
                +'</div>'
                +'<div class="input-group col-md-4 dataDetails"></div> <input type="hidden" name="serial" value="'+r+'">'
                +'<div class="col-md-1 text-right visible-md visible-lg">'
                +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
                +'<div class="row">'
                +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'</div>'
                +'</div>';
            $(item).hide().appendTo("#sortable").fadeIn(500);
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

        $('#storeProduct').click(function () {
            $.easyAjax({
                url: '{{route('admin.stores.projects.storeIssuedProduct')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        })

        $('ul.showProjectTabs .productIssue').addClass('tab-current');

        $(document).on('change', 'select[name=indent_no]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.stores.projects.getIndentData')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('#indentData').html('');
                    $('#indentData').html(data);
                }
            });

        });

        $(document).on('change', '.product', function(){
            var pid = $(this).val();
            var project = $('input[name=project]').val();
            var store = $('input[name=store]').val()
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.stores.projects.productData')}}',
                type: 'POST',
                data: {_token: token, pid: pid, project_id: project, store_id: store},
                success: function (data) {
                    $('.dataDetails').html(data);
                }
            });

        });
        $("#Pi").addClass("active");
    </script>

@endpush