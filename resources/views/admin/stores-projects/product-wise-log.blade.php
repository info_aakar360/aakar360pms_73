@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <style>
        th {
            top: 0;
            z-index: 2;
            }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('admin.stores-projects.show_project_menu')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="col-lg-12">
                    <div class="form-group">
                        <h4>Products Log / {{ $product->name }}
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <?php
                        $ti = 0;
                        $tr = 0;
                        $tb = 0;
                    foreach ($productLogs as $key=>$log) {
                        if($log->transaction_type == 'plus'){
                            $tr += (int)$log->quantity;
                        }else{
                            $ti += (int)$log->quantity;
                        }
                    }
                    ?>
                    <div class="col-md-3">
                        <div class="white-box bg-danger">
                            <h3 class="box-title text-white">Total Issued</h3>
                            <ul class="list-inline two-part">
                                <li><i class="fa fa-arrow-up text-white"></i></li>
                                <li class="text-right"><span id="totalIssued" class="counter text-white">{{ $ti }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="white-box bg-success">
                            <h3 class="box-title text-white">Total Received</h3>
                            <ul class="list-inline two-part">
                                <li><i class="fa fa-arrow-down text-white"></i></li>
                                <li class="text-right"><span id="totalReceived" class="counter text-white">{{ $tr }}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="white-box bg-primary">
                            <h3 class="box-title text-white">Total Balance</h3>
                            <ul class="list-inline two-part">
                                <li><i class="fa fa-balance-scale text-white"></i></li>
                                <li class="text-right"><span id="totalBalance" class="counter text-white">{{ (!empty($stock)) ? $stock : 0 }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div style="height: 45vh; max-height: 45vh; overflow-y: auto;">
                    <table class="table-responsive table-responsive table-bordered">
                        <thead>
                            <tr>
                                <th>@lang('app.sno')</th>
                                <th>Date</th>
                                <th>Detail</th>
                                <th>Issue</th>
                                <th>Receive</th>
                                <th>Running Balance</th>
                            </tr>
                        </thead>
                        <tbody id="loadProData">
                            <?php
                            foreach ($productLogs as $key=>$log) {
                                $k = $key+1;
                                $plusQ = '-';
                                $minusQ = '-';
                                $recieving = 0;
                                $issue = 0;
                                if($log->transaction_type == 'plus'){
                                    $plusQ = '<div class="bg-success" style="text-align: center; color: white;">'.$log->quantity.'</div>';
                                    $recieving = $log->quantity;
                                }
                                if($log->transaction_type == 'minus'){
                                    $minusQ = '<div class="bg-danger" style="text-align: center; color: white;">'.$log->quantity.'</div>';
                                    $issue = $log->quantity;

                                }
                                if($key == 0){
                                    $runningbalance  = (int)$recieving - (int)$issue;
                                }else{
                                    $runningbalance  = ((int)$runningbalance + (int)$recieving) - (int)$issue;
                                }
                                echo '<tr>
                                    <td>'.$k.'</td>
                                    <td>'.Carbon\Carbon::parse($log->created_at)->format('d-m-Y').'</td>
                                    <td>';
                                        if($log->module_name == 'update-stock'){
                                            if($log->transaction_type == 'minus') {
                                                echo 'Shortage';
                                            }else {
                                                echo 'Access';
                                            }
                                        }else {
                                            if ($log->transaction_type == 'minus') {
                                                echo 'Issued';
                                            } else {
                                                echo 'Received';
                                            }
                                        }
                                        echo ' By '.get_user_name($log->created_by).'<br>';
                                        echo (!empty($log->remark)) ? 'Remark : '.$log->remark : '';
                                        echo '</td>
                                    <td>'.$minusQ.'</td>
                                    <td>'.$plusQ.'</td>
                                    <td>'.$runningbalance.'</td>
                                </tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <table>
                        <tr>
                            <td colspan="6" style="text-align: right;">
                                Closing Balance
                            </td>
                            <td>{{ (!empty($stock)) ? $stock : 0 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- .row -->


@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.stores.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function loadProData(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: '{{route('admin.stores.projects.productLogData', [$project->id, $store->id])}}',
                container: '#createRfq',
                type: "POST",
                data: {'_token': token, 'product_id': val},
                success: function(data){
                    $("#loadProData").html(data.html);
                    $("#totalIssued").html(data.issued);
                    $("#totalReceived").html(data.received);
                    $("#totalBalance").html(data.balance);
                }
            })
        }

        $('ul.productIssue').addClass('tab-current');

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        });

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })
        $("#ProductLog").addClass("active");
    </script>

@endpush