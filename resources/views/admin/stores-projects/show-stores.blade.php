@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }} #{{ $project->id }} - <span class="font-bold">{{ ucwords($project->project_name) }}</span></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.module.stores')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/icheck/skins/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <style>
        .wrimagecard{
            margin-top: 0;
            margin-bottom: 1.5rem;
            text-align: left;
            position: relative;
            background: #002f76;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }
        .wrimagecard .fa{
            position: relative;
            font-size: 70px;
        }
        .wrimagecard-topimage_header{
            padding: 0px;
            background-color:white;
        }
        a.wrimagecard:hover, .wrimagecard-topimage:hover {
            box-shadow: 2px 4px 8px 0px rgba(46,61,73,0.2);
        }
        .wrimagecard-topimage a {
            width: 100%;
            height: 100%;
            display: block;
        }
        .wrimagecard-topimage_title {
            padding: 20px 24px;
            height: 65px;
            padding-bottom: 0.75rem;
            position: relative;
            text-align: center;
        }
        .wrimagecard-topimage a {
            border-bottom: none;
            text-decoration: none;
            color: #525c65;
            transition: color 0.3s ease;
        }

        .card-base > .card-icon {
            text-align: center;
            position: relative;
        }

        .imagecard {
            z-index: 2;
            display: block;
            positioN: relative;
            width: 88px;
            height: 88px;
            border-radius: 50%;
            border: 5px solid white;
            box-shadow: 1px 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            margin: 0 auto;
            color: white;
        }
        .card-base > .card-icon > .card-data {
            min-height: 50px !important;
            margin-top: -24px;
            background: ghostwhite;
            border: 1px solid #e0e0e0;
            padding: 15px 0 10px 0;
            box-shadow: 1px 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            min-height: 215px;
        }
        #widgetCardIcon {
            background: #bb7824 !important;
            font-size: 28px;
            line-height: 78px;
        }
        .card-image{
            height: 200px;
            width: 100%;
            margin-top: -20px;
            margin-bottom: -20px;
        }
        .mr-right{
            margin-right: 40px;
            padding-top: 15px;
        }
        .titleText{
            font-size: 15px !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="content-wrap">
                        <section id="section-line-1" class="show">
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($stores as $store)
                                        <div class="col-md-2 col-sm-3 mr-right">
                                            <div class="wrimagecard wrimagecard-topimage">
                                                <a href="{{ route('admin.stores.projects.showStoresProducts', [$store->project_id, $store->id]) }}">
                                                    <div class="wrimagecard-topimage_header">
                                                        <center><img src="{{ asset('js/warehouse.png') }}" class="card-image"></center>
                                                    </div>
                                                    <div class="wrimagecard-topimage_title">
                                                        <h4 class="text-center titleText" style="color: #fff;">{{ substr($store->company_name, 0, 13) . '...' }}</h4>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                    {{--<div class="white-box">--}}
                                        {{--@if($user->can('add_stores'))--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-sm-6">--}}
                                                {{--<div class="form-group">--}}
                                                    {{--<a href="{{ route('admin.stores.createNew', [$project->id]) }}" class="btn btn-outline btn-success btn-sm">@lang('modules.stores.addNewSupplier') <i class="fa fa-plus" aria-hidden="true"></i></a>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@endif--}}
                                        {{--<div class="table-responsive">--}}
                                            {{--<table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">--}}
                                                {{--<thead>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>@lang('app.id')</th>--}}
                                                        {{--<th>@lang('modules.stores.companyName')</th>--}}
                                                        {{--<th>@lang('app.name')</th>--}}
                                                        {{--<th>@lang('app.email')</th>--}}
                                                        {{--<th>@lang('app.mobile')</th>--}}
                                                        {{--<th>@lang('app.createdAt')</th>--}}
                                                        {{--<th>@lang('app.action')</th>--}}
                                                    {{--</tr>--}}
                                                {{--</thead>--}}
                                            {{--</table>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </section>
                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $('#timer-list').on('click', '.stop-timer', function () {
       var id = $(this).data('time-id');
        var url = '{{route('admin.time-logs.stopTimer', ':id')}}';
        url = url.replace(':id', id);
        var token = '{{ csrf_token() }}'
        $.easyAjax({
            url: url,
            type: "POST",
            data: {timeId: id, _token: token},
            success: function (data) {
                $('#timer-list').html(data.html);
            }
        })

    });

    var table;
    $(function() {
        loadTable();
        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('user-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.stores.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
    //                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

    });

    function loadTable() {
        var startDate = $('#start-date').val();
        if (startDate == '') {
            startDate = null;
        }
        var endDate = $('#end-date').val();
        if (endDate == '') {
            endDate = null;
        }
        var status = $('#status').val();
        var store = $('#store').val();
        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.stores.data', [$project->id]) !!}?startDate=' + startDate + '&endDate=' + endDate + '&store=' + store + '&status=' + status,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function (oSettings) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                {data: 'company_name', name: 'company_name'},
                {data: 'contact_person', name: 'contact_person'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'}
            ]
        })
    }

    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle('slide');
    })

    $('#apply-filters').click(function () {
        loadTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        loadTable();
    })

    function exportData(){
        var store = $('#store').val();
        var status = $('#status').val();
        var url = '{{ route('admin.stores.export', [':status', ':store']) }}';
        url = url.replace(':store', store);
        url = url.replace(':status', status);
        window.location.href = url;
    }
    $('ul.showProjectTabs .Stores').addClass('tab-current');
</script>
@endpush
