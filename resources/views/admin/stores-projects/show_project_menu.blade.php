<div class="">
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: white;
        }
    </style>
    @if(!empty($project))
        @php $proId = $project->id @endphp
    @else
        @if(isset($projectId))
            @php $proId = $projectId @endphp
        @else
            @php $proId = 0 @endphp
        @endif
    @endif
    <div class="icon-bar">
        <a class="btn btn-outline btn-blue btn-sm" id="Bom" href="{{ route('admin.stores.projects.showStoresProducts', [$proId, $store->id]) }}"><i class="fa fa-home"></i> BOM</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Stock" href="{{ route('admin.stores.projects.showStoresStock', [$proId, $store->id]) }}"><i class="fa fa-search"></i> @lang('app.menu.stock')</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.stores.projects.indents', [$proId, $store->id]) }}"><i class="fa fa-envelope"></i> @lang('modules.module.indent')</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.stores.projects.productIssue', [$proId, $store->id]) }}"><i class="fa fa-globe"></i> Issue</a>
        {{--<a class="btn btn-outline btn-blue btn-sm" id="ProductLog" href="{{ route('admin.stores.projects.productLog', [$proId, $store->id]) }}"><i class="fa fa-book"></i> Product Log</a>--}}
    </div>
</div>