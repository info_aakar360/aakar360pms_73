@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createSegment" class="btn btn-outline btn-success btn-sm">Add Segment <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.projects')</th>
                            <?php  if(in_array('sub_projects', $user->modules)) {?>
                            <th>@lang('app.sub_projects')</th>
                            <?php }?>
                            <th>@lang('app.boq')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($segments as $key=>$segment) {

                            if(count($segments)!=0) {
                            ?>
                               <tr id="tablerow{{ $segment->id }}">
                                <td>{{ $key+1 }}</td>
                                   <td>{{ $segment->name }}</td>
                                   <td>{{ get_project_name($segment->projectid) }}</td>
                                   <?php  if(in_array('sub_projects', $user->modules)) {?>
                                   <td>{{ get_subproject($segment->titleid) }}</td>
                                   <?php }?>
                                   <td>
                                       <a type="button" class="btn btn-default editSegment" data-segment-id="{{ $segment->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-segment-id="{{ $segment->id }}" class="btn btn-sm btn-danger btn-circle sa-params"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                   </td>
                        </tr>
                        <?php
                           }else{
                               ?>
                           <tr>
                               <td colspan="3">No Segments Found</td>
                           </tr>

                            <?php
                                } }
                               ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <!-- Start Modal add sub location location -->
    <div class="modal" id="createSegment">
        <div class="modal-dialog">
            <div class="modal-content">
            {!! Form::open(['id'=>'createSegmentForm','class'=>'ajax-form','method'=>'POST']) !!}
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Segment</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <div class="form-group">
                        <label class="control-label">@lang('app.name')</label>
                        <input type="text" class="form-control" name="name" required placeholder="Enter Name"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.project')</label>
                        <select class="select2 form-control" data-placeholder="@lang("app.selectProject")" required  id="projectid" name="projectid">
                            <option value="">Select Project</option>
                            @foreach($projectarray as $project)
                                <option  value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if(in_array('sub_projects',$user->modules))
                        <div class="form-group">
                            <label class="control-label">@lang('app.subproject')</label>
                            <select class="select2 form-control" data-placeholder="@lang("app.subproject")"  required id="titlelist" name="subprojectid">
                                <option value="">Select @lang('app.subproject')</option>

                            </select>
                        </div>
                    @endif
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="savebutton">SUBMIT</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    $('#savebutton').click(function (e) {
        console.log('fsdfd');
        e.preventDefault();
        $.easyAjax({
            url: '{{ route('admin.segments.store') }}',
            container: '#createSegmentForm',
            type: "POST",
            data: $('#createSegmentForm').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#createSegmentForm')[0].reset();
                    window.location.reload();
                }
            }
        });
    });

    $('.editSegment').click(function(){
        var id = $(this).data('segment-id');
        var url = '{{ route('admin.segments.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Segment");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('segment-id');
        var recurring = $(this).data('recurring');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted Segment item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm') {

                var url = "{{ route('admin.segments.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        $("#tablerow"+id).remove();
                    }
                });
            }
        });
    });
    $('#projectid').change(function () {
        var id = $(this).val();
        var token = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ route('admin.projects.projecttitles') }}",
            data: {'_token': token,'projectid': id},
            success: function(data){
                var projectdata = JSON.parse(data);
                var titles = '<option value="">Select Sub Project</option>';
                if(projectdata.titles){
                    $(".projecttitle").removeClass("hide");
                    $.each( projectdata.titles, function( key, value ) {
                        titles += '<option value="'+key+'">'+value+'</option>';
                    });
                }
                $("select#titlelist").html("");
                $("select#titlelist").html(titles);
                $('select#titlelist').select2();
            }
        });
    });
</script>
@endpush