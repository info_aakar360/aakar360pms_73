<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Category</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editSegment','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $segment->name }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.project')</label>
                        <select class="select2 form-control" data-placeholder="@lang("app.selectProject")" required  id="projectid2" name="projectid">
                            <option value="">Select Project</option>
                            @foreach($projectarray as $project)
                                <option  value="{{ $project->id }}" <?php if($segment->projectid==$project->id){ echo 'selected';}?>>{{ ucwords($project->project_name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if(in_array('sub_projects',$user->modules))
                        <div class="form-group">
                            <label class="control-label">@lang('app.subproject')</label>
                            <select class="select2 form-control" data-placeholder="@lang("app.subproject")"  required id="titlelist2" name="subprojectid">
                                <option value="">Select @lang('app.subproject')</option>
                                @if($titlesarray)
                                @foreach($titlesarray as $title)
                                    <option  value="{{ $title->id }}" <?php if($segment->titleid==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                @endforeach
                                    @endif
                            </select>
                        </div>
                   @endif

                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-type" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-type').click(function () {
        $.easyAjax({
            url: '{{route('admin.segments.update', [$segment->id])}}',
            container: '#editSegment',
            type: "PATCH",
            data: $('#editSegment').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
    $('#projectid2').change(function () {
        var id = $(this).val();
        var token = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ route('admin.projects.projecttitles') }}",
            data: {'_token': token,'projectid': id},
            success: function(data){
                var projectdata = JSON.parse(data);
                var titles = '<option value="">Select Sub projects</option>';
                if(projectdata.titles){
                    $(".projecttitle").removeClass("hide");
                    $.each( projectdata.titles, function( key, value ) {
                        titles += '<option value="'+key+'">'+value+'</option>';
                    });
                }
                $("select#titlelist2").html("");
                $("select#titlelist2").html(titles);
                $('select#titlelist2').select2();
            }
        });
    });
</script>