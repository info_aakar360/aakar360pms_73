@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<style>
    .swal-footer {
        text-align: center !important;
    }
</style>
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title m-t-30">@lang('app.date')</h5>
                <div class="input-daterange" id="date-range">
                    <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.date')"   />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title">  @lang('app.users') </h5>
                <select class="select2 form-control" name="users" data-placeholder="@lang('app.selectProject')" id="selectuser">
                    <option value=""> @lang('app.users')</option>
                    @foreach($employees as $employe)
                        <option value="{{ $employe->id }}">{{ ucwords($employe->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.status')</label>
                <select class="selectpicker form-control" name="status" data-style="form-control"  id="status">
                    <option value="">Please Status</option>
                    <option value="open" >Open</option>
                    <option value="initiated" >Initiated</option>
                    <option value="completed" >Completed</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <h5 class="box-title m-t-10"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
            <button type="button" class="btn btn-danger" id="reset-filters"><i class="fa fa-check"></i> @lang('app.reset')</button>
        </div>
    </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="row">
                                    <div class="col-sm-1 hidden-xs">
                                        <div class="form-group">
                                            <h2 style="color: #002f76"> @lang('RFI')</h2>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 hidden-xs mt-10">
                                        <div class="form-group"><a href="{{ route('admin.rfi.createRfi') }}" class="btn btn-outline btn-success btn-sm">New RFI <i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-sm-1 text-right hidden-xs">
                                    <div class="form-group">
                                        <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                                    </div>
                                </div>
                            </div>

                <div class="table-responsive">
                    <table class="table table-bordered  toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                            <tr class="solid-border">
                                <th>@lang('app.sno')</th>
                                <th>@lang('app.title')</th>
                                <th>@lang('modules.tasks.assignTo')</th>
                                <th>@lang('app.distribution')</th>
                                <th>@lang('app.location')</th>
                                <th>@lang('app.public') @lang('app.or') @lang('app.private')</th>
                                <th>@lang('app.dueDate')</th>
                                <th>@lang('app.refrence')</th>
                                <th>@lang('app.status')</th>
                                <th>@lang('app.added_by')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                        </thead>

                    </table>
                </div>

            </div>
          </div>
          </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Sub Task e</span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

    loadTable();
    function loadTable(){

        var user   = $('#selectuser').val();
        var selectdate     = $('#start-date').val();
        var status     = $('#status').val();

        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.rfi.rfidata') !!}?user=' + user+ '&selectdate=' + selectdate+ '&status=' + status,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'title', name: 'title' },
                { data: 'assign_to', name: 'assign_to'},
                { data: 'distribution', name: 'distribution'},
                { data: 'location', name: 'location'},
                { data: 'private', name: 'private'},
                { data: 'due_date', name: 'due_date'},
                { data: 'reference', name: 'reference'},
                { data: 'status', name: 'status'},
                { data: 'added_by', name: 'added_by'},
                { data: 'action', name: 'action', width: '15%' }
            ]
        });
    }
    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });
    $('#filter-results').click(function () {
        loadTable();
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });

    $('#reset-filters').click(function () {
        $('#selectuser').val("");
        $('#start-date').val("");
        $('#status').val("");
        loadTable();
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });


    jQuery('#date-range').datepicker({
        toggleActive: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });


    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('task-id');
        var recurring = $(this).data('recurring');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted punch item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm' || isConfirm == 'recurring') {

                var url = "{{ route('admin.rfi.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};

                if(isConfirm == 'recurring')
                {
                    dataObject.recurring = 'yes';
                }

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        if (response.status == "success") {
//                            $.unblockUI();
//                            table._fnDraw();
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });

    $('#tasks-table').on('click', '.show-task-detail', function () {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");

        var id = $(this).data('task-id');
        var url = "{{ route('admin.rfi.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    })

    showTable();
    $('#createTaskCategory').click(function(){
        var url = '{{ route('admin.taskCategory.create')}}';
        $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
        $.ajaxModal('#taskCategoryModal',url);
    })

</script>
@endpush