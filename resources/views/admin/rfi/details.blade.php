@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.rfi.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
    .awaans{
        background-color: #848082;
        color: #fff;
        font-weight: bold;
    }
    .awaanstext{
        color: #fff;
        font-weight: bold;
    }

</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                 <div class="row">
                     <div class="col-md-10">RFI Details</div>
                 <div class="col-md-2">   <?php
                     if ( $rfi->rfi_manager == $user->id){ ?>
                     <a href="{{ route('admin.rfi.editRfi', $rfi->id)}}" class="btn1 btn-info btn-circle edit-btn"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     <?php } ?>
                 </div>
                 </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="col-md-12">
                    <div class="col-md-5">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#menu1">@lang('app.details')</a></li>
                            <li><a data-toggle="pill" href="#home">@lang('app.comment')</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h2><b>{{ ucwords($rfi->title) }}</b></h2>
                    </div>
                    </div>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade">
                                {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @if(count($responsearray)>0)
                                                <h3>Official Responses</h3>
                                                @php $x=1; @endphp
                                                @foreach($responsearray as $reply)
                                                    <div class="col-md-11 timelineblock <?php if($user->id==$reply->added_by){ echo 'float-right';}?>">
                                                        <p class="awaanstextt">Response {{ $x }}</p>
                                                        <div class="col-md-1">
                                                            <img class="img-circle w-100"  src="{{ get_users_image_link($reply->added_by) }}" />
                                                        </div>
                                                        <div class="col-md-11">
                                                            <div class="taskusername">{{ get_user_name($reply->added_by) }} </div>
                                                            <div class="tasksdate">{{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}</div>
                                                            <div class="commenttext">
                                                                <div class="row">
                                                                    <div class="col-md-12 form-group">
                                                                        {!! $reply->comment !!}
                                                                    </div>
                                                                </div>
                                                                <?php $replyfiles =  \App\RfiFile::where('rfi_id',$rfi->id)->where('reply_id',$reply->id)->where('rfitype','reply')->get();?>
                                                                @if(count($replyfiles)>0)
                                                                        @foreach($replyfiles as $file)
                                                                        <div class="col-md-3">
                                                                                @if($file->external_link != '')
                                                                                    <?php $imgurl = $file->external_link;?>
                                                                                @elseif($storage == 'local')
                                                                                    <?php $imgurl = uploads_url().'rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                                                @elseif($storage == 's3')
                                                                                    <?php $imgurl = awsurl().'/rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                                                @elseif($storage == 'google')
                                                                                    <?php $imgurl = $file->google_url;?>
                                                                                @elseif($storage == 'dropbox')
                                                                                    <?php $imgurl = $file->dropbox_link;?>
                                                                                @endif
                                                                                {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                                                <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                                            </div>
                                                                        @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @php $x++; @endphp
                                                @endforeach
                                            @else
                                                <p class="awaanstextt">Awaiting for Official response</p>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Reply</label>
                                                <textarea id="description" name="comment" required class="form-control summernote"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Files</label>
                                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                <div id="file-upload-box" >
                                                    <div class="row" id="file-dropzone">
                                                        <div class="col-md-12">
                                                            <div class="dropzone" id="file-upload-dropzone">
                                                                {{ csrf_field() }}
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple/>
                                                                </div>
                                                                <input name="image_url" id="image_url"type="hidden" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="rfiID" id="rfiID">
                                                <input type="hidden" name="replyID" id="replyID">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-actions">
                                                <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <h2>All Replies</h2>
                                                @foreach($replies as $reply)
                                                    <div class="col-md-11 timelineblock <?php if($user->id==$reply->added_by){ echo 'float-right';}?>">
                                                        <div class="col-md-1">
                                                            <img class="img-circle w-100"  src="{{ get_users_image_link($reply->added_by) }}" />
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="taskusername">{{ get_user_name($reply->added_by) }} </div>
                                                            <div class="tasksdate">{{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}</div>
                                                            <div class="commenttext">
                                                                <div class="row">
                                                                    <div class="col-md-12 form-group">
                                                                        {!! $reply->comment !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="checkbox" <?php if(!empty($reply->officialresponse)&&$reply->officialresponse=='1'){ echo 'checked';}?> onclick="officialresponse({{ $reply->id }})"  value="1" /> <i>Official response</i>
                                                        </div>
                                                        <div class="col-md-10 float-right">
                                                            <?php $replyfiles =  \App\RfiFile::where('rfi_id',$rfi->id)->where('reply_id',$reply->id)->where('rfitype','reply')->get();?>
                                                                @if(count($replyfiles)>0)
                                                                    @foreach($replyfiles as $file)
                                                                        <div class="col-md-3">
                                                                            @if($file->external_link != '')
                                                                                <?php $imgurl = $file->external_link;?>
                                                                            @elseif($storage == 'local')
                                                                                <?php $imgurl = uploads_url().'rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                                            @elseif($storage == 's3')
                                                                                <?php $imgurl = awsurl().'/rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                                            @elseif($storage == 'google')
                                                                                <?php $imgurl = $file->google_url;?>
                                                                            @elseif($storage == 'dropbox')
                                                                                <?php $imgurl = $file->dropbox_link;?>
                                                                            @endif
                                                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div id="menu1" class="tab-pane fade in active">
                                <div class="form-body">
                                    <div class="row">
                                    <div class="col-md-12">
                                        <table class="table-striped" style="width: 100%;">
                                            <tr>
                                                <td style="width: 15%;"><b>@lang('app.title')</b></td>
                                                <td  style="width: 35%;">{{ ucwords($rfi->title) }}</td>
                                                <td style="width: 15%;"><b>Subject</b></td>
                                                <td  style="width: 35%;">{{ $rfi->subject }}</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Draft</b></td>
                                                <td  style="width: 35%;">@if($rfi->draft=='1') <i class="fa fa-check-circle"></i> @else  <i class="fa fa-times"></i> @endif  </td>
                                                <td style="width: 15%;"><b>Public or Private</b></td>
                                                <td  style="width: 35%;"><?php if($rfi->private == '1'){ echo 'Private';}else{ echo 'Public'; }?> </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>RFI Manager</b></td>
                                                <td  style="width: 35%;">
                                                    {{ ucwords(get_users_employee_name($rfi->rfi_manager,$rfi->company_id)) }}
                                                </td>
                                                <td style="width: 15%;"><b>Assigned To</b></td>
                                                <td  style="width: 35%;">
                                                    {{ ucwords(get_users_employee_name($rfi->assign_to,$rfi->company_id)) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Due Date</b></td>
                                                <td  style="width: 35%;">
                                                    <?php
                                                    if($rfi->due_date){
                                                    $exd = explode('-',$rfi->due_date);
                                                    ?>
                                                    {{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}
                                                    <?php } ?>
                                                </td>
                                                <td style="width: 15%;"><b>Responsible Contractor</b></td>
                                                <td  style="width: 35%;">
                                                    {{ ucwords(get_users_contractor_name($rfi->rec_contractor,$rfi->company_id)) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Received From</b></td>
                                                <td  style="width: 35%;">
                                                    {{ ucwords(get_users_employee_name($rfi->received_from,$rfi->company_id)) }}
                                                </td>
                                                <td style="width: 15%;"><b>Distribution List</b></td>
                                                <td  style="width: 35%;">
                                                    {{ ucwords(get_users_employee_name($rfi->distribution,$rfi->company_id)) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Location</b></td>
                                                <td  style="width: 35%;">{{ $rfi->location }}</td>
                                                <td style="width: 15%;"><b>Drawing No.</b></td>
                                                <td  style="width: 35%;">{{ $rfi->drawing_no }}</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Schedule Impact</b></td>
                                                <td  style="width: 35%;">{{ strtoupper($rfi->schimpact) }}</td>
                                                <td style="width: 15%;"><b>Schedule Impact days</b></td>
                                                <td  style="width: 35%;">{{ $rfi->schimpact_days }}</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Cost Impact</b></td>
                                                <td  style="width: 35%;">{{ strtoupper($rfi->costimpact) }}</td>
                                                <td style="width: 15%;"><b>Cost Impact days</b></td>
                                                <td  style="width: 35%;">{{ $rfi->costimpact_days }}</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Status</b></td>
                                                <td  style="width: 35%;">
                                                    <?php if($rfi->status == 'Open'){ echo 'Open'; } ?>
                                                    <?php if($rfi->status == 'Closed'){ echo 'Closed'; } ?>
                                                </td>
                                                <td style="width: 15%;"><b>Added By</b></td>
                                                <td  style="width: 35%;">{{ ucwords(get_user_name($rfi->added_by)) }} </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Project</b></td>
                                                <td  style="width: 35%;">{{ get_project_name($rfi->projectid) }}</td>
                                                <td style="width: 15%;"><b>Activity </b></td>
                                                <td  style="width: 35%;">{{ get_activity($rfi->activityid) }}</td>
                                            </tr>
                                            <tr>
                                                <?php  if(in_array('sub_projects', $user->modules)){?>
                                                <td style="width: 15%;"><b>Sub Project </b></td>
                                                <td  style="width: 35%;">{{ get_title($rfi->titleid) }}</td>
                                                <?php }?>
                                                <?php  if(in_array('segment', $user->modules)){?>
                                                <td style="width: 15%;"><b>Segment</b></td>
                                                <td  style="width: 35%;">{{ get_segment($rfi->segmentid) }}</td>
                                                <?php }?>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;"><b>Task</b></td>
                                                <td  style="width: 35%;">{{ get_boq_name($rfi->costitemid) }}</td>
                                                <td style="width: 15%;"><b>Ball in Court</b></td>
                                                <td  style="width: 35%;">{{ ucwords(get_user_name($rfi->ballincourt)) }} </td>
                                            </tr>
                                        </table>
                                    </div>
                                <div class="col-md-12">
                                    <p><b>Question</b></p>
                                    {!! $rfi->question !!}
                                </div>
                                        @if(count($files))
                                            <div class="col-md-12">
                                                <b>Files</b>
                                                <br>
                                                <div class="row" id="list">
                                                    @foreach($files as $file)
                                                        <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                                            @if($file->external_link != '')
                                                                <?php $imgurl = $file->external_link;?>
                                                            @elseif($storage == 'local')
                                                                <?php $imgurl = uploads_url().'rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                            @elseif($storage == 's3')
                                                                <?php $imgurl = awsurl().'/rfi-files/'.$rfi->id.'/'.$file->hashname;?>
                                                            @elseif($storage == 'google')
                                                                <?php $imgurl = $file->google_url;?>
                                                            @elseif($storage == 'dropbox')
                                                                <?php $imgurl = $file->dropbox_link;?>
                                                            @endif
                                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif

                            </div>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>


<script>

function showDiv() {
    $('#replyDiv').toggleClass('hide', 'show');
}
Dropzone.autoDiscover = false;
//Dropzone class
myDropzone = new Dropzone("div#file-upload-dropzone", {
    url: "{{ route('admin.rfi.storeImage') }}",
    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
    paramName: "file",
    maxFilesize: 10,
    maxFiles: 10,
    acceptedFiles: "image/*,application/pdf",
    autoProcessQueue: false,
    uploadMultiple: true,
    addRemoveLinks:true,
    parallelUploads:10,
    init: function () {
        myDropzone = this;
    }
});

myDropzone.on('sending', function(file, xhr, formData) {
    console.log(myDropzone.getAddedFiles().length,'sending');
    var ids = '{{ $rfi->id }}';
    var replyid = $('#replyID').val();
    formData.append('rfi_id', ids);
    formData.append('rfitype', 'reply');
    formData.append('reply_id', replyid);
});

myDropzone.on('completemultiple', function () {
    var msgs = "@lang('RFI Updated successfully')";
    $.showToastr(msgs, 'success');
    window.location.href = '{{ route('admin.rfi.index') }}'
});

    //    update task
    $('#update-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.rfi.replyPost', [$rfi->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function (data) {
                if(data.error){
                    var msgs = data.error;
                    $.showToastr(msgs, 'error');
                }else{
                $('#storeTask').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    rifID = data.rifID;
                    $('#rifID').val(data.rifID);
                    $('#replyID').val(data.replyID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('Reply posted successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.rfi.index') }}'
                }

                }
            }
        })
    });

    //    update task
    function removeFile(id) {
        var url = "{{ route('admin.rfi.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })
    };

    function officialresponse(id) {
        var url = "{{ route('admin.rfi.officialResponse') }}";
        var token = "{{ csrf_token() }}";
        var rfi = "{{ $rfi->id }}";
        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token,'replyid': id,'rfi': rfi},
            success: function(response){

            }
        })
    };

    function updateTask(){
        $.easyAjax({
            url: '{{route('admin.rfi.replyPost', [$rfi->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                var msgs = "@lang('Reply posted successfully')";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('admin.rfi.index') }}'
            }
        })
    }

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.rfi.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });
</script>

@endpush
