<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Asset</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="form-group">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" name="name" placeholder="Enter  Name" value="{{$categoryss->name}}"/>
            </div>
            <div class="form-group">
                <label for="inputName">Description</label>
                <input type="text" class="form-control" id="inputName" name="description" placeholder="Enter  Name"  value="{{$categoryss->description}}"/>
            </div>
            <div class="form-group">
                <label for="inputName">Area</label>
                <input type="text" class="form-control" id="inputName" name="area" placeholder="Enter  Name"  value="{{$categoryss->area}}"/>

            </div>

            <div class="form-group">
                <label for="inputName">Category</label>
                <select class="form-control"  name="category">
                    <option data-tokens="ketchup mustard">Select Project</option>

                    <?php  ?>
                    @foreach($assetcategories as $cat)
                        <?php
                        $selected = '';
                        if($selected_cat->id !== NULL){
                            if($categoryss->category == $selected_cat->id){
                                $selected = 'selected';
                            } }?>
                        <option value="{{ $cat->id }}" <?=$selected; ?>>{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.asset-category.update-asset', [$categoryss->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

</script>