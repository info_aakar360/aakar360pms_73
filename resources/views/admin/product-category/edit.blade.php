<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Product Category</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'editCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <input type="hidden" id="rowid" name="rowid" value="{{$pcategory->id}}">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$pcategory->name}}">
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="updateCategory()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>
<script>
    // Store Holidays
    function updateCategory(){
        var rowid = $('#rowid').val();
        var name = $('#name').val();
        var url = "{{ route('admin.productCategory.updateCategory') }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            container: '#editCategory',
            data: $('#editCategory').serialize(),
            success: function (response) {
                console.log([response, 'success']);
                loadTable();
                $('#edit-category-form').modal('hide');
            }
        });
    }

</script>