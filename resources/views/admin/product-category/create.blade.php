<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-plus"></i>Category</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'add_pro_category_form', 'class'=>'form-horizontal ','method'=>'POST')) !!}
    <div class="form-body">
        <div class="row">
            <div id="addMoreBox1" class="clearfix">
                <div class="col-md-12">
                    <div id="dateBox" class="form-group ">
                        <label>Category Name</label>
                        <input class="form-control" autocomplete="off" id="name"  name="name" type="text" value="" placeholder="Enter Category Name"/>
                        <div id="errorDate"></div>
                    </div>
                </div>

            </div>
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="storeCategory()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>
<script>
    // Store Holidays
    function storeCategory(){
        var url = "{{ route('admin.productCategory.postProduct') }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            container: '#add_pro_category_form',
            data: $('#add_pro_category_form').serialize(),
            success: function (response) {
                console.log([response, 'success']);
                loadTable();
                $('#add-category-form').modal('hide');
            }
        });
    }
</script>