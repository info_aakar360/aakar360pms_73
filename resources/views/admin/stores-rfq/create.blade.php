@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.stores-rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.rfq.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}

                            <div class="form-body">
                                <h3 class="box-title">@lang('modules.rfq.rfqDetails')</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Project (*)</label>
                                            <select class="form-control select2" name="project_id" id="project" data-style="form-control" required>
                                                <option value="">Select Project</option>
                                                @forelse($projects as $supplier)
                                                    <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.rfq.store') (*)</label>
                                            <select class="form-control select2" name="store_id" id="store" data-style="form-control">
                                                <option value="">Select Store</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.rfq.remark')</label>
                                            <input type="text" id="remark" name="remark" value="" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.rfq.paymentTerms')</label>
                                            <textarea id="payment_terms" name="payment_terms"  class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <h3 class="box-title">@lang('modules.rfq.productDetail')</h3>
                                <hr>
                                <div class="row" style="background-color: #efefef; padding-top: 5px;">
                                    <div class="proentry">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">Products</label>
                                                <select class="form-control select2" name="product" id="bomproducts" data-style="form-control product">
                                                    <option value="">Select Product</option>
                                                   {{-- @forelse($products as $product)
                                                        <option value="{{$product->id}}">{{ $product->name }}</option>
                                                    @empty
                                                    @endforelse--}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.rfq.brand')</label>
                                                <select class="form-control select2" name="brand" data-style="form-control brand">
                                                    <option value="">Select Brand</option>
                                                    @forelse($brands as $brand)
                                                        <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.rfq.quantity')</label>
                                                <input type="number" name="quantity" value="" class="form-control quantity" min="1" placeholder="Enter Quantity">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.rfq.unit')</label>
                                                    <input type="hidden"  class="form-control quantity" name="unit_id" id="unit">
                                                    <input type="text" readonly value="" class="form-control quantity" name="unit_id_name" id="unitData">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.rfq.dateRequired')</label>
                                                <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.rfq.remark')</label>
                                                <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label" style="width: 100%;">@lang('modules.rfq.action')</label>
                                                <a href="javascript:void(0)" class="add-button btn btn-primary" >Add</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="table-responsive" id="pdata">

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <a href="{{ route('admin.stores-rfq.index') }}" class="btn btn-default">@lang('app.reset')</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.stores-rfq.store')}}',
            container: '#createRfq',
            type: "POST",
            redirect: true,
            data: $('#createRfq').serialize()
        })
    });
    /*$(document).on('change', 'select[name=product]', function(){
        var pid = $(this).val();
        var token = '{{ csrf_token() }}';
        $.ajax({
            url: '{{route('admin.rfq.getBrands')}}',
            type: 'POST',
            data: {_token: token, pid: pid},
            success: function (data) {
                $('select[name=brand]').html(data);
                $("select[name=brand]").select2("destroy");
                $("select[name=brand]").select2();
            }
        });

    });*/
    $(document).on('click', '.add-button', function(){
        var btn = $(this);
        var cid = $('select[name=product]').val();
        var bid = $('select[name=brand]').val();
        var qty = $('input[name=quantity]').val();
        var unit = $('#unit').val();
        var dated = $('input[name=date]').val();
        var remark = $('input[name=remarkx]').val();
        if(cid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
            alert('Invalid Data. All fields are mandatory.');
        }else{
            $.ajax({
                url: '{{route('admin.stores-rfq.storeTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                redirect: false,
                beforeSend: function () {
                    btn.html('Adding...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Add');
                }

            });
        }
    });
    $(document).on('click', '.deleteRecord', function(){
        var btn = $(this);
        var did = btn.data('key');


        $.ajax({
            url: '{{route('admin.stores-rfq.deleteTmp')}}',
            type: 'POST',
            data: {_token : '{{ csrf_token()  }} ', did: did},
            redirect: false,
            beforeSend: function () {
                btn.html('Deleting...');
            },
            success: function (data) {
                $('#pdata').html(data);
            },
            complete: function () {
                btn.html('Delete');
            }

        });
    });
    $('#project').change(function () {
        var pid = $(this).val();
        var token = '{{ csrf_token() }}';
        $.ajax({
            url: '{{route('admin.indent.getStores')}}',
            type: 'POST',
            data: {_token: token, pid: pid},
            success: function (data) {
                $('#store').html(data.stores);
            }
        });
    });

    $(document).on('change', 'select[name=product]', function(){
        var pid = $(this).val();
        var token = '{{ csrf_token() }}';
        var store_id = $('select[name=store_id]').val();
        var project_id = $('select[name=project_id]').val();
        $.ajax({
            url: '{{route('admin.stores.projects.indent.getBrands')}}',
            type: 'POST',
            data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
            success: function (data) {
                $('#brandData').html(data.brands);
                $('#unit').val(data.unit);
                $('#unitData').val(data.unitData);
                $("#brandData").select2("destroy");
                $("#brandData").select2();
            }
        });

    });

    $('#store').change(function () {
        var pid = $('#project').val();
        var sid = $(this).val();
        var token = '{{ csrf_token() }}';
        $.ajax({
            url: '{{route('admin.indent.getBomProducts')}}',
            type: 'POST',
            data: {_token: token,pid: pid,sid: sid},
            success: function (data) {
                $('#bomproducts').html(data.products);
            }
        });
    });
</script>
@endpush

