@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.indent.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.indent.createTitle')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.indent.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createIndent','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <input type="hidden" name="indent_id" value="{{ $id }}"/>
                        <input type="hidden" name="rfq" value="{{ $indent->id }}"/>
                        <input type="hidden" name="quote" value="{{ $indent->id }}"/>
                        <div class="form-body">
                            <h3 class="box-title">@lang('modules.indent.indentDetails')</h3>
                            <hr>
                            <div class="row">
                                <input type="text" name="project_id" value="{{ $indent->project_id }}" class="form-control" style="display: none;">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.store') (*)</label>
                                        <select class="form-control select2" name="store_id" id="store" data-style="form-control" required disabled>
                                            <option value="">Select Store</option>
                                            @forelse($stores as $supplier)
                                                <option value="{{$supplier->id}}" {{ ($supplier->id == $indent->store_id) ? 'selected' : '' }}>{{ $supplier->company_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.inventory.dated') (*)</b></label>
                                        <input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.remark')</label>
                                        <input type="text" id="remark" name="remark" value="{{ $indent->remark }}" class="form-control" disabled>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.rfq.paymentTerms') (*)</label>
                                        <textarea id="payment_terms" name="payment_terms"  class="form-control"  required></textarea>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                            <hr>
                            <div class="row" style="background-color: #efefef; padding-top: 5px;display: none;">
                                <div class="proentry">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.category')</label>
                                            <select class="form-control select2" name="product" data-style="form-control product" disabled>
                                                <option value="">Select Category</option>
                                                @forelse($products as $product)
                                                    <option value="{{$product->id}}">{{ $product->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.brand')</label>
                                            <select class="form-control select2" name="brand" data-style="form-control brand" disabled>
                                                <option value="">Select Brand</option>
                                                @forelse($brands as $brand)
                                                    <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.quantity')</label>
                                            <input type="number" name="quantity" value="" class="form-control quantity" placeholder="Enter Quantity" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.unit')</label>
                                            <select class="form-control select2" name="unit" data-style="form-control unit" disabled>
                                                <option value="">Select Unit</option>
                                                @forelse($units as $unit)
                                                    <option value="{{$unit->id}}">{{ $unit->symbol }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.dateRequired')</label>
                                            <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.remark')</label>
                                            <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label" style="width: 100%;">@lang('modules.indent.action')</label>
                                            <a href="javascript:void(0)" class="add-button btn btn-primary" style="color: #ffffff;" disabled>Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Product</th><th>Brand</th><th>Indented Quantity</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax</th><th>Amount</th><th>Remark</th></thead><tbody>';
                                    $i = 1;
                                    if(count($tmpData)){
                                        foreach($tmpData as $data){
                                            $ipq = \App\IndentProducts::where('indent_id', $id)->where('cid',$data->cid)->first();
                                            $products = \App\ProductLog::where('project_id',$indent->project_id)->where('store_id',$indent->store_id )->where('company_id',$companyid)->where('product_id',$data->cid)->where('indent_id',$id)->where('module_name','grn')->sum('quantity');
                                            $preturn = \App\ProductLog::where('project_id',$indent->project_id)->where('store_id',$indent->store_id )->where('company_id',$companyid)->where('product_id',$data->cid)->where('indent_id',$id)->where('module_name','product_return')->sum('quantity');
                                                $iq = 0;
                                                if(!empty($ipq->quantity) && !empty($products)){
                                                    $iq = $ipq->quantity - $products;
                                                }else{
                                                    $iq = $ipq->quantity;
                                                }
                                                if(!empty($preturn)){
                                                    $iq = $iq + $preturn;
                                                }
                                                $html .= '<tr>
                                                    <td>'.$i.'</td>
                                                    <td><input type="hidden" name="cid[]" value="'.$data->cid.'" class="form-control">'.get_local_product_name($data->cid).'</td>
                                                    <td><input type="hidden" name="bid[]" value="'.$data->bid.'" class="form-control">'.get_pbrand_name($data->bid).'</td>
                                                    <td><input type="hidden" name="indent_qty[]" value="'.$iq.'" class="form-control">'.$iq.'</td>
                                                    <td><input type="number" name="qty[]" class="form-control" id="qty'.$i.'" onkeyup="validateQty(this,'.$iq.','.$i.')" placeholder="Quantity" min="1" required ></td>
                                                    <td><input type="hidden" name="unit[]" value="'.$data->unit.'" class="form-control">'.get_unit_name($data->unit).'</td>
                                                    <td><input type="number" name="price[]" class="form-control" id="price'.$i.'" onkeyup="calculateprice(this,'.$i.')" min="0"> '.$data->price.' </td>
                                                    <td><input type="number" name="tax[]"  class="form-control" id="tax'.$i.'" onkeyup="calculatetax(this,'.$i.')" min="0"> '.$data->tax.' </td>
                                                    <td><label id="totalamount'.$i.'" name="amount[]" class="totalamount">0</label></td>
                                                    <td><input type="hidden" name="remark[]" value="'.$data->remark.'" class="form-control">'.$data->remark.'</td>
                                                </tr>';
                                                $i++;

                                        }
                                            $html .= '<tr>
                                                <td style="text-align: right" colspan="8">Total Amount</td>
                                                <td><input type="number" style="display: none" name="grandTotal" id="grandTotal" value="">
                                                <label name="gtTotal" id="gtTotal">0</label>
                                                </td>
                                                <td></td>
                                                </tr>';
                                        $html .= '<tr>
                                                <td style="text-align: right" colspan="8">Freight (If Extra)</td>
                                                <td><input type="number" class="" name="freight" value="0.00"/></td>
                                                <td></td>
                                                 </tr>';
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                 ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Convert to GRN</button>
                            <a href="{{ route('admin.indent.index') }}" class="btn btn-success" style="color: #ffffff;">@lang('app.cancel')</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.indent.postConvertGrn', $indent->id)}}',
                container: '#createIndent',
                type: "POST",
                redirect: true,
                data: $('#createIndent').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.stores.projects.indent.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || qty == '' || qty == 0 || unit == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.stores.projects.indent.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');


            $.ajax({
                url: '{{route('admin.stores.projects.indent.deleteTmp')}}',
                type: 'POST',
                data: {_method: 'DELETE', _token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });

        function getGtTotal(){
            var totalPrice = 0;
            $('.totalamount').each(function (index, value) {
                totalPrice += parseInt($(this).html());
            })
            return totalPrice;
        }
      function validateQty(id,qty,index){
          var inputvalue = id.value;
          if(inputvalue>qty){
              alert('Please Enter Valid Quantity');
              $('#save-form').prop('disabled', true);
          }else{
              var price = $('#price'+index).val();
              var tax = $('#tax'+index).val();
              var qty = $('#qty'+index).val();
              var amount = parseInt(qty*price) + parseInt(qty*price*tax)/100;
              $('#totalamount'+index).html(amount);
              var gtamount2 = getGtTotal();
              $('#gtTotal').html(gtamount2);
              $('#grandTotal').val(gtamount2);
              $('#save-form').prop('disabled',false);
          }
      }
      function calculateprice(price,index){
        var price = price.value;
        var tax = $('#tax'+index).val();
        var qty = $('#qty'+index).val();
        var amount = parseInt(qty*price) + parseInt(qty*price*tax)/100;
        $('#totalamount'+index).html(amount);
          var gtamount2 = getGtTotal();
          $('#gtTotal').html(gtamount2);
          $('#grandTotal').val(gtamount2);
      }
      function calculatetax(tax,index){
          var tax = tax.value;
          var price = $('#price'+index).val();
          var qty = $('#qty'+index).val();
          var amount = parseInt(qty*price) + parseInt(qty*price*tax)/100;
          $('#totalamount'+index).html(amount);
          var gtamount2 = getGtTotal();
          $('#gtTotal').html(gtamount2);
          $('#grandTotal').val(gtamount2);
      }
    </script>
@endpush

