@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.indent.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.indent.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#menu1">@lang('app.details')</a></li>
                                    <li ><a data-toggle="pill" href="#home">@lang('app.comment')</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h2><b>{{ $indent->indent_no }}</b></h2>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade">
                                <div class="form-body form-group">
                                    {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mentiontags">
                                                <label class="control-label"><strong>@lang('modules.tasks.comment'):</strong></label>
                                                <textarea class="form-control ajax-mention" name="comment" rows="7"></textarea>
                                                <input type="hidden" class="mentionsCollection" name="mentionusers" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Upload Files</label>
                                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                            <div id="file-upload-box" >
                                                <div class="row" id="file-dropzone">
                                                    <div class="col-md-12">
                                                        <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                            {{ csrf_field() }}
                                                            <div class="fallback">
                                                                <input name="file" type="file" multiple/>
                                                            </div>
                                                            <input name="image_url" id="image_url" type="hidden" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="indentID" id="indentID">
                                            <input type="hidden" name="replyID" id="replyID">
                                        </div>
                                        <div class="col-md-12">
                                            <button type="button" id="update-task" class="btn btn-success btn-width150"><i class="fa fa-check"></i> @lang('app.save')</button>
                                        </div>
                                    </div>

                                    {!! Form::close() !!}
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        @foreach($replies as $reply)
                                            <div class="col-md-11 timelineblock <?php if($user->id==$reply->added_by){ echo 'float-right';}?>">
                                                <div class="col-md-1">
                                                    <img class="img-circle w-100"  src="{{ get_users_image_link($reply->added_by) }}" />
                                                </div>
                                                <div class="col-md-11">
                                                    <div class="taskusername">{{ get_user_name($reply->added_by) }} </div>
                                                    <div class="tasksdate">{{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}</div>
                                                    <div class="commenttext">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                {!! $reply->comment !!}
                                                            </div>
                                                            <div class="col-md-12 form-group">
                                                                {!! mentionusers_html($reply->mentionusers) !!}
                                                            </div>
                                                        </div>
                                                        <?php

                                                        $replyfiles = \App\IndentsFiles::where('indents_id',$indent->id)->where('reply_id',$reply->id)->get();
                                                        if(count($replyfiles)>0){
                                                        ?>

                                                        <div class="col-md-2">
                                                            <?php
                                                            foreach ($replyfiles as $replyfile){?>
                                                            @if($replyfile->external_link != '')
                                                                <?php $imgurl = $replyfile->external_link;?>
                                                            @elseif($storage == 'local')
                                                                <?php $imgurl = asset_url('indents-files/'.$replyfile->indents_id.'/'.$replyfile->hashname);?>
                                                            @elseif($storage == 's3')
                                                                <?php $imgurl = $url.$companyid.'/indents-files/'.$replyfile->indents_id.'/'.$replyfile->hashname;?>
                                                            @elseif($storage == 'google')
                                                                <?php $imgurl = $replyfile->google_url;?>
                                                            @elseif($storage == 'dropbox')
                                                                <?php $imgurl = $replyfile->dropbox_link;?>
                                                            @endif
                                                            {!! mimetype_thumbnail($replyfile->hashname,$imgurl)  !!}
                                                            <a href="javascript:void(0);" >By {{ get_user_name($replyfile->added_by) }} <br> <?php echo date('d M Y',strtotime($replyfile->created_at));?></a>
                                                            <?php }?>
                                                        </div>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                            <div id="menu1" class="tab-pane fade in active">

                        {!! Form::open(['id'=>'createIndent','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <div class="form-body">
                            <h3 class="box-title">@lang('modules.indent.indentDetails')</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5 class="control-label">@lang('modules.indent.store') : <label class="control-label">{{ get_store_name($indent->store_id) }}</label></h5>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5 class="control-label">Project : <label class="control-label">{{ get_project_name($indent->project_id) }}</label></h5>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.indent.remark')</label>
                                        <br>
                                        {{ $indent->remark }}
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Req. Date</th><th>Remark</th></thead><tbody>';
                                    if(count($tmpData)){
                                        $i = 1;
                                        foreach($tmpData as $data){
                                            $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'</td><td>'.get_pbrand_name($data->bid).'</td><td>'.$data->qty.'</td><td>'.get_unit_name($data->unit).'</td><td>'.$data->dated.'</td><td>'.$data->remark.'</td></tr>';
                                            $i++;
                                        }
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                 ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <a href="{{ route('admin.indent.index') }}" class="btn btn-success" style="color: #ffffff;">@lang('app.back')</a>
                        </div>
                        {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.indent.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = '{{ $indent->id }}';
            formData.append('indent_id', ids);
            var replyid = $("#replyID").val();
            formData.append('reply_id', replyid);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.indentupdatedsuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.indent.index') }}'

        });

        //    update indent
        $('#update-task').click(function () {
            $.easyAjax({
                url: '{{route('admin.indent.replyPost', [$indent->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        indentID = response.indentID;
                        $('#indentID').val(response.indentID);
                        $('#replyID').val(response.replyID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.indentupdatedsuccessfully')";
                        $.showToastr(msgs, 'success');
                         window.location.href = '{{ route('admin.indent.index') }}'
                    }
                }
            })
        });

        //    update indent
        function removeFile(id) {
            var url = "{{ route('admin.indent.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        window.location.reload();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('admin.indent.replyPost', [$indent->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        indentID = response.indentID;
                        $('#indentID').val(response.indentID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.indentCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.indent.index') }}'
                    }
                }
            })
        }

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.indent.update', $indent->id)}}',
                container: '#createIndent',
                type: "PATCH",
                redirect: true,
                data: $('#createIndent').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var store_id = $('input[name=store_id]').val();
            var project_id = $('input[name=project_id]').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.indent.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
                success: function (data) {
                    $('select[name=brand]').html(data.brands);
                    $('#estQty').val(data.estQty);
                    $('#reqQty').val(data.reqQty);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });

        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            var store_id = $('input[name=store_id]').val();
            var project_id = $('input[name=project_id]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == '' || store_id == '' || project_id == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.indent.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');


            $.ajax({
                url: '{{route('admin.indent.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });
        var url = '{{ route('admin.indent.members',[$indent->project_id]) }}';
        $('textarea.ajax-mention').mentionsInput({
            onDataRequest:function (mode, query, callback) {
                $.getJSON(url, function(responseData) {
                    responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                    callback.call(this, responseData);
                });
            }
        });
    </script>
@endpush

