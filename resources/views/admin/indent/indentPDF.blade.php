<!DOCTYPE html>
<html>
<head>
    <title>Indent</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        table {
            border-collapse:collapse; table-layout:fixed;
        }
        .carddesign{
            background-color: #ebf0f9;
            padding: 8px;
            border-top-left-radius:6px;
            border-top-right-radius:6px;
        }
        table tr td,  table tr th {
            border: 1px solid #dae3f4;
            padding:2px;
            font-weight: normal;
            word-wrap:break-word;
            page-break-after: always !important;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            right: 0cm;
            height: 1cm;
        }
    </style>
</head>
<body>
<main>
<table class="table">
    <thead>
        <tr>
            <th scope="col" colspan="4"><h6>Project: @if(!empty($indent->project_id)) {{get_project_name($indent->project_id)}} @endif</h6></th>
            <th scope="col" colspan="2"><h6>Date: @if(!empty($indent->created_at)) {{\Carbon\Carbon::parse($indent->created_at)->format('d-m-Y')}} @endif</h6></th>
        </tr>
    </thead>
</table>
@if(!empty($all_data))
    <table class="table">
<thead>
<tr class="carddesign">
    <th scope="col"><center>S.no.</center></th>
    <th scope="col"><center>Material Name</center></th>
    <th scope="col"><center>Unit</center></th>
    <th scope="col"><center>Quantity</center></th>
    <th scope="col"><center>Req. Date</center></th>
    <th scope="col"><center>Remark</center></th>
</tr>
</thead>
<tbody>
@foreach($all_data as $key=>$data)
    <tr>
        <th scope="row"><center>{{$key+1}}</center></th>
        <td><center>{{get_local_product_name($data->cid)}}</center></td>
        <td><center>{{get_unit_name($data->unit)}}</center></td>
        <td><center>{{$data->quantity}}</center></td>
        <td><center>{{$data->expected_date}}</center></td>
        <td><center>{{$data->remarks}}</center></td>
    </tr>
@endforeach
</tbody>
</table>
@endif
</main>
<footer>
    <center><p style="font-size: 17px; text-align: center">Powered By: <img src="{{ public_path('uploads/aakar-logo.png') }}" width="100px" style="margin-top:5px;" /></p></center>
</footer>
</body>
</html>