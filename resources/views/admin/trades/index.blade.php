@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createTrade" class="btn btn-outline btn-success btn-sm">Add Trade <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
        <div class="col-md-3">
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Symbol</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($trades as $key=>$type) {

                            if(count($trades)!=0) {
                            ?>
                               <tr id="tablerow{{ $type->id }}">
                                <td>{{ $key+1 }}</td>
                        <td>{{ $type->title }}</td>
                        <td>{{ $type->symbol }}</td>
                                   <td>
                                       <a type="button" class="btn btn-default editTrade" data-trade-id="{{ $type->id  }}"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-trade-id="{{ $type->id }}" class="btn btn-sm btn-danger btn-circle sa-params"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                   </td>
                        </tr>
                        <?php
                           }else{
                               ?>
                           <tr>
                               <td colspan="3">No Trades Found</td>
                           </tr>

                            <?php
                                } }
                               ?>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->


    <!-- Start Modal add sub location location -->
    <div class="modal" id="createTrade">
        {!! Form::open(['id'=>'createTradeForm','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Trade</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>

                    <div class="form-group">
                        <label for="inputTitle">Title</label>
                        <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Enter Title"/>
                    </div>
                    <div class="form-group">
                        <label for="inputSymbol">Symbol</label>
                        <input type="text" class="form-control" id="inputSymbol" name="symbol" placeholder="Enter Symbol"/>
                    </div>


                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    $('#createTrade').submit(function () {
        $.easyAjax({
            url: '{{route('admin.trades.store')}}',
            container: '#createLocation',
            type: "POST",
            data: $('#createTradeForm').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        });
        return false;
    });

    $('.editTrade').click(function(){
        var id = $(this).data('trade-id');
        var url = '{{ route('admin.trades.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Trade");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('trade-id');
        var recurring = $(this).data('recurring');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted Trade item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm') {

                var url = "{{ route('admin.trades.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        $("#tablerow"+id).remove();
                    }
                });
            }
        });
    });

</script>
@endpush