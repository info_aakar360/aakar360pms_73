@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> Recurring Component</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">Recurring Component</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel ">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRecurringcomponent','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.salarystructure.create')</h3>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.name')</label>
                                                                <div class="form-group">
                                                                    <input id="component_name" name="component_name" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.componenttype')</label>
                                                                <div class="form-group">
                                                                    <select id="component_type" name="component_type" class="form-control">
                                                                        <option value="allowance">Allowance</option>
                                                                        <option value="reimbursement">Reimbursement</option>
                                                                        <option value="contribution">Contribution</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.lopdependent')</label>
                                                                <div class="form-group">
                                                                    <select id="lop_dependent" name="lop_dependent" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Included In CTC</label>
                                                                <div class="form-group">
                                                                    <select id="include_in_ctc" name="include_in_ctc" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>Individual Override</label>
                                                                <div class="form-group">
                                                                    <select id="individual_override" name="individual_override" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Proof Required</label>
                                                                <div class="form-group">
                                                                    <select id="proof_required" name="proof_required" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                                <label>Taxable</label>
                                                                <div class="form-group">
                                                                    <select id="taxable" name="taxable" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <hr/>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>Calculation Type</label>
                                                                <div class="form-group">
                                                                    <select id="calc_type" name="calc_type" class="form-control">
                                                                        <option value="flat">Flat</option>
                                                                        <option value="slab">Slab Wise</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="anualamount">
                                                                <label>Enter Annual Amount</label>
                                                                <div class="form-group">
                                                                    <input type="text" name="anual_amount" id="anual_amount" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" style="display: none" id="basedon">
                                                                <label>Based On</label>
                                                                <div class="form-group">
                                                                    <select id="baded_on" name="baded_on" class="form-control">
                                                                        <option value="ctc">CTC</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            <div id="insertBefore"></div>
                                                            <div class="clearfix">

                                                            </div>
                                                            <div class="form-group">
                                                                <button type="button" id="plusButton" style="display:none" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                                                    Create Slab<i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                            <div class="form-actions">
                                                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                    @lang('app.save')
                                                                </button>
                                                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        var $insertBefore = $('#insertBefore');
        var $i = 0;
        // Date Picker
        jQuery('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        // Add More Inputs
        $('#plusButton').click(function(){

            $i = $i+1;
            var i = $i;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
                '<div class="col-md-1 "style="margin-left:5px;"><div class="form-group"><label  >Slab '+i+' -></label></div></div>' +
                '<div class="col-md-2"><div class="form-group "><input au class="form-control" id="date'+indexs+'" name="from['+$i+']"  type="text" value="" placeholder="0"/></div></div>' +
                '<div class="col-md-1 "style="margin-left:5px;"><div class="form-group"><label >To<label></div></div>' +
                '<div class="col-md-2 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="to['+$i+']" type="text" value="" placeholder="300000"/></div></div>' +
                '<div class="col-md-1 "style="margin-left:5px;"><div class="form-group"><label  >=</label></div></div>' +
                '<div class="col-md-2 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="equal['+$i+']" type="text" value="" placeholder="300000"/></div></div>' +
                '<div class="col-md-1 "style="margin-left:5px;"><div class="form-group"><label  >/annum</label></div></div>' +
                '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);

            // Recently Added date picker assign
            jQuery('#dateField'+indexs).datepicker({
                autoclose: true,
                todayHighlight: true,
                weekStart:'{{ $global->week_start }}',
                format: '{{ $global->date_picker_format }}',
            });
        });
        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        $('#calc_type').change(function () {

            if($(this).val() == "slab"){
                $("#plusButton").show()
                $("#basedon").show()
                $("#anualamount").hide()
            }else{
                $("#plusButton").hide()
                $("#basedon").hide()
                $("#anualamount").show()
            }
        });
        $('#baded_on').change(function () {

            if($(this).val() == "grade"){
                $("#plusButton").hide()
            }else{
                $("#plusButton").show()
            }
        });
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.component.storeRecurringcomponent')}}',
                container: '#createRecurringcomponent',
                type: "POST",
                redirect: true,
                data: $('#createRecurringcomponent').serialize()
            })
        });
    </script>
@endpush