<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Random Earning Component</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'createnewAdhoc','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Component Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Taxable</label>
                    <select name="taxable" id="taxable" class="form-control">
                        <option value="yes">yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >ESI Applicable</label>
                    <select name="esiapplicable" id="esiapplicable" class="form-control">
                        <option value="yes">yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="save-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.component.postAdhocEarning')}}',
            container: '#createnewAdhoc',
            type: "POST",
            data: $('#createnewAdhoc').serialize(),
            success: function (response) {
                $('#newAdhocEarningModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>