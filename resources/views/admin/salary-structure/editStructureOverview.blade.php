<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.salarystructure.editstructureoverview')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'editStructureOverview','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" >{{$employee->name}}</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" >Effective Date</label>
                        <input type="date" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" >CTC</label>
                        <input type="text" class="form-control" name="ctc" id="ctc" value="{{$employee->monthly_rate}}">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" >Basic (In %)</label>
                        <input type="text" class="form-control" name="basic" id="basic" value="{{$wordata->basic }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" >HRA (In %)</label>
                        <input type="text" class="form-control" name="hra" id="hra" value="{{$wordata->hra }}">
                    </div>
                </div>
            </div>
            @foreach($allowances as $allowance)
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" >{{$allowance->component_name}}</label>
                    <input type="text" class="form-control" name="{{$allowance->component_name}}" id="{{$allowance->component_name}}" value="{{$allowance->anual_amount}}">
                </div>
            </div>
            @endforeach
            <div class="form-actions">
                <button type="submit" id="stoverviewsave-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#stoverviewsave-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.structure.postStructureOverview')}}',
            container: '#editStructureOverview',
            type: "POST",
            data: $('#editStructureOverview').serialize(),
            success: function (response) {
                $('#editStructureOverviewModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>