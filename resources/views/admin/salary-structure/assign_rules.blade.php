<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.salarystructure.assignsalaryStructure')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'assignsalaryStructure','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Structure Name</label>

                        <select id="rules_id" name="rules_id" class="form-control">
                            @foreach($rules as $rule)
                            <option value="{{$rule->id}}">{{$rule->structure_name}}</option>
                            @endforeach
                        </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Effective Date</label>
                    <input type="date" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="assignsave-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::hidden('ids', $ids) !!}
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#assignsave-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.structure.postAssignStructure')}}',
            container: '#assignsalaryStructure',
            type: "POST",
            data: $('#assignsalaryStructure').serialize(),
            success: function (response) {
                $('#salaryStructureAssignModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>