@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> Salary Structure</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">Salary Structure</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel ">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createStructure','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.salarystructure.create')</h3>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>Name</label>
                                                                <div class="form-group">
                                                                    <input id="name" name="name" class="form-control" value="{{$structure->structure_name}}">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label>Description</label>
                                                                <div class="form-group">
                                                                    <input id="description" name="description" class="form-control" value="{{$structure->structure_desc}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.component_basic')</label>
                                                                <div class="form-group">
                                                                    <input id="component_basic" name="component_basic" class="form-control" value="{{$structure->basic}}" placeholder="Enter % Amount of CTC">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.component_hra')</label>
                                                                <div class="form-group">
                                                                    <input id="component_hra" name="component_hra" class="form-control" value="{{$structure->hra}}" placeholder="Enter % Amount of Basic">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <p>@lang('modules.payrollsettings.pfemployer') <span class="pull-right">System Calculated</span></p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>@lang('modules.payrollsettings.esiemployer') <span class="pull-right">System Calculated</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <p>Special Allowance  <span class="pull-right">Balancing Amount of CTC</span></p>
                                                            </div>
                                                        </div>

                                                        @if(!empty($structure->allowance))
                                                        <?php   $allowances = explode(',',$structure->allowance);
                                                          $var = explode(',',$structure->amount);?>
                                                        @foreach($allowances as $key=>$allowanc)

                                                        <div id="addMoreBox{{$key}}" class="clearfix">
                                                            <div class="col-md-5"><div class="form-group ">
                                                                    <select  class="form-control" id="allowance{{$key}}" name="allowance[{{$key}}]"  >
                                                                        <option value=" ">Select Allowance</option>
                                                                        @foreach($componets as $compo)
                                                                            <option
                                                                                    @if($allowanc == $compo->id) selected @endif value="{{$compo->id}}">{{$compo->component_name}}
                                                                            </option>
                                                                        @endforeach
                                                                        @foreach($variables as $variable)

                                                                            <option
                                                                                    @if($allowanc == 'var'.$variable->id) selected @endif value="var{{$variable->id}}">{{$variable->component_name}}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5 "style="margin-left:5px;">
                                                                <div class="form-group">
                                                                    <input class="form-control " id="amount{{$key}}" name="amount[{{$key}}]" type="text" value="{{$var[$key]}}" placeholder="Amount">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button type="button" onclick="removeBox({{$key}})" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        @endif
                                                        <div id="insertBefore"></div>
                                                        <div class="clearfix">

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                                                    Add Salary Component <i class="fa fa-plus"></i>
                                                                </button>
                                                                <div class="form-actions">
                                                                    <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                        @lang('app.save')
                                                                    </button>
                                                                    <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="id" name="id" value="{{$structure->id}}">
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        var $insertBefore = $('#insertBefore');
        var $i = 0;
        // Date Picker
        jQuery('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        // Add More Inputs
        $('#plusButton').click(function(){

            $i = $i+1;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
                '<div class="col-md-5"><div class="form-group "><select  class="form-control" id="allowance'+indexs+'" name="allowance['+$i+']"  ><option value=" ">Select Allowance</option>@foreach($componets as $compo)<option value="{{$compo->id}}">{{$compo->component_name}}</option>@endforeach @foreach($variables as $variable)<option value="var{{$variable->id}}">{{$variable->component_name}}</option>@endforeach</select></div></div>' +
                '<div class="col-md-5 "style="margin-left:5px;"><div class="form-group"><input class="form-control " id="amount'+indexs+'" name="amount['+$i+']" type="text" value="" placeholder="Amount"/></div></div>' +
                '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);

            //Set The Amount Based On Allowance
            $('#allowance'+indexs).change(function () {
                id = $(this).val();
                var url = "{{ route('admin.structure.getStructureAmount',':id') }}";
                url = url.replace(':id', id);
                var token = "{{ csrf_token() }}";
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {'_token': token,'id':id},
                    success: function (data) {
                        $("#amount"+indexs).val(data.amount[0])
                    }
                });
            });
            //End Set The Amount Based On Allowance
        });
        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.structure.updateStructure')}}',
                container: '#createStructure',
                type: "POST",
                redirect: true,
                data: $('#createStructure').serialize()
            })
        });

    </script>
@endpush