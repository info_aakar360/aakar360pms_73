@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel ">

                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createBaseComponent','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.salarystructure.salaryComponent')</h3>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.component_basic')</label>
                                                                <div class="form-group">
                                                                    <input id="component_basic" name="component_basic" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label>@lang('modules.salarystructure.component_hra')</label>
                                                                <div class="form-group">
                                                                    <input id="component_hra" name="component_hra" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.pfemployer')</label><span class="pull-right">System Calculated</span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.esiemployer')</label><span class="pull-right">System Calculated</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <input type="text" name = "specialallowane" class="form-control" value="Special Allowance"><span class="pull-right">Balancing Amount of CTC</span></input>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                @lang('app.save')
                                                            </button>
                                                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });


        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.component.storeBaseComponent')}}',
                container: '#createRecurringcomponent',
                type: "POST",
                redirect: true,
                data: $('#createBaseComponent').serialize()
            })
        });
    </script>
@endpush