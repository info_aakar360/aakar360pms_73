@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('app.update') @lang('app.menu.timeLogs')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTimeLogs','class'=>'ajax-form','method'=>'PUT']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('app.project') @lang('app.name')</label>
                                        <div>{{ $timeLog->project->project_name }}</div>
                                        <input name="project_id" type="hidden" value="{{ $timeLog->project->id }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('app.employee') @lang('app.name')</label>
                                        <div>{{ get_users_employee_name($timeLog->user_id) }}</div>
                                        <input name="user_id" type="hidden" value="{{ $timeLog->user_id }}">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.timeLogs.startDate')</label>
                                        <input type="text" name="start_date" id="start_date"
                                               value="{{ $timeLog->start_time->format($global->date_format) }}"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.timeLogs.endDate')</label>
                                        <input type="text" name="end_date" id="end_date"
                                               value="{{ $timeLog->end_time->format($global->date_format) }}"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <label>@lang('modules.timeLogs.startTime')</label>
                                        <input type="text" name="start_time" id="start_time"
                                               class="form-control" value="{{ $timeLog->start_time->format('H:i') }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <label>@lang('modules.timeLogs.endTime')</label>
                                        <input type="text" name="end_time" id="end_time"
                                               class="form-control" value="{{ $timeLog->end_time->format('H:i') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('modules.timeLogs.totalHours')</label>
                                        <input type="text" name="total_hours" id="total_hours"
                                               value="{{ $timeLog->total_hours }}"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>


                            <div class="row m-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="memo">@lang('modules.timeLogs.memo')</label>
                                        <textarea name="memo" id="memo" class="form-control summernote">{{ $timeLog->memo }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url" type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="taskID" id="taskID">
                                </div>
                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.update')</button>
                            <a href="{{ route('admin.all-time-logs.index') }}" class="btn btn-default">@lang('app.back')</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.time-logs.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#taskID').val();
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Log Updated Successfully";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.all-time-logs.index') }}'

        });

        $("#end_date, #start_date, .date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            format: '{{ $global->date_picker_format }}',
        }).on('hide', function (e) {
            calculateTime();
        });

        $('#start_time, #end_time').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false
            @endif
        }).on('hide.timepicker', function (e) {
            calculateTime();
        });

        function calculateTime() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();
            var startTime = $("#start_time").val();
            var endTime = $("#end_time").val();

            var timeStart = startDate + " " + startTime;
            var timeEnd =  endDate + " " + endTime;

            console.log(startTime);
            console.log(endDate);
            var url = '{{route('admin.all-time-logs.dateCal')}}';
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: url,
                type: "POST",
                data: {timeStart: timeStart,timeEnd: timeEnd, _token: token},
                success: function (data) {
                    $('#total_time').html("");
                    $('#total_time').html(data);
                }
            });

        }

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.time-logs.update', [$timeLog->id])}}',
                container: '#updateTimeLogs',
                type: "POST",
                redirect: true,
                data: $('#updateTimeLogs').serialize(),
                success: function (data) {
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = data.taskID;
                        $('#taskID').val(data.taskID);
                        myDropzone.processQueue();
                    }else{
                        var msgs = "Log Updated Successfully";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.all-time-logs.index') }}'
                    }

                }
            })
        });
    </script>
@endpush

