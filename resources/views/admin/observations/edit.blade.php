@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i>@lang('modules.observation.observation')</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.observations.index') }}">@lang('modules.observation.observation')</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.observation.observation')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Inspection Name:</label>
                                        <P><strong>{{ $inspection->name }}</strong></P>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Question:</label>
                                        <P><strong>{{ !empty($inspectionquestion->question) ? $inspectionquestion->question : '' }}</strong></P>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.title') </label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" value="{{ $observation->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="select2" name="user_id[]" multiple id="user_id" >
                                            <option value="">@lang('modules.observations.chooseAssignee')</option>
                                            <?php $as = explode(',',$observation->assign_to); ?>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}"
                                                <?php  if (in_array($employee->user_id, $as)){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Distribution
                                        </label>
                                        <select class="select2" name="distribution[]"   multiple  required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}"
                                                <?php
                                                    $ass = explode(',',$observation->distribution);
                                                    if (in_array($employee->user_id, $ass)){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open" <?php if($observation->status=='Open'){ echo 'selected';}?>>Open</option>
                                            <option value="Initiated" <?php if($observation->status=='Initiated'){ echo 'selected';}?>>Initiated</option>
                                            <option value="Not accepted" <?php if($observation->status=='Not accepted'){ echo 'selected';}?>>Not accepted</option>
                                            <option value="Closed" <?php if($observation->status=='Closed'){ echo 'selected';}?>>Closed</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type</label>
                                        <select class="select2 form-control" name="type"  required>
                                            <option value="">Please Select type</option>
                                            <option value="Architect" <?php if($observation->type=='Architect'){ echo 'selected';}?>>Architect</option>
                                            <option value="Contractor" <?php if($observation->type=='Contractor'){ echo 'selected';}?>> Contractor</option>
                                            <option value="Owner" <?php if($observation->type=='Owner'){ echo 'selected';}?>>Owner</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" id="location" name="location" class="form-control" value="{{ $observation->location }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.startDate')</label>
                                        <?php
                                        $ex = explode('-',$observation->start_date);
                                        ?>
                                        <input type="text" name="start_date"  id="start_date2" class="form-control" value="{{ !empty($observation->start_date) ? $ex[2].'-'.$ex[1].'-'.$ex[0] : '' }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <?php
                                        $exd = explode('-',$observation->due_date);
                                        ?>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" value="{{ !empty($observation->due_date) ? $exd[2].'-'.$exd[1].'-'.$exd[0] : '' }}" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-6 m-b-20">
                                    <div class="form-group">
                                        <label for="privete">Private or Public</label><br>
                                        <input id="privete" name="private" value="1" type="checkbox" <?php if($observation->private == '1'){ echo 'checked';}?>>
                                        <label for="privete">Private</label>
                                    </div>
                                </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Priority</label>
                                            <select class="selectpicker form-control" name="priority" data-style="form-control" required>
                                                <option value="">Please Select Priority</option>
                                                <?php if($observation->priority == 'High'){ ?>
                                                <option value="High" selected>High</option>
                                                <option value="Medium">Medium</option>
                                                <option value="Low">Low</option>
                                                <?php } if($observation->priority == 'Medium'){ ?>
                                                <option value="High">High</option>
                                                <option value="Medium" selected>Medium</option>
                                                <option value="Low">Low</option>
                                                <?php } if($observation->priority == 'Low'){ ?>
                                                <option value="High">High</option>
                                                <option value="Medium">Medium</option>
                                                <option value="Low" selected>Low</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label class="control-label">Reference
                                            </label>
                                            <input type="text" name="reference" class="form-control" value="{{ $observation->reference }}">
                                        </div>
                                    </div>

                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.description')</label>
                                        <textarea id="description" name="description" class="form-control summernote">{{ $observation->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <label class="control-label">@lang('app.files')</label>
                                                <div class="dropzone"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="observationID" id="observationID">
                                </div>
                                <div class="col-md-12">
                                    @foreach($files as $file)
                                        <div class="col-md-2" style="text-align: center;">
                                            <img src="{{ url('user-uploads/observation-files/'.$file->observation_id.'/'.$file->hashname) }}">
                                            <br>
                                            <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                                Remove
                                            </a>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="observationid" value="{{ $observation->id }}" />
                            <input type="hidden" name="inspection" value="{{ $inspection->id }}" />
                            <input type="hidden" name="question" value="{{ !empty($inspectionquestion->id) ? $inspectionquestion->id : '' }}" />
                            <button type="button" id="update-observation" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="observationCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.observations.observationstoreImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = '{{ $observation->id }}';
            formData.append('observation_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.observationUpdatedSuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.observations.index') }}'

        });

        //    update observation
        $('#update-observation').click(function () {

            $.easyAjax({
                url: '{{route('admin.observations.observationstore')}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        observationID = response.observationID;
                        $('#observationID').val(response.observationID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "Observations updated successfully";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.observations.index') }}'
                    }
                }
            })

        });

        //    update observation
        function removeFile(id) {
            var url = "{{ route('admin.observations.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        window.location.reload();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('admin.observations.observationstore')}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        observationID = response.observationID;
                        $('#observationID').val(response.observationID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.observationCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.observations.index') }}'
                    }
                }
            })
        }

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });



       

    </script> 

@endpush
