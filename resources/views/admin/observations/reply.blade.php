@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.observations.index') }}">@lang('modules.observation.observationsreply')</a></li>
            <li class="active">@lang('app.reply')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
    ul.timelinereply {
        list-style-type: none;
        position: relative;
    }
    ul.timelinereply:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timelinereply > li {
        margin: 5px 0;
        padding-left: 20px;
    }
    ul.timelinereply > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-inverse">
            <div class="panel-heading"> @lang('modules.observation.observationsreply')</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>Inspection Name:</b></label><br>
                                    {{ $inspection->name }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>Question:</b></label><br>
                                    {{ $inspectionquestion ? $inspectionquestion->question : '' }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('app.title')</b> </label><br>
                                    {{ $observation->title }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('modules.observations.assignTo')</b></label><br>
                                   {{ get_user_name($observation->assign_to) }}
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Type</b></label><br>
                                    {{ $observation->type }}

                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label"><b>Distribution</b>
                                    </label><br>
                                    {{ get_user_name($observation->distribution) }}
                                </div>
                            </div>


                            <!--/span-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('app.description')</b></label><br>
                                    {!!  $observation->description !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"><b>Location</b></label><br>
                                    {{ $observation->location }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('app.startDate')</b></label><br>
                                    <?php
                                        $ex = explode('-',$observation->start_date);
                                    ?>
                                    {{ $ex[2].'-'.$ex[1].'-'.$ex[0] }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('app.dueDate')</b></label><br>
                                    <?php
                                    $exd = explode('-',$observation->due_date);
                                    ?>
                                    {{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="privete"><b>Private or Public</b></label><br>
                                    <?php if($observation->private == '1'){ echo 'Private';}else{ echo 'Public'; }?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>Priority</b></label><br>
                                        {{ $observation->priority }}
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label"><b>Reference</b>
                                        </label><br>
                                        {{ $observation->reference }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <ul class="nav nav-pills">
                                   <li class="active"><a data-toggle="pill" href="#home" >Status</a></li>
                                    <li><a data-toggle="pill" href="#menu1" >Comments</a></li>
                                    <li><a data-toggle="pill" href="#menu2" >Attachments</a></li>
                                    <li><a data-toggle="pill" href="#comments">Reply</a></li>
                                 </ul>
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <?php if(count($replies)>0){
                                        ?>
                                        <ul class="timelinereply">
                                            <?php
                                            foreach ($replies as $reply){?>
                                            <li>
                                                <a href="javascript:void(0);" > Status <b>{!! $reply->status !!}</b> Update from {{ get_user_name($reply->added_by) }}</a>
                                                <a href="#" class="float-right"><?php echo date('d M Y',strtotime($reply->created_at));?></a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                        <?php }?>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <?php if(count($replies)>0){
                                        ?>
                                        <ul class="timelinereply">
                                            <?php
                                            foreach ($replies as $reply){?>
                                            <li>
                                                {!! $reply->comment !!}
                                                <a href="#" class="float-right"><?php echo date('d M Y',strtotime($reply->created_at));?></a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                        <?php }?>
                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                        <?php if(count($files)>0){
                                        ?>
                                        <ul class="timelinereply">
                                            @foreach($files as $file)
                                                <li>
                                                    <a href="javascript:void(0);" >Attachment By {{ get_user_name($file->user_id) }}</a>
                                                    <a href="#" class="float-right"><?php echo date('d M Y',strtotime($file->created_at));?></a>
                                                    <p>{{ $file->filename }}</p>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <?php }?>
                                    </div>
                                    <div id="comments" class="tab-pane fade">
                                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                                        <hr>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Comment</label>
                                                <textarea id="description" name="comment" class="form-control summernote"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-12 ">
                                            <div class="form-group">
                                                <label class="control-label">Status
                                                </label>
                                                <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                                    <option value="">Please Select Status</option>
                                                    <?php if($observation->status == 'Open'){ ?>
                                                    <option value="Open" selected>Open</option>
                                                    <option value="Resolved">Resolved</option>
                                                    <option value="Inprogress">Inprogress</option>
                                                    <?php } elseif($observation->status == 'Resolved'){?>
                                                    <option value="Open">Open</option>
                                                    <option value="Resolved" selected>Resolved</option>
                                                    <option value="Inprogress">Inprogress</option>
                                                    <?php } else { ?>
                                                    <option value="Open">Open</option>
                                                    <option value="Resolved">Resolved</option>
                                                    <option value="Inprogress" selected>Inprogress</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">&nbsp;</div>
                                        <!--/span-->
                                        <div class="row m-b-20">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                <div id="file-upload-box" >
                                                    <div class="row" id="file-dropzone">
                                                        <div class="col-md-12">
                                                            <div class="dropzone"
                                                                 id="file-upload-dropzone">
                                                                {{ csrf_field() }}
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple/>
                                                                </div>
                                                                <input name="image_url" id="image_url"type="hidden" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="observationID" id="observationID">
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="button" id="update-observation" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>



                        </div>
                        <!--/row-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="observationCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


<script>
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.observations.observationstoreImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = '{{ $observation->id }}';
        formData.append('observation_id', ids);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "@lang('messages.observationUpdatedSuccessfully')";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.observations.index') }}'

    });

    //    update observation
    $('#update-observation').click(function () {
        $.easyAjax({
            url: '{{route('admin.observations.replyPost', [$observation->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                if(myDropzone.getQueuedFiles().length > 0){
                    observationID = response.observationID;
                    $('#observationID').val(response.observationID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('messages.Observations updated successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.observations.index') }}'
                }
            }
        })
    });

    //    update observation
    function removeFile(id) {
        var url = "{{ route('admin.observations.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })

    };

    function updateTask(){
        $.easyAjax({
            url: '{{route('admin.observations.replyPost', [$observation->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                if(myDropzone.getQueuedFiles().length > 0){
                    observationID = response.observationID;
                    $('#observationID').val(response.observationID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('messages.observationCreatedSuccessfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.observations.index') }}'
                }
            }
        })
    }

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.observations.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });


</script>

@endpush
