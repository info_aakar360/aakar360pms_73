@extends('layouts.app')
<?php
$moduleName = " Ledger Group";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Create";
$breadcrumbMainIcon = "fas fa-file-invoice-dollar";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\IncomeExpenseGroup';
$ParentRouteName = 'admin.income_expense_group';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Put {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Name</label>
                                            <input autofocus value="{{ $group->name }}" name="name" type="text"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Parent Group</label>
                                            <select class="form-control" name="parent" >
                                                <option value="">Select Type</option>
                                                <?php if(count($groupsarray)>0){
                                                foreach ($groupsarray as $groups){
                                                ?>
                                                <option value="{{ $itemsa->id }}" @if($group->id==$itemsa->id) selected @endif >{{ $groups->name }}</option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Type</label>
                                            <select class="form-control" name="type" >
                                                <option value="">Select Type</option>
                                                <?php if(count($itemsarray)>0){
                                                foreach ($itemsarray as $itemsa){
                                                ?>
                                                <option value="{{ $itemsa->id }}" @if($itemsa->id==$group->income_type) selected @endif >{{ $itemsa->name }} - {{ $itemsa->type }}</option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" id="save-form" class="btn btn-primary m-t-15 waves-effect">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.update',['id'=>$group->id]) }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });
    </script>
@endpush