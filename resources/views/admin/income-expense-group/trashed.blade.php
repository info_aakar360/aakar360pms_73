@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }
    </style>
@endpush
<?php
$moduleName = " Ledger Group";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Trashed";
$breadcrumbMainIcon = "fas fa-file-invoice-dollar";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\IncomeExpenseGroup';
$ParentRouteName = 'admin.income_expense_group';
?>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-outline btn-blue btn-sm" id="" href="{{ route($ParentRouteName)  }}"><i class="fa fa-home"></i> All({{ $ModelName::all()->count() }})</a>
            <a class="btn btn-outline btn-blue btn-sm" id="" href="{{ route($ParentRouteName.'.trashed') }}"><i class="fa fa-home"></i> Trash({{ $ModelName::onlyTrashed()->count()  }})</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a class="btn btn-sm btn-info waves-effect" href="{{ url()->previous() }}">Back</a>
                        </div>
                    </div>
                </div>
                <div class="row b-b b-t" style="display: none; background: #fbfbfb;" id="ticket-filters">
                    <div class="col-md-12">
                        <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
                    </div>
                    <div class="col-lg-12">
                        <a class="btn btn-xs btn-info waves-effect"
                           href="{{ route($ParentRouteName)  }}">All({{ $ModelName::all()->count() }})</a>

                        <a class="btn btn-xs btn-danger waves-effect text-black"
                           href="{{ route($ParentRouteName.'.trashed') }}">Trash({{ $ModelName::onlyTrashed()->count()  }}
                            )</a>


                        <ul class="header-dropdown m-r--5">
                            <form class="search" action="{{ route($ParentRouteName.'.active.search') }}"
                                  method="get">
                                {{ csrf_field() }}
                                <input autofocus type="search" name="search" class="form-control input-sm "
                                       placeholder="Search"/>
                            </form>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <form class="actionForm" action="{{ route($ParentRouteName.'.active.action') }}"
                          method="get">
                        <div class="row body">
                            <div class="margin-bottom-0 col-md-2 col-lg-2 col-sm-2">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="apply_comand_top" id="">
                                            <option value="0">Select Action</option>
                                            <option value="1">Restore</option>
                                            <option value="2">Delete</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class=" margin-bottom-0 col-md-2 col-lg-2 col-sm-2">
                                <div class="form-group">
                                    <input class="btn btn-sm btn-info waves-effect" type="submit" value="Apply" name="ApplyTop">
                                </div>
                            </div>
                            <div class=" margin-bottom-0 col-md-8 col-sm-8 col-xs-8">
                                <div class="custom-paginate pull-right">
                                    {{ $items->links() }}
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            {{ csrf_field() }}
                            @if(count($items)>0)
                                <table class="table table-bordered table-hover toggle-circle default footable-loaded footable">
                                    <thead>
                                    <tr>
                                        <th class="checkbox_custom_style text-center">
                                            <input name="selectTop" type="checkbox" class="fpchk" onclick="pFselect()"/>
                                            <label for="md_checkbox_p"></label>
                                        </th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($items as $item)

                                        <tr>
                                            <th class="text-center">
                                                <input name="items[id][]" value="{{ $item->id }}"
                                                       type="checkbox" id="md_checkbox_{{ $i }}"
                                                       class="chk-col-cyan selects pchk"/>
                                                <label for="md_checkbox_{{ $i }}"></label>
                                            </th>

                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->code }}</td>

                                            <td class="tdAction">
                                                <a class="btn btn-info waves-effect restore"
                                                   href="javascript:;"
                                                   data-toggle="tooltip"
                                                   data-placement="top" title="Restore" data-user-id="{{ $item->id }}"><i
                                                            class="fa fa-reply"></i></a>

                                                <a class="btn btn-danger waves-effect sa-params"
                                                   href="javascript:;"
                                                   data-toggle="tooltip"
                                                   data-placement="top" title="Parmanently Delete?" data-user-id="{{ $item->id }}">
                                                    <i class="fa fa-trash"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="body table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">There Has No Data</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endif

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script>
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted group!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route($ParentRouteName.'.kill',['id'=>':id']) }}";
                        {{--var url = "{{ route('admin.clients.destroy',':id') }}";--}}
                            url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'GET',
                            url: url,
                            data: {'_token': token, '_method': 'GET'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.restore', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You want to restore this group!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, restore it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route($ParentRouteName.'.restore',['id'=>':id']) }}";
                        {{--var url = "{{ route('admin.clients.destroy',':id') }}";--}}
                            url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'GET',
                            url: url,
                            data: {'_token': token, '_method': 'GET'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });

        function pSelects(){
            var ele=document.getElementsByClassName('pchk');
            for(var i=0; i<ele.length; i++){
                if(ele[i].type=='checkbox')
                    ele[i].checked=true;
            }
        }
        function pDeSelects(){
            var ele=document.getElementsByClassName('pchk');
            for(var i=0; i<ele.length; i++){
                if(ele[i].type=='checkbox')
                    ele[i].checked=false;
            }
        }

        function pFselect() {
            var fele=document.getElementsByClassName('fpchk');
            if(fele[0].checked){pSelects();}
            if(!fele[0].checked){pDeSelects();}
        }
    </script>
@endpush