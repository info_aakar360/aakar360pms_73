@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }
    </style>
@endpush
<?php
$moduleName = " Ledger Group";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " All";
$breadcrumbMainIcon = "fas fa-file-invoice-dollar";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\IncomeExpenseGroup';
$ParentRouteName = 'admin.income_expense_group';
?>
@section('content')
    {{--<div class="row">
        <div class="col-md-12">
            <a class="btn btn-outline btn-blue btn-sm" id="" href="{{ route($ParentRouteName)  }}"><i class="fa fa-home"></i> All({{ $ModelName::all()->count() }})</a>
            <a class="btn btn-outline btn-blue btn-sm" id="" href="{{ route($ParentRouteName.'.trashed') }}"><i class="fa fa-home"></i> Trash({{ $ModelName::onlyTrashed()->count()  }})</a>
        </div>
    </div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route($ParentRouteName.'.create') }}" class="btn btn-outline btn-success btn-sm">Create Group <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <form class="actionForm" action="{{ route($ParentRouteName.'.active.action') }}"
                          method="get">
                        {{--<div class="row body">
                            <div class="margin-bottom-0 col-md-2 col-lg-2 col-sm-2">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="apply_comand_top" id="">
                                            <option value="0">Select Action</option>
                                            <option value="3">Move To trash</option>
                                            <option value="2">Permanently Delete</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class=" margin-bottom-0 col-md-2 col-lg-2 col-sm-2">
                                <div class="form-group">
                                    <input class="btn btn-sm btn-info waves-effect" type="submit" value="Apply" name="ApplyTop">
                                </div>
                            </div>
                            <div class=" margin-bottom-0 col-md-8 col-sm-8 col-xs-8">
                                <div class="custom-paginate pull-right">
                                    {{ $items->links() }}
                                </div>
                            </div>
                        </div>--}}
                        <div class="table-responsive">
                            {{ csrf_field() }}
                            @if( count($itemsarray) >0 )
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>@lang('app.sno')</th>
                                        <th>@lang('app.name')</th>
                                        <th>@lang('app.type')</th>
                                        <th>@lang('app.parent')</th>
                                        <th>@lang('app.action')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($itemsarray as $item)
                                        <tr>
                                            <th>{{ $i }}</th>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ income_type($item->income_type) }}</td>
                                            <td>{{ income_group($item->parent) }}</td>
                                            <td class="tdTrashAction">
                                                <?php if($item->default=='1'){
                                                    echo 'Default';
                                                }else{?>
                                                <a class="btn btn-info waves-effect"
                                                   href="{{ route($ParentRouteName.'.edit',['id'=>$item->id]) }}"
                                                   data-toggle="tooltip"
                                                   data-placement="top" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                <a class="btn btn-danger waves-effect sa-params"
                                                   href="javascript:;"
                                                   data-toggle="tooltip"
                                                   data-placement="top" title="Trash" data-user-id="{{ $item->id }}"> <i class="fa fa-trash"></i></a>
                                                    <?php }?>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="body table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center">There Has No Data</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endif

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@stop

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You want to trash this group!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, trash it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route($ParentRouteName.'.destroy',['id'=>':id']) }}";
                        {{--var url = "{{ route('admin.clients.destroy',':id') }}";--}}
                            url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'GET',
                            url: url,
                            data: {'_token': token, '_method': 'GET'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });

        function pSelects(){
            var ele=document.getElementsByClassName('pchk');
            for(var i=0; i<ele.length; i++){
                if(ele[i].type=='checkbox')
                    ele[i].checked=true;
            }
        }
        function pDeSelects(){
            var ele=document.getElementsByClassName('pchk');
            for(var i=0; i<ele.length; i++){
                if(ele[i].type=='checkbox')
                    ele[i].checked=false;
            }
        }

        function pFselect() {
            var fele=document.getElementsByClassName('fpchk');
            if(fele[0].checked){pSelects();}
            if(!fele[0].checked){pDeSelects();}
        }
    </script>
@endpush