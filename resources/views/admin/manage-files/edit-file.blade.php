@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title active">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a class="active" href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('modules.projects.files')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .file-bg {
            height: 150px;
            overflow: hidden;
            position: relative;
        }
        .file-bg .overlay-file-box {
            opacity: .9;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            text-align: center;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="files-list-panel">
                                    <div class="white-box" style="padding: 5px;">
                                        <h5 style="margin: 5px 0;">{{ $file->filename }}</h5>
                                        @if(session()->has('message'))
                                            <div class="alert alert-info">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <div class="tab-content" style="margin: 0;">
                                            <div role="tabpanel" class="tab-pane active" id="list">
                                                <div class="iframe-container">
                                                    <iframe src="{{ 'https://pdfediter.aakar360.com?file='.url('').'/uploads/project-files/'.$id.'/'.$urlx.$queryUrl }}" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="thumbnail">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.manage-files.createFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                @csrf
                                <input type="text" class="form-control" name="foldername" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="parent" value="<?=isset($_GET['path'])?$_GET['path']:'0';?>" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="type" value="folder" placeholder="Folder Name *" required>
                                <input type="hidden" class="form-control" name="project_id" value="{{ $id }}" placeholder="Folder Name *" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeModel" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn blue">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.manage-files.editFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                    </div>
                    <div class="modal-body" id="">
                        @csrf
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="foldername" id="editData" placeholder="Folder Name *" value="" required>
                                    <input type="hidden" class="form-control" name="id" value="0" id="folderId" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="parent" value="<?=isset($_GET['path'])?$_GET['path']:'0';?>" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="type" value="folder" placeholder="Folder Name *">
                                    <input type="hidden" class="form-control" name="project_id" value="{{ $id }}" placeholder="Folder Name *" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeEditButton" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" class="btn blue">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })

        $('.closeModel').click(function(){
            $('#taskCategoryModal').hide();
        })

        $('.closeEditButton').click(function(){
            $('#editModal').hide();
        })

        function editModal(val, name) {
            $('#editModal').show();
            $('#modelHeading').html("Edit Name");
            $('#editData').val(name);
            $('#folderId').val(val);

        }
        $('#show-dropzone').click(function () {
            $('#file-dropzone').toggleClass('hide show');
        });

        $('#show-link-form').click(function () {
            $('#file-link').toggleClass('hide show');
        });

        $("body").tooltip({
            selector: '[data-toggle="tooltip"]'
        });

        // "myAwesomeDropzone" is the camelized version of the HTML element's ID
        Dropzone.options.fileUploadDropzone = {
            paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
            dictDefaultMessage: "@lang('modules.projects.dropFile')",
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("success", function (file, response) {
                    var viewName = $('#view').val();
                    if(viewName == 'list') {
                        $('#files-list-panel ul.list-group').html(response.html);
                    } else {
                        $('#thumbnail').empty();
                        $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                    }
                })
            }
        };

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var url = "{{ route('admin.manage-files.destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                window.location.reload();
//                                $('#files-list-panel ul.list-group').html(response.html);
                            }
                        }
                    });
                }
            });
        });

        /*$('.thumbnail').on('click', function(event) {
            event.preventDefault();
            $('#thumbnail').empty();
            $.easyAjax({
                type: 'GET',
                data: {
                  id: projectID
                },
                success: function (response) {
                    $(response.view).hide().appendTo("#thumbnail").fadeIn(500);
                }
            });
        });*/

        $('#save-link').click(function () {
            $.easyAjax({
                url: '{{route('admin.manage-files.storeLink')}}',
                container: '#file-external-link',
                type: "POST",
                redirect: true,
                data: $('#file-external-link').serialize(),
                success: function () {
                    window.location.reload();
                }
            })
        });

        $('#list-tabs').on("shown.bs.tab",function(event){
            var tabSwitch = $('#list').hasClass('active');
            if(tabSwitch == true) {
                $('#view').val('list');
            } else {
                $('#view').val('thumbnail');
            }
        });
        $('ul.showProjectTabs .projectFiles').addClass('tab-current');
    </script>
@endpush
