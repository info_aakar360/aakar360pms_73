@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }
    </style>
@endpush
@section('content')

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <section>
                    <div class="sttabs tabs-style-line">

                        <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.accounts_report_menu')
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="header">
            <h2 class="text-center">Notes</h2>
            <br>
        </div>
        <div class="container-fluid">
            <!-- Inline Layout | With Floating Label -->
            <div class="row clearfix">
                <!-- Ledger Type Wise Start -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 white-box">
                    <div class="card">
                        <div class="card-body">
                            <div class="header bg-cyan">
                                <h2>
                                    LEDGER TYPE WISE
                                    <small>Anything is select show all</small>
                                </h2>
                            </div>
                            <br>
                            <div class="body">
                                <div class="row clearfix">
                                    <form class="form" id="form_validation" method="post" action="{{ route('admin.reports.accounts.notes.type_wise.report')  }}">
                                        {{ csrf_field() }}
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <select data-live-search="true" class="form-control show-tick"
                                                            name="income_expense_type_code">
                                                        <option value="0">Select Ledger Type Name</option>
                                                        @foreach( App\IncomeExpenseType::all() as $Type )
                                                            <option value="{{ $Type->code  }}">{{ $Type->name  }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <select data-live-search="true" class="form-control show-tick"
                                                            name="branch_id">
                                                        <option value="0">Select Branch Name</option>
                                                        @if (App\Branch::all()->count() >0 )
                                                            @foreach( App\Branch::all() as $Branch )
                                                                <option value="{{ $Branch->id  }}">{{ $Branch->name  }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h2 class="card-inside-title">Period</h2>
                                            <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                                <div class="form-line">
                                                    <input autocomplete="off" name="start_from" type="text"
                                                           class="form-control" id="start_date_ledger"
                                                           placeholder="Date start...">
                                                </div>
                                                <span class="input-group-addon">to</span>
                                                <div class="form-line">
                                                    <input autocomplete="off" name="start_to" type="text"
                                                           class="form-control" id="end_date_ledger"
                                                           placeholder="Date end...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h2 class="card-inside-title">Period</h2>
                                            <div class="input-daterange input-group" id="bs_datepicker_range_container1">
                                                <div class="form-line">
                                                    <input autocomplete="off" name="end_from" type="text"
                                                           class="form-control" id="end_from_ledger"
                                                           placeholder="Date start...">
                                                </div>
                                                <span class="input-group-addon">to</span>
                                                <div class="form-line">
                                                    <input autocomplete="off" name="end_to" type="text" class="form-control"
                                                        id="end_to_ledger"   placeholder="Date end...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-line">
                                                <input formtarget="_blank" name="action" value="Show" type="submit"
                                                       class="btn btn-primary m-t-15 waves-effect">
                                                <input name="action" value="Pdf" type="submit"
                                                       class="btn btn-primary m-t-15 waves-effect">
                                                <input name="action" value="Excel" type="submit"
                                                       class="btn btn-primary m-t-15 waves-effect">
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ledger Type End -->

                <!-- Ledger Group Start -->
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 white-box">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                LEDGER GROUP WISE
                                <small>Anything is select show all</small>
                            </h2>
                        </div>
                        <br>
                        <div class="body">
                            <div class="row clearfix">
                                <form class="form1"  method="post"
                                      action="{{ route('admin.reports.accounts.notes.group_wise.report')  }}">
                                    {{ csrf_field() }}
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick" name="income_expense_group_code">
                                                    <option value="0">Select Ledger Group Name</option>
                                                    @foreach( App\IncomeExpenseGroup::all() as $Group )
                                                        <option value="{{ $Group->code  }}">{{ $Group->name  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick" name="branch_id">
                                                    <option value="0">Select Branch Name</option>
                                                    @if (App\Branch::all()->count() >0 )
                                                        @foreach( App\Branch::all() as $Branch )
                                                            <option value="{{ $Branch->id  }}">{{ $Branch->name  }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Period</h2>
                                        <div class="input-daterange input-group" id="bs_datepicker_range_container2">
                                            <div class="form-line">
                                                <input autocomplete="off" name="start_from1" type="text"
                                                       class="form-control" id="start_date_group"
                                                       placeholder="Date start...">
                                            </div>
                                            <span class="input-group-addon">to</span>
                                            <div class="form-line">
                                                <input autocomplete="off" name="start_to1" type="text"
                                                       class="form-control" id="end_date_group"
                                                       placeholder="Date end...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Period</h2>
                                        <div class="input-daterange input-group" id="bs_datepicker_range_container3">
                                            <div class="form-line">
                                                <input autocomplete="off" name="end_from1" type="text"
                                                       class="form-control" id="end_from_group"
                                                       placeholder="Date start...">
                                            </div>
                                            <span class="input-group-addon">to</span>
                                            <div class="form-line">
                                                <input autocomplete="off" name="end_to1" type="text" class="form-control"
                                                       id="end_to_group"  placeholder="Date end...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-line">
                                            <input formtarget="_blank" name="action" value="Show" type="submit"
                                                   class="btn btn-primary m-t-15 waves-effect">
                                            <input name="action" value="Pdf" type="submit"
                                                   class="btn btn-primary m-t-15 waves-effect">
                                            <input name="action" value="Excel" type="submit"
                                                   class="btn btn-primary m-t-15 waves-effect">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ledger Group End -->
            </div>
        </div>
    </section>
@stop
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $("#start_date_ledger").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_date_ledger").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#start_date_group").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_date_group").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_from_ledger").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_to_ledger").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_from_group").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $("#end_to_group").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

    </script>
@endpush
