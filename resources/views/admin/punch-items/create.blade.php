@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.issue.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a, .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.punch_items.newItem')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storeTask','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.title') </label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="selectpicker form-control" name="assign_to" id="user_id">
                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open">Open</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Inprogress">Inprogress</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type</label>
                                        <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                            <option value="">Please Select type</option>
                                            <option value="Architect">Architect</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Owner">Owner</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" id="location" name="location" class="form-control" >
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <br>
                                    <div class="form-group">
                                        <label for="privete">Private or Public</label><br>
                                        <input id="privete" name="private" value="1" type="checkbox">
                                        <label for="privete">Private</label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.startDate')</label>
                                        <input type="text" name="start_date"  id="start_date2" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Priority</label>
                                        <select class="selectpicker form-control" name="priority" data-style="form-control" required>
                                            <option value="">Please Select Priority</option>
                                            <option value="High">High</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Low">Low</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Reference
                                        </label>
                                        <input type="text" name="reference" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Schedule Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control schimpact" name="schimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                                <option value="TBD">TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 schimpactblock" style="display: none;">
                                            <input type="text" name="schimpact_days" class="form-control" placeholder="Impact days *" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Cost Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control costimpact" name="costimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                                <option value="TBD">TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 costimpactblock" style="display: none;">
                                            <input type="text" name="costimpact_days" class="form-control" placeholder="Cost Impact days *" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="project_id" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <?php  if(in_array('sub_projects', $user->modules)){?>
                                <div class="col-sm-4 col-xs-4" id="subprojectblock" style="display: none;">
                                <div class="form-group">
                                        <label for="title_id">Sub Project</label>
                                        <select class="select2 form-control titlelist" id="subprojectlist" name="subproject_id" data-style="form-control" required>
                                            <option value="">Please Select Sub Project</option>
                                        </select>
                                    </div>
                                </div>
                                <?php }?>

                                <?php  if(in_array('segment', $user->modules)){?>
                                    <div class="col-sm-4 col-xs-4 segmentblock"  style="display: none;">
                                        <div class="form-group">
                                            <label for="title_id">@lang('app.segment')</label>
                                            <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.segment')</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php }?>

                                <div class="col-sm-4 col-xs-4" >
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.activity')</label>
                                        <select class="select2 form-control activitylist" id="activitylist" name="activity" data-style="form-control" required>
                                            <option value="">Select  @lang('app.activity')</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-4" >
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.task')</label>
                                        <select class="select2 form-control costitemlist" id="tasklist" name="costitem" data-style="form-control" required>
                                            <option value="">Select  @lang('app.task')</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.description')</label>
                                        <textarea id="description" name="description" class="form-control summernote"></textarea>
                                    </div>
                                </div>

                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone dropheight"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url" type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="taskID" id="taskID">
                                    </div>
                                </div>

                            </div>
                            <!--/row-->
                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="store-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


<script>
    $(".schimpact").change(function () {
        var schimpact = $(this).val();
        if(schimpact=='yes'){
            $(".schimpactblock").show();
            $(".schimpactblock").attr('required','required');
        }else{
            $(".schimpactblock").hide();
            $(".schimpactblock").removeAttr('required','required');
        }
    });
    $(".costimpact").change(function () {
        var costimpact = $(this).val();
        if(costimpact=='yes'){
            $(".costimpactblock").show();
            $(".costimpactblock").attr('required','required');
        }else{
            $(".costimpactblock").hide();
            $(".costimpactblock").removeAttr('required','required');
        }
    });
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.issue.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });
    myDropzone.on('sending', function(file, xhr, formData) {
        var ids = $('#taskID').val();
        formData.append('task_id', ids);
    });

    myDropzone.on('completemultiple', function () {

        var msgs = "Issue updated successfully";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.issue.index') }}'

    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    //    update task
    $('#store-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.issue.store')}}',
            container: '#storeTask',
            type: "POST",
            data: $('#storeTask').serialize(),
            success: function (data) {
                $('#storeTask').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    taskID = data.taskID;
                    $('#taskID').val(data.taskID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('messages.issueCreated')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.issue.index') }}'
                }
            }
        })
    });

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('#project_id').change(function () {
        var id = $(this).val();
        var token = '{{ csrf_token() }}';
        $.ajax({
            type: "POST",
            url: "{{ route('admin.projects.subprojectoptions') }}",
            data: {'_token': token,'projectid': id},
            success: function(data){
                $("#subprojectblock").hide();
                $("#segmentsblock").hide();
                $("select#tasklist").html("");
                $("select#subprojectlist").html("");
                $("select#activitylist").html("");
                $("select#segmentslist").html("");
                if(data.segmentslist){
                    $("#segmentsblock").show();
                    $("select#segmentslist").html(data.segmentslist);
                    $('select#segmentslist').select2();
                }
                if(data.subprojectlist){
                    $("#subprojectblock").show();
                    $("select#subprojectlist").html(data.subprojectlist);
                    $('select#subprojectlist').select2();
                }
                if(data.activitylist){
                    $("select#activitylist").html(data.activitylist);
                    $('select#activitylist').select2();
                }
                if(data.tasklist){
                    $("select#tasklist").html(data.tasklist);
                    $('select#tasklist').select2();
                }
            }
        });

    });
    $('#subprojectlist').change(function () {
        var subprojecid = $(this).val();
        var id = $('#project_id').val();
        var token = '{{ csrf_token() }}';
        $.ajax({
            type: "POST",
            url: "{{ route('admin.projects.subprojectoptions') }}",
            data: {'_token': token,'projectid': id,'subprojectid': subprojecid},
            success: function(data){
                $("#segmentsblock").hide();
                $("select#activitylist").html("");
                $("select#segmentslist").html("");
                $("select#tasklist").html("");
                if(data.segmentslist){
                    $("#segmentsblock").show();
                    $("select#segmentslist").html(data.segmentslist);
                    $('select#segmentslist').select2();
                }
                if(data.activitylist){
                    $("select#activitylist").html(data.activitylist);
                    $('select#activitylist').select2();
                }
                if(data.tasklist){
                    $("select#tasklist").html(data.tasklist);
                    $('select#tasklist').select2();
                }
            }
        });

    });
    $("#activitylist").change(function () {
        var project = $("#project_id").select2().val();
        var titlelist = $("#subprojectlist").val();
        var activity = $(this).val();
        if(project){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': project,'subprojectid': titlelist,'activity': activity},
                success: function(data){
                    if(data.tasklist){
                        $("select#tasklist").html("");
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });
        }
    });
    $('#repeat-task').change(function () {
        if($(this).is(':checked')){
            $('#repeat-fields').show();
        }
        else{
            $('#repeat-fields').hide();
        }
    })

    $('#dependent-task').change(function () {
        if($(this).is(':checked')){
            $('#dependent-fields').show();
        }
        else{
            $('#dependent-fields').hide();
        }
    })
    $('#createTaskCategory').click(function(){
        var url = '{{ route('admin.taskCategory.create-cat')}}';
        $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
        $.ajaxModal('#taskCategoryModal', url);
    })
</script>
@endpush

