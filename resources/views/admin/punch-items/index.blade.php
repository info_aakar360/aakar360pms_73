@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<style>
    .swal-footer {
        text-align: center !important;
    }
</style>
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title m-t-30">@lang('app.date')</h5>
                <div class="input-daterange" id="date-range">
                    <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.date')"   />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title">  @lang('app.users') </h5>
                <select class="select2 form-control" name="users" data-placeholder="@lang('app.users')" id="selectuser">
                    <option value=""> @lang('app.users')</option>
                    @foreach($employees as $employe)
                        <option value="{{ $employe->id }}">{{ ucwords($employe->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title">  @lang('app.projects') </h5>
                <select class="select2 form-control" name="project" data-placeholder="@lang('app.projects')" id="selectproject">
                    <option value=""> @lang('app.projects')</option>
                    @foreach($projectlist as $project)
                        <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.status')</label>
                <select class="selectpicker form-control" name="status" data-style="form-control"  id="status">
                    <option value="">Please Status</option>
                    <option value="open" >Open</option>
                    <option value="initiated" >Initiated</option>
                    <option value="completed" >Completed</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <h5 class="box-title m-t-10"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12  ">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-11">
                        <div class="form-group">
                            <h2 style="color: #002f76">@lang('app.menu.issue')</h2>
                            <a href="{{ route('admin.issue.create') }}" class="btn btn-outline btn-success btn-sm">@lang('modules.punch_items.newItem') <i class="fa fa-plus" aria-hidden="true"></i></a>

                        </div>
                    </div>
                    <div class="col-sm-1 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>@lang('app.sno')</th>
                                <th>@lang('app.title')</th>
                                <th>@lang('app.project')</th>
                                <th>@lang('modules.tasks.assignTo')</th>
                                <th>@lang('app.type')</th>
                                <th>Distribution</th>
                                <th>Location</th>
                                <th>Public or Private</th>
                                <th>@lang('app.dueDate')</th>
                                <th>Priority</th>
                                <th>Reference</th>
                                <th>@lang('app.status')</th>
                                <th>Added By</th>
                                <th>@lang('app.action')</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
            </div>
        </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Sub Task e</span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@if($global->locale == 'en')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
@else
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
@endif
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script>

    loadTable();
    function loadTable(){

        var user   = $('#selectuser').val();
        var project   = $('#selectproject').val();
        var selectdate     = $('#start-date').val();
        var status     = $('#status').val();

        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.issue.issueData') !!}?project=' + project+ '&user=' + user+ '&selectdate=' + selectdate+ '&status=' + status,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'title', name: 'title' },
                { data: 'projectid', name: 'projectid' },
                { data: 'assign_to', name: 'assign_to'},
                { data: 'type', name: 'type'},
                { data: 'distribution', name: 'distribution'},
                { data: 'location', name: 'location'},
                { data: 'private', name: 'private'},
                { data: 'due_date', name: 'due_date'},
                { data: 'priority', name: 'priority'},
                { data: 'reference', name: 'reference'},
                { data: 'status', name: 'status'},
                { data: 'added_by', name: 'added_by'},
                { data: 'action', name: 'action', width: '15%' }
            ]
        });
    }
    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });
    $('#filter-results').click(function () {
        loadTable();
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        loadTable();
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $('body').on('click', '.sa-params', function(){

        var id = $(this).data('task-id');
        var recurring = $(this).data('recurring');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted Cost item!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.issue.destroy',':id') }}";
                url = url.replace(':id', id);
                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });

    $('#createTaskCategory').click(function(){
        var url = '{{ route('admin.taskCategory.create')}}';
        $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
        $.ajaxModal('#taskCategoryModal',url);
    })
    function exportData(){

        var startDate = $('#start-date').val();

        if (startDate == '') {
            startDate = null;
        }

        var endDate = $('#end-date').val();

        if (endDate == '') {
            endDate = null;
        }

        var projectID = $('#project_id').val();
        if (!projectID) {
            projectID = 0;
        }

        if ($('#hide-completed-tasks').is(':checked')) {
            var hideCompleted = '1';
        } else {
            var hideCompleted = '0';
        }

        var url = '{!!  route('admin.issue.export', [':startDate', ':endDate', ':projectId', ':hideCompleted']) !!}';

        url = url.replace(':startDate', startDate);
        url = url.replace(':endDate', endDate);
        url = url.replace(':hideCompleted', hideCompleted);
        url = url.replace(':projectId', projectID);

        window.location.href = url;
    }
</script>
@endpush