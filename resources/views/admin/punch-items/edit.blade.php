@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.issue.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush
@section('content')

        <div class="row ">

            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading"> @lang('modules.punch_items.updateTask')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'PUT']) !!}
                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.title') </label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" value="{{ $punchitem->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="select2 form-control" name="assign_to" id="user_id" >
                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}"
                                                <?php if ($employee->user_id==$punchitem->assign_to){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <?php if($punchitem->status == 'Open'){ ?>
                                            <option value="Open" selected>Open</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Inprogress">Inprogress</option>
                                            <?php } elseif($punchitem->status == 'Resolved'){?>
                                            <option value="Open">Open</option>
                                            <option value="Resolved" selected>Resolved</option>
                                            <option value="Inprogress">Inprogress</option>
                                            <?php } else { ?>
                                            <option value="Open">Open</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Inprogress" selected>Inprogress</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type</label>
                                        <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                            <option value="">Please Select type</option>
                                            <option value="Architect" <?php if ('Architect'==$punchitem->type){ echo 'selected'; } ?>>Architect</option>
                                            <option value="Contractor" <?php if ('Contractor'==$punchitem->type){ echo 'selected'; } ?>>Contractor</option>
                                            <option value="Owner" <?php if ('Owner'==$punchitem->type){ echo 'selected'; } ?>>Owner</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}"
                                                <?php
                                                    $ass = explode(',',$punchitem->distribution);
                                                    if (in_array($employee->user_id, $ass)){
                                                        echo 'selected'; } ?>
                                                >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location <i class="fa fa-map-marker" aria-hidden="true"></i></label>
                                        <input type="text" id="location" name="location" class="form-control" value="{{ $punchitem->location }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <br>
                                    <div class="form-group">
                                        <label for="privete">Private or Public</label><br>
                                        <input id="privete" name="private" value="1" type="checkbox" <?php if($punchitem->private == '1'){ echo 'checked';}?>>
                                        <label for="privete">Private</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.startDate') <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        <?php
                                        $ex = explode('-',$punchitem->start_date);
                                        ?>
                                        <input type="text" name="start_date"  id="start_date2" class="form-control" value="{{ $ex[2].'-'.$ex[1].'-'.$ex[0] }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate') <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                        <?php
                                        $exd = explode('-',$punchitem->due_date);
                                        ?>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" value="{{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}" autocomplete="off">
                                    </div>
                                </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Priority</label>
                                            <select class="selectpicker form-control" name="priority" data-style="form-control" required>
                                                <option value="">Please Select Priority</option>
                                                <option value="High"  <?php if ('High'==$punchitem->priority){ echo 'selected'; } ?> >High</option>
                                                <option value="Medium"  <?php if ('Medium'==$punchitem->priority){ echo 'selected'; } ?>>Medium</option>
                                                <option value="Low"  <?php if ('Low'==$punchitem->priority){ echo 'selected'; } ?>>Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label class="control-label">Reference
                                            </label>
                                            <input type="text" name="reference" class="form-control" value="{{ $punchitem->reference }}">
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Schedule Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control schimpact" name="schimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes" @if($punchitem->schimpact=='yes') selected @endif>Yes</option>
                                                <option value="no" @if($punchitem->schimpact=='no') selected @endif>No</option>
                                                <option value="TBD" @if($punchitem->schimpact=='TBD') selected @endif>TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 schimpactblock" style="display:  @if($punchitem->schimpact=='yes') block @else none @endif;">
                                            <input type="text" name="schimpact_days" class="form-control" value="{{ $punchitem->schimpact_days }}" placeholder="Impact days *" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Cost Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control costimpact" name="costimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes" @if($punchitem->costimpact=='yes') selected @endif>Yes</option>
                                                <option value="no" @if($punchitem->costimpact=='no') selected @endif>No</option>
                                                <option value="TBD" @if($punchitem->costimpact=='TBD') selected @endif>TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 costimpactblock"  style="display:  @if($punchitem->costimpact=='yes') block @else none @endif;">
                                            <input type="text" name="costimpact_days" class="form-control" value="{{ $punchitem->costimpact_days }}" placeholder="Cost Impact days *" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="project_id" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" @if($punchitem->projectid==$project->id) selected @endif >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <?php  if(in_array('sub_projects', $user->modules)) {?>
                                @if(count($titlelist)>0)
                                    <div class="col-sm-4 col-xs-4" id="subprojectblock" style="display: <?php if(empty($punchitem->titleid)){ echo 'none';}else{ echo 'block';}?>;">
                                    <div class="form-group">
                                            <label for="title_id">Sub Project</label>
                                            <select class="select2 form-control titlelist" id="subprojectlist" name="subproject_id" data-style="form-control" required>
                                                <option value="">Select Sub Project</option>
                                                    @foreach($titlelist as $title)
                                                        <option value="{{ $title->id }}" <?php if($punchitem->titleid==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <?php }?>
                                <?php  if(in_array('segment', $user->modules)){?>
                                    @if($titlelist)
                                        <div class="col-sm-4 col-xs-4" id="segmentblock"   style="display: <?php if(empty($punchitem->segmentid)){ echo 'none';}else{ echo 'block';}?>;">
                                            <div class="form-group">
                                                <label for="title_id">@lang('app.segment')</label>
                                                <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                                    <option value="">Select @lang('app.segment')</option>
                                                    @foreach($segmentlist as $segment)
                                                        <option value="{{ $segment->id }}" <?php if($punchitem->segmentid==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                <?php }?>
                                <div class="col-sm-4 col-xs-4" >
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.activity')</label>
                                        <select class="select2 form-control activitylist" id="activitylist" name="activity" data-style="form-control" required>
                                            <option value="">Select  @lang('app.activity')</option>
                                            @foreach($activitiesarray as $activities)
                                                <option value="{{ $activities->id }}"  @if($punchitem->activity_id==$activities->id) selected @endif >{{ ucwords($activities->itemname) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">Task</label>
                                        <select class="select2 form-control costitemlist" id="tasklist" name="costitem" data-style="form-control" required>
                                            <option value="">Please Select Task</option>
                                            @if($costitemlist)
                                                @foreach($costitemlist as $costitem => $costitemname)
                                                    <option value="{{ $costitem }}" <?php if($punchitem->costitemid==$costitem){ echo 'selected';}?>>{{ ucwords($costitemname) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="row  mb-20" >
                                    @forelse($files as $file)
                                        <?php if(file_exists(public_path('/uploads/punch-files/'.$punchitem->id.'/'.$file->hashname))){?>
                                        <div class="col-md-2 form-group full-image">
                                            <?php if($file->external_link != ''){
                                                $imgurl = $file->external_link;
                                            }elseif($storage == 'local'){
                                                    $imgurl = uploads_url().'/punch-files/'.$punchitem->id.'/'.$file->hashname;
                                            }elseif($storage == 's3'){
                                                    $imgurl = awsurl().'/punch-files/'.$punchitem->id.'/'.$file->hashname;
                                            }elseif($storage == 'google'){
                                                $imgurl = $file->google_url;
                                            }elseif($storage == 'dropbox'){
                                                $imgurl = $file->dropbox_link;
                                            }
                                            ?>
                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                            <p><span><?php echo date('d M Y',strtotime($file->created_at));?></span></p>
                                        </div>
                                        <?php }?>
                                    @empty
                                        <div class="col-md-2">
                                            @lang('messages.noFileUploaded')
                                        </div>
                                    @endforelse
                                </div>

                                <div class="col-md-12">&nbsp;</div>
                                <!--/span-->
                                <div class="row m-b-20">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <textarea id="description" name="description" class="form-control summernote border-radius18">{{ $punchitem->description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url"type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="taskID" id="taskID">
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="update-task" class="btn btn-success btn-width150"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>


    <script>

        $(".schimpact").change(function () {
            var schimpact = $(this).val();
            if(schimpact=='yes'){
                $(".schimpactblock").show();
                $(".schimpactblock").attr('required','required');
            }else{
                $(".schimpactblock").hide();
                $(".schimpactblock").removeAttr('required','required');
            }
        });
        $(".costimpact").change(function () {
            var costimpact = $(this).val();
            if(costimpact=='yes'){
                $(".costimpactblock").show();
                $(".costimpactblock").attr('required','required');
            }else{
                $(".costimpactblock").hide();
                $(".costimpactblock").removeAttr('required','required');
            }
        });
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.issue.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = '{{ $punchitem->id }}';
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Issue updated successfully";
            $.showToastr(msgs, 'success');
          window.location.href = '{{ route('admin.issue.index') }}';

        });
        $('#project_id').change(function () {
            var id = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id},
                success: function(data){
                    $("#subprojectblock").hide();
                    $("#segmentsblock").hide();
                    $("select#subprojectlist").html("");
                    $("select#segmentslist").html("");
                    $("select#activitylist").html("");
                    $("select#tasklist").html("");
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.subprojectlist){
                        $("#subprojectblock").show();
                        $("select#subprojectlist").html(data.subprojectlist);
                        $('select#subprojectlist').select2();
                    }
                    if(data.activitylist){
                        $("select#activitylist").html(data.activitylist);
                        $('select#activitylist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });
        $('#subprojectlist').change(function () {
            var subprojecid = $(this).val();
            var id = $('#project_id').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id,'subprojectid': subprojecid},
                success: function(data){
                    $("#segmentsblock").hide();
                    $("select#activitylist").html("");
                    $("select#segmentslist").html("");
                    $("select#tasklist").html("");
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.activitylist){
                        $("select#activitylist").html(data.activitylist);
                        $('select#activitylist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });
        $("#activitylist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token,'projectid': project,'subprojectid': titlelist,'activity': activity},
                    success: function(data){
                        if(data.tasklist){
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        //    update task
        $('#update-task').click(function () {

            $.easyAjax({
                url: '{{route('admin.issue.update', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "Issue updated successfully";
                        $.showToastr(msgs, 'success');
                      window.location.href = '{{ route('admin.issue.index') }}';
                    }
                }
            })

        });

        //    update task
        function removeFile(id) {
            var url = "{{ route('admin.issue.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                         $("#fileid"+id).remove();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('admin.issue.update', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.taskCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.issue.index') }}'
                    }
                }
            })
        }

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.issue.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#list ul.list-group').html(response.html);

                            }
                        }
                    });
                }
            });
        });


        $('#project_id').change(function () {
            var id = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': id},
                success: function(data){
                    $("#subprojectblock").hide();
                    $("#segmentsblock").hide();
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html("");
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.subprojectlist){
                        $("#subprojectblock").show();
                        $("select#subprojectlist").html("");
                        $("select#subprojectlist").html(data.subprojectlist);
                        $('select#subprojectlist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html("");
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });
        $('#subprojectlist').change(function () {
            var projecid = $(this).val();
            var id = $('#project_id').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.projects.subprojectoptions') }}",
                data: {'_token': token,'projectid': projecid,'subprojectid': id},
                success: function(data){
                    $("#segmentsblock").hide();
                    if(data.segmentslist){
                        $("#segmentsblock").show();
                        $("select#segmentslist").html("");
                        $("select#segmentslist").html(data.segmentslist);
                        $('select#segmentslist').select2();
                    }
                    if(data.tasklist){
                        $("select#tasklist").html("");
                        $("select#tasklist").html(data.tasklist);
                        $('select#tasklist').select2();
                    }
                }
            });

        });

    </script>
    <script>
        $('#createTaskCategory').click(function(){
            var url = '{{ route('admin.taskCategory.create-cat')}}';
            $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
            $.ajaxModal('#taskCategoryModal', url);
        })

    </script>

@endpush
