@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h2 style="color: #002f76">@lang('app.menu.labourattendance')</h2>

                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['id'=>'logTime','class'=>'ajax-form','method'=>'POST', 'enctype'=>'multipart/form-data']) !!}
                        <div class="form-body">
                            <div class="row m-t-30">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            @lang('app.contractors')
                                        </label>
                                        <select class="select2 form-control" name="contractor"
                                                data-placeholder="@lang('modules.labourAttendance.selectContractor')"
                                                id="contractor2">
                                            <option value="">Select Contractor</option>
                                            @foreach($contractorsarray as $contractor)
                                                <option value="{{ $contractor->user_id }}">{{ ucwords($contractor->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.workdate')</label>
                                        <input name="workdate" type="text" class="form-control "
                                               value="{{ date('d-m-Y') }}" id="start_date"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.shift')</label>
                                        <select class="form-control" name="shift"  data-style="form-control">
                                            <option value="">Select Shift</option>
                                            <option value="day">Day</option>
                                            <option value="night">Night</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.selectProject')</label>
                                        <select class="form-control" name="project_id" data-placeholder="@lang('modules.labourAttendance.selectProject')" id="project_id">
                                            <option value="">Select Project</option>
                                            @foreach($projectarray as $project)
                                                <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <?php  if(in_array('sub_projects', $user->modules)) {?>
                                <div class="col-md-4" id="subprojectblock" style="display: none">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.subproject')</label>
                                        <select class="form-control" name="title_id" id="subprojectlist"
                                                data-style="form-control">
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <?php  if(in_array('segments', $user->modules)) {?>

                                <div class="col-md-4" id="segmentsblock" style="display: none">
                                    <div class="form-group">
                                        <label>@lang('app.segment')</label>
                                        <select class="form-control" name="segment_id" id="segmentlist" data-style="form-control"></select>
                                    </div>
                                </div>
                                <?php }?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('app.activity')</label>
                                        <select class="form-control" name="activity" id="activitylist" data-style="form-control">
                                            <option value="">Select Activity</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.projecttask')</label>
                                        <select class="form-control" name="costitem" id="tasklist" data-style="form-control">
                                            <option value="">Select Task</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>  @lang('app.labourattendancecategory')</label>
                                        <select class="form-control" name="category[category][]"
                                                data-placeholder="@lang('modules.labourAttendance.selectCategory')"
                                                id="contractor2">
                                            <option value="">Select Category</option>
                                            @foreach($manpowercategoryarray as $manpowercategory)
                                                <option value="{{ $manpowercategory->id }}">{{ ucwords($manpowercategory->title) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.clockin')</label>
                                        <input name="category[clockin][]" type="text"  class="form-control timepicker" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.clockout')</label>
                                        <input name="category[clockout][]" type="text" class="form-control timepicker" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.noofworkers')</label>
                                        <input name="category[noofworkers][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.salaryperday')</label>
                                        <input  name="category[salaryperday][]"   type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.hourspershift')</label>
                                        <input name="category[hourspershift][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.workinghours')</label>
                                        <input name="category[workinghours][]"  type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.totalprice')</label>
                                        <input name="category[totalprice][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <br/>
                                    <a href="javascript:void(0);" class="btn btn-danger removerow">Remove Row</a>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>  @lang('app.labourattendancecategory')</label>
                                        <select class="form-control" name="category[category][]"
                                                data-placeholder="@lang('modules.labourAttendance.selectCategory')"
                                                id="contractor2">
                                            <option value="">Select Category</option>
                                            @foreach($manpowercategoryarray as $manpowercategory)
                                                <option value="{{ $manpowercategory->id }}">{{ ucwords($manpowercategory->title) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.clockin')</label>
                                        <input name="category[clockin][]" type="text"  class="form-control timepicker" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.clockout')</label>
                                        <input name="category[clockout][]" type="text" class="form-control timepicker" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.noofworkers')</label>
                                        <input name="category[noofworkers][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.salaryperday')</label>
                                        <input  name="category[salaryperday][]"   type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.hourspershift')</label>
                                        <input name="category[hourspershift][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.workinghours')</label>
                                        <input name="category[workinghours][]"  type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.totalprice')</label>
                                        <input name="category[totalprice][]" type="text" class="form-control" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <br/>
                                    <a href="javascript:void(0);" class="btn btn-danger removerow">Remove Row</a>
                                </div>
                            <a href="javascript:void(0);" class="btn btn-primary addrow">Add Row</a>
                            </div>
                            <div id="rowlist">

                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="memo">Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-md-12">
                                <button type="button"
                                        class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button"
                                        style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File
                                    Select Or Upload
                                </button>
                                <div id="file-upload-box">
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone dropheight"
                                                 id="file-upload-dropzone">
                                                {{ csrf_field() }}
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>
                                                <input name="image_url" id="image_url" type="hidden"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="manpowerID" id="manpowerID">
                            </div>
                        </div>
                        <div class="form-actions m-t-30 text-right">
                            <button type="button" id="save-form" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.save')</button>
                            <a href="{{ route('admin.man-power-logs.index') }}" class="btn btn-danger">Go Back</a>
                        </div>
                        {!! Form::close() !!}
                        <hr>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.man-power-logs.storeImage') }}",
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: '.png, .jpeg, .jpg, .pdf, .docx, .csv, .xlsx',
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks: true,
            parallelUploads: 10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function (file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length, 'sending');
            var ids = $('#manpowerID').val();
            formData.append('manpower_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Log Updated Successfully";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.man-power-logs.index') }}'

        });
        $('#employeeBox').hide();

        $('.addrow').click(function () {
            var count = 1;
            $.ajax({
                url: '{{route('admin.man-power-logs.addrow')}}',
                type: "POST",
                data: { '_token':'{{ csrf_token() }}','count':count },
                success: function (data) {
                    $("#rowlist").append(data);
                }
            });
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.man-power-logs.store')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if (myDropzone.getQueuedFiles().length > 0) {
                        manpowerID = data.manpowerID;
                        $('#manpowerID').val(data.manpowerID);
                        myDropzone.processQueue();
                    }
                    else {
                        var msgs = "Log Updated Successfully";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.man-power-logs.index') }}'
                    }
                }
            });
        });

        $('#project_id').change(function () {
            var project = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project},
                    success: function (data) {
                        $("#subprojectblock").hide();
                        $("#segmentsblock").hide();
                        $("select#tasklist").html("");
                        $("select#segmentlist").html("");
                        $("select#subprojectlist").html("");
                        $("select#activitylist").html("");
                        if (data.subprojectlist) {
                            $("#subprojectblock").show();
                            $("select#subprojectlist").html(data.subprojectlist);
                            $('select#subprojectlist').select2();
                        }
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $("#subprojectlist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist},
                    success: function (data) {
                        $("#segmentsblock").hide();
                        $("select#segmentlist").html("");
                        $("select#segmentlist").html("");
                        $("select#tasklist").html("");
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $("#activitylist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist, 'activity': activity},
                    success: function (data) {
                        if (data.tasklist) {
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $('#show-add-form').click(function () {
            $('#hideShowTimeLogForm').toggleClass('hide', 'show');
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        jQuery('#date-range').datepicker({
            toggleActive: true,
            weekStart: '{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        var table;

        function showTable() {


            var projectID = $('#project_id').val();
            var employee = $('#employee').val();
            var url = '{{ route('admin.man-power-logs.data') }}?_token={{ csrf_token() }}';
            url = url.replace(':projectId', projectID);
            url = url.replace(':employee', employee);

            table = $('#timelog-table').dataTable({
                destroy: true,
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: {
                    "url": url,
                    "type": "POST",
                    "data": {
                        employee: employee,
                        projectId: projectID
                    }
                },
                deferRender: true,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "order": [[0, "desc"]],
                columns: [
                    {data: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'contractor', name: 'contractor'},
                    {data: 'project_name', name: 'projects.project_name'},
                    {data: 'name', name: 'users.name'},
                    {data: 'manpower', name: 'manpower'},
                    {data: 'workinghours', name: 'workinghours'},
                    {data: 'description', name: 'description'},
                    {data: 'files', name: 'files'},
                    {data: 'action', name: 'action', "searchable": false}
                ]
            });
        }

        $('#filter-results').click(function () {
            showTable();
        });


        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('time-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted time log!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.man-power-logs.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        showTable();


        $('body').on('click', '.edit-time-log', function () {
            var id = $(this).data('time-id');

            var url = '{{ route('admin.man-power-logs.edit', ':id')}}';
            url = url.replace(':id', id);

            $('#modelHeading').html('Update Time Log');
            $.ajaxModal('#editTimeLogModal', url);

        });

        function exportTimeLog() {

            var projectID = $('#project_id').val();
            var employee = $('#employee').val();

            var url = '{{ route('admin.man-power-logs.export', [':startDate', ':endDate', ':projectId', ':employee']) }}';
            url = url.replace(':projectId', projectID);
            url = url.replace(':employee', employee);

            window.location.href = url;
        }

        $('#start_time, #end_time').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false
            @endif
        }).on('hide.timepicker', function (e) {
            calculateTime();
        });

        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart: '{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        }).on('hide', function (e) {
            calculateTime();
        });

        function calculateTime() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();
            var startTime = $("#start_time").val();
            var endTime = $("#end_time").val();

            var timeStart = new Date(startDate + " " + startTime);
            var timeEnd = new Date(endDate + " " + endTime);

            var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds

            var minutes = diff % 60;
            var hours = (diff - minutes) / 60;

            if (hours < 0 || minutes < 0) {
                var numberOfDaysToAdd = 1;
                timeEnd.setDate(timeEnd.getDate() + numberOfDaysToAdd);
                var dd = timeEnd.getDate();

                if (dd < 10) {
                    dd = "0" + dd;
                }

                var mm = timeEnd.getMonth() + 1;

                if (mm < 10) {
                    mm = "0" + mm;
                }

                var y = timeEnd.getFullYear();

                $('#end_date').val(mm + '/' + dd + '/' + y);
                calculateTime();
            } else {
                $('#total_time').html(hours + "Hrs " + minutes + "Mins");
            }

//        console.log(hours+" "+minutes);
        }
    </script>
@endpush