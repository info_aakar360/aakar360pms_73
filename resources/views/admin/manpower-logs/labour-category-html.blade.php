<div class="row onetimelabours">
    <div class="form-body ">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <p>{{ get_manpower_category($manpowerattendance->manpower_category) }}</p>

                            <p>{{ $manpowerattendance->salary }} Per {{ $manpowerattendance->salarytype }} ({{ $manpowerattendance->workinghours }}) hr</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <label class="switch">
                            <input type="checkbox" name="labours[]"  value="{{ $count }}" class="labouronetime" ><div class="slider round"><span class="on"></span><span class="off"></span></div></label>
                        <div class="onetimemarkattendance{{ $count }}" style="display: none;">

                            <div class="col-md-4">
                                <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary"  onclick="callworkers('{{ $count }}','minus');"><i class="fa fa-minus"></i></a></div>
                                <div class="col-md-4"> <input type="text" class="form-control noofworkers{{ $count }}"  onchange="labourupdate({{ $count }});" name="noofworkers[{{ $count }}]" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->manpower; }else{ echo '1';}?>" /></div>
                                <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary" onclick="callworkers('{{ $count }}','plus');"><i class="fa fa-plus"></i></a></div>
                            </div>

                            <div class="col-md-2">
                                <select class="form-control" onchange="labourupdate({{ $count }});" name="shift{{ $count }}" >
                                    <option value="">Select Shift</option>
                                    <option value="Day" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Day'){ echo 'selected'; }?> >Day</option>
                                    <option value="Night" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Night'){ echo 'selected'; }?> >Night</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" onchange="labourupdate({{ $count }});" name="workinghours" value="{{ $manpowerattendance->workinghours }}" />
                            </div>
                            <div class="col-md-1">
                                <input type="hidden" class="workerattendance{{ $count }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>" />
                                <a href="javascript:void(0);"><i  style="font-size: 25px;" class="fa fa-chevron-circle-down"></i></a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<hr/>