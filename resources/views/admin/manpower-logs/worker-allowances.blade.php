<form method="post" action="" id="allowancesAttendances" autocomplete="off">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <span class="caption-subject font-red-sunglo bold uppercase" >@lang('app.allowances')</span>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label>@lang('app.overtime')</label>
                <div class="form-group">
                    <div class="col-md-4">
                    <input type="text" name="overtimehours" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.overtimehours')"  class="form-control overtimehours{{ $attendance->id }}" value="{{ $attendance->overtimehours }}" />
                    </div>
                    <div class="col-md-4">
                    <input type="text" name="overtimeamount" onChange="allowancescal('{{ $attendance->id }}');" placeholder="@lang('app.overtimeamount')"  class="form-control overtimeamount{{ $attendance->id }}" value="{{ $attendance->overtimeamount }}"  />
                    </div>
                    <div class="col-md-4">
                    <input type="text" name="overtimetotal" onChange="allowancescal('{{ $attendance->id }}');" placeholder="@lang('app.overtimetotal')"  class="form-control overtimetotal{{ $attendance->id }}" value="{{ $attendance->overtimetotal }}"  />
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label>@lang('app.latefine')</label>
                <div class="form-group">
                    <div class="col-md-4">
                    <input type="text" name="latefinehours" onChange="allowancescal('{{ $attendance->id }}');" placeholder="@lang('app.latefinehours')"  class="form-control latefinehours{{ $attendance->id }}"  value="{{ $attendance->latefinehours }}" />
                    </div>
                    <div class="col-md-4">
                    <input type="text" name="latefineamount" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.latefineamount')"  class="form-control latefineamount{{ $attendance->id }}"  value="{{ $attendance->latefineamount }}" />
                    </div>
                    <div class="col-md-4">
                    <input type="text" name="latefinetotal" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.latefinetotal')"  class="form-control latefinetotal{{ $attendance->id }}"  value="{{ $attendance->latefinetotal }}" />
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label>@lang('app.allowances')</label>
            </div>
            <div class="col-md-6">
                <a href="javascript:void(0);" onClick="addallowncesrow({{ $attendance->id }});" ><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div id="allowencesmodal">
                <?php
                $x=0;
                if(!empty($attendance->allowances)){
                    $allowencessarray = json_decode($attendance->allowances);
                    foreach($allowencessarray as $allowencess){ ?>
                        <div class="col-md-12 allowcount">
                            <div class="col-md-4 form-group">
                                <input type="text" name="allowances[{{ $x }}][name]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.name')"  class="form-control allowancesname{{ $attendance->id }}"  value="{{ !empty($allowencess->name) ? $allowencess->name : '' }}" />
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="text" name="allowances[{{ $x }}][amount]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.amount')"  class="form-control allowancesamount{{ $attendance->id }}"  value="{{ !empty($allowencess->amount) ? $allowencess->amount : '0' }}" />
                            </div>
                            <div class="col-md-4 form-group">
                                <a href="javascript:void(0);" class="removerow">Remove</a>
                            </div>
                        </div>
           <?php      $x++; }  } ?>
                <div class="col-md-12 allowcount">
                        <div class="col-md-4 form-group">
                        <input type="text" name="allowances[{{ $x }}][name]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.name')"  class="form-control allowancesname{{ $attendance->id }}"  value="" />
                        </div>
                        <div class="col-md-4 form-group">
                        <input type="text" name="allowances[{{ $x }}][amount]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.amount')"  class="form-control allowancesamount{{ $attendance->id }}"  value="0" />
                        </div>
                    <div class="col-md-4 form-group">
                        <a href="javascript:void(0);" class="removerow">Remove</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label>@lang('app.deductions')</label>
            </div>
            <div class="col-md-6">
                <a href="javascript:void(0);" onClick="adddeductionrow({{ $attendance->id }});" ><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div id="deductionmodal">
                <?php if(!empty($attendance->deductions)){
                $x=0;
                $deductionsarray = json_decode($attendance->deductions);
                foreach($deductionsarray as $deductions){  ?>
                <div class="col-md-12 deductcount">
                    <div class="col-md-4 form-group">
                        <input type="text" name="allowances[{{ $x }}][name]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.name')"  class="form-control allowancesname{{ $attendance->id }}"  value="{{ !empty($deductions->name) ? $deductions->name : '' }}" />
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="text" name="allowances[{{ $x }}][amount]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.amount')"  class="form-control allowancesamount{{ $attendance->id }}"  value="{{ !empty($deductions->amount) ? $deductions->amount : '0' }}" />
                    </div>
                    <div class="col-md-4 form-group">
                         <a href="javascript:void(0);" class="removerow">Remove</a>
                        </div>
                </div>
                    <?php      $x++; }  } ?>
                <div class="col-md-12 deductcount">
                    <div class="col-md-4 form-group">
                    <input type="text" name="deductions[{{ $x }}][name]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.name')"  class="form-control deductionname{{ $attendance->id }}"  value="" />
                    </div>
                    <div class="col-md-4 form-group">
                    <input type="text" name="deductions[{{ $x }}][amount]" onChange="allowancescal('{{ $attendance->id }}');"  placeholder="@lang('app.amount')"  class="form-control deductionamount{{ $attendance->id }}"  value="0" />
                    </div>
                    <div class="col-md-4 form-group">
                        <a href="javascript:void(0);" class="removerow">Remove</a>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <textarea name="description"  placeholder="@lang('app.note')"  class="form-control"  >{{ $attendance->description }}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        {{ csrf_field() }}
        <input type="hidden" name="attendanceid" value="{{ $attendance->id }}"/>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-success">Save changes</button>
    </div>
</form>