
    @if($contractor)
      <?php   $contractors = \App\Employee::where('id',$contractor)->where('user_type','contractor')->first();
      ?>
        <h3>{{ ucwords($contractors->name) }}</h3>
        @else
        <h3>@lang('modules.manpowerLogs.departmental')</h3>
    @endif
    <div class="combo-sheet">
        <div class="table-wrapper">

                 <table class="table table-bordered" >
                        <thead>
                        <tr>
                            <th></th>
                            <?php
                            $curmonth = '10-'.$curmonth.'-'.$curyear;
                            $day = date('t',strtotime($curmonth));
                            $month = date('m',strtotime($curmonth));
                            $year = date('Y',strtotime($curmonth));
                            for($x=1;$x<=$day;$x++){
                             $date = date('d M Y',mktime( 0, 0, 0, $month, $x, $year));
                                ?>
                                      <th>{{ $date }}</th>
                             <?php }   ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($manpowercategoryarray)){
                            foreach ($manpowercategoryarray as $manpowercategory){
                            ?>
                        <tr>
                            <td class="cell-inp"><strong>{{ ucwords($manpowercategory->title) }}</strong></td>
                            <?php  for($x=1;$x<=$day;$x++){
                            $date = date('Y-m-d',mktime( 0, 0, 0, $month, $x, $year));
                            $mancount = \App\ManpowerLog::where('manpower_category',$manpowercategory->id)->where('work_date',$date);
                            if(!empty($contractors->user_id)){
                                $mancount = $mancount->where('contractor',$contractors->user_id);
                            }else{
                                $mancount = $mancount->where('contractor','0');
                            }
                            if(!empty($projectid)){
                                $mancount = $mancount->where('project_id',$projectid);
                            }
                            if(!empty($subprojectid)){
                                $mancount = $mancount->where('title_id',$subprojectid);
                            }
                            if(!empty($segmentid)){
                                $mancount = $mancount->where('segment_id',$segmentid);
                            }
                            $mancount = $mancount->sum('manpower');
                            ?>
                            <td class="cell-inp">{{ $mancount ?: '' }}</td>
                            <?php }   ?>
                        </tr>
                        <?php }}?>
                        </tbody>
                    </table>

        </div>
    </div>
