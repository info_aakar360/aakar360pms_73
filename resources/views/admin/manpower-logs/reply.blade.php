@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.man-power-logs.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>

@endpush
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-inverse">
            <div class="panel-heading">@lang('app.labourattendance')</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="col-md-12 form-group">
                        <div class="col-md-5">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#menu1">@lang('app.details')</a></li>
                                <li><a data-toggle="pill" href="#home">@lang('app.comment')</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h2><b>{{ $manpower->title }}</b></h2>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade">
                            <div class="form-body form-group">
                                {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <div class="form-group mentiontags">
                                                <label class="control-label"><strong>@lang('modules.tasks.comment'):</strong></label>
                                                <textarea class="form-control ajax-mention" name="comment" rows="7"></textarea>
                                                <input type="hidden" class="mentionsCollection" name="mentionusers" />
                                            </div>
                                        </div>
                                            <div class="col-md-6 form-group">
                                                <label class="control-label">Upload Files</label>
                                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                <div id="file-upload-box" >
                                                    <div class="row" id="file-dropzone">
                                                        <div class="col-md-12">
                                                            <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                                {{ csrf_field() }}
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple/>
                                                                </div>
                                                                <input name="image_url" id="image_url" type="hidden" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="manpowerID" id="manpowerID">
                                                <input type="hidden" name="replyID" id="replyID">
                                            </div>
                                        <div class="col-md-12 text-right">
                                        <button type="button" id="update-task" class="btn btn-success btn-width150"><i class="fa fa-check"></i> @lang('app.save')</button>
                                        </div>
                                        </div>

                            {!! Form::close() !!}
                                </div>

                            <div class="row">
                                <div class="col-md-8">
                                    @foreach($replies as $reply)
                                        <div class="col-md-11 timelineblock <?php if($user->id==$reply->added_by){ echo 'float-right';}?>">
                                            <div class="col-md-1">
                                                <img class="img-circle w-100"  src="{{ get_users_image_link($reply->added_by) }}" />
                                            </div>
                                            <div class="col-md-11">
                                                <div class="taskusername">{{ get_user_name($reply->added_by) }} </div>
                                                <div class="tasksdate">{{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}</div>
                                                <div class="commenttext">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            {!! $reply->comment !!}
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            {!! mentionusers_html($reply->mentionusers) !!}
                                                        </div>
                                                        </div>
                                                    <?php

                                                    $replyfiles = \App\ManpowerLogFiles::where('manpower_id',$manpower->id)->where('reply_id',$reply->id)->get();
                                                    if(count($replyfiles)>0){
                                                    ?>

                                                    <div class="col-md-2">
                                                        <?php
                                                        foreach ($replyfiles as $replyfile){?>
                                                                @if($replyfile->external_link != '')
                                                                    <?php $imgurl = $replyfile->external_link;?>
                                                                @elseif($storage == 'local')
                                                                    <?php $imgurl = uploads_url().'/manpower-log-files/'.$replyfile->manpower_id.'/'.$replyfile->hashname;?>
                                                                @elseif($storage == 's3')
                                                                    <?php $imgurl = $url.$companyid.'/manpower-log-files/'.$replyfile->manpower_id.'/'.$replyfile->hashname;?>
                                                                @elseif($storage == 'google')
                                                                    <?php $imgurl = $replyfile->google_url;?>
                                                                @elseif($storage == 'dropbox')
                                                                    <?php $imgurl = $replyfile->dropbox_link;?>
                                                                @endif
                                                                {!! mimetype_thumbnail($replyfile->filename,$imgurl)  !!}
                                                                    <a href="javascript:void(0);" >By {{ get_user_name($replyfile->added_by) }} <br> <?php echo date('d M Y',strtotime($replyfile->created_at));?></a>
                                                          <?php }?>
                                                    </div>
                                                    <?php }?>
                            </div>
                            </div>
                            </div>
                            @endforeach
                            </div>
                            </div>

                                    </div>
                        <div id="menu1" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;"><b>Contractor</b></td>
                                        <td  style="width: 35%;">{{ get_users_contractor_name($manpower->contractor) }}</td>
                                        <td style="width: 15%;"><b>@lang('app.labourattendancecategory')</b></td>
                                        <td  style="width: 35%;"> {{ get_manpower_category($manpower->manpower_category) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Project</b></td>
                                        <td  style="width: 35%;">{{ get_project_name($manpower->project_id) }}</td>

                                        <?php  if(in_array('sub_projects', $user->modules)) {?>
                                        <td style="width: 15%;"><b>Sub project</b></td>
                                        <td  style="width: 35%;">{{ get_title($manpower->title_id) }}</td>
                                        <?php }?>
                                        <?php  if(in_array('segments', $user->modules)) {?>
                                        <td style="width: 15%;"><b>Segment</b></td>
                                        <td  style="width: 35%;">{{ get_segment($manpower->segment_id) }}</td>
                                        <?php }?>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Activity</b></td>
                                        <td  style="width: 35%;">{{ get_activity($manpower->activity_id) }}</td>
                                        <td style="width: 15%;"><b>Task</b></td>
                                        <td  style="width: 35%;">{{ get_boq_name($manpower->costitem_id) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.labourattendance')</b></td>
                                        <td  style="width: 35%;">{{ $manpower->manpower }}</td>
                                        <td style="width: 15%;"><b>@lang('app.workinghours')</b></td>
                                        <td  style="width: 35%;">{{ $manpower->workinghours }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.workdate')</b></td>
                                        <td  style="width: 35%;"><?php
                                            $exd = explode('-',$manpower->work_date);
                                            ?>
                                            {{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b>@lang('app.description')</b>
                                {!!  $manpower->description !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b>Files</b>
                                <br>
                                <div class="row" id="list">
                                    <?php
                                     if(count($files)>0){
                                    ?>
                                        <?php
                                        foreach ($files as $replyfile){?>
                                    <div class="col-md-2 form-group full-image">
                                           @if($replyfile->external_link != '')
                                                    <?php $imgurl = $replyfile->external_link;?>
                                                @elseif($storage == 'local')
                                                    <?php $imgurl = uploads_url().'manpower-log-files/'.$replyfile->manpower_id.'/'.$replyfile->hashname;?>
                                                @elseif($storage == 's3')
                                                    <?php $imgurl = $url.$companyid.'/manpower-log-files/'.$replyfile->manpower_id.'/'.$replyfile->hashname;?>
                                                @elseif($storage == 'google')
                                                    <?php $imgurl = $replyfile->google_url;?>
                                                @elseif($storage == 'dropbox')
                                                    <?php $imgurl = $replyfile->dropbox_link;?>
                                                @endif
                                                {!! mimetype_thumbnail($replyfile->filename,$imgurl)  !!}
                                            <a href="javascript:void(0);" >By {{ get_user_name($replyfile->added_by) }} <br> <?php echo date('d M Y',strtotime($replyfile->created_at));?></a>
                                    </div>
                                        <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>


<script>
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.man-power-logs.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        var ids = '{{ $manpower->id }}';
        formData.append('manpower_id', ids);
        var replyid = $("#replyID").val();
        formData.append('reply_id', replyid);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "@lang('messages.attendanceUpdatedSuccessfully')";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.man-power-logs.index') }}';
    });

    //    update task
    $('#update-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.man-power-logs.replyPost', [$manpower->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                if(myDropzone.getQueuedFiles().length > 0){
                    manpowerID = response.manpowerID;
                    $('#manpowerID').val(response.manpowerID);
                    $('#replyID').val(response.replyID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('messages.attendanceCreatedSuccessfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.man-power-logs.index') }}'
                }
            }
        })
    });

    //    update task
    function removeFile(id) {
        var url = "{{ route('admin.man-power-logs.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })

    };

    function updateTask(){
        $.easyAjax({
            url: '{{route('admin.man-power-logs.replyPost', [$manpower->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                if(myDropzone.getQueuedFiles().length > 0){
                    manpowerID = response.manpowerID;
                    $('#manpowerID').val(response.manpowerID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('messages.manpowerUpdatedSuccessfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.man-power-logs.index') }}'
                }
            }
        })
    }

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.man-power-logs.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });

    $('#project_id').change(function () {
        var id = $(this).val();

        // For getting dependent task
        var dependentTaskUrl = '{{route('admin.all-tasks.dependent-tasks', [':id', ':taskId'])}}';
        dependentTaskUrl = dependentTaskUrl.replace(':id', id);
        dependentTaskUrl = dependentTaskUrl.replace(':taskId', '{{ $manpower->id }}');
        $.easyAjax({
            url: dependentTaskUrl,
            type: "GET",
            success: function (data) {
                $('#dependent_task_id').html(data.html);
            }
        })
    });

    $('#createTaskCategory').click(function(){
        var url = '{{ route('admin.taskCategory.create-cat')}}';
        $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
        $.ajaxModal('#taskCategoryModal', url);
    });
    var url = '{{ route('admin.projects.members',[$manpower->project_id]) }}';
    $('textarea.ajax-mention').mentionsInput({
        onDataRequest:function (mode, query, callback) {
            $.getJSON(url, function(responseData) {
                responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                callback.call(this, responseData);
            });
        }
    });

</script>

@endpush
