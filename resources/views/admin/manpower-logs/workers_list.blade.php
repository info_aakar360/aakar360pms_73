<?php if(count($contractorarray)>0){
        foreach ($contractorarray as $contractor){ ?>
    <div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="col-md-6 col-xs-6">
                    <img src="{{ get_employee_image_link($contractor->id) }}" alt="user" class="img-circle" width="40">
                    {{ ucwords($contractor->name) }}
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" id="workerspanel">
                    <?php
                    $workerslist = \App\Workers::where('project_id',$project->id)->where('emp_id',$contractor->id)->get();
                     if(count($workerslist)>0){
                    foreach ($workerslist as $row){
                                $manpowerattendance = \App\ManpowerLog::where('company_id',$project->company_id)->where('project_id',$project->id)->where('contractor',$contractor->user_id)->where('work_date',$startdate)->where('worker_id',$row->id)->first();
                          ?>
                                <div class="row">
                                        <div class="col-md-5">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <?php if($row->type=='category'){?>
                                                    <p>{{ get_manpower_category($row->category) }}</p>
                                                         <?php }?>
                                                    <?php if($row->type=='worker'){?>
                                                    <p>{{ ucwords($row->name) }} ({{ get_manpower_category($row->category) }})</p>
                                                        <?php }?>
                                                        <?php if(!empty($manpowerattendance)){ ?>
                                                    <p>{{ $manpowerattendance->salaryperday }} Per {{ $manpowerattendance->salarytype }} ({{ $manpowerattendance->hourspershift }}) hr</p>
                                                    <?php }else{ ?>
                                                        <p>{{ $row->salary }} Per {{ $row->salarytype }} ({{ $row->workinghours }}) hr</p>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <label class="switch">
                                                    <input type="checkbox" name="workers[]"  value="{{ $row->id }}" class="labourworker" data-contractor="{{ $contractor->id }}" <?php if(!empty($manpowerattendance)){ echo 'checked'; }?> >
                                                    <div class="slider round weblogincheckbox"><span class="on"></span><span class="off"></span></div>
                                                </label>

                                                    <div class="markattendance{{ $row->id }}" style="display: <?php if(!empty($manpowerattendance)){ echo 'block'; }else{ echo 'none'; }?>;">
                                                    <?php if($row->type=='worker'){?>
                                                        <div class="col-md-1">
                                                            <input type="text" class="form-control noofworkers{{ $row->id }}" onchange="workerupdate({{ $row->id }});" name="noofworkers{{ $row->id }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->manpower; }else{ echo '1';}?>" readonly />
                                                        </div>
                                                    <?php }?>
                                                    <?php if($row->type=='category'){?>
                                                        <div class="col-md-4">
                                                            <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary"  onclick="callworkers('{{ $row->id }}','minus');"><i class="fa fa-minus"></i></a></div>
                                                            <div class="col-md-4"> <input type="text" class="form-control noofworkers{{ $row->id }}"  onchange="workerupdate({{ $row->id }});" name="noofworkers[{{ $row->id }}]" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->manpower; }else{ echo '1';}?>" /></div>
                                                            <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary" onclick="callworkers('{{ $row->id }}','plus');"><i class="fa fa-plus"></i></a></div>
                                                            </div>
                                                    <?php }?>
                                                <div class="col-md-2">
                                                    <select class="form-control shift{{ $row->id }}"  onchange="workerupdate({{ $row->id }});" name="shift{{ $row->id }}" >
                                                        <option value="">Select Shift</option>
                                                        <option value="Day" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Day'){ echo 'selected'; }?> >Day</option>
                                                        <option value="Night" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Night'){ echo 'selected'; }?> >Night</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control workinghours{{ $row->id }}"  onchange="workerupdate({{ $row->id }});" name="workinghours{{ $row->id }}"  value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->workinghours; }else{ echo $row->workinghours;}?>" />
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="hidden" class="workerattendance{{ $row->id }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>" />
                                                    <a href="javascript:void(0);" class="allowncessmodal allownattendance{{ $row->id }}" data-attendanceid = "<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>"><i  style="font-size: 25px;" class="fa fa-chevron-circle-down"></i></a>
                                                </div>
                                                <div class="col-md-6">
                                                <p>Total Price: ₹<span class="totalprice<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>"><?php if(!empty($manpowerattendance)){ echo $manpowerattendance->totalprice; }else{ echo '0';}?></span></p>
                                                </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <?php if(!empty($manpowerattendance)){ ?>
                                            <a href="javascript:void(0);" class="removeattendance" data-attendanceid="{{ $manpowerattendance->id }}"><i class="fa fa-times" style="color:red"></i></a>
                                            <?php } ?>
                                        </div>
                                </div>
                        <hr/>
                    <?php  } }?>
                    <?php
                        $labourslist = array();
                        $labourslist =  \App\ManpowerLog::where('company_id',$project->company_id)->where('project_id',$project->id)->where('contractor',$contractor->user_id)->where('work_date',$startdate)->where('worker_id','0')->get();
                        if(count($labourslist)>0){
                        $count=1;  foreach ($labourslist as $manpowerattendance){

                        ?>
                        <div class="row onetimelabours">
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <p>{{ get_manpower_category($manpowerattendance->manpower_category) }}</p>

                                            <p>{{ $manpowerattendance->salaryperday }} Per {{ $manpowerattendance->salarytype }} ({{ $manpowerattendance->hourspershift }}) hr</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="switch">
                                            <input type="checkbox" name="labours[]"  value="{{ $count }}" class="labouronetime" checked ><div class="slider round"><span class="on"></span><span class="off"></span></div></label>
                                        <div class="onetimemarkattendance{{ $count }}" style="display: block;">

                                            <div class="col-md-4">
                                                <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary"  onclick="calloneworkers('{{ $count }}','minus');"><i class="fa fa-minus"></i></a></div>
                                                <div class="col-md-4"> <input type="text" class="form-control onenoofworkers{{ $count }}"  onchange="labourupdate({{ $count }});" name="noofworkers[{{ $count }}]" value="<?php if(!empty($manpowerattendance)){ echo (int)$manpowerattendance->manpower; }else{ echo '1';}?>" /></div>
                                                <div class="col-md-4"><a href="javascript:void(0);" class="btn btn-primary" onclick="calloneworkers('{{ $count }}','plus');"><i class="fa fa-plus"></i></a></div>
                                            </div>

                                            <div class="col-md-2">
                                                <select class="form-control" onchange="labourupdate({{ $count }});" name="shift{{ $count }}" >
                                                    <option value="">Select Shift</option>
                                                    <option value="Day" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Day'){ echo 'selected'; }?> >Day</option>
                                                    <option value="Night" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Night'){ echo 'selected'; }?> >Night</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" onchange="labourupdate({{ $count }});" name="workinghours" value="{{ $manpowerattendance->workinghours }}" />
                                            </div>
                                            <div class="col-md-1">
                                                <input type="hidden" class="workerattendance{{ $row->id }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>" />
                                                <a href="javascript:void(0);" class="allowncessmodal allownattendance{{ $row->id }}" data-attendanceid = "<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>"><i  style="font-size: 25px;" class="fa fa-chevron-circle-down"></i></a>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Total Price: ₹<span class="totalprice<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>"><?php if(!empty($manpowerattendance)){ echo $manpowerattendance->totalprice; }else{ echo '0';}?></span></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <?php if(!empty($manpowerattendance)){ ?>
                                    <a href="javascript:void(0);" class="removeattendance" data-attendanceid="{{ $manpowerattendance->id }}"><i class="fa fa-times" style="color:red"></i></a>
                                    <?php } ?>
                                </div>
                        </div>
                        <hr/>
                    <?php  $count++; } }?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php  } }?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="col-md-6 col-xs-6">
                    Company
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" id="workerspanel">
                    <?php
                    create_company_workers($user->id,$project->id,$project->company_id);
                     $workerslist = \App\Workers::where('company_id',$project->company_id)->where('project_id',$project->id)->where('contractor','0')->get();
                    if(count($workerslist)>0){
                    foreach ($workerslist as $row){
                    $manpowerattendance = \App\ManpowerLog::where('company_id',$project->company_id)->where('project_id',$project->id)->where('contractor',0)->where('work_date',$startdate)->where('worker_id',$row->id)->first();

                    ?>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-10">
                                    <p>{{ ucwords($row->name) }} ({{ get_manpower_category($row->category) }})</p>
                                    <?php if(!empty($manpowerattendance)){ ?>
                                    <p>{{ $manpowerattendance->salaryperday }} Per {{ $manpowerattendance->salarytype }} ({{ $manpowerattendance->hourspershift }}) hr</p>
                                    <?php }else{ ?>
                                    <p>{{ $row->salary }} Per {{ $row->salarytype }} ({{ $row->workinghours }}) hr</p>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="switch">
                                    <input type="checkbox" name="workers[]"  value="{{ $row->id }}" class="labourworker" data-contractor="0" <?php if(!empty($manpowerattendance)){ echo 'checked'; }?> >
                                    <div class="slider round weblogincheckbox"><span class="on"></span><span class="off"></span></div>
                                </label>
                                <div class="markattendance{{ $row->id }}" style="display: <?php if(!empty($manpowerattendance)){ echo 'block'; }else{ echo 'none'; }?>;">
                                    <div class="col-md-1">
                                        <input type="text" class="form-control noofworkers{{ $row->id }}" onchange="workerupdate({{ $row->id }});" name="noofworkers{{ $row->id }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->manpower; }else{ echo '1';}?>" readonly />
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control shift{{ $row->id }}"  onchange="workerupdate({{ $row->id }});" name="shift{{ $row->id }}" >
                                            <option value="">Select Shift</option>
                                            <option value="Day" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Day'){ echo 'selected'; }?> >Day</option>
                                            <option value="Night" <?php if(!empty($manpowerattendance)&&$manpowerattendance->shift=='Night'){ echo 'selected'; }?> >Night</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control workinghours{{ $row->id }}"  onchange="workerupdate({{ $row->id }});" name="workinghours{{ $row->id }}"  value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->workinghours; }else{ echo $row->workinghours;}?>" />
                                    </div>
                                    <div class="col-md-1">
                                        <input type="hidden" class="workerattendance{{ $row->id }}" value="<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>" />
                                        <a href="javascript:void(0);" class="allowncessmodal allownattendance{{ $row->id }}" data-attendanceid = "<?php if(!empty($manpowerattendance)){ echo $manpowerattendance->id; }?>"><i  style="font-size: 25px;" class="fa fa-chevron-circle-down"></i></a>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Total Price: ₹<span class="totalprice{{ $row->id }}"><?php if(!empty($manpowerattendance)){ echo $manpowerattendance->totalprice; }else{ echo '0';}?></span></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-1">
                            <?php if(!empty($manpowerattendance)){ ?>
                            <a href="javascript:void(0);" class="removeattendance" data-attendanceid="{{ $manpowerattendance->id }}"><i class="fa fa-times" style="color:red"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                    <hr/>
                    <?php  } }?>

                </div>
            </div>
        </div>
    </div>
</div>
