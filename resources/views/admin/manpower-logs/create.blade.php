@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h2 style="color: #002f76">@lang('app.menu.labourattendance')</h2>

                <div class="col-md-3">
                    <a href="javascript:void(0);" onClick="workerlist();" ><i class="fa fa-refresh"></i></a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['id'=>'logTime','class'=>'ajax-form','method'=>'POST', 'enctype'=>'multipart/form-data']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.selectProject')</label>
                                        <select class="form-control" name="project_id" data-placeholder="@lang('modules.labourAttendance.selectProject')" id="project_id">
                                            <option value="">Select Project</option>
                                            @foreach($projectarray as $project)
                                                <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>
                                            @lang('app.contractors')
                                        </label>
                                        <select class="select2 form-control" name="contractor"
                                                data-placeholder="@lang('modules.labourAttendance.selectContractor')"
                                                id="contractors">
                                            <option value="0">All Contractor</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.workdate')</label>
                                        <input name="workdate" type="text" class="form-control "
                                               value="{{ date('d-m-Y') }}" id="start_date"/>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-right">
                                        <br>
                                            <a href="javascript:void(0);" class="btn btn-primary createworker"  ><i class="fa fa-plus"></i> Add Fix Worker</a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group text-right">
                                        <br>
                                            <a href="javascript:void(0);" class="btn btn-primary createworkeronetime" ><i class="fa fa-plus"></i> Add One Time Worker</a>
                                    </div>
                                </div>
                              {{--  <div class="col-md-3">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.shift')</label>
                                        <select class="form-control" name="shift"  data-style="form-control">
                                            <option value="">Select Shift</option>
                                            <option value="day">Day</option>
                                            <option value="night">Night</option>
                                        </select>
                                    </div>
                                </div>--}}

                                <?php  if(in_array('sub_projects', $user->modules)) {?>
                                <div class="col-md-4" id="subprojectblock" style="display: none">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.subproject')</label>
                                        <select class="form-control" name="title_id" id="subprojectlist"
                                                data-style="form-control">
                                        </select>
                                    </div>
                                </div>
                                <?php }?>
                                <?php  if(in_array('segments', $user->modules)) {?>

                                <div class="col-md-4" id="segmentsblock" style="display: none">
                                    <div class="form-group">
                                        <label>@lang('app.segment')</label>
                                        <select class="form-control" name="segment_id" id="segmentlist" data-style="form-control"></select>
                                    </div>
                                </div>
                                <?php }?>

                                {{--<div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('app.activity')</label>
                                        <select class="form-control" name="activity" id="activitylist" data-style="form-control">
                                            <option value="">Select Activity</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('modules.labourAttendance.projecttask')</label>
                                        <select class="form-control" name="costitem" id="tasklist" data-style="form-control">
                                            <option value="">Select Task</option>
                                        </select>
                                    </div>
                                </div>--}}
                                <div class="col-md-12" style="display: none;">
                                    <div class="form-group">
                                        <label for="memo">Description</label>
                                        <textarea name="description" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12" style="display: none;">
                                    <button type="button"
                                            class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button"
                                            style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File
                                        Select Or Upload
                                    </button>
                                    <div id="file-upload-box">
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone dropheight"
                                                     id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple />
                                                    </div>
                                                    <input name="image_url" id="image_url" type="hidden"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="manpowerID" id="manpowerID">
                                </div>
                        </div>
                            <div class="row">
                                <div id="workerslist"></div>
                            </div>
                        <div class="form-actions m-t-30 text-right">
                            <a href="{{ route('admin.man-power-logs.index') }}" class="btn btn-danger">Go Back</a>
                        </div>
                        {!! Form::close() !!}
                        <hr>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- .row -->

        <div class="modal fade bs-modal-md in" id="categoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->.
        </div>
        <div class="modal fade bs-modal-md in" id="categoryOneModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->.
        </div>
        <div class="modal fade bs-modal-md in" id="allowncessModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" id="modal-data-application">
                <div class="modal-content">

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->.
        </div>
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.man-power-logs.storeImage') }}",
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: '.png, .jpeg, .jpg, .pdf, .docx, .csv, .xlsx',
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks: true,
            parallelUploads: 10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function (file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length, 'sending');
            var ids = $('#manpowerID').val();
            formData.append('manpower_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Log Updated Successfully";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.man-power-logs.index') }}'

        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.man-power-logs.store')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if (myDropzone.getQueuedFiles().length > 0) {
                        manpowerID = data.manpowerID;
                        $('#manpowerID').val(data.manpowerID);
                        myDropzone.processQueue();
                    }
                    else {
                        var msgs = "Log Updated Successfully";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.man-power-logs.index') }}'
                    }
                }
            });
        });
        $(document).on('submit','#createContractorForm',function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{route('admin.man-power-logs.workercreatepost')}}',
                container: '#createContractorForm',
                type: "POST",
                data: $('#createContractorForm').serialize(),
                success: function (data) {

                        var msgs = "Worker Created Successfully";
                        $.showToastr(msgs, 'success');
                        $("#createContractorForm")[0].reset();
                        $("#categoryModal").modal("hide");
                        workerlist();
                }
            });
        });
        $(document).on('submit','#createOneTimeContractorForm',function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route('admin.man-power-logs.workercreateonetimepost')}}',
                container: '#createOneTimeContractorForm',
                type: "POST",
                data: $('#createOneTimeContractorForm').serialize(),
                success: function (data) {
                    workerlist();
                    $("#createOneTimeContractorForm")[0].reset();
                    $("#categoryOneModal").modal("hide");
                }
            });
        });
        $(document).on('submit','#allowancesAttendances',function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route('admin.man-power-logs.allowanceattendancepost')}}',
                container: '#allowancesAttendances',
                type: "POST",
                data: $('#allowancesAttendances').serialize(),
                success: function (data) {
                    $("#allowncessModal").modal("hide");

                    $(".totalprice"+attendanceid).html("0");
                    $(".totalprice"+attendanceid).html(data.price);
                }
            });
        });

        $('#project_id').change(function () {
            var project = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function (data) {
                        $(".preloader-small").hide();
                        $("#subprojectblock").hide();
                        $("#segmentsblock").hide();
                        $("select#tasklist").html("");
                        $("select#segmentlist").html("");
                        $("select#subprojectlist").html("");
                        $("select#activitylist").html("");
                        if (data.subprojectlist) {
                            $("#subprojectblock").show();
                            $("select#subprojectlist").html(data.subprojectlist);
                            $('select#subprojectlist').select2();
                        }
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        if (data.tasklist) {
                            $("select#contractors").html("");
                            $("select#contractors").html(data.contractors);
                        }
                        workerlist();
                    }
                });
            }
        });
        $("#subprojectlist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist},
                    success: function (data) {
                        $("#segmentsblock").hide();
                        $("select#segmentlist").html("");
                        $("select#segmentlist").html("");
                        $("select#tasklist").html("");
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $("#activitylist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist, 'activity': activity},
                    success: function (data) {
                        if (data.tasklist) {
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                    }
                });
            }
        });
        $("#contractors,#start_date").change(function () {
            workerlist();
        });
        function callworkers(workerid,type){
            var quant = parseInt($(".noofworkers"+workerid).val());

            if(type=='plus'){
                quant = quant+1;
            }
            if(type=='minus'){
                if(quant>1){
                    quant = quant-1;
                }
            }
            $(".noofworkers"+workerid).val(quant)
        }
        function calloneworkers(workerid,type){
            var quant = parseInt($(".onenoofworkers"+workerid).val());

            if(type=='plus'){
                quant = quant+1;
            }
            if(type=='minus'){
                if(quant>1){
                    quant = quant-1;
                }
            }
            $(".onenoofworkers"+workerid).val(quant)
        }
        $(document).on('change','.labourworker',function (e) {
            e.preventDefault();
            let workerid = $(this).val();
            let project = $("#project_id").select2().val();
            let contractor = $(this).attr('data-contractor');
            let startdate = $("#start_date").val();
            let checked = $(this).prop('checked') ? 'true' : 'false';
            if(checked=='true'){
                if (project) {
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.man-power-logs.workerattendance') }}",
                        data: {'_token': token, 'projectid': project, 'contractor': contractor, 'startdate': startdate, 'workerid': workerid},
                        beforeSend:function () {
                            $(".preloader-small").show();
                        },
                        success: function (data) {
                            $(".preloader-small").hide();
                            $(".markattendance"+workerid).show();
                            $(".workerattendance"+workerid).val(data.manpowerID);
                            $(".allownattendance"+workerid).attr('data-attendanceid',data.manpowerID);
                        }
                    });
                }
            }else{
                $(".markattendance"+workerid).hide();
            }

        });
        function workerupdate(workerid){
            var noofworkers =  $(".noofworkers"+workerid).val();
            var shift =  $(".shift"+workerid).val();
            var workinghours =  $(".workinghours"+workerid).val();
            var workerattendance =  $(".workerattendance"+workerid).val();
           var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.man-power-logs.workerattendancepost') }}",
                    data: {'_token': token, 'workerid': workerid, 'noofworkers': noofworkers, 'shift': shift, 'workinghours': workinghours, 'workerattendance': workerattendance},
                    success: function (data) {
                        var item1 = $( "span.totalprice" )[ 0 ];
                        $(".markattendance"+workerid).find(item1).html("0");
                        $(".markattendance"+workerid).find(item1).html(data.price);
                        $(".totalprice"+data.manpowerid).html("0");
                        $(".totalprice"+data.manpowerid).html(data.price);
                    }
                });
        }
       /* $(document).on('change','.labouronetime',function (e) {
            e.preventDefault();
            let workerid = $(this).val();
            let project = $("#project_id").select2().val();
            let contractor = $("#contractors").select2().val();
            let startdate = $("#start_date").val();
            let attendanceid = $(".workerattendance"+workerid).val();
            let checked = $(this).prop('checked') ? 'true' : 'false';
            if(checked=='true'){
                if (project&&contractor) {
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        type: "POST",
                        url: "",
                        data: {'_token': token, 'attendanceid': attendanceid},
                        beforeSend:function () {
                            $(".preloader-small").show();
                        },
                        success: function (data) {
                            $(".preloader-small").hide();
                            $(".onetimemarkattendance"+workerid).show();
                        }
                    });
                }
            }else{
                $(".markattendance"+workerid).hide();
            }

        });*/
        function labourupdate(workerid){
            var noofworkers =  $(".noofworkers"+workerid).val();
            var shift =  $(".shift"+workerid).val();
            var workinghours =  $(".workinghours"+workerid).val();
            var workerattendance =  $(".workerattendance"+workerid).val();
           var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.man-power-logs.workerattendancepost') }}",
                    data: {'_token': token,  'noofworkers': noofworkers, 'shift': shift, 'workinghours': workinghours, 'workerattendance': workerattendance},
                    success: function (data) {
                        var item1 = $( "span.totalprice" )[ 0 ];
                        $(".markattendance"+workerid).find(item1).html("0");
                        $(".markattendance"+workerid).find(item1).html(data.price);
                        $(".totalprice"+data.attendanceid).html("0");
                        $(".totalprice"+data.attendanceid).html(data.price);
                    }
                });
        }
        function workerlist(){
            var project = $("#project_id").select2().val();
            var contractor = $("#contractors").val();
            var startdate = $("#start_date").val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.man-power-logs.workerlist') }}",
                    data: {'_token': token, 'projectid': project, 'contractor': contractor, 'startdate': startdate},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function (data) {
                        $(".preloader-small").hide();
                        if (data) {
                            $("#workerslist").html("");
                            $("#workerslist").html(data);
                        }
                    }
                });
            }
        }
        $(document).on("click",".createworker",function () {
            var project = $("#project_id").val();
            var cont = $("#contractors").val();
            if(!cont){
                cont = 0;
            }
            var id = project+'|'+cont;
            var url = '{{ route('admin.man-power-logs.workercreate',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Create Worker");
            $.ajaxModal('#categoryModal', url);
        });
        $(document).on("click",".createworkeronetime",function () {
            var project = $("#project_id").val();
            var cont = $("#contractors").val();
            if(!cont){
                cont = 0;
            }
            var id = project+'|'+cont;

            let onetimelabours = $(".onetimelabours").length;
            let start_date = $("#start_date").val();
            id = id+'|'+onetimelabours+'|'+start_date;
            var url = '{{ route('admin.man-power-logs.workercreateonetime',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Create Labours");
            $.ajaxModal('#categoryOneModal', url);
        });
        $(document).on("click",".allowncessmodal",function () {
            var attendanceid = $(this).attr('data-attendanceid');
            if(attendanceid){
                var url = '{{ route('admin.man-power-logs.workerallowncess',':id')}}';
                url = url.replace(':id', attendanceid);
                $('#modelHeading').html("Create Labours");
                $.ajaxModal('#allowncessModal', url);
            }
        });
        function addallowncesrow(attenid) {
            var count = $('.allowcount').length;
            var htmlcontent = '<div class="col-md-12">\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                        <input type="text" name="allowances['+count+'][name]" onChange="allowancescal(\'+attenid+\');"  placeholder="Name"  class="form-control allowancesname'+attenid+'"  value="" />\n' +
                '                        </div>\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                        <input type="text" name="allowances['+count+'][amount]" onChange="allowancescal(\'+attenid+\');"  placeholder="Amount"  class="form-control allowancesamount'+attenid+'"  value="0" />\n' +
                '                        </div>\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                         <a href="javascript:void(0);" class="removerow">Remove</a>\n' +
                '                        </div>\n' +
                '                </div>';
            $("#allowencesmodal").append(htmlcontent);
        }
        function adddeductionrow(attenid) {
            var count = $('.deductcount').length;
            var htmlcontent = '';
            htmlcontent = '<div class="col-md-12">\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                        <input type="text" name="deductions['+count+'][name]" onChange="allowancescal('+attenid+');"  placeholder="Name"  class="form-control deductionname'+attenid+'"  value="" />\n' +
                '                        </div>\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                        <input type="text" name="deductions['+count+'][amount]" onChange="allowancescal('+attenid+');"  placeholder="Amount"  class="form-control deductionamount'+attenid+'"  value="0" />\n' +
                '                        </div>\n' +
                '                        <div class="col-md-4 form-group">\n' +
                '                         <a href="javascript:void(0);" class="removerow">Remove</a>\n' +
                '                        </div>\n' +
                '                </div>';
            $("#deductionmodal").append(htmlcontent);
        }
        $(document).on("click",".removerow",function(){
                $(this).parent().parent().remove();
        });
        function  allowancescal(attendanceid) {
            let overtimehours =  $(".overtimehours"+attendanceid).val();
            let overtimeamount =  $(".overtimeamount"+attendanceid).val();
            let overtimetotal =  overtimehours*overtimeamount;
            $(".overtimetotal"+attendanceid).val(overtimetotal);
            let latefinehours =  $(".latefinehours"+attendanceid).val();
            let latefineamount =  $(".latefineamount"+attendanceid).val();
            let latefinetotal =  latefinehours*latefineamount;
            $(".latefinetotal"+attendanceid).val(latefinetotal);
        }
        $(document).on("click",".removeattendance",function () {
            if (confirm('Are you sure want to remove attendance')) {
                var id = $(this).data('attendanceid');
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.man-power-logs.removeattendance') }}",
                    data: {'_token': token, 'attendid': id},
                    beforeSend:function () {
                        $(".preloader-small").show();
                    },
                    success: function (data) {
                        $(".preloader-small").hide();
                        workerlist();
                    }
                });
            }
        });
        $('#show-add-form').click(function () {
            $('#hideShowTimeLogForm').toggleClass('hide', 'show');
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        jQuery('#date-range').datepicker({
            toggleActive: true,
            weekStart: '{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('time-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted time log!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.man-power-logs.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });


        $('#start_time, #end_time').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false
            @endif
        }).on('hide.timepicker', function (e) {
            calculateTime();
        });

        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart: '{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        }).on('hide', function (e) {
            calculateTime();
        });

        function calculateTime() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();
            var startTime = $("#start_time").val();
            var endTime = $("#end_time").val();

            var timeStart = new Date(startDate + " " + startTime);
            var timeEnd = new Date(endDate + " " + endTime);

            var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds

            var minutes = diff % 60;
            var hours = (diff - minutes) / 60;

            if (hours < 0 || minutes < 0) {
                var numberOfDaysToAdd = 1;
                timeEnd.setDate(timeEnd.getDate() + numberOfDaysToAdd);
                var dd = timeEnd.getDate();

                if (dd < 10) {
                    dd = "0" + dd;
                }

                var mm = timeEnd.getMonth() + 1;

                if (mm < 10) {
                    mm = "0" + mm;
                }

                var y = timeEnd.getFullYear();

                $('#end_date').val(mm + '/' + dd + '/' + y);
                calculateTime();
            } else {
                $('#total_time').html(hours + "Hrs " + minutes + "Mins");
            }

//        console.log(hours+" "+minutes);
        }
    </script>
@endpush