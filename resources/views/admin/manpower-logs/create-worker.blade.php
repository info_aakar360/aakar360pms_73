<form method="post" action="" id="createContractorForm" autocomplete="off">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <span class="caption-subject font-red-sunglo bold uppercase" >Worker Category</span>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        @lang('app.contractors')
                    </label>
                    <select class="select2 form-control" name="contractor"
                            data-placeholder="@lang('modules.labourAttendance.selectContractor')"
                            id="contractors">
                        <option value="0|0">All Contractor</option>
                     <?php   foreach ($contractorarray as $item) { ?>
                        <option value="{{ $item->id.'|'.$item->user_id }}">{{ $item->name }}</option>
                        <?php   } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        @lang('app.category')
                    </label>
                    <select class="select2 form-control" required name="category"
                            data-placeholder="@lang('app.category')"
                            id="contractors">
                        <option value="">Select Category</option>
                        @foreach($mancategoryarray as $mancategory)
                            <option value="{{ $mancategory->id }}">{{ ucwords($mancategory->title) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('app.workername')</label>
                    <input type="text" name="workername" class="form-control"  />
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('app.workinghours')</label>
                    <input type="text" name="workinghours" required placeholder="@lang('app.workinghours')"  class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('app.salary')</label>
                    <input type="text" name="salary" value="0"  required placeholder="@lang('app.salary')" class="form-control" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('app.salarytype')</label>
                    <select name="salarytype" class="form-control"  required >
                        <option value="">@lang('app.select') @lang('app.salarytype')</option>
                        <option value="daily">@lang('app.daily')</option>
                        <option value="weekly">@lang('app.weekly')</option>
                        <option value="fifteendays">@lang('app.fifteendays')</option>
                        <option value="monthly">@lang('app.monthly')</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        {{ csrf_field() }}
        <input type="hidden" name="projectid" value="{{ $project->id }}"/>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-success">Save changes</button>
    </div>
</form>