@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.submittals.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a, .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> New Submittals</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storeTask','class'=>'ajax-form','method'=>'POST']) !!}

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Number & Revision</label>
                                        <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="number" value="{{ $newnumber }}" readonly class="form-control" placeholder="Number *" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="revision" value="0" readonly class="form-control" placeholder="Revision *" required>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <div class="form-group row">
                                        <label for="private">Private or Public</label><br>
                                        <input id="private" name="private" value="1" type="checkbox">
                                        <label for="private">Private</label>
                                    </div>
                                    <br>
                                </div>
                                <div class="col-md-1">
                                    <br>
                                    <div class="form-group row">
                                        <label for="private">Draft</label><br>
                                        <input id="draft" name="draft" value="1" type="checkbox">
                                        <label for="draft">Draft</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open">Open</option>
                                            <option value="Draft">Draft</option>
                                            <option value="Close">Close</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Responsible Contractor</label>
                                        <select class="selectpicker form-control" name="rec_contractor" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($contractors as $employee)
                                                <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received From</label>
                                        <select class="selectpicker form-control" name="received_from" id="user_id">
                                            <option value="">Choose Received From</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Submit By</label>
                                        <input type="text" name="submitdate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Issue Date</label>
                                        <input type="text" name="issuedate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received Date</label>
                                        <input type="text" name="receiveddate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Final Due Date</label>
                                        <input type="text" name="finalduedate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Submittals Manager</label>
                                        <select class="selectpicker form-control" name="submittal_manager" id="user_id">
                                            <option value="">Choose Submittals Manager</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}" >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type
                                        </label>
                                        <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                            <option value="">Please Select Type</option>
                                            <option value="Document">Document</option>
                                            <option value="Other">Other</option>
                                            <option value="Plans">Plans</option>
                                            <option value="Prints">Prints</option>
                                            <option value="Product Information">Product Information</option>
                                            <option value="Product Manual">Product Manual</option>
                                            <option value="Sample">Sample</option>
                                            <option value="Shop Drawing">Shop Drawing</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" name="location" class="form-control" placeholder="Location *" required>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution List
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="projectid" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">Sub Project</label>
                                        <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                            <option value="">Please Select Sub Project</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">Task</label>
                                        <select class="select2 form-control costitemlist" id="costitemlist" name="costitem" data-style="form-control" required>
                                            <option value="">Please Select Task</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.description')</label>
                                        <textarea id="description" name="description" class="form-control summernote"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">@lang('app.image')</label>
                                    <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                    <div id="file-upload-box" >
                                        <div class="row" id="file-dropzone">
                                            <div class="col-md-12">
                                                <div class="dropzone" id="file-upload-dropzone">
                                                    {{ csrf_field() }}
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple/>
                                                    </div>
                                                    <input name="image_url" id="image_url"type="hidden" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="submittalsID" id="submittalsID">
                                </div>
                                </div>
                                <div class="row">
                                <hr>
                                <h2>Submital Schedule Information</h2>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">On Site Date</label>
                                        <input type="text" name="onsitedate" class="form-control datepicker onsitedate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lead time</label>
                                        <input type="text" name="leadtime" class="form-control leadtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Planned Return Date</label>
                                        <input type="text" name="planreturndate" class="form-control datepicker planreturndate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Design Team review time</label>
                                        <input type="text" name="designreviewtime" class="form-control designreviewtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Planned Internal review completed Date</label>
                                        <input type="text" name="planinternaldate" class="form-control datepicker planinternaldate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Internal review time</label>
                                        <input type="text" name="internalreviewtime" class="form-control internalreviewtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Planned Submit Date</label>
                                        <input type="text" name="plansubmitdate" class="form-control datepicker plansubmitdate" autocomplete="off">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <h2>Delivery Information</h2>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Anticipated Delivery Date</label>
                                        <input type="text" name="anticipateddeliverydate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Schedule task</label>
                                        <input type="text" name="scheduletask" class="form-control" placeholder="Schedule task " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Delivery Date</label>
                                        <input type="text" name="confirmdeliverydate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Actual Delivery Date</label>
                                        <input type="text" name="actualdeliverydate" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-6">
                                    <h2>Submittal Workflow</h2>
                                    </div>
                                    <div class="col-md-6 m-t-20">
                                        <a href="javascript:void(0);" class="add-row btn1 btn-primary btn-circle"><i class="fa fa-plus-circle"></i> Add Row</a>
                                    </div>
                                </div>
                               <div id="submittalworkflow">

                                </div>
                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions text-right">
                            <button type="button" id="store-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


<script>
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.submittals.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#submittalsID').val();
        formData.append('submittals_id', ids);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "Submittals Created Successfully";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.submittals.index') }}'

    });
    $('.summernote').summernote({
        height: 160,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(".projectid").change(function () {
        var project = $(this).val();
        if(project){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.submittals.projecttitles') }}",
                data: {'_token': token,'projectid': project},
                success: function(data){
                    $("select.titlelist").html("");
                    $("select.titlelist").html(data);
                    $('select.titlelist').select2();
                }
            });
        }
    });
    $("#titlelist").change(function () {
        var project = $("#projectid").select2().val();
        var titlelist = $(this).val();
        if(project){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.submittals.costitembytitle') }}",
                data: {'_token': token,'projectid': project,'title': titlelist},
                success: function(data){
                    $("select.costitemlist").html("");
                    $("select.costitemlist").html(data);
                    $('select.costitemlist').select2();
                }
            });
        }
    });
    $(".leadtime").change(function () {
        var leadtime = $(this).val();
        var onsitedate = $(".onsitedate").datepicker('getDate');
        if(leadtime&&onsitedate){
            onsitedate.setDate(onsitedate.getDate() - leadtime);
            var day = onsitedate.getDate();
            var month = onsitedate.getMonth() + 1;
            var year = onsitedate.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "-" + month + "-" + year;
            $(".planreturndate").datepicker('setDate', date);
        }
    });
    $(".designreviewtime").change(function () {
        var designreviewtime = $(this).val();
        var planreturndate = $(".planreturndate").datepicker('getDate');
        if(designreviewtime&&planreturndate){
            planreturndate.setDate(planreturndate.getDate() - designreviewtime);
            var day = planreturndate.getDate();
            var month = planreturndate.getMonth() + 1;
            var year = planreturndate.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "-" + month + "-" + year;
            $(".planinternaldate").datepicker('setDate', date);
        }
    });
    $(".internalreviewtime").change(function () {
        var internalreviewtime = $(this).val();
        var planinternaldate = $(".planinternaldate").datepicker('getDate');
        if(internalreviewtime&&planinternaldate){
            planinternaldate.setDate(planinternaldate.getDate() - internalreviewtime);
            var day = planinternaldate.getDate();
            var month = planinternaldate.getMonth() + 1;
            var year = planinternaldate.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "-" + month + "-" + year;
            $(".plansubmitdate").datepicker('setDate', date);
        }
    });
    //    update task
    $('#store-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.submittals.storeSubmittals')}}',
            container: '#storeTask',
            type: "POST",
            data: $('#storeTask').serialize(),
            success: function (data) {
                $('#storeTask').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    submittalsID = data.submittalsID;
                    $('#submittalsID').val(data.submittalsID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "Submittals Created";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.submittals.index') }}'
                }
            }
        })
    });

    jQuery('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    var ques = 1;
    $('.add-row').on("click",function () {
        var catid = $(this).data('cat');
        var item = '<div class="form-group">'
            +'<div class="row">'
            +'<div class="col-md-3">'
            +'<select class="form-control" name="workflowusers[]" data-quesid="'+ques+'">'
            +'<option value="">Search People</option>';
                @foreach($employees as $employee)
                    item += '<option value="{{ $employee->id }}">{{ $employee->name }}</option>';
                @endforeach
            item += '</select>'
            +'</div>'
            +'<div class="col-md-3">'
            +'<select class="form-control" name="role[]" >'
            +'<option value="">Select Role</option>'
            +'<option value="Approver">Approver</option>'
            +'<option value="Submitter">Submitter</option>'
            +'</select>'
            +'</div>'
             +'<div class="col-md-3">'
            +'<input type="text" class="form-control datepicker" name="duedate[]" />'
            +'</div>'
             +'<div class="col-md-3">'
            +'<a href="javascript:void(0);"  class="btn btn-outline btn-success btn-sm removeBlock" ><i class="fa fa-trash-o"></i></a>'
            +'</div>'
            +'</div>'
            +'</div>';
        $("#submittalworkflow").append(item);
        ques++;
        $(".datepicker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: 'dd-mm-yyyy',
        });
    });
    $(document).on('click','.removeBlock', function () {
        $(this).parent().parent().parent().remove();
    });
</script>
@endpush

