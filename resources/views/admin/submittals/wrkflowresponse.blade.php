@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.submittals.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
    .awaans{
        background-color: #848082;
        color: #fff;
        font-weight: bold;
    }
    .awaanstext{
        color: #fff;
        font-weight: bold;
    }
    .dropzone .dz-message {
        text-align: center;
        margin: 24% 0 !important;
    }
</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                 <div class="row">
                     <div class="col-md-10">Submittals Details</div>
                 <div class="col-md-2">
                     <a href="{{ route('admin.submittals.editSubmittals', $submittals->id)}}" class="btn1 btn-info btn-circle edit-btn"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      </div>
                 </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p><b>Submittal Revision :</b> {{ $submittals->number.' - '.$submittals->revision }}:</p>
                                {!! $submittals->description  !!}
                            </div>
                        </div>
                    <div class="submitalform">
                        {!! Form::open(['id'=>'updateResponse','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="col-md-12">
                        <div class="col-md-10 form-group">
                            <p><b>Due Date: </b>{{ get_input_date($submitwrkflow->duedate) }}</p>
                        </div>
                        <div class="col-md-2 form-group text-right">
                            @if($submitwrkflow->role=='Approver')
                                Or <a href="javascript:void(0);" class="forwardto" style="color: blue;">Forward to</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" name="response" >
                                <option value="">Select Response</option>
                                <option value="Pending">Pending</option>
                                <option value="Resolve">Resolve</option>
                                <option value="Forward">Forward</option>
                                <option value="Close">Close</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <textarea id="description" name="comment" placeholder="Comment here" class="form-control summernote"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                         <div class="row" id="file-dropzone">
                                <div class="col-md-12">
                                    <div class="dropzone"  id="file-upload-dropzone">
                                        {{ csrf_field() }}
                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                        <input name="image_url" id="image_url" type="hidden" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="submittalsID" id="submittalsID">
                            <input type="hidden" name="workflowID" id="workflowID">
                        </div>
                    </div>
                        {{ csrf_field() }}
                        <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>

                        {!! Form::close() !!}
                    </div>
                    <div class="forwardform" style="display: none;">
                        {!! Form::open(['id'=>'forwardResponse','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="col-md-12">
                        <div class="col-md-10 form-group">
                            <p><b>Due Date: </b>{{ get_input_date($submitwrkflow->duedate) }}</p>
                        </div>
                        <div class="col-md-2 form-group text-right">
                            Or <a href="javascript:void(0);" class="goback" style="color: blue;">Go back</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Forward to:</label>
                            <select class="form-control" name="forwardto" >
                                <option value="">Select person</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Return by:</label>
                            <input type="text" class="form-control datepicker" name="returnby" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="role" >
                                <option value="">Select Role</option>
                                <option value="Approver">Approver</option>
                                <option value="Submitter">Submitter</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <textarea name="comment" placeholder="Comment here" class="form-control summernote"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                            <div id="file-upload-box" >
                                <div class="row" id="file-dropzone">
                                    <div class="col-md-12">
                                        <div class="dropzone dropheight"    id="file-forward-dropzone">
                                            {{ csrf_field() }}
                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                            <input name="image_url" id="image_url"type="hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="submittalsID" id="submittalsID">
                            <input type="hidden" name="workflowID" id="workflowID">
                        </div>
                    </div>
                        {{ csrf_field() }}
                        <button type="button" id="forward-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>

                        {!! Form::close() !!}
                    </div>


                    </div>

                    <hr>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.title')</b></td>
                                        <td  style="width: 35%;">{{ $submittals->title }}</td>
                                        <td style="width: 15%;"><b>Number & Revisions</b></td>
                                        <td  style="width: 35%;">{{ $submittals->number }} - {{ $submittals->revision }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Draft</b></td>
                                        <td  style="width: 35%;">@if($submittals->draft=='1') <i class="fa fa-check-circle"></i> @else  <i class="fa fa-times"></i> @endif  </td>
                                        <td style="width: 15%;"><b>Public or Private</b></td>
                                        <td  style="width: 35%;"><?php if($submittals->private == '1'){ echo 'Private';}else{ echo 'Public'; }?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Status</b></td>
                                        <td  style="width: 35%;">
                                            <?php if($submittals->status == 'Open'){ echo 'Open'; } ?>
                                            <?php if($submittals->status == 'Closed'){ echo 'Closed'; } ?>
                                        </td>
                                        <td style="width: 15%;"><b>Submittals Manager</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_employee_name($submittals->submittals_manager)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Submit By</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->submitdate) }}
                                        </td>
                                        <td style="width: 15%;"><b>Issue Date</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->issuedate) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Received By</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->receiveddate) }}
                                        </td>
                                        <td style="width: 15%;"><b>Final Due Date</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->finalduedate) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Responsible Contractor</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_contractor_name($submittals->rec_contractor)) }}
                                        </td>
                                        <td style="width: 15%;"><b>Received From</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_employee_name($submittals->received_from)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Type</b></td>
                                        <td  style="width: 35%;">
                                            {{ $submittals->type }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Distribution List</b></td>
                                        <td  style="width: 35%;">
                                            <?php $as = explode(',',$submittals->distribution);
                                            for($i = 0; $i < count($as); $i++ ){
                                            ?>
                                            {{ ucwords(get_users_employee_name($as[$i])) }},
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Location</b></td>
                                        <td  style="width: 35%;">{{ $submittals->location }}</td>
                                        <td style="width: 15%;"><b>Drawing No.</b></td>
                                        <td  style="width: 35%;">{{ $submittals->drawing_no }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Added By</b></td>
                                        <td  style="width: 35%;">{{ ucwords(get_user_name($submittals->added_by)) }} </td>
                                        <td style="width: 15%;"><b>Project</b></td>
                                        <td  style="width: 35%;">{{ get_project_name($submittals->projectid) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Project title</b></td>
                                        <td  style="width: 35%;">{{ get_subproject($submittals->titleid) }}</td>
                                        <td style="width: 15%;"><b>Task</b></td>
                                        <td  style="width: 35%;">{{ get_cost_name($submittals->costitemid) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;"><b>Submital Schedule Information</b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>On Site Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->onsitedate) }}</td>
                                        <td style="width: 15%;"><b>Lead time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->leadtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Return Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->planreturndate) }}</td>
                                        <td style="width: 15%;"><b>Design Team review time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->designreviewtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Internal review completed Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->planinternaldate) }}</td>
                                        <td style="width: 15%;"><b>Internal review time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->internalreviewtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Submit Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->plansubmitdate) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Anticipated Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->anticipateddeliverydate) }}</td>
                                        <td style="width: 15%;"><b>Schedule task</b></td>
                                        <td  style="width: 35%;">{{  $submittals->scheduletask }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Confirm Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->confirmdeliverydate) }}</td>
                                        <td style="width: 15%;"><b>Actual Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->actualdeliverydate) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Ball in Court</b></td>
                                        <td  style="width: 35%;">{{ ucwords(get_user_name($submittals->ballincourt)) }} </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Description</b></td>
                                        <td  style="width: 35%;">{!! $submittals->description !!} </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12">&nbsp;</div>

                        </div>
                        <!--/row-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>


<script>
$(".forwardto").click(function () {
    $(".forwardform").show();
    $(".submitalform").hide();
});
$(".goback").click(function () {
    $(".forwardform").hide();
    $(".submitalform").show();
});
function showDiv() {
    $('#replyDiv').toggleClass('hide', 'show');
}
Dropzone.autoDiscover = false;
//Dropzone class
myDropzone = new Dropzone("div#file-upload-dropzone", {
    url: "{{ route('admin.submittals.storeImage') }}",
    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
    paramName: "file",
    maxFilesize: 10,
    maxFiles: 10,
    acceptedFiles: "image/*,application/pdf",
    autoProcessQueue: false,
    uploadMultiple: true,
    addRemoveLinks:true,
    parallelUploads:10,
    init: function () {
        myDropzone = this;
    }
});
myDropzone = new Dropzone("div#file-forward-dropzone", {
    url: "{{ route('admin.submittals.storeImage') }}",
    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
    paramName: "file",
    maxFilesize: 10,
    maxFiles: 10,
    acceptedFiles: "image/*,application/pdf",
    autoProcessQueue: false,
    uploadMultiple: true,
    addRemoveLinks:true,
    parallelUploads:10,
    init: function () {
        myDropzone = this;
    }
});

myDropzone.on('sending', function(file, xhr, formData) {
    console.log(myDropzone.getAddedFiles().length,'sending');
    var ids = '{{ $submittals->id }}';
    var replyid = '{{ $submitwrkflow->id }}';
    formData.append('submittals_id', ids);
    formData.append('workflow_id', replyid);
});

myDropzone.on('completemultiple', function () {
    var msgs = "@lang('Submittals Updated successfully')";
    $.showToastr(msgs, 'success');
    window.location.href = '{{ route('admin.submittals.index') }}'
});

    //    update task
    $('#update-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.submittals.workflowPost', [$submitwrkflow->id])}}',
            container: '#updateResponse',
            type: "POST",
            data: $('#updateResponse').serialize(),
            success: function (data) {
                $('#updateResponse').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    submittalsID = data.submittalsID;
                    $('#submittalsID').val(data.submittalsID);
                    $('#workflowID').val(data.workflowID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('Reply posted successfully')";
                    $.showToastr(msgs, 'success');
                    var submitalid = '{{ $submittals->id }}';
                    window.location.href = '{{ route('admin.submittals.details', [$submittals->id]) }}'
                }
            }
        })
    });

    //    update task
    $('#forward-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.submittals.forwardwrkresponsepost', [$submitwrkflow->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#forwardResponse').serialize(),
            success: function (data) {
                $('#forwardResponse').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    submittalsID = data.submittalsID;
                    $('#submittalsID').val(data.submittalsID);
                    $('#workflowID').val(data.workflowID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('Forward posted successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.submittals.details', [$submittals->id]) }}'
                }
            }
        })
    });

    //    update task
    function removeFile(id) {
        var url = "{{ route('admin.submittals.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })
    };


    function updateTask(){
        $.easyAjax({
            url: '{{route('admin.submittals.replyPost', [$submittals->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                var msgs = "@lang('Reply posted successfully')";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('admin.submittals.index') }}'
            }
        })
    }

    jQuery('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 170,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

</script>

@endpush
