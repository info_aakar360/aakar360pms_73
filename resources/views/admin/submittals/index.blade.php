@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<style>
    .swal-footer {
        text-align: center !important;
    }
</style>
@endpush

@section('content')


    {{--<h2 style="color: #002f76">@lang('app.filterResults')</h2>--}}

    {{--<div class="white-box">--}}
        {{--<div class="row m-b-10">--}}
            {{--{!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}--}}
            {{--<div class="col-md-5">--}}
                {{--<div class="example">--}}
                    {{--<h5 class="box-title m-t-30">@lang('app.selectDateRange')</h5>--}}

                    {{--<div class="input-daterange input-group" id="date-range">--}}
                        {{--<input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"--}}
                               {{--value="{{ \Carbon\Carbon::today()->subDays(15)->format('Y-m-d') }}"/>--}}
                        {{--<span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>--}}
                        {{--<input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"--}}
                               {{--value="{{ \Carbon\Carbon::today()->addDays(15)->format('Y-m-d') }}"/>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">@lang('app.selectProject')</h5>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<select class="select2 form-control" data-placeholder="@lang('app.selectProject')" id="project_id">--}}
                                {{--<option value="all">@lang('app.all')</option>--}}
                                {{--@foreach($projects as $project)--}}
                                    {{--<option--}}
                                            {{--value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">@lang('app.select') @lang('app.client')</h5>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<select class="select2 form-control" data-placeholder="@lang('app.client')" id="clientID">--}}
                                {{--<option value="all">@lang('app.all')</option>--}}
                                {{--@foreach($clients as $client)--}}
                                    {{--<option--}}
                                            {{--value="{{ $client->id }}">{{ ucwords($client->name) }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">@lang('app.select') @lang('modules.tasks.assignTo')</h5>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<select class="select2 form-control" data-placeholder="@lang('modules.tasks.assignTo')" id="assignedTo">--}}
                                {{--<option value="all">@lang('app.all')</option>--}}
                                {{--@foreach($employees as $employee)--}}
                                    {{--<option--}}
                                            {{--value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">@lang('app.select') @lang('modules.tasks.assignBy')</h5>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<select class="select2 form-control" data-placeholder="@lang('modules.tasks.assignBy')" id="assignedBY">--}}
                                {{--<option value="all">@lang('app.all')</option>--}}
                                {{--@foreach($employees as $employee)--}}
                                    {{--<option--}}
                                            {{--value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">@lang('app.select') @lang('app.status')</h5>--}}

                {{--<div class="form-group">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<select class="select2 form-control" data-placeholder="@lang('status')" id="status">--}}
                                {{--<option value="all">@lang('app.all')</option>--}}
                                {{--@foreach($taskBoardStatus as $status)--}}
                                    {{--<option value="{{ $status->id }}">{{ ucwords($status->column_name) }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-3">--}}
                {{--<h5 class="box-title m-t-30">&nbsp;</h5>--}}

                {{--<div class="checkbox checkbox-info">--}}
                    {{--<input type="checkbox" id="hide-completed-tasks">--}}
                    {{--<label for="hide-completed-tasks">@lang('app.hideCompletedTasks')</label>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-12">--}}
                {{--<button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')--}}
                {{--</button>--}}
            {{--</div>--}}
            {{--{!! Form::close() !!}--}}

        {{--</div>--}}
    {{--</div>--}}

    <div class="row mrgn-side submittals">

            <div class="white-box boundry-radius1">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.submittals.createSubmittals') }}" class="btn btn-outline btn-success btn-sm">New Submittals <i class="fa fa-plus" aria-hidden="true"></i></a>
                            {{--<a href="{{ route('admin.taskboard.index') }}" class="btn btn-inverse btn-sm"><i class="ti-layout-column3" aria-hidden="true"></i> @lang('modules.tasks.taskBoard')</a>--}}
                            {{--<a href="javascript:;" id="createTaskCategory" class="btn btn-outline btn-info btn-sm">@lang('modules.taskCategory.addTaskCategory') <i class="fa fa-plus" aria-hidden="true"></i></a>--}}

                        </div>
                    </div>
                    {{--<div class="col-sm-6 text-right hidden-xs">--}}
                        {{--<div class="form-group">--}}
                            {{--<a href="javascript:;" onclick="exportData()" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered  toggle-circle default footable-loaded footable"  id="tasks-table">
                        <thead>
                            <tr class="solid-border">
                                <th>@lang('app.sno')</th>
                                <th>@lang('app.title')</th>
                                <th>@lang('app.number') & @lang('app.revision')</th>
                                <th>@lang('app.issuedate')</th>
                                <th>@lang('app.manager')</th>
                                <th>@lang('app.distribution')</th>
                                <th>@lang('app.location')</th>
                                <th>@lang('app.private')</th>
                                <th>@lang('app.type')</th>
                                <th>@lang('app.status')</th>
                                <th>@lang('app.addedby')</th>
                                <th>@lang('app.action')</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $key=>$item)
                            <tr class="solid-border">
                                <td class="td-padding-rfi">{{ $key+1 }}</td>
                                <td class="td-padding-rfi">{{ $item->title }}</td>
                                <td class="td-padding-rfi">{{ $item->number }} - {{ $item->revision }}</td>
                                <td class="td-padding-rfi">{{ date('d M Y',strtotime($item->issuedate)) }}</td>
                                <td>
                                   {!! get_users_employee_name($item->submittal_manager) !!}
                                </td>
                                <td>
                                    {!! get_users_employee_name($item->distribution) !!}
                                </td>
                                <td class="td-padding-rfi">{{ $item->location }}</td>
                                <td class="td-padding-rfi"><?php if($item->private == '1'){ echo 'Private'; }else{ echo 'Public'; } ?>{{ $item->public }}</td>
                                <td class="td-padding-rfi">{{ $item->type }}</td>
                                <td class="td-padding-rfi">{{ $item->status }}</td>
                                <td>
                                    {!! get_users_images($item->added_by) !!}
                                    {!! get_user_name($item->added_by) !!}
                                </td> 
                                <td class="td-status-padding-rfi">
                                    <a href="{{ route('admin.submittals.details', $item->id)}}" class="btn1 btn-success btn-circle view-btn"
                                       data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                    <a href="{{ route('admin.submittals.editSubmittals', $item->id)}}" class="btn1 btn-info btn-circle edit-btn"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a href="javascript:;" class="btn1 btn-danger btn-circle sa-params"
                                       data-toggle="tooltip" data-task-id="{{ $item->id }}" data-original-title="Delete">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
          </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editTimeLogModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Sub Task e</span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@if($global->locale == 'en')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
@else
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
@endif
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script>
    $("#tasks-table").dataTable();

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: '{{ $global->date_picker_format }}',
        language: '{{ $global->locale }}',
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('task-id');
        var recurring = $(this).data('recurring');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted punch item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm' || isConfirm == 'recurring') {

                var url = "{{ route('admin.submittals.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};

                if(isConfirm == 'recurring')
                {
                    dataObject.recurring = 'yes';
                }

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        if (response.status == "success") {
//                            $.unblockUI();
//                            table._fnDraw();
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });

    $('#tasks-table').on('click', '.show-task-detail', function () {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");

        var id = $(this).data('task-id');
        var url = "{{ route('admin.submittals.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    })

</script>
@endpush