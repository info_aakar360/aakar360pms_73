@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.submittals.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
    .awaans{
        background-color: #848082;
        color: #fff;
        font-weight: bold;
    }
    .awaanstext{
        color: #fff;
        font-weight: bold;
    }
    .dropzone .dz-message {
        text-align: center;
        margin: 24% 0 !important;
    }
    .bakash {
        background-color: #efefef;
    }
    .bakash tbody tr{
        border-radius: 0px !important;
        box-shadow:0px 0px 0px 0px;
    }
</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                Prepare Submittals
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p><b>Submittal Revision :</b> {{ $submittals->number.' - '.$submittals->revision }}:</p>
                                {!! $submittals->title  !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h3><b>Step 2:</b> Prepare Submittal to distribute</h3>
                        {!! Form::open(['id'=>'submitResponse','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="col-md-12 form-group">
                            <label>Select Contractor</label>
                            <select class="selectpicker form-control" name="contractor" data-style="form-control" required>
                                <option value="">Please Select Contractor</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <?php if($workflow){ ?>
                        <div class="col-md-12 form-group">
                            <label>Response</label>
                            <div class="col-md-12 bakash">
                                <table>
                                    <tr>
                                        <td> Name:</td>
                                        <td>  {{ get_user_name($workflow->user_id) }}</td>
                                    </tr>
                                    <tr>
                                        <td> Response:</td>
                                        <td>   {{ $workflow->response }}</td>
                                    </tr>
                                    <tr>
                                        <td> Comment:</td>
                                        <td>   {!! $workflow->comments ?: 'N/A' !!}</td>
                                    </tr>
                                    <tr>
                                        <td> Attachments:</td>
                                        <td>  <?php $replyfiles = \App\SubmittalsFile::where('submittals_id',$submittals->id)->where('workflow_id',$workflow->id)->get();
                                            if(count($replyfiles)>0){
                                            ?>
                                            @foreach($replyfiles as $file)
                                                <?php
                                                $fx = explode('.', $file->hashname);
                                                $ext = $fx[(count($fx)-1)];
                                                $html5class = '';
                                                if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                    $html5class = 'html5lightbox';
                                                }
                                                ?>
                                                <div class="row" id="fileid{{ $file->id }}" >
                                                    {{ $file->filename }}
                                                    <div class="edit-rfi">
                                                        @if($file->external_link != '')
                                                            <a target="_blank" href="{{ $file->external_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle"   {{ $html5class }}><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 'local')
                                                            <a target="_blank" href="{{ asset_url('submittals-files/'.$submittals->id.'/'.$file->hashname) }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 's3')
                                                            <a target="_blank" href="{{ $url.'submittals-files/'.$submittals->id.'/'.$file->hashname }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'google')
                                                            <a target="_blank" href="{{ $file->google_url }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'dropbox')
                                                            <a target="_blank" href="{{ $file->dropbox_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                        class="fa fa-search"></i></a>
                                                        @endif
                                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                           class="btn1 btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                        <span class="detail-month">{{ $file->created_at->diffForHumans() }}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <?php }?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                         <?php   }?>

                        <div class="col-md-12 form-group">
                            <label>Message: </label>
                            <textarea class="form-group summernote" name="comment"></textarea>
                        </div>
                        <div class="form-group text-right">
                        <a href="{{ route('admin.submittals.details',[$submittals->id]) }}" class="btn btn-danger " >Cancel</a>
                        <button id="dissubmittals" class="btn btn-primary " >Submit</button>
                            {{ csrf_field() }}
                            <input type="hidden" name="submittalsid" value="{{ $submittals->id }}"/>
                            <input type="hidden" name="workflowid" value="{{ $workflow->id }}"/>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>


<script>
    $('#dissubmittals').click(function () {
        $.easyAjax({
            url: '{{route('admin.submittals.storeprepareSubmittals')}}',
            container: '#storeTask',
            type: "POST",
            data: $('#submitResponse').serialize(),
            success: function (data) {
                $('#submitResponse').trigger("reset");
                $('.summernote').summernote('code', '');
                var msgs = "Response submitted Successfully";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('admin.submittals.index') }}'
            }
        })
    });
    jQuery('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.punch-items.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });
</script>

@endpush
