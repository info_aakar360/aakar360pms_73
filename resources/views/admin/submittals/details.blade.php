@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('admin.submittals.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
    .awaans{
        background-color: #848082;
        color: #fff;
        font-weight: bold;
    }
    .awaanstext{
        color: #fff;
        font-weight: bold;
    }
    .dropzone .dz-message {
        text-align: center;
        margin: 24% 0 !important;
    }
</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                 <div class="row">
                     <div class="col-md-10">Submittals Details</div>
                 <div class="col-md-2">   <?php
                     if ( $submittals->submittals_manager == \Illuminate\Support\Facades\Auth::user()->id){ ?>
                     <a href="{{ route('admin.submittals.editSubmittals', $submittals->id)}}" class="btn1 btn-info btn-circle edit-btn"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     <?php } ?>
                 </div>
                 </div>

            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <div class="row form-group">
                        <div class="col-md-8">
                            <p><b>Submittal Revision :</b> {{ $submittals->number.' - '.$submittals->revision }}</p>
                            {!! $submittals->title  !!}
                        </div>
                        <div class="col-md-4">
                            <?php
                            if ( $distributecount == 0){
                            if(!empty($submittals->distributed_to)){  ?>
                            <a href="javascript:void(0);" class="btn btn-primary"
                               data-toggle="tooltip" data-original-title="Redistribute Submittals"><i class="fa fa-support"></i> Redistribute</a>
                                 @if ( $submittals->revisesubmittals_id == 0){
                                     <a href="{{ route('admin.submittals.reviseSubmittalsPost', $submittals->id)}}" class="btn btn-warning"
                                   data-toggle="tooltip" data-original-title="Revise Submittals"><i class="fa fa-reply-all"></i> Revise Submittals</a>
                                @endif
                                <?php }else{ ?>
                            <a href="{{ route('admin.submittals.distributeSubmittals', $submittals->id)}}" class="btn btn-primary"
                               data-toggle="tooltip" data-original-title="Distribute Submittals"><i class="fa fa-support"></i> Distribute</a>
                            <?php } } ?>

                        </div>
                    </div>
                    {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.title')</b></td>
                                        <td  style="width: 35%;">{{ $submittals->title }}</td>
                                        <td style="width: 15%;"><b>Number & Revisions</b></td>
                                        <td  style="width: 35%;">{{ $submittals->number }} - {{ $submittals->revision }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Draft</b></td>
                                        <td  style="width: 35%;">@if($submittals->draft=='1') <i class="fa fa-check-circle"></i> @else  <i class="fa fa-times"></i> @endif  </td>
                                        <td style="width: 15%;"><b>Public or Private</b></td>
                                        <td  style="width: 35%;"><?php if($submittals->private == '1'){ echo 'Private';}else{ echo 'Public'; }?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Status</b></td>
                                        <td  style="width: 35%;">
                                            <?php if($submittals->status == 'Open'){ echo 'Open'; } ?>
                                            <?php if($submittals->status == 'Closed'){ echo 'Closed'; } ?>
                                        </td>
                                        <td style="width: 15%;"><b>Submittals Manager</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_employee_name($submittals->submittals_manager)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Submit By</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->submitdate) }}
                                        </td>
                                        <td style="width: 15%;"><b>Issue Date</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->issuedate) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Received By</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->receiveddate) }}
                                        </td>
                                        <td style="width: 15%;"><b>Final Due Date</b></td>
                                        <td  style="width: 35%;">
                                            {{ get_input_date($submittals->finalduedate) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Responsible Contractor</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_contractor_name($submittals->rec_contractor)) }}
                                        </td>
                                        <td style="width: 15%;"><b>Received From</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_users_contractor_name($submittals->received_from)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Type</b></td>
                                        <td  style="width: 35%;">
                                            {{ $submittals->type }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Distribution List</b></td>
                                        <td  style="width: 35%;">
                                            <?php $as = explode(',',$submittals->distribution);
                                            for($i = 0; $i < count($as); $i++ ){
                                            ?>
                                            {{ ucwords(get_users_employee_name($as[$i])) }},
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Location</b></td>
                                        <td  style="width: 35%;">{{ $submittals->location }}</td>
                                        <td style="width: 15%;"><b>Drawing No.</b></td>
                                        <td  style="width: 35%;">{{ $submittals->drawing_no }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Added By</b></td>
                                        <td  style="width: 35%;">{{ ucwords(get_user_name($submittals->added_by)) }} </td>
                                        <td style="width: 15%;"><b>Project</b></td>
                                        <td  style="width: 35%;">{{ get_project_name($submittals->projectid) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Project title</b></td>
                                        <td  style="width: 35%;">{{ get_title($submittals->titleid) }}</td>
                                        <td style="width: 15%;"><b>Task</b></td>
                                        <td  style="width: 35%;">{{ get_cost_name($submittals->costitemid) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;"><b>Submital Schedule Information</b></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>On Site Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->onsitedate) }}</td>
                                        <td style="width: 15%;"><b>Lead time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->leadtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Return Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->planreturndate) }}</td>
                                        <td style="width: 15%;"><b>Design Team review time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->designreviewtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Internal review completed Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->planinternaldate) }}</td>
                                        <td style="width: 15%;"><b>Internal review time</b></td>
                                        <td  style="width: 35%;">{{  $submittals->internalreviewtime }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Planned Submit Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->plansubmitdate) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Anticipated Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->anticipateddeliverydate) }}</td>
                                        <td style="width: 15%;"><b>Schedule task</b></td>
                                        <td  style="width: 35%;">{{  $submittals->scheduletask }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Confirm Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->confirmdeliverydate) }}</td>
                                        <td style="width: 15%;"><b>Actual Delivery Date</b></td>
                                        <td  style="width: 35%;">{{ get_input_date($submittals->actualdeliverydate) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Ball in Court</b></td>
                                        <td  style="width: 35%;">{{ ucwords(get_users_employee_name($submittals->ballincourt)) }} </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Description</b></td>
                                        <td  style="width: 35%;">{!! $submittals->description !!} </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12">&nbsp;</div>

                            <div class="col-md-12">
                                <b>Files</b>
                                <br>
                                <div class="row" id="list">
                                    @foreach($files as $file)
                                        <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                            @if($file->external_link != '')
                                                <?php $imgurl = $file->external_link;?>
                                            @elseif($storage == 'local')
                                                <?php $imgurl = uploads_url().'submittals-files/'.$submittals->id.'/'.$file->hashname;?>
                                            @elseif($storage == 's3')
                                                <?php $imgurl = awsurl().'/submittals-files/'.$submittals->id.'/'.$file->hashname;?>
                                            @elseif($storage == 'google')
                                                <?php $imgurl = $file->google_url;?>
                                            @elseif($storage == 'dropbox')
                                                <?php $imgurl = $file->dropbox_link;?>
                                            @endif
                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                            <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                                Remove
                                            </a>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h3>Submittal Worflow</h3>
                                    <table>
                                        <thead>
                                        <tr>
                                            <td>Name</td>
                                            <td>Role</td>
                                            <td>Sent date</td>
                                            <td>Due date</td>
                                            <td>Returned date</td>
                                            <td>Response</td>
                                            <td>Comment</td>
                                            <td>Files</td>
                                            <td>Reg Date</td>
                                            <td>Action</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($workflowarray as $reply)
                                            <tr>
                                                <td><b>{{ get_user_name($reply->user_id) }}</b></td>
                                                <td> {!! $reply->role !!}</td>
                                                <td><b>{{ get_input_date($reply->sentdate) }}</b></td>
                                                <td><b>{{ get_input_date($reply->duedate) }}</b></td>
                                                <td><b>{{ get_input_date($reply->returneddate) }}</b></td>
                                                <td> {!! $reply->response !!}</td>
                                                <td> {!! $reply->comments !!}</td>
                                                <td>  <?php $replyfiles = \App\SubmittalsFile::where('submittals_id',$submittals->id)->where('workflow_id',$reply->id)->get();
                                                    if(count($replyfiles)>0){
                                                    ?>
                                                    @foreach($replyfiles as $file)
                                                        <?php
                                                        $fx = explode('.', $file->hashname);
                                                        $ext = $fx[(count($fx)-1)];
                                                        $html5class = '';
                                                        if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                            $html5class = 'html5lightbox';
                                                        }
                                                        ?>
                                                        <div class="row" id="fileid{{ $file->id }}" >
                                                            {{ $file->filename }}
                                                            <div class="edit-rfi">
                                                                @if($file->external_link != '')
                                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn1 btn-info btn-circle"   {{ $html5class }}><i
                                                                                class="fa fa-search"></i></a>

                                                                @elseif(config('filesystems.default') == 'local')
                                                                    <a target="_blank" href="{{ asset_url('submittals-files/'.$submittals->id.'/'.$file->hashname) }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                                class="fa fa-search"></i></a>

                                                                @elseif(config('filesystems.default') == 's3')
                                                                    <a target="_blank" href="{{ $url.'submittals-files/'.$submittals->id.'/'.$file->hashname }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif(config('filesystems.default') == 'google')
                                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                                class="fa fa-search"></i></a>
                                                                @elseif(config('filesystems.default') == 'dropbox')
                                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                                       data-toggle="tooltip" data-original-title="View"
                                                                       class="btn1 btn-info btn-circle {{ $html5class }}"><i
                                                                                class="fa fa-search"></i></a>
                                                                @endif
                                                                <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                                   class="btn1 btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                                <span class="detail-month">{{ $file->created_at->diffForHumans() }}</span>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <?php }?>
                                                </td>
                                                <td>{{ date('d/M/Y',strtotime($reply->created_at)) }}</td>

                                                <td>@if(empty($reply->response)) <a href="{{ route('admin.submittals.wrkresponse',[$reply->id]) }}" class="btn1 btn-primary btn-circle"><i class="fa fa-edit"></i> </a>@endif</td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!--/row-->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


<script>

function showDiv() {
    $('#replyDiv').toggleClass('hide', 'show');
}
Dropzone.autoDiscover = false;
//Dropzone class
myDropzone = new Dropzone("div#file-upload-dropzone", {
    url: "{{ route('admin.submittals.storeImage') }}",
    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
    paramName: "file",
    maxFilesize: 10,
    maxFiles: 10,
    acceptedFiles: "image/*,application/pdf",
    autoProcessQueue: false,
    uploadMultiple: true,
    addRemoveLinks:true,
    parallelUploads:10,
    init: function () {
        myDropzone = this;
    }
});

myDropzone.on('sending', function(file, xhr, formData) {
    console.log(myDropzone.getAddedFiles().length,'sending');
    var ids = '{{ $submittals->id }}';
    var replyid = $('#replyID').val();
    formData.append('submittals_id', ids);
    formData.append('submittalstype', 'reply');
    formData.append('reply_id', replyid);
});

myDropzone.on('completemultiple', function () {
    var msgs = "@lang('RFI Updated successfully')";
    $.showToastr(msgs, 'success');
    window.location.href = '{{ route('admin.submittals.index') }}'
});

    //    update task
    $('#update-task').click(function () {
        $.easyAjax({
            url: '{{route('admin.submittals.replyPost', [$submittals->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function (data) {
                $('#storeTask').trigger("reset");
                $('.summernote').summernote('code', '');
                if(myDropzone.getQueuedFiles().length > 0){
                    rifID = data.rifID;
                    $('#rifID').val(data.rifID);
                    $('#replyID').val(data.replyID);
                    myDropzone.processQueue();
                }
                else{
                    var msgs = "@lang('Reply posted successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.submittals.index') }}'
                }
            }
        })
    });

    //    update task
    function removeFile(id) {
        var url = "{{ route('admin.submittals.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })
    };

    function officialresponse(id) {
        var url = "{{ route('admin.submittals.officialResponse') }}";
        var token = "{{ csrf_token() }}";
        var submittals = "{{ $submittals->id }}";
        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token,'replyid': id,'submittals': submittals},
            success: function(response){

            }
        })
    };

    function updateTask(){
        $.easyAjax({
            url: '{{route('admin.submittals.replyPost', [$submittals->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                var msgs = "@lang('Reply posted successfully')";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('admin.submittals.index') }}'
            }
        })
    }

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('admin.submittals.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });
</script>

@endpush
