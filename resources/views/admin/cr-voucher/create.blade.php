@extends('layouts.app')
<?php
$moduleName = " Cr Voucher";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Create";
$breadcrumbMainIcon = "account_balance_wallet";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\Transaction';
$ParentRouteName = 'admin.cr_voucher';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Put {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick"
                                                        name="branch_id">
                                                    <option value="0">Select Branch Name</option>
                                                    @if (App\Branch::all()->count() >0 )
                                                        @foreach( App\Branch::all() as $project )
                                                            <option @if ( $project->id == old('branch_id' ))
                                                                    selected
                                                                    @endif value="{{ $project->id  }}">{{ $project->name  }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick"
                                                        name="bank_cash_id"
                                                        id="">
                                                    <option value="0"> Select Bank Cash Name</option>
                                                    @if (App\BankCash::all()->count() >0 )
                                                        @foreach( App\BankCash::all() as $bank_cash )
                                                            <option @if ( $bank_cash->id == old('bank_cash_id' ))
                                                                    selected
                                                                    @endif value="{{ $bank_cash->id  }}">{{ $bank_cash->name  }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="cheque_number"
                                                       type="text"
                                                       class="form-control" placeholder="Cheque Number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="billData">
                                    <div class="row dr">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <select data-live-search="true"
                                                            class="form-control show-tick income_expense_head_id"
                                                            name="income_expense_head_id[]"
                                                            id="">
                                                        <option value="0"> Select Head of Account Name</option>
                                                        @if (App\IncomeExpenseHead::all()->count() >0 )
                                                            @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                                <option @if ( $HeadOfAccount->id == old('income_expense_head_id' ))
                                                                        selected
                                                                        @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input name="amount[]" type="number" class="form-control amount" placeholder="Amount">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                            <div class="form-group form-float">
                                                <a href="javascript:;" class="btn btn-sm btn-success plus" id="moreButton"><i class="fa fa-plus-circle"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line" id="bs_datepicker_container">
                                                <input autocomplete="off" value="{{ old('voucher_date') }}"
                                                       name="voucher_date"
                                                       type="text" id="start_date"
                                                       class="form-control"
                                                       placeholder="Please choose a date...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                    <textarea name="particulars" rows="2" class="form-control no-resize"
                                                              placeholder="Particulars"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="save-form">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $("#start_date").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.store') }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });

        var $i = 0;
        // Add More Inputs
        $('.plus').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            //append Options
            $('<div class="row" id="drRow'+indexs+'">'
            +'  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">'
            +'      <div class="form-group form-float">'
            +'          <div class="form-line">'
            +'              <select data-live-search="true" class="form-control show-tick income_expense_head_id" name="income_expense_head_id[]" id="">'
            +'                  <option value="0"> Select Head of Account Name</option>'
                                @if (App\IncomeExpenseHead::all()->count() >0 )
                                    @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
            +'                          <option @if ( $HeadOfAccount->id == old('head_of_account_id' )) selected @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>'
                                    @endforeach
                                @endif
            +'              </select>'
            +'          </div>'
            +'      </div>'
            +'  </div>'
            +'  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">'
            +'      <div class="form-group form-float">'
            +'          <div class="form-line">'
            +'              <input name="amount[]" type="number" class="form-control amount" placeholder="Amount">'
            +'          </div>'
            +'      </div>'
            +'  </div>'
            +'  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">'
            +'      <div class="form-group form-float">'
            +'          <a href="javascript:;" class="btn btn-sm btn-danger minus" onclick="removeBox('+indexs+');"><i class="fa fa-minus-circle"></i> </a>'
            +'      </div>'
            +'  </div>'
            +'</div>').appendTo('#billData');
        });
        function removeBox(index){
            $('#drRow'+index).remove();
        }
    </script>
@endpush