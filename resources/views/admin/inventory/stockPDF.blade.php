<!DOCTYPE html>
<html>
<head>
    <title>Inventory Report</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        table {
            border-collapse:collapse; table-layout:fixed;
        }
        table tr td,table tr th {
            border: 1px solid #dae3f4;
            padding:2px;
            font-weight: normal;
            word-wrap:break-word;
            page-break-after: always !important;
        }
        @media screen and (min-width: 601px) {
            table tr td,table tr th {
                font-size: 10px;
            }
        }
        .carddesign{
            background-color: #ebf0f9;
            padding: 8px;
            border-top-left-radius:6px;
            border-top-right-radius:6px;
        }
        @media screen and (max-width: 600px) {
            table tr td,table tr th {
                font-size: 14px;
            }
        }
        footer {
            position: fixed;
            bottom: 0cm;
            right: 0cm;
            height: 1cm;
        }
    </style>
</head>
<body>
<main>
<table class="table">
    <thead>
        <tr>
            <th scope="col" colspan="8"><h6>Project: @if(!empty($project)) {{get_project_name($project)}} @endif</h6></th>
            <th scope="col" colspan="2"><h6>Date: @if(!empty($date)) {{$date}} @endif</h6></th>
        </tr>
    </thead>
</table>
@if(!empty($products))
    <table class="table">
    <thead>
    <tr class="carddesign">
        <th scope="col"><center>S.no.</center></th>
        <th scope="col"><center>Product Name</center></th>
        <th scope="col"><center>Unit</center></th>
        <th scope="col"><center>Opening Stock</center></th>
        <th scope="col"><center>Material Received</center></th>
        <th scope="col"><center>Purchase Return</center></th>
        <th scope="col"><center>Material Issue</center></th>
        <th scope="col"><center>Issue Retrun</center></th>
        <th scope="col"><center>Shortage &  Excess</center></th>
        <th scope="col"><center>Closing Balance</center></th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $key=>$data)
        <tr>
            <th scope="row"><center>{{$key+1}}</center></th>
            <td><center>{{get_local_product_name($data->cid)}}</center></td>
            <td><center>{{get_unit_name($data->unit)}}</center></td>
            <td><center>{{get_opening_stock($data->cid)}}</center></td>
            <td><center>{{get_material_receive($data->cid)}}</center></td>
            <td><center>{{get_purchase_return($data->cid)}}</center></td>
            <td><center>{{get_material_issue($data->cid)}}</center></td>
            <td><center>{{get_issue_return($data->cid)}}</center></td>
            <td><center>{{get_shortage_excess($data->cid)}}</center></td>
            <td><center>{{$data->stock  }}</center></td>
        </tr>
    @endforeach
    </tbody>
</table>
@endif
</main>
<footer>
    <center><p style="font-size: 17px;">Powered By: <img src="{{ public_path('uploads/aakar-logo.png') }}" width="100px" style="margin-top:5px;" /></p></center>
</footer>
</body>
</html>