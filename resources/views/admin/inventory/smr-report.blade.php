<!DOCTYPE html>
<html>
<head>
    <title>Material Report</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        table {
            border-collapse:collapse; table-layout:fixed;
        }
        table tr td,table tr th {
            border: 1px solid #dae3f4;
            padding:2px;
            font-weight: normal;
            word-wrap:break-word;
            page-break-after: always !important;
        }
        .carddesign{
            background-color: #ebf0f9;
            padding: 8px;
            border-top-left-radius:6px;
            border-top-right-radius:6px;
        }
        @media screen and (min-width: 601px) {
            table tr td,table tr th {
                font-size: 10px;
            }
        }
        @media screen and (max-width: 600px) {
            table tr td,table tr th {
                font-size: 14px;
            }
        }
        footer {
            position: fixed;
            bottom: 0cm;
            right: 0cm;
            height: 1cm;
        }
    </style>
</head>
<body>
<main>
    <table class="table">
        <thead>
            <tr>
                <th scope="col" colspan="4"><center><h6>Project: @if(!empty($project)) {{get_project_name($project)}} @endif</h6></center></th>
                <th scope="col" colspan="2"><center><h6>Material: @if(!empty($product)) {{get_local_product_name($product)}} @endif</h6></center></th>
                <th scope="col" colspan="2"><center><h6>Unit: @if(!empty($product)) {{get_unit_by_product($product)}} @endif</h6></center></th>
                <th scope="col" colspan="2"><center><h6>Date: @if(!empty($date)) {{$date}} @endif</h6></center></th>
            </tr>
        </thead>
    </table>
    @if(!empty($productLogs))
        <table class="table">
        <thead>
        <tr class="carddesign">
            <th scope="col"><center>S.no.</center></th>
            <th scope="col" colspan="3"><center>Description</center></th>
            <th scope="col"><center>Received</center></th>
            <th scope="col"><center>Purchase Return</center></th>
            <th scope="col"><center>Issue</center></th>
            <th scope="col"><center>Issue Return</center></th>
            <th scope="col"><center>Shortage &  Excess</center></th>
            <th scope="col"><center>Running Balance</center></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $length = count($productLogs);
        $closingbalance = 0;
        foreach ($productLogs as $key=>$log) {
            $k = $key+1;
            $plusQ = '-';
            $minusQ = '-';
            $purcahseReturn = '-';
            $issueReturn = '-';
            $ShortExcess = '-';
            $recieving = 0;
            $issue = 0;
            if($log->transaction_type == 'plus'){
                $plusQ = $log->quantity;
                $recieving = $log->quantity;
            }
            if($log->transaction_type == 'minus'){
                $minusQ = $log->quantity;
                $issue = $log->quantity;
            }
            if($log->module_name == 'update-stock' && $log->transaction_type == 'minus'){
                $ShortExcess = '<div  style="text-align: center; color: black;background-color: rgba(255, 0, 0, 0.3);">-'.$log->quantity.'</div>';
            }if($log->module_name == 'update-stock' && $log->transaction_type == 'plus'){
                $ShortExcess = '<div style="text-align: center; color: black;background-color:: rgba(0, 128, 0, 0.3);">+'.$log->quantity.'</div>';
            }if($log->module_name == 'product_return'){
                $purcahseReturn = $log->quantity;
            }if($log->module_name == 'product_issue_return'){
                $issueReturn = $log->quantity;
            }
            if($key == 0){
                $runningbalance  = (int)$recieving - (int)$issue;
            }else{
                $runningbalance  = ((int)$runningbalance + (int)$recieving) - (int)$issue;
            }
            echo '<tr>
                <td><center>'.$k.'</center></td>
                <td colspan="3"><center>';
            if($log->module_name == 'update-stock'){
                if($log->transaction_type == 'minus') {
                    echo 'Shortage';
                }else {
                    echo 'Access';
                }
            }else {
                if ($log->transaction_type == 'minus') {
                    echo 'Issued';
                } else {
                    echo 'Received';
                }
            }
            echo ' By '.get_user_name($log->created_by).'<br>';
            echo (!empty($log->remark)) ? 'Remark : '.$log->remark : '';
            echo '</center></td>
                    <td><center>'.$plusQ.'</center></td>
                    <td><center>'.$purcahseReturn.'</center></td>
                    <td><center>'.$minusQ.'</center></td>
                    <td><center>'.$issueReturn.'</center></td>
                    <td><center>'.$ShortExcess.'</center></td>
                    <td><center>'.$runningbalance.'</center></td>
                </tr>';
            if($key+1 ==$length){
                $closingbalance = $runningbalance;
            }
        }
        ?>
        <tr>
            <td colspan="9" style="text-align: right">Closing Balance</td>
            <td id="closingbalance"><center>{{$closingbalance}}</center></td>
        </tr>
        </tbody>
    </table>
    @endif
</main>
<footer>
    <center><p style="font-size: 17px;">Powered By: <img src="{{ public_path('uploads/aakar-logo.png') }}" width="100px" style="margin-top:5px;" /></p></center>
</footer>
</body>
</html>