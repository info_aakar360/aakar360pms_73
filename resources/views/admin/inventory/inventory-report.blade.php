@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3 " style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form method="post" action="{{ route('admin.inventory.inventoryReportPdf') }}">
            {{csrf_field()}}
            <div class="col-md-12">
                <div class="example">
                    <h5 class="box-title">Date</h5>
                    <div class="input-group">
                        <input type="date" class="form-control" name="date" id="date"  value="" />
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">Product Category</h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="category" data-placeholder="Select Category" id="category_id">
                                <option value="">@lang('modules.client.all')</option>
                                @foreach($category as $cat)
                                    <option value="{{ $cat->id }}">{{ ucwords($cat->name) }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">Project</h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="project" data-placeholder="Select Project" id="project_id">
                                <option value="">@lang('modules.client.all')</option>
                                @foreach($projectarray as $project)
                                    <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">Product</h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="product" data-placeholder="Select Product" id="product_id">
                                <option value="">@lang('modules.client.all')</option>
                                @foreach($productarray as $product)
                                    <option value="{{ $product->id }}">{{ ucwords($product->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title">@lang('app.report') </h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="form-control" name="reporttype[]"  data-placeholder="@lang('app.report')" id="reporttypelist">
                                <option value="">Select Report type</option>
                                <option value="inventory">Inventory Report</option>
                                <option value="material">Material Report</option>
                                <option value="material-category">Material Category Report</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
                </div>
                {{--<div class="col-md-4">
                    <button type="submit" class="btn btn-warning btn-sm" name="exporttype" value="indentpdf" ><i class="fa fa-file"></i> Inventory PDF</button>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-warning btn-sm" name="exporttype" value="smrpdf" ><i class="fa fa-file"></i>SMR PDF</button>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-warning btn-sm" name="exporttype" value="mcrpdf" ><i class="fa fa-file"></i>MCR PDF</button>
                </div>--}}
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12" >
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="pull-left">
                            <h2 style="color: #002f76">Inventory Report</h2>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="pull-right">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                        class="fa fa-cog"></i></a>
                            {{-- <a onclick="exportTimeLog()" class="btn btn-info"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                        </div>
                    </div>
                </div>

                <div class="table-responsive m-t-10" id="reporthtml">
                    {!! $reporthtml !!}
                </div>
            </div>
        </div>

    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

    <script>
        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });
        $('#filter-results').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        var table;
        function showTable(){
            var projectID = $('#project_id').val();
            var categoryID = $('#category_id').val();
            var productID = $('#product_id').val();
            var date = $('#date').val();
            var reporttypelist = $('#reporttypelist').val();
            /*var month = $('#month').val();
            var startdate = $('#start-date').val();
            var enddate = $('#end-date').val();*/
            $.ajax({
                url: '{{ route('admin.inventory.reportHtml') }}',
                type: "POST",
                data: {
                    'project_id': projectID,
                    'category_id': categoryID,
                    'product_id': productID,
                    'date': date,
                    'reporttype': reporttypelist,
                    '_token': '{{ csrf_token() }}'
                },
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function (response) {
                    $(".preloader-small").hide();
                    $("#reporthtml").html('');
                    $("#reporthtml").html(response);
                }
            })
        }

        $('#filter-results').click(function () {
            showTable();
        });
        $('body').on('click', '.sa-params', function(){
            var id = $(this).data('time-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted time log!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.projects.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                table._fnDraw();
                            }
                        }
                    });
                }
            });
        });

        $('body').on('click', '.edit-time-log', function () {
            var id = $(this).data('time-id');
            var url = '{{ route('admin.projects.edit', ':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html('Update Time Log');
            $.ajaxModal('#editTimeLogModal', url);
        });
        $('#start_time, #end_time').timepicker({
            @if($global->time_format == 'H:i')
            showMeridian: false
            @endif
        }).on('hide.timepicker', function (e) {
            calculateTime();
        });
        jQuery('#start_date, #end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        }).on('hide', function (e) {
            calculateTime();
        });
        function calculateTime() {
            var startDate = $('#start_date').val();
            var endDate = $('#end_date').val();
            var startTime = $("#start_time").val();
            var endTime = $("#end_time").val();
            var timeStart = new Date(startDate + " " + startTime);
            var timeEnd = new Date(endDate + " " + endTime);
            var diff = (timeEnd - timeStart) / 60000; //dividing by seconds and milliseconds
            var minutes = diff % 60;
            var hours = (diff - minutes) / 60;
            if (hours < 0 || minutes < 0) {
                var numberOfDaysToAdd = 1;
                timeEnd.setDate(timeEnd.getDate() + numberOfDaysToAdd);
                var dd = timeEnd.getDate();
                if (dd < 10) {
                    dd = "0" + dd;
                }
                var mm = timeEnd.getMonth() + 1;
                if (mm < 10) {
                    mm = "0" + mm;
                }
                var y = timeEnd.getFullYear();
                $('#end_date').val(mm + '/' + dd + '/' + y);
                calculateTime();
            } else {
                $('#total_time').html(hours + "Hrs " + minutes + "Mins");
            }

//        console.log(hours+" "+minutes);
        }
        /*function exportTimeLog(){

           var projectID = $('#project_id').val();
            var contractor = $('#contractor').val();
            var subprojectID = $('#titlelist').val();
            var segmentID = $('#segmentlist').val();
            var month = $('#month').val();

            var url = '{{ route('admin.projects.exportprogressreport', [':projectId',':subprojectID',':segmentID', ':month',  ':contractor']) }}';
        url = url.replace(':projectId', projectID);
        url = url.replace(':contractor', contractor);
        url = url.replace(':subprojectID', subprojectID);
        url = url.replace(':segmentID', segmentID);
        url = url.replace(':month', month);
        window.location.href = url;
    }*/
        $('#project_id').change(function () {
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        var projectdata = JSON.parse(data);
                        var titles = '<option value="">Select Title</option>';
                        var cositem = '<option value="">Select Cost item</option>';
                        if(projectdata.titles){
                            $.each( projectdata.titles, function( key, value ) {
                                titles += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#titlelist").html("");
                        $("select#titlelist").html(titles);
                        $('select#titlelist').select2();
                        if(projectdata.cositems){
                            $.each( projectdata.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projectcostitems') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        var titles = JSON.parse(data);
                        var cositem = '';
                        if(titles.cositems){
                            $.each( titles.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
    </script>
@endpush