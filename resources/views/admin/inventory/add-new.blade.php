@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Add New GRN</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <div class="form-body">
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Project (*)</label>
                                        <select class="form-control select2" name="project_id" id="project" data-style="form-control" required>
                                            <option value="">Select Project</option>
                                            @forelse($projects as $supplier)
                                                <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.inventory.store') (*)</label>
                                        <select class="form-control select2" name="store_id" id="store" data-style="form-control" required>
                                            <option value="">Select Store</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.inventory.supplier') (*)</label>
                                        <select class="form-control select2" name="supplier_id" id="supplier" data-style="form-control" required>
                                            <option value="">Select Supplier</option>
                                            @forelse($suppliers as $supplier)
                                                <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><b>GRN @lang('modules.inventory.dated')</b></label>
                                        <input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <!--/span-->
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>@lang('modules.po.poNumber')</b></label>--}}
                                        {{--<input type="text" name="po_number" value="NA" class="form-control"/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>PO @lang('modules.po.date')</b></label>--}}
                                        {{--<input type="date" name="po_dated" value="" class="form-control"/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>GRN @lang('modules.inventory.dated')</b></label>--}}
                                        {{--<input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <!--/span-->
                            </div>
                            {{--<hr>--}}
                            {{--<h3 class="box-title">@lang('modules.inventory.invoiceDetail')</h3>--}}
                            {{--<hr>--}}
                            <div class="row">
                                <!--/span-->
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>@lang('modules.inventory.invoiceNo')</b></label>--}}
                                        {{--<input class="form-control" name="invoice_no" type="text" value="NA" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>@lang('modules.inventory.dated')</b></label>--}}
                                        {{--<input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.inventory.remark')</b></label>
                                        <textarea name="inv_remark" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.inventory.paymentTerms')</b></label>
                                        <textarea name="payment_terms" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                            <hr>
                            <div class="row" style="background-color: #efefef; padding-top: 5px;">
                                <div class="proentry">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Product</label>
                                            <select class="form-control select2" name="product" id="bomproducts" data-style="form-control product">
                                                <option value="">Select Product</option>
                                                {{--@forelse($products as $product)
                                                    <option value="{{$product->id}}">{{ $product->name }}</option>
                                                @empty
                                                @endforelse--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.brand')</label>
                                            <select class="form-control select2" name="brand" data-style="form-control brand">
                                                <option value="">Select Brand</option>
                                                @forelse($brands as $brand)
                                                    <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.quantity')</label>

                                            <input type="number" name="quantity" value="" class="form-control quantity" min="1" placeholder="Enter Quantity">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.unit')</label>
                                            <input type="text" readonly style="display: none;" value="" class="form-control quantity" name="unit_id" id="unit">
                                            <input type="text" readonly value="" class="form-control quantity" name="unit_id_name" id="unitData">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.inventory.price')</label>
                                            <input type="number" name="price" value="0.00" class="form-control" placeholder="Enter Price">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.inventory.tax') (%)</label>
                                            <input type="number" name="tax" value="0.00" class="form-control" placeholder="Enter Tax">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label" style="width: 100%;">@lang('modules.inventory.action')</label>
                                            <a href="javascript:void(0)" class="btn btn-success add-button" style="color: #ffffff;" >Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                    <table class="table"><thead><th>S.No.</th><th>Product</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody><tr><td style="text-align: center" colspan="9">No Records Found.</td></tr></tbody></table>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <span>Freight (If Extra): </span><input type="number" value="0.00" name="freight" style="margin-right: 15px;"/>
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.inventory.postAddNew')}}',
                container: '#createRfq',
                type: "POST",
                redirect: true,
                data: $('#createRfq').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            var store_id = $('select[name=store_id]').val();
            var project_id = $('select[name=project_id]').val();
            $.ajax({
                url: '{{route('admin.stores.projects.indent.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
                success: function (data) {
                    $('#brandData').html(data.brands);
                    $('#unit').val(data.unit);
                    $('#unitData').val(data.unitData);
                    $("#brandData").select2("destroy");
                    $("#brandData").select2();
                }
            });

        });
        $('#project').change(function () {
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.indent.getStores')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('#store').html(data.stores);
                }
            });
        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('input[name=unit_id]').val();
            var price = $('input[name=price]').val();
            var tax = $('input[name=tax]').val();
            if(cid == '' || qty == '' || qty == 0 || unit == '' || price == '' || tax == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.inventory.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, price: price, tax: tax},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');

            $.ajax({
                url: '{{route('admin.inventory.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });
        /*$(document).on('keyup', '.quantity', function(){
            var inp = $(this);
            var key = inp.data('key');
            var qty = inp.val();
            var act_qty = inp.attr('max');
            var bal = parseFloat(act_qty)-parseFloat(qty);
            //alert(bal);
            if(parseInt(bal) >= 0){
                $('.balanceQty[data-key='+key+']').html(bal);
                $('#save-form').removeAttr('disabled');
            }else{
                alert('Invalid quantity.');
                inp.val(act_qty);
                $('.balanceQty[data-key='+key+']').html('0');
                $('#save-form').attr('disabled', true);
            }

        });*/

        $('#store').change(function () {
            var pid = $('#project').val();
            var sid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.indent.getBomProducts')}}',
                type: 'POST',
                data: {_token: token,pid: pid,sid: sid},
                success: function (data) {
                    $('#bomproducts').html(data.products);
                }
            });
        });
    </script>
@endpush

