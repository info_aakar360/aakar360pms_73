
@if(!empty($reporttype))
    <div class="row col-md-12" >
        <div class="combo-sheet mb-20">
            <div class="table-wrapper">
                <table class="table table-bordered" style="text-align: center; width:100%; table-layout: fixed;border-spacing: 0px;" >
                    <thead>
                    <tr style="border:1px solid #dae3f4;">
                        <th  colspan="5"></th>
                        <th  colspan="3"  style="text-align: center;border:1px solid #dae3f4;" >Quantity</th>
                        <th ></th>
                        <th  colspan="3"  style="text-align: center;border:1px solid #dae3f4;" >Amount</th>
                    </tr>
                    <tr style="border:1px solid #dae3f4;">
                        <th  style="text-align: center;border:1px solid #dae3f4;" >@lang('app.contractor')</th>
                        <th >@lang('app.activity')</th>
                        <th >@lang('app.task')</th>
                        <th >Unit</th>
                        <th >Total Qty</th>
                        <th >Previous</th>
                        <th >Progress</th>
                        <th >Cumulative</th>
                        <th >Rate</th>
                        <th >Previous</th>
                        <th >Progress</th>
                        <th >Cumulative</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($projectid)
                        <?php
                        function reporthtml($reportarray){

                        $projectid = (int)$reportarray['projectid'];
                        $segmentid = $reportarray['segmentid'];
                        $subprojectid = $reportarray['subprojectid'];
                        $parent = $reportarray['parent'];
                        $level = $reportarray['level'];
                        $contracid = $reportarray['contractor'];
                        $dateformat = $reportarray['dateformat'];
                        $startdate = $reportarray['startdate'];
                        $enddate = $reportarray['enddate'];
                        $exportype = $reportarray['exportype'];
                        $proproget = $reportarray['categoryarray'];
                        foreach ($proproget as $propro){
                        $catvalue = $propro->itemid;
                        if(!empty($propro->catlevel)){
                            $catvalue = $propro->catlevel.','.$propro->itemid;
                        }

                        $start_date = $startdate.' 00:00:01';
                        $end_date = $enddate.' 23:59:59';
                        if(!empty($segmentid)){
                            $projectcostitemsarray = \App\Task::join('project_segments_product','project_segments_product.id','=','tasks.cost_item_id')
                                ->join('task_percentage','task_percentage.task_id','=','tasks.id')
                                ->select('tasks.*','project_segments_product.unit','project_segments_product.finalrate','project_segments_product.qty','project_segments_product.contractor','project_segments_product.description')
                                ->where('project_segments_product.position_id',$propro->id)->where('project_segments_product.project_id',$projectid)
                                ->where('project_segments_product.segment',$segmentid);
                            if(!empty($subprojectid)){
                                $projectcostitemsarray = $projectcostitemsarray->where('project_segments_product.title',$subprojectid);
                            }

                        }else{
                            $projectcostitemsarray = \App\Task::join('project_cost_items_product','project_cost_items_product.id','=','tasks.cost_item_id')
                                ->leftjoin('task_percentage','task_percentage.task_id','=','tasks.id')
                                ->select('tasks.*','project_cost_items_product.unit','project_cost_items_product.finalrate','project_cost_items_product.qty','project_cost_items_product.contractor','project_cost_items_product.description')
                                ->where('project_cost_items_product.position_id',$propro->id)->where('project_cost_items_product.project_id',$projectid);

                            if(!empty($subprojectid)){
                                $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.title',$subprojectid);
                            }
                            if(!empty($contracid)){
                                $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor',$contracid);
                            }else{
                                $projectcostitemsarray = $projectcostitemsarray->where('project_cost_items_product.contractor','0');
                            }
                        }
                        if(!empty($start_date)&&!empty($end_date)){
                            $projectcostitemsarray = $projectcostitemsarray->where('task_percentage.created_at','>=',$start_date)->where('task_percentage.created_at','<=',$end_date);
                        }
                        $projectcostitemsarray = $projectcostitemsarray->groupby('task_percentage.task_id')->get();
                        if(count($projectcostitemsarray)>0){
                        ?>
                        <tr class="subcat">
                            <td style="border:1px solid #000;border-bottom:1px solid #000;margin: 0px;padding: 5px;"></td>
                            <td  style="border:1px solid #000;border-right:0px;margin: 0px;padding: 5px;"><strong>{{ ucwords($propro->itemname) }}</strong></td>
                            <td style="border:1px solid #000;border-bottom:1px solid #000;margin: 0px;padding: 5px;" colspan="10"></td>
                        </tr>
                        <?php
                        foreach ($projectcostitemsarray as $projectcostitems){
                        if(!empty($projectcostitems)){
                        $totalquantity = !empty($projectcostitems->qty) ? $projectcostitems->qty : 0;
                        $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
                        $progress = $prevcumiliquantity = 0;
                        $prevcumiliquantity = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->sum('todayqty');
                        $cumiliquantitytill = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','<',$start_date)->orderBy('id','desc')->first();
                        $cumiliquantitypercenttill = !empty($cumiliquantitytill) ? $cumiliquantitytill->percentage : 0;
                        if(!empty($cumiliquantity)&&!empty($totalquantity)){
                            $cumiliquantitypercenttill =   ($cumiliquantity/$totalquantity)*100;
                        }
                        $progress = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->sum('todayqty');
                        $percentagetable = \App\TaskPercentage::where('task_id',$projectcostitems->id)->where('moduletype','percentage')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->orderBy('id','desc')->first();
                        $percentage = !empty($percentagetable) ? $percentagetable->percentage : 0;
                        $percentage = $percentage-$cumiliquantitypercenttill;
                        if(!empty($progress)&&!empty($totalquantity)){
                            $percentage =  !empty($progress) ? ($progress/$totalquantity)*100 : 0;
                        }
                        $cumiliquantity = $prevcumiliquantity+$progress;
                        $cumiliquantitypercent = $cumiliquantitypercenttill+$percentage;
                        $rate = $projectcostitems->finalrate;
                        $cumiliquantitytillamt = $prevcumiliquantity*$rate;
                        $cumiliquantityamt = $cumiliquantity*$rate;
                        $progressamt = $progress*$rate;
                        ?>

                        <tr  style="border:1px solid #dae3f4;">
                            @if($projectcostitemsarray[0]->id==$projectcostitems->id)
                                <td  rowspan="<?php echo count($projectcostitemsarray);?>"  ><strong>{{ get_users_contractor_name($projectcostitems->contractor,$projectcostitems->company_id) ?: 'Departmental' }}</strong></td>
                            @endif
                            <td  ></td>
                            <td  >{{ ucwords($projectcostitems->heading) }}</td>
                            <td  >{{ get_unit_name($projectcostitems->unit) }}</td>
                            <td  >{{  $totalquantity }}</td>
                            <td  >{{ $prevcumiliquantity }} ({{ !empty($cumiliquantitypercenttill) ? numberformat($cumiliquantitypercenttill).'%' : '0%' }})</td>
                            <td  >{{ numberformat($progress) }} ({{ numberformat($percentage) }}%)</td>
                            <td  >{{ numberformat($cumiliquantity) }} ({{ numberformat($cumiliquantitypercent) }}%)</td>
                            <td  >{{ numberformat($rate) }}</td>
                            <td  >{{ numberformat($cumiliquantitytillamt) }}</td>
                            <td  >{{ numberformat($progressamt) }}</td>
                            <td  >{{ numberformat($cumiliquantityamt) }}</td>
                        </tr>
                        <?php


                        }
                        }
                        }
                        $newlvel = (int)$propro->level+1;
                        $parent = $propro->id;
                        if(!empty($segmentid)){
                            $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
                        }else{
                            $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
                        }
                        if($subprojectid){
                            $proproget = $proproget->where('title',$subprojectid);
                        }
                        $proproget = $proproget->where('position','row')->where('level',$newlvel)->where('parent',$parent)->orderBy('inc','asc')->get();

                        $reportarray = array();
                        $reportarray['projectid'] = $projectid;
                        $reportarray['segmentid'] = $segmentid;
                        $reportarray['subprojectid'] = $subprojectid;
                        $reportarray['parent'] = $propro->id;
                        $reportarray['level'] = $newlvel;
                        $reportarray['contractor'] = $contracid;
                        $reportarray['dateformat'] = $dateformat;
                        $reportarray['startdate'] = $startdate;
                        $reportarray['enddate'] = $enddate;
                        $reportarray['exportype'] = $exportype;
                        $reportarray['categoryarray'] = $proproget;
                        echo reporthtml($reportarray);
                        }
                        }
                        $contr = array('0'=>'Departmental');
                        $contractorarray = $contractorarray+$contr;
                        if(!empty($contractorarray)){
                        foreach ($contractorarray as $contracid => $contracname){
                        ?>
                        {{--  <tr>
                              <td class="cell-inp" style=""><strong>{{ ucwords($contracname) }}</strong></td>
                              <td class="cell-inp" style="" ></td>
                              <td class="cell-inp" style="" colspan="12"></td>
                          </tr>--}}
                        <?php
                        if(!empty($segmentid)){
                            $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
                        }else{
                            $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
                        }
                        if($subprojectid){
                            $proproget = $proproget->where('title',$subprojectid);
                        }
                        $proproget = $proproget->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();

                        $reportarray = array();
                        $reportarray['projectid'] = $projectid;
                        $reportarray['segmentid'] = $segmentid;
                        $reportarray['subprojectid'] = $subprojectid;
                        $reportarray['parent'] = 0;
                        $reportarray['level'] = 0;
                        $reportarray['contractor'] = $contracid;
                        $reportarray['dateformat'] = $dateformat;
                        $reportarray['startdate'] = $startdate;
                        $reportarray['enddate'] = $enddate;
                        $reportarray['exportype'] = $exportype;
                        $reportarray['categoryarray'] = $proproget;
                        echo reporthtml($reportarray);

                        }  } ?>

                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endif
@if(!empty($reporttype)&&in_array('photos',$reporttype))
    @if(count($tasksimagesarray)>0)
        <?php $storage = storage(); ?>
        <div  class="carddesign" style="margin-bottom: 10px;">
            <h6>Task Documents</h6>
        </div>
        <div class="row  mb-20" >
            @forelse($tasksimagesarray as $file)
                <?php if(file_exists(public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname))){?>
                <div class="col-md-2 form-group full-image">
                    <?php if($file->external_link != ''){
                        $imgurl = $file->external_link;
                    }elseif($storage == 'local'){
                        if($exportype=='pdf'){
                            $imgurl = public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname);
                        }else{
                            $imgurl = uploads_url().'/task-files/'.$file->task_id.'/'.$file->hashname;
                        }
                    }elseif($storage == 's3'){
                        if($exportype=='pdf'){
                            $imgurl = public_path('/uploads/task-files/'.$file->task_id.'/'.$file->hashname);
                        }else{
                            $imgurl = awsurl().'/task-files/'.$file->task_id.'/'.$file->hashname;
                        }
                    }elseif($storage == 'google'){
                        $imgurl = $file->google_url;
                    }elseif($storage == 'dropbox'){
                        $imgurl = $file->dropbox_link;
                    }
                    ?>
                    {!! mimetype_thumbnail_pdf($file->hashname,$imgurl)  !!}
                    <p><span><?php echo date('d M Y',strtotime($file->created_at));?></span></p>
                </div>
                <?php }?>
            @empty
                <div class="col-md-2">
                    @lang('messages.noFileUploaded')
                </div>
            @endforelse
        </div>
    @endif
@endif
@if(!empty($reporttype)&&in_array('labourattendance',$reporttype))
    <?php $contr = array('0'=>'Departmental');
    $contractorarray = $contractorarray+$contr;
    $start_date = $startdate.' 00:00:00';
    $end_date = $enddate.' 23:59:59';
    ?>
    @if(!empty($contractorarray))
        @foreach($contractorarray as $contractor => $name)
            <?php
            $mancontractorsarry= \App\ManpowerLog::where('manpower_logs.company_id',$projectdetails->company_id)
                ->where('manpower_logs.contractor',$contractor)
                ->where('manpower_logs.created_at','>=',$start_date)
                ->where('manpower_logs.created_at','<=',$end_date)->first();
            if(!empty($mancontractorsarry)){
            ?>
            <h3>{{ $name }}</h3>
            <div class="combo-sheet  mb-20">
                <div class="table-wrapper">
                    <table class="table table-bordered  text-center" >
                        <table class="table table-bordered  text-center" >
                            <thead>
                            <tr>
                                <th></th>
                                <?php
                                $datesarray = diffRanges($startdate,$enddate,'Y-m-d');

                                foreach($datesarray as $date){
                                ?>
                                <th>{{ $date }}</th>
                                <?php }   ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($manpowercategoryarray)){
                            foreach ($manpowercategoryarray as $manpowercategory){
                            ?>
                            <tr>
                                <td class="cell-inp">{{ ucwords($manpowercategory->title) }}</td>
                                <?php   foreach($datesarray as $date){

                                $start_date = $date.' 00:00:00';
                                $end_date = $date.' 23:59:59';
                                $mancount = \App\ManpowerLog::where('manpower_category',$manpowercategory->id)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date);
                                if(!empty($contractor)){
                                    $mancount = $mancount->where('contractor',$contractor);
                                }else{
                                    $mancount = $mancount->where('contractor','0');
                                }
                                if(!empty($projectid)){
                                    $mancount = $mancount->where('project_id',$projectid);
                                }
                                if(!empty($subprojectid)){
                                    $mancount = $mancount->where('title_id',$subprojectid);
                                }
                                if(!empty($segmentid)){
                                    $mancount = $mancount->where('segment_id',$segmentid);
                                }
                                $mancountsum = $mancount->sum('manpower');
                                $manworkhours = $mancount->sum('workinghours');
                                ?>
                                <td class="cell-inp">{{ $mancountsum ?: '' }} @if(!empty($manworkhours)) ({{ $manworkhours }}) @endif</td>
                                <?php }   ?>
                            </tr>
                            <?php }}?>
                            </tbody>
                        </table>

                </div>
            </div>
            <?php }?>
        @endforeach
    @endif
@endif
<div class="row col-md-12" >
    @if(!empty($reporttype)&&in_array('issues',$reporttype))
        @if(count($issueslist)>0)
            <div class="col-md-6">
                <div class="combo-sheet  mb-20">
                    <div class="table-responsive">
                        <table class="table table-bordered  text-center" >
                            <thead>
                            <tr>
                                <th>Issues</th>
                                <th>Date</th>
                                <th>Task</th>
                                <th>Due date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($issueslist as $issues){
                            if(!empty($issues->task_id)){
                                $taskdetails = \App\Task::where('id',$issues->task_id)->first();
                            }
                            ?>
                            <tr>
                                <td class="cell-inp">{{ ucwords($issues->title) }}</td>
                                <td class="cell-inp">{{ !empty($issues->created_at) ? date('d M Y',strtotime($issues->created_at)) : '' }}</td>
                                <td class="cell-inp">@if(!empty($taskdetails->heading)) {{ ucwords($taskdetails->heading) }} @endif</td>
                                <td class="cell-inp">{{ !empty($issues->due_date) ? date('d M Y',strtotime($issues->due_date)) : '' }}</td>
                                <td class="cell-inp">{{ ucwords($issues->status) }}</td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    @endif
    @if(!empty($reporttype)&&in_array('indent',$reporttype))
        @if(count($indentslist)>0)
            <div class="col-md-6">
                <div class="combo-sheet  mb-20">
                    <div class="table-responsive">
                        <table class="table table-bordered text-center" >
                            <thead>
                            <tr>
                                <th>Indent</th>
                                <th>Material</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Date</th>
                                <th>Expected date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($indentslist as $indents){
                            ?>
                            <tr>
                                <td class="cell-inp">{{ ucwords($indents->indent_no) }}</td>
                                <td class="cell-inp">{{ get_ind_pro_name($indents->cid) }}</td>
                                <td class="cell-inp">{{ $indents->quantity }}</td>
                                <td class="cell-inp">{{ get_unit_name($indents->unit) }}</td>
                                <td class="cell-inp">{{ date('d M Y',strtotime($indents->created_at)) }}</td>
                                <td class="cell-inp">{{ date('d M Y',strtotime($indents->expected_date)) }}</td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    @endif
</div>

