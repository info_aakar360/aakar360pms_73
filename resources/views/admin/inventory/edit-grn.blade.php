@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.purchase-order.index') }}">@lang('app.menu.po')</a></li>
                <li>{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> GRN</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">@lang('modules.indent.store') :</div>
                                        <div class="col-md-8"><b>{{ get_store_name($po->store_id) }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Project :</div>
                                        <div class="col-md-8"><b>{{ get_project_name($po->project_id) }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Supplier :</div>
                                        <div class="col-md-8"><b>{{ get_supplier_name($po->supplier_id) }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">@lang('modules.po.poNumber') :</div>
                                        <div class="col-md-8"><b>{{ $po->po_id }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">PO @lang('modules.po.date') :</div>
                                        <div class="col-md-8"><b>{{ $po->dated }}</b></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="box-title">Purchase Order Details</h3>
                                    <div class="row">
                                        <div class="col-md-4">GRN Number :</div>
                                        <div class="col-md-8"><b>{{ $po->po_id }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Dated :</div>
                                        <div class="col-md-8"><b>{{ \Carbon\Carbon::parse($po->dated)->format('d M, Y') }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Remark :</div>
                                        <div class="col-md-8"><b>{{ $po->remark }}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">Payment Terms :</div>
                                        <div class="col-md-8"><b>{{ $po->payment_terms }}</b></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h3 class="box-title">@lang('modules.rfq.productDetail')</h3>
                            <hr>
                            {!! Form::open(['id'=>'updateIndent','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                            <input type="number" style="display: none" name="project_id" value="{{$po->project_id}}">
                            <input type="number" style="display: none" name="store_id" value="{{$po->store_id}}">
                            <input type="number" style="display: none" name="indent_id" value="{{$po->store_id}}">
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                    <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Product</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th></thead><tbody>';
                                    if(count($tmpData)){
                                        $i = 1;
                                        foreach($tmpData as $data){
                                            $html .= '<tr><td>'.$i.'</td><td>'.get_products($lproducts,$data->product_id,$i).'</td><input style="display: none" type="number" min="0" name="products[]" value="'.$data->product_id.'"><td>'.get_brands($brands,$data->bid).'</td><input  style="display: none" type="number" min="0" name="brand[]" value="'.$data->bid.'"><td><input type="number" min="0" name="quantity[]" class="form-control" value="'.$data->quantity.'"></td><td><input type="text"  style="display: none;" value="'.$data->unit_id.'" class="form-control quantity" name="unit_id[]" id="unit'.$i.'">
                                            <input type="text" readonly value="'.get_unit_name($data->unit_id).'" class="form-control quantity" name="unit_id_name" id="unitData'.$i.'"></td><td><input type="number" min="0" class="form-control" value="'.$data->price.'"></td><td><input type="number" class="form-control" min="0" value="'.$data->tax.'"></td></tr>';
                                            $i++;
                                        }
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                    ?>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                    <a href="{{ route('admin.indent.index') }}" class="btn btn-success" style="color: #ffffff">@lang('app.cancel')</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.stores.projects.indent.update',[$po->id])}}',
                container: '#updateIndent',
                type: "POST",
                redirect: true,
                data: $('#updateIndent').serialize()
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var index = $(this).data('index');
            var token = '{{ csrf_token() }}';
            var store_id = '{{$po->store_id}}';
            var project_id = '{{$po->project_id}}';
            $.ajax({
                url: '{{route('admin.stores.projects.indent.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
                success: function (data) {
                    $('#brandData').html(data.brands);
                    $('#estQty').val(data.estQty);
                    $('#reqQty').val(data.reqQty);
                    $('#unit'+index).val(data.unit);
                    $('#unitData'+index).val(data.unitData);
                    $("#brandData").select2("destroy");
                    $("#brandData").select2();
                }
            });

        });

        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var dated = $('input[name=date]').val();
            var remark = $('input[name=remarkx]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || dated == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.rfq.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }

                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');
            $.ajax({
                url: '{{route('admin.rfq.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });
    </script>
@endpush