@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel-heading row">
            <div class="col-md-2">
                <label class="control-label">Invoice No</label>
                <p>{{$retruns->invoice_id}}</p>
            </div>
            <div class="col-md-2">
                <label class="control-label">Store</label>
                <p>{{ get_store_name($retruns->store_id) }}</p>
            </div>
            <div class="col-md-2">
                <label class="control-label">Project</label>
                <p>{{ get_project_name($retruns->project_id) }}</p>
            </div>
            <div class="col-md-2">
                <label class="control-label">Po Number</label>
                <p></p>
            </div>
            <div class="col-md-2">
                <label class="control-label">Supplier</label>
                <p>{{ get_supplier_name($retruns->supplier_id) }}</p>
            </div>
            <div class="col-md-2">
                <label class="control-label">Date</label>
                <p>{{Carbon\Carbon::parse($retruns->created_at)->format('d-m-Y')}}</p>
            </div>
        </div>

        <input type="text" style="display: none;" name="pi_id" value="">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Product</th>
                <th>Brand</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Return Quantity</th>
                <th>Remark</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input type="text" style="display: none" name="product_id" value="{{$retruns->cid}}" class="form-control">{{get_local_product_name($retruns->cid)}}</td>
                <td><input type="text" style="display: none" name="bid" value="{{$retruns->id}}" class="form-control">{{get_pbrand_name($retruns->id)}}</td>
                <td><input type="text" style="display: none" name="unit_id" value="{{$retruns->unit}}" class="form-control">{{get_unit_name($retruns->unit)}}</td>
                <td><input type="text" style="display: none" name="quantity" id="availqty" value="{{$retruns->quantity}}" class="form-control">{{$retruns->quantity}}</td>
                <td><input type="number" name="return_quantity" class="form-control ret_qty" min="0" data-qty=""></td>
                <td><input type="text"   name="remark" class="form-control"></td>
            </tr></tbody></table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form method="post" id="storeProductReturn">
                    @csrf
                    <div id="piData"></div>
                    <div class="form-actions text-right">
                        <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.inventory.storeReturn')}}',
                container: '#storeProductReturn',
                type: "POST",
                data: $('#storeProductReturn').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        })

        function getPiProducts(val){
            var grn_id = val;
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.inventory.getPiProducts')}}',
                type: 'POST',
                data: {_token: token, pi_id: grn_id},
                success: function (data) {
                    $('#piData').html(data);
                }
            });

        };
        $('ul.showProjectTabs .ProductIssue').addClass('tab-current');

        $(document).on('keyup', '.ret_qty', function(){
            var retrunqty = $(this).val();
            var availqty = $('#availqty').val();
            if(availqty >= retrunqty){
                $('#save-form').prop('disabled',false);
            }else{
                alert('Invalid quantity.');
                $('#save-form').prop('disabled',true);
                inp.val(availqty);
            }
        });
    </script>

@endpush