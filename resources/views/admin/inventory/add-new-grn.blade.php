@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Add New GRN Against PO</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <div class="form-body">
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Project (*)</label>
                                        <select class="form-control select2" name="project_id" id="project" data-style="form-control" required>
                                            <option value="">Select Project</option>
                                            @forelse($projects as $supplier)
                                                <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.inventory.store') (*)</label>
                                        <select class="form-control select2" name="store_id" id="store" data-style="form-control" required>
                                            <option value="">Select Store</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.inventory.supplier') (*)</label>
                                        <select class="form-control select2" name="supplier_id" id="supplier" data-style="form-control" required>
                                            <option value="">Select Supplier</option>
                                            @forelse($suppliers as $supplier)
                                                <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.po.poNumber')</b></label>
                                        <select class="form-control select2" name="po_number" id="po_number" data-style="form-control" required>
                                            <option value="">Select @lang('modules.po.poNumber')</option>
                                            @forelse($pos as $supplier)
                                                <option value="{{$supplier->po_number}}">{{ $supplier->po_number }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>PO @lang('modules.po.date')</b></label>
                                        <input type="date" name="po_dated" value="" class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>GRN @lang('modules.inventory.dated')</b></label>
                                        <input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            {{--<hr>--}}
                            {{--<h3 class="box-title">@lang('modules.inventory.invoiceDetail')</h3>--}}
                            <div class="row">
                                <!--/span-->
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>@lang('modules.inventory.invoiceNo')</b></label>--}}
                                        {{--<input class="form-control" name="invoice_no" type="text" value="NA" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label"><b>@lang('modules.inventory.dated')</b></label>--}}
                                        {{--<input class="form-control" name="inv_dated" type="date" data-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required/>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.inventory.paymentTerms')</b></label>
                                        <textarea name="payment_terms" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <h3 class="box-title">@lang('modules.indent.productDetail')</h3>

                            <hr>
                            <div class="row">

                                <div class="table-responsive" id="pdata">
                                    <table class="table"><thead><th>S.No.</th><th>Product</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th></thead><tbody><tr><td style="text-align: center" colspan="9">No Records Found.</td></tr></tbody></table>
                                    {{--<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Price</th><th>Tax (%)</th><th>Amount</th><th>Action</th></thead><tbody><tr><td style="text-align: center" colspan="9">No Records Found.</td></tr></tbody></table>--}}
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box" >
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone"
                                                 id="file-upload-dropzone">
                                                {{ csrf_field() }}
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url"type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="taskID" id="taskID">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><b>@lang('modules.inventory.remark')</b></label>
                                    <textarea name="inv_remark" rows="7" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <span>Freight (If Extra): </span><input type="number" value="0.00" name="freight" style="margin-right: 15px;"/><button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.inventory.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#taskID').val();
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "GRN Created";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.inventory.grnAgainstPo') }}'

        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.inventory.postAddNewGrn')}}',
                container: '#createRfq',
                type: "POST",
                redirect: true,
                data: $('#createRfq').serialize(),
                success: function (data) {
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = data.taskID;
                        $('#taskID').val(data.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "GRN Created";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.inventory.grnAgainstPo') }}'
                    }
                }
            })
        });
        $(document).on('change', 'select[name=product]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.rfq.getBrands')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('select[name=brand]').html(data);
                    $("select[name=brand]").select2("destroy");
                    $("select[name=brand]").select2();
                }
            });
        });
        $(document).on('change', 'select[name=po_number]', function(){
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.inventory.getPoData')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('input[name=po_dated]').val(data.date);
                    $('input[name=payment_terms]').val(data.payment_terms);
                    $('#pdata').html(data.podata);
                }
            });
        });
        $(document).on('click', '.add-button', function(){
            var btn = $(this);
            var cid = $('select[name=product]').val();
            var bid = $('select[name=brand]').val();
            var qty = $('input[name=quantity]').val();
            var unit = $('select[name=unit]').val();
            var price = $('input[name=price]').val();
            var tax = $('input[name=tax]').val();
            if(cid == '' || bid == '' || qty == '' || qty == 0 || unit == '' || price == '' || tax == ''){
                alert('Invalid Data. All fields are mandatory.');
            }else{
                $.ajax({
                    url: '{{route('admin.inventory.storeTmp')}}',
                    type: 'POST',
                    data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, price: price, tax: tax},
                    redirect: false,
                    beforeSend: function () {
                        btn.html('Adding...');
                    },
                    success: function (data) {
                        $('#pdata').html(data);
                    },
                    complete: function () {
                        btn.html('Add');
                    }
                });
            }
        });
        $(document).on('click', '.deleteRecord', function(){
            var btn = $(this);
            var did = btn.data('key');
            $.ajax({
                url: '{{route('admin.inventory.deleteTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', did: did},
                redirect: false,
                beforeSend: function () {
                    btn.html('Deleting...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Delete');
                }

            });
        });

        $('#project').change(function () {
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.indent.getStores')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('#store').html(data.stores);
                }
            });
        });
        $(document).on('keyup', '.quantity', function(){
            var recqty = $(this).val();
            var qty = $(this).data('qty');
            var delqty = $(this).data('delqty');
            if(delqty == '' || delqty == '0'){
                if(recqty > qty){
                    alert('Enter Valid Quantity');
                }
            }else{
                if(recqty > delqty){
                    alert('Enter Valid Quantity');
                }
            }
        });

    </script>
@endpush

