@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        .icon-bar {
            width: 55%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 24%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 15px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }

        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }

        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }

        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }
    </style>
@endpush
<?php
$moduleName = " Payment Voucher";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " All";
$breadcrumbMainIcon = "account_balance_wallet";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\Transaction';
$ParentRouteName = 'admin.payment_voucher';
$voucher_type = 'JV';
$transaction = new \App\Transaction();
?>
@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <form method="post" action="{!! route($ParentRouteName.'.ledgerlog') !!}">
            <div class="col-md-12 m-t-50">
                <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
            </div>
            <div class="col-md-12">
                <div class="example">
                    <h5 class="box-title">@lang('app.selectDateRange')</h5>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control"  name="startdate" id="start-date" placeholder="@lang('app.startDate')"   />
                        <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                        <input type="text" class="form-control" name="enddate" id="end-date" placeholder="@lang('app.endDate')"  />
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group" >
                    <h5 class="box-title">  @lang('app.ledger') </h5>
                    <select class="select2" multiple="multiple"  name="ledgerfrom[]" data-placeholder="@lang('app.ledger')" id="selectledger">
                        <option value=""> @lang('app.ledger')</option>
                        @foreach($ledgersarray as $ledgers)
                            <option value="{{ $ledgers->id }}">{{ ucwords($ledgers->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="box-title m-t-10"> </h5>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success"  ><i class="fa fa-check"></i> @lang('app.apply')</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-11">
                        <div class="form-group">
                            <h2 style="color: #002f76">@lang('app.ledger')</h2>

                        </div>
                    </div>
                    <div class="col-sm-1 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">

                        <div class="table-responsive">
                            {{ csrf_field() }}
                                <table class="table table-bordered" id="users-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">@lang('app.sno')</th>
                                        <th>From</th>
                                        <th>TO</th>
                                        <th>Payment Date</th>
                                        <th>Particulars</th>
                                        <th>Dr</th>
                                        <th>CR</th>
                                        <th>Running Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(count($openingbalancearray)>0){
                                                $ledids = array_column($openingbalancearray,'id');
                                                $names = array_column($openingbalancearray,'name');
                                                $amount = array_column($openingbalancearray,'amount');
                                                $voucherdate = array_column($openingbalancearray,'voucherdate');
                                                $sumamount = array_sum($amount);
                                            ?>
                                        <tr>
                                            <th class="text-center">1</th>
                                            <th>{{  !empty($names) ? implode(',',$names) : '' }}</th>
                                            <th></th>
                                            @if(!empty($paymentdate))
                                            <th>{{ date('d M Y',strtotime($paymentdate)) }}</th>
                                                <th>Opening Balance</th>
                                                <th>{{  $runningbalance }}</th>
                                            @else
                                                <th>{{  !empty($voucherdate) ? implode(',',$voucherdate) : '' }}</th>
                                                <th>Opening Balance</th>
                                                <th>{{  !empty($amount) ? implode(',',$amount) : '' }}</th>
                                            @endif
                                            <th></th>
                                            <th>{{  $runningbalance }}</th>
                                        </tr>
                                        <?php }?>
                                        <?php
                                        $openingbalanceamt = $sumamount;
                                        if(count($paymentsarray)>0){
                                        $x=2;
                                            foreach($paymentsarray as $payments){
                                                $dramount =$cramount = 0;
                                                if($payments->type=='dr'){
                                                    $dramount = $payments->amount;
                                                    $openingbalanceamt += $dramount;
                                                }
                                                if($payments->type=='cr'){
                                                    $cramount = $payments->amount;
                                                    $openingbalanceamt -= $cramount;
                                                }

                                                ?>
                                                <tr>
                                                    <th class="text-center">{{ $x }}</th>
                                                    <th>{{ !empty($payments->ledgerfrom) ? $payments->ledgerfrom->name : '' }}</th>
                                                    <th><a href="{{ route('admin.payment_voucher.show',$payments->id) }}" target="_blank">{{ !empty($payments->ledgerto) ? $payments->ledgerto->name : '' }}</a></th>
                                                     <th>{{ !empty($payments->voucher_date) ? date('d M Y',strtotime($payments->voucher_date)) : '' }}</th>

                                                    <th>Created By : {{ get_user_name($payments->created_by) }} <br>
                                                        Remark : {{ $payments->particulars }}</th>
                                                    <th>{{ $dramount }}</th>
                                                    <th>{{ $cramount }}</th>
                                                    <th>{{ $openingbalanceamt }}</th>
                                                </tr>
                                           <?php $x++;  }} ?>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th class="text-center"></th>
                                            <th></th>
                                            <th></th>
                                            <th>Closing Balance</th>
                                            <th></th>
                                            <th></th>
                                            <th>{{  $openingbalanceamt }}</th>
                                        </tr>
                                    </tbody>

                                </table>

                        </div>
                </div>
            </div>
        </div>
    </div>


@stop

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        var table;
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You want to trash this branch!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, trash it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route($ParentRouteName.'.destroy',['id'=>':id']) }}";
                        {{--var url = "{{ route('admin.clients.destroy',':id') }}";--}}
                            url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'GET',
                            url: url,
                            data: {'_token': token, '_method': 'GET'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });
        function loadTable(){

            var startdate   = $('#start-date').val();
            var enddate   = $('#end-date').val();
            var ledgerfrom     = $('#selectledger').val();

            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route($ParentRouteName.'.ledgerlogdata') !!}?ledgerfrom=' + ledgerfrom+'&startdate=' + startdate+ '&enddate=' + enddate,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'ledger_from', name: 'ledger_from' },
                    { data: 'ledger_to', name: 'ledger_to' },
                    { data: 'voucher_date', name: 'voucher_date'},
                    { data: 'particulars', name: 'particulars'},
                    { data: 'dr', name: 'dr'},
                    { data: 'cr', name: 'cr'},
                    { data: 'balance', name: 'balance'},
                ]
            });
        }
        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });
        $('#filter-results').click(function () {
            loadTable();
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        jQuery('#date-range').datepicker({
            toggleActive: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

    </script>
@endpush