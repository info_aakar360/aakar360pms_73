@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12" >
            <div class="white-box">

                <h2 style="color: #002f76">@lang('app.menu.weatherLogs')</h2>

                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['id'=>'updateTime','class'=>'ajax-form','method'=>'PUT']) !!}
                <div class="form-body">
                    <div class="row m-t-30">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    @lang('app.project')
                                </label>
                                <select class="select2 form-control" name="project_id" data-placeholder="@lang('modules.weatherLogs.selectProject')"  id="project_id">
                                    <option value="">Select  @lang('app.project')</option>
                                    @foreach($projectarray as $project)
                                        <option value="{{ $project->id }}"  @if($weather->project_id==$project->id) selected @endif >{{ ucwords($project->project_name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" id="subprojectblock" style="display: none">
                            <div class="form-group">
                                <label> @lang('app.subproject')</label>
                                <select class="form-control" name="title_id" id="titlelist" data-style="form-control">
                                    <option value="">Select  @lang('app.title')</option>
                                    @foreach($titlesarray as $titles)
                                        <option value="{{ $titles->id }}"  @if($weather->title_id==$titles->id) selected @endif >{{ ucwords($titles->title) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" id="segmentblock" style="display: none">
                            <div class="form-group">
                                <label>@lang('app.segment')</label>
                                <select class="form-control" name="segment_id" id="segmentlist" data-style="form-control">
                                    <option value="">Select  @lang('app.segment')</option>
                                    @foreach($segmentsarray as $segments)
                                        <option value="{{ $segments->id }}"  @if($weather->segment_id==$segments->id) selected @endif >{{ ucwords($segments->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>@lang('modules.weatherLogs.tempmin')</label>
                                <input  name="tempmin" type="text"  class="form-control" value="{{ $weather->mintemp }}" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>@lang('modules.weatherLogs.tempmax')</label>
                                <input  name="tempmax" type="text"  class="form-control"  value="{{ $weather->maxtemp }}" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>@lang('modules.weatherLogs.siteCondition')</label>
                                <select class="form-control" name="site_condition"  data-style="form-control">
                                    <option value="">Select Condition</option>
                                    <option value="slushy" @if($weather->sitecondition=='slushy') selected @endif>Slushy</option>
                                    <option value="dry" @if($weather->sitecondition=='dry') selected @endif>Dry</option>
                                    <option value="partialslushy" @if($weather->sitecondition=='partialslushy') selected @endif>Partial Slushy</option>
                                    <option value="partialdry" @if($weather->sitecondition=='partialdry') selected @endif>Partial Dry</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>@lang('modules.weatherLogs.weatherCondition')</label>
                                <select class="form-control weathercondition" name="weather_condition"  data-style="form-control">
                                    <option value="">Select Condition</option>
                                    <option value="normal"  @if($weather->weathercondition=='normal') selected @endif>Normal</option>
                                    <option value="rainy"  @if($weather->weathercondition=='rainy') selected @endif>Rainy</option>
                                    <option value="foggy"  @if($weather->weathercondition=='foggy') selected @endif>Foggy</option>
                                    <option value="cloudy"  @if($weather->weathercondition=='cloudy')  selected @endif>Cloudy</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 raintimingsblock" style="display: @if($weather->weathercondition=='rainy') block @else none @endif" >
                            <div class="form-group">
                                <label>@lang('modules.weatherLogs.raintimings')</label>
                                <div id="raningtimngs">
                                    <?php
                                    if(!empty($weather->raintimings)){
                                    $raintimings = json_decode($weather->raintimings);
                                          $raintimingsfrom = $raintimings->fromtime;
                                          $raintimingsto = $raintimings->totime;
                                        for($x=0;$x<count($raintimingsfrom);$x++){
                                            if($raintimingsfrom[$x]&&$raintimingsto[$x]){
                                        ?>
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <input type="text" name="timings[fromtime][]" value="{{ $raintimingsfrom[$x] }}"  class="form-control timepicker" value="" />
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" name="timings[totime][]" value="{{ $raintimingsto[$x] }}"  class="form-control timepicker" value="" />
                                        </div>
                                        <div class="col-md-3">
                                            <?php if($x==0){?>
                                            <a href="javascript:void(0);" class="addrow btn btn-outline btn-primary">Add row</a>
                                            <?php }else{?>
                                             <a href="javascript:void(0);" class="removerow btn btn-outline btn-danger">Remove row</a>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <?php }?>
                                    <?php }?>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="memo">Description</label>
                                <textarea name="description"  class="form-control">{{ $weather->description }}</textarea>
                            </div>
                        </div>
                        <div class="row  mb-20" >
                            @forelse($files as $file)
                                <?php if(file_exists(public_path('/uploads/weather-log-files/'.$weather->id.'/'.$file->hashname))){?>
                                <div class="col-md-2 form-group full-image">
                                    <?php if($file->external_link != ''){
                                        $imgurl = $file->external_link;
                                    }elseif($storage == 'local'){
                                        $imgurl = uploads_url().'/weather-log-files/'.$weather->id.'/'.$file->hashname;
                                    }elseif($storage == 's3'){
                                        $imgurl = awsurl().'/weather-log-files/'.$weather->id.'/'.$file->hashname;
                                    }elseif($storage == 'google'){
                                        $imgurl = $file->google_url;
                                    }elseif($storage == 'dropbox'){
                                        $imgurl = $file->dropbox_link;
                                    }
                                    ?>
                                    {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                    <p><span><?php echo date('d M Y',strtotime($file->created_at));?></span></p>
                                </div>
                                <?php }?>
                            @empty
                                <div class="col-md-2">
                                    @lang('messages.noFileUploaded')
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                        <div class="row m-b-20">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box" >
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone dropheight"
                                                 id="file-upload-dropzone">
                                                {{ csrf_field() }}
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url"type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="weatherID" id="weatherID" >
                            </div>
                        </div>
                        <div class="form-actions m-t-30 text-right">
                            <button type="button" id="update-form-man" class="btn btn-success"><i
                                        class="fa fa-check"></i> @lang('app.update')</button>
                        </div>
                        {!! Form::close() !!}

                    </div>

                </div>
            </div>

        </div>
        <!-- .row -->

        @endsection

        @push('footer-script')
            <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

            <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

            <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

            <script>
                $(".weathercondition").change(function () {
                    var weather = $(this).val();
                    if(weather=='rainy'){
                        $(".raintimingsblock").show();
                    }else{
                        $(".raintimingsblock").hide();
                    }
                });
                $(".addrow").click(function () {
                    var contenthtml = '';
                    contenthtml += '<div class="row form-group">\n' +
                        '<div class="col-md-3">\n' +
                        '<input type="text" name="timings[fromtime][]" class="form-control timepicker"  />\n' +
                        '</div>\n' +
                        '<div class="col-md-3">\n' +
                        '<input type="text" name="timings[totime][]" class="form-control timepicker"  />\n' +
                        '</div>\n' +
                        '<div class="col-md-3">\n' +
                        '<a href="javascript:void(0);" class="removerow btn btn-outline btn btn-danger">Remove row</a>\n' +
                        '</div>\n' +
                        '</div>';
                    $("#raningtimngs").append(contenthtml);
                });
                $("#raningtimngs").on("click",".removerow",function () {
                    $(this).parent().parent().remove();
                });
                Dropzone.autoDiscover = false;
                //Dropzone class
                myDropzone = new Dropzone("div#file-upload-dropzone", {
                    url: "{{ route('admin.weather-logs.storeImage') }}",
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    paramName: "file",
                    maxFilesize: 10,
                    maxFiles: 10,
                    acceptedFiles: "image/*,application/pdf",
                    autoProcessQueue: false,
                    uploadMultiple: true,
                    addRemoveLinks:true,
                    parallelUploads:10,
                    init: function () {
                        myDropzone = this;
                    }
                });

                myDropzone.on('sending', function(file, xhr, formData) {
                    console.log(myDropzone.getAddedFiles().length,'sending');
                    var ids = $('#weatherID').val();
                    formData.append('weather_id', ids);
                });

                myDropzone.on('completemultiple', function () {
                    var msgs = "Log Updated Successfully";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('admin.weather-logs.index') }}'
                });
                $('#employeeBox').hide();

                $('#update-form-man').click(function () {
                    $.easyAjax({
                        url: '{{route('admin.weather-logs.update',$weather->id)}}',
                        container: '#updateTime',
                        type: "POST",
                        data: $('#updateTime').serialize(),
                        success: function (data) {
                            if(myDropzone.getQueuedFiles().length > 0){
                                weatherID = data.weatherID;
                                $('#weatherID').val(data.weatherID);
                                myDropzone.processQueue();
                            }else{
                                var msgs = "@lang("messages.weatherLogUpdate")";
                                $.showToastr(msgs, 'success');
                                window.location.href = '{{ route('admin.weather-logs.index') }}';
                            }
                        }
                    })
                });

                $('#project_id').change(function () {
                    var id = $(this).val();

                    // For getting dependent task
                    var dependentTaskUrl = '{{route('admin.todo.dependent-todo', ':id')}}';
                    dependentTaskUrl = dependentTaskUrl.replace(':id', id);
                    $.easyAjax({
                        url: dependentTaskUrl,
                        type: "GET",
                        success: function (data) {
                            $('#dependent_task_id').html(data.html);
                        }
                    })
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.projects.subprojectoptions') }}",
                        data: {'_token': token,'projectid': id},
                        success: function(data){
                            $("#subprojectblock").hide();
                            $("#segmentblock").hide();
                            if(data.subprojectlist){
                                $("#subprojectblock").show();
                                $("select#titlelist").html("");
                                $("select#titlelist").html(data.subprojectlist);
                                $('select#titlelist').select2();
                            }
                            if(data.segmentlist){
                                $("#segmentblock").show();
                                $("select#segmentlist").html("");
                                $("select#segmentlist").html(data.segmentlist);
                                $('select#segmentlist').select2();
                            }
                        }
                    });
                });

                $("#titlelist").change(function () {
                    var project = $("#project_id").select2().val();
                    var titlelist = $(this).val();
                    if(project){
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            type: "POST",
                            url: "{{ route('admin.projects.subprojectoptions') }}",
                            data: {'_token': token,'projectid': project,'title': titlelist},
                            success: function(data){
                                if(data.tasklist){
                                    $("select#tasklist").html("");
                                    $("select#tasklist").html(data.activitylist);
                                    $('select#tasklist').select2();
                                }
                            }
                        });
                    }
                });
                $(".select2").select2({
                    formatNoMatches: function () {
                        return "{{ __('messages.noRecordFound') }}";
                    }
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true,
                    weekStart:'{{ $global->week_start }}',
                    format: '{{ $global->date_picker_format }}',
                });
            </script>
        @endpush
    </div>