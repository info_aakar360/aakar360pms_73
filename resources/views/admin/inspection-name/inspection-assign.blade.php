@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title m-t-30">@lang('app.selectDateRange')</h5>
                <div class="input-daterange input-group" id="date-range">
                    <input type="text" class="form-control" name="startdate" id="start-date" placeholder="@lang('app.startDate')"   />
                    <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                    <input type="text" class="form-control" name="enddate" id="end-date" placeholder="@lang('app.endDate')"   />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title">  @lang('app.users') </h5>
                <select class="select2 form-control" name="users" data-placeholder="@lang('app.selectProject')" id="selectuser">
                    <option value=""> @lang('app.users')</option>
                    @foreach($employees as $employe)
                        <option value="{{ $employe->id }}">{{ ucwords($employe->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title"> @lang('app.inspectiontype') </h5>
                <select class="select2 form-control" name="inspectiontype" data-placeholder="@lang('app.selectProject')" id="inspectiontype">
                    <option value="">@lang('app.inspectiontype')</option>
                    @foreach($inspectiontypes as $inspectionty)
                        <option value="{{ $inspectionty->id }}">{{ ucwords($inspectionty->title) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">@lang('app.status')</label>
                <select class="selectpicker form-control" name="status" data-style="form-control"  id="assignstatus">
                    <option value="">Please Status</option>
                    <option value="open" >Open</option>
                    <option value="initiated" >Initiated</option>
                    <option value="completed" >Completed</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <h5 class="box-title m-t-10"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-10">
                        <div class="form-group">
                            <a href="{{ route('admin.inspectionName.inspectionAssigned') }}" class="btn  btn-success btn-sm">Assigned</a>
                            <a href="{{ route('admin.inspection-name.index') }}" class="btn btn-outline btn-success btn-sm">Inspection Form</a>
                            <a href="{{ route('admin.input-fields.index') }}" class="btn btn-outline btn-success btn-sm">Input feilds</a>
                            <a href="{{ route('admin.inspection-type.index') }}" class="btn btn-outline btn-success btn-sm">Inspection type</a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-right">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                        class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                        <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                            <thead>
                            <tr>
                                <th>@lang('app.sno')</th>
                                <th>@lang('app.name')</th>
                                <th>@lang('app.type')</th>
                                <th>@lang('app.description')</th>
                                <th>@lang('app.status')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                            </thead>
                        </table>
                </div>


            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" id="answerForm">
                           {{-- <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Inspection
                                    </label>
                                    <select class="selectpicker form-control" name="id" data-style="form-control" required>
                                        <option value="">Please Select Inspection Name</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Assign User
                                    </label>
                                    <select class="selectpicker form-control" name="assign_to[]" data-style="form-control" multiple required>
                                        <option value="">Please Select User</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>--}}
                            @csrf
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success" onclick="submitAnswer()" value="">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default closeButton" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>

<script>
    loadTable();
    function loadTable(){

        var inspectiontype = $('#inspectiontype').val();
        var user   = $('#selectuser').val();
        var startdate     = $('#start-date').val();
        var enddate     = $('#end-date').val();
        var assignstatus     = $('#assignstatus').val();

        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.inspectionName.inspectionAssignedData') !!}?inspectiontype=' + inspectiontype + '&user=' + user + '&startdate=' + startdate+ '&enddate=' + enddate+ '&assignstatus=' + assignstatus,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'type', name: 'type' },
                { data: 'description', name: 'description'},
                { data: 'assignstatus', name: 'status'},
                { data: 'action', name: 'action', width: '15%' }
            ]
        });
    }
    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });
    $('#filter-results').click(function () {
        loadTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        $('#status').val('all');
        $('.select2').val('all');
        $('#filter-form').find('select').select2();
        loadTable();
    })

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $('ul.showProjectTabs .projectTasks').addClass('tab-current');

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('admin.inspectionName.destroyName',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    $.each(rData, function( index, value ) {
                        var selectData = '';
                        selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                        options.push(selectData);
                    });

                    $('#category_id').html(options);
                    $('#category_id').selectpicker('refresh');
                }
            }
        });
    });

    function showData(val,row) {
        var url = "{{ route('admin.inspectionName.getData') }}";
        var token = "{{ csrf_token() }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, 'id':val, 'row_id':row},
            success: function (data) {
                $('#ansData'+row).html(data.html);
            }
        });
    }

    function submitAnswer() {
        $.easyAjax({
            url: '{{route('admin.inspectionName.assignUserStore')}}',
            container: '#answerForm',
            type: "POST",
            data: $('#answerForm').serialize(),
            success: function (data) {

                var msgs = "@lang('Answer updated successfully')";
                $.showToastr(msgs, 'success');
                window.location.reload();

            }
        })
        return false;
    }
</script>
@endpush