@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <table class="table-striped" style="width: 100%;">
                        <tr>
                            <td style="width: 15%;"><b>@lang('app.name')</b></td>
                            <td  style="width: 35%;">{{ $inspection->name }}</td>
                            <td style="width: 15%;"><b>Type</b></td>
                            <td  style="width: 35%;">{{ get_ins_type_name($inspection->type) }}</td>
                        </tr>
                        <tr>
                            <td style="width: 15%;"><b>Description</b></td>
                            <td  style="width: 35%;">{!! $inspection->description !!}</td>
                        </tr>
                    </table>
                    <div class="col-md-12 m-t-20">
                        <b>Files</b>
                        <br>
                        <div class="row" >
                            @foreach($files as $file)
                                <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                    @if($file->external_link != '')
                                        <?php $imgurl = $file->external_link;?>
                                    @elseif($storage == 'local')
                                        <?php $imgurl = uploads_url().'inspection-files/'.$file->inspection_id.'/'.$file->hashname;?>
                                    @elseif($storage == 's3')
                                        <?php $imgurl = awsurl().'/inspection-files/'.$file->inspection_id.'/'.$file->hashname;?>
                                    @elseif($storage == 'google')
                                        <?php $imgurl = $file->google_url;?>
                                    @elseif($storage == 'dropbox')
                                        <?php $imgurl = $file->dropbox_link;?>
                                    @endif
                                    {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                    <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <form method="post" id="answerForm" action="" >
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Assign List <redcode>*</redcode></label>
                                    <select class="selectpicker form-control" required name="assign_to[]" id="user_id" multiple>
                                        <option value="">Select Assign List</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="control-label">Distribution List
                                    </label>
                                    <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple>
                                        <option value="">Please Select Distribution</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->user_id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.dueDate')</label>
                                    <input type="text" name="due_date"  id="due_date2" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <input type="text" name="location" class="form-control" placeholder="Location">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="privete">Private or Public</label><br>
                                    <input id="privete" name="private" value="1" type="checkbox">
                                    <label for="privete">Private</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">@lang('app.status') <redcode>*</redcode></label>
                                    <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                        <option value="open">Open</option>
                                        <option value="initiated">Initiated</option>
                                        <option value="completed">Completed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                </div>
                            </div>
                            <input type="hidden" name="inspectionid" value="{{ $inspection->id }}">
                            @csrf

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- .row -->


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>

    <script>
        jQuery('#due_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $('.createTaskCategory').click(function(){
            var url = '{{ route('admin.inspectionName.createName')}}';
            $('#modelHeading').html("Create New");
            $.ajaxModal('#taskCategoryModal', url);
        })

        $('.editTaskCategory').click(function(){
            var id = $(this).data('cat-id');
            var url = '{{ route('admin.inspectionName.editName',':id')}}';
            url = url.replace(':id', id);
            $('#modelHeading').html("Edit Category");
            $.ajaxModal('#taskCategoryModal', url);
        })

        $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );

        $('ul.showProjectTabs .projectTasks').addClass('tab-current');

        $('.delete-category').click(function () {
            var id = $(this).data('cat-id');
            var url = "{{ route('admin.inspectionName.destroyName',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                    }
                }
            });
        });

        function showData(val,row) {
            var url = "{{ route('admin.inspectionName.getData') }}";
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, 'id':val, 'row_id':row},
                success: function (data) {
                    $('#ansData'+row).html(data.html);
                }
            });
        }
        $("#answerForm").submit(function (e) {
            e.preventDefault();
            $.easyAjax({
                url: '{{route('admin.inspectionName.SubmitAssignForm')}}',
                container: '#answerForm',
                type: "POST",
                data: $('#answerForm').serialize(),
                success: function (data) {
                    var msgs = "@lang('Assigned updated successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href='{{ route('admin.inspection-name.index') }}';
                }
            })
        });

    </script>
@endpush