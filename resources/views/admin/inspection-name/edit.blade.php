@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
        .card-header {
            background: #efefef;
            padding: 10px;
        }
        .card-body {
            padding: 10px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

        {!! Form::open(['id'=>'editBoqCategory','class'=>'ajax-form','enctype'=>'multipart/form-data','method'=>'POST']) !!}
                <div class="form-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="title" id="title" required class="form-control" value="{{ $name->name }}">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Type</label>
                                <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                    <option value="">Please Select Type</option>
                                    @foreach($types as $employee)
                                        <?php
                                            $selected = '';
                                            if($employee->id == $name->type){
                                                $selected = 'selected';
                                            }
                                        ?>
                                        <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->title) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label">@lang('app.description')</label>
                                <textarea id="description" name="description" class="form-control summernote">{{ $name->description }}</textarea>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <label class="control-label">Files</label>
                            <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                            <div id="file-upload-box" >
                                <div class="row" id="file-dropzone">
                                    <div class="col-md-12">
                                        <div class="dropzone dropheight" id="file-upload-dropzone">
                                            {{ csrf_field() }}
                                            <div class="fallback">
                                                <input name="file[]" type="file" multiple/>
                                            </div>
                                            <input name="image_url" id="image_url" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="inspection_id" value="{{ $name->id }}" id="inspectionID">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @foreach($files as $file)
                        <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                            @if($file->external_link != '')
                                <?php $imgurl = $file->external_link;?>
                            @elseif($storage == 'local')
                                <?php $imgurl = uploads_url().'inspection-files/'.$name->id.'/'.$file->hashname;?>
                            @elseif($storage == 's3')
                                <?php $imgurl = awsurl().'/inspection-files/'.$name->id.'/'.$file->hashname;?>
                            @elseif($storage == 'google')
                                <?php $imgurl = $file->google_url;?>
                            @elseif($storage == 'dropbox')
                                <?php $imgurl = $file->dropbox_link;?>
                            @endif
                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                            <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                Remove
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row m-b-20">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="category" id="category" placeholder="Category">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info" id="add-category">
                            <i class="fa fa-plus"></i>
                            @lang('modules.invoices.addSection')
                        </button>
                    </div>
                </div>
                <div id="sortable">
                    <?php  $categoryarray = \App\InspectionQuestion::where('inspection_name_id',$name->id)->groupBy('category')->get();
                   $cat= 1;     $ques= 1;      foreach ($categoryarray as $category){
                    ?>
                    <div id="categ{{ $category->category }}" class="card mb-10">
                        <div class="card-header">
                            <div class="col-md-9"><p class="rowcat">{{ $category->category }}</p></div>
                            <div class="col-md-3 text-right">
                                <a href="javascript:void(0);" data-category="{{ $category->category }}" class="removeCatBlock"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
                        <div class="card-body">
                            <?php  $questionarray = \App\InspectionQuestion::where('inspection_name_id',$name->id)->where('category',$category->category)->get();
                          foreach ($questionarray as $question){
                            ?>
                            <div id="ques{{ $question->id }}" class="form-group">
                                 <div class="row">
                                    <div class="col-md-6">
                                        <input name="question[{{ $cat }}][]" class="form-control" placeholder="Question" value="{{ $question->question }}"  data-quesid="{{ $ques }}" >
                                        </div>
                                    <div class="col-md-2">
                                        <select class="form-control changereponse" name="response[{{ $cat }}][]" data-quesid="{{ $ques }}">
                                            <option value="">Select Response</option>
                                            @foreach($fields as $cate)
                                                 <option value="{{ $cate->id }}" <?php if($question->input_field_id==$cate->id){ echo 'selected';}?>>{{ $cate->title }}</option>
                                            @endforeach
                                         </select>
                                        </div>
                                    <div class="col-md-3">
                                        <div id="previewblock{{ $ques }}">
                                            <?php echo get_input_field($question->input_field_id,$ques);?>
                                         </div>
                                        </div>
                                    <div class="col-md-1">
                                        <a href="javascript:void(0);" data-id="{{ $question->id }}" class="btn btn-outline btn-success btn-sm removeQuestionBlock" ><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <?php $ques++; }?>
                            <input type="hidden" name="category[]" value="{{ $category->category }}" />
                            <input type="hidden" name="categoryid[]" value="{{ $cat }}" />
                                <div id="questionblock{{ $cat }}">
                                    <div class="col-md-12 form-group">
                                      <a href="javascript:void(0);" data-cat="{{ $cat }}" class="btn btn-outline btn-success btn-sm addQuestion" > Add Question</a>
                                    </div>
                                </div>
                            </div>
                </div>
                        <?php $cat++; }?>
                </div>
                <div class="col-xs-12 m-t-5">&nbsp;</div>
                <div class="form-actions">
                    <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
        {!! Form::close() !!}
    </div>
</div>
    </div>
@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

<script>
    var ques = '{{ $quescount+1 }}';
    $('#sortable').on("click",".addQuestion",function () {
        var catid = $(this).data('cat');
        var gint = '{{ generate_int() }}';
        var item = '<div id="gid'+gint+'" class="form-group row">'
            +'<div class="col-md-6">'
            +'<input name="question['+catid+'][]" class="form-control" placeholder="Question"  data-quesid="'+ques+'" >'
            +'</div>'
            +'<div class="col-md-2">'
            +'<select class="form-control changereponse" name="response['+catid+'][]" data-quesid="'+ques+'">'
            +'<option value="">Select Response</option>';
        @foreach($fields as $category)
            item += '<option value="{{ $category->id }}">{{ $category->title }}</option>';
        @endforeach
            item += '</select>'
            +'</div>'
            +'<div class="col-md-3">'
            +'<div id="previewblock'+ques+'">'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1">'
            +'<a href="javascript:void(0);"  class="btn btn-outline btn-success btn-sm removeQuestionBlock" data-gid="'+gint+'" ><i class="fa fa-trash-o"></i></a>'
            +'</div>'
            +'</div>';
        $("#questionblock"+catid).append(item);
        ques++;
    });
    var r = '{{ $catcount+1 }}';
    $('#add-category').click(function () {
        var cat = $("#category").val();

        if(cat){
            var item = '<div class="card mb-10">'
                +'<div class="card-header">'
                +'<div class="col-md-9"><p class="rowcat">'+cat+'</p></div>'
                +'<div class="col-md-3 text-right">'
                +'<a href="javascript:void(0);" class="removeCatBlock"><i class="fa fa-trash-o"></i></a>'
                +'</div>'
                +'</div>'
                +'<div class="card-body">'
                +'<input type="hidden" name="category[]" value="'+cat+'" />'
                +'<input type="hidden" name="categoryid[]" value="'+r+'" />'
                +'<div id="questionblock'+r+'">'
                +'<div class="col-md-12 form-group">'
                +'  <a href="javascript:void(0);" data-cat="'+r+'" class="btn btn-outline btn-success btn-sm addQuestion" > Add Question</a>'
                +'</div>'
                +'</div>'
                +'</div>';
            $("#sortable").append(item);
            $("#category").val("");
        }else{
            alert('Category is empty');
        }
        r++;
    });
    $(document).on('click','.removeQuestionBlock', function () {
        var url = "{{ route('admin.inspectionName.destroyQuestion') }}";
        var token = "{{ csrf_token() }}";
        var insid = "{{ $name->id }}";
        var id = $(this).data('id');
        var gid = $(this).data('gid');
        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token, 'insid':insid,'id':id},
            success: function(response){
                if(gid){
                    $("#gid"+gid).remove();
                }
                if(id){
                    $("#ques"+id).remove();
                }

            }
        })
    });
    $(document).on('click','.removeCatBlock', function () {
        var category = $(this).data('category');
        var url = "{{ route('admin.inspectionName.destroyQuestion') }}";
        var token = "{{ csrf_token() }}";
        var insid = "{{ $name->id }}";
        $.easyAjax({
            url: url,
            type: "POST",
            data: {'_token': token, 'insid':insid,'category':category},
            success: function(response){
                if (response.status == "success") {
                    $("#categ"+category).remove();
                }
            }
        })
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
    $(document).on('change','.changereponse', function () {
        var quest = $(this).data('quesid');
        var inputfeild = $(this).val();
        if(inputfeild){
            $.easyAjax({
                url: '{{route('admin.inspectionName.getData')}}',
                type: "POST",
                data: {
                    '_token': '{{ csrf_token() }}',
                    'inputfeild': inputfeild,
                },
                success: function (data) {
                    $("#previewblock"+quest).html(data);
                }
            });
        }
    });



    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.inspectionName.storeImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#inspectionID').val();
        formData.append('inspection_id', ids);
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "@lang('Inspection created successfully')";
        $.showToastr(msgs, 'success');
        window.location.href = '{{ route('admin.inspection-name.index') }}'

    });

    $('.summernote').summernote({
        height: 210,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });


    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.inspectionName.updateName', [$name->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.inspectionID){
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        inspectionID = response.inspectionID;
                        $('#inspectionID').val(response.inspectionID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.inspectionUpdatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.inspection-name.index') }}'
                    }
                   /* window.location.reload();*/
                }
            }
        })
    });

    //    update inspection
    function removeFile(id) {
        var url = "{{ route('admin.inspectionName.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    $("#fileid"+id).remove();
                }
            }
        })

    };


</script>
@endpush