@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropify/dist/css/dropify.css') }}">
    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
        .card-header {
            background: #efefef;
            padding: 10px;
        }
        .card-body {
            padding: 10px;
        }
        .boxdesign {
            padding: 10px;
            background-color: #efefef;
            margin: 10px 0px;
        }

        .float-right {
            float: right !important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">

                    <div class="col-md-5">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#home">@lang('app.inspection')</a></li>
                            <li><a data-toggle="pill" href="#menu1">@lang('app.details')</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6"><h2><b>{{ $inspection->name }}</b></h2></div>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                            <?php
                        $labelscolors = array();
                            $inspectionquescount = \App\InspectionQuestion::where('inspection_name_id',$inspection->id)->count();
                            $inspectionanswercount = \App\InspectionAnswer::where('inspection_id',$inspection->id)->groupBy('inspection_question_id')->get();
                            $dataarray = $labelscolor = $labelarray = array();
                            $labelscolor =  \App\InspectionAnswer::select('*',\Illuminate\Support\Facades\DB::raw('max(id) as mid,color as labcolor'))->where('inspection_id',$inspection->id)->where('answered_by',$userid)->where('replytype','<>','comment')->orderBy('created_at','desc')->groupBy('inspection_question_id')->get();
                            $numbercount = 0;
                            foreach ($labelscolor as $labelscol){
                                $showcolr = \App\InspectionAnswer::find($labelscol->mid);
                                $smalabel = $showcolr->color;
                                $labelscolors[] = $smalabel;
                            }
                        $arraycountval = array_count_values($labelscolors);
                        $labelscolors = array_keys($arraycountval);
                        $dataarray = array_values($arraycountval);
                        $labelarray = array_values($arraycountval);
                          if($inspectionquescount>count($inspectionanswercount)){
                                $dataarray[] = $inspectionquescount-count($inspectionanswercount);
                                $labelarray[] = $inspectionquescount-count($inspectionanswercount);
                                $labelscolors[] = '#ebebeb';
                            }
                            ?>
                            <div class="col-md-2 text-right">
                                <div style="margin-top: 60%;text-align: center;">
                                    <h3>{{ count($inspectionanswercount).'/'.$inspectionquescount }}</h3>
                                    <p>Item inspected</p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="chart-container">
                                    <div class="pie-chart-container">
                                        <canvas id="pie-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="chart-container">
                                    <div class="pie-chart-container">
                                        <canvas id="pie-chart-response"></canvas>
                                    </div>
                                </div>
                                <?php $responsedataarray = $responselabelarray = array();
                                $responselabelarray = array('Open','Initiated','Not accepted','Closed');
                                foreach ($responselabelarray as $item) {
                                    $responsedataarray[] = \App\Observations::select()->where('inspection_id',$inspection->id)->where('status',$item)->count();
                                }
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php  $categoryarray = \App\InspectionQuestion::where('inspection_name_id',$inspection->id)->groupBy('category')->get();
                                    $cat= 1;     foreach ($categoryarray as $category){
                                    ?>
                                    <div class="form-group">
                                        <p>{{ $cat }}. {{ $category->category }}</p>
                                    </div>
                                    <?php  $questionarray = \App\InspectionQuestion::where('inspection_name_id',$inspection->id)->where('category',$category->category)->get();
                                    $ques= 1;     foreach ($questionarray as $question){
                                      ?>
                                    <div class="card mb-10">
                                        <div class="card-header">
                                            <div class="col-md-8"><p class="rowcat">{{ $cat.'.'.$ques }}.  {{ $question->question }}</p></div>
                                            <div class="col-md-4 text-right">
                                                <?php echo get_response_field($question->input_field_id,$question,$userid,$assigned->id);?>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <form method="post"  action="{{ route('admin.inspectionName.storeAnswer') }}" enctype="multipart/form-data" >
                                                    <div class="row">
                                                        <div class="col-md-6 form-group">
                                                            <textarea class="form-control" name="answer" placeholder="Comment"></textarea>
                                                        </div>
                                                        <div class="col-md-1 form-group dropheight">
                                                            <input type="file" class="dropify" name="attachments[]" multiple  />
                                                        </div>
                                                        <div class="col-md-1 form-group">
                                                            <br>
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="replytype" value="comment" />
                                                            <input type="hidden" name="formid" value="{{ $assigned->id }}" />
                                                            <input type="hidden" name="questionid" value="{{ $question->id }}" />
                                                            <button type="submit" class="comment-submit btn btn-success btn-block" data-questionid="{{ $question->id }}">@lang('app.save')</button>

                                                        </div>
                                                        <div class="col-md-1 form-group text-center m-t-30">
                                                            Or
                                                        </div>
                                                        <div class="col-md-2 form-group dropheight m-t-20">
                                                            <a class="btn btn-primary" href="{{ route('admin.inspectionName.AddObservation',[$inspection->id,$question->id]) }}" >Create Observation</a>
                                                        </div>
                                                    </div>
                                                </form>
                                                <ul class="nav nav-pills">
                                                    <?php $inspectionresonselist = \App\InspectionAnswer::where('inspection_id',$inspection->id)->where('inspection_question_id',$question->id)->where('replytype','response')->orderBy('id','desc')->get();?>
                                                    <?php $inspectioncommentlist = \App\InspectionAnswer::where('inspection_id',$inspection->id)->where('inspection_question_id',$question->id)->where('replytype','comment')->orderBy('id','desc')->get();?>
                                                    <?php $inspectionattachmentlist = \App\InspectionFile::where('inspection_id',$inspection->id)->where('question_id',$question->id)->where('filetype','attachment')->orderBy('id','desc')->get();?>
                                                    <?php $inspectionphotolist = \App\InspectionFile::where('inspection_id',$inspection->id)->where('question_id',$question->id)->where('filetype','photo')->orderBy('id','desc')->get();?>
                                                    <?php $observationsarray = \App\Observations::where('inspection_id',$inspection->id)->where('question_id',$question->id)->orderBy('id','desc')->get();?>
                                                    <li class="active"><a data-toggle="pill" href="#all{{ $question->id }}" >All <span class="btn btn-warning btn-sm">{{ count($inspectionresonselist)+count($inspectionattachmentlist)+count($inspectionphotolist) +count($inspectioncommentlist)+count($observationsarray) }}</span></a></li>
                                                    <li><a data-toggle="pill" href="#home{{ $question->id }}" >Response Change: <span class="btn btn-warning btn-sm">{{ count($inspectionresonselist) }}</span></a></li>
                                                    <li><a data-toggle="pill" href="#menu1{{ $question->id }}" >Attachments: <span class="btn btn-warning btn-sm">{{ count($inspectionattachmentlist) }}</span></a></li>
                                                    <li><a data-toggle="pill" href="#menu2{{ $question->id }}" >Photos: <span class="btn btn-warning btn-sm">{{ count($inspectionphotolist) }}</span></a></li>
                                                    <li><a data-toggle="pill" href="#comments{{ $question->id }}">Comments: <span class="btn btn-warning btn-sm">{{ count($inspectioncommentlist) }}</span></a></li>
                                                    <li><a data-toggle="pill" href="#observations{{ $question->id }}">Observations: <span class="btn btn-warning btn-sm">{{ count($observationsarray) }}</span></a></li>
                                                 </ul>
                                                <div class="tab-content" style="margin-top: 10px;">
                                                    <div id="all{{ $question->id }}" class="tab-pane fade active in">

                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($inspectionresonselist as $inspectionres){?>
                                                            <li>
                                                                <a href="javascript:void(0);" >Response from {{ get_user_name($inspectionres->answered_by) }}</a>
                                                                <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionres->created_at));?></a>
                                                                <p>{{ $inspectionres->answer }}</p>
                                                            </li>
                                                            <?php }?>
                                                                <?php if(count($inspectionattachmentlist)>0){ ?>
                                                                <li>
                                                                    <div class="row">
                                                                        <?php
                                                                        foreach ($inspectionattachmentlist as $inspectionattach){?>
                                                                        <div class="col-md-2 form-group full-image">
                                                                        <a href="javascript:void(0);" >Attachment By {{ get_user_name($inspectionattach->user_id) }}</a>
                                                                        <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionattach->created_at));?></a>
                                                                        <p>@if($inspectionattach->external_link != '')
                                                                            <?php $imgurl = $inspectionattach->external_link;?>
                                                                        @elseif($storage == 'local')
                                                                            <?php $imgurl = uploads_url().'inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 's3')
                                                                            <?php $imgurl = $url.$companyid.'/inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 'google')
                                                                            <?php $imgurl = $inspectionattach->google_url;?>
                                                                        @elseif($storage == 'dropbox')
                                                                            <?php $imgurl = $inspectionattach->dropbox_link;?>
                                                                        @endif
                                                                        {!! mimetype_thumbnail($inspectionattach->filename,$imgurl)  !!}
                                                                        </p>
                                                                        </div>
                                                                        <?php }?>
                                                                    </div>
                                                                </li>
                                                                <?php }?>
                                                                <?php if(count($inspectionphotolist)>0){ ?>
                                                                <li>
                                                                    <div class="row">
                                                                    <?php
                                                                    foreach ($inspectionphotolist as $inspectionattach){?>
                                                                    <div class="col-md-2 form-group full-image">
                                                                        <a href="javascript:void(0);" >Attachment By {{ get_user_name($inspectionattach->user_id) }}</a>
                                                                        <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionattach->created_at));?></a>
                                                                        <p>
                                                                        @if($inspectionattach->external_link != '')
                                                                            <?php $imgurl = $inspectionattach->external_link;?>
                                                                        @elseif($storage == 'local')
                                                                                <?php $imgurl = uploads_url().'inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 's3')
                                                                            <?php $imgurl = $url.$companyid.'/inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 'google')
                                                                            <?php $imgurl = $inspectionattach->google_url;?>
                                                                        @elseif($storage == 'dropbox')
                                                                            <?php $imgurl = $inspectionattach->dropbox_link;?>
                                                                        @endif
                                                                        {!! mimetype_thumbnail($inspectionattach->filename,$imgurl)  !!}
                                                                        </p>
                                                                    </div>
                                                                    <?php }?>
                                                                    </div>
                                                                </li>
                                                                <?php }?>
                                                                <?php
                                                                foreach ($inspectioncommentlist as $inspectionres){?>
                                                                <li>
                                                                    <a href="javascript:void(0);" >{{ get_user_name($inspectionres->answered_by) }} left Comment a on <?php echo date('d M Y',strtotime($inspectionres->created_at));?></a>
                                                                    <p>{{ $inspectionres->answer }}</p>
                                                                </li>
                                                                <?php }?>
                                                                <?php
                                                                foreach ($observationsarray as $observations){?>
                                                                <li>
                                                                    <a href="javascript:void(0);" >Observation created by {{ get_user_name($observations->added_by) }} on <?php echo date('d M Y',strtotime($observations->created_at));?></a>
                                                                    <p>{{ $observations->title }}</p>
                                                                </li>
                                                                <?php }?>
                                                        </ul>
                                                    </div>
                                                    <div id="home{{ $question->id }}" class="tab-pane fade">
                                                        <?php if(count($inspectionresonselist)>0){
                                                        ?>
                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($inspectionresonselist as $inspectionres){?>
                                                            <li>
                                                                <a href="javascript:void(0);" >Response from {{ get_user_name($inspectionres->answered_by) }}</a>
                                                                <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionres->created_at));?></a>
                                                                <p>{{ $inspectionres->answer }}</p>
                                                            </li>
                                                            <?php }?>

                                                        </ul>
                                                        <?php }?>
                                                    </div>
                                                    <div id="menu1{{ $question->id }}" class="tab-pane fade">
                                                        <?php if(count($inspectionattachmentlist)>0){
                                                        ?>
                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($inspectionattachmentlist as $inspectionattach){?>
                                                            <li>
                                                                <div class="col-md-2 form-group full-image">
                                                                    <a href="javascript:void(0);" >Attachment By {{ get_user_name($inspectionattach->user_id) }}</a>
                                                                    <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionattach->created_at));?></a>
                                                                     <a href="javascript:void(0);" >Attachment By {{ get_user_name($inspectionattach->user_id) }}</a>
                                                                    <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionattach->created_at));?></a>
                                                                    <p>
                                                                        @if($inspectionattach->external_link != '')
                                                                            <?php $imgurl = $inspectionattach->external_link;?>
                                                                        @elseif($storage == 'local')
                                                                            <?php $imgurl = uploads_url().'inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 's3')
                                                                            <?php $imgurl = $url.$companyid.'/inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 'google')
                                                                            <?php $imgurl = $inspectionattach->google_url;?>
                                                                        @elseif($storage == 'dropbox')
                                                                            <?php $imgurl = $inspectionattach->dropbox_link;?>
                                                                        @endif
                                                                        {!! mimetype_thumbnail($inspectionattach->filename,$imgurl)  !!}
                                                                    </p>
                                                            </div>
                                                            </li>
                                                            <?php }?>
                                                        </ul>
                                                        <?php }?>
                                                    </div>
                                                    <div id="menu2{{ $question->id }}" class="tab-pane fade">
                                                        <?php if(count($inspectionphotolist)>0){
                                                        ?>
                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($inspectionphotolist as $inspectionattach){?>
                                                            <li>
                                                                <div class="col-md-2 form-group full-image">
                                                                    <a href="javascript:void(0);" >Attachment By {{ get_user_name($inspectionattach->user_id) }}</a>
                                                                    <a href="#" class="float-right"><?php echo date('d M Y',strtotime($inspectionattach->created_at));?></a>
                                                                    <p>
                                                                        @if($inspectionattach->external_link != '')
                                                                            <?php $imgurl = $inspectionattach->external_link;?>
                                                                        @elseif($storage == 'local')
                                                                            <?php $imgurl = uploads_url().'inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 's3')
                                                                            <?php $imgurl = $url.$companyid.'/inspection-files/'.$inspectionattach->inspection_id.'/'.$inspectionattach->hashname;?>
                                                                        @elseif($storage == 'google')
                                                                            <?php $imgurl = $inspectionattach->google_url;?>
                                                                        @elseif($storage == 'dropbox')
                                                                            <?php $imgurl = $inspectionattach->dropbox_link;?>
                                                                        @endif
                                                                        {!! mimetype_thumbnail($inspectionattach->filename,$imgurl)  !!}
                                                                    </p>
                                                                </div>
                                                            </li>
                                                            <?php }?>
                                                        </ul>
                                                        <?php }?>
                                                    </div>
                                                    <div id="comments{{ $question->id }}" class="tab-pane fade">

                                                        <?php if(count($inspectioncommentlist)>0){
                                                        ?>
                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($inspectioncommentlist as $inspectionres){?>
                                                            <li>
                                                                <a href="javascript:void(0);" >{{ get_user_name($inspectionres->answered_by) }} left Comment a on <?php echo date('d M Y',strtotime($inspectionres->created_at));?></a>
                                                                <p>{{ $inspectionres->answer }}</p>
                                                            </li>
                                                            <?php }?>
                                                        </ul>
                                                        <?php }?>
                                                    </div>
                                                    <div id="observations{{ $question->id }}" class="tab-pane fade">

                                                        <?php if(count($observationsarray)>0){
                                                        ?>
                                                        <ul class="timelinereply">
                                                            <?php
                                                            foreach ($observationsarray as $observations){?>
                                                            <li>
                                                                <a href="javascript:void(0);" >Observation created by {{ get_user_name($observations->added_by) }} on <?php echo date('d M Y',strtotime($observations->created_at));?></a>
                                                                <p>{{ $observations->title }}</p>
                                                            </li>
                                                            <?php }?>
                                                        </ul>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php $ques++; }?>
                                    <?php $cat++; }?>
                                </div>
                            </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.name')</b></td>
                                        <td  style="width: 35%;">{{ $inspection->name }}</td>
                                        <td style="width: 15%;"><b>@lang('app.type')</b></td>
                                        <td  style="width: 35%;">{{ get_ins_type_name($inspection->type) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.duedate')</b></td>
                                        <td  style="width: 35%;">{{ $assigned->due_date }}</td>
                                        <td style="width: 15%;"><b>@lang('app.location')</b></td>
                                        <td  style="width: 35%;">{{ $assigned->location }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.status')</b></td>
                                        <td  style="width: 35%;">{{ ucwords($assigned->status) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">@lang('app.status')</label>
                                <select class="selectpicker form-control" id="assign-status" name="status" data-style="form-control" required >
                                    <option value="open" @if($assigned->status=='open') selected @endif>Open</option>
                                    <option value="initiated" @if($assigned->status=='initiated') selected @endif>Initiated</option>
                                    <option value="completed" @if($assigned->status=='completed') selected @endif>Completed</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <button class="btn btn-primary"  id="save-status" value="{{ $assigned->id }}" >@lang('app.save')</button>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <b>Files</b>
                            <br>
                            @forelse($files as $file)
                                        <div class="col-md-1">
                                            @if($file->external_link != '')
                                                <?php $imgurl = $file->external_link;?>
                                            @elseif($storage == 'local')
                                                <?php $imgurl = uploads_url().'inspection-files/'.$file->inspection_id.'/'.$file->hashname;?>
                                            @elseif($storage == 's3')
                                                <?php $imgurl = awsurl().$file->company_id.'/inspection-files/'.$file->inspection_id.'/'.$file->hashname;?>
                                            @elseif($storage == 'google')
                                                <?php $imgurl = $file->google_url;?>
                                            @elseif($storage == 'dropbox')
                                                <?php $imgurl = $file->dropbox_link;?>
                                            @endif
                                                {!! mimetype_thumbnail($file->filename,$imgurl)  !!}
                                            <span class="clearfix">{{ $file->created_at->diffForHumans() }}</span>
                                        </div>
                            @empty
                                @lang('messages.noFileUploaded')
                            @endforelse
                        </div>
                        <div class="col-md-12">
                            <b>Description</b>
                            <br>
                            {!! $inspection->description !!}
                        </div>
                    </div>
                </div>
                </div>

            </div>
        </div>
    </div>
    <!-- .row -->


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropify/dist/js/dropify.js') }}"></script>
    <script src="{{ asset('js/Chart.js') }}"></script>
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>

    <script>
        $(".dropify").dropify({
            messages: {
                'default': '',
            }
        });

        $("#save-status").click(function () {
            var status = $("#assign-status").val();
            var token = "{{ csrf_token() }}";
            var val = $("#save-status").val();
            var url = '{{ route('admin.inspectionName.assignStatusUpdate') }}'
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, 'assignid':val, 'status':status},
                success: function (data) {
                    var msgs = "@lang('Status updated successfully')";
                    $.showToastr(msgs, 'success');
                }
            });
        });

        $('ul.showProjectTabs .projectTasks').addClass('tab-current');

        $('.delete-category').click(function () {
            var id = $(this).data('cat-id');
            var url = "{{ route('admin.inspectionName.destroyName',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                    }
                }
            });
        });

        function showData(val,row) {
            var url = "{{ route('admin.inspectionName.getData') }}";
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, 'id':val, 'row_id':row, 'formtype':'reply'},
                success: function (data) {
                    $('#ansData'+row).html(data.html);
                }
            });
        }

        function submitAnswer(qid,formid,opt=false) {
            var token = "{{ csrf_token() }}";
            if(opt){
                var val = $(".submitoption"+qid+opt).attr('data-value');
                var color = $(".submitoption"+qid+opt).attr('data-color');
            }else{
                var val = $("#answer"+qid).val();
                var color = $("#answer"+qid).attr('data-color');
            }
          $.easyAjax({
                url: '{{route('admin.inspectionName.storeAnswer')}}',
                container: '#answerForm',
                type: "POST",
                data: {'_token': token, 'questionid':qid, 'answer':val,  'formid':formid, 'color':color, 'replytype':'response'},
                success: function (data) {
                   /* var msgs = "@lang('Answer updated successfully')";
                    $.showToastr(msgs, 'success');
                    $(".submitoption"+qid+opt).css('background-color:'+color);*/
                     window.location.reload();
                }
            });
            return false;
        }
        $(function(){
            //get the pie chart canvas
            var ctx = $("#pie-chart");

            //pie chart data
            var data = {
                labels: <?php echo json_encode($labelarray);?>,
                datasets: [
                    {
                        label: "Users Count",
                        data: <?php echo json_encode($dataarray);?>,
                        backgroundColor:  ["<?php echo implode('","',$labelscolors);?>"]
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    fontStyle:'Normal',
                    text: "Inspection Item Status",
                    fontSize: 18,
                    fontColor: "#686868"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                }
            };

            //create Pie Chart class object
            var chart1 = new Chart(ctx, {
                type: "pie",
                data: data,
                options: options
            });
            //get the pie chart canvas
            var ctx = $("#pie-chart-response");

            //pie chart data
            var data = {
                labels: <?php echo json_encode($responselabelarray);?>,
                datasets: [
                    {
                        label: "Users Count",
                        data: <?php echo json_encode($responsedataarray);?>,
                        backgroundColor: [
                            "#FAC935",
                            "#0d6bdc",
                            "#A576FF",
                            "#2E8B57",
                            "#1aa911",
                        ]
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    fontStyle:'Normal',
                    text: "Observations response",
                    fontSize: 18,
                    fontColor: "#686868"
                },
                legend: {
                    display: true,
                    position: "bottom",
                    padding: "50",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                }
            };

            //create Pie Chart class object
            var chart1 = new Chart(ctx, {
                type: "pie",
                data: data,
                options: options
            });

        });
    </script>
@endpush