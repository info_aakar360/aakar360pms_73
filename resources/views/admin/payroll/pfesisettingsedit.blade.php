@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel ">
                <div class="panel-heading">@lang('modules.payrollsettings.pfsettings')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createPfEsiSettings','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>@lang('modules.payrollsettings.effectivedate')</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.payrollsettings.pfstatus')</h3>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>PF Applicable</label>
                                                                <div class="form-group">
                                                                    <select id="pf_applicable" name="pf_applicable" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label>PF Number</label>
                                                                <div class="form-group">
                                                                    <input id="pf_number" name="pf_number" class="form-control" placeholder="Enter PF Number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>PF Joining Date</label>
                                                                <div class="form-group">
                                                                    <input id="pf_joining_date" name="pf_joining_date" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>UAN</label>
                                                                <div class="form-group">
                                                                    <input id="uan" name="uan" class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>EPS Number</label>
                                                                <div class="form-group">
                                                                    <input id="eps_number" name="eps_number" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>VFO</label>
                                                                <div class="form-group">
                                                                    <input id="vfo" name="vfo" value="0" class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>ESI Applicable</label>
                                                                <div class="form-group">
                                                                    <select id="esi_applicable" name="esi_applicable" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>ESI Number</label>
                                                                <div class="form-group">
                                                                    <input id="esi_number" name="esi_number" value="0" class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <hr/>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>PF Override</label>
                                                                <div class="form-group">
                                                                    <select id="pf_override" name="pf_override" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>PF Employee</label>
                                                                <div class="form-group">
                                                                    <input id="pf_employee" name="pf_employee" @if(!empty($pfEsi->emp_pf_contribution)) value="{{$pfEsi->emp_pf_contribution}}" @endif class="form-control" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>Pf Employer</label>
                                                                <div class="form-group">
                                                                    <input id="pf_employer" name="pf_employer" @if(!empty($pfEsi->employer_pf_contibution)) value="{{$pfEsi->employer_pf_contibution}}" @endif class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>ESI Override</label>
                                                                <div class="form-group">
                                                                    <select id="esi_override" name="esi_override"class="form-control" >
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>ESI Employee</label>
                                                                <div class="form-group">
                                                                    <input id="esi_employee" name="esi_employee" value="0" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>ESI Employer</label>
                                                                <div class="form-group">
                                                                    <input id="esi_employer" name="esi_employer"  @if(!empty($pfEsi->employer_contribution)) value="{{$pfEsi->employer_contribution}}" @endif class="form-control" >
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                    @lang('app.save')
                                                                </button>
                                                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                            </div>
                                                            {!! Form::hidden('id',$pfEsi->id) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('#custum_formula_select').multiselect({
            nonSelectedText: 'Select Framework',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth:'400px'
        });



        jQuery('#effective_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        jQuery('#pf_joining_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.updatePfEsiSettings')}}',
                container: '#createPfEsiSettings',
                type: "POST",
                redirect: true,
                data: $('#createPfEsiSettings').serialize()
            })
        });
    </script>
@endpush