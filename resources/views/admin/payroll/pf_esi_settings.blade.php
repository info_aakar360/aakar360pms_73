@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <style>
        #custom_formula .btn-group, #custom_formula .btn-group .multiselect.dropdown-toggle{
            width: 100% !important;
        }
        #custom_formula {
            width: 97%;  
            top: 70px;
            left: 7px;
            padding: 10px 10px;
            visibility: visible;
            background-color: #f9f9f9;
            color: #fff;
            border-radius: 2px;
            position: absolute;
            z-index: 1;
            border: 1px solid gainsboro;
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel ">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab"><a href="#pf" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Pf</span> </a> </li>
                                <li class="tab"><a href="#esi" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">ESI</span> </a> </li>
                                <li class="tab"><a href="#overview" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Overview</span> </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="pf">
                        <div class="panel-heading">@lang('modules.payrollsettings.pfsettings')</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                {!! Form::open(['id'=>'createPfEsiSettings','class'=>'ajax-form','method'=>'POST']) !!}
                                <div class="form-body">
                                    <div class="row">
                                        <input type="hidden" id="pfesiid" value="{{$pfesisettings->id}}">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label>@lang('modules.payrollsettings.effectivedate')</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Provident Fund Status</label>
                                                <div class="form-group">
                                                    <select  class="form-control" name="fund_status" id="fund_status" value="">
                                                        <option  @if($pfesisettings->pf_applicable == 'off') selected @endif value="off">Off</option>
                                                        <option @if($pfesisettings->pf_applicable == 'on') selected @endif value="on">on</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 ">
                                            <div id="accordion">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                <h3>@lang('modules.payrollsettings.pfstatus')</h3>
                                                            </button>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.pfminimumcalamount')</label>
                                                                        <div class="form-group">
                                                                            <input id="min_calc_amount" name="min_calc_amount" class="form-control" @if(!empty($pfesisettings->min_pf_calc_amount)) value="{{$pfesisettings->min_pf_calc_amount}}" @endif>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.emppfcontribution')</label>
                                                                        <div class="form-group">
                                                                            <select id="emp_pf_contribution" name="emp_pf_contribution" class="form-control">
                                                                                <option @if(!empty($pfesisettings->employer_pf_contibution) && $pfesisettings->employer_pf_contibution == "Basic*12%") selected @endif value="Basic*12%">Basic*12%</option>
                                                                                <option @if(!empty($pfesisettings->employer_pf_contibution) && $pfesisettings->employer_pf_contibution == "(Basic + Special Allowance) * 12%") selected @endif value="(Basic + Special Allowance) * 12%">(Basic + Special Allowance) * 12%</option>
                                                                                <option @if(!empty($pfesisettings->employer_pf_contibution) && $pfesisettings->employer_pf_contibution == "Fixed Amount") selected @endif value="Fixed Amount">Fixed Amount</option>
                                                                                <option @if(!empty($pfesisettings->employer_pf_contibution) && $pfesisettings->employer_pf_contibution == "Custom Formula") selected @endif value="Custom Formula">Custom Formula</option>
                                                                            </select>
                                                                            <div id="custom_formula" style="display: none">
                                                                            <div class="row" >
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" >
                                                                                            <select id="custum_formula_select" name="custum_formula_select[]" multiple class="form-control">
                                                                                                @foreach($componets as $compo)<option value="{{$compo->id}}">{{$compo->component_name}}</option>@endforeach
                                                                                                @foreach($variables as $variable)<option value="var{{$variable->id}}">{{$variable->component_name}}</option>@endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <label ><i class="fa fa-star"></i></label>
                                                                                    </div>
                                                                                    <div class="col-md-5">
                                                                                        <div class="form-group" >
                                                                                            <input id="custFurmulPerc" name="custum_formula_percent" class="form-control" value="12%" placeholder="12%">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="showselected"></div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" id="fixed_amount" style="display: none">
                                                                    <div class="form-group" >
                                                                        <input id="fixed_amount" name="fixed_amount" class="form-control" placeholder="Enter Fixed Amount">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.employerpfcontribution')</label>
                                                                        <div class="form-group">
                                                                            <input id="employer_pf_contibution" name="employer_pf_contibution" class="form-control" @if(!empty($pfesisettings->employer_pf_contibution)) value="{{$pfesisettings->employer_pf_contibution}}" @endif readonly>
                                                                        </div>
                                                                        <div class="showselected"></div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.enableminceiling')</label>
                                                                        <div class="form-group">
                                                                              <select id="enable_min_ceilig" name="enable_min_ceilig" class="form-control">
                                                                                     <option @if(!empty($pfesisettings->enable_min_ceilig) && $pfesisettings->enable_min_ceilig == 'yes') selected @endif value="yes">Yes</option>
                                                                                     <option @if(!empty($pfesisettings->enable_min_ceilig) && $pfesisettings->enable_min_ceilig == 'no') selected @endif value="no">No</option>
                                                                               </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.lopdependent')</label>
                                                                        <div class="form-group">
                                                                            <select id="lop_dependent" name="lop_dependent" class="form-control">
                                                                                <option value="">Please Select</option>
                                                                                <option @if(!empty($pfesisettings->lop_dependent) && $pfesisettings->lop_dependent == 'yes') selected @endif value="yes">Yes</option>
                                                                                <option @if(!empty($pfesisettings->lop_dependent) && $pfesisettings->lop_dependent == 'no') selected @endif value="no">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.employercontctc')</label>
                                                                        <div class="form-group">
                                                                            <select id="emploer_ctc_contribution" name="emploer_ctc_contribution" class="form-control">
                                                                                <option value="">Please Select</option>
                                                                                <option @if(!empty($pfesisettings->emploer_ctc_contribution) && $pfesisettings->emploer_ctc_contribution == 'yes') selected @endif value="yes">Yes</option>
                                                                                <option @if(!empty($pfesisettings->emploer_ctc_contribution) && $pfesisettings->emploer_ctc_contribution == 'no') selected @endif value="no">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.allowindpfoveride')</label>
                                                                        <div class="form-group">
                                                                            <select id="allow_override" name="allow_override" class="form-control">
                                                                                <option @if(!empty($pfesisettings->allow_override) && $pfesisettings->allow_override == 'yes') selected @endif value="yes">Yes</option>
                                                                                <option @if(!empty($pfesisettings->allow_override) && $pfesisettings->allow_override == 'no') selected @endif value="no">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>@lang('modules.payrollsettings.includadmincharges')</label>
                                                                        <div class="form-group">
                                                                            <select id="amin_ctc_charges" name="amin_ctc_charges" class="form-control">
                                                                                <option @if(!empty($pfesisettings->admin_ctc_charges) && $pfesisettings->admin_ctc_charges == 'yes') selected @endif value="yes">Yes</option>
                                                                                <option @if(!empty($pfesisettings->admin_ctc_charges) && $pfesisettings->admin_ctc_charges == 'no') selected @endif value="no">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <hr/>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                        @lang('app.save')
                                                                    </button>
                                                                    <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                                </div>
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="esi">
                        {!! Form::open(['id'=>'createEsiSettings','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label>@lang('modules.payrollsettings.effectivedate')</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>ESI Status</label>
                                    <div class="form-group">
                                        <select  class="form-control" name="esi_status" id="esi_status" value="">
                                            <option @if(!empty($pfesisettings->esi_applicable) && $pfesisettings->esi_applicable == 'off') selected @endif value="off">Off</option>
                                            <option @if(!empty($pfesisettings->esi_applicable) && $pfesisettings->esi_applicable == 'on') selected @endif value="on">on</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="panel ">
                                <div class="card-header">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <h3>@lang('modules.payrollsettings.esisettings')</h3>
                                        </button>
                                    </h5>
                                </div>
                                <div class="col-md-12">
                                    <label>@lang('modules.payrollsettings.employercont')</label>
                                    <div class="form-group">
                                        <input type="text" id="employer_cont" name="employer_cont" value="3.25% of Gross Salary" placeholder="3.25% of Gross Salary" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label>Statutory Maximum Monthly Gross For ESI Calculation</label>
                                        <div class="form-group">
                                            <input id="max_month_gross" name="max_month_gross" class="form-control" @if(!empty($pfesisettings->employer_contribution) ) value = "{{$pfesisettings->employer_contribution}}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Employee Contribution</label>
                                        <div class="form-group">
                                            <input id="employee_contribution" name="employee_contribution" value = "0.75% of Gross Salary" class="form-control" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label>@lang('modules.payrollsettings.includeemployerctc')</label>
                                        <div class="form-group">
                                            <select id="include_empl_ctc" name="include_empl_ctc" class="form-control">
                                                <option @if(!empty($pfesisettings->include_empl_ctc) && $pfesisettings->include_empl_ctc == 'yes') selected @endif value="yes">Yes</option>
                                                <option @if(!empty($pfesisettings->include_empl_ctc) && $pfesisettings->include_empl_ctc == 'no') selected @endif value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>@lang('modules.payrollsettings.allowindoverride')</label>
                                        <div class="form-group">
                                            <select id="allow_esi_override" name="allow_esi_override" class="form-control">
                                                <option @if(!empty($pfesisettings->allow_esi_override) && $pfesisettings->allow_esi_override == 'yes') selected @endif value="yes">Yes</option>
                                                <option @if(!empty($pfesisettings->allow_esi_override) && $pfesisettings->allow_esi_override == 'no') selected @endif value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" id="save-form-3" class="btn btn-success"><i class="fa fa-check"></i>
                                            @lang('app.save')
                                        </button>
                                        <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="overview">
                        <div class="col-md-12">
                                <div class="panel">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select id="periodtype" name="periodtype" class="form-control">
                                                    <option value="monthly">Monthly</option>
                                                    <option value="yearly">Yearly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('app.id')</th>
                                                        <th>@lang('modules.payrollsettings.employeename')</th>
                                                        <th>@lang('modules.payrollsettings.pfapplicable')</th>
                                                        <th >@lang('modules.payrollsettings.pfnumber')</th>
                                                        <th>@lang('modules.payrollsettings.pfdatejoined')</th>
                                                        <th>@lang('modules.payrollsettings.uan')</th>
                                                        <th>@lang('modules.payrollsettings.epsnumber')</th>
                                                        <th>@lang('modules.payrollsettings.pfoverride')</th>
                                                        <th>@lang('modules.payrollsettings.pfemployer')</th>
                                                        <th>@lang('modules.payrollsettings.pfemployee')</th>
                                                        <th>@lang('modules.payrollsettings.vps')</th>
                                                        <th>@lang('modules.payrollsettings.includeedli')</th>
                                                        <th>@lang('modules.payrollsettings.esiapplicaple')</th>
                                                        <th>@lang('modules.payrollsettings.esinumber')</th>
                                                        <th>@lang('modules.payrollsettings.esioverride')</th>
                                                        <th>@lang('modules.payrollsettings.esiemployee')</th>
                                                        <th>@lang('modules.payrollsettings.esiemployer')</th>
                                                        <th>@lang('app.action')</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        //pf setting Status On/off
        $('#fund_status').change(function () {
            var val = $(this).val();
            var id = $('#pfesiid').val();
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: '{{route('admin.payroll.updatePfStatus')}}',
                type: "POST",
                data: {
                    id: id,
                    status: val,
                    _token: token
                },
                success: function (response) {

                }
            })
        });
        //esi setting Status On/off
        $('#esi_status').change(function () {
            var val = $(this).val();
            var id = $('#pfesiid').val();
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: '{{route('admin.payroll.updateEsiStatus')}}',
                type: "POST",
                data: {
                    id: id,
                    status: val,
                    _token: token
                },
                success: function (response) {

                }
            })
        });

        var table;

        $(function() {
            loadTable();

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.employees.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });


        });
        function loadTable(){
            var periodtype = $('#periodtype').val();
            table = $('#users-table').dataTable({
                responsive:true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.payroll.data') !!}?periodtype=' + periodtype,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'pfapplicable', name: 'pfapplicable' },
                    { data: 'pfnumber', name: 'pfnumber' },
                    { data: 'pfdatejoined', name: 'pfdatejoined'},
                    { data: 'uan', name: 'uan' },
                    { data: 'epsnumber', name: 'epsnumber' },
                    { data: 'pfoverride', name: 'pfoverride' },
                    { data: 'pfemployer', name: 'pfemployer' },
                    { data: 'pfemployee', name: 'pfemployee' },
                    { data: 'vps', name: 'vps' },
                    { data: 'includeedli', name: 'includeedli' },
                    { data: 'esiapplicable', name: 'esiapplicable' },
                    { data: 'esinumber', name: 'esinumber' },
                    { data: 'esioverride', name: 'esioverride' },
                    { data: 'esiemployee', name: 'esiemployee' },
                    { data: 'esiemployer', name: 'esiemployer' },
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
        }
        $('#custum_formula_select').multiselect({
            selectAllText:' Select all',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth:'400px',
            onChange: function(element, checked) {
                var brands = $('#custum_formula_select option:selected');
                var selected = [];
                $(brands).each(function(index, brand){
                    selected.push([$(this).val()]);
                });
                $('.showselected').html('('+selected+')'+'*12%');
                console.log(selected);
            }
        });
        jQuery('#effective_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $("#emp_pf_contribution").change(function () {
            var value = $(this).val();
            $("#employer_pf_contibution").val(value);
            if($(this).val() == "Fixed Amount"){
                $("#fixed_amount").show();
            }else{$("#fixed_amount").hide();}
            if($(this).val() == "Custom Formula"){
                $("#custom_formula").show();
            }else{$("#custom_formula").hide();}
        });

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.storePfEsiSettings')}}',
                container: '#createPfEsiSettings',
                type: "POST",
                redirect: true,
                data: $('#createPfEsiSettings').serialize()
            })
        });
        $('#save-form-3').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.storeEsiSettings')}}',
                container: '#createEsiSettings',
                type: "POST",
                redirect: true,
                data: $('#createEsiSettings').serialize()
            })
        });


    </script>
@endpush