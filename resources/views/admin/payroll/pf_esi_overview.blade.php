@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="periodtype" name="periodtype" class="form-control">
                                <option value="monthly">Monthly</option>
                                <option value="yearly">Yearly</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" onclick="exportData()"  class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                        </div>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.id')</th>
                            <th>@lang('modules.payrollsettings.employeename')</th>
                            <th>@lang('modules.payrollsettings.pfapplicable')</th>
                            <th >@lang('modules.payrollsettings.pfnumber')</th>
                            <th>@lang('modules.payrollsettings.pfdatejoined')</th>
                            <th>@lang('modules.payrollsettings.uan')</th>
                            <th>@lang('modules.payrollsettings.epsnumber')</th>
                            <th>@lang('modules.payrollsettings.pfoverride')</th>
                            <th>@lang('modules.payrollsettings.pfemployer')</th>
                            <th>@lang('modules.payrollsettings.pfemployee')</th>
                            <th>@lang('modules.payrollsettings.vps')</th>
                            <th>@lang('modules.payrollsettings.includeedli')</th>
                            <th>@lang('modules.payrollsettings.esiapplicaple')</th>
                            <th>@lang('modules.payrollsettings.esinumber')</th>
                            <th>@lang('modules.payrollsettings.esioverride')</th>
                            <th>@lang('modules.payrollsettings.esiemployee')</th>
                            <th>@lang('modules.payrollsettings.esiemployer')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        var table;

        $(function() {
            loadTable();

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.employees.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });


        });
        function loadTable(){

            var periodtype = $('#periodtype').val();
            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.payroll.data') !!}?periodtype=' + periodtype,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'pfapplicable', name: 'pfapplicable' },
                    { data: 'pfnumber', name: 'pfnumber' },
                    { data: 'pfdatejoined', name: 'pfdatejoined'},
                    { data: 'uan', name: 'uan' },
                    { data: 'epsnumber', name: 'epsnumber' },
                    { data: 'pfoverride', name: 'pfoverride' },
                    { data: 'pfemployer', name: 'pfemployer' },
                    { data: 'pfemployee', name: 'pfemployee' },
                    { data: 'vps', name: 'vps' },
                    { data: 'includeedli', name: 'includeedli' },
                    { data: 'esiapplicapl', name: 'esiapplicapl' },
                    { data: 'esinumber', name: 'esinumber' },
                    { data: 'esioverride', name: 'esioverride' },
                    { data: 'esiemployee', name: 'esiemployee' },
                    { data: 'esiemployer', name: 'esiemployer' },
                    { data: 'action', name: 'action', width: '15%' }


                ]
            });
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle('slide');
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var employee = $('#employee').val();
            var status   = $('#status').val();
            var role     = $('#role').val();

            var url = '{{ route('admin.employees.export', [':status' ,':employee', ':role']) }}';
            url = url.replace(':role', role);
            url = url.replace(':status', status);
            url = url.replace(':employee', employee);

            window.location.href = url;
        }

    </script>
@endpush