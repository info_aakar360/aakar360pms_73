@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel ">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab"><a href="#ptsettings" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Pt Settings</span> </a> </li>
                                <li class="tab"><a href="#ptoverview" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Overview</span> </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="ptsettings">
                <div class="panel-heading">@lang('modules.payrollsettings.ptsettings')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createPtSettings','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="col-md-6">
                                        <label>@lang('modules.payrollsettings.pt')</label>
                                        <div class="form-group">
                                            <select type="text" class="form-control" name="pt_applicable" id="pt" >
                                                <option value="no">Off</option>
                                                <option value="yes">ON</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>@lang('modules.payrollsettings.effectivedate')</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.payrollsettings.pt')</h3>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.ptstate')</label>
                                                                <div class="form-group">
                                                                    <select id="pt_state" name="pt_state" class="form-control">
                                                                        @foreach($states as $state)
                                                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.stateoverride')</label>
                                                                <div class="form-group">
                                                                    <select id="state_override" name="state_override" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions">
                                                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                    @lang('app.save')
                                                                </button>
                                                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>

                    </div>
                </div>
                    </div>
                    <div class="tab-pane active" id="ptoverview">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select id="periodtype" name="periodtype" class="form-control">
                                        <option value="monthly">Monthly</option>
                                        <option value="yearly">Yearly</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6 text-right hidden-xs">
                                <div class="form-group">
                                    <a href="javascript:;" onclick="exportData()"  class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                                        <thead>
                                            <tr>
                                                <th>@lang('app.id')</th>
                                                <th>@lang('app.name')</th>
                                                <th>@lang('modules.payrollsettings.worklocation')</th>
                                                <th >@lang('modules.payrollsettings.ptapplicable')</th>
                                                <th>@lang('modules.payrollsettings.ptstate')</th>
                                                <th>@lang('modules.payrollsettings.ptamount')</th>
                                                <th>@lang('app.action')</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        var table;

        $(function() {
            loadTable();

            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.employees.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });


        });
        function loadTable(){

            var periodtype = $('#periodtype').val();
            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.payroll.ptData') !!}?periodtype=' + periodtype,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'address', name: 'address' },
                    { data: 'ptapplicable', name: 'ptapplicable' },
                    { data: 'ptstate', name: 'ptstate'},
                    { data: 'ptamount', name: 'ptamount' },
                    { data: 'action', name: 'action' }

                ]
            });
        }

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.storePtSettings')}}',
                container: '#createPtSettings',
                type: "POST",
                redirect: true,
                data: $('#createPtSettings').serialize()
            })
        });
    </script>
@endpush