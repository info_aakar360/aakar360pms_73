@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            background: #ffffff !important;
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel ">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createDecSettings','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.payrollsettings.declarationsettings')</h3>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                            <div class="col-md-12">
                                                                <label>@lang('modules.payrollsettings.declarationwindow')</label>
                                                                <div class="form-group">
                                                                    <select id="declaration_window" name="declaration_window" class="form-control">
                                                                        <option value="off">Off</option>
                                                                        <option value="on">On</option>
                                                                    </select>
                                                                </div>
                                                            </div></br>
                                                            <div class="card-header" id="headingOne">
                                                                <h5 class="mb-0">
                                                                    <button class="btn btn-link"  aria-controls="collapseOne">
                                                                        <h3>@lang('modules.payrollsettings.monthly')</h3>
                                                                    </button>

                                                                </h5>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <label>@lang('modules.payrollsettings.monthlydeclaration')</label>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <select id="montly_select_from" name="montly_select_from" class="form-control">
                                                                                    @for($i=1;$i<=31;$i++)
                                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-1">
                                                                            <div class="form-group" >
                                                                                <label>to</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <select id="montly_select_to" name="montly_select_to" class="form-control">
                                                                                    @for($i=1;$i<=31;$i++)
                                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label>@lang('modules.payrollsettings.cutoffdeclaration')</label>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <select id="cutoff_select_month" name="cutoff_select_month" class="form-control">
                                                                                    @for ($m=1; $m<=12; $m++)
                                                                                        {{$month = date('F', mktime(0,0,0,$m, 1, date('Y')))}}
                                                                                        <option value="{{$month}}">{{$month}}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <select id="cutoff_select" name="cutoff_select" class="form-control">
                                                                                    @for($i=1;$i<=31;$i++)
                                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                                    @endfor
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                    </div>
                                                    <div class="row">
                                                         <div class="card-header" id="headingOne">
                                                                <h5 class="mb-0">
                                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                        <h3>Yearly Window</h3>
                                                                    </button>
                                                                </h5>
                                                         </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.yearlydeclaration')</label>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control" name="from" id="from" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control" name="to" id="to" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="checkbox"  name="checkall" id="checkall" onchange="checkAll()" class="align-right" value=""><span> Apply to All</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <h4>OR</h4>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <select name="select_employee[]" id="select_employee"  multiple="multiple" class="select2 m-b-10 select2-multiple" data-style="form-control">
                                                                            <option value="">Apply to Select Employee</option>
                                                                            @foreach($users as $user)
                                                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="checkbox" name="proof_submission" id="proof_submission" class="align-right" value="" onchange="checkSubmission()"><span>Mendatory Proof Submission</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                @lang('app.save')
                                                            </button>
                                                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                        </div>
                                                        {!! Form::close() !!}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('#declaration_window').change(function () {
            if ($(this).val("yes"))
                $('#autoUpdate').fadeOut('slow');
            else
                $('#autoUpdate').fadeIn('slow');
        });
        jQuery('#from').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        jQuery('#to').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        function checkSubmission() {
            if($('#proof_submission').is(":checked")){
                $('#proof_submission').val(1);
            }else{
                $('#proof_submission').val(0);
            }
        }
        function checkAll() {
            if($('#checkall').is(":checked")){
                $('#checkall').val(1);
            }else{
                $('#checkall').val(0);
            }
        }
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.storeDecSettings')}}',
                container: '#createDecSettings',
                type: "POST",
                redirect: true,
                data: $('#createDecSettings').serialize()
            })
        });
    </script>
@endpush