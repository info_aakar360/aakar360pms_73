@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.leaves.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="panel ">
                <div class="panel-heading" style="color: #002f76">@lang('modules.payrollsettings.payrollsettings')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createPaySettings','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>@lang('modules.payrollsettings.effectivedate')</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <h3>@lang('modules.payrollsettings.paysettings')</h3>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.payfrequecy')</label>
                                                                <div class="form-group">
                                                                    <select id="pay_frequncy" name="pay_frequncy" class="form-control">
                                                                        <option value="monthly">Monthly</option>
                                                                        <option value="hourly">Hourly</option>
                                                                        <option value="weekly">Weekly</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.salarybasedon')</label>
                                                                <div class="form-group">
                                                                    <select id="pay_structure" name="pay_structure" class="form-control">
                                                                        <option value="gross">Gross</option>
                                                                        <option value="ctc">CTC</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.paycycle')</label>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <span>From</span>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <select id="pay_cycle" name="pay_cycle" class="form-control">
                                                                                @for($i=1;$i<=31;$i++)
                                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                                @endfor
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <span>to 31</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.inputcycle');</label>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <span>From</span>
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <select id="input_cycle" name="input_cycle" class="form-control">
                                                                                @for($i=1;$i<=31;$i++)
                                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                                @endfor
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <span>to 31</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.payoutdate')</label>
                                                                <div class="form-group">
                                                                    <select id="payout_date" name="payout_date" class="form-control">
                                                                        @for($i=1;$i<=31;$i++)
                                                                            <option value="{{$i}}">{{$i}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.payrolldoj')</label>
                                                                <div class="form-group">
                                                                    <select id="doj_date" name="doj_date" class="form-control">
                                                                        @for($i=1;$i<=31;$i++)
                                                                            <option value="{{$i}}">{{$i}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-header">
                                                                <h5 class="mb-0">
                                                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                        <h3>@lang('modules.payrollsettings.paydaycalc')</h3>
                                                                    </button>
                                                                </h5>
                                                         </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.weeklyoff')</label>
                                                                <div class="form-group">
                                                                    <select id="include_weekly_leave" name="include_weekly_leave" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>@lang('modules.payrollsettings.includehollyday')</label>
                                                                <div class="form-group">
                                                                    <select id="include_hollyday_leave" name="include_hollyday_leave" class="form-control">
                                                                        <option value="yes">Yes</option>
                                                                        <option value="no">No</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions">
                                                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                                                    @lang('app.save')
                                                                </button>
                                                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#effective_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.payroll.store')}}',
                container: '#createPaySettings',
                type: "POST",
                redirect: true,
                data: $('#createPaySettings').serialize()
            })
        });
    </script>
@endpush