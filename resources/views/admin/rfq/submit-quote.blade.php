@extends('layouts.public-quote')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfq.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.Edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="@if($submitted) col-md-4 col-md-offset-4 @else col-md-12 @endif">

            <div class="panel panel-inverse">
                @if($submitted)
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body text-center">
                            <h1><i class="fa fa-exclamation-circle" style="color: orange; font-size: 120px"></i></h1>
                            <h2>Quotation already submitted.</h2>
                            <h5>You can not submit same quotation multiple times.</h5>
                        </div>
                    </div>
                @else
                <div class="panel-heading"> Quotation Information <span style="float: right">Supplier : {{$supplier->company_name}}</span></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createRfq','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}
                        <div class="form-body">
                            <input type="text" name="project_id" class="form-control" value="{{ $rfq->project_id }}" style="display: none;">
                            <h3 class="box-title">@lang('modules.rfq.productDetail')</h3>
                            <hr>
                            <div class="row">
                                <div class="table-responsive" id="pdata">
                                    <?php
                                    $html = '<table class="table"><thead><th>S.No.</th><th>Category</th><th>Brand</th><th>Quantity</th><th>Unit</th><th>Req. Date</th><th>Remark</th><th>Price</th><th>Tax (%)</th><th>Amount</th></thead><tbody>';
                                    if(count($tmpData)){
                                        $i = 1;
                                        foreach($tmpData as $data){
                                            $html .= '<tr><td>'.$i.'</td><td>'.get_local_product_name($data->cid).'<input type="hidden" name="cid[]" value="'.$data->cid.'"/></td><td>'.get_pbrand_name($data->bid).'<input type="hidden" name="bid[]" value="'.$data->bid.'"/></td><td>'.$data->quantity.'</td><input type="hidden" class="quantity" data-key="'.$i.'" name="quantity[]" value="'.$data->quantity.'"/><td>'.get_unit_name($data->unit).'<input type="hidden" name="unit[]" value="'.$data->unit.'"/></td><td>'.$data->expected_date.'<input type="hidden" name="dated[]" value="'.$data->expected_date.'"/></td><td>'.$data->remarks.'<input type="hidden" name="remarks[]" value="'.$data->remarks.'"/></td><td><input class="form-control price-inp" type="text" value="0" name="price[]" style="max-width: 150px;" data-key="'.$i.'"/></td><td><input class="form-control tax-inp" type="text" value="0" name="tax[]" style="max-width: 100px;" data-key="'.$i.'"/></td><td><span id="tamt'.$i.'" class="amount">0.00</span></td></tr>';
                                            $i++;
                                        }
                                        $html .= '<tr><td style="text-align: right" colspan="9">Grand Total : </td><td><span id="gtamt" style="font-weight: 900">0.00</span></td></tr>';
                                    }else{
                                        $html .= '<tr><td style="text-align: center" colspan="11">No Records Found.</td></tr>';
                                    }
                                    $html .= '</tbody></table>';
                                    echo $html;
                                    ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.rfq.remark')</b></label>
                                        <textarea name="remark" class="form-control" rows="5">{{ $rfq->remark }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><b>@lang('modules.rfq.paymentTerms')</b></label>
                                        <textarea name="payment_terms" class="form-control" rows="5">{{ $rfq->payment_terms }}</textarea>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions text-right">
                            <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('front.postQuotation', [$rfq->id, $supplier->id])}}',
                container: '#createRfq',
                type: "PATCH",
                redirect: true,
                data: $('#createRfq').serialize()
            })
        });
        $(document).on('keyup', '.price-inp, .tax-inp', function(){
            var inp = $(this);
            var row = inp.data('key');
            var pinp = $('.price-inp[data-key='+row+']');
            var taxinp = $('.tax-inp[data-key='+row+']');
            var qtyinp = $('.quantity[data-key='+row+']');
            var price = 0;
            var tax = 0;
            var qty = parseFloat(qtyinp.val());
            if(parseFloat(pinp.val()) > 0){
                price = parseFloat(pinp.val());
            }
            if(parseFloat(taxinp.val()) > 0){
                tax = parseFloat(taxinp.val());
            }
            amt = price+(price*tax/100);
            tamt = qty*amt;
            $('#tamt'+row).html(tamt.toFixed(2));
            var gtamt = 0.00;
            $('.amount').each(function(){
                gtamt = gtamt + parseFloat($(this).html());
            })
            $('#gtamt').html(gtamt.toFixed(2));
        });

    </script>
@endpush

