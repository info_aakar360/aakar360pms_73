                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $percenttype = $boqarray['percenttype'];
                                $percentvalue = $boqarray['percentvalue'];
                                $invoice = $boqarray['invoice'];
                                $invoicepayment = $boqarray['invoicepayment'];
                                $proproget = $boqarray['proproget'];
                                $projectid = $boqarray['projectid'];
                                $subprojectid = $boqarray['subprojectid'];
                                $snorow = $boqarray['snorow'];
                                foreach ($proproget as $propro){

                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }

                                $level = (int)$propro->level;
                                $parent = $propro->id;

                                $proprogetdatarra = \App\ProjectCostItemsProduct::join('invoices_boq','invoices_boq.product_id','=','project_cost_items_product.id')
                                                    ->join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')
                                                    ->select('invoices_boq.*','cost_items.cost_item_name')
                                                    ->where('invoices_boq.invoice_id',$invoice->id)->where('project_cost_items_product.position_id',$propro->id)->orderBy('project_cost_items_product.inc','asc')->get();

                                if(count($proprogetdatarra)>0){
                                ?>
                                    <tr >
                                        <td align="left">{{ $snorow }}</td>
                                        <td><?php if (isset($propro->itemname)){ echo sub_str($propro->itemname,20); } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                $tid = 1;    foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                                    $totalamt = $proprogetdat->amount;
                                    $paidamount = $paidpercent= 0;
                                $previnvoice = \App\InvoicePaymentsboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
                                    \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"),
                                ])->where('invoice_boq_id',$proprogetdat->id);
                                    if(!empty($invoicepayment->id)){
                                        $previnvoice =    $previnvoice->where('invoice_payments_id',$invoicepayment->id);
                                    }
                                $previnvoice = $previnvoice->first();

                                if(!empty($previnvoice->invpercent)&&!empty($previnvoice->invamount)){
                                    $paidamount = $previnvoice->invamount;
                                    $paidpercent = $previnvoice->invpercent;
                                }
                                    $remaining = $totalamt-$paidamount;
                                    $thisinvoicepercent = 100-$paidpercent;
                                    $thisinvoiceamt = $totalamt-$paidamount;
                                    if($percentvalue>$thisinvoicepercent){
                                        $percentvalue = $thisinvoicepercent;
                                    }
                                ?>
                                <tr data-costitemrow="{{ $proprogetdat->id }}" data-depth="1" class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">
                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td><input type="checkbox" name="taskid[]" checked value="{{ $proprogetdat->id }}"> {{  sub_str($proprogetdat->cost_item_name,20) }}</td>
                                    <td>₹{{ $totalamt }}</td>
                                    <td>₹{{ $paidamount }}</td>
                                    <td>₹{{ $remaining }}</td>
                                    <?php if($percenttype=='percent'){
                                    $thisinvoiceamt = ($percentvalue / 100) * $totalamt;
                                        ?>
                                    <td>{{ $percentvalue }}%</td>
                                    <td>₹{{ $thisinvoiceamt }} <input type="hidden" class="invrowamt" value="{{ $thisinvoiceamt }}" />
                                    <?php }elseif($percenttype=='custom'){
                                        ?>
                                    <td>
                                        <input type="text" placeholder="10 %" name="invoiceboqpercent[{{ $proprogetdat->id }}]"  class="calinvrow invrowpercent{{ $proprogetdat->id }}" data-type="percent" data-totalamt="{{ $remaining }}" data-taskid="{{ $proprogetdat->id }}" value="">
                                    </td>
                                    <td>
                                        <input type="text" name="invoiceboqamt[{{ $proprogetdat->id }}]" class="calinvrow invrowamt invrowamt{{ $proprogetdat->id }}" data-type="amount" data-totalamt="{{ $remaining }}" data-taskid="{{ $proprogetdat->id }}" value="{{ $thisinvoiceamt }}" />
                                    </td>
                                    <?php }else{?>
                                    <td>{{ $thisinvoicepercent }}%</td>
                                    <td>₹{{ $thisinvoiceamt }} <input type="hidden" class="invrowamt" value="{{ $thisinvoiceamt }}" />
                                    <?php }?>
                                    </td>
                                </tr>
                                <?php
                                $tid++;  }
                                }

                                }
                                $newlevel = $level+1;
                                $parent = $propro->id;
                                 $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();
                                $boqarray = array();
                                $boqarray['percenttype'] = $percenttype;
                                $boqarray['percentvalue'] = $percentvalue;
                                $boqarray['invoice'] = $invoice;
                                $boqarray['proproget'] = $proprogetlevel1;
                                $boqarray['projectid'] = $projectid;
                                $boqarray['subprojectid'] = $subprojectid;
                                $boqarray['invoicepayment'] = $invoicepayment;
                                $boqarray['level'] = $newlevel;
                                $boqarray['parent'] = $parent;
                                $boqarray['snorow'] = $snorow;
                                 echo boqhtml($boqarray);

                                } }
                                $invoicepayment = \App\InvoicePayments::where('id',$invoicepaymentid)->first();
                                $invoice = \App\Invoice::where('id',$invoiceid)->first();
                                $project = $invoice->project_id ?: 0;
                                $subproject = $invoice->subproject_id ?: 0;
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$project)->where('title',$subproject)->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();

                                if($percentvalue>100){
                                    $percentvalue = 100;
                                }
                                $boqarray = array();
                                $boqarray['percenttype'] = $percenttype;
                                $boqarray['percentvalue'] = $percentvalue;
                                $boqarray['invoice'] = $invoice;
                                $boqarray['invoicepayment'] = $invoicepayment;
                                $boqarray['proproget'] = $proproget;
                                $boqarray['projectid'] = $project;
                                $boqarray['subprojectid'] = $subproject;
                                $boqarray['snorow'] = 0;
                                $boqarray['level'] = 0;
                                $boqarray['parent'] = 0;
                                echo  boqhtml($boqarray);
                                ?>