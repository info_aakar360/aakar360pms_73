                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $invoice = $boqarray['invoice'];
                                $proproget = $boqarray['proproget'];
                                $projectid = $boqarray['projectid'];
                                $subprojectid = $boqarray['subprojectid'];
                                $snorow = $boqarray['snorow'];
                                foreach ($proproget as $propro){

                                $level = (int)$propro->level;

                                $newlevel = $level+1;
                                $parent = $propro->id;
                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }
                                $proprogetdatarra = \App\ProjectCostItemsProduct::join('invoices_boq','invoices_boq.product_id','=','project_cost_items_product.id')
                                                    ->join('cost_items','cost_items.id','=','project_cost_items_product.cost_items_id')
                                    ->select('invoices_boq.*','project_cost_items_product.qty','cost_items.cost_item_name','project_cost_items_product.finalrate')
                                                    ->where('invoices_boq.invoice_id',$invoice->id)->where('project_cost_items_product.position_id',$propro->id)->orderBy('project_cost_items_product.inc','asc')->get();
                                 if(count($proprogetdatarra)>0){
                                ?>
                                    <tr >
                                        <td align="left">{{ $snorow }}</td>
                                        <td><?php if (isset($propro->itemname)){ echo sub_str($propro->itemname,20); } ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php
                                $tid=1;  foreach ($proprogetdatarra as $item){
                                if(!empty($item->id)){
                                $totalamt = $item->quantity*$item->finalrate;
                                $invoicepaidboq = \App\InvoicePaymentsboq::where('invoice_boq_id',$item->id)->sum('amount');

                                ?>
                                <tr>
                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td></td>
                                    <td>{{ ucfirst($item->cost_item_name) }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $item->finalrate }}</td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $totalamt }}</td>
                                    <td> {!! htmlentities($invoice->currency_symbol)  !!}{{ $invoicepaidboq }} </td>
                                    <td>{!! htmlentities($invoice->currency_symbol)  !!}{{ $totalamt-$invoicepaidboq }}</td>
                                </tr>
                                <?php
                                $tid++;  }
                         }

                                }
                                 $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();
                                 $boqarray = array();
                                $boqarray['invoice'] = $invoice;
                                $boqarray['proproget'] = $proprogetlevel1;
                                $boqarray['projectid'] = $projectid;
                                $boqarray['subprojectid'] = $subprojectid;
                                $boqarray['snorow'] = $snorow;
                                $boqarray['level'] = $newlevel;
                                 echo boqhtml($boqarray);

                                } }
                                $invoice = \App\Invoice::where('id',$invoiceid)->first();
                                $project = $invoice->project_id ?: 0;
                                $subproject = $invoice->subproject_id ?: 0;
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$project)->where('title',$subproject)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['invoice'] = $invoice;
                                $boqarray['proproget'] = $proproget;
                                $boqarray['projectid'] = $project;
                                $boqarray['subprojectid'] = $subproject;
                                $boqarray['snorow'] = 0;
                                $boqarray['level'] = 0;
                                echo  boqhtml($boqarray);
                                ?>