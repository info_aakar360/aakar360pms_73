@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang("app.menu.home")</a></li>
                <li><a href="{{ route('admin.all-invoices.index') }}">@lang("app.menu.invoices")</a></li>
                <li class="active">@lang('modules.invoices.editrecordpayment')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    .ribbon-wrapper {
        background: #ffffff !important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="white-box bg-inverse">
                <h3 class="box-title text-white">@lang('modules.payments.totalAmount')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right"><span class="counter text-white">{{ $invoice->currency_symbol}} {{ $invoice->total }}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="white-box bg-success">
                <h3 class="box-title text-white">@lang('modules.payments.totalPaid')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right">
                        <span class="counter text-white">
                            {{ $invoice->currency_symbol.' '.$invoice->amountPaid() }}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="white-box bg-danger">
                <h3 class="box-title text-white">@lang('modules.payments.totalDue')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right">
                        <span class="counter text-white">
                            {{ $invoice->currency_symbol.' '.$invoice->amountDue() }}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box ribbon-wrapper">
                <div class="row">
                <div class="col-md-12">
                    <h3>@lang('modules.invoices.editrecordpayment')</h3>
                    <p>Invoice:  {{ $invoice->unique_id }}</p>
                </div>
                <div class="col-md-12">
                    <form method="post" action="" autocomplete="off" id="storePayments">
                        <div class="row">
                        <div class="col-md-3">
                        <div class="form-group">
                            <label>Payment date</label>
                            <input type="text" class="form-control" required id="payment_date" name="payment_date" value="@if(!empty($invoicepayment->payment_date)) {{ date('d-m-Y',strtotime($invoicepayment->payment_date)) }} @else {{ date('d-m-Y') }} @endif">
                        </div>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                            <label>Payment mode</label>
                            <select class="form-control" name="paymentmode">
                                <option value="">Select Mode</option>
                                <option value="cash" @if($invoicepayment->payment_mode=='cash') selected @endif>Cash</option>
                                <option value="bank" @if($invoicepayment->payment_mode=='bank') selected @endif>Bank</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label>Note</label>
                            <textarea class="form-control" required name="note" >{{ $invoicepayment->note }}</textarea>
                        </div>
                        </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <strong>How Much do you want to invoice</strong>
                                </div>
                                <div class="col-md-3">
                                    <input type="radio" name="percenttype" class="invoicepercenttype" value="all"  @if($invoicepayment->percenttype=='all') checked @endif >  @lang('modules.invoices.remainingalltasks') </div>
                                <div class="col-md-3">
                                    <input type="radio" name="percenttype"  class="invoicepercenttype" value="percent" @if($invoicepayment->percenttype=='percent') checked @endif > <input  type="text" onkeyup="boqinfo();" placeholder="%" min="0" max="{{ $paidpercent }}" value="{{ $invoicepayment->percentvalue }}" name="percentvalue" class="invoicepercent" > @lang('modules.invoices.ofalltasks')
                                </div>
                                <div class="col-md-3">
                                    <input type="radio" name="percenttype"  class="invoicepercenttype"  value="custom" @if($invoicepayment->percenttype=='custom') checked @endif > @lang('modules.invoices.customalltasks')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <hr />
                            <div class="col-md-12">
                                <table  class="table table2 table-bordered default footable-loaded footable">
                                    <thead>
                                    <tr>
                                        <th>@lang('app.sno')</th>
                                        <th>@lang('app.activity') / @lang('app.task')</th>
                                        <th>@lang('app.total')</th>
                                        <th>Paid To Date(₹)</th>
                                        <th>@lang('app.remaining')</th>
                                        <th>This Payment %</th>
                                        <th>This Payment ₹</th>
                                    </tr>
                                    </thead>
                                    <tbody id="invoiceboq">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row m-t-5 font-bold">
                            <div class="col-md-offset-9 col-md-1 col-xs-6 text-right p-t-10" >@lang('modules.invoices.total')</div>

                            <p class="form-control-static col-xs-6 col-md-2" >
                                <span class="total">0.00</span>
                            </p>


                            <input type="hidden" class="total-field" name="total" value="0">
                        </div>
                        <div class="form-actions" style="margin-top: 70px">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="{{ route('admin.all-invoices.show',$invoice->id) }}" class="btn btn-danger" > Go Back</a>
                                    <input type="hidden" value="{{ $invoice->id }}" name="invoiceid">
                                    <input type="hidden" value="{{ $invoicepayment->id }}" name="invoicepaymentid">
                                    {{ csrf_field() }}
                                    <button type="button" id="record-payment" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(".invoicepercent").keyup(function () {
        $(this).on('change', function(){
            var _self = this,
                v = parseFloat(_self.value),
                min = parseFloat(_self.min),
                max = parseFloat(_self.max);
            if (v >= min && v <= max){
                _self.value = v;
            }
            else {
                _self.value = v < min ? min : max;
            }
        });
    });

    jQuery('#payment_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    $(".invoicepercenttype").change(function () {
        boqinfo();
    });
     boqinfo();
    function boqinfo() {
        var percenttype = $(".invoicepercenttype:checked").val();
        var percentvalue = parseInt($(".invoicepercent").val());
        var invoiceid = '{{ $invoice->id }}';
        var invoicepaymentid = '{{ $invoicepayment->id }}';
        if(percentvalue>100){
            percentvalue = 100;
        }
        if(invoiceid){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.all-invoices.invoice-task-loop') }}",
                data: {'_token': token,'percenttype': percenttype,'percentvalue': percentvalue,'invoiceid': invoiceid,'invoicepaymentid': invoicepaymentid},
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function(data){
                    $(".preloader-small").hide();
                    var totalamt = 0;
                    $("#invoiceboq").html("");
                    $("#invoiceboq").html(data);
                    $(".invrowamt").each(function () {
                        var price = $(this).val();
                        totalamt += parseInt(price);
                    });
                    $(".total").html("₹"+totalamt.toFixed(2));
                    $(".total-field").val(totalamt);
                }
            });
        }
    }
    $("body").on('change',".calinvrow",function(){
        var calinvpercent = $(this).val();
        var type = $(this).data('type');
        var caltotalamt = $(this).data('totalamt');
        var caltaskid = $(this).data('taskid');
        if(type=='percent'){
            var percent = Math.round(((calinvpercent / 100) * caltotalamt));
            $(".invrowamt"+caltaskid).val(percent);
        }
        if(type=='amount'){
            var percent = Math.round(((calinvpercent / caltotalamt) * 100));
            $(".invrowpercent"+caltaskid).val(percent);
        }
        var totalamt = 0;
        $(".invrowamt").each(function () {
            var price = $(this).val();
            totalamt += parseInt(price);
        });
        $(".total").html("₹"+totalamt.toFixed(2));
        $(".total-field").val(totalamt);
    });
   $('#record-payment').click(function () {

        $.easyAjax({
            url: '{{route('admin.all-invoices.invoice-record-payment-post')}}',
            container: '#storePayments',
            type: "POST",
            data: $('#storePayments').serialize(),
            success: function(response){

            }
        })
    });

</script>
@endpush