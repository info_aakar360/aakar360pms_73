@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang("app.menu.home")</a></li>
                <li><a href="{{ route('admin.all-invoices.index') }}">@lang("app.menu.invoices")</a></li>
                <li class="active">@lang('app.invoice')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">

<style>
    .ribbon-wrapper {
        background: #ffffff !important;
    }
</style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="white-box bg-inverse">
                <h3 class="box-title text-white">@lang('modules.payments.totalAmount')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right"><span class="counter text-white">{{ $invoice->currency_symbol}} {{ $invoice->total }}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="white-box bg-success">
                <h3 class="box-title text-white">@lang('modules.payments.totalPaid')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right">
                        <span class="counter text-white">
                            {{ $invoice->currency_symbol.' '.$invoice->amountPaid() }}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="white-box bg-danger">
                <h3 class="box-title text-white">@lang('modules.payments.totalDue')</h3>
                <ul class="list-inline two-part">
                    <li><i class="fa fa-money text-white"></i></li>
                    <li class="text-right">
                        <span class="counter text-white">
                            {{ $invoice->currency_symbol.' '.$invoice->amountDue() }}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                   <i class="fa fa-check"></i> {!! $message !!}
                </div>
                <?php Session::forget('success');?>
            @endif

            @if ($message = Session::get('error'))
                <div class="custom-alerts alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    {!! $message !!}
                </div>
                <?php Session::forget('error');?>
            @endif


            <div class="white-box printableArea ribbon-wrapper">

                {{--<button type="button" data-clipboard-text="{{ route('front.invoice', [md5($invoice->id)]) }}" class="btn btn-success pull-right m-r-10 btn-copy">
                    <i class="fa fa-copy"></i> 
                    <span id="copy_payment_text">
                        @lang('modules.invoices.copyPaymentLink')
                    </span>
                </button>--}}
                <div class="clearfix"></div>
                        <div class="ribbon-content ">
                        @if($invoice->status == 'paid')
                            <div class="ribbon ribbon-bookmark ribbon-success">@lang('modules.invoices.paid')</div>
                        @elseif($invoice->status == 'partial')
                            <div class="ribbon ribbon-bookmark ribbon-info">@lang('modules.invoices.partial')</div>
                        @elseif($invoice->status == 'review')
                            <div class="ribbon ribbon-bookmark ribbon-warning">@lang('modules.invoices.review')</div>
                        @else
                            <div class="ribbon ribbon-bookmark ribbon-danger">@lang('modules.invoices.unpaid')</div>
                        @endif
                        </div>
                    <h3><span class="pull-right"><b>@lang('app.invoice')</b>: {{ $invoice->unique_id }}</span> </h3>
                <div class="clearfix"></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="pull-left">
                                <address>
                                    <p> @lang('app.company'):</p>
                                    <h3><b class="text-danger">{{ ucwords($global->company_name) }}</b></h3>
                                    @if(!is_null($settings))
                                        <p class="text-muted m-l-5"> @lang('app.address'):<br/>{!! nl2br($global->address) !!}</p>
                                    @endif
                                    @if($invoiceSetting->show_gst == 'yes' && !is_null($invoiceSetting->gst_number))
                                        <p class="text-muted m-l-5"><b>@lang('app.gstIn')
                                                :</b>{{ $invoiceSetting->gst_number }}</p>
                                    @endif
                                </address>
                            </div>
                            <div class="pull-right text-right">
                                <address>
                                   @if(!empty($invoice->client_id))
                                        <h3>To,</h3>
                                        <h4 class="font-bold">{{ ucwords(get_users_client_name($invoice->client_id)) }}</h4>

                                            {{--<p class="text-muted m-l-30">{!! nl2br($invoice->project->client->address) !!}</p>
                                            @if($invoiceSetting->show_gst == 'yes' && !is_null($invoice->project->client->client_details->gst_number))
                                                <p class="m-t-5"><b>@lang('app.gstIn')
                                                        :</b>  {{ $invoice->project->client->client_details->gst_number }}
                                                </p>
                                            @endif--}}
                                    @endif
                                    <p class="m-t-30"><b>@lang('app.invoice') @lang('app.date') :</b> <i class="fa fa-calendar"></i> {{ $invoice->issue_date->format($global->date_format) }}</p>

                                    <p><b>@lang('app.dueDate') :</b> <i class="fa fa-calendar"></i> {{ $invoice->due_date->format($global->date_format) }} </p>
                                    @if($invoice->recurring == 'yes')
                                        <p><b class="text-danger">@lang('modules.invoices.billingFrequency') : </b> {{ $invoice->billing_interval . ' '. ucfirst($invoice->billing_frequency) }} ({{ ucfirst($invoice->billing_cycle) }} cycles)</p>
                                    @endif
                                </address>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="m-t-40 table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">@lang("app.sno")</th>
                                        <th>@lang("app.activity")</th>
                                        <th>@lang("app.task")</th>
                                        <th>@lang("app.quantity")</th>
                                        <th>@lang("app.rate")</th>
                                        <th>@lang("app.amount")</th>
                                        <th>@lang("modules.invoices.paidtodate")</th>
                                        <th>@lang("modules.invoices.outstanding")</th>
                                    </tr>
                                    </thead>
                                    <tbody  id="invoiceboq">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pull-right m-t-30 text-right">

                                <h3><b>@lang("modules.invoices.totalpaid")
                                        :</b> {!! htmlentities($invoice->currency_symbol)  !!}{{ !empty($invoicepaidamount) ? $invoicepaidamount : 0  }}
                                </h3>
                               {{-- <p>@lang("modules.invoices.subTotal")
                                    : {!! htmlentities($invoice->currency_symbol)  !!}{{ $invoice->sub_total }}</p>

                                <p>@lang("modules.invoices.discount")   : {!! htmlentities($invoice->currency_symbol)  !!}{{ $discount }} </p>
                                <hr>
                                <hr>
                                @if ($invoice->credit_notes()->count() > 0)
                                    <p>
                                        @lang('modules.invoices.appliedCredits'): {{ $invoice->currency_symbol.''.$invoice->appliedCredits() }}
                                    </p>
                                @endif
                                <p>
                                    @lang('modules.invoices.amountPaid'): {{ $invoice->currency_symbol.''.$invoice->getPaidAmount() }}
                                </p>
                                <p class="@if ($invoice->amountDue() > 0) text-danger @endif">
                                    @lang('modules.invoices.amountDue'): {{ $invoice->currency_symbol.''.$invoice->amountDue() }}
                                </p>--}}
                            </div>

                            @if(!is_null($invoice->note))
                                <div class="col-md-12">
                                    <p><strong>@lang('app.note')</strong>: {{ $invoice->note }}</p>
                                </div>
                            @endif

                            <div class="text-right">

                                {{--<a class="btn btn-default btn-outline"
                                   href="{{ route('admin.all-invoices.download', $invoice->id) }}"> <span><i class="fa fa-file-pdf-o"></i> @lang('modules.invoices.downloadPdf')</span> </a>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box ribbon-wrapper">

                @if($invoice->amountDue()>0)
                     <a href="{{ route('admin.all-invoices.record-payments',$invoice->id) }}"  class="btn btn-primary"><i class="fa fa-plus"></i>  @lang('app.menu.recordpayments')</a>
                @endif
                 <div class="row">
                <div class="col-md-12">
                    <h4>Payment Records</h4>
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">@lang("app.sno")</th>
                                <th>@lang("modules.invoices.paymentdate")</th>
                                <th>@lang("modules.invoices.paymentmode")</th>
                                <th>@lang("modules.invoices.notes")</th>
                                <th>@lang("modules.invoices.amount")</th>
                                <th>@lang("app.action")</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0; ?>
                            @foreach($invoicepaymentsarray as $item)
                                <tr id="tdrow{{ $item->id }}">
                                    <td>{{ ++$count }}</td>
                                    <td>{{ $item->payment_date }}</td>
                                    <td>{{ $item->payment_mode }}</td>
                                    <td>{{ substr($item->note,0,100) }}</td>
                                    <td> {!! htmlentities($invoice->currency_symbol)  !!}{{ $item->totalamount }} </td>
                                    <td><a href="{{ route('admin.all-invoices.record-payments-edit',$item->id) }}"><i class="fa fa-pencil"></i></a> | <a href="javascript:void(0);" class="sa-params" data-invoice-id="{{ $item->id }}"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-lg in" id="paymentDetail" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
         <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-lg in" id="appliedCredits" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
    
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    @lang('app.loading')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">@lang('app.close')</button>
                    <button type="button" class="btn blue">@lang('app.save') @lang('app.changes')</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}


@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/clipboard/clipboard.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script>

    var clipboard = new ClipboardJS('.btn-copy');

    clipboard.on('success', function(e) {
        var copied = "<?php echo __("app.copied") ?>";
        // $('#copy_payment_text').html(copied);
        $.toast({
            heading: 'Success',
            text: copied,
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500
        });
    });

    function showAppliedCredits(url) {
        $.ajaxModal('#appliedCredits', url);
    }

    function deleteAppliedCredit(invoice_id, id) {
        let url = '{{ route('admin.all-invoices.delete-applied-credit', [':id']) }}';
        url = url.replace(':id', id);

        $.easyAjax({
            url: url,
            type: 'POST',
            data: { invoice_id: invoice_id, _token: '{{csrf_token()}}'},
            success: function (response) {
                $('#appliedCredits .modal-content').html(response.view);
                $('#appliedCredits').on('hide.bs.modal', function (e) {
                    location.reload();
                })
            }
        })
    }

    $(function () {
        var table = $('#invoices-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.all-invoices.create') }}',
            deferRender: true,
            "order": [[0, "desc"]],
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function (oSettings) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'project_name', name: 'projects.project_name'},
                {data: 'invoice_number', name: 'invoice_number'},
                {data: 'currency_symbol', name: 'currencies.currency_symbol'},
                {data: 'total', name: 'total'},
                {data: 'issue_date', name: 'issue_date'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

    });

    boqinfo();
    function boqinfo() {
        var percenttype = $(".invoicepercenttype:checked").val();
        var percentvalue = parseInt($(".invoicepercent").val());
        var invoiceid = '{{ $invoice->id }}';
        if(percentvalue>100){
            percentvalue = 100;
        }
        if(invoiceid){
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.all-invoices.invoice-loop') }}",
                data: {'_token': token,'invoiceid': invoiceid},
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function(data){
                    $(".preloader-small").hide();
                    var totalamt = 0;
                    $("#invoiceboq").html("");
                    $("#invoiceboq").html(data);
                    $(".invrowamt").each(function () {
                        var price = $(this).val();
                        totalamt += parseInt(price);
                    });
                    $(".total").html("₹"+totalamt.toFixed(2));
                    $(".total-field").val(totalamt);
                }
            });
        }
    }
    // Show Payment detail modal
    function showPayments() {
        var url = '{{route('admin.all-invoices.payment-detail', $invoice->id)}}';
        $.ajaxModal('#paymentDetail', url);
    }
    $('body').on('click', '.sa-params', function(){
        var id = $(this).data('invoice-id');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted invoice!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                var url = "{{ route('admin.all-invoices.paymentsdestroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        console.log(response.status);
                        if (response.status == "success") {
                            $("#tdrow"+tdrow).remove();
                            $.unblockUI();
                        }
                    }
                });
            }
        });
    });
</script>
@endpush