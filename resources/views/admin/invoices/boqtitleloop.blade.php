                                <?php
                                 /* Level 0 category */
                                function boqhtml($boqarray){
                                $invoiceid = $boqarray['invoiceid'];
                                $proproget = $boqarray['proproget'];
                                $id = $boqarray['id'];
                                $title = $boqarray['title'];
                                $snorow = $boqarray['snorow'];
                                $level = $boqarray['level'];

                                foreach ($proproget as $propro){

                                $level = (int)$propro->level;
                                $newlevel = $level+1;
                                $parent = $propro->id;

                                if((int)$propro->level==0){
                                    $snorow = $snorow+1;
                                }else{
                                    $snorow = $snorow.'.'.$propro->level;
                                }
                                ?>
                                    <tr >
                                        <td align="left">{{ $snorow }}</td>
                                        <td><strong><?php if (isset($propro->itemname)){ echo get_dots_by_level($propro->level).''.sub_str($propro->itemname,20); } ?></strong></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php

                                 $proprogetdatarra = \App\ProjectCostItemsProduct::where('project_id',$id)->where('title',$title)->where('position_id',$propro->id)->orderBy('inc','asc')->get();
                             $tid = 1;  foreach ($proprogetdatarra as $proprogetdat){
                                if(!empty($proprogetdat->id)){
                                $rate = $proprogetdat->finalrate;
                                     $totalamt = $proprogetdat->finalrate*$proprogetdat->qty;
                                    $marginamt = 0;
                                    $adjustamt = 0;
                                    $invoicetodatepercent = 0;
                                    $invoicetodateamt = 0;
                                    $remaining = $totalamt;
                                    $thisinvoicepercent = 0;
                                    $invamount = 0;
                                $invoicetodatequanity = 0;
                                $previnvoice = \App\Invoiceboq::select([\Illuminate\Support\Facades\DB::raw("SUM(percent) as invpercent"),
                                    \Illuminate\Support\Facades\DB::raw("SUM(amount) as invamount"), \Illuminate\Support\Facades\DB::raw("SUM(quantity) as invquantity"),
                                ])->where('product_id',$proprogetdat->id);
                                if(!empty($invoiceid)){
                                    $previnvoice = $previnvoice->where('invoice_id','<',$invoiceid);
                                }
                                $previnvoice = $previnvoice->first();

                                if(!empty($previnvoice->invpercent)&&!empty($previnvoice->invamount)){
                                    $invoicetodatepercent = $previnvoice->invpercent;
                                    $invoicetodateamt = $previnvoice->invamount;
                                    $invoicetodatequanity = $previnvoice->invquantity;
                                    $remaining = $remaining-$invoicetodateamt;
                                }
                                $invquantity = 0;
                                 if(!empty($invoiceid)){
                                    $invoiceboqedit = \App\Invoiceboq::where('invoice_id',$invoiceid)->where('product_id',$proprogetdat->id)->first();
                                    if(!empty($invoiceboqedit->quantity)){
                                        $invquantity = $previnvoice->invquantity+$invoiceboqedit->quantity;
                                        $invamount = $previnvoice->invamount+$invoiceboqedit->amount;
                                        $remaining = $remaining-$invamount;
                                    }
                                }
                                ?>
                                <tr data-invoicetodatequat="{{ $invoicetodatequanity }}"   data-rate="{{ $rate }}" data-qty="{{ $proprogetdat->qty }}" data-totalamt="{{ $totalamt }}"  data-invoicetodateamt="{{ $invoicetodateamt }}"  class="collpse level1 catrow{{ $proprogetdat->category }}" id="costitem{{ $proprogetdat->id }}">

                                    <td align="left">{{ $snorow.'.'.$tid }}</td>
                                    <td align="left"><input type="checkbox" name="taskid[]" @if(!empty($invoiceboqedit)&&$invoiceboqedit->product_id==$proprogetdat->id) checked @endif value="{{ $proprogetdat->id }}"> {{ sub_str(get_cost_name($proprogetdat->cost_items_id),20) }}</td>
                                    <td>{{ get_unit_name($proprogetdat->unit) }}</td>
                                    <td align="right">{{ $proprogetdat->qty }}</td>
                                    <td align="right">₹{{ $rate }}</td>
                                    <!-- Percentage -->
                                    <td>{{ $invoicetodatepercent }}%</td>
                                    <td  ><input type="text" name="percentage[{{ $proprogetdat->id }}]" class="cell-inp numbervalid  percenttotal{{ $proprogetdat->id }}" value="{{  !empty($invoiceboqedit) ? $invoiceboqedit->percent : '' }}" /></td>

                                    <!-- Quantity -->
                                    <td>{{ $invoicetodatequanity }}</td>
                                    <td style="position:relative;"><a href="javascript:;" class="open-measurement-sheet" title="Open Measurement Sheet" data-toggle="tooltip" data-id="{{ $proprogetdat->id }}" data-invoiceid="{{  !empty($invoiceid) ? $invoiceid : '' }}"><i class="fa fa-ellipsis-v"></i></a>
                                        <input type="text" name="quantity[{{ $proprogetdat->id }}]" data-totalqty="{{ $proprogetdat->qty }}" class="cell-inp numbervalid qtysheet{{ $proprogetdat->id }}" onchange="calcTotal({{ $proprogetdat->id }});" value="{{ !empty($invoiceboqedit) ? $invoiceboqedit->quantity : 0 }}" />
                                    </td>
                                    <td class="qtytotal{{ $proprogetdat->id }}">{{ !empty($invquantity) ?  $invquantity : '' }}</td>
                                    <!-- Amount -->
                                    <td align="right">₹{{ $invoicetodateamt }}</td>
                                    <td align="right" class="invrowamt prototal{{ $proprogetdat->id }}">{{  !empty($invoiceboqedit) ? '₹'.numberformat($invoiceboqedit->amount) : '' }}</td>
                                   {{-- <td class="amttotal{{ $proprogetdat->id }}">{{  !empty($invamount) ? '₹'.$invamount : 0 }}</td>--}}
                                   </tr>
                                <?php
                                $tid++; } }
                                $proprogetlevel1 = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

                                $boqarray = array();
                                $boqarray['invoiceid'] = $invoiceid;
                                $boqarray['proproget'] = $proprogetlevel1;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = (int)$title ?: 0;
                                $boqarray['snorow'] = $snorow;
                                $boqarray['level'] = $newlevel;
                                 echo boqhtml($boqarray);

                                }
                                }
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$id)->where('title',$title)->where('position','row')->where('level','0')->orderBy('inc','asc')->get();
                                  $boqarray = array();
                                $boqarray['invoiceid'] = $invoiceid;
                                $boqarray['proproget'] = $proproget;
                                $boqarray['id'] = $id;
                                $boqarray['title'] = $title ?: 0;
                                $boqarray['parent'] = 0;
                                $boqarray['level'] =  0;
                                $boqarray['snorow'] = 0;

                                 echo  boqhtml($boqarray);
                                ?>