@push('head-script')

@endpush

@section('content')
    <style>
        .RowResource{
            width: 255px;
        }
        .RowRate{
            width: 90px;
        }
        .RowUnitx{
            width: 90px;
        }
        .RowFormula{
            width: 255px;
        }
        .RowResult{
            width: 100px;
        }
        .close-rate-sheet{
            font-size: 16px;
        }
        .rate-formula-sheet .cell-inp[data-col="1"],.formula-sheet .cell-inp[data-col="1"]{
            text-align: right;
        }
        .formula-sheet thead th, .rate-formula-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;

        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .cubeicon {
            font-size: 16px;
        }

    </style>
    <form method="post" autocomplete="off" id="rate-sheet-form">
        @csrf

        <div class="row pdl10">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="padding: 5px 15px;">
                <a href="javascript:;" class="close-rate-sheet pull-right"><i class="fa fa-times"></i> </a>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <div class="rate-formula-sheet">
            <div class="row pdl10">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <h4><strong>Task :</strong> {{ get_cost_name($product->cost_items_id) }}</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <p><strong>Date :</strong> {{ !empty($product->start_date) ? date('d M Y',strtotime($product->start_date)) : '' }} {{ !empty($product->due_date) ? ' - '.date('d M Y',strtotime($product->due_date)) : '' }}</p>
                     </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <p><strong>Unit:</strong> {{ get_unit_name($product->unit) }}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <p>{{ $product->description }}</p>
                    </div>
                </div>
            <div class="table-wrapper">
                <table class="table table-bordered  table-center">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Nos</th>
                            <?php if(!empty($units->measuretype)){
                                switch ($units->measuretype){
                                    case 'area':  ?>
                                    <th>Length</th>
                                    <th>Breadth</th>
                                    <?php
                                    break;
                            case 'volume': ?>
                            <th>Length</th>
                            <th>Breadth</th>
                            <th>Height</th>
                            <?php
                            break;
                            case 'count': ?>
                            <th>Count</th>
                                <?php
                                     break;
                                }
                            } ?>
                            <th>Qty</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tablerowmeasure">
                    <?php
                    $res_count = !empty($measurementsheets) ? count($measurementsheets) : 0;
                    $totalRow = 35 - $res_count;
                    if($totalRow <= 1){
                        $totalRow = 2;
                    }
                    $row = 1;
                    $totalamt = 0;
                    ?>
                    @if(!empty($measurementsheets))
                        @foreach($measurementsheets as $costitemresource)
                            <?php
                            $totalamt += $costitemresource->qty;
                            ?>
                            <tr class="resource_list" data-row="{{$row}}" data-id="{{ $costitemresource->id }}">
                                <input name="ratesheetid[]" type="hidden" data-row="{{ $row }}" class="ratesheetid" value="{{ $costitemresource->id }}">
                                <td data-col="0"><input type="text" name="name[]" data-col="0" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->name }}"  ></td>
                                <td data-col="1"><input type="text" name="nos[]" data-col="1" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $costitemresource->nos }}" ></td>

                                <?php if(!empty($units->measuretype)){
                                $types = !empty($costitemresource->types) ? explode(',',$costitemresource->types) : array();
                                switch ($units->measuretype){
                                    case 'area':  ?>
                                    <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[0]) ? $types[0] : '' }}" ></td>
                                    <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[1]) ? $types[1] : '' }}" ></td>
                                    <?php
                                    break;
                                    case 'volume': ?>
                                    <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[0]) ? $types[0] : '' }}" ></td>
                                    <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[1]) ? $types[1] : '' }}" ></td>
                                    <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[2]) ? $types[2] : '' }}" ></td>
                                    <?php
                                        break;
                                        case 'count': ?>
                                        <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ !empty($types[0]) ? $types[0] : '' }}" ></td>
                                    <?php
                                    break;
                                    }
                                } ?>
                                <td data-col="3"><input type="text" name="qty[]" data-col="3" data-row="{{ $row }}" class="cell-inp cell-new qtyrow" value="{{ $costitemresource->qty }}"/></td>
                                <td data-col="4"><a href="javascript:void(0);" data-row="{{ $row }}" class="removeresourcerow"><i class="fa fa-trash"></i></a></td>
                             </tr>
                            <?php $row++ ?>
                        @endforeach
                    @endif
                    @for($i=0;$i<$totalRow;$i++)
                        <tr data-row="{{$row}}" class="resource_list">
                            <input name="ratesheetid[]" type="hidden" data-row="{{ $row }}" class="ratesheetid">
                            <td data-col="0"><input type="text" name="name[]"  data-col="0" data-row="{{$row}}" class="cell-inp cell-new" /></td>
                            <td data-col="1"><input type="text" name="nos[]"  data-col="1" data-row="{{$row}}" class="cell-inp cell-new" /></td>
                            <?php if(!empty($units->measuretype)){
                            $types = !empty($costitemresource->types) ? explode(',',$costitemresource->types) : array();
                            switch ($units->measuretype){
                            case 'area':  ?>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"  ></td>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" ></td>
                            <?php
                            break;
                            case 'volume': ?>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"  ></td>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" ></td>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"  ></td>
                            <?php
                            break;
                            case 'count': ?>
                            <td data-col="2"><input type="text" name="types[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new"  ></td>
                            <?php
                            break;
                            }
                            } ?>
                            <td data-col="3"><input type="text" name="qty[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new qtyrow"  /></td>
                            <td data-col="4"><a href="javascript:void(0);" data-row="{{$row}}" class="removeresourcerow"></a></td>
                        </tr>
                        <?php
                        $row++;
                        ?>
                    @endfor
                    </tbody>
                </table>
            </div>
            </div>
            </div>
        </div>
    </form>

    <script>
        "use strict";
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            var cat = inp.data('category');
            var row = inp.data('row');
            var resourceid = inp.attr('data-resourceid');
            $(".combosheetbutton").attr('disabled','disabled');
            $('.combosheetbutton').removeAttr('data-resource');
            if(cat==1){
                $('.combosheetbutton').removeAttr('disabled');
                $('.combosheetbutton').attr('data-resource',resourceid);
            }
        });
        $(document).on('change', '.rate-formula-sheet input.cell-inp[data-col=0]', function (e) {
            var inp = $(this);
            var id = inp.val();
            var row = inp.data('row');
            var name = inp.val();
            var ratesheetid = $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val();
            var productid = '{{ $product->id }}';
            var invoiceid = '{{ $invoiceid }}';
            var token = '{{ csrf_token() }}';
                if(id != ''){
                    $.ajax({
                        url: '{{ route('admin.all-invoices.addqtytomeasuresheet') }}',
                        type: 'POST',
                        data: {'_token': token,'productid': productid,'invoiceid': invoiceid,'sheetid': ratesheetid,'name': name},
                        success: function(rData){
                            $('.rate-formula-sheet tr.resource_list[data-row='+row+']').attr('data-id',rData.id);
                            $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val(rData.id);
                            $('.rate-formula-sheet tr.resource_list input.qtyrow[data-row='+row+']').val(rData.qty);
                            $('.rate-formula-sheet .removeresourcerow[data-row='+row+']').html("<i class='fa fa-trash'></i>");
                            $('.qtysheet'+productid).val(rData.totalqty);
                            calcTotal(productid);
                        }
                    });
                    e.stopImmediatePropagation();
                    return false;
                }else{
                    $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
                    $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val('');
                }
        });
        $(document).on('change', '.rate-formula-sheet input.cell-inp[data-col=1]', function (e) {
            var inp = $(this);
            var id = inp.val();
            var row = inp.data('row');
            var nos = inp.val();
            var ratesheetid = $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val();
            var productid = '{{ $product->id }}';
            var invoiceid = '{{ $invoiceid }}';
            var token = '{{ csrf_token() }}';
            if(id != ''){
                $.ajax({
                    url: '{{ route('admin.all-invoices.addqtytomeasuresheet') }}',
                    type: 'POST',
                    data: {'_token': token,'productid': productid,'invoiceid': invoiceid,'sheetid': ratesheetid,'nos': nos},
                    success: function(rData){
                        $('.rate-formula-sheet tr.resource_list[data-row='+row+']').attr('data-id',rData.id);
                        $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val(rData.id);
                        $('.rate-formula-sheet tr.resource_list input.qtyrow[data-row='+row+']').val(rData.qty);
                        $('.rate-formula-sheet .removeresourcerow[data-row='+row+']').html("<i class='fa fa-trash'></i>");
                        $('.qtysheet'+productid).val(rData.totalqty);
                        calcTotal(productid);
                    }
                });
                e.stopImmediatePropagation();
                return false;
            }else{
                $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
                $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val('');
            }
        });

        $(document).on('change', '.rate-formula-sheet input.cell-inp[data-col=2]', function (e) {
            var inp = $(this);
            var id = inp.val();
            var row = inp.data('row');
            var types = [];
            $('.rate-formula-sheet tr.resource_list[data-row='+row+'] input.cell-inp[data-col=2]').each(function() {
                var singqty = $(this).val();
                if(singqty){
                    types.push(singqty);
                }
            });
            var ratesheetid = $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val();
            var productid = '{{ $product->id }}';
            var invoiceid = '{{ $invoiceid }}';
            var token = '{{ csrf_token() }}';
            if(id != ''){
                $.ajax({
                    url: '{{ route('admin.all-invoices.addqtytomeasuresheet') }}',
                    type: 'POST',
                    data: {'_token': token,'productid': productid,'invoiceid': invoiceid,'sheetid': ratesheetid,'types': types},
                    success: function(rData){
                        $('.rate-formula-sheet tr.resource_list[data-row='+row+']').attr('data-id',rData.id);
                        $('.rate-formula-sheet input.ratesheetid[data-row='+row+']').val(rData.id);
                        $('.rate-formula-sheet tr.resource_list input.qtyrow[data-row='+row+']').val(rData.qty);
                        $('.rate-formula-sheet .removeresourcerow[data-row='+row+']').html("<i class='fa fa-trash'></i>");
                        $('.qtysheet'+productid).val(rData.totalqty);
                        calcTotal(productid);
                    }
                });
                e.stopImmediatePropagation();
                return false;
            }else{
                $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
                $('.rate-formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val('');
            }
        });

        $(document).on('click','.removeresourcerow',function (e) {
            var datarow = $(this).data('row');
            var prid = $(this).data('prid');
            var val = $(this).val();
            var token = '{{ csrf_token() }}';
            var forrow = $('.rate-formula-sheet tr.resource_list input[name="formula[]"][data-row='+datarow+']');
            var ratesheetid = $('.rate-formula-sheet input.ratesheetid[data-row='+datarow+']').val();
            var forrowval = forrow.val();
            $.ajax({
                url: '{{ route('admin.all-invoices.removeqtytomeasuresheet') }}',
                type: 'POST',
                data: {'_token': token,'sheetid': ratesheetid},
                success: function(rData){
                    $('.rate-formula-sheet tr.resource_list[data-row='+datarow+']').remove();
                    calcTotal(prid);
                }
            });
        });
        $('#resourceCreate').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ route('admin.resources.store') }}',
                container: '#resourceCreate',
                type: "POST",
                data: $('#resourceCreate').serialize(),
                success: function (rData) {
                    var response = JSON.parse(rData);
                    if(response.status == 'success'){
                        $("#comboModal").modal('hide');
                        var costitemlist = '';
                        costitemlist += '<option value="'+response.id+'" >'+response.name+'</option>';
                        $("#costitemlist").append(costitemlist);
                    }
                }
            })
        });
        $('#rate-sheet-submit').click(function (e) {
            $('#rate-sheet-form .cell-inp').removeAttr('disabled');
            e.preventDefault();
            $.ajax({
                url: '{{ route('admin.projects.ratesheetstore') }}',
                container: '#rate-sheet-form',
                type: "POST",
                data: $('#rate-sheet-form').serialize(),
                beforeSend: function(){
                    var html = '<div class="loaderx">' +
                        '                        <div class="cssload-speeding-wheel"></div>' +
                        '                    </div>';
                    $('.rate-formula-sheet').html(html).show();
                },
                success: function (rData) {
                    var response = JSON.parse(rData);
                    if(response.status == 'success'){
                        $('.ratevalue'+response.costitem).val(response.result);
                        $('.rate-formula-sheet').html("").hide();
                        calmarkup(response.costitem);
                    }
                }
            })
        });

    </script>
@endsection

@push('footer-script')

@endpush