@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="sttabs tabs-style-line col-md-12">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li><a href="{{route('admin.employees.edit', [$userDetail->id])}}"><span>@lang('modules.employees.general')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.employee_job_profile', [$userDetail->id])}}"><span>@lang('modules.employees.job')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{route('admin.employees.employee_profile_documents', [$userDetail->id])}}"><span>@lang('modules.employees.documents')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.team', [$userDetail->id])}}"><span>@lang('modules.employees.team')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.education', [$userDetail->id])}}"><span>@lang('modules.employees.education')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.family', [$userDetail->id])}}"><span>@lang('modules.employees.family')</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" style="float: right">
                                                <a href="javascript:;" class="btn btn-primary" id="department-setting"><i class="fa fa-plus-circle"></i> @lang('modules.profile.uploadDocument') </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach($docs as $doc)
                                                <tr id="cat-{{ $doc->id }}">
                                                    <td>{{ get_doc_type($doc->doc_type) }}</td>
                                                    <td>
                                                        @if(config('filesystems.default') == 'local')
                                                            <a href="{{ asset_url('documents/'.$doc->filename) }}" class="btn btn-success btn-circle"
                                                               data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        @elseif(config('filesystems.default') == 's3')
                                                            <a target="_blank" href="{{ $url.'documents/'.$doc->filename }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle"><i
                                                                        class="fa fa-eye"></i></a>
                                                        @elseif(config('filesystems.default') == 'google')
                                                            <a target="_blank" href="{{ $doc->filename }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle"><i
                                                                        class="fa fa-eye"></i></a>
                                                        @elseif(config('filesystems.default') == 'dropbox')
                                                            <a target="_blank" href="{{ $doc->filename }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn1 btn-info btn-circle"><i
                                                                        class="fa fa-eye"></i></a>
                                                        @endif
                                                        <a href="javascript:;" data-user-id="{{ $doc->id }}" class="btn btn-primary btn-circle edit-doc"
                                                           data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                        <a href="javascript:;" class="btn btn-danger btn-circle delete-category"
                                                           data-toggle="tooltip" data-user-id="{{ $doc->id }}" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="departmentModel" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script>
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.employees.store')}}',
                container: '#createEmployee',
                type: "POST",
                redirect: true,
                file: (document.getElementById("image").files.length == 0) ? false : true,
                data: $('#createEmployee').serialize()
            })
        });

        $('#department-setting').on('click', function (event) {
            event.preventDefault();
            var id = '{{ $userDetail->id }}';
            $('#modelHeading').html("Upload Document");
            var url = '{{ route('admin.employees.addId', ':id')}}';
            url = url.replace(':id', id);
            $.ajaxModal('#departmentModel', url);
        });

        $('.edit-doc').on('click', function (event) {
            event.preventDefault();
            var id = $(this).data('user-id');
            $('#modelHeading').html("Upload Document");
            var url = '{{ route('admin.employees.editId', ':id')}}';
            url = url.replace(':id', id);
            $.ajaxModal('#departmentModel', url);
        });

        $('.delete-category').click(function () {
            var id = $(this).data('user-id');
            var url = "{{ route('admin.employees.deleteId',':id') }}";
            url = url.replace(':id', id);
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        $.unblockUI();
                        $('#cat-'+id).fadeOut();
                        var options = [];
                        var rData = [];
                        rData = response.data;
                        $.each(rData, function( index, value ) {
                            var selectData = '';
                            selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                            options.push(selectData);
                        });

                        $('#category_id').html(options);
                        $('#category_id').selectpicker('refresh');
                        window.location.reload();
                    }
                }
            });
        });
    </script>
@endpush