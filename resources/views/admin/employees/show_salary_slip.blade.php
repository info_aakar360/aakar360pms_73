@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="salary-slip" >
                    <table class="empDetail">
                        <tr height="100px" style='background-color: #4F5467'>
                            <td colspan='4'>
                                <img height="30px" width="100px"  src='{{ $global->logo() }}' /></td>
                            <td colspan='4' class="companyName">Aakar360</td>
                        </tr>
                        <tr>
                            <th>
                                Name
                            </th>
                            <td>
                                Admin
                            </td>
                            <td></td>
                            <th>
                                Bank Code
                            </th>
                            <td>
                                ABC123
                            </td>
                            <td></td>
                            <th>
                                Branch Name
                            </th>
                            <td>
                                ABC123
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Employee Id
                            </th>
                            <td>
                                XXXXXXXXXXX
                            </td>
                            <td></td>
                            <th>
                                Bank Name
                            </th>
                            <td>
                                XXXXXXXXXXX
                            </td>
                            <td></td>
                            <th>
                                Payslip no.
                            </th>
                            <td>
                                XXXXXXXXXX
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Cost Centre
                            </th>
                            <td>
                                XXXXXXXXXXX
                            </td><td></td>
                            <th>
                                Bank Branch
                            </th>
                            <td>
                                XXXXXXXXXX
                            </td><td></td>
                            <th>
                                Pay Period
                            </th>
                            <td>
                                XXXXXXXXXXX
                            </td>
                        </tr>
                        <tr>
                            <th>
                                CC Description:
                            </th>
                            <td>
                                XXXXXXXXXXX
                            </td><td></td>
                            <th>
                                Bank A/C no.
                            </th>
                            <td>
                                XXXXXXXXXX
                            </td><td></td>
                            <th>
                                Personel Area
                            </th>
                            <td>
                                XXXXXXXXXX
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Grade:
                            </th>
                            <td>
                                18
                            </td><td></td>
                            <th>
                                Employee Group
                            </th>
                            <td>
                                Sales Manager
                            </td><td></td>
                            <th>
                                PAN No:
                            </th>
                            <td>
                                MOP72182E
                            </td>
                        </tr>
                        <tr class="myBackground">
                            <th colspan="2">
                                Payments
                            </th>
                            <th >
                                Particular
                            </th>
                            <th class="table-border-right">
                                Amount (Rs.)
                            </th>
                            <th colspan="2">
                                Deductions
                            </th>
                            <th >
                                Particular
                            </th>
                            <th >
                                Amount (Rs.)
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                Basic Salary
                            </th>
                            <td></td>
                            <td class="myAlign">
                                4935.00
                            </td>
                            <th colspan="2" >
                                Provident Fund
                            </th >
                            <td></td>

                            <td class="myAlign">
                                00.00
                            </td>
                        </tr >

                        <tr class="myBackground">
                            <th colspan="3">
                                Total Payments
                            </th>
                            <td class="myAlign">
                                10000
                            </td>
                            <th colspan="3" >
                                Total Deductions
                            </th >
                            <td class="myAlign">
                                1000
                            </td>
                        </tr >

                        <tr>
                            <td colspan="2">
                                Total Income
                            </td> <td></td>
                            <td class="myAlign">
                                00.00
                            </td>
                            <td colspan="4"></td>
                        </tr >
                        <tbody class="border-center">
                        <tr>
                            <th>
                                Attend/ Absence
                            </th>
                            <th>
                                Days in Month
                            </th>
                            <th>
                                Days Paid
                            </th>
                            <th>
                                Days Not Paid
                            </th>
                            <th>
                                Leave Position
                            </th>
                            <th>
                                Privilege Leave
                            </th>
                            <th>
                                Sick Leave
                            </th>
                            <th>
                                Casual Leave
                            </th>
                        </tr>
                        <tr>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td>Yearly Open Balance</td>
                            <td>0.0</td> <td>0.0</td>
                            <td>0.0</td>
                        </tr >
                        <tr>
                            <th >Current Month</th>
                            <td >31.0</td>
                            <td >31.0</td>
                            <td >31.0</td>
                            <td>Availed</td>
                            <td>0.0</td> <td>0.0</td>
                            <td>0.0</td>
                        </tr >
                        <tr>
                            <td colspan="4"></td>
                            <td>Closing Balance</td>
                            <td>0.0</td> <td>0.0</td>
                            <td>0.0</td>
                        </tr >
                        <tr>
                            <td colspan="4"> &nbsp; </td>
                            <td > </td>
                            <td > </td>
                            <td > </td>
                            <td > </td>
                        </tr >
                        <tr>
                            <td colspan="4"></td>
                            <td>Company Pool Leave Balance</td>
                            <td>1500</td>
                            <td ></td>
                            <td ></td>
                        </tr >
                        </tbody>
                    </table >

                </div >

            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

@endpush