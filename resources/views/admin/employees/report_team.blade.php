@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="sttabs tabs-style-line col-md-12">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li><a href="{{route('admin.employees.edit', [$userDetail->id])}}"><span>@lang('modules.employees.general')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.employee_job_profile', [$userDetail->id])}}"><span>@lang('modules.employees.job')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.employee_profile_documents', [$userDetail->id])}}"><span>@lang('modules.employees.documents')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{route('admin.employees.team', [$userDetail->id])}}"><span>@lang('modules.employees.team')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.education', [$userDetail->id])}}"><span>@lang('modules.employees.education')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.family', [$userDetail->id])}}"><span>@lang('modules.employees.family')</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'reportTeam','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="col-md-12">
                                <label>REPORTING MANAGER</label><br>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($employeeTeam) == 0)
                                        <tr>
                                            <td colspan="4">No Primary managers assigned.<br>
                                            No Secondary managers assigned.</td>
                                        </tr>
                                    @endif
                                    @foreach($employeeTeam as $et)
                                        <tr>
                                            <td>
                                                {{ get_user_name($et->team_id) }}
                                            </td>
                                            <td>
                                                {{ $et->type }}
                                            </td>
                                            <td>
                                                {{ get_department_name($et->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_designation_name($et->team_id) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <div id="insertBefore"></div>
                                <div class="clearfix">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                        Add <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<button type="submit" id="saveManager" class="btn btn-sm btn-info" style="margin-bottom: 20px; float: right;">--}}
                                        {{--Save <i class="fa fa-plus"></i>--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="saveManager" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'directReport','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="form-body">
                        <div class="col-md-12">
                            <label>DIRECT REPORT</label><br>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($employeeDirect as $ed)
                                        <tr>
                                            <td>
                                                {{ get_user_name($ed->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_department_name($ed->team_id) }}
                                            </td>
                                            <td>
                                                {{ get_designation_name($ed->team_id) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div id="directinsertBefore"></div>
                            <div class="clearfix">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" id="addButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                        Add <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                         <div class="form-actions">
                             <button type="submit" id="saveDirect" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                         </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
    </div>

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script>
        var $insertBefore = $('#insertBefore');
        var $i = 0;
        // Add More Inputs
        $('#plusButton').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-5"><input type="hidden" name="user_id" value="{{ $userDetail->id }}"><div class="form-group "><select name="employee['+$i+']" class="form-control" id="employee_id'+indexs+'"><option value=""> Select Employee</option></select></div></div>' +
            '<div class="col-md-5"style="margin-left:5px;"><div class="form-group"><select name="type['+$i+']" class="form-control" id="type['+$i+']"   ><option value=""> Select Type</option><option value="primary"> Primary</option><option value="secondary">Seondary</option></select></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);
            $('#employee_id'+indexs).html('{!! $employeelists !!}');
        });

        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        var $directinsertBefore = $('#directinsertBefore');
        var $i = 0;
        // Add More Inputs
        $('#addButton').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-10"><div class="form-group "><input type="hidden" name="user_id" value="{{ $userDetail->id }}"><select name="employee['+$i+']" class="form-control" id="employee_id'+indexs+'"><option value=""> Select Employee</option></select></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($directinsertBefore);
            $('#employee_id'+indexs).html('{!! $employeelists !!}');
        });
        // Remove fields
        function removeBox(index){
            $('#addMoreBox'+index).remove();
        }

        $('#saveManager').click(function () {
            $.easyAjax({
                url: '{{route('admin.employees.storeReportTeam')}}',
                container: '#reportTeam',
                type: "POST",
                redirect: true,
                data: $('#reportTeam').serialize()
            })
        });

        $('#saveDirect').click(function () {
            $.easyAjax({
                url: '{{route('admin.employees.storeDirectTeam')}}',
                container: '#directReport',
                type: "POST",
                redirect: true,
                data: $('#directReport').serialize()
            })
        });
    </script>
@endpush

