
@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="sttabs tabs-style-line col-md-12">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li><a href="{{route('admin.employees.edit', [$userDetail->id])}}"><span>@lang('modules.employees.general')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.employee_job_profile', [$userDetail->id])}}"><span>@lang('modules.employees.job')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.employee_profile_documents', [$userDetail->id])}}"><span>@lang('modules.employees.documents')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.team', [$userDetail->id])}}"><span>@lang('modules.employees.team')</span></a>
                                </li>
                                <li class="tab-current"><a href="{{route('admin.employees.education', [$userDetail->id])}}"><span>@lang('modules.employees.education')</span></a>
                                </li>
                                <li><a href="{{route('admin.employees.family', [$userDetail->id])}}"><span>@lang('modules.employees.family')</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'educationDetail','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">

                            <div class="col-md-12">
                                <div id="insertBefore"></div>
                                <div class="clearfix">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                            Add Education<i class="fa fa-plus"></i>
                                        </button>
                                    </div>

                                </div>

                            </div>
                            <div class="form-actions">
                                 <button type="submit" id="save-form-2" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                 <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                             </div>
                            {!! Form::hidden('user_id', Auth::id()) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>    <!-- .row -->
    </div>
        @endsection

        @push('footer-script')
            <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
            <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
            <script>
                var $insertBefore = $('#insertBefore');
                var $i = 0;
                // Add More Inputs
                $('#plusButton').click(function(){

                    $i = $i+1;
                    var indexs = $i+1;
                    $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
                        '<div class="col-md-4"><div class="form-group "><select  class="form-control" id="qualification_type['+$i+']" name="qualification_type['+$i+']"><option value="">Select Qualification Type</option><option value="graduation">Graduation</option><option value="post_graduation">Post Graduation</option><option value="doctorate">Doctorate</option><option value="diploma">Diploma</option><option value="pre_univercity">Pre Univercity</option><option value="other_education">Other Education</option><option value="certification">Certification</option></select></div></div>' +
                        '<div class="col-md-4 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="course_name['+$i+']" id = "course_name['+$i+']" type="text" value="" placeholder="Course Name"/></div></div>' +
                        '<div class="col-md-3 "style="margin-left:5px;"><div class="form-group"><select class="form-control " name="course_type['+$i+']" id="course_type['+$i+']" ><option value="">Select Course Type</option><option value="full_time">Full Time</option><option value="part_time">Part Time</option><option value="correpondence">Correspondence</option><option value="certificate">Certificate</option></select></div></div>' +
                        '<div class="col-md-4 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="stream['+$i+']" id="stream['+$i+']" type="text" value="" placeholder="Stream"/></div></div>' +
                        '<div class="col-md-4"><div class="form-group "><input autocomplete="off" class="form-control date-picker'+$i+'" id="startDate'+indexs+'" name="startDate['+$i+']" data-date-format="dd/mm/yyyy" type="text" value="" placeholder="Course Start Date"/></div></div>' +
                        '<div class="col-md-3"><div class="form-group "><input autocomplete="off" class="form-control date-picker'+$i+'" id="endDate'+indexs+'" name="endDate['+$i+']" data-date-format="dd/mm/yyyy" type="text" value="" placeholder="Course End Date"/></div></div>' +
                        '<div class="col-md-5 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="college_name['+$i+']" id="college_name['+$i+']" type="text" value="" placeholder="College Name"/></div></div>' +
                        '<div class="col-md-5 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="univercity_name['+$i+']" id="univercity_name['+$i+']" type="text" value="" placeholder="University Name"/></div></div>' +
                        '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);

                    // Recently Added statrt date picker assign
                    jQuery('#startDate'+indexs).datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        weekStart:'{{ $global->week_start }}',
                        format: '{{ $global->date_picker_format }}',
                    });// Recently Added End date picker assign
                    jQuery('#endDate'+indexs).datepicker({
                        autoclose: true,
                        todayHighlight: true,
                        weekStart:'{{ $global->week_start }}',
                        format: '{{ $global->date_picker_format }}',
                    });
                });
                // Remove fields
                function removeBox(index){
                    $('#addMoreBox'+index).remove();
                }

                $('#save-form-2').click(function () {
                    var url = "{{ route('admin.employees.storeEducationDetail') }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        container: '#educationDetail',
                        data: $('#educationDetail').serialize(),
                        success: function (response) {
                            console.log([response, 'success']);

                        }
                    });
                });

            </script>
    @endpush