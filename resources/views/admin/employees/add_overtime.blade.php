@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.expenses.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">


@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel ">
                <div class="panel-heading">@lang('modules.employees.addOvertime')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createExpense','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label>@lang('modules.messages.chooseMember')</label>
                                        <select id="user_id" class="select2 form-control" data-placeholder="@lang('modules.messages.chooseMember')" name="user_id">
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee['user']['id'] }}">{{ ucwords($employee['user']['name']) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!--/span-->
                                <input type="hidden"  id = "date" name = "date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}"></input>

                                <!--/span-->
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="col-md-6">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>@lang('modules.attendance.clock_in')</label>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-group bootstrap-timepicker timepicker">
                                                    <input type="text" name="clock_in_time" id="clock-in"
                                                           class="form-control a-timepicker"   autocomplete="off"  value="">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>@lang('modules.attendance.clock_out')</label>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-group bootstrap-timepicker timepicker">
                                                    <input type="text" name="clock_in_out" id="clock-in"
                                                           class="form-control b-timepicker"   autocomplete="off"  value="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <!--/span-->
                                    <div class="col-md-12 ">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>@lang('modules.overtime.salarytypeoption')</label>
                                                <select id="sallary_id" class="select2 form-control" data-placeholder="@lang('modules.overtime.salarytype')" name="sallary_id">

                                                    <option value="salaried">@lang('modules.overtime.salaried')</option>
                                                    <option value="lumpsum">@lang('modules.overtime.lumpsum')</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="padding: 25px">
                                            <input type="text"  placeholder="@lang('modules.overtime.enteramount')" id = "amount" name = "amount" class="form-control"></input>
                                        </div>

                                    </div>

                                    <!--/span-->
                            </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i>
                                @lang('app.save')
                            </button>
                            <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')


            <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>

            <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

            <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
            <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
    <script>

        var employees = @json($employees);

        $('#user_id').change(function (e) {
            // get projects of selected users
            var opts = '';

            var employee = employees.filter(function (item) {
                return item.id == e.target.value
            });

            employee[0].user.projects.forEach(project => {
                opts += `<option value='${project.id}'>${project.project_name}</option>`
        })

            $('#project_id').html('<option value="0">Select Project...</option>'+opts)
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        jQuery('#purchase_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });


                    $('.a-timepicker').timepicker({
                        @if($global->time_format == 'H:i')
                        showMeridian: false,
                        @endif
                        minuteStep: 1,
                        defaultTime: false

                    });

                    $('.b-timepicker').timepicker({
                        @if($global->time_format == 'H:i')
                        showMeridian: false,
                        @endif
                        minuteStep: 1,
                        defaultTime: false
                    });



        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.employees.postOvertime')}}',
                container: '#createExpense',
                type: "POST",
                redirect: true,
                data: $('#createExpense').serialize()
            })
        });
    </script>
@endpush
