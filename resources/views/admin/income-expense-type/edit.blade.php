@extends('layouts.app')
<?php
$moduleName = "  Ledger Type";
$createItemName = "Edit" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Edit";
$breadcrumbMainIcon = "fas fa-file-invoice-dollar";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\IncomeExpenseType';
$ParentRouteName = 'admin.income_expense_type';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Edit {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ $item->name }}" name="name" type="text"
                                                   class="form-control">
                                            <label class="form-label">Name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input readonly value="{{ $item->code }}" name="code" type="number"
                                                   class="form-control">
                                            <label class="form-label">Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" id="save-form" class="btn btn-primary m-t-15 waves-effect">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.update',['id'=>$item->id]) }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });
    </script>
@endpush