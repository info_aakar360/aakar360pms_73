<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Resource type</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editType','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $type->title }}">
                    </div>
                    <div class="form-group">
                        <label>Symbol</label>
                        <input type="text" name="symbol" id="symbol" class="form-control" value="{{ $type->symbol }}">
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-type" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-type').click(function () {
        $.easyAjax({
            url: '{{route('admin.types.update', [$type->id])}}',
            container: '#editType',
            type: "PATCH",
            data: $('#editType').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

</script>