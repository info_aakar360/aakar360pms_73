@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.all-tasks.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('css/rangeslider.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
        ul.timelinereply {
            list-style-type: none;
            position: relative;
        }
        ul.timelinereply:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }
        ul.timelinereply > li {
            margin: 5px 0;
            padding-left: 20px;
        }
        ul.timelinereply > li:before {
            content: ' ';
            background: white;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;
        }
        .statusicon {
            padding: 5px;
            margin: 2px;
            display: inline-block;
        }
        .statusicon.active {
             border: 1px solid;
             border-radius: 50px;
         }
        .w-100{
            width: 100%;
        }
        .smalltext{
            font-size: 10px;
            text-align: right;
        }
        .float-left{
            float: left;
        }
        .float-right{
            float: right;
        }
        .bck-box {
            background-color: #EFEFEF;
            padding: 10px 20px;
            border: 2px solid #c7cbcbed;
            border-bottom: 0 !important;
        }
        .taskusername {
            font-size: 15px;
            font-weight: bold;
            display: inline-block;
        }
        .tasksdate {
            font-size: 10px;
            float: right;
        }
        .timelineblock {
            background-color: #efefef;
            padding: 10px;
            border-radius: 10px;
            margin-bottom: 10px;
        }
        .list-group-item a, .list-group-item span{
            font-size: 10px;
        }
        .nav.nav-pills.text-center {
            justify-content: center;
            display: flex;
        }
        .dropzone {
            min-height: 155px;
        }
        .mb-30 {
            margin-bottom: 30px;
        }
        .mb-20 {
            margin-bottom: 20px;
        }
        .mt-10{
            margin-top: 10px;
        }
    </style>

@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.tasks.updateTask')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="col-md-12">
                        <div class="form-group">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Task:</strong></label>
                                                <p>{{ $task->heading }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Start date:</strong></label>
                                                <p>{{ date('d M Y',strtotime($task->start_date)) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Due date:</strong></label>
                                                <p>{{ date('d M Y',strtotime($task->due_date)) }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                            <div class="form-group">
                                                <?php
                                                $statusicon = '<label class="control-label"><strong>Not Started</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="Not Started" class="img-circle" width="60" src="'.asset('public/user-uploads/not-started.png').'" />';

                                                switch ($task->status){
                                                    case 'notstarted':
                                                        $statusicon = '<label class="control-label"><strong>Not Started</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="Not Started"  class="img-circle" width="60" src="'.asset('public/user-uploads/not-started.png').'" />';
                                                        break;
                                                    case 'inprogress':
                                                        $statusicon = '<label class="control-label"><strong>In Progress</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="In Progress"  class="img-circle" width="60" src="'.asset('public/user-uploads/in-progress.png').'" />';
                                                        break;
                                                    case 'inproblem':
                                                        $statusicon = '<label class="control-label"><strong>In Problem</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="In Problem"  class="img-circle" width="60" src="'.asset('public/user-uploads/in-problem.png').'" />';
                                                        break;
                                                    case 'delayed':
                                                        $statusicon = '<label class="control-label"><strong>Delayed</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="Delayed"  class="img-circle" width="60" src="'.asset('public/user-uploads/delay.png').'" />';
                                                        break;
                                                    case 'completed':
                                                        $statusicon = '<label class="control-label"><strong>Completed</strong></label>&nbsp;&nbsp;&nbsp;<img data-toggle="tooltip" data-original-title="Completed"  class="img-circle" width="60" src="'.asset('public/user-uploads/completed.png').'" />';
                                                        break;
                                                }
                                                echo $statusicon;
                                                ?>
                                            </div>
                                            </div>

                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label"><strong>@lang('modules.tasks.taskpercentage'):</strong></label>
                                                <output class="pull-right percentclass">{{  !empty($task) ?  numberformat($task->percentage) : 0 }}%</output>
                                                <input type="range" name="percentage" class="taskpercentage"  min="0" max="100" step="1" value="{{  !empty($task) ?  $task->percentage : 0 }}" data-rangeslider>
                                                <output class="pull-right"><span class="percentqty">{{  !empty($task->taskqty) ?  $task->taskqty : 0 }}</span> / {{  !empty($projectcostitem->qty) ? $projectcostitem->qty : 0 }} {{ !empty($projectcostitem->unit) ? get_unit_name($projectcostitem->unit) : '' }}</output>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-30">
                                            <div class="form-group">
                                                 <div class="text-center">
                                                     <div class="col-md-3">
                                                         <div class="form-group">
                                                     <a href="javascript:void(0);" class="statusicon active">
                                                         <img data-toggle="tooltip" data-original-title="Not Started" data-status="notstarted" class="img-circle" width="60" src="{{ asset('public/user-uploads/not-started.png') }}" />
                                                      </a>
                                                         <p>Not Started</p>
                                                     </div>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <div class="form-group">
                                                     <a href="javascript:void(0);" class="statusicon">
                                                         <img data-toggle="tooltip" data-original-title="In Progress" data-status="inprogress" class="img-circle" width="60" src="{{ asset('public/user-uploads/in-progress.png') }}" />
                                                     </a>
                                                         <p>In Progress</p>
                                                     </div>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <div class="form-group">
                                                     <a href="javascript:void(0);" class="statusicon">
                                                         <img data-toggle="tooltip" data-original-title="In Problem" data-status="inproblem" class="img-circle" width="60" src="{{ asset('public/user-uploads/in-problem.png') }}" />
                                                      </a>
                                                        <p>In Problem</p>
                                                     </div>
                                                     </div>
                                                     <div class="col-md-2">
                                                         <div class="form-group">
                                                     <a href="javascript:void(0);" class="statusicon">
                                                         <img data-toggle="tooltip" data-original-title="Delayed" data-status="delayed" class="img-circle" width="60" src="{{ asset('public/user-uploads/delay.png') }}" />
                                                     </a>
                                                         <p>Delayed</p>
                                                     </div>
                                                     </div>
                                                     <div class="col-md-3">
                                                         <div class="form-group">
                                                     <a href="javascript:void(0);" class="statusicon">
                                                         <img data-toggle="tooltip" data-original-title="Completed" data-status="completed"  class="img-circle" width="60" src="{{ asset('public/user-uploads/completed.png') }}" />
                                                     </a>
                                                         <p>Completed</p>
                                                     </div>
                                                     </div>
                                                     <input type="hidden" class="form-control taskstatus" value="notstarted"  name="status" />
                                                 </div>
                                            </div>
                                        </div>
                                    <div class="col-md-12 mb-30">
                                        <?php $totalqty = !empty($projectcostitem) ? $projectcostitem->qty : 0;?>
                                        <div class="col-md-3">
                                            <div class="form-group text-center">
                                                <label class="control-label mb-20"><strong>@lang('app.todaysquantity')</strong></label>
                                                <input type="text" class="form-control todayqty" placeholder="Today Quantity" value="{{ $todquant =  !empty($todayquantity) ? $todayquantity->todayqty : 0 }}" name="todayqty" />
                                             </div>
                                        </div>
                                            <div class="col-md-3">
                                            <div class="form-group text-center">
                                                <label class="control-label mb-20"><strong>@lang('app.completedquantity')</strong></label>
                                                <p class="taskqtyhtml">{{ !empty($completedquantity) ? $completedquantity : 0 }}</p>
                                                <input type="hidden" class="form-control taskqty" placeholder="Quantity" value="{{ !empty($completedquantity) ? $completedquantity : 0 }}" name="taskqty" />
                                                <input type="hidden" class="completedqty" value="{{ !empty($completedquantity) ? $completedquantity : 0 }}" />
                                            </div>
                                        </div>
                                            <div class="col-md-3">
                                            <div class="form-group text-center">
                                                <label class="control-label mb-20"><strong>@lang('app.balancequantity')</strong></label>
                                                <p class="balanceqty">{{ !empty($completedquantity) ? $totalqty-$completedquantity : 0 }}</p>
                                            </div>
                                        </div>
                                            <div class="col-md-3">
                                            <div class="form-group text-center">
                                                <label class="control-label mb-20"><strong>@lang('app.totalquantity')</strong></label>
                                                <p>{{ $totalqty  }}</p>
                                            </div>
                                        </div>

                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group mentiontags">
                                        <label class="control-label"><strong>@lang('modules.tasks.comment'):</strong></label>
                                        <textarea class="form-control ajax-mention" name="comment" rows="7"></textarea>
                                        <input type="hidden" class="mentionsCollection" name="mentionusers" />
                                    </div>
                                </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label"><strong>Images:</strong></label>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url" type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="taskID" id="taskID">
                                        <input type="hidden" name="percentageID" id="percentageID">
                                    </div>
                                    </div>
                                <div class="col-md-12">
                                    <div class="form-actions text-right">
                                        <input type="hidden" name="moduletype" class="moduletype" value="comment" />
                                        <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.update')</button>
                                    </div>
                                    </div>
                            </div>
                            <!--/row-->
                        </div>
                        {!! Form::close() !!}
                        </div>
                        </div>
                        <div class="col-md-12">
                            <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#home">@lang('app.menu.timeline')</a></li>
                            <li><a data-toggle="pill" href="#menu1">@lang('app.menu.labourattendance')</a></li>
                            <li><a data-toggle="pill" href="#menu2">@lang('app.menu.issue')</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="col-md-6">
                                <?php if(count($timelinearray)>0){
                                ?>
                                <?php
                                foreach ($timelinearray as $timeline){
                                $tablename = $timeline->getTable();
                                switch($tablename){
                                case 'task_percentage': ?>
                                <div class="col-md-9 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                                    <div class="col-md-1">
                                        <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                                    </div>
                                    <div class="col-md-10">
                                        <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                                        <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                                        <div class="commenttext">
                                            @if($timeline->moduletype=='percentage')
                                                <a href="javascript:void(0);" > Percentage <b>{!! $timeline->percentage.'%' !!}</b> Task Update from {{ get_user_name($timeline->added_by) }} with Updated Quantity <b>{{ $timeline->taskqty }}</b>, Today Quantity is <b>{{ $timeline->todayqty }} {{ !empty($projectcostitem->unit) ? get_unit_name($projectcostitem->unit) : '' }}</b></a>
                                            @endif
                                            @if($timeline->moduletype=='comment')
                                                <div>
                                                    <strong>Comment: </strong><br>
                                                    {{ $timeline->comment }}
                                                </div>
                                            @endif
                                            @if($timeline->moduletype=='status')
                                                <div>
                                                    <strong>Status: </strong><br>
                                                    Task status update to {{ $timeline->status }}
                                                </div>
                                            @endif
                                                <div class="col-md-12 form-group">
                                                    {!! mentionusers_html($timeline->mentionusers) !!}
                                                </div>
                                                <div class="row">
                                                <?php $files = \App\TaskFile::where('task_id',$timeline->task_id)->where('task_percentage_id',$timeline->percentid)->get();

                                                $storage = storage();
                                                ?>
                                                @foreach($files as $file)
                                                        <div class="col-md-3">
                                                            @if($file->external_link != '')
                                                                <?php $imgurl = $file->external_link;?>
                                                            @elseif($storage == 'local')
                                                                <?php $imgurl = uploads_url().'task-files/'.$file->task_id.'/'.$file->hashname;?>
                                                            @elseif($storage == 's3')
                                                                <?php $imgurl = awsurl().'/task-files/'.$file->task_id.'/'.$file->hashname;?>
                                                            @elseif($storage == 'google')
                                                                <?php $imgurl = $file->google_url;?>
                                                            @elseif($storage == 'dropbox')
                                                                <?php $imgurl = $file->dropbox_link;?>
                                                            @endif
                                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                        </div>
                                                @endforeach
                                        </div>

                                        </div>
                                    </div>
                                </div>
                                <?php    break;
                                case 'manpower_logs': ?>
                                <div class="col-md-9 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                                    <div class="col-md-1">
                                        <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                                    </div>
                                    <div class="col-md-11">
                                        <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                                        <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                                        <div class="commenttext">
                                            <div class="row">
                                                <div class="col-md-11 form-group">
                                                    <a href="javascript:void(0);" > Manpower <b>{!! $timeline->manpower !!}</b> With Work time <b>{!! $timeline->workinghours !!}</b> Update from {{ get_user_name($timeline->added_by) }}</a>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="{{ route('admin.man-power-logs.edit',$timeline->id) }}" class="float-right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php $files = \App\ManpowerLogFiles::where('manpower_id',$timeline->manpowerid)->get();
                                                $storage = storage();
                                                ?>
                                                    @foreach($files as $file)
                                                        <div class="col-md-3">
                                                            @if($file->external_link != '')
                                                                <?php $imgurl = $file->external_link;?>
                                                            @elseif($storage == 'local')
                                                                <?php $imgurl = uploads_url().'manpower-log-files/'.$file->manpower_id.'/'.$file->hashname;?>
                                                            @elseif($storage == 's3')
                                                                <?php $imgurl = awsurl().'/manpower-log-files/'.$file->manpower_id.'/'.$file->hashname;?>
                                                            @elseif($storage == 'google')
                                                                <?php $imgurl = $file->google_url;?>
                                                            @elseif($storage == 'dropbox')
                                                                <?php $imgurl = $file->dropbox_link;?>
                                                            @endif
                                                            {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                            <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                        </div>
                                                    @endforeach
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <?php    break;
                                case 'punch_item': ?>
                                <div class="col-md-9 timelineblock <?php if($user->id==$timeline->added_by){ echo 'float-right';}?>">
                                    <div class="col-md-1">
                                        <img class="img-circle w-100"  src="{{ get_users_image_link($timeline->added_by) }}" />
                                    </div>
                                    <div class="col-md-11">
                                        <div class="taskusername">{{ get_user_name($timeline->added_by) }} </div>
                                        <div class="tasksdate">{{ \Carbon\Carbon::parse($timeline->created_at)->format('d/m/Y') }}</div>
                                        <div class="commenttext">
                                            <div class="row">
                                                <div class="col-md-11 form-group">
                                                    <a href="javascript:void(0);" > Punch item <b>{!! $timeline->title !!}</b> With <b>{!! $timeline->status !!}</b> Status Update by {{ get_user_name($timeline->added_by) }}</a>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="{{ route('admin.issue.edit',$timeline->id) }}" class="float-right"><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php $files = \App\PunchItemFiles::where('task_id',$timeline->punchitemid)->get();  ?>
                                                @foreach($files as $file)
                                                    <div class="col-md-3">
                                                        @if($file->external_link != '')
                                                            <?php $imgurl = $file->external_link;?>
                                                        @elseif($storage == 'local')
                                                            <?php $imgurl = uploads_url().'punch-files/'.$file->task_id.'/'.$file->hashname;?>
                                                        @elseif($storage == 's3')
                                                            <?php $imgurl = awsurl().'/punch-files/'.$file->task_id.'/'.$file->hashname;?>
                                                        @elseif($storage == 'google')
                                                            <?php $imgurl = $file->google_url;?>
                                                        @elseif($storage == 'dropbox')
                                                            <?php $imgurl = $file->dropbox_link;?>
                                                        @endif
                                                        {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                                        <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                break;
                                }
                                ?>
                                <?php }?>
                                </ul>
                                <?php }else{?>
                                <p>Timeline not found</p>
                                <?php }?>
                            </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! Form::open(['id'=>'logTime','class'=>'ajax-form','method'=>'POST', 'enctype'=>'multipart/form-data']) !!}
                                        <div class="form-body">
                                            <div class="row m-t-30">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>
                                                            @lang('modules.manpowerLogs.selectContractor')
                                                        </label>
                                                        <select class="select2 form-control" name="contractor" data-placeholder="@lang('modules.manpowerLogs.selectContractor')" id="contractor2">
                                                            <option value=""></option>
                                                            @foreach($contractorsarray as $contractor)
                                                                <option value="{{ $contractor->user_id }}">{{ ucwords($contractor->name) }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>
                                                            @lang('modules.manpowerLogs.manpowerCat')
                                                        </label>
                                                        <select class="select2 form-control" name="category" data-placeholder="@lang('modules.manpowerLogs.manpowerCat')" id="contractor2">
                                                            <option value=""></option>
                                                            @foreach($manpowercategory as $manpowercat)
                                                                <option value="{{ $manpowercat->id }}">{{ ucwords($manpowercat->title) }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>@lang('modules.manpowerLogs.manpower')</label>
                                                        <input  name="manpower" type="text"  class="form-control" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>@lang('modules.manpowerLogs.workinghours')</label>
                                                        <input  name="workinghours" type="text"  class="form-control" value="" />
                                                    </div>
                                                </div>
                                               {{-- <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>
                                                            @lang('modules.manpowerLogs.selectProject')
                                                        </label>
                                                        <select class="select2 form-control" name="project_id" data-placeholder="@lang('modules.manpowerLogs.selectProject')"  id="project_id2">
                                                            <option value=""></option>
                                                            @foreach($projectarray as $project)
                                                                <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 projecttitle hide">
                                                    <div class="form-group">
                                                        <label>@lang('modules.manpowerLogs.projectTitle')</label>
                                                        <select class="form-control" name="title_id" id="titlelist" data-style="form-control">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>@lang('modules.manpowerLogs.projectCostitem')</label>
                                                        <select class="form-control" name="costitem" id="costitemlist" data-style="form-control">
                                                        </select>
                                                    </div>
                                                </div>--}}
                                            </div>
                                            <div class="row m-t-20">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="memo">Description</label>
                                                        <textarea name="description"  class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row m-b-20">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                <div id="file-upload-box" >
                                                    <div class="row" id="file-dropzone">
                                                        <div class="col-md-12">
                                                            <div class="dropzone dropheight"
                                                                 id="file-upload-dropzone-manpower">
                                                                {{ csrf_field() }}
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple/>
                                                                </div>
                                                                <input name="image_url" id="image_url" type="hidden" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="manpowerID" id="manpowerID">
                                            </div>
                                        </div>
                                        <div class="form-actions m-t-30">
                                            <button type="button" id="manpower-save-form" class="btn btn-success"><i
                                                        class="fa fa-check"></i> @lang('app.save')</button>
                                            <input type="hidden" name="task_id" value="{{ $task->id }}" />
                                            <input type="hidden" name="project_id" value="{{ $task->project_id }}" />
                                        </div>
                                        {!! Form::close() !!}
                                        <hr>
                                    </div>
                                </div>
                                  </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! Form::open(['id'=>'storePunchitem','class'=>'ajax-form','method'=>'POST']) !!}
                                        <div class="form-body">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('app.title') </label>
                                                        <input type="text" name="title" class="form-control" required placeholder="Title *" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                                        <select required class="selectpicker form-control" name="assign_to" id="user_id">
                                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                                            @foreach($employees as $employee)
                                                                <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label class="control-label">Status
                                                        </label>
                                                        <select required class="selectpicker form-control" name="status" data-style="form-control" required>
                                                            <option value="">Please Select Status</option>
                                                            <option value="Open">Open</option>
                                                            <option value="Resolved">Resolved</option>
                                                            <option value="Inprogress">Inprogress</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Type</label>
                                                        <select required class="selectpicker form-control" name="type" data-style="form-control" required>
                                                            <option value="">Please Select type</option>
                                                            <option value="Architect">Architect</option>
                                                            <option value="Contractor">Contractor</option>
                                                            <option value="Owner">Owner</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label class="control-label">Distribution
                                                        </label>
                                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                                            <option value="">Please Select Distribution</option>
                                                            @foreach($employees as $employee)
                                                                <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Location</label>
                                                        <input required type="text" id="location" name="location" class="form-control" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="privete">Private or Public</label><br>
                                                        <input id="privete" name="private" value="1" type="checkbox">
                                                        <label for="privete">Private</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('app.startDate')</label>
                                                        <input type="text" required name="start_date"  id="start_date2" class="form-control" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('app.dueDate')</label>
                                                        <input type="text" required name="due_date"  id="due_date2" class="form-control" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Priority</label>
                                                        <select required class="selectpicker form-control" name="priority" data-style="form-control" required>
                                                            <option value="">Please Select Priority</option>
                                                            <option value="High">High</option>
                                                            <option value="Medium">Medium</option>
                                                            <option value="Low">Low</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Reference
                                                        </label>
                                                        <input type="text" name="reference" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="control-label">Schedule Impact days</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select class="selectpicker form-control schimpact" name="schimpact" data-style="form-control"  required>
                                                                <option value="">Please Select</option>
                                                                <option value="yes">Yes</option>
                                                                <option value="no">No</option>
                                                                <option value="TBD">TBD</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 schimpactblock" style="display: none;">
                                                            <input type="text" name="schimpact_days" class="form-control" placeholder="Impact days *" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="control-label">Cost Impact days</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select class="selectpicker form-control costimpact" name="costimpact" data-style="form-control"  required>
                                                                <option value="">Please Select</option>
                                                                <option value="yes">Yes</option>
                                                                <option value="no">No</option>
                                                                <option value="TBD">TBD</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 costimpactblock" style="display: none;">
                                                            <input type="text" name="costimpact_days" class="form-control" placeholder="Cost Impact days *" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">@lang('app.description')</label>
                                                        <textarea id="description" name="description" class="form-control summernote"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row m-b-20">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                        <div id="file-upload-box" >
                                                            <div class="row" id="file-dropzone">
                                                                <div class="col-md-12">
                                                                    <div class="dropzone dropheight"
                                                                         id="file-upload-dropzone-punchitem">
                                                                        {{ csrf_field() }}
                                                                        <div class="fallback">
                                                                            <input name="file" type="file" multiple/>
                                                                        </div>
                                                                        <input name="image_url" id="image_url"type="hidden" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="punchID" id="punchID">
                                                    </div>
                                                </div>

                                            </div>
                                            <!--/row-->

                                        </div>
                                        <div class="form-actions">
                                            <input type="hidden" name="project_id" value="{{ $task->project_id }}" />
                                            <input type="hidden" name="task_id" value="{{ $task->id }}" />
                                            <button type="button" id="store-punchitem" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <h3>Menu 3</h3>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            </div>
                        </div>
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/rangeslider.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>

    <script>
        /*function changeQuantity(){
            var taskqty = $(".taskqty").val();
            var taskpercentage = $(".taskpercentage").val();
            var totalquantity = '{{ $totalqty }}';
            if(){

            }
        }*/
        $(".todayqty").change(function(){
            var totalquantity = '{{ $totalqty }}';
            if(totalquantity>0){
            var todayqty = parseFloat($(this).val());
            var completedqty = parseFloat($(".completedqty").val());
               var taskqty = completedqty+todayqty;
                $(".taskqty").val(taskqty);
                $(".taskqtyhtml").html("");
                $(".taskqtyhtml").html(taskqty);

                var balqty = totalquantity-taskqty;
                if(balqty<0){
                    $(".todayqty").val(balqty+todayqty);
                    balqty = 0;
                }
                    $(".balanceqty").html("");
                $(".balanceqty").html(balqty.toFixed(2));

                var taskpercentage = (totalquantity-taskqty)/totalquantity*100;
                taskpercentage = 100-taskpercentage;
                $(".taskpercentage").val(taskpercentage).change();
                $(".percentclass").html("");
                if(taskpercentage>100){
                    taskpercentage = 100;
                }
                $(".percentclass").html(taskpercentage.toFixed(2)+"%");
                $(".percentqty").html("");
                $(".percentqty").html(taskqty);
            }
        });

        $(".taskpercentage").change(function(){
            var taskpercentage = $(this).val();
            var totalquantity = '{{ $totalqty }}';
            var completequant = $(".completedqty").val();
            if(totalquantity>0){
                var percentInDecimal = taskpercentage / 100;
                var taskqty = percentInDecimal * totalquantity;
                var balqty = totalquantity-taskqty;
                var todayqty = taskqty-completequant;
                  if((taskqty ^ 0) !== taskqty){
                      $(".taskqty").val(taskqty.toFixed(2));
                }else{
                      $(".taskqty").val(taskqty);
                }
                $(".todayqty").val(todayqty.toFixed(2));
                $(".percentclass").html(taskpercentage+".00%");
                $(".taskqtyhtml").html("");
                $(".taskqtyhtml").html(taskqty.toFixed(2));
                $(".balanceqty").html("");
                $(".balanceqty").html(balqty.toFixed(2));

                $(".moduletype").val('percentage');
            }
        });

        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.task-files.store') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = '{{ $task->id }}';
            var percentageID = $('#percentageID').val();
            formData.append('task_id', ids);
            formData.append('percentage_id', percentageID);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.taskUpdatedSuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}'
        });

        myDropzoneMan = new Dropzone("div#file-upload-dropzone-manpower", {
            url: "{{ route('admin.man-power-logs.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzoneMan = this;
            }
        });
        myDropzoneMan.on('sending', function(file, xhr, formData) {
            var ids = '{{ $task->id }}';
            var manpowerID = $('#manpowerID').val();
            formData.append('task_id', ids);
            formData.append('manpower_id', manpowerID);
        });

        myDropzoneMan.on('completemultiple', function () {
            var msgs = "@lang('messages.manPowerUpdatedSuccessfully')";
            $.showToastr(msgs, 'success');
            var ids = '{{ $task->id }}';
           window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}'
        });
        myDropzonePunch = new Dropzone("div#file-upload-dropzone-punchitem", {
            url: "{{ route('admin.issue.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzonePunch = this;
            }
        });
        myDropzonePunch.on('sending', function(file, xhr, formData) {
            var ids = '{{ $task->id }}';
            var punchID = $('#punchID').val();
            formData.append('task_id', punchID);
        });

        myDropzonePunch.on('completemultiple', function () {
            var msgs = "@lang('messages.punchUpdate')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}';
        });
        var $document = $(document);
        var selector = '[data-rangeslider]';
        var $element = $(selector);
        // For ie8 support
        var textContent = ('textContent' in document) ? 'textContent' : 'innerText';
        function valueOutput(element) {
            var value = element.value;
            var output = element.parentNode.getElementsByTagName('output')[0] || element.parentNode.parentNode.getElementsByTagName('output')[0];
            output[textContent] = value+'%';
        }

        $document.on('input', 'input[type="range"], ' + selector, function(e) {
            valueOutput(e.target);
        });
        //    update task
        $('#update-task').click(function () {
            $.easyAjax({
                url: '{{route('admin.all-tasks.updatePercentage', [$task->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        percentageID = response.percentageID;
                        $('#taskID').val(response.taskID);
                        $('#percentageID').val(response.percentageID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.taskStatusSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}';
                    }
                }
            })
        });
        $(".schimpact").change(function () {
            var schimpact = $(this).val();
            if(schimpact=='yes'){
                $(".schimpactblock").show();
                $(".schimpactblock").attr('required','required');
            }else{
                $(".schimpactblock").hide();
                $(".schimpactblock").removeAttr('required','required');
            }
        });
        $(".costimpact").change(function () {
            var costimpact = $(this).val();
            if(costimpact=='yes'){
                $(".costimpactblock").show();
                $(".costimpactblock").attr('required','required');
            }else{
                $(".costimpactblock").hide();
                $(".costimpactblock").removeAttr('required','required');
            }
        });
        $('#store-punchitem').click(function () {
            $.easyAjax({
                url: '{{route('admin.issue.store')}}',
                container: '#storePunchitem',
                type: "POST",
                data: $('#storePunchitem').serialize(),
                success: function (data) {
                    $('#storePunchitem').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzonePunch.getQueuedFiles().length > 0){
                        $('#punchID').val(data.taskID);
                        myDropzonePunch.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.punchUpdate')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}';
                    }
                }
            })
        });
        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $('#manpower-save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.man-power-logs.storedata')}}',
                container: '#logTime',
                type: "POST",
                data: $('#logTime').serialize(),
                success: function (data) {
                    if(myDropzoneMan.getQueuedFiles().length > 0){
                        manpowerID = data.manpowerID;
                        $('#manpowerID').val(data.manpowerID);
                        myDropzoneMan.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.manpowerUpdate')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.all-tasks.updateTask',$task->id) }}'
                    }

                }
            })
        });

        $('#project_id2').change(function () {
            var project = $(this).val();
            if(project){
                $(".projecttitle").addClass("hide");
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        var projectdata = JSON.parse(data);
                        var titles = '<option value="">Select Title</option>';
                        var cositem = '<option value="">Select Cost item</option>';
                        if(projectdata.titles){
                            $(".projecttitle").removeClass("hide");
                            $.each( projectdata.titles, function( key, value ) {
                                titles += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#titlelist").html("");
                        $("select#titlelist").html(titles);
                        $('select#titlelist').select2();
                        if(projectdata.cositems){
                            $.each( projectdata.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#project_id2").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.projectcostitems') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        var titles = JSON.parse(data);
                        var cositem = '';
                        if(titles.cositems){
                            $.each( titles.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#costitemlist").html("");
                        $("select#costitemlist").html(cositem);
                        $('select#costitemlist').select2();
                    }
                });
            }
        });
        $(".statusicon img").click(function(){
            var status = $(this);
            var stat = status.attr('data-status');
            $(".statusicon").removeClass('active');
            status.parent().addClass('active');
            $(".taskstatus").val(stat);
            $(".moduletype").val('status');
        });

        var url = '{{ route('admin.projects.members',[$task->project_id]) }}';
        $('textarea.ajax-mention').mentionsInput({
            onDataRequest:function (mode, query, callback) {
                $.getJSON(url, function(responseData) {
                    responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                    callback.call(this, responseData);
                });
            }
        });

    </script>

@endpush
