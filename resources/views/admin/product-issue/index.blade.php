@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="col-lg-12">
                    <div class="form-group">
                        <h4 style="color: #002f76">Issued Products </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.product-issue.create') }}" class="btn btn-outline btn-success btn-sm">Issue<i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <table class="table-responsive table-responsive" id="example">
                    <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.pid')</th>
                            <th>@lang('app.tQty')</th>
                            <th>@lang('app.issuedby')</th>
                            <th>@lang('app.idt')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($issuedProducts as $key=>$product)
                            <?php $qcount = \App\ProductIssue::where('unique_id',$product->unique_id)->get(); ?>
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $product->unique_id }}</td>
                                <td>{{ count($qcount) }}</td>
                                <td>{{ get_user_name($product->issued_by) }}</td>
                                <td>{{ \Carbon\Carbon::parse($product->created_at)->format('d-m-Y') }}</td>
                                <td>
                                    <a href="{{ route('admin.product-issue.issuedProductDetail',[$product->unique_id])}}" class="btn btn-success btn-circle"
                                       data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="View Issued Products">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('admin.product-issue.return',[$product->unique_id])}}" class="btn btn-warning btn-circle"
                                       data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="Return Issued Products">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </a>
                                    <a data-user-id="{{ $product->unique_id }}" class="btn btn-danger btn-circle sa-params"
                                      data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!-- .row -->


@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.stores.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.productIssue').addClass('tab-current');
        $(document).ready(function() {
            $('#example').DataTable( {
                deferRender:    true,
                scrollCollapse: true,
                scroller:       true
            } );
        } );

        $(function() {
            $('body').on('click', '.sa-params', function () {
                var id = $(this).data('user-id');
                var url = "{{ route('admin.product-issue.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE'},
                    success: function (response) {
                        if (response.status === "success") {
                            location.reload();
                        }
                    }
                });
            });
        });
        $("#Pi").addClass("active");
    </script>

@endpush