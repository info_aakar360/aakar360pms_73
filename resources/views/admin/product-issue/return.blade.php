@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <h4>{{ $issuedProducts[0]->unique_id }}
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                            <tr class="bg-inverse">
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Total Quantity</th>
                                <th>Returned Quantity</th>
                                <th>Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                        {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                            {{ csrf_field() }}

                            @foreach($issuedProducts as $key=>$product)
                                @php
                                    $issueretrun = \App\ProductIssueReturn::where('unique_id',$product->unique_id)->where('product_id',$product->product_id)->sum('qty');
                                        if(!empty($issueretrun)){
                                            $totalqty = $product->quantity - $issueretrun;
                                        }else{
                                             $totalqty = $product->quantity;
                                        }
                                @endphp
                                <input type="hidden" name="project_id" id="project_id" value="{{$product->project_id}}">
                                <input type="hidden" name="store_id" id="store_id" value="{{$product->store_id}}">
                                <tr>
                                    <td>{{ $key+1 }}<input type="hidden" class="form-control" name="id[]" value="{{ $product->id }}"></td>
                                    <td>{{ get_pcat_name($product->product_id) }}<input type="hidden" class="form-control" name="product_id[]" value="{{ $product->product_id }}"></td>
                                    <td><input type="hidden" class="form-control " name="quantity[]" value="{{ $totalqty }}">{{ $totalqty }}</td>
                                    <td><input type="number" class="form-control returnqty" data-qty="{{$totalqty}}" name="returned_quantity[]" min="0" placeholder="Returned Quantity"></td>
                                    <td>{{ get_unit_name($product->unit_id) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5">
                                    <button type="button" id="storeProduct" class="btn btn-success" style="float: right;">
                                        <i class="fa fa-check"></i>
                                        @lang('app.save')
                                    </button>
                                </td>
                            </tr>
                        {!! Form::close() !!}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $('#storeProduct').click(function () {
            $.easyAjax({
                url: '{{route('admin.product-issue.storeReturn')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        })
        $('ul.showProjectTabs .ProductIssue').addClass('tab-current');

        $('.returnqty').on('keyup',function () {
           var returnqty = $(this).val();
           var qty = $(this).data('qty');
           if(qty < returnqty){
               alert('Please Enter Valid Quantity');
               $(this).val(qty);
               $('#storeProduct').prop('disabled',true);
           }else{
               $('#storeProduct').prop('disabled',false);
           }
        });
    </script>

@endpush