@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <style>
        table, th, td {
            padding: 5px !important;
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <style>
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0px;
            }
        </style>
        <div class="col-md-12">
            {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
            {{ csrf_field() }}
            <div class="white-box">
                <div class="col-lg-12">
                    <div class="form-group">
                        <h4>Issue Products
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                            <label>Project</label>
                            <select class="form-control" name="project_id" id="project" data-style="form-control">
                                <option value="">Please select Project</option>
                                @foreach($projects as $category)
                                    <option value="{{ $category->id }}">{{ $category->project_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                            <label>Store</label>
                            <select class="form-control" name="store_id" id="store" data-style="form-control">
                                <option value="">Please select Store</option>

                            </select>
                        </div>
                        <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                            <label>Indent</label>
                            <select class="form-control" name="indent_no" data-style="form-control">
                                <option value="">Please select Indent</option>
                                @foreach($indents as $category)
                                    <option value="{{ $category->indent_no }}">{{ $category->indent_no }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" id="indentData" style="padding-top: 60px;">
                    <table class="table responsive">
                        <thead>
                            <th>Product</th>
                            <th >Unit</th>
                            <th >Brand</th>
                            {{--<th>Required Date</th>--}}
                            <th>Issued Quantity</th>
                            <th>To Be Issue Quantity</th>
                            </thead>
                        <tbody id="sortable">
                        <tr >
                            <td>
                                <div class="skuData" data-key="0" style="display: none;"></div>
                                <select class="selectpicker form-control product" name="product_id[]" data-key="0" data-style="form-control">
                                    <option value="">Please select Product</option>
                                    @foreach($products as $category)
                                        <option value="{{ $category->product_id }}">{{ get_local_product_name($category->product_id) }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td style="width: 15%;">
                                <input type="text" style="display: none;"  value="" class="form-control unitId" name="unit_id[]" data-key="0">
                                <input type="text" readonly class="form-control unitName" value="" name="unitName" data-key="0">
                            </td>
                            <td><input type="text" readonly class="form-control dataBrand" data-key="0"></td>
                            <td><input type="text" readonly class="form-control dataIqty" data-key="0"></td>
                            <td class="dataQty" data-key="0"><input type="number" name="quantity[]" placeholder="To Be Issue Quantity" class="form-control"></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-xs-12 m-t-5">
                    <button type="button" class="btn btn-info" id="add-item">
                        <i class="fa fa-plus"></i>
                        @lang('modules.invoices.addItem')
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12 m-t-5">
                        <button type="button" id="storeProduct" class="btn btn-success" style="float: right;"> <i class="fa fa-check"></i> @lang('app.save')</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
    <!-- .row -->
    {{--Ajax Modal--}}
    <a href="#taskCategoryModal" id="modelOpenButton" style="display: none;"></a>
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body" id="modelData">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        var r = 1;
        $('#add-item').click(function () {
            r++;
            var item = '<tr id="row'+r+'">'
                +'<td>'
                    +'<div class="skuData" data-key="'+r+'" style="display: none;"></div>'
                    +'<select class="select2 form-control product" data-key="'+r+'" name="product_id[]" data-style="form-control">'
                        +'<option value="">Select Product</option>';
                        @foreach($products as $category)
                            item  +='<option value="{{ $category->product_id }}">{{ get_local_product_name($category->product_id) }}</option>'
                        @endforeach
                    item += '</select>'
                +'</td>'
                +'<td style="width: 15%;">'
                    +'<input type="text" style="display: none;"  value="" data-key="'+r+'" class="form-control unitId" name="unit_id[]" data-key="0">'+
                            '<input type="text" readonly class="form-control unitName" value="" data-key="'+r+'" name="unitName" data-key="0">'
                +'</td>'
                +'<td><input type="text" readonly class="form-control dataBrand" data-key="'+r+'"></td><td><input type="number" readonly class="form-control dataIqty" data-key="'+r+'"></td><td class="dataQty" data-key="'+r+'"><input type="number" name="quantity[]" placeholder="To Be Issue Quantity" class="form-control"></td> <input type="hidden" name="serial" value="'+r+'">'
                +'<td>'
                    +'<button type="button" class="btn btn-circle btn-danger" onclick="removeItem('+r+')"><i class="fa fa-remove"></i></button>'
                +'</td>'

            +'</tr>';
            $(item).hide().appendTo("#sortable").fadeIn(500);
            $('.product').on('change',function(){
                var pid = $(this).val();
                var key = $(this).data('key');
                var project = $('select[name=project_id]').val();
                var store = $('select[name=store_id]').val();
                var token = '{{ csrf_token() }}';
                if(store == '' || project == ''){
                    alert('Invalid Data. Project and Store are mandatory.');
                }else {
                    $.ajax({
                        url: '{{route('admin.stores.projects.productPopup')}}',
                        type: 'POST',
                        data: {_token: token, pid: pid, project_id: project, store_id: store, key: key},
                        success: function (data) {
                            $('#taskCategoryModal').modal('show');
                            $('#modelData').html(data);
                        }
                    });
                }
            });
        });
        function removeItem(index) {
            $('#row'+index).remove();
        };

        $('#storeProduct').click(function () {
            $.easyAjax({
                url: '{{route('admin.product-issue.store')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            })
            return false;
        })

        $('ul.showProjectTabs .productIssue').addClass('tab-current');

        $(document).on('change', 'select[name=indent_no]', function(){
            var pid = $(this).val();
            var project = $('select[name=project_id]').val();
            var store = $('select[name=store_id]').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.stores.projects.getIndentData')}}',
                type: 'POST',
                data: {_token: token, pid: pid, store_id: store, project_id: project},
                success: function (data) {
                    $('#indentData').html('');
                    $('#indentData').html(data);
                }
            });

        });

        $('.product').on('change',function(){
            var pid = $(this).val();
            var key = $(this).data('key');
            var project = $('select[name=project_id]').val();
            var store = $('select[name=store_id]').val();
            var token = '{{ csrf_token() }}';
            if(store == '' || project == ''){
                alert('Invalid Data. Project and Store are mandatory.');
            }else {
                $.ajax({
                    url: '{{route('admin.stores.projects.productPopup')}}',
                    type: 'POST',
                    data: {_token: token, pid: pid, project_id: project, store_id: store, key: key},
                    success: function (data) {
                        $('#taskCategoryModal').modal('show');
                        $('#modelData').html(data);
                    }
                });
            }
        });
        $('#project').change(function () {
            var pid = $(this).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.indent.getStores')}}',
                type: 'POST',
                data: {_token: token, pid: pid},
                success: function (data) {
                    $('#store').html(data.stores);
                }
            });
        });
        $(document).on('click', '.sel_product', function(){
            var pid = $(this).val();
            var key = $(this).data('key');
            var sku_id = $(this).data('sku-id');
            var stock_id = $(this).data('id');
            var htmData = '<input type="text" class="stockId" name="stock_id[]" value="'+stock_id+'" style="display: none;" data-key="0"><input type="text" class="skuId" name="sku_id[]" value="'+sku_id+'" style="display: none;" data-key="0">';
            var project = $('select[name=project_id]').val();
            var store = $('select[name=store_id]').val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{route('admin.stores.projects.productData')}}',
                type: 'POST',
                data: {_token: token, pid: pid, project_id: project, store_id: store},
                success: function (data) {
                    $('.dataBrand[data-key='+key+']').val(data.brand);
                    $('.dataIqty[data-key='+key+']').val(data.iqty);
                    $('.unitId[data-key='+key+']').val(data.unit_id);
                    $('.unitName[data-key='+key+']').val(data.unit_name);
                    $('.skuData[data-key='+key+']').html(htmData);
                    $('#taskCategoryModal').modal('hide');
                }
            });
        });

        $("#Pi").addClass("active");
    </script>

@endpush