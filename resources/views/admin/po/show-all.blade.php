@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th colspan="5"></th>
                            @foreach($suppliers as $supplier)
                                <?php
                                $qx = \App\Quotes::where('rfq_id', $rfq->id)->where('supplier_id', $supplier->id)->first();
                                $final = 0;
                                $qpx = \App\QuoteProducts::where('quote_id', $qx->id)->get();
                                foreach($qpx as $qpxx){
                                    $final = $final + (is_numeric($qpxx->price) ? $qpxx->quantity*$qpxx->price : 0);
                                }
                                ?>
                            <th colspan="2" class="bg-inverse text-center">{{ $supplier->company_name }} <br><br> Final Amount : <span class="final" data-key="{{ $supplier->id }}">{{ $final }}</span></th>
                            @endforeach
                        </tr>
                        <tr class="bg-inverse">
                            <th class="bg-inverse">S. No.</th>
                            <th class="bg-inverse">Category</th>
                            <th class="bg-inverse">Brand</th>
                            <th class="bg-inverse">Quantity</th>
                            <th class="bg-inverse">Unit</th>
                            @foreach($suppliers as $supplier)
                            <th class="bg-inverse text-center"><label><input type="checkbox" name="select_all[{{$supplier->id}}]" data-key="{{$supplier->id}}" class="all_check"/> Rate</label></th>
                            <th class="bg-inverse text-center">Amount</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ get_pcat_name($product->cid) }}</td>
                                <td>{{ get_pbrand_name($product->bid) }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ get_unit_name($product->unit) }}</td>
                                @foreach($suppliers as $supplier)
                                    <?php
                                        $q = \App\Quotes::where('rfq_id', $rfq->id)->where('supplier_id', $supplier->id)->first();
                                        $qp = \App\QuoteProducts::where('quote_id', $q->id)->where('bid', $product->bid)->where('quantity', $product->quantity)->where('unit', $product->unit)->first();
                                        $minQuote = get_min_quote($rfq->id, $product->bid);
                                    ?>
                                    <td class="@if($minQuote == $qp->price) alert-success @endif text-center"><label><input type="checkbox" name="product[{{$supplier->id}}][{{$qp->id}}]" data-key="{{$supplier->id}}" data-qp="{{$qp->id}}" class="pro_check"/> {{ $qp->price }}</label></td>
                                    <td class="@if($minQuote == $qp->price) alert-success @endif text-center">{{ is_numeric($qp->price) ? $product->quantity*$qp->price : '-' }}</td>
                                @endforeach
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        <tr>
                            <th colspan="5"></th>
                            @foreach($suppliers as $supplier)
                                <th class="bg-inverse text-center" colspan="2">
                                    <form id="form{{$supplier->id}}" method="post" action="{{route('admin.quotations.generatePO')}}">
                                        <input type="hidden" name="qpid" value=""/>
                                        <input type="hidden" name="supplier_id" value="{{$supplier->id}}"/>
                                        {{ csrf_field() }}
                                    </form>
                                    <a href="javascript:;" class="btn btn-success generate_po" data-key="{{ $supplier->id }}">Generate PO</a>
                                </th>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    <script>

        $(function () {
            var id = 0;
            var min = 0;
            $('.final').each(function(){
                var obj = $(this);
                var pr = parseInt(obj.text().trim());
                if(min == 0){
                    min = pr;
                    id = obj.data('key');
                }else{
                    if(min > pr){
                        min = pr;
                        id = obj.data('key');
                    }
                }
            });
            if(id != 0 && min != 0){
                $('.final[data-key='+id+']').addClass('alert-success');
            }
        });

        $(document).on('click', '.pro_check', function(){
            var check = $(this);
            var key = check.data('key');
            var count = $('input.pro_check:checkbox:checked').length;
            if(count == 0){
                $('.pro_check').removeAttr("disabled");
                $('.all_check').removeAttr("disabled");
                $('.generate_po').removeAttr("disabled");
                $('#form'+key+' input[name=qpid]').val('');
            }else {
                $('.pro_check').attr("disabled", true);
                $('.all_check').attr("disabled", true);
                $('.generate_po').attr("disabled", true);
                $('.all_check[data-key=' + key + ']').removeAttr('disabled');
                $('.pro_check[data-key=' + key + ']').removeAttr('disabled');
                $('.generate_po[data-key=' + key + ']').removeAttr('disabled');
                var qpid = [];
                $('.pro_check[data-key=' + key + ']:checked').each(function(){
                    qpid.push($(this).data('qp'));
                });
                var output = qpid.join(',');
                $('#form'+key+' input[name=qpid]').val(output);
            }
        });
        $(document).on('click', '.all_check', function(){
            var check = $(this);
            var key = check.data('key');
            $(".pro_check[data-key="+key+"]").prop('checked', check.prop('checked'));
            var count = $('input.pro_check:checkbox:checked').length;
            if(count == 0){
                $('.pro_check').removeAttr("disabled");
                $('.all_check').removeAttr("disabled");
                $('.generate_po').removeAttr("disabled");
                $('#form'+key+' input[name=qpid]').val('');
            }else {
                $('.pro_check').attr("disabled", true);
                $('.all_check').attr("disabled", true);
                $('.generate_po').attr("disabled", true);
                $('.all_check[data-key=' + key + ']').removeAttr('disabled');
                $('.pro_check[data-key=' + key + ']').removeAttr('disabled');
                $('.generate_po[data-key=' + key + ']').removeAttr('disabled');
                var qpid = [];
                $('.pro_check[data-key=' + key + ']:checked').each(function(){
                    qpid.push($(this).data('qp'));
                });
                var output = qpid.join(',');
                $('#form'+key+' input[name=qpid]').val(output);
            }
        });

        $(document).on('click', '.generate_po', function() {
            var btn = $(this);
            var key = btn.data('key');
            var count = $('input.pro_check:checkbox[data-key='+key+']:checked').length;
            if(count){
                $('#form'+key).submit();
            }else{
                alert('Please select rates of this supplier to proceed.');
            }
        });
    </script>
@endpush