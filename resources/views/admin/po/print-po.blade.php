<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
    @media print {
        .noPrint{
            display:none;
        }
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h2>Purchase Order</h2><h3 class="pull-right" style="margin-top: -10px">{{ $po->po_number }}</h3>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Company :</strong><br>
                        <strong>Name :</strong> {{ $company->company_name }}<br>
                        <strong>Contact Person :</strong> {{ $company->contact_person }}<br>
                        <strong>Email :</strong> {{ $company->email }}<br>
                        <strong>Contact No. :</strong> {{ $company->phone }}<br>
                        <strong>Address :</strong> {{ $company->address }}<br>
                        <strong>GST No. :</strong> {{ $company->gst_no }}<br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Supplier :</strong><br>
                        <strong>Name :</strong> {{ (!empty($supplier)) ? $supplier->company_name : '' }}<br>
                        <strong>Contact Person :</strong> {{ (!empty($supplier)) ? $supplier->contact_person : '' }}<br>
                        <strong>Email :</strong> {{ (!empty($supplier)) ? $supplier->email : '' }}<br>
                        <strong>Contact No. :</strong> {{ (!empty($supplier)) ? $supplier->phone : '' }}<br>
                        <strong>Address :</strong> {{ (!empty($supplier)) ? $supplier->address : '' }}<br>
                        <strong>GST No. :</strong> {{ (!empty($supplier)) ? $supplier->gst_no : '' }}<br>
                    </address>
                </div>
                <div class="col-xs-6">
                    <address>
                        <strong>Payment Terms :</strong> {{ $po->payment_terms }}<br>
                    </address>
                </div>
                <div class="col-xs-6">
                    <address class="text-right">
                        <strong>Date:</strong><br>
                        {{ \Carbon\Carbon::parse($po->dated)->format('d M, Y') }}<br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Purchase Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Tax (%)</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                    $htm = '';
                            if(count($tmpData)){
                                $i = 1;
                                $st = 0;
                                $tax = 0;
                                $tAmt = 0;
                                foreach($tmpData as $data){
                                    $total = is_numeric($data->price) ? $data->quantity*$data->price : '0';
                                    ?>
                                    <tr>
                                        <td><?=$i ?></td>
                                        <td><?=get_local_product_name($data->cid) ?></td>
                                        <td><?=get_pbrand_name($data->bid) ?></td>
                                        <td><?=$data->quantity ?></td>
                                        <td><?=get_unit_symbol($data->unit) ?></td>
                                        <td><?=number_format($data->price, 2, '.', '') ?></td>
                                        <td><?=number_format($data->tax, 2, '.', '') ?></td>
                                        <td><?=(number_format($total, 2, '.', '')) ?></td>
                                    </tr>
                                    <?php $tAmt += ((int)$data->price + (((int)$data->price * (int)$data->tax)/100));
                                    $i++;
                                    $st += $total;
                                    $tax += ($data->price*$data->tax/100)*$data->quantity;
                                } ?>
                                <tr>
                                    <td colspan="7" class="text-right"><strong style="margin-right: 65px;">Sub Total</strong></td>
                                    <td><?=number_format($st, 2, '.', '') ?></td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="text-right"><strong style="margin-right: 65px;">Tax Amount (INR)</strong></td>
                                    <td><?=number_format($tax, 2, '.', '') ?></td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="text-right"><strong style="margin-right: 65px;">Total</strong></td>
                                    <td><?=number_format(($st+$tax), 2, '.', '') ?></td>
                                </tr>
                            <?php }else{ ?>
                                <tr><td style="text-align: center" colspan="8">No Records Found.</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-xs-12">
                        <address>
                            <strong>Remark :</strong> {{ $po->remark }}
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button onclick="window.print();" class="noPrint">
        Print
    </button>
</div>
