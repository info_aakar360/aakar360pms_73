@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form action="" id="filter-form">
            <div class="col-md-12">
                <h5 >@lang('app.selectDateRange')</h5>
                <div class="input-daterange input-group" id="date-range">
                    <input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"
                           value=""/>
                    <span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>
                    <input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"
                           value=""/>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <h5 >@lang('app.status')</h5>
                    <select class="form-control" name="status" id="status" data-style="form-control">
                        <option value="all">@lang('modules.indent.all')</option>
                        <option value="0">Pending Purchase</option>
                        <option value="1">Products Received</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <h5 >@lang('app.store')</h5>
                    <select class="form-control select2" name="store" id="store" data-style="form-control">
                        <option value="all">@lang('modules.stores.all')</option>
                        @forelse($stores as $supplier)
                            <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <h5 >@lang('app.projects')</h5>
                    <select class="form-control select2" name="projects" id="project" data-style="form-control">
                        <option value="all">@lang('modules.stores.all')</option>
                        @forelse($projects as $supplier)
                            <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group p-t-10">
                    <label class="control-label col-xs-12">&nbsp;</label>
                    <button type="button" id="apply-filters" class="btn btn-success col-md-6"><i class="fa fa-check"></i> @lang('app.apply')</button>
                    <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        {{--<div class="col-md-3">--}}
            {{--<div class="white-box bg-inverse">--}}
                {{--<h3 class="box-title text-white">@lang('modules.dashboard.totalPos')</h3>--}}
                {{--<ul class="list-inline two-part">--}}
                    {{--<li><i class="icon-user text-white"></i></li>--}}
                    {{--<li class="text-right"><span id="totalWorkingDays" class="counter text-white">{{ $pos }}</span></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-11 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" onclick="exportData()" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                        </div>
                    </div>
                    <div class="col-sm-1 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>
                {{--<div class="row b-b b-t" style="display: none; background: #fbfbfb;" id="ticket-filters">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>--}}
                    {{--</div>--}}
                    {{--<form action="" id="filter-form">--}}
                        {{--<div class="col-md-4">--}}
                            {{--<h5 >@lang('app.selectDateRange')</h5>--}}
                            {{--<div class="input-daterange input-group" id="date-range">--}}
                                {{--<input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"--}}
                                       {{--value=""/>--}}
                                {{--<span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>--}}
                                {{--<input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"--}}
                                       {{--value=""/>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-2">--}}
                            {{--<div class="form-group">--}}
                                {{--<h5 >@lang('app.status')</h5>--}}
                                {{--<select class="form-control" name="status" id="status" data-style="form-control">--}}
                                    {{--<option value="all">@lang('modules.indent.all')</option>--}}
                                    {{--<option value="0">Pending Purchase</option>--}}
                                    {{--<option value="1">Products Received</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<h5 >@lang('app.store')</h5>--}}
                                {{--<select class="form-control select2" name="store" id="store" data-style="form-control">--}}
                                    {{--<option value="all">@lang('modules.stores.all')</option>--}}
                                    {{--@forelse($stores as $supplier)--}}
                                        {{--<option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>--}}
                                    {{--@empty--}}
                                    {{--@endforelse--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<h5 >@lang('app.projects')</h5>--}}
                                {{--<select class="form-control select2" name="projects" id="project" data-style="form-control">--}}
                                    {{--<option value="all">@lang('modules.stores.all')</option>--}}
                                    {{--@forelse($projects as $supplier)--}}
                                        {{--<option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>--}}
                                    {{--@empty--}}
                                    {{--@endforelse--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-2">--}}
                            {{--<div class="form-group p-t-10">--}}
                                {{--<label class="control-label col-xs-12">&nbsp;</label>--}}
                                {{--<button type="button" id="apply-filters" class="btn btn-success col-md-6"><i class="fa fa-check"></i> @lang('app.apply')</button>--}}
                                {{--<button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> @lang('app.reset')</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle footable-loaded" id="users-table">
                        <thead>
                            <tr>
                                <th>@lang('app.sno')</th>
                                <th>@lang('modules.po.po_no')</th>
                                <th>@lang('modules.po.rfq_no')</th>
                                <th>@lang('modules.po.supplier')</th>
                                <th>@lang('modules.po.link')</th>
                                <th>@lang('modules.po.store')</th>
                                <th>@lang('app.status')</th>
                                <th>@lang('app.approve')</th>
                                <th>@lang('app.createdAt')</th>
                                <th>@lang('app.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.po.delete',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this PO?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this PO?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.purchase-order.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                  swal("Updated!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

        });

        function loadTable() {
            var searchQuery = '';
            var startDate = $('#start-date').val();
            if (startDate == '') {
                startDate = null;
            }
            var endDate = $('#end-date').val();
            if (endDate == '') {
                endDate = null;
            }
            var status = $('#status').val();
            var store = $('#store').val();
            var project_id = $('#project').val();

            searchQuery = '?project_id='+project_id+'&startDate=' + startDate + '&endDate=' + endDate + '&store=' + store + '&status=' + status;
            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.purchase-order.data') !!}'+searchQuery,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    {data: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'po_number', name: 'po_number'},
                    {data: 'rfq_id', name: 'rfq_id'},
                    {data: 'company_name', name: 'supplier_id'},
                    {data: 'link', name: 'link'},
                    {data: 'store_id', name: 'store_id'},
                    {data: 'status', name: 'status'},
                    {data: 'approve', name: 'approve'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            })
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        })

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){

            var rfq = $('#rfq').val();
            var status = $('#status').val();

            var url = '{{ route('admin.rfq.export', [':status', ':rfq']) }}';
            url = url.replace(':rfq', rfq);
            url = url.replace(':status', status);

            window.location.href = url;
        }

        $(document).on('click', '.sendEmail', function(){
            var id = $(this).data('id');
            var poid = $(this).data('supplier-id');
            var url = '{{ route('admin.purchase-order.sendEmail', [':id', ':poid']) }}';
            url = url.replace(':id', id);
            url = url.replace(':poid', poid);
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: url,
                type: 'POST',
                data: {_token: token, pid: poid},
                success: function (data) {
                    var msgs = "Mail Sent Successfully";
                    $.showToastr(msgs, 'success');
                }
            });

        });

        {{--'.route('admin.purchase-order.sendEmail', [$row->id, $row->supplier_id]).'--}}
    </script>
@endpush