@extends('layouts.app') @section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection @push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}" />

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}" />
    <style>
        .box {
            width: 20px;
            height: 20px;
            border: 1px #000 solid;
            background: green;
            border-radius: 50%;
            display: inline-block;
        }

        .red {
            background: red;
        }.green {
             background: green;
         }.yellow {
              background: yellow;
          }

    </style>
@endpush @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="tab-content">
                    <div class="tab-pane active" id="general_rules">
                        <div class="steamline">
                            {!! Form::open(['id'=>'rules-container','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <span class="font-light text-muted">Settings</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" id="name" class="form-control" @if(!empty($rulesSettings->name)) value="{{ $rulesSettings->name}}" @endif  >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="description" value="" id="desc" class="form-control" >@if(!empty($rulesSettings->description)) {{$rulesSettings->description}} @endif</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive tableFixHead">
                                    <table class="table table-nowrap mb-0">
                                        <thead >
                                        <tr>
                                            <th>Week</th>
                                            <th>Mon</th>
                                            <th>Tue</th>
                                            <th>Wed</th>
                                            <th>Thu</th>
                                            <th>Fri</th>
                                            <th>Sat</th>
                                            <th>Sun</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <input type="checkbox" name="half_day" id="half_day" value="0"><span>Half Day</span>
                                        <div class="col-md-12">
                                            <hr/>
                                        </div>
                                        <?php $data = array(); ?>
                                        @for($i=1;$i<=5;$i++)
                                            <tr>
                                                <th scope="row">{{$i}}</th>
                                                @for($j=1;$j<=7;$j++)
                                                    <td>
                                                        <div class='box' onclick="changeColor('{{$i.$j}}')" id= "<?php echo $i.$j ?>">
                                                            <input type="hidden" name = "filed_id[]"  value="{{$i.$j}}">
                                                            <input type="hidden" name = "filed_class[]" id="filed_class{{$i.$j}}" class="classval" value="box">
                                                        </div>
                                                    </td>
                                                @endfor
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class='box green'></div> Working Days
                                                </div>
                                            </div><div class="col-md-4">
                                                <div class="form-group">
                                                    <div class='box red'></div> Working Off

                                                </div>
                                            </div><div class="col-md-4">
                                                <div class="form-group">
                                                    <div class='box yellow'></div> Half day
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection @push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

    <script>

        function  changeColor(index) {
            var half_day_enable = $("#half_day").val();
            if(half_day_enable =='1'){
                $('#'+index).toggleClass("yellow");
            }else{
                $('#'+index).toggleClass("red");
            }
            var classval = $('#'+index).attr('class');
            $('#filed_class'+index).val(classval);

        }
        $("#half_day").change(function () {
            if(this.checked) {
                $(this).val('1');
            }else{
                $(this).val('0');
            }
        });
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.leave.storeWorkday')}}',
                container: '#rules-container',
                type: "POST",
                redirect: true,
                data: $('#rules-container').serialize()
            })
        });
    </script>
@endpush