@extends('layouts.app') @section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection @push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}" />

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}" />
@endpush @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

                <div class="tab-content">
                    <div class="tab-pane active" id="general_rules">
                        <div class="steamline">
                            {!! Form::open(['id'=>'rules-container','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <span class="font-light text-muted">General Settings</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" value="" id="desc" class="form-control">This is a default system provided option for all users in cases of low leave balance.</textarea>
                                    </div>
                                </div>
                                </div>
                                <hr>
                                <h3>Leaves Count</h3>
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Leave Allowed In Year </label>
                                            <input type="text" class="form-control" id="leave_allowed_in_year" name="leave_allowed_in_year" value="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Max Leave Allowed In Month</label>
                                            <input type="text" class="form-control" id="leave_allowed_in_month" name="leave_allowed_in_month" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Continue Leave Allowed </label>
                                            <input type="text" class="form-control" id="continue_leave_allowed" name="continue_leave_allowed" value="" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>Leave Count Beetween Holidays </span><input type="checkbox" class="pull-right" id="hollyday_between_leave" name="hollyday_between_leave" value="0" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>Leave Count Between Weekends  </span><input type="checkbox" class="pull-right" id="weekends_between_leave" name="weekends_between_leave" value="0">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h3>Carry Forword</h3>
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>Carry Forward  </span><input type="checkbox" id="enbale_carry_forword" class="pull-right" name="enbale_carry_forword" value="0">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>Backdated Leaves Allowed  </span><input type="checkbox" id="backend_leave_allow" class="pull-right" name="backend_leave_allow" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Max Leave To Carry Forword</label>
                                            <input type="text" class="form-control" name="max_carry_forword" id="max_carry_forword">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span>All Remaining Leaves </span><input type="checkbox" id="all_remaining_leave" class="pull-right" name="all_remaining_leave" value="0">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h3>Miscellaneous</h3>
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label>Backdated Leaves Allowed up to(Days)</label>
                                                <select id="allow_leave_days" name="allow_leave_days" class="form-control">
                                                    @for($i=1;$i<=100;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Allowed under Probation</label>
                                        <select id="allowed_under_probation" name="allowed_under_probation" class="form-control">
                                            <option value="yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="form-actions text-right">
                                    <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                                    <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                </div>
                                {!! Form::close() !!}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection @push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

    <script>
        $(".namefileds").click(function(){
            $('#name').attr('contenteditable','true');
            $('#desc').removeAttr("readonly");
        });

        // when checked and unchecked
        $('#hollyday_between_leave').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#weekends_between_leave').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#enbale_carry_forword').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#backend_leave_allow').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });
        $('#all_remaining_leave').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });
    
        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.leave.storeLeaveRules')}}',
                container: '#rules-container',
                type: "POST",
                redirect: true,
                data: $('#rules-container').serialize(),
                success: function (response) {
                if(response.status == 'success'){
                    window.location.href = '{{ route('admin.leave.leaverules') }}';
                }
            }
            })
        });
    </script>
@endpush