@extends('layouts.app') @section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection @push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/tagify-master/dist/tagify.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}" />

    <link rel="stylesheet" href="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}" />
@endpush @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="tab-content">
                    <div class="tab-pane active" id="general_rules">
                        <div class="steamline">
                            {!! Form::open(['id'=>'rules-container','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <span class="font-light text-muted">General Settings</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" value="{{$leaveRules->leave_rule_name}}" class="form-control" @if($leaveRules->leave_rule_name == "Loss Of Pay") readonly @endif>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" value="" id="desc" class="form-control">{{$leaveRules->leave_rules_descriptionn}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Leave Count Beetween Holidays </span><input type="checkbox" id="hollyday_between_leave" name="hollyday_between_leave" class="pull-right" @if(!empty($leaveRules->hollyday_bet_leaves)) value="{{$leaveRules->hollyday_bet_leaves}}" checked @endif>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Leave Count Between Weekends  </span><input type="checkbox" id="weekends_between_leave" class="pull-right" name="weekends_between_leave" @if(!empty($leaveRules->weekend_bet_leave)) value="{{$leaveRules->weekend_bet_leave}}" checked @endif>
                                    </div>
                                </div>
                            </div>
                            @if($leaveRules->leave_rule_name != "Loss Of Pay")
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Carry Forward  </span><input type="checkbox" id="enbale_carry_forword" class="pull-right" name="enbale_carry_forword" @if(!empty($leaveRules->leave_carrey_forward)) value="{{$leaveRules->leave_carrey_forward}}" checked @endif>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span>Backend Leaves Allowed  </span><input type="checkbox" id="backend_leave_allow" class="pull-right" name="backend_leave_allow" @if(!empty($leaveRules->leave_backdated_allow)) value="{{$leaveRules->leave_backdated_allow}}" checked @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>Backdated Leaves Allowed up to(Days)</label>
                                            <select id="allow_leave_days" name="allow_leave_days" class="form-control">
                                                @for($i=1;$i<=100;$i++)
                                                    <option @if($leaveRules->backdated_allow_upto == $i) value="{{$leaveRules->backdated_allow_upto}}" selected @endif>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Allowed under Probation</label>
                                    <select id="allowed_under_probation" name="allowed_under_probation" class="form-control">
                                        <option @if($leaveRules->allow_probation== "Yes") value="{{$leaveRules->allow_probation}}" selected @endif>Yes</option>
                                        <option @if($leaveRules->allow_probation== "No") value="{{$leaveRules->allow_probation}}" selected @endif>No</option>
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="form-actions text-right">
                                <button type="submit" id="save-form-2" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                            </div>
                            {!! Form::hidden('id', $leaveRules->id) !!}
                            {!! Form::close() !!}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- .row -->

@endsection @push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/tagify-master/dist/tagify.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

    <script>
        $(".namefileds").click(function(){
            $('#name').attr('contenteditable','true');
            $('#desc').removeAttr("readonly");
        });

        // when checked and unchecked
        $('#hollyday_between_leave').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#weekends_between_leave').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#enbale_carry_forword').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });// when checked and unchecked
        $('#backend_leave_allow').click(function() {
            if ($(this).is(":checked") == true) {
                $(this).val('1');
            } else {
                $(this).val('0');
            }
        });

        $('#save-form-2').click(function () {
            $.easyAjax({
                url: '{{route('admin.leave.updateLeaveRules')}}',
                container: '#rules-container',
                type: "POST",
                redirect: true,
                data: $('#rules-container').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.href = '{{ route('admin.leave.leaverules') }}';
                    }
                }
            })
        });
    </script>
@endpush