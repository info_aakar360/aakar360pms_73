<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.leaves.addNewRules')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'addLeaveRules','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Rule Name</label>
                    <input type="text" class="form-control" name="leave_rules" id="leave_rules">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="addRules-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#addRules-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.leave.storeLeaveRules')}}',
            container: '#assignRules',
            type: "POST",
            data: $('#addLeaveRules').serialize(),
            success: function (response) {
                $('#addRulesModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>