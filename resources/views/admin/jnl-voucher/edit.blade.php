@extends('layouts.app')
<?php
$moduleName = " Journal Voucher";
$createItemName = " Edit" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Edit";
$breadcrumbMainIcon = "account_balance_wallet";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\Transaction';
$ParentRouteName = 'admin.jnl_voucher';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Put {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true" class="form-control show-tick"
                                                    name="branch_id">

                                                <option value="0">Select Branch Name ( Dr )</option>
                                                @foreach( App\Branch::all() as $branch )
                                                    <option @if ( $branch->id == $items->dr[0]->branch_id  )
                                                            selected
                                                            @endif value="{{ $branch->id  }}">{{ $branch->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row dr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id"
                                                    name="income_expense_head_id[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Dr )</option>
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    @if ( !empty($items->dr[0]->income_expense_head_id) )
                                                        @php( $id=$items->dr[0]->income_expense_head_id )
                                                    @else
                                                        @php( $id=0 )
                                                    @endif
                                                    <option @if ( $HeadOfAccount->id ==$id )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ (!empty($items->dr[0]->dr )) ? $items->dr[0]->dr : 0  }}"
                                                   name="amount[]" type="number"
                                                   class="form-control amount">
                                            <label class="form-label">Amount ( Dr ) </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row dr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id"
                                                    name="income_expense_head_id[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Dr )</option>
                                                @if ( !empty( $items->dr[1]->income_expense_head_id) )
                                                    @php( $id =$items->dr[1]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif

                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id  )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ (!empty($items->dr[1]->dr)) ? $items->dr[1]->dr : 0 }}"
                                                   name="amount[]" type="number"
                                                   class="form-control amount">
                                            <label class="form-label">Amount ( Dr ) </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row dr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id"
                                                    name="income_expense_head_id[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Dr )</option>
                                                @if ( !empty($items->dr[2]->income_expense_head_id) )
                                                    @php( $id =$items->dr[2]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id ==$id )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{  !empty($items->dr[2]->dr ) ? $items->dr[2]->dr : 0  }}"
                                                   name="amount[]" type="number"
                                                   class="form-control amount">
                                            <label class="form-label">Amount ( Dr )</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row dr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id"
                                                    name="income_expense_head_id[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Dr )</option>
                                                @if ( !empty($items->dr[3]->income_expense_head_id) )
                                                    @php( $id =$items->dr[3]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id ==$id )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{  !empty($items->dr[3]->dr ) ? $items->dr[3]->dr : 0  }}"
                                                   name="amount[]" type="number"
                                                   class="form-control amount">
                                            <label class="form-label">Amount ( Dr )</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row dr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id"
                                                    name="income_expense_head_id[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Dr )</option>
                                                @if ( !empty($items->dr[4]->income_expense_head_id) )
                                                    @php( $id =$items->dr[4]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ !empty($items->dr[4]->dr ) ? $items->dr[4]->dr : 0  }}"
                                                   name="amount[]" type="number"
                                                   class="form-control amount">
                                            <label class="form-label">Amount ( Dr )</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true" class="form-control show-tick"
                                                    name="branch_id_cr">
                                                <option value="0">Select Branch Name ( Cr )</option>
                                                @foreach( App\Branch::all() as $branch )
                                                    <option @if ( $branch->id ==  $items->cr[0]->branch_id )
                                                            selected
                                                            @endif value="{{ $branch->id  }}">{{ $branch->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row cr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>

                                                @if ( !empty( $items->cr[0]->income_expense_head_id) )
                                                    @php( $id =$items->cr[0]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif

                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id  )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ $items->cr[0]->cr   }}" name="amount_cr[]"
                                                   type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr ) </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row cr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>

                                                @if ( !empty( $items->cr[1]->income_expense_head_id) )
                                                    @php( $id =$items->cr[1]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id ))
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ !empty($items->cr[1]->cr ) ? $items->cr[1]->cr : 0   }}"
                                                   name="amount_cr[]" type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr ) </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row cr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>
                                                @if ( !empty( $items->cr[2]->income_expense_head_id) )
                                                    @php( $id =$items->cr[2]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif

                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id  )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ !empty( $items->cr[2]->cr ) ? $items->cr[2]->cr : 0  }}"
                                                   name="amount_cr[]" type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr )</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row cr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>

                                                @if ( !empty( $items->cr[3]->income_expense_head_id) )
                                                    @php( $id = $items->cr[3]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif

                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id  )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ !empty( $items->cr[3]->cr ) ? $items->cr[3]->cr : 0  }}"
                                                   name="amount_cr[]" type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr )</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row cr">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>
                                                @if ( !empty( $items->cr[4]->income_expense_head_id) )
                                                    @php( $id = $items->cr[4]->income_expense_head_id )
                                                @else
                                                    @php( $id=0 )
                                                @endif
                                                @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                                                    <option @if ( $HeadOfAccount->id == $id  )
                                                            selected
                                                            @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input value="{{ !empty($items->cr[4]->cr ) ? $items->cr[4]->cr : 0  }}"
                                                   name="amount_cr[]" type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr )</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-minus-circle btn btn-sm btn-danger minus"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line" id="bs_datepicker_container">
                                            <input autocomplete="off"
                                                   value="{{ date("m-d-Y", strtotime($items->dr[0]->date))  }}"
                                                   name="voucher_date"
                                                   type="text"
                                                   class="form-control"
                                                   placeholder="Please choose a voucher date...">

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                                    <textarea name="particulars" rows="2" class="form-control no-resize"
                                                              placeholder="Particulars">{{  $items->dr[0]->particulars }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" id="save-form" class="btn btn-primary m-t-15 waves-effect">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.update',['id'=>$items->dr[0]->voucher_no]) }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });

        var $i = 0;
        // Add More Inputs
        $('.plus').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            //append Options
            $('<div class="row" id="drRow'+indexs+'">'
                +'  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">'
                +'      <div class="form-group form-float">'
                +'          <div class="form-line">'
                +'              <select data-live-search="true" class="form-control show-tick income_expense_head_id" name="income_expense_head_id[]" id="">'
                +'                  <option value="0"> Select Head of Account Name</option>'
                    @if (App\IncomeExpenseHead::all()->count() >0 )
                    @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                +'                          <option @if ( $HeadOfAccount->id == old('head_of_account_id' )) selected @endif value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>'
                    @endforeach
                    @endif
                +'              </select>'
                +'          </div>'
                +'      </div>'
                +'  </div>'
                +'  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">'
                +'      <div class="form-group form-float">'
                +'          <div class="form-line">'
                +'              <input name="amount[]" type="number" class="form-control amount" placeholder="Amount">'
                +'          </div>'
                +'      </div>'
                +'  </div>'
                +'  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">'
                +'      <div class="form-group form-float">'
                +'          <a href="javascript:;" class="btn btn-sm btn-danger minus" onclick="removeBox('+indexs+');"><i class="fa fa-minus-circle"></i> </a>'
                +'      </div>'
                +'  </div>'
                +'</div>').appendTo('#billData');
        });
        function removeBox(index){
            $('#drRow'+index).remove();
        }
    </script>
    <script>
        @if(Session::has('success'))
            toastr["success"]('{{ Session::get('success') }}');
        @endif

                @if(Session::has('error'))
            toastr["error"]('{{ Session::get('error') }}');
        @endif

                @if ($errors->any())
                @foreach ($errors->all() as $error)
            toastr["error"]('{{ $error }}');
        @endforeach
        @endif

        // Validation and calculation on Cr Voucher
        var UiController = (function () {
            var DOMString = {
                submit_form: 'form.form',
                field_area: '.field_area',
                project_id: 'select[name=branch_id]',
                project_id_cr: 'select[name=branch_id_cr]',
                head_of_account_id: '.income_expense_head_id',
                amount: '.amount',
                head_of_account_id_cr: '.income_expense_head_id_cr',
                amount_cr: '.amount_cr',
                date: 'input[name=voucher_date]',
                particulars: 'textarea[name=particulars]',
                drCloset: '.dr',
                crCloset: '.cr',
                dr: 'dr',
                cr: 'cr',
                plus: 'plus',
                minus: 'minus',
            };

            return {
                getDOMString: function () {
                    return DOMString;
                },
                getFields: function () {
                    return {
                        get_form: document.querySelector(DOMString.submit_form),
                        get_project_id: document.querySelector(DOMString.project_id),
                        get_project_id_cr: document.querySelector(DOMString.project_id_cr),
                        get_head_of_account_id: document.querySelectorAll(DOMString.head_of_account_id),
                        get_amount: document.querySelectorAll(DOMString.amount),
                        get_head_of_account_id_cr: document.querySelectorAll(DOMString.head_of_account_id_cr),
                        get_amount_cr: document.querySelectorAll(DOMString.amount_cr),
                        get_date: document.querySelector(DOMString.date),
                        get_particulars: document.querySelector(DOMString.particulars),
                        get_dr: document.getElementsByClassName(DOMString.dr),
                        get_cr: document.getElementsByClassName(DOMString.cr),
                        get_plus: document.getElementsByClassName(DOMString.plus),
                        get_minus: document.getElementsByClassName(DOMString.minus),
                    }
                },
                getValues: function () {
                    var Fields = this.getFields();
                    return {
                        project_id: Fields.get_project_id.value == "" ? 0 : parseFloat(Fields.get_project_id.value),
                        project_id_cr: Fields.get_project_id_cr.value == "" ? 0 : parseFloat(Fields.get_project_id_cr.value),
                        date: Fields.get_date.value == "" ? 0 : Fields.get_date.value,
                        particulars: Fields.get_particulars.value == "" ? 0 : Fields.get_particulars.value,

                    }
                },

                hide: function (Field) {
                    var DomString = this.getDOMString();
                    var Area = Field.closest(DomString.field_area);

                    if (Area) {
                        Area.style.display = 'none';
                    }
                },
                show: function (Field) {
                    var DomString = this.getDOMString();
                    var Area = Field.closest(DomString.field_area);
                    if (Area) {
                        Area.style.display = 'block';
                    }
                },

                setEmpty: function(Field){
                    Field.value = 0;
                },

                hideHeadAmountArea: function (Field) {
                    if (Field) {
                        Field.style.display = 'none';
                    }
                },
                showHeadAmountArea: function (Field) {
                    var DomString = this.getDOMString();
                    if (Field) {
                        Field.style.display = 'block';
                    }
                },
            }
        })();

        var MainController = (function (UICnt) {
            var DOMString = UICnt.getDOMString();
            var Fields = UICnt.getFields();
            var setUpEventListner = function () {
                Fields.get_form.addEventListener('submit', validation);
                Array.prototype.forEach.call(Fields.get_plus, function (plus, index) {
                    plus.addEventListener('click', function (event) {
                        addItem(event, index);
                    }, false);
                });
                Array.prototype.forEach.call(Fields.get_minus, function (minus, index) {
                    minus.addEventListener('click', function (event) {
                        removeItem(event, index);
                    }, false);
                });
            };

            var validation = function (e) {
                var Values, Fields;
                Values = UICnt.getValues();
                Fields = UICnt.getFields();
                var TotalDrAmount = 0, TotalCrAmount = 0;
                if (Values.project_id == 0) {
                    toastr["error"]('Select  Branch ( Dr )');
                    e.preventDefault();
                }
                if (Values.project_id_cr == 0) {
                    toastr["error"]('Select  Branch ( Cr )');
                    e.preventDefault();
                }
                if (Fields.get_head_of_account_id[0].querySelector('select').value == 0) {
                    toastr["error"]('Select Head Of Account ( Dr )');
                    e.preventDefault();
                }
                if (Fields.get_amount[0].value == '') {
                    toastr["error"]('Put Amount ( Dr )');
                    e.preventDefault();
                } else {
                    TotalDrAmount += parseFloat(Fields.get_amount[0].value);
                }
                if (Fields.get_head_of_account_id_cr[0].querySelector('select').value == 0) {
                    toastr["error"]('Select Head Of Account ( Cr )');
                    e.preventDefault();
                }
                if (Fields.get_amount_cr[0].value == '') {
                    toastr["error"]('Put Amount ( Cr )');
                    e.preventDefault();
                } else {
                    TotalCrAmount += parseFloat(Fields.get_amount_cr[0].value);
                }
                if (Values.date == 0) {
                    toastr["error"]('Set Date');
                    e.preventDefault();
                }
                if (Fields.get_dr[1].style.display == 'block') {
                    if (Fields.get_head_of_account_id[2].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Dr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount[1].value == '') {
                        toastr["error"]('Put Amount ( Dr )');
                        e.preventDefault();
                    } else {
                        TotalDrAmount += parseFloat(Fields.get_amount[1].value);
                    }
                }

                if (Fields.get_dr[2].style.display == 'block') {
                    if (Fields.get_head_of_account_id[4].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Dr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount[2].value == '') {
                        toastr["error"]('Put Amount ( Dr )');
                        e.preventDefault();
                    } else {
                        TotalDrAmount += parseFloat(Fields.get_amount[2].value);
                    }
                }

                if (Fields.get_dr[3].style.display == 'block') {
                    if (Fields.get_head_of_account_id[6].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Dr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount[3].value == '') {
                        toastr["error"]('Put Amount ( Dr )');
                        e.preventDefault();
                    } else {
                        TotalDrAmount += parseFloat(Fields.get_amount[3].value);
                    }
                }

                if (Fields.get_dr[4].style.display == 'block') {

                    if (Fields.get_head_of_account_id[8].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Dr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount[4].value == '') {
                        toastr["error"]('Put Amount ( Dr )');
                        e.preventDefault();
                    } else {
                        TotalDrAmount += parseFloat(Fields.get_amount[4].value);
                    }
                }

                if (Fields.get_cr[1].style.display == 'block') {
                    if (Fields.get_head_of_account_id_cr[2].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Cr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount_cr[1].value == '') {
                        toastr["error"]('Put Amount ( Cr)');
                        e.preventDefault();
                    } else {
                        TotalCrAmount += parseFloat(Fields.get_amount_cr[1].value);
                    }
                }

                if (Fields.get_cr[2].style.display == 'block') {
                    if (Fields.get_head_of_account_id_cr[4].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Cr ) ');
                        e.preventDefault();
                    }
                    if (Fields.get_amount_cr[2].value == '') {
                        toastr["error"]('Put Amount ( Cr )');
                        e.preventDefault();
                    } else {
                        TotalCrAmount += parseFloat(Fields.get_amount_cr[2].value);
                    }
                }

                if (Fields.get_cr[3].style.display == 'block') {
                    if (Fields.get_head_of_account_id_cr[6].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Cr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount_cr[3].value == '') {
                        toastr["error"]('Put Amount ( Cr )');
                        e.preventDefault();
                    } else {
                        TotalCrAmount += parseFloat(Fields.get_amount_cr[3].value);
                    }
                }

                if (Fields.get_cr[4].style.display == 'block') {
                    if (Fields.get_head_of_account_id_cr[8].querySelector('select').value == 0) {
                        toastr["error"]('Select Head Of Account ( Cr )');
                        e.preventDefault();
                    }
                    if (Fields.get_amount_cr[4].value == '') {
                        toastr["error"]('Put Amount ( Cr )');
                        e.preventDefault();
                    } else {
                        TotalCrAmount += parseFloat(Fields.get_amount_cr[4].value);
                    }
                }

                var head_of_account_Ids = [];
                var get_head_of_account_id_one = Fields.get_head_of_account_id[0].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id[0].querySelector('select').value);
                var get_head_of_account_id_two = Fields.get_head_of_account_id[2].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id[2].querySelector('select').value);
                var get_head_of_account_id_three = Fields.get_head_of_account_id[4].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id[4].querySelector('select').value);
                var get_head_of_account_id_four = Fields.get_head_of_account_id[6].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id[6].querySelector('select').value);
                var get_head_of_account_id_five = Fields.get_head_of_account_id[8].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id[8].querySelector('select').value);
                var get_head_of_account_id_cr_one = Fields.get_head_of_account_id_cr[0].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id_cr[0].querySelector('select').value);
                var get_head_of_account_id_cr_two = Fields.get_head_of_account_id_cr[2].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id_cr[2].querySelector('select').value);
                var get_head_of_account_id_cr_three = Fields.get_head_of_account_id_cr[4].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id_cr[4].querySelector('select').value);
                var get_head_of_account_id_cr_four = Fields.get_head_of_account_id_cr[6].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id_cr[6].querySelector('select').value);
                var get_head_of_account_id_cr_five = Fields.get_head_of_account_id_cr[8].querySelector('select').value == "" ? 0 : parseFloat(Fields.get_head_of_account_id_cr[8].querySelector('select').value);
                if (get_head_of_account_id_one > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_one);
                }
                if (get_head_of_account_id_two > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_two);
                }
                if (get_head_of_account_id_three > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_three);
                }
                if (get_head_of_account_id_four > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_four);
                }
                if (get_head_of_account_id_five > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_five);
                }



                if (get_head_of_account_id_cr_one > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_cr_one);
                }
                if (get_head_of_account_id_cr_two > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_cr_two);
                }
                if (get_head_of_account_id_cr_three > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_cr_three);
                }
                if (get_head_of_account_id_cr_four > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_cr_four);
                }
                if (get_head_of_account_id_cr_five > 0) {
                    head_of_account_Ids.push(get_head_of_account_id_cr_five);
                }

                function checkUniqueOrNot(head_of_account_Ids) {
                    var counts = [];
                    for (var i = 0; i <= head_of_account_Ids.length; i++) {
                        if (counts[head_of_account_Ids[i]] === undefined) {
                            counts[head_of_account_Ids[i]] = 1;
                        } else {
                            return true;
                        }
                    }
                    return false;
                }
                if (checkUniqueOrNot(head_of_account_Ids)) {
                    toastr["error"]('Head of Account should unique');
                    e.preventDefault();
                }
                if (TotalDrAmount === TotalCrAmount) {
                } else {
                    toastr["error"]('Total Amount of Dr and Cr Should same');
                    e.preventDefault();
                }
            };

            var addItem = function (event, index) {
                var Fields;
                Fields = UICnt.getFields();
                var DomString = UICnt.getDOMString();
                var Cr = event.target.closest(DomString.crCloset);
                if (Cr) {
                    Cr.nextElementSibling.style.display = 'block';
                } else {
                    UICnt.showHeadAmountArea(Fields.get_dr[index + 1]);
                }
            };

            var removeItem = function (event, index) {
                var Fields;
                Fields = UICnt.getFields();
                var DomString = UICnt.getDOMString();
                var Cr = event.target.closest(DomString.crCloset);
                var Dr = event.target.closest(DomString.drCloset);
                if (Cr) {
                    Cr.style.display = 'none';
                    Cr.querySelector('select').value= '0';
                    Cr.querySelector(DomString.amount_cr).value = "";
                } else {
                    UICnt.hideHeadAmountArea(Fields.get_dr[index + 1]);
                    Dr.querySelector('select').value= '0';
                    Dr.querySelector(DomString.amount).value = "";
                }
            };

            return {
                init: function () {
                    console.log("App Is running");
                    setUpEventListner();
                    var Values, Fields;
                    Values = UICnt.getValues();
                    Fields = UICnt.getFields();
                    @if( !empty($items->dr[1]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_dr[1]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_dr[1]);
                    @endif
                    @if( !empty($items->dr[2]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_dr[2]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_dr[2]);
                    @endif
                    @if( !empty($items->dr[3]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_dr[3]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_dr[3]);
                    @endif
                    @if( !empty($items->dr[4]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_dr[4]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_dr[4]);
                    @endif
                    @if( !empty($items->cr[1]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_cr[1]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_cr[1]);
                    @endif

                    @if( !empty($items->cr[2]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_cr[2]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_cr[2]);
                    @endif

                    @if( !empty($items->cr[3]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_cr[3]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_cr[3]);
                    @endif

                    @if( !empty($items->cr[4]->income_expense_head_id) )
                        UICnt.showHeadAmountArea(Fields.get_cr[4]);
                    @else
                        UICnt.hideHeadAmountArea(Fields.get_cr[4]);
                    @endif
                }
            }
        })(UiController);
        MainController.init();
    </script>
@endpush