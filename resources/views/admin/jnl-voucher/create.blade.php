@extends('layouts.app')
<?php
$moduleName = " Journal Voucher";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Create";
$breadcrumbMainIcon = "account_balance_wallet";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\Transaction';
$ParentRouteName = 'admin.jnl_voucher';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}

                            <div id="drrow">
                                <div  class="row" >
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick income_expense_head_id"
                                                        name="ledger_id[]"
                                                        id="">
                                                    <option value=""> Select Ledger ( Dr )</option>
                                                        @foreach( $ledgerdetails as $HeadOfAccount )
                                                            <option value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="amount[]" type="number"
                                                       class="form-control amount">
                                                <label class="form-label">Amount ( Dr ) </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                        <div class="form-group form-float">
                                            <i class="fa fa-plus-circle btn btn-sm btn-success drplus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="crrow">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select data-live-search="true"
                                                    class="form-control show-tick income_expense_head_id_cr"
                                                    name="income_expense_head_id_cr[]"
                                                    id="">
                                                <option value="0"> Select Head of Account Name ( Cr )</option>
                                                @foreach( $ledgerdetails as $HeadOfAccount )
                                                    <option value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="amount_cr[]"
                                                   type="number"
                                                   class="form-control amount_cr">
                                            <label class="form-label">Amount ( Cr ) </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">
                                    <div class="form-group form-float">
                                        <i class="fa fa-plus-circle btn btn-sm btn-success plus"></i>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line" id="bs_datepicker_container">
                                            <input autocomplete="off" value="{{ old('voucher_date') }}"
                                                   name="voucher_date"
                                                   type="text" id="start_date"
                                                   class="form-control"
                                                   placeholder="Please choose a date...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                                    <textarea name="particulars" rows="2" class="form-control no-resize"
                                                              placeholder="Particulars">{{ old('particulars')  }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" id="save-form" class="btn btn-primary m-t-15 waves-effect">
                                            Create
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $("#start_date").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.store') }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });

        var $i = 0;
        // Add More Inputs
        $('.drplus').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            //append Options
            $('<div  class="row" >\n' +
                '                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">\n' +
                '                <div class="form-group form-float">\n' +
                '                <div class="form-line">\n' +
                '                <select data-live-search="true" class="form-control show-tick income_expense_head_id"\n' +
                '            name="ledger_id[]"\n' +
                '            id="">\n' +
                '                <option value=""> Select Ledger ( Dr )</option>\n' +
                '            @foreach( $ledgerdetails as $HeadOfAccount )\n' +
                '            <option value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>\n' +
                '                    @endforeach\n' +
                '                </select>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">\n' +
                '                <div class="form-group form-float">\n' +
                '                <div class="form-line">\n' +
                '                <input name="amount[]" type="number"\n' +
                '        class="form-control amount">\n' +
                '                <label class="form-label">Amount ( Dr ) </label>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">\n' +
                '                <div class="form-group form-float">\n' +
                '                <i class="fa fa-minus-circle btn btn-sm btn-danger drminus"></i>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '                </div>').appendTo('#drplus');
        });
        function removeBox(index){
            $('#drRow'+index).remove();
        }

        $('.pluscr').click(function(){
            $i = $i+1;
            var indexs = $i+1;
            //append Options
            $('<div class="row" id="crRow'+indexs+'">'
                +'  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 field_area">'
                +'      <div class="form-group form-float">'
                +'          <div class="form-line">'
                +'              <select data-live-search="true" class="form-control show-tick income_expense_head_id selectpicker" name="income_expense_head_id_cr[]" id="">'
                +'                  <option value="0"> Select Head of Account Name (Cr)</option>'
                                    @if (App\IncomeExpenseHead::all()->count() >0 )
                                        @foreach( App\IncomeExpenseHead::all() as $HeadOfAccount )
                +'                          <option value="{{ $HeadOfAccount->id  }}">{{ $HeadOfAccount->name  }}</option>'
                                        @endforeach
                                    @endif
                +'              </select>'
                +'          </div>'
                +'      </div>'
                +'  </div>'
                +'  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 field_area">'
                +'      <div class="form-group form-float">'
                +'          <div class="form-line">'
                +'              <input name="amount_cr[]" type="number" class="form-control amount" placeholder="Amount (Cr)">'
                +'          </div>'
                +'      </div>'
                +'  </div>'
                +'  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 field_area">'
                +'      <div class="form-group form-float">'
                +'          <a href="javascript:;" class="btn btn-sm btn-danger minus" onclick="removeBoxCr('+indexs+');"><i class="fa fa-minus-circle"></i> </a>'
                +'      </div>'
                +'  </div>'
                +'</div>').appendTo('#crData');
        });
        function removeBoxCr(index){
            $('#crRow'+index).remove();
        }
    </script>
    <script>

        drplus

    </script>
@endpush