<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Template</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqtemplate','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $boqtemplate->name }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.category')</label>
                        <select class="select2 form-control" data-placeholder="@lang("app.category")"  id="category" name="category">
                            <option value="">Select Category</option>
                            @foreach($projectcategoryarray as $projectcategory)
                                <option  value="{{ $projectcategory->id }}" @if($projectcategory->id==$boqtemplate->category) selected @endif >{{ ucwords($projectcategory->category_name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.description')</label>
                        <textarea class="form-control" name="description" required placeholder="Enter description" >{{ $boqtemplate->description }}</textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-type" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-type').click(function () {
        $.easyAjax({
            url: '{{route('admin.boq-template.update', [$boqtemplate->id])}}',
            container: '#editBoqtemplate',
            type: "PATCH",
            data: $('#editBoqtemplate').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>