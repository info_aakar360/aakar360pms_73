@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #042c74"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title m-t-30"> @lang('app.category') </h5>
                <select class="select2 form-control" name="categorytype" data-placeholder="@lang('app.selectProject')" id="categorytype">
                    <option value="">@lang('app.category')</option>
                    @foreach($projectcategoryarray as $projectcategory)
                        <option value="{{ $projectcategory->id }}">{{ ucwords($projectcategory->category_name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <h5 class="box-title m-t-30"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <a  data-toggle="modal" data-target="#createBoqtemplate" class="btn btn-outline btn-success btn-sm">Add @lang('app.menu.boqtemplate') <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                            class="fa fa-cog"></i></a>
            </div>
       </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.title')</th>
                            <th>@lang('app.category')</th>
                            <th>@lang('app.description')</th>
                            <th>@lang('app.boq')</th>
                            <th>@lang('app.addedby')</th>
                            <th>@lang('app.createdAt')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <!-- Start Modal add sub location location -->
    <div class="modal" id="createBoqtemplate">
        {!! Form::open(['id'=>'createTemplateForm','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Template</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <div class="form-group">
                        <label class="control-label">@lang('app.name')</label>
                        <input type="text" class="form-control" name="name" required placeholder="Enter Name"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.category')</label>
                        <select class="select2 form-control" data-placeholder="@lang("app.category")" required  id="category" name="category">
                            <option value="">Select Category</option>
                            @foreach($projectcategoryarray as $projectcategory)
                                <option  value="{{ $projectcategory->id }}"  >{{ ucwords($projectcategory->category_name) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">@lang('app.description')</label>
                        <textarea class="form-control" name="description" required placeholder="Enter description" ></textarea>
                    </div>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>


            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- End Modal sub add location -->

    <!-- Start Modal edit sub location location -->
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- End Modal sub edit location -->





@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>
<script>
    loadTable();
    function loadTable(){

        var category = $('#categorytype').val();

        table = $('#users-table').dataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            destroy: true,
            stateSave: true,
            ajax: '{!! route('admin.boq-template.data') !!}?category=' + category,
            language: {
                "url": "<?php echo __("app.datatable") ?>"
            },
            "fnDrawCallback": function( oSettings ) {
                $("body").tooltip({
                    selector: '[data-toggle="tooltip"]'
                });
            },
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'category', name: 'category' },
                { data: 'description', name: 'description'},
                { data: 'boqlist', name: 'boqlist'},
                { data: 'added_by', name: 'addedby'},
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action', width: '15%' }
            ]
        });
    }

    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
    });

    $('#filter-results').click(function () {
        loadTable();
    });

    $('#reset-filters').click(function () {
        $('#filter-form')[0].reset();
        loadTable();
    })

    $('#createBoqtemplate').submit(function () {
        $.easyAjax({
            url: '{{route('admin.boq-template.store')}}',
            container: '#createBoqtemplate',
            type: "POST",
            data: $('#createTemplateForm').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#createTemplateForm')[0].reset();
                         loadTable();
                }
            }
        });
        return false;
    });

    $(document).on('click','.editBoqtemplate',function(){
        var id = $(this).data('boqtemplate-id');
        var url = '{{ route('admin.boq-template.edit',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Boqtemplate");
        $.ajaxModal('#taskCategoryModal', url);
        $('#category_id').selectpicker('refresh')
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('boqtemplate-id');
        var recurring = $(this).data('recurring');

        var buttons = {
            cancel: "No, cancel please!",
            confirm: {
                text: "Yes, delete it!",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted Boqtemplate item!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons
        }).then(function (isConfirm) {
            if (isConfirm == 'confirm') {

                var url = "{{ route('admin.boq-template.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        loadTable();
                    }
                });
            }
        });
    });
    $('#projectid').change(function () {
        var id = $(this).val();
        var token = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ route('admin.projects.projecttitles') }}",
            data: {'_token': token,'projectid': id},
            success: function(data){
                var projectdata = JSON.parse(data);
                var titles = '<option value="">Select Title</option>';
                if(projectdata.titles){
                    $(".projecttitle").removeClass("hide");
                    $.each( projectdata.titles, function( key, value ) {
                        titles += '<option value="'+key+'">'+value+'</option>';
                    });
                }
                $("select#titlelist").html("");
                $("select#titlelist").html(titles);
                $('select#titlelist').select2();
            }
        });
    });
</script>
@endpush