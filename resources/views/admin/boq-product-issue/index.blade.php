@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush

@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form action="" id="filter-form">
            {{--<div class="col-md-12">--}}
                {{--<h5 >@lang('app.selectDateRange')</h5>--}}
                {{--<div class="input-daterange input-group" id="date-range">--}}
                    {{--<input type="text" class="form-control" id="start-date" placeholder="@lang('app.startDate')"--}}
                           {{--value=""/>--}}
                    {{--<span class="input-group-addon bg-info b-0 text-white">@lang('app.to')</span>--}}
                    {{--<input type="text" class="form-control" id="end-date" placeholder="@lang('app.endDate')"--}}
                           {{--value=""/>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        @lang('modules.manpowerLogs.selectProject')
                    </label>
                    <select class="select2 form-control" name="project_id" data-placeholder="@lang('modules.manpowerLogs.selectProject')"  id="project_id">
                        <option value="0">Please Select Project</option>
                        @foreach($projects as $project)
                            <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        Store
                    </label>
                    <select class="select2 form-control" name="store_id" id="store_id" data-placeholder="Select Store">
                        <option value="0">Please select store</option>
                        @foreach($stores as $store)
                            <option value="{{ $store->id }}">{{ ucwords($store->company_name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('modules.manpowerLogs.subproject')</label>
                    <select class="form-control" name="sub_project_id" id="sub_project_id" data-style="form-control">
                        <option value="0">Please select sub project</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('app.segment')</label>
                    <select class="form-control" name="segment_id" id="segment_id" data-style="form-control">
                        <option value="0">Please select segment</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>@lang('modules.manpowerLogs.projecttask')</label>
                    <select class="form-control" name="task_id" id="task_id" data-style="form-control">
                        <option value="0">Please select task</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group p-t-10">
                    <label class="control-label col-xs-12">&nbsp;</label>
                    <button type="button" id="apply-filters" class="btn btn-success col-md-6"><i class="fa fa-check"></i> @lang('app.apply')</button>
                    <button type="button" id="reset-filters" class="btn btn-inverse col-md-5 col-md-offset-1"><i class="fa fa-refresh"></i> @lang('app.reset')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        {{--<div class="col-md-3">--}}
        {{--<div class="white-box bg-inverse">--}}
        {{--<h3 class="box-title text-white">@lang('modules.dashboard.totalIndents')</h3>--}}
        {{--<ul class="list-inline two-part">--}}
        {{--<li><i class="icon-user text-white"></i></li>--}}
        {{--<li class="text-right"><span id="totalWorkingDays" class="counter text-white">{{ $totalIndents }}</span></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <a href="{{ route('admin.inventory.boqProductIssue') }}" class="btn btn-outline btn-success btn-sm">Issue Product <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" onclick="exportData()" class="btn btn-info btn-sm"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>
                        </div>
                    </div>
                    <div class="col-sm-1 text-right hidden-xs">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i  class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover1 toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Task Name</th>
                            <th>Product Issue Id</th>
                            <th>Total Quantity</th>
                            <th>Issued By</th>
                            <th>Issued Date</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($issuedProducts as $key=>$product)--}}
                            {{--<?php $qcount = \App\ProductIssue::where('unique_id',$product->unique_id)->get(); ?>--}}
                            {{--<tr>--}}
                                {{--<td>{{ $key+1 }}</td>--}}
                                {{--<td>{{ get_cost_name($product->task_id) }}</td>--}}
                                {{--<td>{{ $product->unique_id }}</td>--}}
                                {{--<td>{{ count($qcount) }}</td>--}}
                                {{--<td>{{ get_user_name($product->issued_by) }}</td>--}}
                                {{--<td>{{ \Carbon\Carbon::parse($product->created_at)->format('d-m-Y') }}</td>--}}
                                {{--<td>--}}
                                    {{--<a href="{{ route('admin.product-issue.issuedProductDetail',[$product->unique_id])}}" class="btn btn-success btn-circle"--}}
                                       {{--data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="View Issued Products">--}}
                                        {{--<i class="fa fa-eye" aria-hidden="true"></i>--}}
                                    {{--</a>--}}
                                    {{--<a href="{{ route('admin.product-issue.return',[$product->unique_id])}}" class="btn btn-warning btn-circle"--}}
                                       {{--data-toggle="tooltip" data-task-id="{{ $product->unique_id }}" data-original-title="Return Issued Products">--}}
                                        {{--<i class="fa fa-minus" aria-hidden="true"></i>--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('#date-range').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var table;
        $(function() {
            loadTable();
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.indent.destroy',':id') }}";
                        url = url.replace(':id', id);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });
            $('body').on('click', '.approve-btn', function(){
                var val = $(this).data('key');
                var id = $(this).data('id');
                var title = 'Are you sure to Approve this Indent?';
                if(parseInt(val) == 0){
                    title = 'Are you sure to Refuse this Indent?';
                }
                swal({
                    title: title,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        var url = "{{ route('admin.indent.approve',[':id', ':key']) }}";
                        url = url.replace(':id', id);
                        url = url.replace(':key', val);
                        var token = "{{ csrf_token() }}";
                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'POST'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                  swal("Updated!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });

        });

        function loadTable() {
            var searchQuery = '';

            var store = $('#store_id').val();
            var project_id = $('#project_id').val();
            var sub_project_id = $('#sub_project_id').val();
            var segment_id = $('#segment_id').val();
            var task_id = $('#task_id').val();

            searchQuery = '?project_id='+project_id+'&store=' + store + '&sub_project_id=' + sub_project_id+ '&segment_id=' + segment_id+ '&task_id=' + task_id;
            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.inventory.boqProductIssueData') !!}'+searchQuery,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    {data: 'task_id', name: 'task_id'},
                    {data: 'unique_id', name: 'unique_id'},
                    {data: 'quantity', name: 'quantity'},
                    {data: 'issued_by', name: 'issued_by'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'remark', name: 'remark'},
                    {data: 'action', name: 'action'}
                ]
            })
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
        });

        $('#apply-filters').click(function () {
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
        })

        function exportData(){
            var indent = $('#indent').val();
            var status = $('#status').val();
            var url = '{{ route('admin.indent.export', [':status', ':indent']) }}';
            url = url.replace(':indent', indent);
            url = url.replace(':status', status);
            window.location.href = url;
        }
        $('#project_id').change(function () {
            var project = $(this).val();
            if(project){
                $(".projecttitle").addClass("hide");
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.inventory.boqGetTitlesByProjects') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        var projectdata = JSON.parse(data);
                        var titles = '<option value="0">Select Subproject</option>';
                        var cositem = '<option value="0">Select Task</option>';
                        var segments = '<option value="0">Select Segment</option>';
                        if(projectdata.titles){
                            $(".projecttitle").removeClass("hide");
                            $.each( projectdata.titles, function( key, value ) {
                                titles += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#sub_project_id").html("");
                        $("select#sub_project_id").html(titles);
                        $('select#sub_project_id').select2();
                        if(projectdata.cositems){
                            $.each( projectdata.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#task_id").html("");
                        $("select#task_id").html(cositem);
                        $('select#task_id').select2();
                        if(projectdata.segments){
                            $.each( projectdata.segments, function( key, value ) {
                                segments += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#segment_id").html("");
                        $("select#segment_id").html(segments);
                        $('select#segment_id').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#project_id").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.inventory.boqGetCostitemByTitle') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        var titles = JSON.parse(data);
                        var cositem = '<option value="0">Select Task</option>';
                        var segments = '<option value="0">Select Segment</option>';
                        if(titles.cositems){
                            $.each( titles.cositems, function( key, value ) {
                                cositem += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#task_id").html("");
                        $("select#task_id").html(cositem);
                        $('select#task_id').select2();
                        if(projectdata.segments){
                            $.each( titles.segments, function( key, value ) {
                                segments += '<option value="'+key+'">'+value+'</option>';
                            });
                        }
                        $("select#segment_id").html("");
                        $("select#segment_id").html(segments);
                        $('select#segment_id').select2();
                    }
                });
            }
        });
    </script>
@endpush