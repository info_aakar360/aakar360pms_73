<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Adhoc</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'updateAdhoc','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Employee Name</label>

                    <select id="user_id" name="user_id" class="form-control">
                        @foreach($employees as $employee)
                            <option <?php if($employee->id == $adhoc->user_id) { echo 'selected'; } ?> value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Component Type</label>
                    <select  name="compo_type" id="compo_type" class="form-control">
                        <option <?php if($adhoc->compo_type == "earning") { echo 'selected'; } ?>  value="earning">Earning</option>
                        <option <?php if($adhoc->compo_type == "diduction") { echo 'selected'; } ?> value="diduction">Diduction</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Component Name</label>
                    <select  name="compo_name" id="compo_name" class="form-control namebytype">
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Amount</label>
                    <input type="text" name="amount" id="amount" value="{{$adhoc->amount}}" class="form-control">
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{$adhoc->id}}">
            <div class="form-actions">
                <button type="submit" id="updateAdhocAction-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#updateAdhocAction-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.declaration.updateAdhoc')}}',
            container: '#updateAdhoc',
            type: "POST",
            data: $('#updateAdhoc').serialize(),
            success: function (response) {
                $('#employeeAdhocDetailModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });

    $("#compo_type").change(function () {
        var val = $(this).val();
        var url = '{{route('admin.declaration.getComponentName')}}';
        $.easyAjax({
            type: 'POST',
            url: url,
            data: { 'val': val,'_token': '{{ csrf_token() }}' },
            success: function (data) {
                $(".namebytype").html(data);
            }
        });
    });
</script>