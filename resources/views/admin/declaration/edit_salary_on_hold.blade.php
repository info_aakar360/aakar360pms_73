<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Salary On Hold</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'salaryOnHold','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Employee Name</label>

                    <select id="user_id" name="user_id" class="form-control">
                        @foreach($employees as $employee)
                            <option <?php if($payaction->user_id == $employee->id){ echo  'selected';} ?> value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Pay Action</label>
                    <select  name="pay_action" id="pay_action" class="form-control">
                        <option <?php if($payaction->payaction == "pay"){ echo  'selected';} ?> value="pay">Pay</option>
                        <option <?php if($payaction->payaction == "hold"){ echo  'selected';} ?> value="hold">On Hold</option>
                    </select>
                </div>
            </div>


            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Comment</label>
                    <input type="text" name="comment" id="comment" value="{{$payaction->comment}}" class="form-control">
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{$payaction->id}}">
            <div class="form-actions">
                <button type="submit" id="updatepayaction-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#updatepayaction-form').click(function () {
        var year = $('#year').val();
        var month = $('#month').val();
        $.easyAjax({
            url: '{{route('admin.declaration.updateSalaryOnHold')}}?'+'month=' +month+'&year='+year,
            container: '#salaryOnHold',
            type: "POST",
            data: $('#salaryOnHold').serialize(),
            success: function (response) {
                $('#editSalaryDetailModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>