<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.employees.addNewEmployee')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'addLop','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Employee Name</label>

                    <select id="user_id" name="user_id" class="form-control">
                        @foreach($employees as $employee)
                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Lop</label>
                    <input type="text" name="lop" id="lop" class="form-control">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="addLop-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#addLop-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.declaration.postLop')}}',
            container: '#addLop',
            type: "POST",
            data: $('#addLop').serialize(),
            success: function (response) {
                $('#employeeLopDetailModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>