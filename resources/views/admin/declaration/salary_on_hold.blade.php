<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.employees.addNewEmployee')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'payAction','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Employee Name</label>

                    <select id="user_id" name="user_id" class="form-control">
                        @foreach($employees as $employee)
                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Pay Action</label>
                    <select  name="pay_action" id="pay_action" class="form-control">
                        <option value="pay">Pay</option>
                        <option value="hold">On Hold</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Comment</label>
                    <input type="text" name="comment" id="comment" class="form-control">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="payaction-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>
$('#payaction-form').click(function () {
    var year = $('#year').val();
    var month = $('#month').val();
    $.easyAjax({
        url: '{{route('admin.declaration.postSalaryOnHold')}}?'+'month=' +month+'&year='+year,
        container: '#payAction',
        type: "POST",
        data: $('#payAction').serialize(),
        success: function (response) {
            $('#payActionModal').modal('hide');
            window.location.reload();
        }
    });

    return false;
});
</script>