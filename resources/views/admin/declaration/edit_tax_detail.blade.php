<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Tax Detail</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'updateTaxDetail','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Employee Name</label>

                    <select id="user_id" name="user_id" class="form-control">
                        @foreach($employees as $employee)
                            <option <?php if($taxdetail->user_id == $employee->id){ echo  'selected'; } ?> value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Income Tax</label>
                    <input type="text" name="income_tax" id="income_tax" value="{{$taxdetail->incometax}}" class="form-control">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Comment</label>
                    <input type="text" name="comment" id="comment" value="{{$taxdetail->comment}}" class="form-control">
                </div>
            </div>
            <input type="hidden" name="id" id="id" value="{{$taxdetail->id}}">
            <div class="form-actions">
                <button type="submit" id="updatetaxdetail-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#updatetaxdetail-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.declaration.updateTaxDetail')}}',
            container: '#updateTaxDetail',
            type: "POST",
            data: $('#updateTaxDetail').serialize(),
            success: function (response) {
                $('#editTaxDetailModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>