@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.employees.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <style>
        .counter{
            font-size: large;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
@endpush

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab"><a href="#taxscheme" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">@lang('app.menu.taxscheme')</span> </a> </li>
                    <li class="tab"><a href="#hra" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">@lang('app.menu.hra')</span> </a> </li>
                    <li class="tab"><a href="#deduction" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="icon-layers"></i></span> <span class="hidden-xs">@lang('app.menu.deduction')</span> </a> </li>
                    <li class="tab"><a href="#incomeandloss" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-list"></i></span> <span class="hidden-xs">@lang('app.menu.incomeandloss')</span> </a> </li>
                    <li class="tab"><a href="#lta" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-logout"></i></span> <span class="hidden-xs">@lang('app.menu.lta')</span> </a> </li>
                    <li class="tab"><a href="#incomeprevious" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.incomeprevious')</span> </a> </li>
                    <li class="tab"><a href="#forms" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="icon-clock"></i></span> <span class="hidden-xs">@lang('app.menu.forms')</span> </a> </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="taxscheme">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="taxscm" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.employees.taxscheme')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="hra">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="hraa" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.declaration.dataRange')</th>
                                    <th>@lang('modules.declaration.anualamount')</th>
                                    <th>@lang('modules.declaration.landlordPan')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="deduction">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="deduct" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.declaration.declareamount')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="incomeandloss">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="incomeloss" style="width: 100%">
                                <thead>
                                <tr>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.invoices.type')</th>
                                    <th>@lang('modules.declaration.incomeloss')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="tab-pane" id="lta">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="ltas" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.invoices.type')</th>
                                    <th>@lang('modules.declaration.component')</th>
                                    <th>@lang('modules.declaration.declareamount')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="incomeprevious">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="income" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.declaration.income')</th>
                                    <th>@lang('modules.declaration.incometax')</th>
                                    <th>@lang('modules.declaration.pf')</th>
                                    <th>@lang('modules.declaration.pt')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="forms">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
                                   id="form" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>@lang('app.id')</th>
                                    <th>@lang('modules.employees.employeeName')</th>
                                    <th>@lang('modules.employees.location')</th>
                                    <th>@lang('modules.invoices.income')</th>
                                    <th>@lang('modules.declaration.incometax')</th>
                                    <th>@lang('modules.declaration.pf')</th>
                                    <th>@lang('modules.declaration.pt')</th>
                                    <th>@lang('modules.employees.documents')</th>
                                    <th>@lang('modules.tasks.comment')</th>
                                    <th>@lang('app.status')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script>
        var table;

        $(function() {
            loadTable();

            /*$('body').on('click', '.sa-params', function(){
                var id = $(this).data('user-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted user!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{--{{ route('admin.attendances.deleteRules',':id') }}--}}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'POST',
                            url: url,
                            data: {'_token': token, '_method': 'DELETE'},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                                    table._fnDraw();
                                }
                            }
                        });
                    }
                });
            });*/


        });
        function loadTable(){


            table = $('#taxscm').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.taxSchemeData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'taxscheme', name: 'taxscheme' },
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });

            table = $('#hraa').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.hraData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'daterange', name: 'daterange' },
                    { data: 'anualamount', name: 'anualamount'},
                    { data: 'landlardpan', name: 'landlardpan'},
                    { data: 'documents', name: 'documents'},
                    { data: 'comment', name: 'comment'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
            table = $('#deduct').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.declareData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'declareamount', name: 'declareamount' },
                    { data: 'documents', name: 'documents'},
                    { data: 'comments', name: 'comments'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
            table = $('#incomeloss').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.incomelossData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'type', name: 'type' },
                    { data: 'incomeloss', name: 'incomeloss'},
                    { data: 'documents', name: 'documents'},
                    { data: 'comments', name: 'comments'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });

            table = $('#ltas').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.ltaData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'type', name: 'type' },
                    { data: 'component', name: 'component'},
                    { data: 'declareamount', name: 'declareamount'},
                    { data: 'documents', name: 'documents'},
                    { data: 'comments', name: 'comments'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
            table = $('#income').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.declaration.incomePreviousData') !!}',
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'location', name: 'location' },
                    { data: 'income', name: 'income' },
                    { data: 'incometax', name: 'incometax'},
                    { data: 'pf', name: 'pf'},
                    { data: 'pt', name: 'pt'},
                    { data: 'documents', name: 'documents'},
                    { data: 'comments', name: 'comments'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });

        }
    </script>
@endpush