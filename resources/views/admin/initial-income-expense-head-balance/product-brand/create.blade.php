<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-plus"></i>Brand</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'add_brand_form', 'class'=>'form-horizontal ','method'=>'POST')) !!}
    <div class="form-body">
        <div class="row">
            <div id="addMoreBox1" class="clearfix">
                <div class="col-md-12">
                    <div id="dateBox" class="form-group ">
                        <label>Brand Name</label>
                        <input class="form-control" autocomplete="off" id="name"  name="name" type="text" value="" placeholder="Enter Brand Name"/>
                        <div id="errorDate"></div>
                    </div>
                </div>

            </div>
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="storeBrand()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>
<script>
    // Store Holidays
    function storeBrand(){
        var name = $('#name').val();
        var url = "{{ route('admin.productBrand.postBrand') }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            container: '#add_brand_form',
            data: $('#add_brand_form').serialize(),
            success: function (response) {
                $('#add-brand-form').modal('hide');
                loadTable();
            }
        });
    }
</script>