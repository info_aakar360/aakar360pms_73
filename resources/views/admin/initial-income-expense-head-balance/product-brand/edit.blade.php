<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"> <i class="fa fa-plus"></i>Brand</h4>
</div>
<div class="modal-body">
    {!! Form::open(array('id' => 'update_brand_form', 'class'=>'form-horizontal ','method'=>'POST')) !!}
    <div class="form-body">
        <div class="row">
            <div id="addMoreBox1" class="clearfix">
                <div class="col-md-12">
                    <input type="hidden" id="rowid" name="rowid" value="{{$brands->id}}">
                    <div id="dateBox" class="form-group ">
                        <label>Brand Name</label>
                        <input class="form-control" autocomplete="off" id="name"  name="name" type="text" @if(!empty($brands)) value="{{$brands->name}}" @endif placeholder="Enter Brand Name"/>
                        <div id="errorDate"></div>
                    </div>
                </div>

            </div>
        </div>
        <!--/row-->
    </div>
    {!! Form::close() !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
    <button type="button" onclick="updateBrand()" class="btn btn-info save-event waves-effect waves-light"><i class="fa fa-check"></i> @lang('app.save')
    </button>
</div>
<script>
    // Store Holidays
    function updateBrand(){
        var rowid = $('#rowid').val();
        var name = $('#name').val();
        var url = "{{ route('admin.productBrand.updateBrand') }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            container: '#update_brand_form',
            data: $('#update_brand_form').serialize(),
            success: function (response) {
                $('#edit-brand-form').modal('hide');
                loadTable();
            }
        });
    }
</script>