<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Add Input Fields</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'createInputform','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control" onchange="getOptionData(this.value);">
                            <option value="">Please select type</option>
                            <option value="text">Text</option>
                            <option value="date">Date</option>
                            <option value="number">Number</option>
                            <option value="button">Button</option>
                            <option value="dropdown">Dropdown</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12" id="responseselect" style="display: none;">
                    <div class="form-group">
                        <div id="sortableselect">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-4">Name</div>
                                <div class="col-md-4">Association (Select One)</div>
                            </div>
                            <div class="rescount">
                                <div class="form-group row">
                                    <div class="col-md-2">Response <span class="resnum">1</span>:</div>
                                    <div class="col-md-5"><input type="text" name="options[]" class="form-control" placeholder="Text"></div>
                                    <div class="col-md-4">
                                        <p class="colrblock colrblock1 colrblock11" onclick="selectColour(1,1);" data-color="red" style="background: red;"></p>
                                        <p class="colrblock colrblock1 colrblock12"  onclick="selectColour(1,2);" data-color="green" style="background: green;"></p>
                                        <p class="colrblock colrblock1 colrblock13"  onclick="selectColour(1,3);" data-color="gray" style="background: gray;"></p>
                                        <input type="hidden" name="colours[]" class="selectcolor1" />
                                    </div>
                                    <div class="col-md-1"><a href="javascript:void(0);" class="removerow"><i class="fa fa-trash-o"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1 m-t-5">
                            <button type="button" class="btn btn-info" id="add-select-item"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createInputform').submit(function () {
        $.easyAjax({
            url: '{{route('admin.inputFields.storeFields')}}',
            container: '#createInputform',
            type: "POST",
            data: $('#createInputform').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.inputFields.storeFields')}}',
            container: '#createProjectCategory',
            type: "POST",
            data: $('#createInputform').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    function getOptionData(val) {
        var type = val;
        if(type == 'button'||type == 'dropdown'){
            $('#responseselect').show();
        }else{
            $('#responseselect').hide();
        }
    }
    function selectColour(row,col){
        var clrb = $('.colrblock'+row);
        clrb.css("opacity", "0.5");
        $('.colrblock'+row+col).css("opacity", "1");
       var color= $('.colrblock'+row+col).data('color');
        $('.selectcolor'+row).val("");
        $('.selectcolor'+row).val(color);
    }

    $(document).on('click','.removerow',function () {
            $(this).parent().parent().parent().remove();
    });
    $('#add-select-item').click(function () {
        var rescount = $(".rescount").length;
            rescount = rescount+1;
            var thisd = 'this';
        var item = '<div class="rescount">'
            +'<div class="form-group row ">'
            +'<div class="col-md-2">Response <span class="resvalue">'+rescount+'</span>:</div>'
            +'<div class="col-md-5"><input type="text" name="options[]" class="form-control" placeholder="Text"></div>'
            +'<div class="col-md-4">'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'1" onclick="selectColour('+rescount+',1);" data-color="red"  style="background: red;"></p>'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'2" onclick="selectColour('+rescount+',2);" data-color="green"    style="background: green;"></p>'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'3" onclick="selectColour('+rescount+',3);" data-color="gray"   style="background: gray;"></p>'
            +'<input type="hidden" name="colours[]" class="selectcolor'+rescount+'" />'
            +'</div>'
            +'<div class="col-md-1"><a href="javascript:void(0);" class="removerow"><i class="fa fa-trash-o"></i></a></div>'
            +'</div>';
            +'</div>';
        $("#sortableselect").append(item);
    });
    /*$('#add-select-item').click(function () {
        var item = '<div class="col-xs-12 item-row margin-top-5" style="padding-top: 5px;">'
            +'<div class="input-group col-md-10" style="float: left; padding-right: 5px;">'
            +'<input type="text" class="form-control" name="options[]" placeholder="Option">'
            +'</div>'


            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortableselect").fadeIn(500);
    });*/
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
</script>