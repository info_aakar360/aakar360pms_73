<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Input Fields</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $field->title }}">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control" onchange="getOptionData(this.value);">
                            <option value="">Please select type</option>
                            <option value="text" <?php if($field->type == 'text'){ echo 'selected'; } ?>>Text</option>
                            <option value="date" <?php if($field->type == 'date'){ echo 'selected'; } ?>>Date</option>
                            <option value="number" <?php if($field->type == 'number'){ echo 'selected'; } ?>>Number</option>
                            <option value="button" <?php if($field->type == 'button'){ echo 'selected'; } ?>>Button</option>
                            <option value="dropdown" <?php if($field->type == 'dropdown'){ echo 'selected'; } ?>>Dropdown</option>

                        </select>
                    </div>
                </div>
                <div class="col-xs-12" id="responseselect" style="display: <?php if($field->type == 'dropdown'||$field->type == 'button'){ echo 'block'; }else{ echo 'none'; } ?>;">
                    <div class="form-group">
                        <div id="sortableselect">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-4">Name</div>
                                <div class="col-md-4">Association (Select One)</div>
                            </div>
                            <?php
                            $optionsarray = !empty($field->options) ? explode(',',$field->options) : array();
                            $colorsarray = !empty($field->colors) ? explode(',',$field->colors) : array();
                          $x=1;  $y=0;  foreach ($optionsarray as $option){
                            ?>
                            <div class="rescount">
                                <div class="form-group row">
                                    <div class="col-md-2">Response <span class="resnum">{{ $x }}</span>:</div>
                                    <div class="col-md-5"><input type="text" name="options[]" value="{{ $option }}" class="form-control" placeholder="Text"></div>
                                    <div class="col-md-4">
                                        <p class="colrblock colrblock{{ $x }} colrblock{{ $x }}1" onclick="selectColour('{{ $x }}',1);" data-color="red" style="background: red; <?php if(!empty($colorsarray[$y])&&$colorsarray[$y] == 'red'){ echo 'opacity:1;'; }?>"></p>
                                        <p class="colrblock colrblock{{ $x }} colrblock{{ $x }}2"  onclick="selectColour('{{ $x }}',2);" data-color="green" style="background: green; <?php if(!empty($colorsarray[$y])&&$colorsarray[$y] == 'green'){ echo 'opacity:1;'; }?>"></p>
                                        <p class="colrblock colrblock{{ $x }} colrblock{{ $x }}3"  onclick="selectColour('{{ $x }}',3);" data-color="gray" style="background: gray; <?php if(!empty($colorsarray[$y])&&$colorsarray[$y] == 'gray'){ echo 'opacity:1;'; }?>"></p>
                                        <input type="hidden" name="colours[]" value="@if(!empty($colorsarray[$y])) {{ $colorsarray[$y] }} @endif" class="selectcolor{{ $x }}" />
                                    </div>
                                    <div class="col-md-1"><a href="javascript:void(0);" class="removerow"><i class="fa fa-trash-o"></i></a></div>
                                </div>
                            </div>
                            <?php $x++; $y++; }?>
                        </div>
                        <div class="col-xs-1 m-t-5">
                            <button type="button" class="btn btn-info" id="add-select-item"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('admin.inputFields.updateFields', [$field->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })



    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.inputFields.updateFields', [$field->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    function getOptionData(val) {
        var type = val;
        if(type == 'button'||type == 'dropdown'){
            $('#responseselect').show();
        }else{
            $('#responseselect').hide();
        }
    }
    function selectColour(row,col){
        var clrb = $('.colrblock'+row);
        clrb.css("opacity", "0.5");
        $('.colrblock'+row+col).css("opacity", "1");
        var color= $('.colrblock'+row+col).data('color');
        $('.selectcolor'+row).val("");
        $('.selectcolor'+row).val(color);
    }

    $(document).on('click','.removerow',function () {
        $(this).parent().parent().parent().remove();
    });
    $('#add-select-item').click(function () {
        var rescount = $(".rescount").length;
        rescount = rescount+1;
        var thisd = 'this';
        var item = '<div class="rescount">'
            +'<div class="form-group row ">'
            +'<div class="col-md-2">Response <span class="resvalue">'+rescount+'</span>:</div>'
            +'<div class="col-md-5"><input type="text" name="options[]" class="form-control" placeholder="Text"></div>'
            +'<div class="col-md-4">'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'1" onclick="selectColour('+rescount+',1);" data-color="red"  style="background: red;"></p>'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'2" onclick="selectColour('+rescount+',2);" data-color="green"    style="background: green;"></p>'
            +'<p class="colrblock colrblock'+rescount+' colrblock'+rescount+'3" onclick="selectColour('+rescount+',3);" data-color="gray"   style="background: gray;"></p>'
            +'<input type="hidden" name="colours[]" class="selectcolor'+rescount+'" />'
            +'</div>'
            +'<div class="col-md-1"><a href="javascript:void(0);" class="removerow"><i class="fa fa-trash-o"></i></a></div>'
            +'</div>';
        +'</div>';
        $("#sortableselect").append(item);
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });

    $(document).on('click','.remove-option', function () {
        $(this).closest('.option-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
</script>