@extends('layouts.app')
<?php
$moduleName = " Initial Bank Cash Balance";
$createItemName = "Create" . $moduleName;
$breadcrumbMainName = $moduleName;
$breadcrumbCurrentName = " Create";
$breadcrumbMainIcon = "fas fa-balance-scale";
$breadcrumbCurrentIcon = "archive";
$ModelName = 'App\Transaction';
$ParentRouteName = 'admin.initial_bank_cash_balance';
?>
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route($ParentRouteName) }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading"> Put {{ $moduleName  }} Information</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form" id="form_validation" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick"
                                                        name="branch_id">
                                                    <option value="0">Select Branch Name</option>
                                                    @if (App\Branch::all()->count() >0 )
                                                        @foreach( App\Branch::all() as $project )
                                                            <option @if ( $project->id == old('branch_id' ))
                                                                    selected
                                                                    @endif value="{{ $project->id  }}">{{ $project->name  }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 field_area">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select data-live-search="true" class="form-control show-tick"
                                                        name="bank_cash_id"
                                                        id="">
                                                    <option value="0"> Select Bank Or Cash Name </option>
                                                    @if (App\BankCash::all()->count() >0 )
                                                        @foreach( App\BankCash::all() as $bank_cash )
                                                            <option @if ( $bank_cash->id == old('bank_cash_id' ))
                                                                    selected
                                                                    @endif value="{{ $bank_cash->id  }}">{{ $bank_cash->name  }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Amount</label>
                                            <input autocomplete="off" value="{{ old('amount') }}" name="amount"
                                                   type="number"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <label class="control-label">Date</label>
                                            <input autocomplete="off" value="{{ old('voucher_date') }}"
                                                   name="voucher_date" type="text"
                                                   class="form-control" id="start_date"
                                                   placeholder="Please choose a voucher date...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 field_area">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Cheque Number</label>
                                            <input name="cheque_number"
                                                   type="text"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="control-label">Particulars</label>
                                            <textarea class="form-control" name="particulars">{{ old('particulars')  }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-line">
                                        <button type="button" id="save-form" class="btn btn-primary m-t-15 waves-effect">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(".date-picker").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{ route($ParentRouteName.'.store') }}',
                container: '#form_validation',
                type: "POST",
                redirect: true,
                data: $('#form_validation').serialize()
            })
        });
        $("#start_date").datepicker({
            todayHighlight: true,
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        })
    </script>
@endpush