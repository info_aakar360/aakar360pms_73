@extends('layouts.app')


@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/morrisjs/morris.css') }}"><!--Owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.css') }}"><!--Owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('plugins/bower_components/owl.carousel/owl.theme.default.css') }}"><!--Owl carousel CSS -->

<style>
    .col-in {
        padding: 0 20px !important;

    }

    .fc-event{
        font-size: 10px !important;
    }

    @media (min-width: 769px) {
        #wrapper .panel-wrapper{
            height: 500px;
            overflow-y: auto;
        }
    }

</style>
@endpush

@section('content')

    <div class="rightsidebarfilter col-md-3 " style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>
        <form method="post" action="{{ route('admin.dashboard') }}"  >

            <div class="col-md-12">
                <h5 class="box-title">
                    @lang('app.project') </h5>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2 form-control" name="project_id" data-placeholder="@lang('app.selectProject')" id="project_id">
                                <option value="">@lang('modules.client.all')</option>
                                @foreach($projectarray as $project)
                                    <option value="{{ $project->id }}">{{ ucwords($project->project_name) }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> @lang('app.apply')</button>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12" >
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6 text-right hidden-xs">
                        <div class="pull-left">
                            <h2 style="color: #002f76">@lang('app.menu.dashboard')</h2>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="pull-right">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                        class="fa fa-cog"></i></a>
                            {{-- <a onclick="exportTimeLog()" class="btn btn-info"><i class="ti-export" aria-hidden="true"></i> @lang('app.exportExcel')</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
         <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.menu.calender')</div>
                <div class="panel-wrapper collapse in" style="overflow: auto">
                    <div class="panel-body">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">@lang('modules.dashboard.taskStatus')</div>
                        <div class="panel-wrapper collapse in" style="overflow: auto">
                            <div class="panel-body">
                                <ul class="list-task list-group" data-role="tasklist">
                                    <li class="list-group-item" data-role="task">
                                        <strong>@lang('app.status')</strong> <span class="pull-right"><strong>@lang('modules.dashboard.count')</strong></span>
                                    </li>
                                    <li class="list-group-item" data-role="task">
                                        <strong><img style="width:5%;margin-right:5px;" src="{{ asset('user-uploads/not-started.png') }}" >Not Started</strong> <span class="pull-right"><strong> {{ $notstarted }}</strong></span>
                                    </li>
                                    <li class="list-group-item" data-role="task">
                                        <strong><img style="width:5%;margin-right:5px;" src="{{ asset('user-uploads/in-progress.png') }}" >In Progress</strong> <span class="pull-right"><strong> {{ $inprogress }}</strong></span>
                                    </li>
                                    <li class="list-group-item" data-role="task">
                                        <strong><img style="width:5%;margin-right:5px;" src="{{ asset('user-uploads/in-problem.png') }}" >In Problem</strong> <span class="pull-right"><strong> {{ $inproblem }}</strong></span>
                                    </li>
                                    <li class="list-group-item" data-role="task">
                                        <strong><img style="width:5%;margin-right:5px;" src="{{ asset('user-uploads/delay.png') }}" >Delayed</strong> <span class="pull-right"><strong> {{ $delayed }}</strong></span>
                                    </li>
                                    <li class="list-group-item" data-role="task">
                                        <strong><img style="width:5%;margin-right:5px;" src="{{ asset('user-uploads/completed.png') }}" >Completed</strong> <span class="pull-right"><strong> {{ $completed }}</strong></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.activityLevelProgress')</div>
                <div class="panel-wrapper collapse in" style="overflow: auto">
                    <div class="panel-body">
                        <?php $activitypoints = 0;?>
                        @if(count($activitylist)>0)
                            @foreach($activitylist as $activityl)
                                <?php $tasksarray = \App\Task::where('project_id',$projectid)->where('task_category_id',$activityl->itemid)->pluck('id')->toArray();
                                $percentage = \App\Task::whereIn('id',$tasksarray)->avg('percentage');
                                    $activitypoints += ceil($percentage);
                                ?>
                        <div class="form-group">
                            <label class="control-label"><strong>{{ $activityl->itemname }}:</strong></label>
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger black-colour" role="progressbar" aria-valuenow="{{  !empty($percentage) ?  ceil($percentage) : 0 }}"
                                     aria-valuemin="0" aria-valuemax="100" style="width:{{  !empty($percentage) ?  ceil($percentage) : 0 }}%">
                                    {{  !empty($percentage) ?  ceil($percentage) : 0 }}%
                                </div>
                            </div>
                        </div>

                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('app.projectLevelProgress')</div>
                <div class="panel-wrapper collapse in" style="overflow: auto">
                    <div class="panel-body">

                        <div class="form-group">
                            <?php  $levelprogress = 0;
                                if(!empty($activitypoints)&&!empty($activitylist)){
                                    $levelprogress = $activitypoints/count($activitylist);
                                }
                            ?>
                            <label class="control-label"><strong>@lang('app.actualprojectProgress')</strong></label>
                            <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{  !empty($levelprogress) ?  ceil($levelprogress) : 0 }}"
                                         aria-valuemin="0" aria-valuemax="100" style="width:{{  !empty($levelprogress) ?  ceil($levelprogress) : 0 }}%">
                                        {{  !empty($levelprogress) ?  ceil($levelprogress) : 0 }}%
                                    </div>
                                </div>
                          </div>
                        <div class="form-group">
                            <label class="control-label"><strong>@lang('app.plannedprojectProgress')</strong></label>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{  !empty($plannedprogress) ?  ceil($plannedprogress) : 0 }}"
                                     aria-valuemin="0" aria-valuemax="100" style="width:{{  !empty($plannedprogress) ?  ceil($plannedprogress) : 0 }}%">
                                    {{  !empty($plannedprogress) ?  ceil($plannedprogress) : 0 }}%
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="eventDetailModal" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Sub Task e</span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection


@push('footer-script')

<script>
    $('.toggle-filter').click(function () {
        $('#ticket-filters').toggle("slide", {direction: "right" }, 1000);
    });
    var taskEvents = [
            @foreach($tasks as $task)
            @if (is_null($task->start_date))
        {
            id: '{{ $task->id }}',
            url: '{{ route('admin.all-tasks.updateTask',$task->id) }}',
            title: 'Task - {{ ucfirst($task->heading) }}',
            start: '{{ $task->due_date }}',
            className: 'bg-success'
        },
            @else
        {
            id: '{{ $task->id }}',
            url: '{{ route('admin.all-tasks.updateTask',$task->id) }}',
            title: 'Task - {{ ucfirst($task->heading) }}',
            start: '{{ $task->start_date }}',
            end:  '{{ $task->due_date->addDay() }}',
            className: 'bg-success'
        },
            @endif
            @endforeach
            @foreach($todolist as $task)
            @if (is_null($task->start_date))
        {
            id: '{{ $task->id }}',
            title: 'Todo - {{ ucfirst($task->title) }}',
            url: '{{ route('admin.todo.edit',$task->id) }}',
            start: '{{ $task->due_date }}',
            className: 'bg-dander'
        },
            @else
        {
            id: '{{ $task->id }}',
            title: 'Todo - {{ ucfirst($task->title) }}',
            url: '{{ route('admin.todo.edit',$task->id) }}',
            start: '{{ $task->start_date }}',
            end:  '{{ $task->due_date->addDay() }}',
            className: 'bg-dander'
        },
            @endif
            @endforeach
            @foreach($issueslist as $issue)
            @if (is_null($issue->start_date))
        {
            id: '{{ $issue->id }}',
            title: 'Issue - {{ ucfirst($issue->title) }}',
            url: '{{ route('admin.issue.reply',$issue->id) }}',
            start: '{{ $issue->due_date }}',
            className: 'bg-warning'
        },
            @else
        {
            id: '{{ $issue->id }}',
            title: 'Issue - {{ ucfirst($issue->title) }}',
            url: '{{ route('admin.issue.reply',$issue->id) }}',
            start: '{{ $issue->start_date }}',
            end:  '{{ $issue->due_date }}',
            className: 'bg-warning'
        },
            @endif
            @endforeach
            @foreach($rfilist as $rfi)
            @if (!is_null($rfi->due_date))
        {
            id: '{{ $rfi->id }}',
            title: 'Rfi - {{ ucfirst($rfi->title) }}',
            url: '{{ route('admin.rfi.details',$rfi->id) }}',
            start: '{{ $rfi->due_date }}',
            className: 'bg-info'
        },
            @endif
            @endforeach
            @foreach($projectholidays as $projecth)
            @if (!is_null($projecth->date))
        {
            id: '{{ $projecth->id }}',
            title: 'Holiday - {{ ucfirst($projecth->occassion) }}',
            url: '',
            start: '{{ $projecth->date }}',
            className: 'bg-info'
        },
            @endif
            @endforeach
            @foreach($myleavesarray as $myleaves)
            @if (!is_null($myleaves->leave_date))
        {
            id: '{{ $myleaves->id }}',
            title: 'Leave - {{ ucfirst($myleaves->reason) }}',
            url: '',
            start: '{{ date('Y-m-d',strtotime($myleaves->leave_date)) }}',
            className: 'bg-info'
        },
            @endif
            @endforeach
            @foreach($indentlist as $indent)
            @if (!is_null($indent->expected_date)){
            <?php $duedate = date('Y-m-d',strtotime($indent->expected_date)); ?>
            id: '{{ $indent->id }}',
            title: 'Indent - {{ ucfirst($indent->indent_no) }}',
            url: '{{ route('admin.indent.viewIndent',$indent->id) }}',
            start: '{{ $duedate }}',
            className: 'bg-info'
        },
            @endif
            @endforeach
            @foreach($submittalslist as $submittals)
            @if (!is_null($submittals->submitdate))
        {
            id: '{{ $submittals->id }}',
            title: 'Submittals - {{ ucfirst($submittals->title) }}',
            url: '{{ route('admin.submittals.details',$submittals->id) }}',
            start: '{{ $submittals->submitdate }}',
            className: 'bg-info'
        },
            @endif
            @endforeach
            @foreach($inspectionlist as $inspection)
            @if (!is_null($inspection->due_date)){
            <?php $duedate = \Carbon\Carbon::createFromFormat($global->date_format, $inspection->due_date)->format('Y-m-d'); ?>
            id: '{{ $inspection->id }}',
            title: 'Inspection - {{ ucfirst($inspection->name) }}',
            url: '{{ route('admin.inspectionName.inspectionAssignForm',$inspection->id) }}',
            start: '{{ $duedate }}',
            className: 'bg-primary'
        },
            @endif
            @endforeach
            @foreach($observationslist as $observations)
            @if (is_null($observations->start_date))
        {
            id: '{{ $observations->id }}',
            title: 'Observation - {{ ucfirst($observations->title) }}',
            url: '{{ route('admin.observations.reply',$inspection->id) }}',
            start: '{{ $observations->due_date }}',
            className: 'bg-primary'
        },
            @else
        {
            id: '{{ $observations->id }}',
            title: 'Observation - {{ ucfirst($observations->title) }}',
            url: '{{ route('admin.observations.reply',$observations->id) }}',
            start: '{{ $observations->start_date }}',
            end:  '{{ $observations->due_date }}',
            className: 'bg-primary'
        },
            @endif
            @endforeach

            @foreach($meetingslist as $meetings)
            @if (!is_null($meetings->meeting_date)){
            <?php
                /*$duedate = \Carbon\Carbon::createFromFormat($global->date_format, $meetings->meeting_date)->format('Y-m-d');*/
                $duedate = date('Y-m-d',strtotime($meetings->meeting_date));
                $starttime = $duedate.'T'.$meetings->start_time.':00';
                $endtime = $duedate.'T'.$meetings->finish_time.':00';
                ?>
            id: '{{ $meetings->id }}',
            title: 'Meetings - {{ ucfirst($meetings->meetingtitle) }}',
            url: '{{ route('admin.meetings.viewMeeting',$meetings->id) }}',
            start: '{{ $starttime }}',
            end: '{{ $endtime }}',
            className: 'bg-danger'
        },
        @endif
        @endforeach
    ];

    var getEventDetail = function (id) {
        var url = '{{ route('admin.leaves.show', ':id')}}';
        url = url.replace(':id', id);

        $('#modelHeading').html('Event');
        $.ajaxModal('#eventDetailModal', url);
    }

    var calendarLocale = '{{ $global->locale }}';
    var firstDay = '{{ $global->week_start }}';

    $('.leave-action').click(function () {
        var action = $(this).data('leave-action');
        var leaveId = $(this).data('leave-id');
        var url = '{{ route("admin.leaves.leaveAction") }}';

        $.easyAjax({
            type: 'POST',
            url: url,
            data: { 'action': action, 'leaveId': leaveId, '_token': '{{ csrf_token() }}' },
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        });
    })
</script>


<script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
{{--<script src="{{ asset('plugins/bower_components/morrisjs/morris.js') }}"></script>--}}

<script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>

<!-- jQuery for carousel -->
<script src="{{ asset('plugins/bower_components/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/owl.carousel/owl.custom.js') }}"></script>

<!--weather icon -->
<script src="{{ asset('plugins/bower_components/skycons/skycons.js') }}"></script>

<script src="{{ asset('plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('plugins/bower_components/calendar/dist/locale-all.js') }}"></script>
<script src="{{ asset('js/event-calendar.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    $('.show-task-detail').click(function () {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");

        var id = $(this).data('task-id');
        var url = "{{ route('admin.all-tasks.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    })

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.dashboard.widget')}}',
            container: '#createProject',
            type: "POST",
            redirect: true,
            data: $('#createProject').serialize()
        })
    });

</script>
@endpush
