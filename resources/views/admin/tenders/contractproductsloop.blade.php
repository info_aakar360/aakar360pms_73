<?php
if(!empty($awardedcontract)){
    function costitemsloop($boqarray){
        $awardedcontract = $boqarray['awardedcontract'];
        $level = $boqarray['level'];
        $unitsarray = $boqarray['unitsarray'];
        $categoryarray = $boqarray['categoryarray'];
        $projectid = $boqarray['projectid'];
        $subprojectid = $boqarray['subprojectid'];
        $segmentid = $boqarray['segmentid'];
        $parent = $boqarray['parent'];
        $catparent = $boqarray['catparent'];
        $snorow = $boqarray['snorow'];
            $level1categoryarray =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',$level)->where('parent',$parent)->orderBy('inc','asc')->get();
            if(count($level1categoryarray)>0){
$act = 1;  foreach ($level1categoryarray as $level1category){
            if((int)$level1category->level==0){
                $snorow = $snorow+1;
                $actrow = $snorow;
            }else{
                $actrow = $snorow.'.'.$act;
            }
            ?>
            <tr class="item-row">
                <td align="center">{{ $actrow }}</td>
                <td><a href="javascript:void(0);"  class="red remove-item-category" data-rowid="{{ $level1category->id }}" data-toggle="tooltip"  data-original-title="Delete Activity" ><i class="fa fa-trash"></i></a></td>
                <td><i class="fa fa-plus-circle"></i></td>
                <td></td>
                <td>{{ get_category($level1category->category) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
            <?php

            if(!empty($segmentid)){
                $costitemsarray = \App\ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position_id',$level1category->product_category)->orderBy('inc','asc')->get();
            }else{
                $costitemsarray = \App\ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('position_id',$level1category->product_category)->orderBy('inc','asc')->get();
                  }
            $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('awarded_category',$level1category->id)->orderBy('inc','asc')->get();
            $tid = 1;
            $addedproducts = array();
            foreach ($tenderproductsarray as $tenderproducts){
            ?>
            <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                <td align="center">{{ $actrow.'.'.$tid }}</td>
                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                <td><i class="fa fa-plus-circle"></i></td>
                <td></td>
                <td>
                    <input  class="cell-inp" list="costitemlistaward{{ $tenderproducts->id }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
                    <datalist  id="costitemlistaward{{ $tenderproducts->id }}">
                        <?php    foreach($costitemsarray as $costitem){
                        $costitemname = get_cost_name($costitem->cost_items_id); ?>
                        <option value="{{ $costitem->id }}"  >{{ $costitemname }}</option>
                        <?php   } ?>
                    </datalist>
                </td>
                <td>
                    <input  class="cell-inp unitvalue{{ $tenderproducts->id }}"  list="unitdatacataward{{ $tenderproducts->id }}"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ get_unit_name($tenderproducts->unit) }}">
                    <datalist id="unitdatacataward{{ $tenderproducts->id }}">
                       <?php foreach($unitsarray as $units){ ?>
                        <option  value="{{ $units->id }}" >{{ $units->name }}</option>
                        <?php   } ?>
                    </datalist>
                </td>
                <td><input type="text" class="cell-inp qty{{ $tenderproducts->id }}" onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->qty }}"></td>
                <td><input type="text" class="cell-inp ratevalue{{ $tenderproducts->id }}"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->rate }}"></td>
                <td><input type="text" class="cell-inp finalrate{{ $tenderproducts->id }}" name="finalrate[{{ $tenderproducts->id }}]"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->finalrate }}"></td>

            </tr>
            <?php
            $tid++;
            $addedproducts[] = $tenderproducts->products;
            }?>

            <?php
            $newlevel = $level+1;
            $catparent  = $level1category->product_category;
            $parent  = $level1category->id;

            $addparentcats =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',$newlevel)->where('parent',$parent)->pluck('product_category')->toArray();
            if(!empty($segmentid)){
                $levelcategoryarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid);
            }else{
                $levelcategoryarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid);
            }
            if(!empty($addparentcats)){
                $levelcategoryarray = $levelcategoryarray->whereNotIn('id',$addparentcats);
            }
            $levelcategoryarray = $levelcategoryarray->where('position','row')->where('level',$newlevel)->where('parent',$catparent)->orderBy('inc','asc')->get();


            $boqarray = array();
            $boqarray['awardedcontract'] = $awardedcontract;
            $boqarray['unitsarray'] = $unitsarray;
            $boqarray['categoryarray'] = $levelcategoryarray;
            $boqarray['level'] = $newlevel;
            $boqarray['catparent'] = $catparent;
            $boqarray['parent'] = $parent;
            $boqarray['snorow'] = $actrow;
            $boqarray['projectid'] = $projectid;
            $boqarray['subprojectid'] = $subprojectid;
            $boqarray['segmentid'] = $segmentid;
            echo costitemsloop($boqarray);


            if(!empty($segmentid)){
                $costitemsarray = \App\ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position_id',$level1category->product_category);
            }else{
                $costitemsarray = \App\ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$subprojectid)->where('position_id',$level1category->product_category);
            }
            if(!empty($addedproducts)){
                $costitemsarray = $costitemsarray->whereNotIn('id',$addedproducts);
            }
            $levelcostitemsarray = $costitemsarray->orderBy('inc','asc')->get();
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><i class="fa fa-plus-circle"></i></td>
                <td>
                    <input  class="cell-inp costitemcatrow sdfds"  data-level="{{ $newlevel }}"  data-parent="{{ $level1category->id }}"  list="costitemcatlist{{ $level1category->id }}" placeholder="Activity">
                    <datalist class="costitemscatlist"  id="costitemcatlist{{ $level1category->id }}">
                        <?php  if(!empty($levelcategoryarray)){
                        foreach ($levelcategoryarray as $colums) { ?>
                        <option value="{{ $colums->id }}" data-itemid="{{ $colums->itemid }}" >{{ $colums->itemname }}</option>
                        <?php     } }?>

                    </datalist>
                </td>
                <td><input type="text" class="cell-inp"></td>
                <td><input type="text" class="cell-inp"></td>
                <td><input type="text" class="cell-inp"></td>
                <td><input type="text" class="cell-inp"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <input  class="cell-inp costitemrow" data-cat="{{ $level1category->id }}" list="costitemlist{{ $level1category->id }}" placeholder="Task">
                    <datalist class="costitemslist"  id="costitemlist{{ $level1category->id }}">
                        <?php  if(count($levelcostitemsarray)>0){
                        foreach ($levelcostitemsarray as $costitem) {
                        $costitemname = get_cost_name($costitem->cost_items_id); ?>
                        <option value="{{ $costitem->id }}" data-name="{{ $costitemname }}" >{{ $costitemname }}</option>
                        <?php     } }?>
                    </datalist>
                </td>
                <td><input type="text" class="cell-inp"></td>
                <td>
                    <input  class="cell-inp " list="unitdatacat" >
                    <datalist id="unitdatacat">
                        <?php foreach($unitsarray as $units){ ?>
                        <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                        <?php }?>
                    </datalist>
                </td>
                <td><input type="text" class="cell-inp"></td>
                <td><input type="text" class="cell-inp"></td>
            </tr>
            <?php

            $act++;
            }

            }
    }
    if(!empty($awardedcontract->project_id)){
        $projectid = $awardedcontract->project_id;
    }
    if(!empty($awardedcontract->subproject_id)){
        $subprojectid = $awardedcontract->subproject_id;
    }else{
        $subprojectid = 0;
    }
    if(!empty($awardedcontract->segment_id)){
        $segmentid = $awardedcontract->segment_id;
    }else{
        $segmentid = 0;
    }
    $addparentcats =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level','0')->where('parent','0')->pluck('product_category')->toArray();
    if(!empty($segmentid)){
        $categoryarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid);
    }else{
        $categoryarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid);
    }
    if(!empty($addparentcats)){
        $categoryarray = $categoryarray->whereNotIn('id',$addparentcats);
    }
    $categoryarray = $categoryarray->where('position','row')->where('level','0')->where('parent','0')->orderBy('inc','asc')->get();
    $boqarray = array();
    $boqarray['awardedcontract'] = $awardedcontract;
    $boqarray['unitsarray'] = $unitsarray;
    $boqarray['categoryarray'] = $categoryarray;
    $boqarray['level'] = 0;
    $boqarray['parent'] = 0;
    $boqarray['catparent'] = 0;
    $boqarray['snorow'] = 0;
    $boqarray['projectid'] = $projectid;
    $boqarray['subprojectid'] = $subprojectid;
    $boqarray['segmentid'] = $segmentid;
    echo costitemsloop($boqarray);
    ?>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td><i class="fa fa-plus-circle"></i></td>
    <td>
        <input  class="cell-inp costitemcatrow"  data-level="0"  data-parent="0"  list="costitemcatlist" placeholder="Activity">
        <datalist class="costitemscatlist"  id="costitemcatlist">
            <?php  if(count($categoryarray)>0){
            foreach ($categoryarray as $colums) { ?>
            <option value="{{ $colums->id }}" data-itemid="{{ $colums->itemid }}" >{{ $colums->itemname }}</option>
            <?php     } }?>

        </datalist>
    </td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
</tr>
<?php }?>