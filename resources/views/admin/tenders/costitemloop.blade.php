                            <?php
                                    function boqhtml($boqarray){

                            $user = $boqarray['user'];
                                        $tenders = $boqarray['tenders'];
                                        $unitsarray = $boqarray['unitsarray'];
                                        $categoryarray = $boqarray['categoryarray'];
                                        $catitem = $boqarray['catitem'];
                            $level = $boqarray['level'];
                            $catparent = $boqarray['catparent'];
                            $snorow = $boqarray['snorow'];
                            $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',$level)->where('parent',$catparent)->orderBy('inc','asc')->get();

                            if(count($level1categoryarray)>0){
                            foreach ($level1categoryarray as $level1category){
                            if(!empty($catitem)){
                                $catitem .= ','.$level1category->category;
                            }else{
                                $catitem = $level1category->category;
                            }

                            if((int)$level1category->level==0){
                                $snorow = $snorow+1;
                            }else{
                                $snorow = $snorow.'.'.$level1category->level;
                            }

                            $boqposition = $level1category->product_category;
                            $subprojectid = $tenders->title_id ?: 0;
                            $productcostarray  = \App\TendersProduct::where('tender_id',$tenders->id)->pluck('products')->toArray();
                            $costitemsarray = \App\ProjectCostItemsProduct::where('project_id',$tenders->project_id)->where('title',$subprojectid)->where('position_id',$boqposition);
                            if(!empty($productcostarray)){
                                $costitemsarray = $costitemsarray->whereNotIn('id',$productcostarray);
                            }
                            $costitemsarray = $costitemsarray->get();
                               ?>
                            <tr class="collpse">
                                <td><a href="javascript:void(0);"  class="red sa-params-cat" data-position-id="{{ $level1category->id }}" data-catitem="{{ $level1category->category }}" data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Activity" ><i class="fa fa-trash"></i></a></td>
                                <td align="left">{{ $snorow }}</td>
                                <td>{{ get_dots_by_level($level1category->level) }}{{ get_category($level1category->category) }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('tenders_category',$level1category->id)->orderBy('inc','asc')->get();
                            $tid=1;    foreach ($tenderproductsarray as $tenderproducts){
                            ?>
                            <tr   id="rowitem{{ $tenderproducts->id }}">
                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                <td align="left">{{ $snorow.'.'.$tid }}</td>
                                <td>
                                    <input  class="cell-inp" list="costitemlist{{ $tenderproducts->id }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
                                    <datalist  id="costitemlist{{ $tenderproducts->id }}">
                                        <?php    if(count($costitemsarray)>0){
                                            foreach($costitemsarray as $costitem){
                                        $costitemname = get_cost_name($costitem->cost_items_id); ?>
                                        <option value="{{ $costitem->id }}" data-itemname="{{ $costitemname }}"  >{{ $costitemname }}</option>
                                        <?php   }
                                        } ?>
                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp" name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                <td>
                                    <input  class="cell-inp" list="unitdatacat{{ $tenderproducts->id }}" value="{{ get_unit_name($tenderproducts->unit) }}">
                                    <datalist id="unitdatacat{{ $tenderproducts->id }}">
                                        <?php foreach($unitsarray as $units){ ?>
                                        <option  value="{{ $units->id }}" >{{ $units->name }}</option>
                                        <?php   }  ?>
                                    </datalist>
                                </td>
                            </tr>
                            <?php $tid++; }?>

                            <?php

                            $catparent = $level1category->id;
                            $newlevel = $level+1;
                            $level1categorycon =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',$newlevel)->where('parent',$catparent)->pluck('product_category')->toArray();
                            $proprogetlevel1 = \App\ProjectCostItemsPosition::where('position','row')
                                ->where('project_id',$tenders->project_id)
                                ->where('title',$subprojectid)
                                ->where('level',$newlevel);
                            if(count($level1categorycon)>0){
                                $proprogetlevel1 = $proprogetlevel1->whereNotIn('id',$level1categorycon);
                            }
                            $proprogetlevel1 = $proprogetlevel1->where('parent',$boqposition)
                                ->orderBy('inc','asc')
                                ->get();
                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['unitsarray'] = $unitsarray;
                            $boqarray['categoryarray'] = $proprogetlevel1;
                            $boqarray['catitem'] = '';
                            $boqarray['level'] = $newlevel;
                            $boqarray['catparent'] = $catparent;
                            $boqarray['snorow'] = $snorow;
                            echo  boqhtml($boqarray);

                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <input  class="cell-inp costitemcatrow" data-level="{{ $newlevel }}" data-parent="{{ $catparent }}"  list="costitemcatlist{{ $catparent }}" placeholder="Activity">
                                    <datalist class="costitemscatlist"  id="costitemcatlist{{ $catparent }}">
                                        <?php  if(!empty($proprogetlevel1)){
                                        foreach ($proprogetlevel1 as $colums) { ?>
                                        <option value="{{ $colums->id }}"  data-itemid="{{ $colums->itemid }}" data-itemname="{{ $colums->itemname }}" >{{ $colums->itemname }}</option>
                                        <?php     } }?>

                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td><input type="text" class="cell-inp"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <input  class="cell-inp costitemrow" data-tendercat="{{ $level1category->id }}" data-cat="{{ $level1category->product_category }}" list="tasklist{{ $level1category->id }}" placeholder="Task">
                                    <datalist  id="tasklist{{ $level1category->id }}">
                                        <?php if(count($costitemsarray)>0){
                                        foreach($costitemsarray as $costitem){
                                        $costitemname = get_cost_name($costitem->cost_items_id); ?>
                                        <option value="{{ $costitem->id }}" data-itemname="{{ $costitemname }}"  >{{ $costitemname }}</option>
                                        <?php }  } ?>
                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td>
                                    <input  class="cell-inp" list="unitdatacat" >
                                    <datalist id="unitdatacat">
                                        <?php foreach($unitsarray as $units){ ?>
                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                        <?php } ?>
                            </datalist>
                                </td>
                            </tr>

                            <?php } }     }  ?>

                            <?php
                            $categoryarray = array();

                            $subprojectid = $tenders->title_id ?: 0;
                            $level1categorycon =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',0)->where('parent',0)->pluck('product_category')->toArray();
                            $categoryarray = \App\ProjectCostItemsPosition::where('position','row')
                                ->where('project_id',$tenders->project_id)
                                ->where('title',$subprojectid)
                                ->where('level','0')
                                ->where('parent','0');
                               if(count($level1categorycon)>0){
                                   $categoryarray = $categoryarray->whereNotIn('id',$level1categorycon);
                               }
                            $categoryarray = $categoryarray->orderBy('inc','asc')->get();
                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['unitsarray'] = $unitsarray;
                            $boqarray['categoryarray'] = $categoryarray;
                            $boqarray['catitem'] = '';
                            $boqarray['level'] = '0';
                            $boqarray['catparent'] = '0';
                            $boqarray['snorow'] = 0;
                            echo  boqhtml($boqarray);
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <input  class="cell-inp costitemcatrow" data-level="0" data-parent="0"  list="costitemcatlist" placeholder="Activity">
                                    <datalist class="costitemscatlist"  id="costitemcatlist">
                                        <?php  if(!empty($categoryarray)){
                                        foreach ($categoryarray as $colums) { ?>
                                        <option value="{{ $colums->id }}"  data-itemid="{{ $colums->itemid }}" data-itemname="{{ $colums->itemname }}" >{{ $colums->itemname }}</option>
                                        <?php     } }?>

                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td><input type="text" class="cell-inp"></td>
                            </tr>