@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding Sheet</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Contractor Bidding List
                        </div>
                        <div class="col-md-6 text-right"  >
                            <a href="{{ route('admin.tenders.bidding-list', [$tenders->id]) }}" style="color:white" class="btn1 btn-info btn-circle"><i class="fa fa-list-alt"></i> Bid List</a>
                            <a href="{{ route('admin.tenders.bidding-sheet', [$tenders->id]) }}"  style="color:white" class="btn1 edit-btn btn-circle"><i class="fa fa-eye"></i> Bidding sheet</a>
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">

                <div class="row">
                    <div class="col-md-12 form-group">
                        <table class="table-striped" style="width: 100%;">
                            <tr>
                                <td style="width: 15%;"><b>Tender</b></td>
                                <td  style="width: 35%;">{{ $tenders->name }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><b>@lang('app.sno')</b></td>
                                <td style="width: 15%;"><b>@lang('app.number')</b></td>
                                <td  style="width: 35%;">{{ $tenders->number }}</td>
                                <td style="width: 15%;"><b>@lang('app.status')</b></td>
                                <td  style="width: 35%;">{{ $tenders->status }}</td>
                                <td style="width: 15%;"><b>@lang('app.startdate')</b></td>
                                <td  style="width: 35%;">{{ $tenders->startdate ? date('d-m-Y',strtotime($tenders->startdate)) : '' }}</td>
                                <td style="width: 15%;"><b>@lang('app.duedate')</b></td>
                                <td  style="width: 35%;">{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}</td>
                            </tr>
                        </table>
                    </div>
                    <form method="post" id="createProjectCategory" action="{{ route('admin.tenders.tenderSellerSubmit') }}" enctype="multipart/form-data" autocomplete="off" >
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="table-responsive">
                        <table id="mytable" class="table table-bordered">
                            <thead class="row_head">
                                <tr>
                                    <th >@lang('app.sno')</th>
                                    <th colspan="3"></th>
                                    @foreach($supplierlist as $supplier)
                                        <th colspan="2" class="bg-inverse text-center"><input type="radio" class="supplier" required value="{{ $supplier->user_id }}" name="supplier"/> {{ $supplier->name }}</th>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Task</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    @foreach($supplierlist as $supplier)
                                        <th class="text-center">Rate</th>
                                        <th class="text-center">Amount</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody class="colposition">
                            <?php
                                function tenderboqhtml($boqarray){

                                    $user = $boqarray['user'];
                                    $tenders = $boqarray['tenders'];
                                    $level = $boqarray['level'];
                                    $supplierlist = $boqarray['supplierlist'];
                            $level1categoryarray = $boqarray['categoryarray'];
                            $snorow = $boqarray['snorow'];
                            $grandtotal = 0;
                            if($level1categoryarray){
                            $act = 1;    foreach ($level1categoryarray as $level1category){
                            if((int)$level1category->level==0){
                                $snorow = $snorow+1;
                                $actrow = $snorow;
                            }else{
                                $actrow = $snorow.'.'.$act;
                            }
                            ?>
                            <tr class="collpse maincat">
                                <td align="left">{{ $actrow.'.0' }}</td>
                                <td>{{ get_dots_by_level($level1category->level) }}{{ get_category($level1category->category) }}</td>
                                <td></td>
                                <td></td>
                                <?php foreach($supplierlist as $supplier){ ?>
                                <td></td>
                                    <td></td>
                                <?php } ?>
                            </tr>
                            <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('tenders_category',$level1category->id)->orderBy('inc','asc')->get();
                            $catprice = $finalprice =  0;
                            if(count($tenderproductsarray)>0){
                            $tid = 1;  foreach ($tenderproductsarray as $tenderproducts){  ?>
                            <tr   id="rowitem{{ $tenderproducts->id }}">
                                <td align="left">{{ $actrow.'.'.$tid }}</td>
                                <td>{{ get_cost_name($tenderproducts->cost_items) }}</td>
                                <td>{{ $tenderproducts->qty }}</td>
                                <td>{{ get_unit_name($tenderproducts->unit) }}</td>
                                <?php
                                foreach($supplierlist as $supplier){
                                    $finalprice = 0;
                                    $price = 0;
                                    $minQuote = 0;

                                            $q = \App\TenderBidding::where('tender_id', $tenders->id)->where('user_id', $supplier->user_id)->first();

                                if(!empty($q)){
                                        $qp = \App\TenderBiddingProduct::where('tender_product',$tenderproducts->id)->where('tender_id', $tenders->id)->where('bidding_id', $q->id)->first();

                                      if(!empty($qp)){
                                            $price = $qp->price;
                                            $finalprice = $qp->finalprice;
                                            $minQuote = get_min_price($tenders->id, $tenderproducts);
                                        }
                                    }
                                    ?>
                                    <td class="@if($minQuote == $price) alert-success @endif text-center">{{ $price }}</td>
                                    <td class="@if($minQuote == $price) alert-success @endif text-center">{{  !empty($finalprice) ? $finalprice : '-' }}</td>
                                        <?php } ?>
                            </tr>
                            <?php  $tid++;   }   } ?>

                            <?php
                            $newlevel = $level+1;
                            $parent = $level1category->id;
                            $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['supplierlist'] = $supplierlist;
                            $boqarray['categoryarray'] = $level1categoryarray;
                            $boqarray['level'] = $newlevel;
                            $boqarray['parent'] = $parent;
                            $boqarray['snorow'] = $actrow;
                            echo tenderboqhtml($boqarray);


                            $act++;   }

                             }

                                }    ?>


                            <?php

                            $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',0)->where('parent',0)->orderBy('inc','asc')->get();

                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['supplierlist'] = $supplierlist;
                            $boqarray['categoryarray'] = $level1categoryarray;
                            $boqarray['level'] = 0;
                            $boqarray['parent'] = 0;
                            $boqarray['snorow'] = 0;
                            echo tenderboqhtml($boqarray);
                            ?>
                            <tr class="item-row maincat">
                                <td></td>
                                <td>Grand Total</td>
                                <td></td>
                                <td></td>
                                <?php foreach($supplierlist as $supplier){
                                $finalprice = 0;
                                $q = \App\TenderBidding::where('tender_id', $tenders->id)->where('user_id', $supplier->user_id)->first();
                                if($q !== null){
                                    $qp = \App\TenderBiddingProduct::where('tender_id', $tenders->id)->where('bidding_id', $q->id)->sum('finalprice');
                                    $finalprice = $qp;
                                }
                                ?>
                                <td></td>
                                <td>{{ $finalprice }}</td>
                                <?php } ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    </div>

                    <div class="col-sm-12 col-xs-12 text-right">
                        <div class="form-group">
                            <input type="hidden" name="tender_id" id="tender_id" value="{{ $tenders->id }}">
                            <button type="button" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                @lang('app.save')
                            </button>
                            {{ csrf_field() }}
                         </div>
                    </div>
                    </form>
                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.tenders.tenderSellerSubmit')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createProjectCategory').serialize(),
                success: function (response) {
                     // silence
                }
            });
        });
        var r = 0;
        $(".bidupdate").change(function () {
            var tenderproduct = $(this).data('tenderproduct');
            var cat = $(this).data('cat');
            var price = $(this).val();
            var qty = $(".qty"+tenderproduct).html();
            var token = "{{ csrf_token() }}";
            var famount = price*qty;
            $(".finalamount"+tenderproduct).val(famount);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.contractor_bidding_product') }}",
                data: {'_token': token,'tenderproduct': tenderproduct,'price': price},
                success: function(data){
                    if(data.amount>0){
                        $(".finalamount"+tenderproduct).val(data.amount);
                    }
                }
            }).done(function(data){
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".catfinal"+cat).each(function() {
                    sum += parseFloat($(this).val());
                    $(".catamount"+cat).html(sum);
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".grandtotal").each(function() {
                    sum += parseFloat($(this).val());
                    $(".grandamount").html(sum);
                });
            });
        });
        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.tenders.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#rowitem"+id).remove();
                            }
                        }
                    });
                }
            });
        });
        $('.boqtable').dragtable({
            persistState: function(table) {

             }
        });
    </script>
@endpush

