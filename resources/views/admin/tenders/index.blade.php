@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.tenders.create') }}" class="btn btn-outline btn-success btn-sm">@lang('app.add') @lang('app.menu.tenders') <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.title')</th>
                            <th>@lang('app.number')</th>
                            <th>@lang('app.status')</th>
                            <th>@lang('app.startdate')</th>
                            <th>@lang('app.duedate')</th>
                            <th>@lang('app.biddinglist')</th>
                            <th>@lang('app.sendtocontractors')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $x=1;?>
                        @foreach($tendersarray as $tenders)
                            <tr>
                                <td>{{ $x }}</td>
                                <td>{{ $tenders->name }}</td>
                                <td>{{ $tenders->number }}</td>
                                <td>{{ $tenders->status }}</td>
                                <td>{{ $tenders->startdate ? date('d-m-Y',strtotime($tenders->startdate)) : '' }}</td>
                                <td>{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}</td>
                                <td><a href="{{ route('admin.tenders.bidding-list', [$tenders->id]) }}" class="btn1 btn-info  btn-circle"><i class="fa fa-eye"></i> View</a></td>
                                <td><a href="javascript:void(0);" data-tender="{{ $tenders->id }}" class="btn1 btn-info  btn-circle showContractors"><i class="fa fa-list-alt"></i></a></td>
                                <td>
                                    <a href="{{ route('admin.tenders.edit', [$tenders->id]) }}" class="btn1 edit-btn btn-circle"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:;" data-tenders-id="{{ $tenders->id }}" class="btn1 btn-danger  btn-circle sa-params"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <?php $x++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <div class="modal fade bs-modal-md in"  id="ContractorsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" id="modal-data-application">
            <div class="modal-content">
                <form method="post" autocomplete="off" action="" id="setbaselineupdate">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Contractors</span>
                    </div>
                    <div class="modal-body">
                        <div id="showerror"></div>
                        <div class="form-group">
                            <table id="contractors-table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>@lang('app.sno')</th>
                                    <th></th>
                                    <th>@lang('app.name')</th>
                                    <th>@lang('app.link')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input class="tenderid" type="hidden" />
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-success" id="send-mail">Send Mail</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $("#users-table").dataTable();
        "use strict";
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('tenders-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted team!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.tenders.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'DELETE',
                            url: url,
                            data: {'_token': token},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".showContractors").click(function () {
            var id = $(this).attr('data-tender');
            $("#ContractorsModal").modal('show');
            $("#showerror").html("");
            $(".tenderid").val(id);
            var url = "{{ route('admin.tenders.contractorslist',':id') }}";
            url = url.replace(':id', id);
            table  = $('#contractors-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: url,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "initComplete": function(settings, json) {
                    if(json.status=='fail'){
                        $("#showerror").html("<div  class='alert alert-danger' >"+json.message+"</div>");
                         $("#contractors-table_paginate").hide();
                         $("#contractors-table_info").hide();
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    {data: 'checkbox', name: 'checkbox'},
                    {data: 'name', name: 'name'},
                    {data: 'link', name: 'link'}
                ]
            });
        });

            $(document).on("click","#send-mail",function () {
                var arr = $.map($('.contractors:checked'), function(e, i) {
                    return e.value; // +e.value will return NaN for String and non number values
                });
                $("#showerror").html("");
                var contr = arr.join(',');
                if(contr){
                    var url = '{{ route('admin.tenders.sendcontractorsmail') }}';
                    var  tenderid = $(".tenderid").val();
                    var  token = '{{ csrf_token() }}';
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {
                            '_token': token,
                            'tender': tenderid,
                            'contractors': contr
                        },
                        success: function (response) {
                            if (response.success) {
                                $("#showerror").html("<div  class='alert alert-success' >Mail sent Successfully</div>");
                                $("#ContractorsModal").modal('hide');
                            }else{
                                $("#showerror").html("<div  class='alert alert-danger' >"+response.message+"</div>");
                            }
                        }
                    });
                }else{
                    $("#showerror").html("<div  class='alert alert-danger' >Please select atleast one contractor</div>");
                }
            });

    </script>
@endpush