@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 5px !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
            padding:2px;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .row_position {
            overflow-y: auto;
            max-height: 500px;
          /*  display: block;
            width: 100%;*/
        }
        .row_head1 {
            display: table; /* to take the same width as tr */
            width: calc(100% - 17px); /* - 17px because of the scrollbar width */
        }
        thead td {
            font-size: 14px;
            font-weight: bold;
        }
    </style>

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                           Awarded Contract Tender Information
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#home">Details</a></li>
                            <li><a data-toggle="pill" href="#menu1">Task info</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="title"><b>@lang('app.title')</b></label>
                                            <p>{{ $tenderawarded->title }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-2">
                                        <div class="form-group">
                                            <label for="number"><b>@lang('app.number')</b></label>
                                            <p>{{ $tenderawarded->number }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-2">
                                        <div class="form-group">
                                            <label for="title"><b>Status</b></label>
                                            <p>{{ $tenderawarded->status }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-2">
                                        <div class="form-group">
                                            <label for="title"><b>Distribution</b></label>
                                            <p>{{ $tenderawarded->distribution }}</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <label for="duedate"><b>Documents</b></label>
                                    </div>
                                    <div class="col-md-12">
                                        @foreach($files as $file)
                                            <div id="fileid{{ $file->id }}" class="col-md-2 form-group" style="text-align: center;">
                                                <?php   $storage = storage();
                                                switch ($storage){
                                                case 'local': ?>
                                                <img  src="{{ url('public/user-uploads/tender-files/'.$file->tender_id.'/'.$file->hashname) }}">
                                                <?php
                                                break;
                                                case 's3': ?>
                                                <img style="height: 100px; width:100px;" src="{{ $url.'tender-files/'.$file->tender_id.'/'.$file->hashname }}">
                                                <?php
                                                break;
                                                case 'google': ?>
                                                <img src="{{ $file->google_url }}">
                                                <?php
                                                break;
                                                case 'dropbox': ?>
                                                <img src="{{ $file->dropbox_link }}">
                                                <?php
                                                break;
                                                }
                                                ?>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">

                                <div class="row">

                                    <div class="col-sm-12 col-xs-12">
                                        <div class="combo-sheet">
                                            <div class="table-responsive">
                                                <table id="mytable" class="table table-bordered">
                                                    <thead class="row_head">
                                                    <tr>
                                                        <td>Task</td>
                                                        <td>Quantity</td>
                                                        <td>Unit</td>
                                                        <td>Rate</td>
                                                        <td>Total</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="row_position">
                                                    <?php
                                                    $catitem = '';
                                                    $grandtotal = 0;
                                                    $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',1)->orderBy('inc','asc')->get();
                                                    if($level1categoryarray){
                                                    foreach ($level1categoryarray as $level1category){
                                                    $catitem = $level1category->category;
                                                    ?>
                                                    <tr class="item-row maincat">
                                                        <td>{{ get_category($level1category->category) }}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('category',$level1category->category)->orderBy('inc','asc')->get();
                                                    $catprice = $finalprice = 0;
                                                    if(count($tenderproductsarray)>0){
                                                    foreach ($tenderproductsarray as $tenderproducts){
                                                    $tenderbidding = \App\TenderBidding::where('user_id',$user->id)->where("tender_id",$tenders->id)->where("project_id",$tenders->project_id)->first();
                                                    if($tenderbidding){
                                                        $tenderbiddingproduct = \App\TenderBiddingProduct::where('tender_id',$tenderproducts->tender_id)->where('bidding_id',$tenderbidding->id)->where('products',$tenderproducts->cost_items)->where('category',$tenderproducts->category)->first();
                                                        $finalprice = !empty($tenderbiddingproduct) ? $tenderbiddingproduct->finalprice : 0;
                                                    }
                                                    ?>
                                                    <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                                                        <td>{{ get_cost_name($tenderproducts->cost_items) }}</td>
                                                        <td>{{ $tenderproducts->qty }}</td>
                                                        <td>{{ get_unit_name($tenderproducts->unit) }}</td>
                                                        <td><input type="text" class="cell-inp bidupdate" data-cat="{{ $tenderproducts->category }}" data-tenderproduct="{{ $tenderproducts->id }}" value="{{ !empty($tenderbiddingproduct) ? $tenderbiddingproduct->price : 0 }}" ></td>
                                                        <td><input type="text" class="cell-inp grandtotal catfinal{{ $tenderproducts->category }} finalamount{{ $tenderproducts->id }}"  value="{{ $finalprice }}"  ></td>
                                                    </tr>
                                                    <?php
                                                    $catprice += $finalprice;
                                                    $grandtotal += $finalprice;
                                                    }}?>
                                                    <tr class="item-row maincat">
                                                        <td>Sub total</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="catamount{{ !empty($tenderproducts) ? $tenderproducts->category : '' }}">{{ $catprice }}</td>
                                                    </tr>
                                                    <?php
                                                    $level2categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',2)->orderBy('inc','asc')->get();
                                                    if($level2categoryarray){
                                                    foreach ($level2categoryarray as $level2category){
                                                    $catitem .= ','.$level2category->category;
                                                    ?>
                                                    <tr class="item-row subcat">
                                                        <td><span  style="padding-left:10px; !important;">{{ get_category($level2category->category) }}</span></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('category',$catitem)->orderBy('inc','asc')->get();
                                                    $catprice = 0;
                                                    if(count($tenderproductsarray)>0){
                                                    foreach ($tenderproductsarray as $tenderproducts){
                                                    $tenderbidding = \App\TenderBidding::where('user_id',$user->id)->where("tender_id",$tenders->id)->where("project_id",$tenders->project_id)->first();
                                                    $tenderbiddingproduct = \App\TenderBiddingProduct::where('tender_id',$tenderproducts->tender_id)->where('bidding_id',$tenderbidding->id)->where('products',$tenderproducts->cost_items)->where('category',$tenderproducts->category)->first();
                                                    $finalprice = !empty($tenderbiddingproduct) ? $tenderbiddingproduct->finalprice : 0;
                                                    ?>
                                                    <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                                                        <td ><span  style="padding-left:10px; !important;">{{ get_cost_name($tenderproducts->cost_items) }}</span></td>
                                                        <td class="qty{{ $tenderproducts->id }}">{{ $tenderproducts->qty }}</td>
                                                        <td>{{ get_unit_name($tenderproducts->unit) }}</td>
                                                        <td><input type="text" class="cell-inp bidupdate" data-cat="{{ $tenderproducts->category }}" data-tenderproduct="{{ $tenderproducts->id }}" value="{{ !empty($tenderbiddingproduct) ? $tenderbiddingproduct->price : 0 }}" ></td>
                                                        <td><input type="text" class="cell-inp grandtotal catfinal{{ $tenderproducts->category }} finalamount{{ $tenderproducts->id }}"  value="{{ $finalprice }}"  ></td>
                                                    </tr>
                                                    <?php
                                                    $catprice += $finalprice;
                                                    $grandtotal += $finalprice;
                                                    } }?>
                                                    <tr class="item-row maincat">
                                                        <td>Sub total</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="catamount{{ !empty($tenderproducts) ? $tenderproducts->category : '' }}">{{ $catprice }}</td>
                                                    </tr>
                                                    <?php }?>
                                                    <?php }?>
                                                    <?php } }    ?>
                                                    <tr class="item-row maincat">
                                                        <td>Grand Total</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="grandamount">{{ $grandtotal }}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var r = 0;
        $(".bidupdate").change(function () {
            var tenderproduct = $(this).data('tenderproduct');
            var cat = $(this).data('cat');
            var price = $(this).val();
            var qty = $(".qty"+tenderproduct).html();
            var token = "{{ csrf_token() }}";
            var famount = price*qty;
            $(".finalamount"+tenderproduct).val(famount);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.contractor_bidding_product') }}",
                data: {'_token': token,'tenderproduct': tenderproduct,'price': price},
                success: function(data){
                    if(data.amount>0){
                        $(".finalamount"+tenderproduct).val(data.amount);
                    }
                }
            }).done(function(data){
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".catfinal"+cat).each(function() {
                    sum += parseFloat($(this).val());
                    $(".catamount"+cat).html(sum);
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".grandtotal").each(function() {
                    sum += parseFloat($(this).val());
                    $(".grandamount").html(sum);
                });
            });
        });
        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.tenders.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#rowitem"+id).remove();
                            }
                        }
                    });
                }
            });
        });

    </script>
@endpush

