@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')


    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 5px !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
            padding:2px;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .row_position {
            overflow-y: auto;
            max-height: 500px;
          /*  display: block;
            width: 100%;*/
        }
        .row_head1 {
            display: table; /* to take the same width as tr */
            width: calc(100% - 17px); /* - 17px because of the scrollbar width */
        }
        thead td {
            font-size: 14px;
            font-weight: bold;
        }
    </style>

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Bidding List for {{ $tenders->name }}
                        </div>
                        <div class="col-md-6 text-right"  >
                            <a href="{{ route('admin.tenders.bidding-list', [$tenders->id]) }}" style="color:white" class="btn1 edit-btn btn-circle"><i class="fa fa-list-alt"></i> Bid List</a>
                            <a href="{{ route('admin.tenders.bidding-sheet', [$tenders->id]) }}"  style="color:white" class="btn1 btn-info btn-circle"><i class="fa fa-eye"></i> Bidding sheet</a>
                       </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                            <table class="table table-bordered"  id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Sno</th>
                                        <th>Contractor name</th>
                                      {{--  <th>Contractor Link</th>--}}
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($tendercontractsarray)){
                                      $x=1;  foreach ($tendercontractsarray as $tendercontracts){
                                    ?>
                                    <tr>
                                       <td>{{  $x  }}</td>
                                       <td>{{ get_employee_name($tendercontracts->employee_id) }}</td>
                                       {{-- <td><a href="javascript:void(0);" onClick="copyLink('id','{{ $tendercontracts->id }}')" class="btn btn-warning btn-circle" data-toggle="tooltip" data-original-title="Copy Link"><i class="fa fa-copy" aria-hidden="true"></i></a>
                                            &nbsp;<a href="https://api.whatsapp.com/send?text='{{ route('front.tenderContract', [$tendercontracts->id, $tendercontracts->unique_id]) }}'" class="btn btn-success btn-circle white-colour" target="_blank" data-toggle="tooltip" data-original-title="Share Link in Whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                            &nbsp;<a href="javascript:;" class="btn btn-primary btn-circle sendEmail" data-id="{{ $tendercontracts->id }}" data-tendercontract-id="{{ $tendercontracts->id }}" data-toggle="tooltip" data-original-title="Send Email"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                            <br><span id="id{{ $tendercontracts->id }}">{{ route('front.tenderContract', [$tendercontracts->id, $tendercontracts->unique_id]) }}</span></td>--}}
                                        <td><a href="{{ route('admin.tenders.bidding', [$tendercontracts->id]) }}"  style="color:white" class="btn1 btn-info btn-circle"><i class="fa fa-eye"></i>View</a></td>

                                    </tr>
                                <?php $x++; } }?>
                                </tbody>
                            </table>
                        <div class="col-sm-12 col-xs-12 text-right"  >
                            {{ csrf_field() }}
                            <a href="{{ route('admin.tenders.index') }}" class="btn btn-info white-colour">Go Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')


    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $("#dataTable").dataTable();
        function copyLink(id){
            var copyText = document.getElementById(id);
            copyToClipboard(copyText);
        }
        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var r = 0;
        $(".bidupdate").change(function () {
            var tenderproduct = $(this).data('tenderproduct');
            var cat = $(this).data('cat');
            var price = $(this).val();
            var qty = $(".qty"+tenderproduct).html();
            var token = "{{ csrf_token() }}";
            var famount = price*qty;
            $(".finalamount"+tenderproduct).val(famount);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.contractor_bidding_product') }}",
                data: {'_token': token,'tenderproduct': tenderproduct,'price': price},
                success: function(data){
                    if(data.amount>0){
                        $(".finalamount"+tenderproduct).val(data.amount);
                    }
                }
            }).done(function(data){
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".catfinal"+cat).each(function() {
                    sum += parseFloat($(this).val());
                    $(".catamount"+cat).html(sum);
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".grandtotal").each(function() {
                    sum += parseFloat($(this).val());
                    $(".grandamount").html(sum);
                });
            });
        });
        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.tenders.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#rowitem"+id).remove();
                            }
                        }
                    });
                }
            });
        });

    </script>
@endpush

