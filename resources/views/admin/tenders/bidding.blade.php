@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 5px !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
            padding:2px;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .row_position {
            overflow-y: auto;
            max-height: 500px;
          /*  display: block;
            width: 100%;*/
        }
        .row_head1 {
            display: table; /* to take the same width as tr */
            width: calc(100% - 17px); /* - 17px because of the scrollbar width */
        }
        thead td {
            font-size: 14px;
            font-weight: bold;
        }
    </style>

@endpush
@section('content')


    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Bidding View
                        </div>
                        <div class="col-md-6 text-right"  >
                            <a href="{{ route('admin.tenders.bidding-list', [$tenders->id]) }}" style="color:white" class="btn1 btn-info btn-circle"><i class="fa fa-list-alt"></i> Bid List</a>
                         </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">

                <div class="row">
                    {!! Form::open(['id'=>'createTenderPayment','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="title"><b>@lang('app.title')</b></label>
                           <p>{{ $tenders->name }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="number"><b>@lang('app.number')</b></label>
                            <p>{{ $tenders->number }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="title"><b>Status</b></label>
                            <p>{{ $tenders->status }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="duedate"><b>Start date</b></label>
                            <p>{{ $tenders->startdate ? date('d-m-Y',strtotime($tenders->startdate)) : '' }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="duedate"><b>Due date</b></label>
                            <p>{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="duedate"><b>Anticipated Award date</b></label>
                            <p>{{ $tenders->anticipated_date ? date('d-m-Y',strtotime($tenders->anticipated_date)) : '' }}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2">
                        <div class="form-group">
                            <label for="duedate"> <?php if($tenders->include_bid_doc=='1'){ ?><i class="fa fa-check-circle"></i><?php }else{?><i class="fa fa-times"></i><?php }?> <b>Include Bid Documents</b></label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                            <label for="duedate"><b>Documents</b></label>
                    </div>
                    <div class="col-md-12">
                        @foreach($files as $file)
                            <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                @if($file->external_link != '')
                                    <?php $imgurl = $file->external_link;?>
                                @elseif($storage == 'local')
                                    <?php $imgurl = uploads_url().'tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                                @elseif($storage == 's3')
                                    <?php $imgurl = awsurl().'/tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                                @elseif($storage == 'google')
                                    <?php $imgurl = $file->google_url;?>
                                @elseif($storage == 'dropbox')
                                    <?php $imgurl = $file->dropbox_link;?>
                                @endif
                                {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                            </div>
                        @endforeach

                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="bidding_information"><b>Bidding Information</b></label>
                            {!! $tenders->bidding_information !!}
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="project_information"><b>Project Information</b></label>
                            {!! $tenders->project_information !!}
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <h3>Pre Bid RFI Information</h3>
                        <hr>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <h3>Pre Bid WalkThrough Information</h3>
                        <hr>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="prebid_rfi_date">Pre-Bid RFI Deadline</label>
                            <p>{{ $tenders->prebid_rfi_date ? date('d-m-Y',strtotime($tenders->prebid_rfi_date)) : '' }}</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="prebid_walkthrough_date">Pre-Bid WalkThrough Deadline</label>
                            <p>{{ $tenders->prebid_walkthrough_date ? date('d-m-Y',strtotime($tenders->prebid_walkthrough_date)) : '' }}</p>  </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="walkthough_information"><b>Walkthough Information</b></label>
                            {!! $tenders->walkthough_information !!}
                         </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="duedate"><b>Project</b></label>
                            <p>{{ get_project_name($tenders->project_id) }}</p>
                        </div>
                    </div>

                    <?php  if(in_array('sub_projects', $user->modules)) {?>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="duedate"><b>Sub project</b></label>
                            <p>{{ get_title($tenders->title_id) }}</p>
                        </div>
                    </div>
                    <?php }?>
                    <?php  if(in_array('segment', $user->modules)){?>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="duedate"><b>Segment</b></label>
                            <p>{{ get_segment($tenders->segment_id) }}</p>
                        </div>
                    </div>
                    <?php }?>
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>@lang('app.sno')</th>
                                        <th>@lang('app.activity') / @lang('app.task')</th>
                                        <th>@lang('app.quantity')</th>
                                        <th>@lang('app.units')</th>
                                        <th>@lang('app.rate')</th>
                                        <th>@lang('app.total')</th>
                                    </tr>
                                </thead>
                                <tbody id="costitemloop">

                                </tbody>
                            </table>
                    </div>
                    </div>
                    <input type="hidden" name="projectID" id="projectID">
                    <div class="col-sm-12 col-xs-12 text-right"  >
                        {{ csrf_field() }}
                        <a href="{{ route('admin.awardedcontracts.awardedContracts') }}" class="btn1 btn-info btn-circle white-colour"><i class="fa fa-list-alt"></i> Go Back</a>
                    </div>

                    {!! Form::close() !!}
                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        var r = 0;
        $("body").on("change",".bidupdate",function () {
            var tenderproduct = $(this).data('tenderproduct');
            var cat = $(this).data('cat');
            var price = $(this).val();
            var qty = 0;
            qty = $(".qty"+tenderproduct).html();
            var token = "{{ csrf_token() }}";
            var famount = price*qty;
            if(famount){
                $(".finalamount"+tenderproduct).val(famount);
            }
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.contractor_bidding_product') }}",
                data: {'_token': token,'tenderproduct': tenderproduct,'price': price},
                success: function(data){
                    if(data.amount>0){
                        $(".finalamount"+tenderproduct).val(data.amount);
                    }
                }
            }).done(function(data){
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".catfinal"+cat).each(function() {
                    sum += parseFloat($(this).val());
                    $(".catamount"+cat).html(sum);
                });
                var sum = 0;
                //iterate through each textboxes and add the values
                $(".grandtotal").each(function() {
                    sum += parseFloat($(this).val());
                    $(".grandamount").html(sum);
                });
            });
        });
        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.tenders.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#rowitem"+id).remove();
                            }
                        }
                    });
                }
            });
        });
        costitemloop();
        function  costitemloop() {
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var tender = '{{ $tenders->id }}';
            var tendercontract = '{{ $tendercontract->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                url : "{{ route('admin.tenders.biddingloop') }}",
                method: 'POST',
                data: {'_token': token,'projectid': project,'titleid': titleid,'tender': tender,'tendercontract': tendercontract},
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function (response) {
                    $(".preloader-small").hide();
                    $("#costitemloop").html("");
                    $("#costitemloop").html(response);
                }
            });
        }
    </script>
@endpush

