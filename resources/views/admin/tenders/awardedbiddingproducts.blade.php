@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.awardedcontracts.awardedContracts') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding Products</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->

    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                           Awarded contract information
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <table class="table-striped" style="width: 100%;">
                                <tr>
                                    <td style="width: 15%;"><b>Contract Title</b></td>
                                    <td  style="width: 35%;">{{ $awardedcontract->title }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;"><b>@lang('app.number')</b></td>
                                    <td  style="width: 35%;">{{ $awardedcontract->number }}</td>
                                    <td style="width: 15%;"><b>@lang('app.status')</b></td>
                                    <td  style="width: 35%;">{{ $awardedcontract->status }}</td>
                                    <td style="width: 15%;"><b>@lang('app.contractor')</b></td>
                                    <td  style="width: 35%;">{{ $awardedcontract->contractor_id ? get_users_contractor_name($awardedcontract->contractor_id,$awardedcontract->company_id) : '' }}</td>
                                    <td style="width: 15%;"><b>@lang('app.createdon')</b></td>
                                    <td  style="width: 35%;">{{ $awardedcontract->created_at ? date('d-m-Y',strtotime($awardedcontract->created_at)) : '' }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label for="project_id"><b>@lang('app.projects')</b></label>
                                    <select class="select2 form-control projectid" id="projectid"  name="project_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.projects')</option>
                                        @foreach($projectlist as $project)
                                            <option value="{{ $project->id }}" <?php if($awardedcontract->project_id==$project->id){ echo 'selected';}?>>{{ ucwords($project->project_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <?php  if(in_array('sub_projects', $user->modules)) {?>
                            <div class="col-sm-4 col-xs-4" id="subprojectblock" @if(count($titlelist)<=0) style="display: none;" @endif>
                                <div class="form-group">
                                    <label for="title_id">@lang('app.subprojects')</label>
                                    <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.subprojects')</option>
                                        @if($titlelist)
                                            @foreach($titlelist as $title)
                                                <option value="{{ $title->id }}" <?php if($awardedcontract->subproject_id==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <?php  if(in_array('segments', $user->modules)) {?>
                            <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)<=0) style="display: none;" @endif>
                                <div class="form-group">
                                    <label for="title_id">@lang('app.segment')</label>
                                    <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.segment')</option>
                                        @if(count($segmentlist)>0)
                                            @foreach($segmentlist as $segment)
                                                <option value="{{ $segment->id }}" <?php if($awardedcontract->segment_id==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <table id="boqtable" class="table table-bordered default footable-loaded footable boqtable" data-resizable-columns-id="users-table">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Activity / Task</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                <th>Final price</th>
                                            </tr>
                                            </thead>
                                            <tbody class="colposition">
                                            <?php
                                                    function costitemsboq($boqarray){
                                                        $awardedcontract = $boqarray['awardedcontract'];
                                                        $level = $boqarray['level'];
                                                        $parent = $boqarray['parent'];
                                            $level1categoryarray =  $boqarray['categoryarray'];
                                           if($level1categoryarray){
                                            foreach ($level1categoryarray as $level1category){
                                                $subtotal = 0;
                                            ?>
                                            <tr class="maincat">
                                                <td><a href="javascript:void(0);"  class="red remove-item-category" data-catitem="{{ $level1category->category }}" data-catrow="{{ $level1category->id }}" data-toggle="tooltip"  data-original-title="Delete Activity" ><i class="fa fa-trash"></i></a></td>
                                                 <td>{{ get_dots_by_level($level1category->level) }}{{ get_category($level1category->category) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('awarded_category',$level1category->id)->orderBy('inc','asc')->get();
                                            foreach ($tenderproductsarray as $tenderproducts){
                                            ?>
                                            <tr  id="rowitem{{ $tenderproducts->id }}">
                                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                                <td><input  class="cell-inp" type="text"   readonly value="{{ get_cost_name($tenderproducts->cost_items) }}"> </td>
                                                <td><input type="text" class="cell-inp"   readonly value="{{ get_unit_name($tenderproducts->unit) }}"></td>
                                                <td><input type="text" class="cell-inp"  readonly name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                                <td><input type="text" class="cell-inp" readonly value="{{ $tenderproducts->rate }}"></td>
                                                <td><input type="text" class="cell-inp" readonly  value="{{ $tenderproducts->finalrate }}"></td>

                                            </tr>
                                            <?php $subtotal += $tenderproducts->finalrate;
                                            } ?>

                                            <tr class="subcat">
                                                <td></td>
                                                 <td><strong>Sub total</strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('awarded_category',$level1category->id)->sum('finalrate');
                                                    echo $tenderproductsarray;
                                                    ?>
                                                </td>
                                            </tr>
                                           <?php

                                                   $newlevel = $level+1;
                                                   $parent = $level1category->id;
                                           $level1categoryarray =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',$newlevel)->where('parent',$parent)->orderBy('inc','asc')->get();

                                           $boqarray = array();
                                            $boqarray['awardedcontract'] = $awardedcontract;
                                            $boqarray['level'] = $newlevel;
                                            $boqarray['parent'] = $parent;
                                            $boqarray['categoryarray'] = $level1categoryarray;
                                            echo costitemsboq($boqarray); ?>

                                                <?php    }?>

                                            <?php  } }    ?>
                                            <?php
                                           $level1categoryarray =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',0)->where('parent',0)->orderBy('inc','asc')->get();

                                           $boqarray = array();
                                            $boqarray['awardedcontract'] = $awardedcontract;
                                            $boqarray['level'] = 0;
                                            $boqarray['parent'] = 0;
                                           $boqarray['categoryarray'] = $level1categoryarray;
                                            echo costitemsboq($boqarray);
                                            ?>

                                            <tr >
                                                <td></td>
                                                <td><strong>Grand total</strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->sum('finalrate');
                                                    echo $tenderproductsarray;
                                                    ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="projectID" id="projectID">
                            <div class="col-sm-12 col-xs-12 text-right"  >
                                {{ csrf_field() }}
                                <a href="{{ route('admin.awardedcontracts.awardedContracts') }}" class="btn1 btn-info btn-circle white-colour"> Go Back</a>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.tenders.tenderSellerSubmit')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createProjectCategory').serialize(),
                success: function (response) {
                     // silence
                }
            })
        });
        $('#projectid').change(function () {
            var project = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project},
                    success: function (data) {
                        $("#subprojectblock").hide();
                        $("#segmentsblock").hide();
                        $("select#tasklist").html("");
                        $("select#segmentlist").html("");
                        $("select#subprojectlist").html("");
                        $("select#activitylist").html("");
                        if (data.subprojectlist) {
                            $("#subprojectblock").show();
                            $("select#subprojectlist").html(data.subprojectlist);
                            $('select#subprojectlist').select2();
                        }
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        contractproductsloop();
                    }
                });
            }
        });
        $("#subprojectlist").change(function () {
            var project = $("#projectid").select2().val();
            var titlelist = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist},
                    success: function (data) {
                        $("#segmentsblock").hide();
                        $("select#segmentlist").html("");
                        $("select#segmentlist").html("");
                        $("select#tasklist").html("");
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        contractproductsloop();
                    }
                });
            }
        });
    </script>
@endpush

