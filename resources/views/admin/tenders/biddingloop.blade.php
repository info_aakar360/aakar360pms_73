                    <?php

                    $grandtotal = 0;
                    function boqhtml($boqarray){

                            $user = $boqarray['user'];
                            $tenders = $boqarray['tenders'];
                            $contractor = $boqarray['contractor'];
                            $level = $boqarray['level'];
                            $parent = $boqarray['parent'];
                            $unitsarray = $boqarray['unitsarray'];
                            $categoryarray = $boqarray['categoryarray'];
                    $snorow = $boqarray['snorow'];
                                        if(count($categoryarray)>0){
                    $act = 1;   foreach ($categoryarray as $level1category){
                    if((int)$level1category->level==0){
                        $snorow = $snorow+1;
                        $actrow = $snorow;
                    }else{
                        $actrow = $snorow.'.'.$act;
                    }

                                        ?>
                                        <tr class="item-row maincat">
                                            <td align="left">{{ $actrow.'.0' }}</td>
                                         <td>{{ get_dots_by_level($level1category->level) }}{{ get_category($level1category->category) }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        </tr>
                                    <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('tenders_category',$level1category->id)->orderBy('inc','asc')->get();

                                    $catprice = 0;
                                    if(count($tenderproductsarray)>0){
                                    $tid = 1;
                                    foreach ($tenderproductsarray as $tenderproducts){
                                    $finalprice =$rateprice = 0;
                                    if($tenderproducts){
                                        if(!empty($contractor)){
                                            $tenderbidding = \App\TenderBidding::where('user_id',$contractor)->where("tender_id",$tenders->id)->where("project_id",$tenders->project_id)->first();
                                            if($tenderbidding){
                                                $tenderbiddingproduct = \App\TenderBiddingProduct::where('tender_id',$tenderbidding->tender_id)->where('bidding_id',$tenderbidding->id)->where('tender_product',$tenderproducts->id)->first();
                                                $finalprice = !empty($tenderbiddingproduct) ? $tenderbiddingproduct->finalprice : 0;
                                                $rateprice = !empty($tenderbiddingproduct) ? $tenderbiddingproduct->price : 0;
                                            }
                                        }
                                  ?>
                                    <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                                        <td align="left">{{ $actrow.'.'.$tid }}</td>
                                        <td>{{ get_cost_name($tenderproducts->cost_items) }}</td>
                                        <td>{{ $tenderproducts->qty }}</td>
                                        <td>{{ get_unit_name($tenderproducts->unit) }}</td>
                                        <td>{{ $rateprice }}</td>
                                        <td>{{ $finalprice }}</td>
                                        {{--<td><input type="text" class="cell-inp bidupdate" data-cat="{{ $tenderproducts->category }}" data-tenderproduct="{{ $tenderproducts->id }}" value="{{ !empty($tenderbiddingproduct) ? $tenderbiddingproduct->price : 0 }}" ></td>
                                        <td><input type="text" class="cell-inp grandtotal catfinal{{ $tenderproducts->category }} finalamount{{ $tenderproducts->id }}"  value="{{ $finalprice }}"  ></td>--}}
                                    </tr>
                                 <?php
                                 $catprice += $finalprice;
                                 $tid++;  }   }   } ?>
                                <tr class="item-row subcat">
                                    <td></td>
                                    <td>Sub total</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ numberformat($catprice) }}</td>
                                </tr>
                                <?php
                                        $parent = $level1category->id;
                                        $newlevel = $level+1;

                                $categoryarray = \App\TendersCategory::where('tender_id',$tenders->id)->where('level',$newlevel)->where('parent',$parent)->get();

                                $boqarray = array();
                                $boqarray['user'] = $user;
                                $boqarray['tenders'] = $tenders;
                                $boqarray['contractor'] = $contractor;
                                $boqarray['unitsarray'] = $unitsarray;
                                $boqarray['categoryarray'] = $categoryarray;
                                $boqarray['level'] = $newlevel;
                                $boqarray['parent'] = $parent;
                                $boqarray['snorow'] = $actrow;
                                echo  boqhtml($boqarray);
                                $act++;
                            }
                                } }
                                $categoryarray = \App\TendersCategory::where('tender_id',$tenders->id)->where('level',0)->where('parent',0)->get();

                         $boqarray = array();
                         $boqarray['user'] = $user;
                         $boqarray['tenders'] = $tenders;
                         $boqarray['contractor'] = $contractor;
                         $boqarray['unitsarray'] = $unitsarray;
                         $boqarray['categoryarray'] = $categoryarray;
                         $boqarray['level'] = 0;
                         $boqarray['parent'] = 0;
                         $boqarray['snorow'] = 0;
                         echo  boqhtml($boqarray);
                         ?>
                    <?php            $tendergrandamount = 0;
                            if($contractor){
                                $tenderbidding = \App\TenderBidding::where('user_id',$contractor)->where("tender_id",$tenders->id)->where("project_id",$tenders->project_id)->first();
                        if($tenderbidding){
                            $tendergrandamount = \App\TenderBiddingProduct::where('tender_id',$tenders->id)->where('bidding_id',$tenderbidding->id)->sum('finalprice');

                        }

                            }

                                ?>
                            <tr class="item-row maincat">
                                <td></td>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ numberformat($tendergrandamount) }}</td>
                             </tr>