@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="rightsidebarfilter col-md-3" style="display: none;background: #fbfbfb;" id="ticket-filters">
        <div class="col-md-12 m-t-50">
            <h4>@lang('app.filterBy') <a href="javascript:;" class="pull-right toggle-filter"><i class="fa fa-times-circle-o"></i></a></h4>
        </div>

        <div class="col-md-12">
            <div class="form-group" >
                <h5 class="box-title m-t-30">@lang('app.menu.tenders')</h5>
                <select class="select2 form-control" name="tenders" data-placeholder="@lang('app.selecttenders')" id="tender">
                    <option value="">@lang('app.menu.tenders')</option>
                    @foreach($tendersarray as $tenders)
                        <option value="{{ $tenders->id }}">{{ ucwords($tenders->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="duedate">Contractors</label>
                <select class="select2 form-control" name="contractor" data-style="form-control" required  id="contractor">
                    <option value="">Please Select Contractor</option>
                    @foreach($contractorsarray as $contractor)
                        <option value="{{ $contractor->id }}">{{ ucwords($contractor->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <h5 class="box-title m-t-30"> </h5>
            {{ csrf_field() }}
            <button type="button" class="btn btn-success" id="filter-results"><i class="fa fa-check"></i> @lang('app.apply')</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-10">
                        <div class="form-group">
                            <a href="{{ route('admin.awardedcontracts.awardcreate') }}" class="btn btn-outline btn-success btn-sm">@lang('app.menu.awardnewcontracts')<i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-right">
                        <div class="form-group">
                            <a href="javascript:;" id="toggle-filter" class="btn btn-outline btn-danger btn-sm toggle-filter"><i
                                        class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.sno')</th>
                            <th>@lang('app.title')</th>
                            <th>@lang('app.project')</th>
                            <th>@lang('app.contractor')</th>
                            <th>@lang('app.number')</th>
                            <th>@lang('app.status')</th>
                            <th>@lang('app.distribution')</th>
                            <th>@lang('app.addedby')</th>
                            <th>@lang('app.createdon')</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <div class="modal fade bs-modal-md in"  id="ContractorsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" autocomplete="off" action="" id="setbaselineupdate">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Contractors</span>
                    </div>
                    <div class="modal-body">
                        <div id="showerror"></div>
                        <div class="form-group">
                            <table id="contractors-table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Sl no</th>
                                    <th></th>
                                    <th>@lang('app.name')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input class="tenderid" type="hidden" />
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-success" id="send-mail">Send Mail</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        loadTable();
        function loadTable(){

            var tender = $('#tender').val();
            var contractor = $('#contractor').val();

            table = $('#users-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: '{!! route('admin.awardedcontracts.awardeddata') !!}?tender=' + tender+'&contractor=' + contractor,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function( oSettings ) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'title', name: 'title' },
                    { data: 'project_name', name: 'project_name'},
                    { data: 'contractor_id', name: 'contractor_id'},
                    { data: 'number', name: 'number' },
                    { data: 'status', name: 'status' },
                    { data: 'distribution', name: 'distribution' },
                    { data: 'added_by', name: 'addedby'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'action', name: 'action', width: '15%' }
                ]
            });
        }

        $('.toggle-filter').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        });

        $('#filter-results').click(function () {
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
            loadTable();
        });

        $('#reset-filters').click(function () {
            $('#filter-form')[0].reset();
            $('#status').val('all');
            $('.select2').val('all');
            $('#filter-form').find('select').select2();
            loadTable();
            $('#ticket-filters').toggle("slide", {direction: "right" }, 500);
        })

        $(document).on('click','.delete-category',function () {
            var id = $(this).data('cat-id');
            var url = "{{ route('admin.awardedcontracts.deleteAwardContractor',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";

            $.easyAjax({
                type: 'POST',
                url: url,
                data: {'_token': token, '_method': 'DELETE'},
                success: function (response) {
                    if (response.status == "success") {
                        loadTable();
                    }
                }
            });
        });

    </script>
@endpush