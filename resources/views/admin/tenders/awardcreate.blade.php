@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.awardedcontracts.awardedContracts') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('app.menu.awardnewcontracts')</h3>

                <p class="text-muted m-b-30 font-13"></p>
                <div class="row">

                    <ul class="nav nav-pills">
                        <li @if(empty($awardedcontract->id)) class="active" @endif><a data-toggle="pill" href="#home">Details</a></li>
                        <li @if(!empty($awardedcontract->id)) class="active" @endif ><?php if(empty($awardedcontract->id)){?><a href="javascript:void(0);">Task info</a><?php }else{?><a data-toggle="pill" class="" id="taskinfoli" href="#menu1">Task info</a><?php }?></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade @if(empty($awardedcontract->id)) in active @endif">
                            {!! Form::open(['id'=>'createTenderPayment','class'=>'ajax-form','method'=>'POST']) !!}

                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="title">@lang('app.title')</label>
                                    <input type="text" class="form-control" id="title" name="title" value="@if(!empty($awardedcontract->id)) {{ $awardedcontract->title }}   @endif" />
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="number">@lang('app.number')</label>
                                    <input type="text" class="form-control" id="number" name="number" value="@if(!empty($awardedcontract->id)) {{ $awardedcontract->number }}   @endif" />
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="title">Status</label>
                                    <select class="select2 form-control" name="status" data-style="form-control" required>
                                        <option value="">Please Select Status</option>
                                        <option value="Open" @if(!empty($awardedcontract->status)&&$awardedcontract->status=='Open') selected   @endif>Open</option>
                                        <option value="Initiated" @if(!empty($awardedcontract->status)&&$awardedcontract->status=='Initiated') selected   @endif>Initiated</option>
                                        <option value="Not accepted" @if(!empty($awardedcontract->status)&&$awardedcontract->status=='Not accepted') selected   @endif>Not accepted</option>
                                        <option value="Closed" @if(!empty($awardedcontract->status)&&$awardedcontract->status=='Closed') selected   @endif>Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="duedate">Contractors</label>
                                    <select class="select2 form-control" name="contractor" data-style="form-control" required>
                                        <option value="">Please Select Contractor</option>
                                        @foreach($contractorsarray as $contractor)
                                            <option value="{{ $contractor->user_id }}" @if(!empty($awardedcontract->contractor_id)&&$awardedcontract->contractor_id==$contractor->user_id) selected   @endif>{{ ucwords($contractor->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6 form-group">
                                <div class="form-group">
                                    <label for="duedate">Distribution</label>
                                    <select class="select2" name="distribution[]" data-style="form-control" multiple required>
                                        <option value="">Please Select Distribution</option>
                                        <?php if(!empty($awardedcontract->distribution)){
                                            $distribarray = explode(',',$awardedcontract->distribution);
                                        }?>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->user_id }}" @if(!empty($distribarray)&&in_array($employee->user_id,$distribarray)) selected @endif>{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 form-group" >
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box" >
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                <div class="fallback">
                                                    <input name="file[]" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url" type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12 col-xs-12 text-right">
                                <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                                    @lang('app.save')
                                </button>
                                {{ csrf_field() }}
                                <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>

                                <input type="hidden" name="project_id" id="projectID" value="<?php if(!empty($awardedcontract->project_id)){ echo $awardedcontract->project_id; } ?>">
                                <input type="hidden" name="subproject_id" id="subprojectID" value="<?php if(!empty($awardedcontract->subproject_id)){ echo $awardedcontract->subproject_id; } ?>">
                                <input type="hidden" name="segment_id" id="segmentID" value="<?php if(!empty($awardedcontract->segment_id)){ echo $awardedcontract->segment_id; } ?>">
                                 <input type="hidden" name="awardedID" id="awardedID" value="@if(!empty($awardedcontract)){{ $awardedcontract->id }}@endif">

                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div id="menu1" class="tab-pane fade @if(!empty($awardedcontract->id)) in active @endif">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label for="project_id"><b>@lang('app.projects')</b></label>
                                            <select class="select2 form-control projectid" id="projectid" name="project_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.projects')</option>
                                                @foreach($projectlist as $project)
                                                    <option value="{{ $project->id }}" @if(!empty($awardedcontract)&&($awardedcontract->project_id==$project->id)) selected @endif >{{ ucwords($project->project_name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <?php  if(in_array('sub_projects', $user->modules)) {?>
                                    <div class="col-sm-4 col-xs-4" id="subprojectblock" style="display: none">
                                        <div class="form-group">
                                            <label for="title_id">@lang('app.subprojects')</label>
                                            <select class="select2 form-control titlelist" id="subprojectlist" name="title_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.subprojects')</option>
                                                @if($titlelist)
                                                    @foreach($titlelist as $title)
                                                        <option value="{{ $title->id }}" @if(!empty($awardedcontract)&&($awardedcontract->subproject_id==$title->id)) selected @endif >{{ ucwords($title->title) }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <?php  if(in_array('segments', $user->modules)) {?>
                                    <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)>0) style="display: none;" @endif>
                                        <div class="form-group">
                                            <label for="title_id">@lang('app.segment')</label>
                                            <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.segment')</option>
                                                @if(count($segmentlist)>0)
                                                    @foreach($segmentlist as $segment)
                                                        <option value="{{ $segment->id }}" @if(!empty($awardedcontract)&&($awardedcontract->segment_id==$segment->id)) selected @endif >{{ ucwords($segment->name) }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="combo-sheet">
                                                <table id="mytable" class="table table-bordered">

                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>@lang('app.activity') / @lang('app.task') </th>
                                                        <th>@lang('app.units')</th>
                                                        <th>@lang('app.quantity')</th>
                                                        <th>@lang('app.rate')</th>
                                                        <th>@lang('app.amount')</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="colposition" id="contractproductsloop">

                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12 text-right"  >
                                        <a href="{{ route('admin.awardedcontracts.awardedContractsclear') }}" class="btn1 btn-info btn-circle white-colour"><i class="fa fa-list-alt"></i> Save and Go Back</a>
                                    </div>
                                         </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.awardedcontracts.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });
        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = $('#awardedID').val();
            formData.append('awarded_id', ids);
        });
        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.contractCreatedSuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.awardedcontracts.awardcreate') }}';
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });
        function calmarkup(costitem){
            var calvalue = 0;
            var  percentvalue = 0;
            var qty = parseFloat($(".qty"+costitem).val());
            var rate = parseFloat($(".ratevalue"+costitem).val());
            var unit =  $(".unitvalue"+costitem).val();
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{ route('admin.awardedcontracts.updateboqcostitem') }}',
                method: 'POST',
                data: {
                    '_token':token,
                    qty:qty,
                    rate:rate,
                    unit:unit,
                    itemid:costitem
                },
                success: function(response){
                    contractproductsloop();
                }
            });
        }
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.awardedcontracts.awardedstore')}}',
                container: '#createTenderPayment',
                type: "POST",
                redirect: true,
                data: $('#createTenderPayment').serialize(),
                success: function(response){
                     if(response.awardedID){
                         if(myDropzone.getQueuedFiles().length > 0){
                             myDropzone.processQueue();
                         }else{
                             var msgs = "@lang('messages.contractCreatedSuccessfully')";
                             $.showToastr(msgs, 'success');
                             window.location.href = '{{ route('admin.awardedcontracts.awardcreate') }}';
                         }

                     }
                }
            })
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
            });
        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $('#projectid').change(function () {
            var project = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project},
                    success: function (data) {
                        $("#subprojectblock").hide();
                        $("#segmentsblock").hide();
                        $("select#tasklist").html("");
                        $("select#segmentlist").html("");
                        $("select#subprojectlist").html("");
                        $("select#activitylist").html("");
                        if (data.subprojectlist) {
                            $("#subprojectblock").show();
                            $("select#subprojectlist").html(data.subprojectlist);
                            $('select#subprojectlist').select2();
                        }
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        contractproductsloop();
                    }
                });
            }
        });
        $("#subprojectlist").change(function () {
            var project = $("#projectid").select2().val();
            var titlelist = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist},
                    success: function (data) {
                        $("#segmentsblock").hide();
                        $("select#segmentlist").html("");
                        $("select#segmentlist").html("");
                        $("select#tasklist").html("");
                        if (data.segmentlist) {
                            $("#segmentsblock").show();
                            $("select#segmentlist").html(data.segmentlist);
                            $('select#segmentlist').select2();
                        }
                        if (data.activitylist) {
                            $("select#activitylist").html(data.activitylist);
                            $('select#activitylist').select2();
                        }
                        if (data.tasklist) {
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        contractproductsloop();
                    }
                });
            }
        });

        $("#activitylist").change(function () {
            var project = $("#projectid").select2().val();
            var titlelist = $("#subprojectlist").val();
            var activity = $(this).val();
            if (project) {
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.projects.subprojectoptions') }}",
                    data: {'_token': token, 'projectid': project, 'subprojectid': titlelist, 'activity': activity},
                    success: function (data) {
                        if (data.tasklist) {
                            $("select#tasklist").html("");
                            $("select#tasklist").html(data.tasklist);
                            $('select#tasklist').select2();
                        }
                        contractproductsloop();
                    }
                });
            }
        });
        $("#segmentslist").change(function () {
            var awardedcontract = $("#awardedID").val();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $(this).val();
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.awardedcontracts.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'segmentid': segmentid,'awardedcontract': awardedcontract},
                    success: function(data){
                        contractproductsloop();
                    }
                });
            }
        });
        var r = 0;
        $(document).on("change",".costitemrow",function () {
            r++;
            var awardedcontract = $("#awardedID").val();
            var trindex = $(this).closest('tr').index();
            var project = $("#projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $("#segmentslist").find(':selected').val();

            var rowid = $(this).val();
            var catid = $(this).data('cat');
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.awardedcontracts.boqrow') }}",
                data: {
                    '_token': token,
                    'awardedcontract': awardedcontract,
                    'projectid': project,
                    'titleid': titleid,
                    'segmentid': segmentid,
                    'rowid': rowid,
                    'catid': catid
                },
                success: function(data){
                    contractproductsloop();
                }
            });
        });
        var r = 0;
        $(document).on("change",".costitemcatrow",function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentsid = $("#segmentslist").find(':selected').val();
            var level = $(this).data('level');
            var parent = $(this).data('parent');
            var catid = $(this).val();
            if(parent==0){
                var itemid = $("#costitemcatlist option[value='" + catid + "']").attr('data-itemid');
            }else{
                var itemid = $("#costitemcatlist"+parent+" option[value='" + catid + "']").attr('data-itemid');
            }
            var awardedcontract = $("#awardedID").val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.awardedcontracts.boqcatrow') }}",
                data: {
                    '_token': token,
                    'awardedcontract': awardedcontract,
                    'projectid': project,
                    'titleid': titleid,
                    'segmentsid': segmentsid,
                    'itemid': itemid,
                    'catid': catid,
                    'level': level,
                    'parent': parent
                },
                success: function(data){
                    if(data=='failed') {
                        alert('Contract not found. Please try again');
                    }else{
                        contractproductsloop();
                    }
                    /* if(trindex>0){
                         $("#mytable > tbody > tr").eq(trindex-1).after(data);
                     }else{
                         $(".row_position").prepend(data);
                     }*/
                }
            });
        });

        contractproductsloop();
            function contractproductsloop(){
                var contractid = '{{ !empty($awardedcontract) ? $awardedcontract->id :0 }}';
                var projectid = $("#projectid").val();
                if(projectid){
                    var subprojectid = $("#subprojectlist").val();
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        url : "{{ route('admin.awardedcontracts.contractproductsloop') }}",
                        method: 'POST',
                        data: {'_token': token,'contractid': contractid,'projectid': projectid,'subprojectid': subprojectid },
                        beforeSend:function () {
                            $(".preloader-small").show();
                        },
                        success: function (response) {
                            $(".preloader-small").hide();
                            $("#contractproductsloop").html("");
                            $("#contractproductsloop").html(response);
                        }
                    });
                }
            }

        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.awardedcontracts.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                contractproductsloop();
                            }
                        }
                    });
                }
            });
        });
        $('body').on('click', '.remove-item-category', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.awardedcontracts.boqrowcatremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                contractproductsloop();
                            }
                        }
                    });
                }
            });
        });
    </script>
@endpush

