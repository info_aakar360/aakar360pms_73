@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet thead th.bg-inverse {
            font-size: 15px;
            color: #fff;
        }
        .combo-sheet td{
            padding: 5px !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
        .combo-sheet .maincat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #6A6C6B;
            padding:2px;
        }
        .combo-sheet .subcat td {
            font-size: 14px;
            font-weight: bold;
            color: #fff;
            background-color: #A0A1A0;
        }
        .row_position {
            overflow-y: auto;
            max-height: 500px;
          /*  display: block;
            width: 100%;*/
        }
        .row_head1 {
            display: table; /* to take the same width as tr */
            width: calc(100% - 17px); /* - 17px because of the scrollbar width */
        }
        thead td {
            font-size: 14px;
            font-weight: bold;
        }
    </style>

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                           Bidding info
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table-striped" style="width: 100%;">
                            <tr>
                                <td style="width: 15%;"><b>Tender</b></td>
                                <td  style="width: 35%;">{{ $tenders->name }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><b>@lang('app.number')</b></td>
                                <td  style="width: 35%;">{{ $tenders->number }}</td>
                                <td style="width: 15%;"><b>@lang('app.status')</b></td>
                                <td  style="width: 35%;">{{ $tenders->status }}</td>
                                <td style="width: 15%;"><b>@lang('app.contractor')</b></td>
                                <td  style="width: 35%;">{{ $tenderawarded->contractor_id ? get_user_name($tenderawarded->contractor_id) : '' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label for="project_id"><b>@lang('app.projects')</b></label>
                                    <select class="select2 form-control projectid" name="project_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.projects')</option>
                                        @foreach($projectlist as $project)
                                            <option value="{{ $project->id }}" <?php if($tenders->project_id==$project->id){ echo 'selected';}?>>{{ ucwords($project->project_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label for="title_id">@lang('app.subprojects')</label>
                                    <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.subprojects')</option>
                                        @if($titlelist)
                                            @foreach($titlelist as $title)
                                                <option value="{{ $title->id }}" <?php if($tenders->title_id==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)>0) style="display: none;" @endif>
                                <div class="form-group">
                                    <label for="title_id">@lang('app.segment')</label>
                                    <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.segment')</option>
                                        @if(count($segmentlist)>0)
                                            @foreach($segmentlist as $segment)
                                                <option value="{{ $segment->id }}" <?php if($tenders->segment_id==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="combo-sheet">
                                    <div class="table-responsive">
                                        <table id="mytable" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Task</td>
                                                <td>Quantity</td>
                                                <td>Unit</td>
                                                <td>Rate</td>
                                            </tr>
                                            </thead>
                                            <tbody class="row_position">
                                            <?php
                                            $catitem = '';
                                            $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',1)->orderBy('inc','asc')->get();
                                            if($level1categoryarray){
                                            foreach ($level1categoryarray as $level1category){
                                            $catitem = $level1category->category;
                                            ?>
                                            <tr class="item-row">
                                                <td><a href="javascript:void(0);"  class="red remove-item-category" data-catitem="{{ $level1category->category }}" data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Category" ><i class="fa fa-trash"></i></a></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td></td>
                                                <td>{{ get_category($level1category->category) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('category',$level1category->category)->orderBy('inc','asc')->get();
                                            foreach ($tenderproductsarray as $tenderproducts){
                                            ?>
                                            <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td></td>
                                                <td>
                                                    <input  class="cell-inp" list="costitemlist{{ $tenderproducts->id }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
                                                    <datalist  id="costitemlist{{ $tenderproducts->id }}">
                                                        <?php    foreach($costitemsarray as $costitem =>$value){
                                                        $costitemname = get_cost_name($value); ?>
                                                        <option value="{{ $costitem }}"  >{{ $costitemname }}</option>
                                                        <?php   } ?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp" name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                                <td>
                                                    <input  class="cell-inp" list="unitdatacat{{ $tenderproducts->id }}" value="{{ get_unit_name($tenderproducts->unit) }}">
                                                    <datalist id="unitdatacat{{ $tenderproducts->id }}">
                                            <?php    foreach($unitsarray as $units){ ?>
                                                        <option  value="'.$units->id.'" >'.$units->name.'</option>
                                                       <?php     }?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp" name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>

                                            </tr>
                                            <?php }?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <input  class="cell-inp costitemrow" data-cat="{{ $catitem }}" list="costitemlist" placeholder="Task">
                                                    <datalist class="costitemslist"  id="costitemlist">
                                                        <?php  if(!empty($costitemsarray)){
                                                        foreach ($costitemsarray as $key => $value) {
                                                        $costitemname = get_cost_name($value); ?>
                                                        <option value="{{ $key }} " data-name="{{ $costitemname }}" >{{ $costitemname }}</option>
                                                        <?php     } }?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td>
                                                    <input  class="cell-inp" list="unitdatacat" >
                                                    <datalist id="unitdatacat">
                                                        @foreach($unitsarray as $units)
                                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                                        @endforeach
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                            </tr>


                                            <?php
                                            $level2categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',2)->orderBy('inc','asc')->get();
                                            if($level2categoryarray){
                                            foreach ($level2categoryarray as $level2category){
                                            $catitem .= ','.$level2category->category;
                                            ?>
                                            <tr class="item-row">
                                                <td><a href="javascript:void(0);"  class="red remove-item-category" data-catitem="{{ $level2category->category }}"  data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Category" ><i class="fa fa-trash"></i></a></td>
                                                <td></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td>{{ get_category($level2category->category) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('category',$catitem)->orderBy('inc','asc')->get();
                                            foreach ($tenderproductsarray as $tenderproducts){
                                            ?>
                                            <tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
                                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                                <td></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td>
                                                    <input  class="cell-inp" list="costitemlist{{ $tenderproducts->id }}" data-parent="{{ $catitem }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
                                                    <datalist  id="costitemlist{{ $tenderproducts->id }}">
                                                        <?php    foreach($costitemsarray as $costitem =>$value){
                                                        $costitemname = get_cost_name($value); ?>
                                                        <option value="{{ $costitem }}"  >{{ $costitemname }}</option>
                                                        <?php   } ?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp" name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                                <td>
                                                    <input  class="cell-inp" list="unitdatacat{{ $tenderproducts->id }}" value="{{ get_unit_name($tenderproducts->unit) }}">
                                                    <datalist id="unitdatacat{{ $tenderproducts->id }}">
                                                        <?php foreach($unitsarray as $units){ ?>
                                                        <option  value="{{ $units->id }}" >{{ $units->name }}</option>
                                                        <?php } ?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                            </tr>
                                            <?php }?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><i class="fa fa-plus-circle"></i></td>

                                                <td>
                                                    <input  class="cell-inp costitemrow" data-cat="{{ $catitem }}" list="costitemlist" placeholder="Task">
                                                    <datalist class="costitemslist"  id="costitemlist">
                                                        <?php  if(!empty($costitemsarray)){
                                                        foreach ($costitemsarray as $key => $value) {
                                                        $costitemname = get_cost_name($value); ?>
                                                        <option value="{{ $key }} " data-name="{{ $costitemname }}" >{{ $costitemname }}</option>
                                                        <?php     } }?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td>
                                                    <input  class="cell-inp" list="unitdatacat" >
                                                    <datalist id="unitdatacat">
                                                        @foreach($unitsarray as $units)
                                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                                        @endforeach
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                            </tr>
                                            <?php }?>
                                            <?php }?>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td>
                                                    <input  class="cell-inp costitemcatrow" data-parent="{{ $catitem }}" data-level="2"  list="costitemcatlist" placeholder="Task Category">
                                                    <datalist class="costitemscatlist"  id="costitemcatlist">
                                                        <?php  if(!empty($categoryarray)){
                                                        foreach ($categoryarray as $colums) { ?>
                                                        <option value="{{ $colums->id }} " data-name="{{ $colums->title }}" >{{ $colums->title }}</option>
                                                        <?php     } }?>

                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td><input type="text" class="cell-inp"></td>
                                            </tr>
                                            <?php } }    ?>
                                            <tr>
                                                <td></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td></td>
                                                <td>
                                                    <input  class="cell-inp costitemcatrow" data-parent="0" data-level="1"  list="costitemcatlist" placeholder="Task Category">
                                                    <datalist class="costitemscatlist"  id="costitemcatlist">
                                                        <?php  if(!empty($categoryarray)){
                                                        foreach ($categoryarray as $colums) { ?>
                                                        <option value="{{ $colums->id }} " data-name="{{ $colums->title }}" >{{ $colums->title }}</option>
                                                        <?php     } }?>
                                                    </datalist>
                                                </td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td><input type="text" class="cell-inp"></td>
                                                <td><input type="text" class="cell-inp"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="projectID" id="projectID">
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.tenders.tenderSellerSubmit')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createProjectCategory').serialize(),
                success: function (response) {
                     // silence
                }
            })
        });
        $(".projectid").change(function () {
            var project = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.projecttitles') }}",
                    data: {'_token': token,'projectid': project,'tender': tender},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $(".projectid").find(':selected').val();
            var titleid = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'tender': tender},
                    success: function(data){
                        $("select.segmentslist").select2();
                        $("select.segmentslist").html("");
                        $(".costitemslist").html("");
                        $(".costitemscatlist").html("");
                        if(data.segments){
                            $(".segmentblock").show();
                            $("select.segmentslist").html(data.segments);
                        }else{
                            $(".segmentblock").hide();
                        }
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html(data.costcat);

                    }
                });
            }
        });
        $("#segmentslist").change(function () {
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'segmentid': segmentid,'tender': tender},
                    success: function(data){
                        $(".costitemslist").html("");
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html("");
                        $(".costitemscatlist").html(data.costcat);
                    }
                });
            }
        });
        var r = 0;
        $(".costitemrow").change(function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $("#segmentslist").find(':selected').val();
            var rowid = $(this).val();
            var catid = $(this).data('cat');
            var tender = '{{ $tenders->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.boqrow') }}",
                data: {'_token': token,'projectid': project,'titleid': titleid,'segmentid': segmentid,'tender': tender,'showrate': '1','rowid': rowid,'catid': catid},
                success: function(data){
                    if(trindex>0){
                        $("#mytable > tbody > tr").eq(trindex-1).after(data);
                    }else{
                        $(".row_position").prepend(data);
                    }
                    $(".costitemrow").val("");
                }
            });
        });
        var r = 0;
        $(".costitemcatrow").change(function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var level = $(this).data('level');
            var parent = $(this).data('parent');
            var catid = $(this).val();
            var tender = '{{ $tenders->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.boqcatrow') }}",
                data: {'_token': token,'projectid': project,'titleid': titleid,'catid': catid,'tender': tender,'level': level,'parent': parent},
                success: function(data){
                    window.location.reload();
                    $(".costitemcatrow").val("");
                    /* if(trindex>0){
                         $("#mytable > tbody > tr").eq(trindex-1).after(data);
                     }else{
                         $(".row_position").prepend(data);
                     }*/
                }
            });
        });
    </script>
@endpush

