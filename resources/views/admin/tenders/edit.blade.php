@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 0 !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
    </style>

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-md-6">
                    @lang('app.edit') @lang('app.menu.tenders')
                    </div>
                    <div class="col-md-6 text-right"  >
                            <a href="{{ route('admin.tenders.edit', [$tenders->id]) }}" style="color:white" class="btn1 edit-btn btn-circle"><i class="fa fa-edit"></i> Edit</a>
                            <a href="{{ route('admin.tenders.editcostitem', [$tenders->id]) }}" style="color:white" class="btn1 btn-info btn-circle"><i class="icon-settings"></i> Task</a>
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                    {!! Form::open(['id'=>'createTenderPayment','class'=>'ajax-form','autocomplete'=>'off','method'=>'PUT']) !!}
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="title">@lang('app.title')</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $tenders->name }}">
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="number">@lang('app.number')</label>
                            <input type="text" class="form-control" id="number" name="number"  value="{{ $tenders->number }}">
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="title">Status</label>
                            <select class="select2 form-control" name="status" data-style="form-control" required>
                                <option value="">Please Select Status</option>
                                <option value="Open" <?php if($tenders->status=='Open'){ echo 'selected';}?>>Open</option>
                                <option value="Initiated" <?php if($tenders->status=='Initiated'){ echo 'selected';}?>>Initiated</option>
                                <option value="Not accepted" <?php if($tenders->status=='Not accepted'){ echo 'selected';}?>>Not accepted</option>
                                <option value="Closed" <?php if($tenders->status=='Closed'){ echo 'selected';}?>>Closed</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="duedate">Start date</label>
                            <input type="text" class="form-control datepicker" id="startdate" name="start_date" value="{{ $tenders->startdate ? date('d-m-Y',strtotime($tenders->startdate)) : '' }}">
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="duedate">Due date</label>
                            <input type="text" class="form-control datepicker" id="duedate"  name="due_date"  value="{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}">
                            <input type="checkbox" name="accept_submition" value="1" <?php if($tenders->accept_submition=='1'){ echo 'checked';}?> > Accept Submittion after due date
                        </div>
                    </div>
                        <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="duedate">Distribution</label>
                            <?php $distributionsarray = explode(',',$tenders->distribution);?>
                            <select class="select2" name="distribution[]" data-style="form-control" multiple required>
                                <option value="">Please Select Distribution</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->user_id }}" <?php if(in_array($employee->user_id,$distributionsarray)){ echo 'selected';}?>>{{ ucwords($employee->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="duedate">Enable Blind Biding</label>
                            <input type="checkbox" name="enable_blind" value="1"  <?php if($tenders->enable_blind=='1'){ echo 'checked';}?>  >
                        </div>
                        <div class="form-group">
                            <label for="duedate">Include bid Documents</label>
                            <input type="checkbox" name="include_bid_doc" value="1"  <?php if($tenders->include_bid_doc=='1'){ echo 'checked';}?>  >
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label for="count_email">Countdown Emails</label>
                            <input type="text" class="form-control" id="count_email" value="{{ $tenders->count_email }}" name="count_email">
                            <input type="checkbox" name="send_count_emails" value="1"  <?php if($tenders->send_count_emails=='1'){ echo 'checked';}?>  > Send Count down emails
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div class="form-group">
                            <label for="anticipateddate">Anticipated Award date</label>
                            <input type="text" class="form-control datepicker" id="anticipateddate" name="anticipated_date"  value="{{ $tenders->anticipated_date ? date('d-m-Y',strtotime($tenders->anticipated_date)) : '' }}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        @foreach($files as $file)
                            <div id="fileid{{ $file->id }}" class="col-md-1" style="text-align: center;">
                                @if($file->external_link != '')
                                    <?php $imgurl = $file->external_link;?>
                                @elseif($storage == 'local')
                                    <?php $imgurl = uploads_url().'tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                                @elseif($storage == 's3')
                                    <?php $imgurl = awsurl().'/tender-files/'.$file->tender_id.'/'.$file->hashname;?>
                                @elseif($storage == 'google')
                                    <?php $imgurl = $file->google_url;?>
                                @elseif($storage == 'dropbox')
                                    <?php $imgurl = $file->dropbox_link;?>
                                @endif
                                {!! mimetype_thumbnail($file->hashname,$imgurl)  !!}
                                <span class="fnt-size-10">{{ $file->created_at->diffForHumans() }}</span>
                                <a href="javascript:;" onclick="removeFile({{ $file->id }})" style="text-align: center;">
                                    Remove
                                </a>
                            </div>
                        @endforeach

                    </div>
                    <div class="col-sm-12 col-xs-12 form-group" >
                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                        <div id="file-upload-box" >
                            <div class="row" id="file-dropzone">
                                <div class="col-md-12">
                                    <div class="dropzone dropheight"   id="file-upload-dropzone">
                                        <div class="fallback">
                                            <input name="file[]" type="file" multiple/>
                                        </div>
                                        <input name="image_url" id="image_url"type="hidden" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="bidding_information">Bidding Information</label>
                            <textarea class="summernote" name="bidding_information" id="bidding_information" >{!! $tenders->bidding_information !!}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="project_information">Project Information</label>
                            <textarea class="summernote" name="project_information" id="project_information" >{!! $tenders->project_information !!}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <h3>Pre Bid RFI Information</h3>
                        <hr>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="enable_prebid_rfi">Pre-Bid RFI Deadline:</label>
                            <input type="checkbox" name="enable_prebid_rfi" value="1" <?php if($tenders->enable_prebid_rfi=='1'){ echo 'checked';}?> >
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="prebid_rfi_date">Pre-Bid RFI Deadline</label>
                            <input type="text" class="form-control datepicker" id="prebid_rfi_date" name="prebid_rfi_date" value="{{ $tenders->prebid_rfi_date ? date('d-m-Y',strtotime($tenders->prebid_rfi_date)) : '' }}">
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <h3>Pre Bid WalkThrough Information</h3>
                        <hr>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="enable_prebid_walkthrough">Pre-Bid WalkThrough Deadline:</label>
                            <input type="checkbox" name="enable_prebid_walkthrough" value="1" <?php if($tenders->enable_prebid_walkthrough=='1'){ echo 'checked';}?> >
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="prebid_walkthrough_date">Pre-Bid WalkThrough Deadline</label>
                            <input type="text" class="form-control datepicker" id="prebid_walkthrough_date" name="prebid_walkthrough_date"  value="{{ $tenders->prebid_walkthrough_date ? date('d-m-Y',strtotime($tenders->prebid_walkthrough_date)) : '' }}">
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="walkthough_information">Walkthough Information</label>
                            <textarea class="summernote" name="walkthough_information" id="walkthough_information" >{!! $tenders->walkthough_information !!}</textarea>
                        </div>
                    </div>


                            <div class="col-sm-12 col-xs-12 text-right">
                        <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.update')
                        </button>
                        {{ csrf_field() }}
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                        <input name="tenderID" id="tenderID" type="hidden" />
                    </div>
                    {!! Form::close() !!}
                </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        projectID = '';
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.tenders.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });
        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = $('#tenderID').val();
            formData.append('tender_id', ids);
        });
        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.tenderUpdated')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.tenders.index') }}'
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $(".projectid").change(function () {
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $(".projectid").find(':selected').val();
            var titleid = $(this).val();
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid},
                    success: function(data){
                        console.log(data);
                        $(".costitemslist").html("");
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html("");
                        $(".costitemscatlist").html(data.costcat);
                    }
                });
            }
        });

        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.tenders.update',[$tenders->id])}}',
                container: '#createTenderPayment',
                type: "POST",
                redirect: true,
                data: $('#createTenderPayment').serialize(),
                beforeSend:function () {
                    $(".preloader-small").show();
                },
                success: function(response){
                    $(".preloader-small").hide();
                    if(myDropzone.getQueuedFiles().length > 0){
                        tenderID = response.tenderID;
                        $('#tenderID').val(response.tenderID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.tenderUpdated')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.tenders.index') }}'
                    }
                }
            })
        });
        var r = 0;
        $(".costitemrow").change(function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var rowid = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.boqrow') }}",
                data: {'_token': token,'projectid': project,'titleid': titleid,'rowid': rowid},
                success: function(data){
                    if(trindex>0){
                        $("#mytable > tbody > tr").eq(trindex-1).after(data);
                    }else{
                        $(".row_position").prepend(data);
                    }
                    $(".costitemrow").val("");
                }
            });
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
            });
        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        //    update inspection
        function removeFile(id) {
            var url = "{{ route('admin.tenders.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        $("#fileid"+id).remove();
                    }
                }
            })

        };
    </script>
@endpush