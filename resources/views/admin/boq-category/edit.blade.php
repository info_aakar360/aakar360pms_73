<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Activity</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqActivity','class'=>'ajax-form','method'=>'PUT']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Activity Name</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $activity->title }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Parent Activity</label>
                        <select name="parent" id="parent" class="form-control">
                            <option value="">Please select parent activity</option>
                            <?php  ?>
                            @foreach($categories as $cat)
                                <?php
                                $selected = '';
                                if($activity->parent !== NULL){
                                if($cat->id == $activity->parent){
                                    $selected = 'selected';
                                } }?>
                                <option value="{{ $cat->id }}" <?=$selected; ?>>{{ $cat->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-activity" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createBoqActivity').submit(function () {
        $.easyAjax({
            url: '{{route('admin.activity.update', [$activity->id])}}',
            container: '#editBoqActivity',
            type: "POST",
            data: $('#editBoqActivity').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    });
    $('#save-activity').click(function () {
        $.easyAjax({
            url: '{{route('admin.activity.update', [$activity->id])}}',
            container: '#editBoqActivity',
            type: "POST",
            data: $('#editBoqActivity').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>