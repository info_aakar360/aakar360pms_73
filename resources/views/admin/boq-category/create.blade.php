<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('app.activity')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead>
                <tr>
                    <th>@lang('app.sno')</th>
                    <th>@lang('app.activity')</th>
                    <th>@lang('app.parent')</th>
                    <th>@lang('app.action')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($categories as $key=>$category)
                    <tr id="cat-{{ $category->id }}">
                        <td>{{ $key+1 }}</td>
                        <td>{{ ucwords($category->title) }}</td>
                        <td>{{ get_category($category->parent) }}</td>
                        <td><a href="javascript:;" data-cat-id="{{ $category->id }}" class="btn btn-sm btn-danger btn-rounded delete-category">@lang("app.remove")</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3">@lang('messages.noTaskCategory')</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <hr>

        {!! Form::open(['id'=>'createBoqActivity','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Activity Name</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Activity Name" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Parent Activity</label>
                        <select name="parent" id="parent" class="form-control">
                            <option value="0">Please select parent Activity</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $("#dataTable").dataTable();

    $('#createBoqActivity').submit(function () {
        $.easyAjax({
            url: '{{route('admin.activity.store')}}',
            container: '#createBoqActivity',
            type: "POST",
            data: $('#createBoqActivity').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#taskActivityModal').modal('hide');
                    window.location.href = '{{ route('admin.activity.index') }}';
                }
            }
        });
        return false;
    })


    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.activity.store')}}',
            container: '#createProjectActivity',
            type: "POST",
            data: $('#createBoqActivity').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    $('#taskActivityModal').modal('hide');
                    window.location.href = '{{ route('admin.activity.index') }}';
                }
            }
        })
    });
    $('#taskCategoryModal').on('click','.delete-category',function () {
        var cat  = $(this).attr('data-cat-id');
        var url = "{{ route('admin.activity.destroy',':id') }}";
        url = url.replace(':id', cat);
        var token = "{{ csrf_token() }}";
        var dataObject = {'_token': token, '_method': 'DELETE'};
        $.easyAjax({
            type: 'POST',
            url: url,
            data: dataObject,
            success: function (response) {
                if (response.status == "success") {
                     $("#cat-"+cat).remove();
                }
            }
        });
    });
</script>