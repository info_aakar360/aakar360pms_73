<ul class="chatonline style-none userList p-0">
    <?php
  $x=1;  foreach($conversationarray as $conversation){
         switch ($conversation['module']){
             case 'chat':  ?>
                        <li id="userchat_{{$conversation['chatid']}}" class="@if($x==1) active @endif">
                            <a href="javascript:void(0)" id="dpa_{{$conversation['chatid']}}" onclick="getChatBox('{{ $conversation['chatid'] }}')">
                                <img src="{{ $conversation['image'] }}" alt="user-img"   class="img-circle">
                                <span  class="font-bold"> {{ ucwords($conversation['name']) }}
                                 {{--<small class="text-simple"> @if($users->last_message){{  \Carbon\Carbon::parse($users->last_message)->diffForHumans()}} @endif
                                         <label class="btn btn-{{ \Illuminate\Support\Facades\Request::is('pulse') ? 'success' : 'outline' }} btn-xs btn-outline">{{ ucwords($users->user_type) }}</label>
                                        </small>--}}
                                    </span>
                            </a>
                        </li>
              <?php    break;
             case 'project':  ?>
                    <li id="projectpulse_{{$conversation['projectid']}}" class="@if($x==1) active @endif">
                        <a href="javascript:void(0)" id="dpa_{{$conversation['projectid']}}" onclick="getPulseProjectBox('{{$conversation['projectid'] }}')">
                            <img src="{{ $conversation['image'] }}" alt="user-img"   class="img-circle">
                            <span class="font-bold" >{{ ucwords($conversation['name']) }}</span>
                        </a>
                    </li>
              <?php    break;
             case 'group':  ?>
                    <li id="groupchat_{{$conversation['groupid']}}" class="@if($x==1) active @endif">
                        <a href="javascript:void(0)" id="dpa_{{$conversation['groupid']}}" onclick="getGroupChatBox('{{ $conversation['groupid'] }}')">
                            <img src="{{ $conversation['image'] }}" alt="user-img"   class="img-circle">
                            <span class="font-bold" >{{ ucwords($conversation['name']) }}</span>
                        </a>
                    </li>
              <?php    break;
         } $x++; } ?>
</ul>

