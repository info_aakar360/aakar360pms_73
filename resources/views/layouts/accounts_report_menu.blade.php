<div>
    <style>
        .icon-bar {
            width: 100%;
            overflow: auto;
            padding-bottom: 10px;
            float: right;
        }
        .icon-bar a {
            float: left;
            width: 9%;
            text-align: center;
            padding: 8px 0;
            transition: all 0.3s ease;
            color: black;
            font-size: 12px;
        }
        .icon-bar a:hover {
            background-color: #002f76;
            color: white;
        }
        .btn-blue, .btn-blue.disabled {
            background: #002f76;
            border: 1px solid #002f76;
            margin-right: 5px;
        }
        div .icon-bar a.active {
            background-color: #002f76;
            color: white;
        }
        .btn-blue.btn-outline {
            color: #002f76;
            background-color: transparent;
        }
    </style>
    <div class="icon-bar">
        <a class="btn btn-outline btn-blue btn-sm" id="Bom" href="{{ route('admin.reports.accounts.ledger') }}">Ledger</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Stock" href="{{ route('admin.reports.accounts.trial_balance') }}">Trial Balance</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Indents" href="{{ route('admin.reports.accounts.cost_of_revenue') }}">Cost of Revenue</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.profit_or_loss_account') }}">P&L account</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.retained_earnings') }}">Retained Earnings</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.fixed_asset_schedule') }}">Fixed asset schedule</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.balance_sheet') }}">Balance sheet</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.cash_flow') }}">Cash flow</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.receive_payment') }}">Receive payment</a>
        <a class="btn btn-outline btn-blue btn-sm" id="Pi" href="{{ route('admin.reports.accounts.notes') }}">Notes</a>
    </div>
</div>