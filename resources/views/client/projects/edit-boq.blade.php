@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('member.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('member.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('member.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('member.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="white-box">
                                    <div class="row">
                                        <form method="post" action="{{ route('member.projects.updateBoq', [$id, $title->id]) }}">
                                        <div class="col-lg-12">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <?php
                                                    $titlename = \App\Title::get();
                                                    ?>
                                                    <div class="col-lg-5" style="float: left;">
                                                        <select name="old_title" class="form-control" id="">
                                                            <option value="">Please select Title</option>
                                                            @foreach($titlename as $category)
                                                                <?php
                                                                    $selected = '';
                                                                    if($category->id == $pc->title){
                                                                        $selected = 'selected';
                                                                    }
                                                                ?>
                                                                <option value="{{ $category->title }}" {{ $selected }}>{{ $category->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">&nbsp;</div>
                                            <div class="row">
                                                <?php $cat = json_decode($pc->category); ?>
                                                @if(isset($cat))
                                                @foreach($cat as $c)
                                                    <div class="col-lg-3">
                                                        <select class="form-control" name="category[]">
                                                            <option value="">Please select category</option>
                                                            @foreach($categories as $category)
                                                                <?php
                                                                    $selected = '';
                                                                    if($category->id == $c){
                                                                        $selected = 'selected';
                                                                    }
                                                                ?>
                                                                <option value="{{ $category->id }}" {{ $selected }}>{{ $category->title }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @endforeach
                                                @endif
                                            </div>
                                            <div class="row">&nbsp;</div>
                                                <table class="table">
                                                <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>Category Name</td>
                                                    <td>Brand Name</td>
                                                    <td>Unit</td>
                                                    <td>Quantity</td>
                                                    <td>Wastage</td>
                                                    <td>Rate</td>
                                                    <td>Cost</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                            @foreach($ci as $c)
                                                <?php
                                                    $ciname = \App\CostItems::where('id',$c->cost_items_id)->first();
                                                    $cp = \App\ProjectCostItemsProduct::where('cost_items_id',$ciname->id)->where('project_id',$id)->get();
                                                     ?>
                                                <tr>
                                                    <td colspan="8">{{ $ciname->cost_item_name }}</td>
                                                </tr>
                                                @foreach($cp as $key=>$propro)

                                                    <?php $ciid = $propro->cost_items_id;
                                                    $pc = $propro->product_category_id;
                                                    $pb = $propro->product_brand_id;
                                                    $pu = $propro->unit;
                                                    $pcn = \App\ProductCategory::where('id',$pc)->first();
                                                    $pbn = \App\ProductBrand::where('id',$pb)->first();
                                                    $pun = \App\Units::where('id',$pu)->first();
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="project_id" class="form-control" value="{{ $id }}">
                                                            <input type="hidden" name="cost_item_id[{{ $key }}]" class="form-control" value="{{ $ciid }}">{{ $key+1 }}
                                                        </td>
                                                        <td><input type="hidden" name="product_category_id[{{ $key }}]" class="form-control" value="{{ $pc }}">{{ $pcn->name }}</td>
                                                        <td><input type="hidden" name="product_brand_id[{{ $key }}]" class="form-control" value="{{ $pb }}">{{ $pbn->name }}</td>
                                                        <td><input type="hidden" name="units[{{ $key }}]" class="form-control" value="{{ $pu }}">{{ $pun->name }}</td>
                                                        <td><input type="text" name="qty[{{ $key }}]" id="qty{{ $key }}" class="form-control" value="{{ $propro->qty }}"></td>
                                                        <td><input type="text" name="wastage[{{ $key }}]" class="form-control" value="{{ $propro->wastage }}"></td>
                                                        <td><input type="text" data-cat-id="{{ $key }}" onchange="getCost({{ $key }});" id="rate{{ $key }}" name="rate[{{ $key }}]" value="{{ $propro->rate }}" class="form-control"></td>
                                                        <td><input type="text" name="cost[{{ $key }}]" id="cost{{ $key }}" class="form-control" value="{{ $propro->cost }}" readonly></td>
                                                    </tr>
                                                @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                                <div class="row">
                                                    <div class="col-lg-12" style="float: right;">
                                                        <input type="submit" class="btn btn-primary" name="submit" value="Save" style="float: right;">
                                                    </div>
                                                </div>

                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
        </div>
    </div>
    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(document).on('click', '#saveProduct', function(){
            alert($('#productCostItem').serialize());
            $.easyAjax({
                url: '{{ route('member.projects.addCostItems') }}',
                container: '#productCostItem',
                type: "POST",
                data: $('#productCostItem').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
        });

        function getChild(val,id) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getChild') }}",
                data: {'_token': token, 'category_id': val, 'id':id},
                success: function(data){
                    if(data !== '') {
                        $("#child").append(data);
                        getLavel(val);
                    }else{
                        getLavel(val);
                    }
                }
            });
        }

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function getProduct(val) {
            var token = "{{ csrf_token() }}";
            var id = "{{ $id }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getProduct') }}",
                data: {'_token': token, 'cost_item_id': val, 'id': id},
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');

        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#rate'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#cost'+id).val(total);
        }

    </script>
@endpush