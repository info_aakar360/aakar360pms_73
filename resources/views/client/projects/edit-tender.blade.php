@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('member.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('member.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('member.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('member.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form method="post" id="productCostItem" enctype="multipart/form-data">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="project_id" value="{{ $pid }}"/>
                        <input type="hidden" name="sourcing_package_id" value="{{ $id }}"/>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" name="tender_name" class="form-control" placeholder="Tender Name" value="{{ $tender->name }}">
                            </div>
                        </div>
                        <div class="col-lg-12">&nbsp;</div>
                        <div class="row">
                            <div class="col-xs-12 ">
                                <div class="form-group">
                                    @foreach($cons as $con)
                                    <div id="sortable">
                                        <div class="col-xs-12 item-row margin-top-5">
                                            <div class="col-md-6" style="float: left; padding-right: 20px;">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="input-group col-md-12">
                                                            <select name="conditions[]" id="cost_item_lavel" class="form-control col-md-12">
                                                                <option value="">Please select Conditions</option>
                                                                <?php  ?>
                                                                @foreach($conditions as $category)

                                                                    <option value="{{ $category->id }}" <?php if($category->id == $con->condition_id){ echo 'selected'; } ?>>{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" name="terms[]" id="title" class="form-control" placeholder="Terms" value="{{ $con->terms }}">
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="col-xs-12 m-t-5">
                                        <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">&nbsp;</div>
                        <div class="col-lg-4" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                            <div style="padding: 5px;"><b>Product</b></div>
                        </div>
                        <div class="col-lg-4" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                            <div style="padding: 5px;"><b>Quantity</b></div>
                        </div>
                        <div class="col-lg-4" style="padding-left: 30px; border: 1px solid #d0e5f5;">
                            <div style="padding: 5px;"><b>Brand</b></div>
                        </div>
                        <div class="col-lg-12">
                            <?php
                            $cf = json_decode($package->create_from);
                            $p  = 1;
//                            dd($pros);
                            foreach ($pros as $key=>$product) {
                            ?>
                            <div class="col-lg-4" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 50px;">
                                <label style="padding: 5px;">
                                    <?php $cat = \App\ProductCategory::where('id', $product->products)->first(); //dd($cat); ?>
                                    <input type="hidden" name="products[{{ $key }}]" value="{{ $product->products }}" class="form-check-input" checked>
                                    @if($cat){{ $cat->name }}@endif
                                </label>
                            </div>
                            <div class="col-lg-4" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 50px;">
                                <input type="hidden" name="qty[{{ $key }}]" value="{{ $product->qty }}" class="form-check-input">
                                <label style="padding: 5px;">
                                    {{ $product->qty }}
                                </label>
                            </div>
                            <div class="col-lg-4 option-row" style="padding-left: 30px; border: 1px solid #d0e5f5; min-height: 50px;">
                                <?php
                                $bex = explode(',', $product->brands);
                                foreach($bex as $be){
                                    $brand = \App\ProductBrand::where('id',$be)->first();
                                ?>
                                    <span class="tm-tag tm-tag-info" id="BVrkl_1"><span>{{ $brand->name }}</span><a href="#" class="tm-tag-remove" id="BVrkl_Remover_1" tagidtoremove="1">x</a></span>
                                <?php } ?>
                                <input type="text" name="brands[{{ $key }}]" placeholder="Type here.." class="typeahead{{ $key }} tm-input{{ $key }} form-control tm-input-info"/>
                                <input type="hidden" id="bids{{$key}}" name="brand_ids[{{ $key }}]" value=""/>
                            </div>

                            <?php } //}?>
                        </div>

                        <div class="col-lg-12" id="productData"></div>
                        <div class="col-md-12" style="text-align: right;">&nbsp;</div>
                        <!--/span-->
                        <div class="col-md-12">
                            <b>Files</b>
                            <br>
                            <div class="row" id="list">
                                <ul class="list-group" id="files-list">
                                    @foreach($files as $file)
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-9">
                                                {{ $file->filename }}
                                            </div>
                                            <div class="col-md-3">
                                                <?php $rfi = \App\Tenders::where('id',$file->tender_id)->first(); ?>
                                                @if($file->external_link != '')
                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 'local')
                                                    <a target="_blank" href="{{ asset_url('tender-files/'.$rfi->name.'/'.$file->hashname) }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 's3')
                                                    <a target="_blank" href="{{ $url.'tender-files/'.$rfi->name.'/'.$file->filename }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'google')
                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'dropbox')
                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @endif

                                                {{--@if(is_null($file->external_link))--}}
                                                {{--<a href="{{ route('member.task-files.download', $file->id) }}"--}}
                                                {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                {{--class="btn btn-inverse btn-circle"><i--}}
                                                {{--class="fa fa-download"></i></a>--}}
                                                {{--@endif--}}

                                                <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                   class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                    </li>

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box" >
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone"
                                                 id="file-upload-dropzone">
                                                {{ csrf_field() }}
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url"type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="taskID" id="taskID">
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: right;">
                            <input type="button" name="submit" class="btn btn-success saveProduct" value="Submit" id="saveProduct">
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/bootstrap3-typeahead.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.js"></script>

    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('member.projects.storeTenderFiles') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#taskID').val();
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('messages.taskCreatedSuccessfully')";
            $.showToastr(msgs, 'success');
            window.location.href = "{{ route('member.projects.sourcingPackages', [$id]) }}"

        });
        $(document).on('click', '#saveProduct', function(){
            $.easyAjax({
                url: "{{ route('member.projects.saveTenders',[$id]) }}",
                container: '#productCostItem',
                type: "POST",
                data: $('#productCostItem').serialize(),
                success: function (data) {
                    $('#storeTask').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = data.taskID;
                        $('#taskID').val(data.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('Tender Created Successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = "{{ route('member.punch-items.index') }}"
                    }
                }
            })
        });

        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }

        function getProduct(val) {
            var token = "{{ csrf_token() }}";
            var id = "{{ $id }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getProduct') }}",
                data: {'_token': token, 'cost_item_id': val, 'id': id},
                success: function(data){
                    $("#productData").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Sourcing').addClass('tab-current');

        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#rate'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#cost'+id).val(total);
        }

        $('#add-item').click(function () {
            var item = '<div class="col-xs-12 item-row margin-top-5">'
                +'<div class="col-md-5" style="float: left; padding-right: 20px;">'
                +   '<div class="form-group">'
                +       '<select name="conditions[]" class="form-control">'
                +           '<option value="">Please select Conditions</option>';
                            @foreach($conditions as $cat)
                            item += '<option value="{{ $cat->id }}">{{ $cat->name }}</option>';
                            @endforeach
                item += '</select>'
                +   '</div>'
                +'</div>'
                +'<div class="col-md-6" style="float: left; padding-right: 20px;">'
                +   '<div class="form-group">'
                +       '<input type="text" name="terms[]" id="title" class="form-control" placeholder="Terms">'
                +   '</div>'
                +'</div>'
                +'<div class="col-md-1 text-right visible-md visible-lg">'
                +   '<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
                +   '<div class="row">'
                +       '<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
                +   '</div>'
                +'</div>'
                +'</div>';
            $(item).hide().appendTo("#sortable").fadeIn(500);
        });

        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

        $(document).ready(function() {
            var tags = $(".tm-input").tagsManager();
            var token = "{{ csrf_token() }}";
            jQuery(".typeahead").typeahead({
                source: function (query, process) {
                    return $.get("{{ route('member.projects.getBrandsTag') }}", { query: query }, function (data) {
                        data = $.parseJSON(data);
                        return process(data);
                    });
                },
                afterSelect :function (item){
                    tags.tagsManager("pushTag", item);
                }
            });
        });

        $(document).on('click','.tm-tag-remove', function () {
            $(this).closest('.tm-tag-info').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

    </script>

    <script>
        <?php foreach ($packageProducts as $key=>$product){?>
        $(document).ready(function() {
            var tags = $(".tm-input{{ $key }}").tagsManager();
            var token = "{{ csrf_token() }}";
            jQuery(".typeahead{{ $key }}").typeahead({
                source: function (query, process) {
                    return $.get("{{ route('member.projects.getBrandsTag') }}", { query: query }, function (data) {
                        data = $.parseJSON(data);
                        return process(data);
                    });
                },
                afterSelect :function (item){
                    tags.tagsManager("pushTag", item.name);
                    var bids = $("#bids{{ $key }}").val();
                    var bidarr = bids.split(',');
                    bidarr.push(item.id);
                    var newArray = bidarr.filter(function(v){return v!==''});
                    $("#bids{{ $key }}").val(newArray.join(','));
                }
            });
        });
        <?php } ?>
    </script>
@endpush