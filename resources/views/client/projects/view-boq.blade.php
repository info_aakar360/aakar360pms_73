@extends('layouts.member-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('member.projects.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.details')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <style>
        tr.hide-table-padding td {
            padding: 0;
        }

        .expand-button {
            position: relative;
        }

        .accordion-toggle .expand-button:after
        {
            position: absolute;
            left:.75rem;
            top: 50%;
            transform: translate(0, -50%);
            content: '-';
        }
        .accordion-toggle.collapsed .expand-button:after
        {
            content: '+';
        }
    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section>
                <div class="sttabs tabs-style-line">
                    <div class="white-box">
                        <nav>
                            <ul>
                                <li class="tab-current"><a href="{{ route('member.projects.show', $project->id) }}"><span>@lang('modules.projects.overview')</span></a>
                                </li>

                                @if(in_array('employees',$modules))
                                    <li><a href="{{ route('member.project-members.show', $project->id) }}"><span>@lang('modules.projects.members')</span></a></li>
                                @endif

                                @if(in_array('tasks',$modules))
                                    <li><a href="{{ route('member.tasks.show', $project->id) }}"><span>@lang('app.menu.tasks')</span></a></li>
                                @endif

                                <li><a href="{{ route('member.projects.showFiles', $project->id) }}"><span>@lang('modules.projects.files')</span></a></li>
                                @if(in_array('timelogs',$modules))
                                    <li><a href="{{ route('member.time-log.show-log', $project->id) }}"><span>@lang('app.menu.timeLogs')</span></a></li>
                                @endif
                                <li class="Boq">
                                    <a href="{{ route('member.projects.boq', $project->id) }}"><span>BOQ</span></a>
                                </li>
                                <li class="Scheduling">
                                    <a href="{{ route('member.projects.scheduling', $project->id) }}"><span>Scheduling</span></a>
                                </li>
                                <li class="Sourcing">
                                    <a href="{{ route('member.projects.sourcingPackages', $project->id) }}"><span>Sourcing Packages</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="white-box" style="padding-top: 0; padding-bottom: 0;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <h4>{{ $title->title }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">

                    <div class="card">
                        <div class="card-header" id="heading">
                            <div class="col-sm-4">
                                Name
                            </div>
                            <div class="col-sm-8">
                                <div class="col-sm-3">
                                    Unit
                                </div>
                                <div class="col-sm-3">
                                    Qty
                                </div>
                                <div class="col-sm-3">
                                    Rate
                                </div>
                                <div class="col-sm-3">
                                    Total Value
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="post" action="{{ route('member.projects.ProjectFinalQty',[$id]) }}">
                        @csrf
                        <?php $k = 0; ?>
                    @foreach($projectproduct as $propro)
                        <?php

                        $tv = DB::table('project_cost_items_product')
                            ->select(DB::raw('SUM(cost) as total_cost'))
                            ->where('title', $propro->title)
                            ->where('project_id', $propro->project_id)
                            ->groupBy('title')
                            ->get();

                        $tq = DB::table('project_cost_items_product')
                            ->select(DB::raw('count(*) as total_count'))
                            ->where('title', $propro->title)
                            ->where('project_id', $propro->project_id)
                            ->groupBy('title')
                            ->get();

                        $catparent = explode(',',$propro->category);


                        $ctq = \Illuminate\Support\Arr::pluck(DB::table('project_cost_items_product')->whereRaw("FIND_IN_SET($catparent[$k], category)")->where('project_id', $propro->project_id)
                            ->get(), 'cost_items_id');
//                            dd($ctq);

                        $cunique = array_unique($ctq);

                        $tatm = DB::table('project_cost_items_product')
                            ->select(DB::raw('sum(cost) as total_amount'))
                            ->where('title', $propro->title)
                            ->where('project_id', $propro->project_id)
                            ->groupBy('title')
                            ->first();

                        ?>
                    <div class="accordion" id="accordionExample{{ $propro->id }}">
                        <div class="card">
                            {{--<div class="card-header" id="heading">--}}
                                {{--<div class="col-sm-4" style="border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD;">--}}
                                    {{--<h2 class="mb-0 mt-0" style="margin: 0;">--}}
                                        {{--<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $propro->id }}" aria-expanded="true" aria-controls="collapse">--}}
                                            {{--<i class="fa fa-angle-down rotate-icon"></i> {{ get_cat_name($catparent[0]) }}--}}
                                        {{--</button>--}}
                                    {{--</h2>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-8" style=" border-bottom: 1px solid #DDDDDD;">--}}
                                    {{--<div class="col-sm-3" style="border-right: 1px solid #DDDDDD;">--}}
                                        {{--<span style="line-height: 36px">&nbsp;</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-3" style="border-right: 1px solid #DDDDDD;">--}}
                                        {{--<span style="line-height: 36px">&nbsp;</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-3" style="border-right: 1px solid #DDDDDD;">--}}
                                        {{--<span style="line-height: 36px">&nbsp;</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-3">--}}
                                        {{--<span style="line-height: 36px">&nbsp;</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div id="collapse{{ $propro->id }}" class="collapse in" aria-labelledby="heading" data-parent="#{{ $propro->id }}">
                                @if(isset($catparent))
                                {{--@for($i=1; $i<count($catparent);$i++)--}}
                                    <div class="card-body">
                                        <div class="card-header col-sm-12" id="heading" style="border-bottom: 1px solid #DDDDDD;">
                                            <div class="col-sm-4" style=" border-right: 1px solid #DDDDDD;">
                                                <h2 class="mb-0 mt-0" style="margin: 0;">
                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $catparent[$k] }}" aria-expanded="true" aria-controls="collapse">
                                                        <i class="fa fa-angle-down rotate-icon"></i> {{ get_cat_name($catparent[$k]) }}
                                                    </button>
                                                </h2>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="col-sm-3" style=" border-right: 1px solid #DDDDDD;">
                                                    <span style="line-height: 36px">&nbsp;</span>
                                                </div>
                                                <div class="col-sm-3" style=" border-right: 1px solid #DDDDDD;">
                                                    <span style="line-height: 36px">&nbsp;</span>
                                                </div>
                                                <div class="col-sm-3" style=" border-right: 1px solid #DDDDDD;">
                                                    <span style="line-height: 36px">&nbsp;</span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span style="line-height: 36px">&nbsp;</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse{{ $catparent[$k] }}" class="collapse in col-sm-12" aria-labelledby="heading" data-parent="#{{ $catparent[$k] }}">
                                                <?php $cii = $propro->title;
                                                    $coitem = \App\ProjectCostItemsProduct::where('title', $cii)->groupBy('cost_items_id')->get();

                                                ?>
                                            @foreach($coitem as $coi)
                                                <?php
                                                    $catname = \App\CostItems::where('id',$coi->cost_items_id)->first();
                                                    $qtq = DB::table('project_cost_items_product')
                                                        ->select(DB::raw('count(*) as total_count'))
                                                        ->where('title', $propro->title)
                                                        ->where('project_id', $propro->project_id)
                                                        ->groupBy('cost_items_id')
                                                        ->first();

                                                    $atm = DB::table('project_cost_items_product')
                                                        ->select(DB::raw('sum(cost) as total_amount'))
                                                        ->where('title', $propro->title)
                                                        ->where('project_id', $propro->project_id)
                                                        ->groupBy('cost_items_id')
                                                        ->first();

                                                    $unit = \App\Units::where('id', $catname->unit)->first();

                                                    $cipfq = \App\ProjectCostItemsFinalQty::where('project_id',$propro->project_id)
                                                        ->where('title', $propro->title)
                                                        ->where('cost_item_id', $catname->id)
                                                        ->where('category', $catparent[$k])->first();
                                                ?>
                                                <div class="card-body">
                                                    <div class="col-sm-4" style="border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; height: 40px;">
                                                        <span class="mb-0 mt-0" style="margin-left: 30px;">
                                                            {{ $catname->cost_item_name }}
                                                            <input type="hidden" class="form-control" name="cost_item_id[{{ $catparent[$k].$catname->id.$coi->id }}]" value="{{ $catname->id }}">
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-3" style="border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; height: 40px;">
                                                                <span class="mb-0 mt-0" style="">{{ $unit->name }}</span>
                                                        </div>
                                                        <input type="hidden" class="form-control" name="category[{{ $catparent[$k].$catname->id.$coi->id }}]" value="{{ $catparent[$k] }}">
                                                        <input type="hidden" class="form-control" name="project_id" value="{{ $propro->project_id }}">
                                                        <input type="hidden" class="form-control" name="title" value="{{ $propro->title }}">
                                                        <div class="col-sm-3" style="border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; height: 40px;">

                                                            <span class="mb-0 mt-0" style=""><input type="text" class="form-control" <?php
                                                                if($cipfq){?>
                                                                value="{{ $cipfq->qty }}"
                                                                <?php }
                                                                ?> name="total_qty[{{ $catparent[$k].$catname->id.$coi->id }}]" id="qty{{ $catparent[$k].$catname->id.$coi->id }}" onchange="getCost({{ $catparent[$k].$catname->id.$coi->id }});"></span>
                                                        </div>
                                                        <div class="col-sm-3" style="border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; height: 40px;">
                                                            <span class="mb-0 mt-0" style="">{{ $atm->total_amount }}<input type="hidden" class="form-control" name="amount" id="amt{{ $catparent[$k].$catname->id.$coi->id }}" value="{{ $atm->total_amount }}"></span>
                                                        </div>
                                                        <div class="col-sm-3" style=" border-bottom: 1px solid #DDDDDD; height: 40px;">
                                                            <span class="mb-0 mt-0" style=""><input type="text" readonly class="form-control" <?php
                                                                if($cipfq){ ?>
                                                                value="{{ $cipfq->total_amount }}"
                                                                <?php }
                                                                ?> name="total_amount[{{ $catparent[$k].$catname->id.$coi->id }}]" id="tamt{{ $catparent[$k].$catname->id.$coi->id }}"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                        <?php $k++; ?>
                    @endforeach
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="submit" name="submit" value="Save" class="btn btn-primary" style="float: right;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    <!-- .row -->
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    @if($global->locale == 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}-AU.min.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.{{ $global->locale }}.min.js"></script>
    @endif
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        function getCost(val) {
            var id = val;
            var qty = $('#qty'+id).val();
            var rate = $('#amt'+id).val();
            var total = (parseFloat(qty)*parseFloat(rate)).toFixed(2);
            $('#tamt'+id).val(total);
        }
        function getLavel(val) {
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('member.projects.getItemLavel') }}",
                data: {'_token': token, 'category_id': val},
                success: function(data){
                    $("#cost_item_lavel").html(data);
                }
            });
        }
        $('ul.showProjectTabs .Boq').addClass('tab-current');
    </script>
@endpush