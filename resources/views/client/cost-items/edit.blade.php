@extends('layouts.client-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')

@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
        {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'PUT']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Task Name</label>
                        <input type="text" name="cost_item_name" id="cost_item_name" class="form-control" value="{{ $category->cost_item_name }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Select BOQ Categories</label>
                        <div id="sortable">
                            <div class="col-xs-12 item-row margin-top-5">
                                @foreach($costlavel as $cl)

                                <div class="col-md-12" style="float: left; padding-right: 20px;">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                <select name="cost_item_lavel[]" id="cost_item_lavel" class="form-control col-md-12">
                                                    <option value="">Please select boq category</option>

                                                    @foreach($categories as $cate)
                                                        <?php $selected = '';
                                                        if($cl->boq_category_id == $cate->id){
                                                            $selected = 'selected';
                                                        }?>
                                                        <option value="{{ $cate->id }}" <?=$selected; ?>>{{ $cate->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i> @lang('modules.invoices.addItem')</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Task Description</label>
                        <textarea name="cost_item_description" id="cost_item_description" class="summernote">{{ $category->cost_item_description }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <select name="unit" id="unit" class="form-control col-md-12">
                            <option value="">Please select unit</option>
                            @foreach($units as $u)
                                <?php $selected = '';
                                    if($category->unit == $u->id){
                                        $selected = 'selected';
                                    }
                                    ?>
                                <option value="{{ $u->id }}" {{ $selected }}>{{ $u->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">&nbsp;</div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <select name="type" id="type" class="form-control col-md-12">
                            <option value="">Please select type</option>
                            <option value="Workforce" <?php if( $category->type == 'Workforce'){ echo 'selected'; } ?>>Workforce</option>
                            <option value="Equipment" <?php if( $category->type == 'Equipment'){ echo 'selected'; } ?>>Equipment</option>
                            <option value="Material" <?php if( $category->type == 'Material'){ echo 'selected'; } ?>>Material</option>
                            <option value="Commitment" <?php if( $category->type == 'Commitment'){ echo 'selected'; } ?>>Commitment</option>
                            <option value="Owner Cost" <?php if( $category->type == 'Owner Cost'){ echo 'selected'; } ?>>Owner Cost</option>
                            <option value="Professional Services" <?php if( $category->type == 'Professional Services'){ echo 'selected'; } ?>>Professional Services</option>
                            <option value="Other" <?php if( $category->type == 'Other'){ echo 'selected'; } ?>>Other</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
                <div class="row">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
</div>
@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('client.cost-items.update', [$category->id])}}',
            container: '#createBoqCategory',
            type: "PUT",
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })



    $('#save-category').click(function () {
        alert($('#createBoqCategory').serialize());
        $.easyAjax({
            url: '{{route('client.cost-items.update', [$category->id])}}',
            container: '#createProjectCategory',
            type: "PUT",
            data: $('#createBoqCategory').serialize(),

            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    $('#add-item').click(function () {
        var item = '<div class="col-xs-12 item-row margin-top-5">'
            +'<div class="col-md-8" style="float: left; padding-right: 20px;">'
            +'<div class="form-group">'
            +'<select name="cost_item_lavel[]" class="form-control">'
            +'<option value="">Please select boq category</option>';
                @foreach($cats as $cat)
                    item += '<option value="{{ $cat->id }}">{{ $cat->title }}</option>';
                @endforeach
                    item += '</select>'
            +'</div>'
            +'</div>'
            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortable").fadeIn(500);
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault()
        $form = $(evt.target).closest('form')
        $form[0].reset()
        $form.find('select').selectpicker('render')
    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
</script>
@endpush