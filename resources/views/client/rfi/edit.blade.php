@extends('layouts.client-app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('client.punch-items.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-client/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Update RFI</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">RFI</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" value="{{ $punchitem->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Subject</label>
                                        <input type="text" name="subject" class="form-control" placeholder="Subject *" value="{{ $punchitem->subject }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">RFI Manager</label>
                                        <select class="selectpicker form-control" name="rfi_manager" id="user_id">
                                            <option value="">Choose RFI Manager</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $punchitem->rfi_manager){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="selectpicker form-control" name="assign_to[]" id="user_id" multiple>
                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                <?php
                                                    $ass = explode(',',$punchitem->assign_to);
                                                    if (in_array($employee->id, $ass)){
                                                        echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <?php
                                        $exd = explode('-',$punchitem->due_date);
                                        ?>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" value="{{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open" <?php if($punchitem->status == 'Open'){ echo 'selected'; } ?>>Open</option>
                                            <option value="Resolved" <?php if($punchitem->status == 'Resolved'){ echo 'selected'; } ?>>Resolved</option>
                                            <option value="Inprogress" <?php if($punchitem->status == 'Inprogress'){ echo 'selected'; } ?>>Inprogress</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Responsible Contractor</label>
                                        <select class="selectpicker form-control" name="rec_contractor" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $punchitem->rfi_manager){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received From</label>
                                        <select class="selectpicker form-control" name="received_from" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $punchitem->rfi_manager){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution List
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                <?php
                                                    $ass = explode(',',$punchitem->distribution);
                                                    if (in_array($employee->id, $ass)){
                                                        echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" name="location" class="form-control" placeholder="Location *" value="{{ $punchitem->location }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Drawing Number</label>
                                        <input type="text" name="drawing_no" class="form-control" placeholder="Drawing Number *" value="{{ $punchitem->drawing_no }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Spec Section</label>
                                        <input type="text" name="spec_section" class="form-control" placeholder="Spec Section *" value="{{ $punchitem->spec_section }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="privete">Private or Public</label><br>
                                        <input id="privete" name="private" value="1" type="checkbox">
                                        <label for="privete">Private</label>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Reference
                                        </label>
                                        <input type="text" name="reference" class="form-control" value="{{ $punchitem->reference }}">
                                    </div>
                                </div>


                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Question</label>
                                        <textarea id="description" name="question" class="form-control summernote">{{ $punchitem->question }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <b>Files</b>
                                    <br>
                                    <div class="row" id="list">
                                        <ul class="list-group" id="files-list">
                                    @foreach($files as $file)
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    {{ $file->filename }}
                                                </div>
                                                <div class="col-md-3">
                                                    <?php $rfi = \App\Rfi::where('id',$file->rfi_id)->first(); ?>
                                                    @if($file->external_link != '')
                                                        <a target="_blank" href="{{ $file->external_link }}"
                                                           data-toggle="tooltip" data-original-title="View"
                                                           class="btn btn-info btn-circle"><i
                                                                    class="fa fa-search"></i></a>

                                                    @elseif(config('filesystems.default') == 'local')
                                                        <a target="_blank" href="{{ asset_url('task-files/'.$rfi->title.'/'.$file->hashname) }}"
                                                           data-toggle="tooltip" data-original-title="View"
                                                           class="btn btn-info btn-circle"><i
                                                                    class="fa fa-search"></i></a>

                                                    @elseif(config('filesystems.default') == 's3')
                                                        <a target="_blank" href="{{ $url.'task-files/'.$rfi->title.'/'.$file->filename }}"
                                                           data-toggle="tooltip" data-original-title="View"
                                                           class="btn btn-info btn-circle"><i
                                                                    class="fa fa-search"></i></a>
                                                    @elseif(config('filesystems.default') == 'google')
                                                        <a target="_blank" href="{{ $file->google_url }}"
                                                           data-toggle="tooltip" data-original-title="View"
                                                           class="btn btn-info btn-circle"><i
                                                                    class="fa fa-search"></i></a>
                                                    @elseif(config('filesystems.default') == 'dropbox')
                                                        <a target="_blank" href="{{ $file->dropbox_link }}"
                                                           data-toggle="tooltip" data-original-title="View"
                                                           class="btn btn-info btn-circle"><i
                                                                    class="fa fa-search"></i></a>
                                                    @endif

                                                    {{--@if(is_null($file->external_link))--}}
                                                    {{--<a href="{{ route('client.task-files.download', $file->id) }}"--}}
                                                    {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                    {{--class="btn btn-inverse btn-circle"><i--}}
                                                    {{--class="fa fa-download"></i></a>--}}
                                                    {{--@endif--}}

                                                    <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                    class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                                    <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                                </div>
                                            </div>
                                        </li>

                                    @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url"type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="taskID" id="taskID">
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                        </div>

                        <div class="form-actions">
                            <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-client/dist/dropzone.js') }}"></script>


    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('client.rfi.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = '{{ $punchitem->id }}';
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('RFI Updated successfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('client.rfi.index') }}'

        });

        //    update task
        $('#update-task').click(function () {

            $.easyAjax({
                url: '{{route('client.rfi.updateRfi', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('RFI Updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('client.rfi.index') }}'
                    }
                }
            })

        });

        //    update task
        function removeFile(id) {
            var url = "{{ route('client.rfi.removeFile',':id') }}";
            url = url.replace(':id', id);

            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        window.location.reload();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('client.rfi.updateRfi', [$punchitem->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = response.taskID;
                        $('#taskID').val(response.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('RFI Updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('client.rfi.index') }}'
                    }
                }
            })
        }

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('client.rfi.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#list ul.list-group').html(response.html);

                            }
                        }
                    });
                }
            });
        });

        $('#project_id').change(function () {
            var id = $(this).val();

            // For getting dependent task
            var dependentTaskUrl = '{{route('client.all-tasks.dependent-tasks', [':id', ':taskId'])}}';
            dependentTaskUrl = dependentTaskUrl.replace(':id', id);
            dependentTaskUrl = dependentTaskUrl.replace(':taskId', '{{ $punchitem->id }}');
            $.easyAjax({
                url: dependentTaskUrl,
                type: "GET",
                success: function (data) {
                    $('#dependent_task_id').html(data.html);
                }
            })
        });

    </script>
@endpush
