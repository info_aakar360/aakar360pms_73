@extends('layouts.client-app')
@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
            <li><a href="{{ route('client.rfi.index') }}">{{ __($pageTitle) }}</a></li>
            <li class="active">@lang('app.edit')</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection
 @push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-client/dist/dropzone.css') }}">

<style>
    .panel-black .panel-heading a,
    .panel-inverse .panel-heading a {
        color: unset!important;
    }
</style>

@endpush
@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-inverse">
            <div class="panel-heading"> RFI Details</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <tr>
                                        <td style="width: 15%;"><b>@lang('app.title')</b></td>
                                        <td  style="width: 35%;">{{ $punchitem->title }}</td>
                                        <td style="width: 15%;"><b>Subject</b></td>
                                        <td  style="width: 35%;">{{ $punchitem->subject }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>RFI Manager</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_user_name($punchitem->rfi_manager)) }}
                                        </td>
                                        <td style="width: 15%;"><b>Assigned To</b></td>
                                        <td  style="width: 35%;">
                                            <?php $as = explode(',',$punchitem->assign_to);
                                            for($i = 0; $i < count($as); $i++ ){
                                            ?>
                                                {{ ucwords(get_user_name($as[$i])) }},
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Due Date</b></td>
                                        <td  style="width: 35%;">
                                            <?php
                                            if($punchitem->due_date){
                                            $exd = explode('-',$punchitem->due_date);
                                            ?>
                                            {{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}
                                            <?php } ?>
                                        </td>
                                        <td style="width: 15%;"><b>Responsible Contractor</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_user_name($punchitem->rec_contractor)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Received From</b></td>
                                        <td  style="width: 35%;">
                                            {{ ucwords(get_user_name($punchitem->received_from)) }}
                                        </td>
                                        <td style="width: 15%;"><b>Distribution List</b></td>
                                        <td  style="width: 35%;">
                                            <?php $as = explode(',',$punchitem->distribution);
                                            for($i = 0; $i < count($as); $i++ ){
                                            ?>
                                            {{ ucwords(get_user_name($as[$i])) }},
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Location</b></td>
                                        <td  style="width: 35%;">{{ $punchitem->location }}</td>
                                        <td style="width: 15%;"><b>Drawing No.</b></td>
                                        <td  style="width: 35%;">{{ $punchitem->drawing_no }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Spec Section</b></td>
                                        <td  style="width: 35%;">{{ $punchitem->spec_section }}</td>
                                        <td style="width: 15%;"><b>Public or Private</b></td>
                                        <td  style="width: 35%;"><?php if($punchitem->private == '1'){ echo 'Private';}else{ echo 'Public'; }?> </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Status</b></td>
                                        <td  style="width: 35%;">
                                            <?php if($punchitem->status == 'Open'){ echo 'Open'; } ?>
                                            <?php if($punchitem->status == 'Resolved'){ echo 'Resolved'; } ?>
                                            <?php if($punchitem->status == 'Inprogress'){ echo 'Inprogress'; } ?>
                                        </td>
                                        <td style="width: 15%;"><b>Added By</b></td>
                                        <td  style="width: 35%;">{{ ucwords(get_user_name($punchitem->added_by)) }} </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;"><b>Question</b></td>
                                        <td colspan="3" style="width: 85%;">{!! $punchitem->question !!} </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12">
                                <b>Files</b>
                                <br>
                                <div class="row" id="list">
                                    <ul class="list-group" id="files-list">
                                        @foreach($files as $file)
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        {{ $file->filename }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?php $rfi = \App\Rfi::where('id',$file->rfi_id)->first(); ?>
                                                        @if($file->external_link != '')
                                                            <a target="_blank" href="{{ $file->external_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 'local')
                                                            <a target="_blank" href="{{ asset_url('task-files/'.$rfi->title.'/'.$file->hashname) }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>

                                                        @elseif(config('filesystems.default') == 's3')
                                                            <a target="_blank" href="{{ $url.'task-files/'.$rfi->title.'/'.$file->filename }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'google')
                                                            <a target="_blank" href="{{ $file->google_url }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @elseif(config('filesystems.default') == 'dropbox')
                                                            <a target="_blank" href="{{ $file->dropbox_link }}"
                                                               data-toggle="tooltip" data-original-title="View"
                                                               class="btn btn-info btn-circle"><i
                                                                        class="fa fa-search"></i></a>
                                                        @endif

                                                        <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                                    </div>
                                                </div>
                                            </li>

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-12">&nbsp;</div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" onclick="showDiv()"><b>All Replies <i class="fa fa-caret-down"></i> </b></label>
                                    <div class="col-md-12 hide" id="replyDiv">
                                        @foreach($replies as $reply)
                                            <p>Added By: <b>{{ ucfirst(get_user_name($reply->added_by)) }}</b> <br>{{ $reply->created_at->diffForHumans() }}
                                                <span style="padding-left: 10px;">{!! $reply->comment !!}</span>
                                                <br>
                                            </p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Reply</label>
                                    <textarea id="description" name="comment" class="form-control summernote"></textarea>
                                </div>
                            </div>
                            <?php
                            $uid = \Illuminate\Support\Facades\Auth::user()->id;
                            $urole = \App\RoleUser::where('user_id',$uid)->first();
                            $userRole = \App\Role::where('id',$urole->role_id)->first();
                            if (
                            $punchitem->added_by == \Illuminate\Support\Facades\Auth::user()->id
                            || $punchitem->rfi_manager == \Illuminate\Support\Facades\Auth::user()->id
                            || $punchitem->received_from == \Illuminate\Support\Facades\Auth::user()->id
                            || $userRole->name == 'admin'){ ?>
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label class="control-label">Status
                                    </label>
                                    <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                        <option value="">Please Select Status</option>

                                        <option value="Open" <?php if($punchitem->status == 'Open'){ echo 'selected'; } ?>>Open</option>
                                        <option value="Resolved" <?php if($punchitem->status == 'Resolved'){ echo 'selected'; } ?>>Resolved</option>
                                        <option value="Inprogress" <?php if($punchitem->status == 'Inprogress'){ echo 'selected'; } ?>>Inprogress</option>

                                    </select>
                                </div>
                            </div>
                            <?php } ?>



                        </div>
                        <!--/row-->

                    </div>
                    <div class="form-actions">
                        <button type="button" id="update-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- .row -->

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->.
</div>
{{--Ajax Modal Ends--}}
@endsection
 @push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-client/dist/dropzone.js') }}"></script>


<script>

function showDiv() {
    $('#replyDiv').toggleClass('hide', 'show');
}

    //    update task
    $('#update-task').click(function () {
        $.easyAjax({
            url: '{{route('client.rfi.replyPost', [$punchitem->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                    var msgs = "@lang('Reply posted successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.href = '{{ route('client.rfi.index') }}'
            }
        })
    });

    //    update task
    function removeFile(id) {
        var url = "{{ route('client.punch-items.removeFile',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";
        $.easyAjax({
            url: url,
            container: '#updateTask',
            type: "POST",
            data: {'_token': token, '_method': 'DELETE'},
            success: function(response){
                if (response.status == "success") {
                    window.location.reload();
                }
            }
        })

    };

    function updateTask(){
        $.easyAjax({
            url: '{{route('client.rfi.replyPost', [$punchitem->id])}}',
            container: '#updateTask',
            type: "POST",
            data: $('#updateTask').serialize(),
            success: function(response){
                var msgs = "@lang('Reply posted successfully')";
                $.showToastr(msgs, 'success');
                window.location.href = '{{ route('client.rfi.index') }}'
            }
        })
    }

    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('body').on('click', '.sa-params', function () {
        var id = $(this).data('file-id');
        var deleteView = $(this).data('pk');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = "{{ route('client.punch-items.destroy',':id') }}";
                url = url.replace(':id', id);

                var token = "{{ csrf_token() }}";

                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            $.unblockUI();
                            $('#list ul.list-group').html(response.html);

                        }
                    }
                });
            }
        });
    });

    $('#project_id').change(function () {
        var id = $(this).val();

        // For getting dependent task
        var dependentTaskUrl = '{{route('client.all-tasks.dependent-tasks', [':id', ':taskId'])}}';
        dependentTaskUrl = dependentTaskUrl.replace(':id', id);
        dependentTaskUrl = dependentTaskUrl.replace(':taskId', '{{ $punchitem->id }}');
        $.easyAjax({
            url: dependentTaskUrl,
            type: "GET",
            success: function (data) {
                $('#dependent_task_id').html(data.html);
            }
        })
    });

</script>

@endpush
