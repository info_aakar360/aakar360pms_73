@extends('layouts.client-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <b>Name</b>
                        <br>
                        {{ $inspection->name }}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Type</b>
                        <br>
                        {{ get_ins_type_name($inspection->type) }}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Description</b>
                        <br>
                        {!! $inspection->description !!}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <b>Files</b>
                        <br>
                        <div class="row" id="list">
                            <ul class="list-group" id="files-list">
                                @forelse($files as $file)
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-9">
                                                {{ $file->filename }}
                                            </div>
                                            <div class="col-md-3">
                                                @if($file->external_link != '')
                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 'local')
                                                    <a target="_blank" href="{{ asset_url('task-files/'.$file->inspection_id.'/'.$file->hashname) }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>

                                                @elseif(config('filesystems.default') == 's3')
                                                    <a target="_blank" href="{{ $url.'task-files/'.$file->inspection_id.'/'.$file->filename }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'google')
                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @elseif(config('filesystems.default') == 'dropbox')
                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                       data-toggle="tooltip" data-original-title="View"
                                                       class="btn btn-info btn-circle"><i
                                                                class="fa fa-search"></i></a>
                                                @endif

                                                {{--@if(is_null($file->external_link))--}}
                                                    {{--<a href="{{ route('client.task-files.download', $file->id) }}"--}}
                                                       {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                       {{--class="btn btn-inverse btn-circle"><i--}}
                                                                {{--class="fa fa-download"></i></a>--}}
                                                {{--@endif--}}

                                                <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" data-file-id="{{ $file->id }}"
                                                   data-pk="list" class="btn btn-danger btn-circle sa-delete"><i class="fa fa-times"></i></a>
                                                <span class="clearfix m-l-10">{{ $file->created_at->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                    </li>
                                @empty
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-10">
                                                @lang('messages.noFileUploaded')
                                            </div>
                                        </div>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="javascript:;" class="btn btn-outline btn-success btn-sm createTaskCategory">Add Question <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="row">
                        @foreach($questions as $question)
                        <div class="col-md-12" style="padding-bottom: 20px;">
                            <div class="col-md-12" style="background-color: #c3c1c1; padding: 13px;">{{ $question->category }}</div>
                            <div class="col-md-12">
                                <table class="table-striped" style="width: 100%;">
                                    <?php
                                        $q = \App\InspectionQuestion::where('category',$question->category)->get();
                                    ?>
                                    @foreach($q as $p=>$r)
                                    <tr>
                                        <td style="width: 10%;">{{ $p+1 }}</td>
                                        <td style="width: 30%;">{{ $r->question }}</td>
                                        <td style="width: 30%;">{{ get_field_name($r->input_field_id) }}</td>
                                        <td style="width: 30%;">
                                            <?php
                                            $ia = \App\InspectionAnswer::where('inspection_question_id',$r->id)->where('inspection_id',$inspection->id)->get();
                                            ?>
                                            @if($ia)
                                                @foreach($ia as $i)
                                                    <b>{{ get_user_name($i->answered_by) }}</b>
                                                    <br>
                                                    {{ $i->answer }}<br>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <form method="post" id="questionData">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>

                    <div class="modal-body" style="max-height: 400px; overflow-y: scroll;">

                        <div class="input-group col-md-12" style="float: left; padding-right: 5px;">
                            <input type="text" class="form-control" name="category" id="qty" placeholder="Category">
                        </div>
                        <input type="hidden" value="{{ $inspection->id }}" name="inspection_id">
                        @csrf
                        <div class="col-md-12">&nbsp;</div>
                        <div id="sortable">
                            <div class="col-xs-12 item-row margin-top-5" style="padding-bottom: 10px;">
                                <div class="col-md-12" style="float: left; padding-right: 20px;">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="input-group col-md-7" style="float: left; padding-right: 5px;">
                                                <input type="text" class="form-control" name="question[1]" id="rate" placeholder="Question">
                                            </div>
                                            <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                                <select class="selectpicker form-control" name="input_field_id[1]" data-style="form-control">
                                                    <option value="">Please select input field</option>
                                                    @foreach($fields as $category)
                                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 m-t-5">
                            <button type="button" class="btn btn-info" id="add-item">
                                <i class="fa fa-plus"></i>
                                @lang('modules.invoices.addItem')
                            </button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default closeBtn" data-dismiss="modal">Close</button>
                        <button type="button" class="btn blue" onclick="creatBoqCategory()">Save changes</button>
                    </div>

            </div>
            <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>

<script>
    $('.createTaskCategory').click(function(){
        {{--var url = '{{ route('client.inspectionName.createName')}}';--}}
        $('#modelHeading').html("Create New");
//        $.ajaxModal('#taskCategoryModal', url);
        $('#taskCategoryModal').show();
    })

    $('.closeBtn').click(function(){
        {{--var url = '{{ route('client.inspectionName.createName')}}';--}}
        $('#modelHeading').html("Create New");
//        $.ajaxModal('#taskCategoryModal', url);
        $('#taskCategoryModal').hide();
    })

    $('.editTaskCategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('client.inspectionName.editName',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $(document).ready(function() {
        $('#example').DataTable( {
            deferRender:    true,
            scrollCollapse: true,
            scroller:       true
        } );
    } );

    $('ul.showProjectTabs .projectTasks').addClass('tab-current');

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('client.inspectionName.destroyName',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    $.each(rData, function( index, value ) {
                        var selectData = '';
                        selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                        options.push(selectData);
                    });

                    $('#category_id').html(options);
                    $('#category_id').selectpicker('refresh');
                }
            }
        });
    });

    function showData(val,row) {
        var url = "{{ route('client.inspectionName.getData') }}";
        var token = "{{ csrf_token() }}";
        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, 'id':val, 'row_id':row},
            success: function (data) {
                $('#ansData'+row).html(data.html);
            }
        });
    }

    var r = 1;

    $('#add-item').click(function () {
        r++;
        var item = '<div class="col-xs-12 item-row margin-top-5">'


            +'<div class="input-group col-md-7" style="float: left; padding-right: 5px;">'
            +'<input type="text" class="form-control" name="question['+r+']" id="rate" placeholder="Question">'
            +'</div>'
            +'<div class="col-md-4" style="float: left; padding-right: 20px;">'
            +'<div class="form-group">'
            +'<select class="select2 form-control" name="input_field_id['+r+']" data-style="form-control">'
            +'<option value="">Select input field</option>';
            @foreach($fields as $category)
            item += '<option value="{{ $category->id }}">{{ $category->title }}</option>';
            @endforeach
            item += '</select>'
            +'</div>'
            +'</div> <input type="hidden" name="serial" value="'+r+'">'


            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortable").fadeIn(500);
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    function creatBoqCategory() {
        $.easyAjax({
            url: '{{route('client.inspectionName.storeQuestion')}}',
            container: '#questionData',
            type: "POST",
            data: $('#questionData').serialize(),
            success: function (data) {

                    var msgs = "@lang('Question updated successfully')";
                    $.showToastr(msgs, 'success');
                    window.location.reload();

            }
        })
        return false;
    }
</script>
@endpush