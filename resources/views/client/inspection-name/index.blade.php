@extends('layouts.client-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('client.inspectionName.createName')}}" class="btn btn-outline btn-success btn-sm">Create New <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>

                            <th>Added By</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($names as $key=>$category)
                            <tr id="cat-{{ $category->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ ucwords($category->name) }}</td>
                                <td>{{ get_ins_type_name($category->type) }}</td>
                                <td>{!!  $category->description !!}</td>

                                <td>
                                    <?php
                                    $userx = \App\User::where('id',$category->added_by)->first();
                                    ?>
                                    @if($userx->image)
                                        <img data-toggle="tooltip" data-original-title="{{ ucwords($userx->name) }}" src="{{ asset('user-uploads/avatar/' . $userx->image) }}"
                                             alt="user" class="img-circle" width="30">
                                        {{--<img src="{{  asset('user-uploads/avatar/' . $userx->image) }}" alt="user" class="img-circle" width="30">--}}
                                    @else
                                        <img data-toggle="tooltip" data-original-title="{{ ucwords($userx->name) }}" src="{{ asset('default-profile-2.png') }}"
                                             alt="user" class="img-circle" width="30">
                                        {{--<img src="{{ asset('default-profile-2.png') }}" alt="user" class="img-circle" width="30">--}}
                                    @endif
                                    {{--                                    {{ ucwords($userx->name) }} <br>--}}
                                </td>
                                <td>
                                    <a href="{{ route('client.inspectionName.editName',[$category->id])}}" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle"
                                       data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    &nbsp;
                                    {{--<a href="{{ route('client.inspectionName.inspectionQuestion',[$category->id])}}" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle"--}}
                                       {{--data-toggle="tooltip" data-original-title="Add Question"><i class="fa fa-question" aria-hidden="true"></i></a>--}}
                                    {{--&nbsp;--}}

                                    <?php
                                    $uid = \Illuminate\Support\Facades\Auth::user()->id;
                                    $urole = \App\RoleUser::where('user_id',$uid)->first();
                                    $userRolex = \App\Role::where('id',$urole->role_id)->first();
                                    if ($category->added_by == \Illuminate\Support\Facades\Auth::user()->id || $userRolex->name == 'client'){ ?>
                                    {{--<a href="{{ route('client.inspectionName.assignUser',[$category->id])}}" data-cat-id="{{ $category->id }}" class="btn btn-info btn-circle"--}}
                                       {{--data-toggle="tooltip" data-original-title="Assign Users"><i class="fa fa-plus" aria-hidden="true"></i></a>--}}
                                    {{--&nbsp;--}}
                                    <a href="javascript:;" data-cat-id="{{ $category->id }}" class="btn btn-sm btn-danger btn-circle delete-category" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                    <?php } ?>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">No Inspection Found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        background: #ffffff !important;
    }
</style>

<script>
    $('.createTaskCategory').click(function(){
        var url = '{{ route('client.inspectionName.createName')}}';
        $('#modelHeading').html("Create New");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $('.editTaskCategory').click(function(){
        var id = $(this).data('cat-id');
        var url = '{{ route('client.inspectionName.editName',':id')}}';
        url = url.replace(':id', id);
        $('#modelHeading').html("Edit Category");
        $.ajaxModal('#taskCategoryModal', url);
    })

    $(document).ready(function() {
        $('#example').DataTable( {
            deferRender:    true,
            scrollCollapse: true,
            scroller:       true
        } );
    } );

    $('ul.showProjectTabs .projectTasks').addClass('tab-current');

    $('.delete-category').click(function () {
        var id = $(this).data('cat-id');
        var url = "{{ route('client.inspectionName.destroyName',':id') }}";
        url = url.replace(':id', id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token, '_method': 'DELETE'},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
//                                    swal("Deleted!", response.message, "success");
                    $('#cat-'+id).fadeOut();
                    var options = [];
                    var rData = [];
                    rData = response.data;
                    $.each(rData, function( index, value ) {
                        var selectData = '';
                        selectData = '<option value="'+value.id+'">'+value.category_name+'</option>';
                        options.push(selectData);
                    });

                    $('#category_id').html(options);
                    $('#category_id').selectpicker('refresh');
                }
            }
        });
    });
</script>


@endpush