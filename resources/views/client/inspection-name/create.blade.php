@extends('layouts.client-app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title" style="color: #002f76"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('client.dashboard.index') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">

                {!! Form::open(['id'=>'createBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
                <div class="form-body">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="title" id="title" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 ">
                            <div class="form-group">
                                <label>Type</label>
                                <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                    <option value="">Please Select Type</option>
                                    @foreach($types as $employee)
                                        <option value="{{ $employee->id }}">{{ ucwords($employee->title) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 ">
                            <div class="form-group">
                                <label class="control-label">@lang('app.description')</label>
                                <textarea id="description" name="description" class="form-control summernote"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row m-b-20">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                        <div id="file-upload-box" >
                            <div class="row" id="file-dropzone">
                                <div class="col-md-12">
                                    <div class="dropzone"
                                         id="file-upload-dropzone">
                                        {{ csrf_field() }}
                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                        <input name="image_url" id="image_url" type="hidden" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="taskID" id="taskID">
                    </div>
                </div>
                <div id="sortable">
                    <div class="col-xs-12 item-row margin-top-5" style="padding-bottom: 10px;">
                        <div class="col-md-12" style="float: left; padding-right: 20px;">
                            <div class="row">
                                <div class="form-group">
                                    <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                        <input type="text" class="form-control" name="category[1]" id="rate" placeholder="Category">
                                    </div>
                                    <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                        <input type="text" class="form-control" name="question[1]" id="rate" placeholder="Question">
                                    </div>
                                    <div class="input-group col-md-4" style="float: left; padding-right: 5px;">
                                        <select class="selectpicker form-control" name="input_field_id[1]" data-style="form-control">
                                            <option value="">Please select input field</option>
                                            @foreach($fields as $category)
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 m-t-5">
                    <button type="button" class="btn btn-info" id="add-item">
                        <i class="fa fa-plus"></i>
                        @lang('modules.invoices.addItem')
                    </button>
                </div>
                <div class="col-md-12">&nbsp;</div>
                <div class="form-actions">
                    <button type="button" id="store-task" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('footer-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('client.inspectionName.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#taskID').val();
            formData.append('task_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('Inspection created successfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('client.inspection-name.index') }}'

        });
        var r = 1;

        $('#add-item').click(function () {
            r++;
            var item = '<div class="col-xs-12 item-row margin-top-5">'

                +'<div class="input-group col-md-4" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" name="category['+r+']" id="rate" placeholder="Category">'
                +'</div>'
                +'<div class="input-group col-md-4" style="float: left; padding-right: 5px;">'
                +'<input type="text" class="form-control" name="question['+r+']" id="rate" placeholder="Question">'
                +'</div>'
                +'<div class="col-md-3" style="float: left; padding-right: 20px;">'
                +'<div class="form-group">'
                +'<select class="select2 form-control" name="input_field_id['+r+']" data-style="form-control">'
                +'<option value="">Select input field</option>';
                @foreach($fields as $category)
                    item += '<option value="{{ $category->id }}">{{ $category->title }}</option>';
                @endforeach
                item += '</select>'
                +'</div>'
                +'</div> <input type="hidden" name="serial" value="'+r+'">'


                +'<div class="col-md-1 text-right visible-md visible-lg">'
                +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
                +'<div class="row">'
                +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
                +'</div>'
                +'</div>'
                +'</div>';
            $(item).hide().appendTo("#sortable").fadeIn(500);
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
                calculateTotal();
            });
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $('#createBoqCategory').submit(function () {
            $.easyAjax({
                url: '{{route('client.inspectionName.storeName')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (data) {
                    $('#storeTask').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = data.taskID;
                        $('#taskID').val(data.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.taskCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('client.inspection-name.index') }}'
                    }
                }
            })
            return false;
        })



        {{--$('#save-category').click(function () {--}}
            {{--$.easyAjax({--}}
                {{--url: '{{route('client.inspectionName.storeName')}}',--}}
                {{--container: '#createProjectCategory',--}}
                {{--type: "POST",--}}
                {{--data: $('#createBoqCategory').serialize(),--}}
                {{--success: function (response) {--}}
                    {{--if(response.status == 'success'){--}}
                        {{--window.location.reload();--}}
                    {{--}--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}

        //    update task
        $('#store-task').click(function () {
            $.easyAjax({
                url: '{{route('client.inspectionName.storeName')}}',
                container: '#createBoqCategory',
                type: "POST",
                data: $('#createBoqCategory').serialize(),
                success: function (data) {
                    $('#createBoqCategory').trigger("reset");
                    $('.summernote').summernote('code', '');
                    if(myDropzone.getQueuedFiles().length > 0){
                        taskID = data.taskID;
                        $('#taskID').val(data.taskID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('messages.taskCreatedSuccessfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('client.inspection-name.index') }}'
                    }
                }
            })
        });
    </script>
@endpush