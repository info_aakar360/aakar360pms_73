@extends('layouts.auth')
<!-- local css -->
<style>
    .scrollable {
        height: 600px;
        /* or any value */
        overflow-y: auto;
    }
</style>
@section('content')
    <form class="form-horizontal form-material" id="loginform" action="{{ route('postregister') }}" method="POST"
          autocomplete="off">
        {{ csrf_field() }}
        <h3 class="box-title m-b-0">Register Now</h3>
        <small>Create your account and enjoy</small>
            <div class="form-group m-t-20 {{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control" type="text" id="name" name="name" required="" value="{{ old('name') }}"
                           autofocus placeholder="Name">
                    @if ($errors->has('name'))
                        <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control" type="email" value="{{ old('email') }}" name="email" id="email"
                           required="" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control" type="text" onkeypress="return isNumberKey(event)" onkeyup="sendotp();"
                           maxlength="10" pattern="\d{10}" value="{{ old('mobile') }}" name="mobile" id="mobile"
                           required="" placeholder="Mobile number">

                    @if ($errors->has('mobile'))
                        <span class="help-block">
                        {{ $errors->first('mobile') }}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('otp') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control" type="text" value="{{ old('mobile') }}" name="otp" id="otp" required=""
                           placeholder="otp">

                    @if ($errors->has('otp'))
                        <span class="help-block">
                        {{ $errors->first('otp') }}
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('otp') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <label>Gender</label>
                </div>
                <div class="col-xs-6">
                    <input type="radio" value="male" name="gender" required="">
                    @if ($errors->has('gender'))
                        <span class="help-block">
                        {{ $errors->first('gender') }}
                    </span>
                    @endif
                    <label>Male</label>
                </div>
                <div class="col-xs-6">
                    <input type="radio" value="female" name="gender" required="">
                    @if ($errors->has('gender'))
                        <span class="help-block">
                        {{ $errors->first('gender') }}
                    </span>
                    @endif
                    <label>Female</label>
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control" type="password" id="password" name="password"
                           onkeyup="checkPasswordStrength();" required="" placeholder="Password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                    @endif
                    <div id="password-strength-status"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-12">
                    <input class="form-control" id="password-confirm" name="password_confirmation"
                           onkeyup="checkPasswordStrength();" type="password" required=""
                           placeholder="Confirm Password">
                    <div id="CheckPasswordMatch"></div>
                </div>
            </div>
            @if(!is_null($setting->google_recaptcha_key))
                <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                    <div class="col-xs-12">
                        <div class="g-recaptcha"
                             data-sitekey="{{ $setting->google_recaptcha_key }}">
                        </div>
                        @if ($errors->has('g-recaptcha-response'))
                            <div class="help-block with-errors">{{ $errors->first('g-recaptcha-response') }}</div>
                        @endif
                    </div>
                </div>
            @endif
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    {{ csrf_field() }}
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" id="submitype"
                            type="submit">Sign Up
                    </button>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>Already have an account? <a href="{{ route('login') }}" class="text-primary m-l-5"><b>Sign In</b></a>
                    </p>
                </div>
            </div>

    </form>
@endsection


@section('footerscripts')
    <script>
        function checkPasswordStrength() {
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 8) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('weak-password');
                $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('strong-password');
                    $('#password-strength-status').html("Strong");
                } else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('medium-password');
                    $('#password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }

        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#password-confirm").val();
            if (password != confirmPassword) {
                $("#CheckPasswordMatch").html("Passwords does not match!").addClass('weak-password');
                $("#submitype").attr('disabled', 'disabled');
            } else {
                $("#CheckPasswordMatch").html("Passwords match.").addClass('strong-password');
                $("#submitype").removeAttr('disabled');
            }
        }

        $(document).ready(function () {
            $("#password-confirm").keyup(checkPasswordMatch);
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        $('#loginform').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ route('postregister') }}',
                container: '#loginform',
                type: "POST",
                data: $('#loginform').serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.href = '{{ url('admin/projects') }}';
                    } else {
                        alert(response.message);
                    }
                }
            });
            return false;
        });
        function sendotp() {
            var mobile = $("#mobile").val();
            if (mobile.length == 10) {
                $.ajax({
                    url: '{{ route('sendotp') }}',
                    container: '#loginform',
                    type: "POST",
                    data: {
                        _token: '{{ csrf_token() }}',
                        mobile: mobile
                    },
                    success: function (response) {
                        alert(response.message);
                    }
                });
            }
            return false;
        }
    </script>
@endsection
