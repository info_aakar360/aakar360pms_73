@extends('layouts.auth')

@section('content')


    <form class="form-horizontal" id="loginform"  method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        @if (session('status'))
            <div class="alert alert-success m-t-10">
                {{ session('status') }}
            </div>
        @endif

        <h3 class="box-title m-t-40 m-b-0">@lang('app.validateOTP')</h3>

        <div class="form-group {{ $errors->has('otp') ? 'has-error' : '' }}">
            <div class="col-xs-12">
                <input class="form-control" type="text" required id="otp" name="otp" value="{{ old('otp') }}" required="" placeholder="@lang('app.otp')">
                @if ($errors->has('otp'))
                    <span class="help-block">
                        {{ $errors->first('otp') }}
                    </span>
                @endif
            </div>
        </div>
        @if(!is_null($setting->google_recaptcha_key))
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="col-xs-12">
                    <div class="g-recaptcha"
                         data-sitekey="{{ $setting->google_recaptcha_key }}">
                    </div>
                    @if ($errors->has('g-recaptcha-response'))
                        <div class="help-block with-errors">{{ $errors->first('g-recaptcha-response') }}</div>
                    @endif
                </div>
            </div>
        @endif
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-rounded btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">@lang('app.submitOTP')</button>
            </div>
        </div>

        <div class="form-group m-b-0">
            <div class="col-sm-12 text-center">
                <p><a href="{{ route('login') }}" class="text-primary m-l-5"><b>@lang('app.login')</b></a></p>
            </div>
        </div>

    </form>
@endsection

@section('footerscripts')
    <script>
    $('#loginform').submit(function (e) {
        e.preventDefault();
        $.ajax({
        url: '{{ route('post-validate-otp') }}',
        container: '#loginform',
        type: "POST",
        data: $('#loginform').serialize(),
        success: function (response) {
        if(response.status == 'success'){
          window.location.href = '{{ route('reset-password') }}';
        }else{
        alert(response.message);
        }
        }
        });
        return false;
        });
</script>
@endsection