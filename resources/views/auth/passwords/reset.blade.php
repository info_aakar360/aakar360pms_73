@extends('layouts.auth')

@section('content')


    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="form-horizontal"  method="POST" id="loginform" action="{{ route('post-reset-password') }}">
        {{ csrf_field() }}

        <h3 >Reset Password</h3>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-xs-12 ">Password</label>
            <div class="col-xs-12">
                <input  type="password" id="password" onkeyup="checkPasswordStrength();"  class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                @endif
                <div id="password-strength-status" ></div>
            </div>
        </div>

        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm" class="col-xs-12 ">Confirm Password</label>
            <div class="col-xs-12">
                <input id="password-confirm" id="password-confirm"  type="password" class="form-control" name="password_confirmation" required>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                @endif
                <div id="CheckPasswordMatch" ></div>

            </div>
        </div>
        @if(!is_null($setting->google_recaptcha_key))
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="col-xs-12">
                    <div class="g-recaptcha"
                         data-sitekey="{{ $setting->google_recaptcha_key }}">
                    </div>
                    @if ($errors->has('g-recaptcha-response'))
                        <div class="help-block with-errors">{{ $errors->first('g-recaptcha-response') }}</div>
                    @endif
                </div>
            </div>
        @endif
        <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
                <button class="btn btn-primary btn-rounded btn-lg btn-block text-uppercase waves-effect waves-light" id="submitype" type="submit">Reset Password</button>
            </div>
        </div>

        <div class="form-group m-b-0">
            <div class="col-sm-12 text-center">
                <p><a href="{{ route('login') }}" class="text-primary m-l-5"><b>@lang('app.login')</b></a></p>
            </div>
        </div>

    </form>
@endsection

@section('footerscripts')
    <script>
        function checkPasswordStrength(){
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 8) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('weak-password');
                $('#password-strength-status').html("Weak (should be atleast 8 characters.)");
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('strong-password');
                    $('#password-strength-status').html("Strong");
                } else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('medium-password');
                    $('#password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
                }
            }
        }
        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#password-confirm").val();
            if (password != confirmPassword){
                $("#CheckPasswordMatch").html("Passwords does not match!").addClass('weak-password');
                $("#submitype").attr('disabled','disabled');
            }else{
                $("#CheckPasswordMatch").html("Passwords match.").addClass('strong-password');
                $("#submitype").removeAttr('disabled');
            }
        }
        $(document).ready(function () {
            $("#password-confirm").keyup(checkPasswordMatch);
        });
        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        $('#loginform').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ route('post-reset-password') }}',
                container: '#loginform',
                type: "POST",
                data: $('#loginform').serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        alert("Password Updated please login to continue");
                        window.location.href = '{{ url('login') }}';
                    }else{
                        alert(response.message);
                    }
                }
            });
            return false;
        });
    </script>
@endsection
