@component('mail::message')
# New Task

@lang('email.newTask.subject')

<h5>Task @lang('app.details')</h5>

@component('mail::text', ['text' => $content])

@endcomponent


@component('mail::button', ['url' => $url])
@lang('app.view') Task
@endcomponent

@lang('email.regards'),<br>
{{ config('app.name') }}
@endcomponent
